<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @Export raw data
  */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

  // Load template file
  $pexcel_obj = $template_reader->load($templateRoot . "rawdata.xlsx");
  $rawdataSheet = $pexcel_obj->getSheet(0);

 // Row Position
 $rowPosition = 1;

 // Pull data from $_POST Request
 $rawResults = unserialize(gzuncompress(base64_decode($_POST['rawdata'])));

 // Student Count
 $studentCount = count($rawResults);

 // loop through array of rows
 foreach ($rawResults as $theRow) {
     $colPosition = 0;
     foreach ($theRow as $vkey => $value) {

         // Build up Student IDs Order for Export Detailed Results Spreadsheet
         if ($exportDetailed && $rowPosition > 1 && $vkey == "studentid") {
             $studentIDs[] = $value;
         }

         // Exclude weighting columns (if display/export switched off)
         if (preg_match("/(stationweighting|overallweighted)/i", $vkey) && !$displayweighted) {
             continue;
         }

         // Exclude percentages columns
         if (preg_match("/(examinerpercent|stationpercent|overallpercent)/i", $vkey) && !$displaypercent) {
             continue;
         }

         // Exclude grades columns
         if (preg_match("/(examinerpassed|stationpassed|overallgrade)/i", $vkey) && !$displaygrade) {
             continue;
         }

         // If not station result ID
         if (preg_match("/(stationresultid|stationid|completed|stationgrsfail|stationdivergence|failedgrs)/i", $vkey)) {
             continue;
         }

         // Strip tags
         $value = strip_Tags_Omis(adjustHTML($value));

         // Collect student data for other spreadsheets - Complete Detailed Results Spreadsheet
         if ($exportDetailedComplete) {

             $rawData[$rowPosition - 1][$colPosition] = $value;

         }

         // Student ID or Examiner ID
         if (in_array($vkey, ['studentid', 'examnum']) || preg_match("/examinerid/i", $vkey)) {

             $rawdataSheet->setCellValueExplicitByColumnAndRow(
                 $colPosition++,
                 $rowPosition,
                 $value,
                 PHPExcel_Cell_DataType::TYPE_STRING2
             );

         } else {

             $rawdataSheet->setCellValueByColumnAndRow($colPosition++, $rowPosition, $value);

         }

     }
     $rowPosition++;
 }

 // Add to master worksheet
 $masterWorkbook->addExternalSheet($rawdataSheet, 0);
