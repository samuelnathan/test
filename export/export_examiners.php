<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Examiners Export
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

use \OMIS\Auth\Role as Role;

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Load template file
$pexcel_obj = $template_reader->load($templateRoot . "examiners.xlsx");

// Get Sheet
$examiners_sheet = $pexcel_obj->getSheet(0);
$examiners_sheet->setTitle(ucwords(gettext('examiners')));

// Pull data from $_POST Examiner Request
$examiners = unserialize(gzuncompress(base64_decode($_POST['examinerdata'])));

// Row Position
$rwPosition = 2;

// loop through array of rows
foreach ($examiners as $theRow) {
    // Loop through columns
    $colPosition = 0;
       
    foreach ($theRow as $vkey => $value) {
    
        // clean && strip tags
        $value = strip_Tags_Omis(adjustHTML($value));
      
        // Set Cell Value
        if ($vkey == 0) {
            $examiners_sheet->setCellValueExplicitByColumnAndRow(
                $colPosition++,
                $rwPosition,
                $value,
                PHPExcel_Cell_DataType::TYPE_STRING2
            );
        } else {
            $examiners_sheet->setCellValueByColumnAndRow(
                $colPosition++,
                $rwPosition,
                $value
            );
        }
    }
    $rwPosition++;
}

// Add to master workbook
$masterWorkbook->addExternalSheet($examiners_sheet, ++$sheetIndex);