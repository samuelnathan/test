<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @ export detailed complete
  */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Push item titles onto main title row
 $itemsSizePosition = [];

 // + 1 to include student ID field
 $columnPosition = $numberOfColumns;

 // Default number columns per station column
 $defaultNumSubColumns = (int)$displayweighted + (int)$displaypercent + (int)$displaygrade;

 // Load template file
 $pexcel_obj = $template_reader->load($templateRoot . "scenarios_complete.xlsx");
 $cdetailed_sheet = $pexcel_obj->getSheet(0);


 /*
  * Copy title row for the raw data, empty it and prepend again as 
  * we have 2 title rows aligned
  */ 
 array_unshift($rawData, (array_fill(0, sizeof($rawData[0]), '')));

 /*
  * Add scenario item titles to raw data array
  */
 foreach ($scenarioItemTitles as $identifier => $scenarioTitles) {

     $titleRowOne = array_merge(...(array_column($scenarioTitles, 1)));
     $titleRowTwo = array_merge(...(array_column($scenarioTitles, 2)));
     
     $numOfItems = sizeof($titleRowTwo);
     $columnPosition += (4 + $defaultNumSubColumns);

     // Repeat results in case of multi results
     $numOfResults = $multiExaminerCounts[$identifier];

     // Sub columns offset
     $columnOffset = $numOfResults > 1 ? ($defaultNumSubColumns + (int)!$displayweighted) : 0;

     for ($resultIndex=1; $resultIndex<=$numOfResults; $resultIndex++) {

        // Current column position plus the number of additional sub columns
        $columnPosition += $columnOffset + $additionalSubColumns;

        // Take note of thew number of items and column position
        $itemsSizePosition[$identifier][] = [
            'num_of_items' => $numOfItems,
            'start_position' => $columnPosition
        ];

        // Add item descriptions to the title row
        array_splice($rawData[0], $columnPosition, 0, $titleRowOne);
        array_splice($rawData[1], $columnPosition, 0, $titleRowTwo);

        // Increment the column position by the number of items titles added
        $columnPosition += $numOfItems;

     }

 }

 
 // Row Count
 $rowCount = sizeof($rawData);
 $rowPosition = 1;
 $colPosition = 0;

 /*
  * Add scenario data to the raw data array
  */
 for ($rowIndex=2; $rowIndex<$rowCount; $rowIndex++) {

     // Student ID
     $studentID = $rawData[$rowIndex][0];

     /*
      * Loop through the expected number of item scores and their column positions per scenario,
      * in turn insert the item scores into each array (raw_data) that represents a row in the excel sheet
      */
     foreach ($itemsSizePosition as $identifier => $sizePositions) {

         // Loop through all item scores per station (can be multiple)
         foreach ($sizePositions as $resultIndex => $values) {

           $numOfItems = $values['num_of_items'];
           $startPosition = $values['start_position'];

           // Item scores found then populate data row aligned with the item titles (descriptions)
           if (isset($studentScenarioData[$studentID][$identifier][$resultIndex])) {

             $itemScoresData = $studentScenarioData[$studentID][$identifier][$resultIndex];
             array_splice($rawData[$rowIndex], $startPosition, 0, $itemScoresData);

           // If no item scores found then insert blanks (NA's)
           } else {
               
             array_splice($rawData[$rowIndex], $startPosition, 0, array_fill(0, $numOfItems, 'NA'));
             
           }

         }

     }

 }

 // Populate the template with complete set of data now contained in $rawData[]
 foreach ($rawData as $rawDataRow) {

     // Column Position
     $colPosition = 0;

     // Loop through values
     foreach ($rawDataRow as $columnIndex => $value) {

         // Student ID
         if ($columnIndex == 0) {

             $cdetailed_sheet->setCellValueExplicitByColumnAndRow(
                     $colPosition++,
                     $rowPosition,
                     $value,
                     PHPExcel_Cell_DataType::TYPE_STRING2
            );

         // All other fields 
         } else { 

             $cdetailed_sheet->setCellValueByColumnAndRow(
                     $colPosition++,
                     $rowPosition,
                     $value
             );

         }

     }

     // Increment row position
     $rowPosition++;

 }

 // Format the item text/description fields
 $cdetailed_sheet->getStyle('A1:JN1')->getAlignment()->setWrapText(true);
 $cdetailed_sheet->getRowDimension('1')->setRowHeight(50);

 // Export Detailed On
 $masterWorkbook->addExternalSheet(
         $cdetailed_sheet,
         ++$sheetIndex
 );
 