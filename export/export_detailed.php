<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 * @Detailed Export
 */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;
 
 // Include helper functions
 require_once 'ExportHelper.php';

 // Initial Variables
 $tempItemTitles = [];
 $position = ['col' => 0, 'row' => 2];
 $counts = ['item' => 1, 'indicator' => 1];

 // Is self assessment
 $selfAssess = $db->selfAssessments->scoresheetHasSelfAssessment($formID);

 // Is weighted form
 $weightedForm = ($displayweighted && $db->forms->hasWeightedItems($formID));

 // Export Detailed On
 if ($exportDetailed) {

     // Load template file
     $pexcel_obj = $template_reader->load($templateRoot . "scenario.xlsx");
     
     // Get Sheet
     $detailed_sheet = $pexcel_obj->getSheet(0);

     /*
      * Clean form name in preparation for excel export (used as spreadsheet title)
      */
     $formName = remove_alt_char(
         html_entity_decode(
             $scenario['form_name'],
             ENT_QUOTES,
             'UTF-8'
         )
     );

     $spreadsheetTitle = mb_substr($formName, 0, 31);

     // Cannot have repeat spreadsheet titles
     $spreadsheetNum = 1;
     while (in_array($spreadsheetTitle, $spreadsheetTitles)) {

         $spreadsheetTitle = mb_substr($spreadsheetTitle, 0, 29) . $spreadsheetNum;
         $spreadsheetNum++;

     }

     // Set Spreadsheet Title
     $detailed_sheet->setTitle($spreadsheetTitle);
     $spreadsheetTitles[] = $spreadsheetTitle;

     // Detailed Scenario Titles
     foreach ($detailedTitles as $vkey => $value) {

         $detailed_sheet->setCellValueByColumnAndRow($position['col']++, 2, $value);

     }
 
 }
 
 // Station Items
 $stationItems = $db->forms->getFormItems($formID);
 
 // Loop through station items text
 while ($itemrow = mysqli_fetch_assoc($stationItems)) {
 
    // Strip Tags
    if ($exportDetailed || $exportDetailedComplete) {
         
        // Self assessment
        if ($itemrow['self_assessment']) {

            $question = $db->selfAssessments->getQuestion($exam_id, $itemrow['item_order']);
            $itemrow['text'] = empty($question) ? "No self assessment question data found" : $question['question'];  

        } else {
          
            $itemrow['text'] = strip_Tags_Omis($itemrow['text']);

        }

        // Parent form weighted?
        $itemrow['form_weighted'] = $weightedForm;
        
    }
 
    // Structure the item titles
    $itemTitles = structureItemTitles($itemrow, $counts, $position);

    // Populate the item titles    
    if ($exportDetailed) 
       populateItemCells($detailed_sheet, $itemTitles);

    // Pass titles onto the comprehensive sheet
    if ($exportDetailedComplete)
       $tempItemTitles[] = $itemTitles;
 
 }

 // Collect Item Descs for Complete Detailed Spreadsheet
 if ($exportDetailedComplete) {
 
    // If Combine By Station # / Station TAG
    if ($cnm == STATION_COMBINE_DEFAULT || $cnm == STATION_COMBINE_TAG) {

        setA($scenarioItemTitles, $scenario_group, []);

        if (sizeof($scenarioItemTitles[$scenario_group]) == 0 || sizeof($tempItemTitles) > sizeof(end($scenarioItemTitles))) {
            $scenarioItemTitles[$scenario_group] = $tempItemTitles;
        }

    } else {
        
        setA($scenarioItemTitles, $formID, []);
        $scenarioItemTitles[$formID] = $tempItemTitles;

    }

 }

  // Get form station identifiers
 $stationIDs = array_column( 
  \OMIS\Database\CoreDB::into_array(
      $db->forms->getFormStations($formID, $sessionIDs)
 ), 'station_id');

 // Student Results
 $studentsResults = $db->results->getResultsByStationsGroups(
        $stationIDs, $groups, $studentIDs
 );
 
 // Go through student lists
 while ($eachrow = mysqli_fetch_assoc($studentsResults)) {
 
    // Detailed Export On
    if ($exportDetailed) {
 
        // Row Position
        $position['row']++;
 
        // Column Position
        $position['col'] = 0;
 
        // Student ID
        $detailed_sheet->setCellValueExplicitByColumnAndRow(
            $position['col']++,
            $position['row'],
            $eachrow['student_id'],
            PHPExcel_Cell_DataType::TYPE_STRING2
        );

        // Student Name
        if ($displaystudentname) {

            $detailed_sheet->setCellValueByColumnAndRow(
                $position['col']++,
                $position['row'],
                $eachrow['surname']
            )->setCellValueByColumnAndRow(
                $position['col']++, 
                $position['row'], 
                $eachrow['forename'])
            ;

        }
 
        // If Student Nationality
        if ($displaynationality)
            $detailed_sheet->setCellValueByColumnAndRow($position['col']++, $position['row'], $eachrow['nationality']);

        // If Student Order
        if ($displayorder)
            $detailed_sheet->setCellValueByColumnAndRow($position['col']++, $position['row'], $eachrow['student_order']);

        // If Group Names
        if ($displaygroups)
            $detailed_sheet->setCellValueByColumnAndRow($position['col']++, $position['row'], $groupData[$eachrow['group_id']]['group_name']);

        // If Session
        if ($displaysession) {

            $session = $sessionData[$groupData[$eachrow['group_id']]['session_id']];
            $sessionText = formatDate($session['session_date']) . ' ' . $session['session_description'];
            unset($session);
            $detailed_sheet->setCellValueByColumnAndRow($position['col']++, $position['row'], $sessionText);

        }
 
        // If Examiner ID
        if ($exportExaminers) {

            $detailed_sheet->setCellValueExplicitByColumnAndRow(
                $position['col']++,
                $position['row'],
                $eachrow['examiner_id'],
                PHPExcel_Cell_DataType::TYPE_STRING2
            );

        }
 
        // If First Submission
        if ($displaytimestamp) {

            $detailed_sheet->setCellValueByColumnAndRow(
                $position['col']++,
                $position['row'],
                date_format(date_create($eachrow['created_at']), 'H:i:s')
            );

        }

        // Station Total Score
        $detailed_sheet->setCellValueByColumnAndRow(
            $position['col']++,
            $position['row'],
            $eachrow['score']
        );
 
        // Station Percentage (%)
        if ($displaypercent) {

          // Self assessment
          $owner = $selfAssess ? $db->selfAssessments->getOwnerByStudentExam($eachrow['student_id'], $exam_id) : "";

          $realTotalValue = ($selfAssess && $owner != $eachrow['examiner_id']) ? 
              $db->selfAssessments->getNonOwnerPossible($formID) : $scenario_data[$formID]['total_value'];
        
          $detailed_sheet->setCellValueByColumnAndRow(
             $position['col']++,
             $position['row'],
             percent($eachrow['score'], $realTotalValue) . ' %'
          );
 
       }

       // Weighted score
       if ($displayweighted) {
           
          $detailed_sheet->setCellValueByColumnAndRow(
               $position['col']++,
               $position['row'],
               $eachrow['weighted_score']
          );

       }
 
    }
 
    // Loop through station item score values
    $scores = $db->results->getItemScoresPerResult(
        $eachrow['result_id'], true, false, true
    );
 
    // Loop through scores
    $tmpScenarioData = [];
    mysqli_data_seek($stationItems, 0);
    while ($itemrow = mysqli_fetch_assoc($stationItems)) {

        // Parent form weighted?
        $itemrow['form_weighted'] = $weightedForm;        

        // Export detailed on, apply score to cell
        list($itemValue, $itemStructure) = structureItemValues($itemrow, $scores, $position);

        // Populate item values if detailed spreadsheet required
        if ($exportDetailed) 
           populateItemCells($detailed_sheet, $itemStructure);

        // Gather Student Scenario Data
        if ($exportDetailedComplete)
            $tmpScenarioData = array_merge($tmpScenarioData, ...$itemStructure);

    }
 
    // Gather Student Scenario Data
    if ($exportDetailedComplete) {
 
        // If Combine By Station # / Station TAG
        if ($cnm == STATION_COMBINE_DEFAULT || $cnm == STATION_COMBINE_TAG) {
            $studentScenarioData[$eachrow['student_id']][$scenario_group][] = $tmpScenarioData;
        } else {
            $studentScenarioData[$eachrow['student_id']][$formID][] = $tmpScenarioData;
        }

    }

 }

 // Add to master workbook
 if ($exportDetailed) 
   $masterWorkbook->addExternalSheet($detailed_sheet, ++$sheetIndex);
 
 // Clean some arrays
 unset($tmpScenarioData);
 