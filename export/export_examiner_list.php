<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Export examiner list
 */

// 5 Minutes Limit Script Time
set_time_limit(800); 

//Error Reporting = 0
//error_reporting(0);

// Critical Session Check
$session = new OMIS\Session;
$session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

//Initial Variables
$templateRoot = "../files/export_templates/";

// Export examiner lists in either csv or xlsx format
if ((in_array($_POST['todo'], array('exportcsv', 'exportxls'))) && isset($_POST['examiner_check'])) {
    try {

        //Set Timezone
        date_default_timezone_set('Europe/London');

        //Examiners selected
        $examiners_export = $_POST['examiner_check'];

        //Examiners Data
        $examiner_data = unserialize(gzuncompress(base64_decode($_POST['examiners_data'])));

        //File Extension
        switch (filter_input(INPUT_POST, "todo", FILTER_SANITIZE_SPECIAL_CHARS)) {
            case "exportxls":
                $file_extension = "xlsx";
                $file_type = "Excel2007";
                break;
            case "exportcsv":
            default:
                $file_extension = "csv";
                $file_type = "CSV";
        }
        
        //Template Reader
        $template_reader = PHPExcel_IOFactory::createReader('Excel2007');
        
        //Load template file 
        $template_obj = $template_reader->load($templateRoot . "exam_team_list.xlsx");
        $examiners_sheet = $template_obj->getSheet(0);

        //Row index
        $row_index = 2;
        
        $roles = [];
        
        //Loop through examiners and add to sheet
        foreach ($examiners_export as $examiner_id) {
            //Data Row
            $data_row = $examiner_data[$examiner_id];

            //Loop through row values
            $col_index = 0;
            $examiners_sheet->setCellValueExplicitByColumnAndRow(
                $col_index++,
                $row_index,
                strip_Tags_Omis(adjustHTML($examiner_id)), 
                PHPExcel_Cell_DataType::TYPE_STRING2);
            foreach ($data_row as $key => $value) {
                // Ensure that the roles are translated from numbers to names.
                if ($key == "user_role") {
                    // (DW) Make sure we have the name of the row, not its ID.
                    $role_id = $value;
                    $this_role = null;
                    // Check if we know it already.
                    foreach ($roles as $role) {
                        if ($role->role_id == $role_id) {
                            $this_role = $role;
                            break;
                        }
                    }
                    // If not, load it and add it to the list of known Roles.
                    if (is_null($this_role)) {
                        $this_role = \OMIS\Auth\Role::loadID($role_id);
                        $roles[] = $this_role;
                    }
                    $value = gettext($this_role->name);
                }

                $examiners_sheet->setCellValueByColumnAndRow(
                    $col_index++,
                    $row_index,
                    strip_Tags_Omis(adjustHTML($value))
                );
            }

            $row_index++;
        }

        //Output File To Browser
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=\"" . gettext('exam') . "_team_list." . $file_extension . "\"");
        header("Cache-Control: max-age=0");
        header("Cache-Control: private");
        header("Pragma: private");

        $excelWriter = PHPExcel_IOFactory::createWriter($template_obj, $file_type);
        $excelWriter->save('php://output');
    } catch (Exception $e) {
        trigger_error('Error loading file: ' . $e->getMessage());
    }
}