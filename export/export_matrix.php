<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Export exam matrix
 */
define("RETURN_AS_ARRAY", true);
define("BLANK_STUDENT", 1);

// Client has authorization to use this feature
$matrixFeatureEnabled = $db->features->enabled(
        "exam-matrix",
        $_SESSION["user_role"]
);

// Candidate number feature
$candidateFeatureEnabled = $db->features->enabled(
        "candidate-number", 
        $_SESSION["user_role"]
);

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess() || !$matrixFeatureEnabled) {
    echo "You do not have permissions to perform this operation, " .
         "please contact the system administrator for further assistance";
    return false;
}

// Get all circuit colour names
$colours = $db->exams->getAllCircuitColours("circuit_colour");

$PHPExcelFill = [
    "fill" => [
        "type" => PHPExcel_Style_Fill::FILL_SOLID,
        "color" => ["rgb" => "FFFFFF"]
]];

// Initial Variables
$examIDParam = filter_input(INPUT_POST, "exam_matrix");

if (is_null($examIDParam) || !$db->exams->doesExamExist($examIDParam)) {
   echo "Exam not found or has recently been deleted";
   return false;
}

// Get exam record
$examRecord = $db->exams->getExam($examIDParam);
$examSettings = $db->examSettings->get($examIDParam);
$candidatesEnabled = ($candidateFeatureEnabled && $examSettings["exam_student_id_type"] == CANDIDATE_NUMBER_TYPE);
$termID = $candidatesEnabled  ? $examRecord["term_id"] : null;

// Hide deprecated messages
error_reporting(E_ALL & ~E_DEPRECATED);

// Template reader
$excelReader = PHPExcel_IOFactory::createReader("Excel2007");

// Load template file
$excelObject = $excelReader->load("../files/export_templates/exam_matrix.xlsx");

// Get spread sheet
$spreadSheet = $excelObject->getSheet(0);

// Get list of exam sessions
$sessions = $db->exams->getExamSessions(
        $examIDParam, 
        RETURN_AS_ARRAY
);

// Title and data XF Index (Styling)
$titleXFIndex = $spreadSheet->getCell("B1")->getXfIndex();
$dataXFIndex = $spreadSheet->getCell("B3")->getXfIndex();

// Get unique station numbers
$stationNumbers = $db->exams->getUniqueStationNumbers($examIDParam);
$columnIndex = 6;
foreach($stationNumbers as $number) {
   
    // Station number
    $spreadSheet->setCellValueExplicitByColumnAndRow(
       $columnIndex,
       1,
       "Station " . $number, 
       PHPExcel_Cell_DataType::TYPE_STRING2
    );
    
    // Use title styling
    $spreadSheet->getCellByColumnAndRow($columnIndex, 1)->setXfIndex($titleXFIndex);    
    $columnIndex++;
    
}

// Loop through the exam sessions and apply to matrix
$rowIndex = 2;
foreach($sessions as $session) {
    
    $groups = $db->exams->getSessionGroups(
            $session["session_id"], 
            RETURN_AS_ARRAY
    );
    
    foreach($groups as $group) {
      
       // Group ID
       $groupID = $group["group_id"];
      
       // Group name
       $groupName = $group["group_name"];       
       
       // Colour Hexadecimal
       $sessionColour = strtoupper($session["circuit_colour"]);
       if (isset($colours[$sessionColour])) {
          $colourData = $colours[$sessionColour];
          $colourHex = $colourData["circuit_colour_light"];
       } else {
          $colourHex = "";
          foreach($colours as $colourData) {
           
             if (preg_match("/" . $colourData["colour_name"] . "/i", $groupName)) {
                $colourHex = $colourData["circuit_colour_light"];
                break;  
             }
           
          }
          
       }
       
       // Set fill with new colour
       if (strlen($colourHex) > 0) {
          $PHPExcelFill["fill"]["color"]["rgb"] = $colourHex;  
       }
       
       // Get examinee records
       $examinees = $db->exams->getStudentsAndBlanks($groupID, $termID);

       for($cycleNum=1; $cycleNum<=count($examinees); $cycleNum++) {
                   
            // Session Description
            $spreadSheet->setCellValueExplicitByColumnAndRow(
                0,
                $rowIndex,
                html_entity_decode($session["session_description"], ENT_QUOTES, "UTF-8"), 
                PHPExcel_Cell_DataType::TYPE_STRING2
            );
            
            // Session Date
            $spreadSheet->setCellValueExplicitByColumnAndRow(
                1,
                $rowIndex,
                formatDate($session["session_date"]), 
                PHPExcel_Cell_DataType::TYPE_STRING2
            );
            
            // Circuit Colour
            $colourName = isset($colours[$sessionColour]) ? $colours[$sessionColour]["colour_name"] : "";
            $spreadSheet->setCellValueExplicitByColumnAndRow(
                2,
                $rowIndex,
                $colourName, 
                PHPExcel_Cell_DataType::TYPE_STRING2
            );            
            
            // Group name cell
            $spreadSheet->setCellValueExplicitByColumnAndRow(
               3,
               $rowIndex,
               html_entity_decode($groupName, ENT_QUOTES, "UTF-8"), 
               PHPExcel_Cell_DataType::TYPE_STRING2
            );
            
            // Start time
            $spreadSheet->setCellValueExplicitByColumnAndRow(
               4,
               $rowIndex,
               $session["start_time"], 
               PHPExcel_Cell_DataType::TYPE_STRING2
            );

            // End time
            $spreadSheet->setCellValueExplicitByColumnAndRow(
               5,
               $rowIndex,
               $session["end_time"], 
               PHPExcel_Cell_DataType::TYPE_STRING2
            );
       
            // Apply styling to main data cells in current row
            for ($columnIndex=0; $columnIndex<=5; $columnIndex++) {
                 $spreadSheet->getCellByColumnAndRow($columnIndex, $rowIndex)->setXfIndex($dataXFIndex);
            }
                
             /**
             *  Add colour code
             */
            if (strlen($colourHex) > 0) {
               $coordinates = "A" . $rowIndex . ":" 
                            . PHPExcel_Cell::stringFromColumnIndex($columnIndex) . $rowIndex;
               $spreadSheet->getStyle($coordinates)->applyFromArray($PHPExcelFill);
            }
 
            // Add examinees (students) to list 
            foreach ($examinees as $examinee) {
               
               // Blank (aka ghost) student
               if ($examinee["type"]== BLANK_STUDENT) {
                  $output = "BLANK";
               // Regular student (student id or candidate number)
               } elseif ($candidatesEnabled && !empty($examinee["candidate_number"])) {
                  $output = $examinee["candidate_number"];
               } else {
                  $output = $examinee["student_id"];
               }
               
               $spreadSheet->setCellValueExplicitByColumnAndRow(
                   $columnIndex,
                   $rowIndex,
                   html_entity_decode($output, ENT_QUOTES, "UTF-8"),
                   PHPExcel_Cell_DataType::TYPE_STRING2
               );
               $spreadSheet->getCellByColumnAndRow($columnIndex, $rowIndex)->setXfIndex($dataXFIndex);
               
               /**
                *  Add colour code
                */
               if ($output != "BLANK" && strlen($colourHex) > 0) {
                   $coordinates = PHPExcel_Cell::stringFromColumnIndex($columnIndex) . $rowIndex;
                   $spreadSheet->getStyle($coordinates)->applyFromArray($PHPExcelFill);
               }
              
               $columnIndex++;
            }
            
            // Shift all examinees (students) by one position
            $lastExaminee = array_pop($examinees);
            array_unshift($examinees, $lastExaminee);

          $rowIndex++;
        }
       
    }
    
}

// Output File to Browser
$fileName = sprintf(
    "%s.xlsx",
    sanitizeFileName($examRecord["exam_name"])
);

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment;filename=\"$fileName\"");
header("Cache-Control: max-age=0");
header("Cache-Control: private");
header("Pragma: private");

// Write excel file
$excelWriter = PHPExcel_IOFactory::createWriter($excelObject, "Excel2007");
$excelWriter->save("php://output");
