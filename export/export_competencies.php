<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Examiners Summary Export
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Load template file
$excelObject = $template_reader->load($templateRoot . "student_competencies.xlsx");

// Get Sheet
$competencySheet = $excelObject->getSheet(0);
$competencySheet->setTitle(ucwords(gettext('Competency') . "Data"));

// Pull data from $_POST Summary Request
$competencies = unserialize(gzuncompress(base64_decode($_POST['competenciesdata'])));

// Num of rows
$recordSize = sizeof($competencies);

// loop through array of rows
for ($rowNum=0; $rowNum<$recordSize; $rowNum++) {
    
    // Loop through columns
    $columnNum = 0;
    foreach ($competencies[$rowNum] as $rowValue) {
        
        // Get value from array and strip tags
        $value = strip_Tags_Omis(adjustHTML($rowValue));

        // Set Cell Value
        if ($columnNum == 0) {
          $competencySheet->setCellValueExplicitByColumnAndRow(
                  $columnNum,
                  $rowNum+1,
                  $value,
                  PHPExcel_Cell_DataType::TYPE_STRING2
          );
        }  else {
          $competencySheet->setCellValueByColumnAndRow(
                  $columnNum,
                  $rowNum+1,
                  $value
          );
        }
        $columnNum++;
    }
}

// Add to master workbook
$masterWorkbook->addExternalSheet($competencySheet, ++$sheetIndex);
