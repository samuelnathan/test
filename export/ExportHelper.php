<?php 
 /**
  * Helper functions for exporting data
  * @copyright Qpercom Ltd. 2019
  */

 /* Structure item titles 
  * @param mixed[] $itemData     Item data
  * @param int[] $counts         Both item and indicator
  * @param int[] $position       Just need the column position in excel/csv spreadsheet
  * @return string[]             Title data
  */ 
  function structureItemTitles($itemData, &$counts, &$position)
  {
 
     $data = [];

     // Indicators (not scoring items)
     if (in_array($itemData['type'], ['radiotext', 'checkbox'])) {
 
           $data[1][$position['col']] = $itemData['text'];
           $data[2][$position['col']]  = "Indicator ". $counts['indicator'];       
           $counts['indicator']++;
 
     // Items both raw and/or weighted    
     } elseif ($itemData['form_weighted']) {
 
           $data[1][$position['col']] = $itemData['text'];
           $data[2][$position['col']] = "Item ". $counts['item'] . " Raw";

           $position['col']++;

           $data[1][$position['col']] = "";
           $data[2][$position['col']] = "Item ". $counts['item'] . " Weighted";
           $counts['item']++;
                
     // Regular raw scoring item without weightings      
     } else {
 
           $data[1][$position['col']] = $itemData['text'];
           $data[2][$position['col']] = "Item " . $counts['item'];
           $counts['item']++;

     }

     $position['col']++;
 
     return $data;

   }

 /* Populate item titles and values
  * @param int $sheetObject      Spreadsheet object
  */ 
 function populateItemCells($sheetObject, $titles)
 {

    foreach ($titles as $row => $rowData) {

        foreach ($rowData as $col => $title) {

            $sheetObject->setCellValueByColumnAndRow($col, $row, $title);

        }

    }

  }
  

 /* Structure item values
  * @param mixed[] $itemData     Item data
  * @param mixed[] $scoreData    Item score data
  * @param int $position         Row/Column position in excel/csv spreadsheet
  * @return string[]             Item values data
  */ 
  function structureItemValues($itemData, $scoreData, &$position)
  {
    
     // Score record ID
     $itemID = $itemData['item_id'];
     $data = [];
    
     $itemValue = isset($scoreData[$itemID]) ? $scoreData[$itemID]['option_value'] : "";
     $data[$position['row']][$position['col']] = $itemValue;
     $position['col']++;

     // add weighting values if any
     if ($itemData['form_weighted'] && !in_array($itemData['type'], ['radiotext', 'checkbox'])) {
        
         $weightedValue = isset($scoreData[$itemID]) ? $scoreData[$itemID]['option_weighted_value'] : "";
         $data[$position['row']][$position['col']] = $weightedValue;
         $position['col']++;

     }

     return [$itemValue,$data];

   }

 ?>