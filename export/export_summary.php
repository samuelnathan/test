<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Examiners Summary Export
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Load template file
$pexcel_obj = $template_reader->load($templateRoot . "summary.xlsx");

// Get Sheet
$summarySheet = $pexcel_obj->getSheet(0);

// Pull data from $_POST Summary Request
$summary = unserialize(gzuncompress(base64_decode($_POST['summarydata'])));

// Num of rows
$summary_size = sizeof($summary);

// Row Position
$rwPosition = 1;

// loop through array of rows
for ($k = 0; $k < $summary_size; $k++) {
    // Row Position
    if ($k == $summary_size - 1 || $k == $summary_size - 2) {
        $rwPosition++;
    }

    // Loop through columns
    for ($v = 0; $v < sizeof($summary[$k]); $v++) {
        // Get value from array and strip tags
        $value = strip_Tags_Omis(adjustHTML($summary[$k][$v]));

        // Set Cell Value
        $summarySheet->setCellValueByColumnAndRow($v, $rwPosition, $value);
    }
    $rwPosition++;
}

// Add Scenario Title
$summarySheet->setCellValueByColumnAndRow(0, 1, 'Scenario');

// Add to master workbook
$masterWorkbook->addExternalSheet($summarySheet, ++$sheetIndex);
