<?php
/**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @Export analysis parent processing file
  */

 define("_OMIS", 1);

 //5 Minutes Limit Script Time
 // (DW) 800 seconds = 13m20s :)
 set_time_limit(800);

 //Set Timezone
 date_default_timezone_set("Europe/London");
 error_reporting(E_ALL & ~E_DEPRECATED);

 include __DIR__ . "/../extra/essentials.php";

 //Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess())
     return false;

 //Turn off entity clean from DB
 $db->set_Convert_Html_Entities(false);

 $templateRoot = "../files/export_templates/";
 $templateConfigRoot = "../" . $config->borderline_templates_path . "/";

 /**
  * Custom Error handler whose job it is to elevate E_NOTICE errors into exceptions
  * so that we can catch them. This is to handle parts of the PHP codebase that
  * are still stuck in the pre-exception era.
  * @TODO (DW) Put this in a helper function file or something.
  *
  * @param int $errno
  * @param string $errstr
  * @param string $errfile
  * @param int $errline
  * @return mixed
  * @throws ErrorException
  */
 function noticeErrorHandler($errno, $errstr, $errfile, $errline)
 {
     if ($errno === E_NOTICE) {
         // For an E_NOTICE, elevate it to an exception we can catch()...
         throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
     } else {
         // For any other type of error, let PHP's own built-in error handling
         // deal with it.
         return \FALSE;
     }
 }

   // Define the filter options for filtering an int that's either 0 or 1 (let's call
   // it a "binary int"...
   $filter_options_binary_int = ["options" => ["min_range" => 0, "max_range" => 1]];


 if (filter_has_var(INPUT_POST, "export_overall")) {
     //Export Overall Analysis from Results Analysis Section, see out_analysis.php
     //Exam ID Request Variable
     $exam_id = filter_input(INPUT_POST, "exam_id", FILTER_SANITIZE_NUMBER_INT);
     if ($exam_id === false) {
         $exam_id = "";
     }

     //Validate POST Variables Check
     // List of POST params that must be set
     $postparams_required = [
         "groupsdata",
         "rawdata",
         "summarydata",
         "scenariodata",
         "stationdata"
     ];

     // List of POST params of which at least one must be set.
     $postparams_optional = [
         "raw_export",
         "summary_export",
         "detailed_export",
         "cdetailed_export",
         "examiners_export",
         "competencies_export"
     ];

     // Lambda function to determine if that param is set or not.
     $post_isset = function ($param) {
         $set = filter_input(INPUT_POST, $param, FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);

         return (!empty($set));
     };

     /* Get all required parameters that have in fact been set, and check that
      * it's the same list we started with...
      */
     $set_required = (array_filter($postparams_required, $post_isset) == $postparams_required);

     /* Ditto for the optional ones, but in this case we're just checking if
      * any have been set.
      */
     $optionalVariables = array_filter($postparams_optional, $post_isset);
     $set_optional = (!empty($optionalVariables));

     // The newly condensed version of the great big statement...
     if ($set_required && $set_optional && (filter_input(INPUT_POST, "raw_export") == 1)) {
         include_once "../analysis/analysis_definitions.php";

         try {
             //Exam Details
             $exam_details = $db->exams->getExam($exam_id);
             if (is_null($exam_details)) {
                 echo "exam does not exit, please close popup window and choose a different exam";
                 exit;
             }

             // Some Arrays
             $allStationsData = [];
             $allStudentValues = [];
             $allStudentBls = [];

             $sessionIDs = [];
             $groupData = [];
             $sessionData = [];
             $spreadsheetTitles = [];
             $studentIDs = [];
             $rawData = [];

             $scenarioItemTitles = [];
             $studentScenarioData = [];

             /* Lambda function to do the unpacking as it's a bit of a mouthful
              * to type.
              */
             $inflate_postparam = function ($param) {
                 $filteredParams = filter_input(INPUT_POST, $param, FILTER_DEFAULT, FILTER_NULL_ON_FAILURE);

                 if (!empty($filteredParams)) {
                     // Try to decode what we expect to be base64.
                     $compressed_binary_data = base64_decode($filteredParams);
                     if ($compressed_binary_data === \FALSE) {
                         error_log(__FILE__ . ": Export Overall: Compressed POST Params not in base64 format");

                         return "";
                     }

                     // The decoded binary data should be gzipped... try to decompress.
                     $uncompressed_binary_data = gzuncompress($compressed_binary_data);
                     if ($uncompressed_binary_data === \FALSE) {
                         error_log(__FILE__ . ": Export Overall: POST Params not valid gzipped data");

                         return "";
                     }

                     // Set a custom error handler
                     set_error_handler("noticeErrorHandler");

                     try {
                         $unserialized_data = unserialize($uncompressed_binary_data);
                     } catch (ErrorException $ex) {
                         // This will now catch the E_NOTICE that unserialize()
                         // unhelpfully throws when it's handed bad/invalid data
                         error_log(
                             __FILE__ . ": Export Overall: POST Params not "
                             . "valid serialized data: " . $ex->getMessage()
                         );
                         // Restore the original error handler. Can do a try...finally
                         // block once we migrate to PHP 5.5.
                         restore_error_handler();
                         return "";
                     }

                     // Restore the original error handler. Can do a try...finally
                     // block once we migrate to PHP 5.5.
                     restore_error_handler();

                     // If we get to here, then the data is valid and suitably
                     // unpacked and formatted.
                     return $unserialized_data;
                 } else {
                     return "";
                 }
             };

             // Groups
             $groups = $inflate_postparam("groupsdata");
             if (!is_array($groups)) {
                 $error_message = __FILE__ . ": POST parameter 'groupsdata' is empty or invalid";
                 error_log($error_message);
                 die($error_message);
             }

             // Station Data
             $stationDatas = $inflate_postparam("stationdata");
             if (!is_array($stationDatas)) {
                 $error_message = __FILE__ . ": POST parameter 'stationdata' is empty or invalid";
                 error_log($error_message);
                 die($error_message);
             }

             // Scenario Data
             $scenario_data = $inflate_postparam("scenariodata");
             if (!is_array($scenario_data)) {
                 $error_message = __FILE__ . ": POST parameter 'scenariodata' is empty or invalid";
                 error_log($error_message);
                 die($error_message);
             }

             // Multiple examiners count per station
             $multiExaminerCounts = $inflate_postparam("multi_examiner_counts");
             if (!is_array($multiExaminerCounts)) {
                 $error_message = __FILE__ . ": POST parameter 'multi_examiner_counts' is empty or invalid";
                 error_log($error_message);
                 die($error_message);
             }

             // Other Variables
             $cnm = (!isset($_POST["cnm"])) ? STATION_COMBINE_SCENARIO : $_POST["cnm"];
             $numberOfColumns = iS($_POST, "nrofcols");               // Number of Main Columns
             $additionalSubColumns = iS($_POST, "additionalsubcols"); // Additional Number of Sub Columns

             // Export Competencies Analysis
             $exportCompetencies = (int) filter_input(
                 INPUT_POST,
                 "competencies_export",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Export Summary Analysis
             $exportSummary = (int) filter_input(
                 INPUT_POST,
                 "summary_export",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Export Examiner Analysis
             $exportExaminers = (int) filter_input(
                 INPUT_POST,
                 "examiners_export",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Export Detailed Scenario Analysis
             $exportDetailed = (int) filter_input(
                 INPUT_POST,
                 "detailed_export",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Export Complete Detailed Scenario Analysis
             $exportDetailedComplete = (int) filter_input(
                 INPUT_POST,
                 "cdetailed_export",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display Student Names
             $displaystudentname = (int) filter_input(
                 INPUT_POST,
                 "displaystudentname",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display Group Names
             $displaygroups = (int) filter_input(
                 INPUT_POST,
                 "displaygroups",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display Student Order
             $displayorder = (int) filter_input(
                 INPUT_POST,
                 "displayorder",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display Session
             $displaysession = (int) filter_input(
                 INPUT_POST,
                 "displaysession",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display Nationality
             $displaynationality = (int) filter_input(
                 INPUT_POST,
                 "displaynationality",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display submission Timestamp
             $displaytimestamp = (int) filter_input(
                 INPUT_POST,
                 "displaytimestamp",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display grade
              $displaygrade = (int) filter_input(
                 INPUT_POST,
                 "displaygrade",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display percentages
             $displaypercent = (int) filter_input(
                 INPUT_POST,
                 "displaypercent",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Display weightings
             $displayweighted = (int) filter_input(
                 INPUT_POST,
                 "displayweighted",
                 FILTER_VALIDATE_INT,
                 $filter_options_binary_int
             );

             // Groups Information
             foreach ($groups as $group) {

                 $groupRecord = $db->groups->getGroup($group, false);
                 $groupData[$group] = [
                     "group_id" => $groupRecord["group_id"],
                     "session_id" => $groupRecord["session_id"],
                     "group_name" => $groupRecord["group_name"]
                 ];

                 if (!isset($sessionData[$groupRecord["session_id"]])) {

                     $sessionDB = $db->exams->getSession($groupRecord["session_id"]);
                     $sessionIDs[] = $groupRecord["session_id"];
                     $sessionData[$groupRecord["session_id"]] = [
                         "session_id" => $groupRecord["session_id"],
                         "session_date" => $sessionDB["session_date"],
                         "session_description" => $sessionDB["session_description"]
                     ];

                 }

             }

             //New Master Work Sheet
             $masterWorkbook = new PHPExcel();

             //Remove unneeded worksheet
             $masterWorkbook->removeSheetByIndex(0);

             //Template Reader
             $template_reader = PHPExcel_IOFactory::createReader("Excel2007");

             //Sheet Index
             $sheetIndex = 0;

             // Include Script Raw Data Export Processing
             include "export_rawdata.php";

             // Summary Export Processing
             if ($exportSummary)
                 include "export_summary.php";

             // Student Competencies Export Processing
             if ($exportCompetencies)
                 include "export_competencies.php";

             // Examiners Export Processing
             if ($exportExaminers)
                 include "export_examiners.php";

             //Detailed Export Processing
             if ($exportDetailed || $exportDetailedComplete) {

                 // If Exporting Detailed
                 if ($exportDetailed) {

                     //Prepare Detailed Titles
                     $detailedTitles = ["id" => "Identifier"];

                     if ($displaystudentname) {
                         $detailedTitles["surname"] = "Surname";
                         $detailedTitles["forenames"] = "Forenames";
                     }

                     if ($displaynationality)
                         $detailedTitles["nationality"] = "Nationality";

                     if ($displayorder)
                         $detailedTitles["order"] = "Order";

                     if ($displaygroups)
                         $detailedTitles["groups"] = "Group";

                     if ($displaysession)
                         $detailedTitles["session"] = "Circuit/Day";

                     if ($exportExaminers)
                         $detailedTitles["examiner"] = gettext("Examiner");

                     if ($displaytimestamp)
                         $detailedTitles["submitted"] = "Submitted";

                     $detailedTitles["total"] = "Total";

                     if ($displaypercent)
                         $detailedTitles["percent"] = "Total %";

                     if ($displayweighted)
                         $detailedTitles[] = "Weighting Total";

                 }

                 //Loop through detailed results scenarios
                 $scenarioCount = $stationCounter = 0;
                 $current_scenario_group = null;
                 foreach ($scenario_data as $formID => $scenario) {

                     if ($cnm == STATION_COMBINE_TAG) {
                         $scenario_group = $scenario["station_code"];
                     } else {
                         $scenario_group = $scenario["station_number"];
                     }

                     if ($scenarioCount == 0)
                         $current_scenario_group = $scenario_group;

                     if ($current_scenario_group != $scenario_group) {

                         $current_scenario_group = $scenario_group;
                         $stationCounter++;

                     }

                     include "export_detailed.php";
                     $scenarioCount++;

                 }

             }

             // Free up some memory
             $studentIDs = null;
             $spreadsheetTitles = null;
             $sessionData = null;
             $groupData = null;
             $sessionIDs = null;
             unset($studentIDs, $spreadsheetTitles, $sessionData, $groupData, $sessionIDs);

             //Detailed Comprehensive Export
             if ($exportDetailedComplete) {
                 include "export_detailed_complete.php";
             }

             //Free up some more memory
             $rawData = null;
             $scenarioItemTitles = null;
             $studentScenarioData = null;
             unset($rawData, $scenarioItemTitles, $studentScenarioData);

             //Run garabage collector
             gc_collect_cycles();

             //Output File To Browser
             $header_filename = sprintf(
                 "complete_analysis_%s.xlsx",
                 sanitizeFileName($exam_details["exam_name"])
             );
             header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
             header("Content-Disposition: attachment;filename=\"$header_filename\"");
             header("Cache-Control: max-age=0");
             header("Cache-Control: private");
             header("Pragma: private");

             // Complete export cookie : wait symbol and delete button
             header("Set-Cookie: cexp=" . filter_input(INPUT_POST, "cexp") . "; Path=/;");

             $excelWriter = PHPExcel_IOFactory::createWriter($masterWorkbook, "Excel2007");
             $excelWriter->setPreCalculateFormulas(false);
             $excelWriter->save("php://output");
         } catch (Exception $e) {
             trigger_error("Error loading file: " . $e->getMessage());
         }
     } else { //Invalid, return to results Analysis page
         header("Refresh: 3; url='../manage.php?page=out_analysis&tab_persist=results-export&o=" . $exam_id . "'");
         echo "Please choose at least one option to export analysis. redirecting in 3 seconds...";
         exit;
     }

     //Invalid, redirect to home page
 } else {
     header("Location: ../");
 }
