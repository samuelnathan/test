<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Export student groups
 */

define("_OMIS", 1);
define("RETURN_AS_ARRAY", true);
defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . "/../vendor/autoload.php";
}

// Include database connection
include __DIR__ . "/../extra/essentials.php";

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    echo "You do not have permissions to perform this operation, " .
         "please contact the system administrator for further assistance";
    return false;
}

// Initial Variables
$templateRoot = "../files/export_templates/";
$sessionID = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
$exportSubmit = filter_input(INPUT_POST, "export_student_groups", FILTER_SANITIZE_STRING);

if (is_null($exportSubmit) || !$db->exams->doesSessionExist($sessionID)) {
   echo "Session not found or has recently been deleted";
   exit;
}

// Get session record
$sessionRecord = $db->exams->getSession($sessionID);

// Get exam settings
$examSettings = $db->examSettings->get($sessionRecord["exam_id"]);
$examIDType = $examSettings["exam_student_id_type"];

// 5 Minutes Limit Script Time
set_time_limit(800);

// Set Timezone
date_default_timezone_set("Europe/London");

// Hide deprecated messages
error_reporting(E_ALL & ~E_DEPRECATED);

// Template reader
$templateReader = PHPExcel_IOFactory::createReader("Excel2007");

// Load template file
$excelObject = $templateReader->load($templateRoot . "student_groups.xlsx");

// Get spread sheet
$spreadSheet = $excelObject->getSheet(0);

// Get list of groups
$groups = $db->exams->getSessionGroups(
     $sessionID,
     RETURN_AS_ARRAY
);

$rowIndex = 2;
foreach ($groups as $group) {
    $groupID = $group["group_id"];
    $groupName = $group["group_name"];
    $examineeRecords = $db->exams->getStudentsAndBlanks(
            $groupID, 
            $sessionRecord["term_id"]
    );
    
    // Render examinees
    foreach ($examineeRecords as $examinee) {
    
       // Group name
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          0,
          $rowIndex,
          html_entity_decode($groupName, ENT_QUOTES, "UTF-8"), 
          PHPExcel_Cell_DataType::TYPE_STRING2
       );
        
       // Examinee order number
       $examineeOrder = $examinee["student_order"];
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          1,
          $rowIndex,
          $examineeOrder,
          PHPExcel_Cell_DataType::TYPE_NUMERIC
       );
       
       // Blank student type
       $examineeType = $examinee["type"];
       if ($examineeType == 1) {
           
          // Blank student label
          $spreadSheet->setCellValueExplicitByColumnAndRow(
               2,
               $rowIndex,
               "BLANK STUDENT",
               PHPExcel_Cell_DataType::TYPE_STRING2
          );
          
          $rowIndex++;
          continue;  
       }
       
       // Examinee identifier
       if ($examIDType == EXAM_NUMBER_TYPE) {
         $examineeID = $examinee["exam_number"];  
       } else if ($examIDType == CANDIDATE_NUMBER_TYPE) {
         $examineeID = $examinee["candidate_number"];    
       } else {
         $examineeID = $examinee["student_id"];
       }
       
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          2,
          $rowIndex,
          html_entity_decode($examineeID, ENT_QUOTES, "UTF-8"),
          PHPExcel_Cell_DataType::TYPE_STRING2
       );
       
       // Examinee surname
       $surname = $examinee["surname"];
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          3,
          $rowIndex,
          html_entity_decode($surname, ENT_QUOTES, "UTF-8"),
          PHPExcel_Cell_DataType::TYPE_STRING2
       );
       
       // Examinee forenames
       $forenames = $examinee["forename"];
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          4,
          $rowIndex,
          html_entity_decode($forenames, ENT_QUOTES, "UTF-8"),
          PHPExcel_Cell_DataType::TYPE_STRING2
       );

       // Examinee forenames
       $gender = $examinee["gender"];
       $spreadSheet->setCellValueExplicitByColumnAndRow(
          5,
          $rowIndex,
          html_entity_decode($gender, ENT_QUOTES, "UTF-8"),
          PHPExcel_Cell_DataType::TYPE_STRING2
       );
       
       $rowIndex++;
    }
}

// Output File to Browser
$sessionName = $sessionRecord["session_description"] . "-"
             . formatDate($sessionRecord["session_date"]);
$fileName = sprintf(
    "%s.xlsx",
    sanitizeFileName($sessionName)
);

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment;filename=\"$fileName\"");
header("Cache-Control: max-age=0");
header("Cache-Control: private");
header("Pragma: private");

$excelWriter = PHPExcel_IOFactory::createWriter($excelObject, "Excel2007");
$excelWriter->save("php://output");
