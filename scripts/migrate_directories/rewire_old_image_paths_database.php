<?php
/**
 * Updates the text field in the section_items table
 * to rewire the old image paths
 * share/ to storage/app/share
 * <img src="share/"/> to <img src="storage/app/share"/>
 */
 
 // Include what's needed to make everything possible
 define('_OMIS', 1);
 global $loader, $config;
 $loader = require_once '../../vendor/autoload.php';
 $config = new OMIS\Config();
 $db = \OMIS\Database\CoreDB::getInstance();
 $db->set_Convert_Html_Entities(false);
 $mysqlResult = $db->query("SELECT item_id, text FROM section_items");
 $items = \OMIS\Database\CoreDB::into_array($mysqlResult);
 
 // Rewire paths for all section_item description fields
 $count = $found = 0;
 foreach ($items as $item) {
    
    if (preg_match("(src=\"share\/)", $item['text'])) {
        
        $found++;
        echo "($found) Found Item (id: " . $item['item_id']. ") => " . $item['text'] . "\n";
        
        $rewiredString = preg_replace(
            "(src=\"share\/)",
            'src="storage/app/share/',
            $item['text']
        );
     
        $updateSql = 'UPDATE section_items SET text = "' . $db->clean($rewiredString) . '" ' .
                     'WHERE item_id = "' . $item['item_id'] . '"';

        if ($db->query($updateSql)) {
            echo "($found) Updated Item (id: " . $item['item_id']. ") => " . $rewiredString . "\n\n";
            $count++;
        }

    }
    
 }
 
 // Update summary
 echo count($items) . " item(s) found\n";
 echo "$found item(s) with image paths found\n";
 echo "$count item(s) updated with new paths\n\n";
 
 ?>