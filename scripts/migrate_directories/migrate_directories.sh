#!/bin/sh

# Select the root as working dir
cd ../../

# Create directories
mkdir -p storage/
mkdir -p storage/cache
mkdir -p storage/app

# Move the content of the current directories to the new ones
mv compilation_cache storage/cache/twig_compilation_cache
mv custom/ storage/custom
mv imagecaches storage/cache/student_images
mv logs storage/logs
mv logo storage/logo
mv share storage/app/share
mv tmp storage/app/tmp

# Fix permissions in the new directories
declare apache_user=$(lsof -i | grep :http | tr -s ' ' | cut -f3 -d" " | uniq | grep -v "root");
declare apache_usergroup=$(groups $apache_user | sed -e 's/ //g');

declare -a folders=('storage/cache' 'storage/app' 'storage/logs');

for folder in "${folders[@]}"; do
        if [ -d "$folder" ]; then
                chown -R $apache_usergroup $folder/;
                chmod -R 0744 $folder/;
        else
                echo "(!) Folder $folder doesn't exist. Are you in the right folder?"
        fi
done

# Rewire scoresheet item paths (runs rewire_image_paths.php script)
printf '\nRewiring existing item image (html) paths in the current database (share => storage/app/share):\n'
cd scripts/migrate_directories
php rewire_old_image_paths_database.php

# Show the next steps the user must perform
printf 'Next steps:\n';
echo ' * Manually rewire the paths in configuration.php in paths'
echo ' * Manually rewire the path of the logo folder in configuration.php in tcpdf_environment to storage/logo'
echo ' * Manually rewire the path of the students image cache in configuration.php in imagecache to storage/cache/student_images/students'
echo ' * Make sure the reference in the moxiemanager configuration is up to date with storage/app/share'
