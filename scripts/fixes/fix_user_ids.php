<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 16/12/2013
   2013 Qpercom Limited. All rights reserved */
 session_start();
$_SESSION['osce_inst'] = 'qp';
require_once 'operations/configuration.php';
$config = new OMIS\Config;
require_once'operations/oscesql_funcs.php';
$sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);
require_once'operations/usefulphpfunc.php';

set_time_limit(500);

$q1 = " ALTER TABLE  `user_preference` DROP FOREIGN KEY  `user_preference_ibfk_1`";
if ($sf->query($q1)) {
    echo "Dropped user_preference relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not drop user_preference relationship</span><br/><br/>";
}

$q2 = " ALTER TABLE  `examiner_training` DROP FOREIGN KEY  `examiner_training_ibfk_1`";
if ($sf->query($q2)) {
    echo "Dropped examiner_training relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not drop examiner_training relationship</span><br/><br/>";
}

$q3 = "ALTER TABLE  `examiner_dept_term` DROP FOREIGN KEY  `examiner_dept_term_ibfk_2`";
if ($sf->query($q3)) {
    echo "Dropped examiner_dept_term relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not drop examiner_dept_term relationship</span><br/><br/>";
}

$q4 = "ALTER TABLE  `session_assistant` DROP FOREIGN KEY  `session_assistant_ibfk_2`";
$q5 = "ALTER TABLE  `session_assistant` ADD FOREIGN KEY (  `oscetd_id` ) REFERENCES  `osce_session` (`oscetd_id`) ON DELETE CASCADE ON UPDATE RESTRICT";
$q6 = "ALTER TABLE  `session_assistant` DROP FOREIGN KEY  `session_assistant_ibfk_1`";
if ($sf->query($q4) && $sf->query($q5) && $sf->query($q6)) {
    echo "Dropped session_assistant relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not drop session_assistant relationship</span><br/><br/>";
}

$q7 = "ALTER TABLE  `user` CHANGE  `user_id`  `user_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q7)) {
    echo "Changed user 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change user 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q8 = "ALTER TABLE  `user_preference` CHANGE  `user_id`  `user_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q8)) {
    echo "Changed user_preference 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change user_preference 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q9 = "ALTER TABLE  `examiner_dept_term` CHANGE  `user_id`  `user_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q9)) {
    echo "Changed `examiner_dept_term` 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change `examiner_dept_term` 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q10 = "ALTER TABLE  `examiner_training` CHANGE  `user_id`  `user_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q10)) {
    echo "Changed `examiner_training` 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change `examiner_training` 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q11 = "ALTER TABLE  `station_instance` CHANGE  `examiner_id`  `examiner_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q11)) {
    echo "Changed `station_instance` 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change `station_instance` 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q12a = "ALTER TABLE  `session_assistant` CHANGE  `user_id`  `user_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q12a)) {
    echo "Changed `session_assistant` 'user_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change `session_assistant` 'user_id' collate to utf8_bin</span><br/><br/>";
}

$q12b = "ALTER TABLE  `user_preference` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (
          `user_id`) ON DELETE CASCADE ON UPDATE CASCADE";
if ($sf->query($q12b)) {
    echo "Added user_preference relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not add user_preference relationship</span><br/><br/>";
}

$q13 = "ALTER TABLE  `examiner_dept_term` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE";
if ($sf->query($q13)) {
    echo "Added examiner_dept_term relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not add examiner_dept_term relationship</span><br/><br/>";
}

$q15 = "ALTER TABLE  `examiner_training` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE";
if ($sf->query($q15)) {
    echo "Added examiner_training relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not add examiner_training relationship</span><br/><br/>";
}

$q16 = "ALTER TABLE  `session_assistant` DROP FOREIGN KEY  `session_assistant_ibfk_2`";
$q17 = "ALTER TABLE  `session_assistant` ADD FOREIGN KEY (  `oscetd_id` ) REFERENCES  `osce_session` (`oscetd_id`) ON DELETE CASCADE ON UPDATE RESTRICT";
$q18 = "ALTER TABLE  `session_assistant` ADD FOREIGN KEY (  `user_id` ) REFERENCES  `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE";

if ($sf->query($q16) && $sf->query($q17) && $sf->query($q18)) {
    echo "Added session_assistant relationship<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not add session_assistant relationship</span><br/><br/>";
}

$q19 = "ALTER TABLE  `sms_message_recipients` CHANGE  `recipient_id`  `recipient_id` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
if ($sf->query($q19)) {
    echo "Changed sms_message_recipients 'recipient_id' collate to utf8_bin<br/><br/>";
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not change sms_message_recipients 'recipient_id' collate to utf8_bin</span><br/><br/>";
}
