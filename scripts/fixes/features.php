<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 18/05/2012
   2012 Qpercom Limited. All rights reserved 
*/

 session_start();
 $_SESSION['osce_inst'] = 'qp';
 require_once 'operations/configuration.php';
 require_once 'operations/usefulphpfunc.php';
 $config = new OMIS\Config;
 require_once'operations/oscesql_funcs.php';
 $sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);
 
/**** 1.7.2 Updates *****/

#Add Feature Rows [Dummy Data]
$q = "INSERT IGNORE INTO `feature` (`feature_id`, `feature_desc`) VALUES ('bse', 'Borderline Analysis Station Export')";
if($sf->query($q)){
        echo "New Feature Values 'Borderline Analysis Station Export' Added<br/>";
} else{ echo "<span style = 'font-weight: bold; color: red'>New Feature 'Borderline Analysis Station Export' Values Not Added</span><br/>"; }

#Add Feature Levels Rows
$q = "INSERT IGNORE INTO `feature_levels` (`feature_id` ,`feature_level`, `level_enabled`) VALUES ('bse', 'super admin', 1), ('bse', 'admin', 0), ('bse', 'examiner admin', 0)";
if($sf->query($q)){
        echo "New Feature Levels 'Borderline Analysis Station Export' Rows Added<br/><br/>";
} else { echo "<span style = 'font-weight: bold; color: red'>New Feature Levels Rows NOT Added</span><br/><br/>"; }
 

 
#Add Feature Rows 'Student Feedback System' [Dummy Data]
$q = "INSERT IGNORE INTO `feature` (`feature_id`, `feature_desc`) VALUES ('sfs', 'Student Feedback System')";
if($sf->query($q)){
        echo "New Feature Values 'Student Feedback System' Added<br/>";
} else{ echo "<span style = 'font-weight: bold; color: red'>New Feature 'Student Feedback System' Values Not Added</span><br/>"; }

#Add Feature Levels Rows 'Student Feedback System'
$q = "INSERT IGNORE INTO `feature_levels` (`feature_id` ,`feature_level`, `level_enabled`) VALUES ('sfs', 'super admin', 1), ('sfs', 'admin', 0), ('sfs', 'examiner admin', 0)";
if($sf->query($q)){
        echo "New Feature Levels 'Student Feedback System' Rows Added<br/><br/>";
} else { echo "<span style = 'font-weight: bold; color: red'>New Feature Levels 'Student Feedback System' Rows NOT Added</span><br/><br/>"; }



#Add Feature Rows 'Compulsory Feedback Option' [Dummy Data]
$q = "INSERT IGNORE INTO `feature` (`feature_id`, `feature_desc`) VALUES ('fc', 'Compulsory Feedback Option')";
if($sf->query($q)){
        echo "New Feature Values 'Compulsory Feedback Option' Added<br/>";
} else{ echo "<span style = 'font-weight: bold; color: red'>New Feature 'Compulsory Feedback Option' Values Not Added</span><br/>"; }

#Add Feature Levels Rows 'Compulsory Feedback Option'
$q = "INSERT IGNORE INTO `feature_levels` (`feature_id` ,`feature_level`, `level_enabled`) VALUES ('fc', 'super admin', 1), ('fc', 'admin', 0), ('fc', 'examiner admin', 0)";
if($sf->query($q)){
        echo "New Feature Levels 'Compulsory Feedback Option' Rows Added<br/><br/>";
} else { echo "<span style = 'font-weight: bold; color: red'>New Feature Levels 'Compulsory Feedback Option' Rows NOT Added</span><br/><br/>"; }

