<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 16/12/2011
   2012 Qpercom Limited. All rights reserved */
 session_start();
 $_SESSION['osce_inst'] = 'qp';
 require_once 'operations/configuration.php';
 $config = new OMIS\Config;
 require_once'operations/oscesql_funcs.php';
 $sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);
 require_once'operations/usefulphpfunc.php';
   
#Students 
$students = $sf->get_Students();
  $count = 1;
  while($row = mysqli_fetch_object($students)){
		$new_student_id = strrev($row->student_id);
		$email = "forename".$count.".surname".$count."@univ.com";
		$sf->query("UPDATE student SET student_id = '".$new_student_id."', forename = 'Fname$count', surname = 'surname$count', email = '$email' WHERE student_id = '".$row->student_id."'");
		$count++;
  }

  
#Examiners
$users =  $sf->get_All_Users();
$count = 1;
while($row = mysqli_fetch_object($users)) {
     $user_id = $row->user_id;
     $email = "forename".$count.".surname".$count."@univ.com";
	 $sf->query("UPDATE user SET forename = 'Fname$count', surname = 'surname$count', email = '$email' WHERE user_id = '$user_id'");
	  
	 $count++;
  
}

?>