<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 16/12/2012
   2012 Qpercom Limited. All rights reserved 
   
   Transfer OSCE FROM 1 Database to another.
   */
session_start();
$_SESSION['osce_inst'] = 'qp';
require_once 'operations/configuration.php';
$config = new OMIS\Config;
require_once'operations/oscesql_funcs.php';
$sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);
require_once'operations/usefulphpfunc.php';

set_time_limit(500);

$updated_count = 0;

#Sessions
$osces = $sf->query("SELECT * FROM `osce` WHERE  term_id = '2011-12'");
while ($osce = mysqli_fetch_assoc($osces)) {
    $tds = $sf->get_Tds_By_Osce($osce['osce_id']);

    #Sessions
    while ($td = mysqli_fetch_assoc($tds)) {
        $stations = $sf->query("SELECT station_id, oscetd_id, stationset.station_id, station_number, station_order FROM stationset INNER JOIN station USING(station_id) WHERE oscetd_id = '" . $td['oscetd_id'] . "' GROUP BY stationset.station_id ORDER BY station_number, station_id");
        $station_order = 1;
        #Stations
        while ($station = mysqli_fetch_assoc($stations)) {
            if ($sf->query("UPDATE stationset SET station_order = '" . $station_order . "' WHERE station_id = '" . $station['station_id'] . "'")) {
                $updated_count++;
            }
            $station_order++;
        }
    }
}