Student Record Purge Script
===========================

Why is this here?
-----------------
This script is intended to fix situations where bugs or other issues have caused multiple records to be created for each student/examiner/station combination to remove the unnecessary ones. 

(This is required because the examination tool will complain that there are "too many records" and refuse to examine a student to whom this has occurred)

What it does
------------
The script looks in three tables:

 * `student_results`
 * `item_scores`
 * `section_feedback`
 
...for invalid data. First, it checks the `student_records` table for any station/examiner/student combinations that have more than one attached record. If it finds any cases of same, it examines the item scores and feedback tables to see if they have data in those tables. It also checks if the results are marked absent and/or incomplete.

Usage & Sample output
-------------

    $ php fix_purge_studentrecords.php
    Station 299 / Examiner 'examiner4' / Student '67600011'
    =====================================================================
    2 Result records found, newest first:
     1. [--SF] #15006, (Score 55.00) last updated 2014-06-16 13:33:42
     2. [A---] #14999, (Score 0.00) last updated 2014-06-16 13:22:11
    
    [A: Absent, I:Incomplete: S:Has Item Scores: F:Has Section Feedback]
    Select the index of the record you wish to KEEP and press return (or press return to skip):2
    Deleting Result #15006
    -> Executing: DELETE FROM item_scores WHERE (result_id = 15006)
    -> Executing: DELETE FROM section_feedback WHERE (result_id = 15006)
    -> Executing: DELETE FROM student_results WHERE (result_id = 15006)
    
    Done.
    
In this example, examiner "examiner4" at station #299 has logged two results for student 67600011. Of these, the first is a complete score (because the "(A)bsent" and "(I)ncomplete" flags aren't set, but the "Has Item (S)cores" and "Has Section (F)eedback" are) and the second is where the student was marked as (A)bsent. To reiterate, the flags are (in order of appearance):

 * **A**bsent: Student was marked absent by examiner
 * **I**ncomplete: Record was not submitted by examiner - either browser window was closed prematurely or
the record is an autosave.
 * Has Item **S**cores: As well as the bare student record, there are individual competence item records stored.
 * Has Section **F**eedback: As per above, feedback has been entered for one or more section in the assessment form.

If any of these flags are not applicable to the current record, they're replaced with a '-'.

To delete a record, simply type the number of the item you wish to *keep*, and the script will delete the others. In the above example, we've selected to keep the absent record, and delete the others. It then shows its progress in deleting the other records. If for some reason you don't want to delete any of the records in this conflict set right now, just press Enter to move to the next set.

This process will be repeated until all conflicting results sets have been processed (script reports "Done").