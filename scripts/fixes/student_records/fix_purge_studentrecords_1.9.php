#!/usr/bin/env php
<?php
/* Make sure that there is only one result per student/examiner/station triplet. If there
 * are more than that, we need to purge the old ones.
 */

use \OMIS\Database\CoreDB as CoreDB; 
 
define('_OMIS', 1);
include_once '../../../vendor/autoload.php';

if (php_sapi_name() !== PHP_SAPI) {
    die("This script should only be run from the command line!");
}

/**
 * Ask a question on the console and return the response.
 * 
 * @param string $question Text of the question.
 * @return string Supplied response.
 */
function ask($question) {
    echo $question;
    $handle = fopen("php://stdin", "r");
    $line = fgets($handle);
    fclose($handle);
    return trim($line);
}

/**
 * Get the number of Item Scores attached to a given student result.
 * 
 * @global \OMIS\Database\CoreDB $db        Database instance.
 * @param int                    $result_id ID of result being queried.
 * @return int Number of item scores found.
 */
function getItemScoreCount($result_id) {
    global $db;
    $sql = "SELECT COUNT(score_id) FROM item_scores WHERE (result_id = $result_id)";

    $result = $db->query($sql);
    // This will return a zero if the query itself fails, so that's fine.
    return (int) $db->single_result($result);
}

/**
 * Get the number of Section Feedback items attached to a given student result.
 * 
 * @global \OMIS\Database\CoreDB $db        Database instance.
 * @param int                    $result_id ID of result being queried.
 * @return int Number of item scores found.l
 */
function getSectionFeedbackCount($result_id) {
    global $db;
    $sql = "SELECT COUNT(section_id) FROM section_feedback WHERE (result_id = $result_id)";

    $result = $db->query($sql);
    if ($result === false) {
        return 0;
    } else {
        return (int) $db->single_result($result);
    }
}

function executeTransactionStatement($sql) {
    global $db;
    $link = $db->getConnection();
    echo "-> Executing: $sql\n";
    if ($db->query($sql) == false) {
        echo "! Failed. Attempting to roll back...";
        // Roll back.
        if (mysqli_rollback($link) === false) {
            die("failed.\n");
        } else {
            echo "succeeded.\n";
            return false;
        }
    }

    // If we get to here, then we succeeded.
    return true;
}

/**
 * Delete a student result record from the database.
 * 
 * @global \OMIS\Database\CoreDB $db
 * @param type $result_id
 * @return type
 */
function deleteResult($result_id) {
    global $db;

    // Need this for the transaction stuff.
    $link = $db->getConnection();

    // Disable autocommits in mysqli, equivalent of beginning a transaction.
    mysqli_autocommit($link, false);

    // First, try to delete the item scores if there are any.
    if (getItemScoreCount($result_id) > 0) {
        $sql = "DELETE FROM item_scores WHERE (result_id = $result_id)";
        if (!executeTransactionStatement($sql)) {
            return false;
        }
    }

    // Next, try to delete the section feedback if there is any.
    if (getSectionFeedbackCount($result_id) > 0) {
        $sql = "DELETE FROM section_feedback WHERE (result_id = $result_id)";
        if (!executeTransactionStatement($sql)) {
            return false;
        }
    }

    // Last thing is to delete the actual record itself.
    $sql = "DELETE FROM student_results WHERE (result_id = $result_id)";
    $result = executeTransactionStatement($sql);
    if ($result) {
        // If we get to here, attempt to commit the entire block. If it fails, whinge.
        if (mysqli_commit($link) === false) {
            die("Failed to rollback changes for result $result_id\n");
        }
    }

    mysqli_autocommit($link, true);
    return $result;
}

echo "Connecting to database...";
$db = \OMIS\Database\CoreDB::getInstance();
echo "done.\n";

// SQL to check for incorrect additional records in the database...
$check_sql = "SELECT student_id, examiner_id, station_id, count(*) AS total FROM student_results "
    . "GROUP BY student_id, examiner_id, station_id";

$results = $db->query($check_sql);

if ($results === false) {
    die("Error getting results from database");
}

/* Extract the relevant results from the overall list - these are those examiner/student
 * /station pairings that have more than one result per record.
 */
$relevant_results = array_filter(
    CoreDB::into_array($results), function ($item) {
        // Only return items whose total is greater than one, i.e. has extra records.
        return ($item['total'] > 1);
    }
);

if (empty($relevant_results)) {
    echo "No conflict sets found.\n";
    exit(0);
}

// Iterate through the results and offer the user the option of which record to keep.
foreach ($relevant_results as $result) {
    list($student_id, $examiner_id, $station_id, $count) = array_values($result);
    $record_query = "SELECT result_id, updated_at, absent, is_complete, score FROM student_results WHERE student_id = "
            . "'$student_id' and examiner_id='$examiner_id' and station_id=$station_id ORDER BY "
            . "updated_at DESC";
    $record_result = $db->query($record_query);
    if ($record_result === false) {
        die("Failed getting record list for Station $station_id, Examiner $examiner_id, Student $student_id\n");
    }

    $records = CoreDB::into_array($record_result);

    echo "Station $station_id / Examiner '$examiner_id' / Student '$student_id'\n";
    echo "=====================================================================\n";
    echo "$count Result records found, newest first:\n";
    $record_index = 0;
    foreach ($records as $record) {
        $record_index++;
        $flags = [];

        // Check if student was absent
        if ($record['absent'] == 1) {
            $flags[] = "A";
        } else {
            $flags[] = "-";
        }

        // Check if item is complete
        if ($record['is_complete'] == 0) {
            $flags[] = "I";
        } else {
            $flags[] = "-";
        }

        // Check if there are item scores for this record.
        $item_score_count = getItemScoreCount($record['result_id']);
        if ($item_score_count == 0) {
            $flags[] = "-";
        } else {
            $flags[] = "S";
        }

        // Check if there's section feedback for this record.
        $section_feedback_count = getSectionFeedbackCount($record['result_id']);
        if ($section_feedback_count == 0) {
            $flags[] = "-";
        } else {
            $flags[] = "F";
        }

        printf(" %d. [%s] #%d, (Score %.2f) last updated %s\n", $record_index, implode('', $flags), $record['result_id'], $record['score'], $record['updated_at']
        );
    }
    echo "\n[A: Absent, I:Incomplete: S:Has Item Scores: F:Has Section Feedback]\n";

    $valid_choice = false;
    while (!$valid_choice) {
        $choice = ask("Select the index of the record you wish to KEEP and press return (or press return to skip):");

        if ($choice == '') {
            continue 2;
        }

        $choice_value = intval($choice);
        if (($choice_value >= 1) && ($choice_value <= count($records))) {
            $valid_choice = true;

            $result_index = 0;
            foreach (array_column($records, 'result_id') as $result_id) {
                $result_index++;    // Keeps things 1-based for simplicity of reference.
                // If we find the record we wish to keep, skip it and go to the next one.
                if ($result_index == $choice_value) {
                    continue;
                }

                echo "Deleting Result #$result_id\n";
                deleteResult($result_id);
            }
        } else {
            echo "! Invalid selection: Valid choices are 1-" . count($records) . ".\n\n";
        }

        echo "\n";
    }
}

echo "Done.\n";
