<?php 
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Recalculate student results
 */

 // Include what's needed to make everything possible
 define('_OMIS', 1);
 global $loader, $config;
 $loader = require_once 'vendor/autoload.php';
 $config = new OMIS\Config();
 $db = \OMIS\Database\CoreDB::getInstance();
 $db->set_Convert_Html_Entities(false);

 // Recalc student results (NOTE: RADIO OPTIONS ONLY)
 /*
 $mysqlResult = $db->query("SELECT * FROM student_results WHERE station_id IN (382)");
 $results = \OMIS\Database\CoreDB::into_array($mysqlResult);

  foreach ($results as $result) {

    if ($db->results->recalcStudentResult($result['result_id'])) {

       $updatedRecord = $db->results->getResultByID($result['result_id']); 
       $station = $db->exams->getStation($result['station_id']);  

       echo "Student " . $result['student_id']
          . " score updated from " . $result['score'] . " to " 
          . $updatedRecord['score'] . " at station " 
          . $station['station_number'] . " - " . $station['form_name'] . "\n";
        
    }

 } */

 // Remap item score values (NOTE: RADIO OPTIONS ONLY)
 $mysqlResult = $db->query("SELECT *
 FROM item_scores
 INNER JOIN item_options USING(option_id)
 INNER JOIN section_items ON (section_items.item_id = item_options.item_id)
 WHERE option_value != option_value AND type = 'radio'
 AND result_id IN (SELECT result_id FROM student_results INNER JOIN session_stations USING (station_id) WHERE form_id = '1447')
 GROUP BY score_id");

 $itemScores = \OMIS\Database\CoreDB::into_array($mysqlResult);

 foreach ($itemScores as $score) {

      $db->query(
        "UPDATE item_scores SET option_value = '" 
        . $score['option_value'] 
        . "' WHERE score_id = '" 
        . $score['score_id'] . "'"
      );

      echo "score item " . $score['score_id'] . " updated\n";

 }
  
 ?>