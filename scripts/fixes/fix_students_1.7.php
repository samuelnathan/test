<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 16/12/2011
   2012 Qpercom Limited. All rights reserved */
 session_start();
 $_SESSION['osce_inst'] = 'qp';
 require_once 'operations/configuration.php';
 $config = new OMIS\Config;
 require_once'operations/oscesql_funcs.php';
 $sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);
 require_once'operations/usefulphpfunc.php';
 
#Clean Student Table
$res = $sf->get_Students();
while($r = mysqli_fetch_assoc($res)){
      $fn = mb_convert_case($r['forename'], MB_CASE_TITLE, 'UTF-8');
	  $sn = mb_convert_case($r['surname'], MB_CASE_TITLE, 'UTF-8');
  $sf->query("UPDATE student SET forename = '".$fn."', surname = '".$sn."' WHERE student_id = '".$r['student_id']."'");
}


?>