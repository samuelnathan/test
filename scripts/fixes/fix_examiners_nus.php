<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 04/04/2013
   2013 Qpercom Limited. All rights reserved 
*/

session_start();
$_SESSION['osce_inst'] = 'qp';
require_once 'operations/configuration.php';
require_once 'operations/usefulphpfunc.php';
$config = new OMIS\Config;
require_once'operations/oscesql_funcs.php';
$sf = oscesql_funcs::getInst(
    $config->db_host,
    $config->db_user,
    $config->db_pass,
    $config->db_name,
    $config->prod_mode
);


$training_q = "SELECT DISTINCT user_id " .
    "FROM user " .
    "WHERE (level = '2') AND user_id NOT IN (SELECT user_id FROM examiner_dept_term WHERE term_id = '2013-14')";
$examiners = $sf->query($training_q);

#Loop through examiners
while ($examiner = mysqli_fetch_assoc($examiners)) {

    if($sf->query("INSERT INTO examiner_dept_term (user_id, dept_id, term_id) VALUES ('" . $examiner['user_id'] . "', 'Examiner Training', '2013-14')")){
	  echo 'Insert<br/>';
	}
}

