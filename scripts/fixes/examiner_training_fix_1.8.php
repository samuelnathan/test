<?
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 14/05/2013
   2013 Qpercom Limited. All rights reserved #
   Examiner Training fix 1.8
*/

session_start();
$_SESSION['osce_inst'] = 'qp';
require_once 'operations/configuration.php';
require_once 'operations/usefulphpfunc.php';
$config = new OMIS\Config;
require_once'operations/oscesql_funcs.php';
$sf = oscesql_funcs::getInst($config->db_host, $config->db_user, $config->db_pass, $config->db_name, $config->prod_mode);

$fixed = false;
 
#Remove foreign key relationship 
$training_q1 = "ALTER TABLE  `examiner_training` DROP FOREIGN KEY  `examiner_training_ibfk_1`";

if ($sf->query($training_q1)) {
    echo "Removed relationship with 'user' table from 'examiner_training' table<br/><br/>";
    $fixed = true;
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not remove relationship with 'user' table from 'examiner_training' table</span><br/><br/>";
}

#Put relationship back
$q_training2 = "ALTER TABLE examiner_training ADD FOREIGN KEY ( user_id ) REFERENCES user (user_id) ON DELETE CASCADE ON UPDATE CASCADE";

if ($sf->query($q_training2)) {
    echo "Altered table 'examiner_training', added relationship to 'user' table<br/><br/>";
    $fixed = true;
} else {
    echo "<span style = 'font-weight: bold; color: red'>Did not alter table 'examiner_training' to add relationship to 'user' table</span><br/><br/>";
}

#Is fixed
if ($fixed) {
    echo "Table 'examiner_training' Fixed <br/><br/>";
}
