-- Author: Enda Phelan
-- Date: 18/11/2016
-- OMIS database upgrade script version 1.9.85 => 1.9.86
-- Add a flag to to exam_settings that will allow examiners to hide form notes for good.

ALTER TABLE `exam_settings` 
ADD COLUMN `allow_notes_override` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Allows examiners to hide form notes for good.' AFTER `preserve_group_order`;

-- Log that the current version of the database is 86, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (86);