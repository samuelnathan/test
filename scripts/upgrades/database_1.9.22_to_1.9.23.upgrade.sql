-- Fixes for OMIS issue #130 (examiner not allowed select examination in
-- assessment tool)
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   22/05/2014
-- Omis database upgrade script version 1.9.22 => 1.9.23

-- Replacing permissions settings for ajx_exams [5] and ajx_assessment [22]
DELETE FROM `page_levels` WHERE `page_id` IN (5, 22);

INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (5, 1, 1), (5, 2, 0), (5, 3, 1), (5, 4, 0), (5, 5, 1), (5, 6, 1);
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (22, 1, 1), (22, 2, 1), (22, 3, 1), (22, 4, 0), (22, 5, 0), (22, 6, 1);

-- Log that the current version of the database is 23, and it was added now.
INSERT INTO `database` (`version`) VALUES (23);
