-- Author: David Cunningham
-- Date: 28/06/2016
-- OMIS database upgrade script version 1.9.73 => 1.9.74
-- Adds new competency framework, 4 Core Domains to OMIS

-- Communication had 18 learning outcomes (14 of the 18 needed to be achieve to pass that virtual station)
-- Risk had 8 learning outcomes (6 of the 8 needed to be achieve to pass that virtual station)
-- Prescribing had 7 learning outcomes (5 of the 7 needed to be achieve to pass that virtual station)
-- Physical Health had 11 learning outcomes (8 of the 11 needed to be achieve to pass that virtual station)

-- Add new framework
INSERT INTO `competency_frameworks` (`id`, `name`) VALUES ('3', 'College of Psychiatrists Core Domains');

-- Add competencies
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('42', '3', 'College of Psychiatrists Core Domains', 'College of Psychiatrists Core Domains');

INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('43', '3', 'Communication', 'Communication');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('44', '3', 'Risk', 'Risk');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('45', '3', 'Prescribing', 'Prescribing');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('46', '3', 'Physical Health', 'Physical Health');

INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('47', '3', 'Not applicable', 'Not applicable');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('48', '3', 'Not applicable', 'Not applicable');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('49', '3', 'Not applicable', 'Not applicable');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('50', '3', 'Not applicable', 'Not applicable');

INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('42', '42', '0');

INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('43', '43', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('44', '44', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('45', '45', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('46', '46', '0');

INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('47', '47', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('48', '48', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('49', '49', '0');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('50', '50', '0');

INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('42', '43', '1');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('42', '44', '1');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('42', '45', '1');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('42', '46', '1');

INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('43', '47', '2');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('44', '48', '2');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('45', '49', '2');
INSERT INTO `framework_level_treepaths` (`ancestor`, `descendant`, `tree_length`) VALUES ('46', '50', '2');

-- Adds new column to competency_framework_levels table for competency colour coding
ALTER TABLE `competency_framework_levels`
ADD COLUMN `level_colour_code` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `description`;

-- Standard Framework (ID:1)
UPDATE `competency_framework_levels` SET `level_colour_code`='' WHERE `id`='1' and`framework_id`='1';
UPDATE `competency_framework_levels` SET `level_colour_code`='#fa8072' WHERE `id`='3' and`framework_id`='1';
UPDATE `competency_framework_levels` SET `level_colour_code`='#90ee90' WHERE `id`='4' and`framework_id`='1';
UPDATE `competency_framework_levels` SET `level_colour_code`='#ffa500' WHERE `id`='5' and`framework_id`='1';
UPDATE `competency_framework_levels` SET `level_colour_code`='#00c0ff' WHERE `id`='6' and`framework_id`='1';

-- CanMeds2015 Framework (ID:2)
UPDATE `competency_framework_levels` SET `level_colour_code`='#fa8072' WHERE `id`='7' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#90ee90' WHERE `id`='8' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#ffa500' WHERE `id`='9' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#00c0ff' WHERE `id`='10' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#ffC0cb' WHERE `id`='11' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#d799fe' WHERE `id`='12' and`framework_id`='2';
UPDATE `competency_framework_levels` SET `level_colour_code`='#c0c0c0' WHERE `id`='13' and`framework_id`='2';

-- College of Psychiatrists Core Domains/Framework (ID:3)
UPDATE `competency_framework_levels` SET `level_colour_code`='#fa8072' WHERE `id`='43' and`framework_id`='3';
UPDATE `competency_framework_levels` SET `level_colour_code`='#90ee90' WHERE `id`='44' and`framework_id`='3';
UPDATE `competency_framework_levels` SET `level_colour_code`='#ffa500' WHERE `id`='45' and`framework_id`='3';
UPDATE `competency_framework_levels` SET `level_colour_code`='#00c0ff' WHERE `id`='46' and`framework_id`='3';

-- Log that the current version of the database is 74, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (74);
