-- Author: David Cunningham
-- Date: 19/01/2018
-- Version 1.9.110 => 1.9.111

-- Remove old tables from previous RFC sprint
DROP TABLE IF EXISTS self_assessment_items;
DROP TABLE IF EXISTS self_assessments;

CREATE TABLE self_assessments
(
    self_assessment_id BIGINT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    exam_id BIGINT(10) NOT NULL,
    title VARCHAR(45),
    max_score INT(11),
    CONSTRAINT self_assessments_exams_exam_id_fk FOREIGN KEY (exam_id) REFERENCES exams (exam_id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX self_assessments_exam_id_uindex ON self_assessments (exam_id);

CREATE TABLE self_assessment_questions
(
    question_id BIGINT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    self_assessment_id BIGINT(10) NOT NULL,
    question TEXT NOT NULL,
    question_ord INT NOT NULL,
    CONSTRAINT self_assessment_questions_self_assessments_self_assessment_id_fk FOREIGN KEY (self_assessment_id) REFERENCES self_assessments (self_assessment_id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX self_assessment_questions_question_id_question_ord_uindex ON self_assessment_questions (question_id, question_ord);

CREATE TABLE self_assessment_responses
(
    response_id BIGINT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    question_id BIGINT(10) NOT NULL,
    student_id VARCHAR(30) NOT NULL,
    score INT NOT NULL,
    response TEXT,
    CONSTRAINT responses_questions_question_id_fk FOREIGN KEY (question_id) REFERENCES self_assessment_questions (question_id) ON DELETE CASCADE,
    CONSTRAINT responses_students_stu_id_fk FOREIGN KEY (student_id) REFERENCES students (stu_id) ON DELETE CASCADE ON UPDATE CASCADE 
);
CREATE UNIQUE INDEX self_assessment_responses_question_id_student_id_uindex ON self_assessment_responses (question_id, student_id);

CREATE TABLE self_assessment_ownerships
(
    examiner_id VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    student_id VARCHAR(30) NOT NULL,
    self_assessment_id BIGINT(10) NOT NULL,
    CONSTRAINT self_assessment_ownership_self_assessment_id_student_id_pk PRIMARY KEY (self_assessment_id, student_id),
    CONSTRAINT self_assessment_ownership_users_user_id_fk FOREIGN KEY (examiner_id) REFERENCES users (user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT self_assessment_ownership_self_assessments_self_assessment_id_fk FOREIGN KEY (self_assessment_id) REFERENCES self_assessments (self_assessment_id) ON DELETE CASCADE,
    CONSTRAINT self_assessment_ownership_students_stu_id_fk FOREIGN KEY (student_id) REFERENCES students (stu_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Add self assessment flag field to scoresheet section
ALTER TABLE `form_sections` 
ADD COLUMN `self_assessment` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `feedback_entry`;

-- Add new self assessment results and analysis script
INSERT IGNORE INTO `pages` (`page_name`) VALUES ('self_assessments');

INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,1,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,2,0);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,3,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,4,0);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,5,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,6,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (210,7,1);

-- Adds new import section page 'json_department_exams_selfassessment'
INSERT IGNORE INTO `pages` (`page_name`) VALUES ('json_department_exams_selfassessment');

INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,1,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,2,0);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,3,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,4,0);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,5,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,6,1);
INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (211,7,1);

-- Allow all exam admins and station managers to have access to the manage student pages but depends
-- on feature being enabled/disabled from the manage features section
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='81' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='81' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='82' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='82' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='83' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='83' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='12' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='12' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='13' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='13' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='14' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='14' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='25' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='25' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='88' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='88' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='16' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='16' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='84' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='84' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='72' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='72' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='73' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='73' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='74' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='74' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='75' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='75' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='15' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='15' and`page_level`='5';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='174' and`page_level`='3';
UPDATE `page_levels` SET `level_enabled`=1 WHERE `page_id`='174' and`page_level`='5';

-- Log that the current version of the database is 110, and it was added now.
INSERT IGNORE INTO `database_versions` (`version`) VALUES (111);
