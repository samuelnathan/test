-- Issue #64 'Redesign student course and module relationship in DB'
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   03/07/2015
-- Omis database upgrade script version 1.9.45 => 1.9.46

-- Rename 'student_modules' table to 'student_courses'
ALTER TABLE `student_modules` 
RENAME TO  `student_courses`;

-- Drop 'student_years' table
DROP TABLE `student_years`;

-- Rename examinee (student) section pages
UPDATE `pages` SET `page_name`='json_examinees' WHERE `page_id`='76';
UPDATE `pages` SET `page_name`='manage_examinees' WHERE `page_id`='85';
UPDATE `pages` SET `page_name`='update_examinees' WHERE `page_id`='151';
UPDATE `pages` SET `page_name`='examinees_uploadimages' WHERE `page_id`='164';
UPDATE `pages` SET `page_name`='examinees_processimages' WHERE `page_id`='165';

-- Log that the current version of the database is 46, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (46);


