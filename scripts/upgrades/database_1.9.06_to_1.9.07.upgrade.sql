-- Page naming error - 'ajx_support_lu' should be 'ajx_support_be'
-- (We don't have any clients in Luxembourg... yet!)
-- Also, pages for adding 
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   12/03/2014
-- Omis database upgrade script version 1.9.6 => 1.9.7


-- Turn off safe mode
SET SQL_SAFE_UPDATES = 0;

UPDATE `pages` SET `page_name` = 'ajx_support_be' WHERE `page_name` = 'ajx_support_lu';

-- New pages for the assignment of examiners to stations and their permissions.
INSERT INTO `pages` (`page_name`) VALUES ('examiner_osce');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (166, 1, 1), (166, 2, 0), (166, 3, 1), (166, 4, 0), (166, 5, 1), (166, 6, 1);

INSERT INTO `pages` (`page_name`) VALUES ('ajx_exmrs');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (167, 1, 1), (167, 2, 0), (167, 3, 1), (167, 4, 0), (167, 5, 1), (167, 6, 1);

-- Log that the current version of the database is 7, and it was added now.
INSERT INTO `database` (`version`) VALUES (7);

-- Turn on safe mode
SET SQL_SAFE_UPDATES = 1;

