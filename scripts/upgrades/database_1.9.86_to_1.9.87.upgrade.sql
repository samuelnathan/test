-- Author: Enda Phelan
-- Date: 18/11/2016
-- OMIS database upgrade script version 1.9.86 => 1.9.87
-- Add a flag to to exam_rules that, if set, requires a minimum number of stations that must be passed in order to pass the exam.

ALTER TABLE `exam_rules` 
ADD COLUMN `min_stations_to_pass` TINYINT(1) UNSIGNED NULL DEFAULT NULL COMMENT 'Minimum number of stations that must be passed in order to pass the exam' AFTER `max_grs_fail`;

-- Log that the current version of the database is 87, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (87);
