-- Author: Domhnall Walsh
-- Date:   02/03/2016
-- OMIS database upgrade script version 1.9.67 => 1.9.68
-- Adds unique identifier to the student_results table to facilitate 
-- pre-generation on a client system without necessarily knowing the ID it will
-- get assigned at the server.

-- UUID4 strings are 36 characters long.
ALTER TABLE .`student_results` 
ADD COLUMN `result_uuid` VARCHAR(36) NULL DEFAULT NULL COMMENT '' AFTER `last_updated_child`;

-- Define a trigger so that we can get MySQL to inject a randomly-generated UUID
-- (well, technically a UUID4) in as the default value in the result_uuid field.
--
-- If no UUID is set, assign one on INSERT or UPDATE. The API should provide a
-- UUID with each transaction, but OMIS 'proper' won't, so this injects one to
-- allow for consistency going forward in the event that somehow the same result
-- is being edited via OMIS and the app (or another API consumer) simultaneously
DELIMITER $$

DROP TRIGGER IF EXISTS student_results_BEFORE_INSERT$$

CREATE DEFINER = CURRENT_USER TRIGGER `student_results_BEFORE_INSERT` BEFORE INSERT ON `student_results` FOR EACH ROW
BEGIN
    IF NEW.`result_uuid` IS NULL OR NEW.`result_uuid` = '' THEN
        SET NEW.`result_uuid` = UUID();
    END IF;
END
$$

DROP TRIGGER IF EXISTS student_results_BEFORE_UPDATE$$

CREATE DEFINER = CURRENT_USER TRIGGER `student_results_BEFORE_UPDATE` BEFORE UPDATE ON `student_results` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
    IF NEW.`result_uuid` IS NULL OR NEW.`result_uuid` = '' THEN
        SET NEW.`result_uuid` = UUID();
    END IF;
END$$
DELIMITER ;

-- Turn off safe updates so that the following update runs in MySql Workbench.
SET sql_SAFE_UPDATES = 0;

-- Inject UUIDs into existing records where there are none currently.
UPDATE `student_results` SET `result_uuid` = UUID() WHERE `result_uuid` IS NULL;

-- Turn safe updates back on.
SET sql_SAFE_UPDATES = 1;

-- Log that the current version of the database is 68, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (68);
