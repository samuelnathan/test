-- Author: David Cunningham
-- Date: 14/06/2017
-- Merges flag count in student_results table to simplify things as there is no major demand at the moment 
-- for multi-colour flagging. Custom flag icons can now be dropped into the new custom folder per client instance.  

ALTER TABLE `student_results`
DROP COLUMN `redflag_count`,
CHANGE COLUMN `amberflag_count` `flag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0;

ALTER IGNORE TABLE `student_results_audit_trail`
DROP COLUMN `redflag_count`,
CHANGE COLUMN `amberflag_count` `flag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0;

-- Log that the current version of the database is 104, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (104);
