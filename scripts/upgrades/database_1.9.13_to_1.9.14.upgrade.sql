-- Issue#97 Remove all reserved words from table and field names in the database
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   15/04/2014
-- Omis database upgrade script version 1.9.13 => 1.9.14

-- Rename iso_country_codes.name field to iso_country_codes.iso_name
ALTER TABLE `iso_country_codes` 
CHANGE COLUMN `name` `iso_name` VARCHAR(80) NOT NULL;

-- Rename item_options.value field to item_options.option_value
ALTER TABLE `item_scores` 
CHANGE COLUMN `value` `option_value` VARCHAR(20) NULL DEFAULT NULL;

-- Rename users.password field to users.user_password
-- Rename users.level field to users.user_role
ALTER TABLE `users` 
DROP FOREIGN KEY `user_role_fk`;

ALTER TABLE `users` 
    CHANGE COLUMN `password` `user_password` VARCHAR(255) NOT NULL ,
    CHANGE COLUMN `level` `user_role` TINYINT(3) UNSIGNED NOT NULL DEFAULT '2';

ALTER TABLE `users` 
ADD CONSTRAINT `user_role_fk`
    FOREIGN KEY (`user_role`)
    REFERENCES `roles` (`role_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

-- Log that the current version of the database is 14, and it was added now.
INSERT INTO `database` (`version`) VALUES (14);