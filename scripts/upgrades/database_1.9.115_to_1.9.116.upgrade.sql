-- Author: David Cunningham
-- Date: 28/01/2019
-- HEE RFC 18 - item weighting
-- Version 1.9.115 => 1.9.116

ALTER TABLE section_items
  ADD weighted_value DOUBLE(3, 2) DEFAULT 1.00 NOT NULL;

ALTER TABLE forms
  ADD total_weighted_value DECIMAL(9, 3) NULL;

ALTER TABLE user_presets
  ADD feedback_item_weighting tinyint(1) unsigned DEFAULT 0 NOT NULL;

CREATE TABLE `student_station_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `station_number` SMALLINT(5) UNSIGNED NOT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `station_number`, `session_id`),
   INDEX `outcome_student_fk_idx` (`student_id` ASC),
   CONSTRAINT `outcome_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcome_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE)
  ENGINE = InnoDB
  COMMENT = 'Table to store the station outcomes for students';

ALTER TABLE `session_stations`
    CHANGE COLUMN `station_number` `station_number` SMALLINT(5) UNSIGNED NOT NULL ;

ALTER TABLE item_scores ADD option_weighted_value DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `option_value`;

ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_raw_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `grade_updated`,
  ADD COLUMN `overall_weighted_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_raw_score`,
  ADD COLUMN `overall_possible_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_weighted_score`;

ALTER TABLE `student_results`
  ADD COLUMN `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
  ADD COLUMN `possible_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `weighted_score`,
  ADD COLUMN `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `possible_score`;

ALTER TABLE `student_results_deleted`
  ADD COLUMN `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
  ADD COLUMN `possible_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `weighted_score`,
  ADD COLUMN `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `possible_score`;

ALTER TABLE `item_scores_deleted` ADD option_weighted_value DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `option_value`;

-- Overall possible pass
ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_pass` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_possible_score`;

ALTER TABLE `student_results` ADD examiner_grade TEXT DEFAULT '' AFTER `examiner_id`;
ALTER TABLE `student_results_deleted` ADD examiner_grade TEXT DEFAULT '' AFTER `examiner_id`;

ALTER TABLE `student_station_outcomes`
  ADD COLUMN `station_grade` TEXT DEFAULT '' AFTER `station_number`,
  ADD COLUMN `station_pass` DECIMAL(9,3) NULL DEFAULT NULL AFTER `station_grade`;

ALTER TABLE `student_exam_data`
  ADD COLUMN up_to_date TINYINT(1) DEFAULT 0 NOT NULL AFTER `overall_pass`;

CREATE TABLE `student_form_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `form_id`  BIGINT(10) NOT NULL,
   `passed` TINYINT(1) UNSIGNED DEFAULT NULL,
   `weighted_passed` TINYINT(1) UNSIGNED DEFAULT NULL,
   `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `form_id`, `session_id`),
   INDEX `outcomeform_student_fk_idx` (`student_id` ASC),
   CONSTRAINT `outcomeform_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeform_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeform_form_fk`
     FOREIGN KEY (`form_id`)
       REFERENCES `forms` (`form_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE
       )
  ENGINE = InnoDB
  COMMENT = 'Table to store the form outcomes for students';

-- Change name of 'station_tag' field to 'station_code' makes more sense when talking about
-- scenario codes/group codes
ALTER TABLE `session_stations`
  CHANGE COLUMN `station_tag` `station_code` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Group stations by tag' ;

CREATE TABLE `student_code_outcomes` (
     `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
     `student_id` VARCHAR(30) NOT NULL,
     `session_id` BIGINT(10) NOT NULL,
     `station_code` VARCHAR(100) NULL DEFAULT NULL,
     `passed` TINYINT(1) UNSIGNED DEFAULT NULL,
     `weighted_passed` TINYINT(1) UNSIGNED DEFAULT NULL,
     `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL,
     `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`outcome_id`),
     UNIQUE KEY (`student_id`, `session_id`, `station_code`),
     INDEX `outcomecode_student_fk_idx` (`student_id` ASC),
     CONSTRAINT `outcomecode_student_fk`
       FOREIGN KEY (`student_id`)
         REFERENCES `students` (`student_id`)
         ON DELETE CASCADE
         ON UPDATE CASCADE,
     CONSTRAINT `outcomecode_session_fk`
       FOREIGN KEY (`session_id`)
         REFERENCES `exam_sessions` (`session_id`)
         ON DELETE CASCADE
         ON UPDATE CASCADE
)
  ENGINE = InnoDB
  COMMENT = 'Table to store the station code outcomes for students';

-- Pass fields type/name changes (just run these to amend)
ALTER TABLE `student_results`
  CHANGE COLUMN `examiner_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `student_results_deleted`
  CHANGE COLUMN `examiner_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `student_station_outcomes`
  ADD COLUMN `weighted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `passed`,
  CHANGE COLUMN `station_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
  CHANGE COLUMN `station_pass` `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL ;

ALTER TABLE `student_exam_data`
  ADD COLUMN `weighted_grade` TEXT NULL DEFAULT '' AFTER `grade`;

ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_possible_weighted_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER overall_possible_score;

ALTER TABLE `student_exam_data`
  ADD COLUMN `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `grade_updated`,
  ADD COLUMN `weighted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `passed`;

CREATE TABLE `student_item_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `item_id` BIGINT(10) NOT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `session_id`, `item_id`),
   INDEX `outcomeitem_student_fk_index` (`student_id` ASC, `item_id` ASC),
   CONSTRAINT `outcomeitem_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeitem_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeitem_item_fk`
     FOREIGN KEY (`item_id`)
       REFERENCES `section_items` (`item_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Table to store the item outcomes for students';

SET @grance_value = '0.0'

-- UPDATE RESULTS FOR NOT SELF_ASSESSMENTS_OWNERSHIP
UPDATE student_results AS result INNER JOIN
(
  SELECT sr.result_id resultID,
        SUM(st.`highest_value`) sumPossibleValue,
        IF (((sr.score / SUM(st.`highest_value`)) * 100) - @grance_value >= ss.pass_value, 1, 0) passed,
        sr.score,
        (sr.score / SUM(st.`highest_value`) * 100) pecentage,
         ss.pass_value,
         @grance_value
  FROM student_results sr
    INNER JOIN session_stations ss ON sr.station_id = ss.station_id
    INNER JOIN item_scores its ON its.result_id = sr.result_id
    INNER JOIN section_items st ON its.item_id = st.item_id
    INNER JOIN form_sections fs ON fs.section_id = st.section_id AND fs.self_assessment = 0
  WHERE sr.result_id NOT IN
    (
      SELECT sr.result_id FROM student_results sr
        INNER JOIN session_stations ss ON sr.station_id = ss.station_id
        INNER JOIN exam_sessions ex ON ex.session_id = ss.session_id
        INNER JOIN self_assessments sa ON sa.exam_id = ex.exam_id
        INNER JOIN self_assessment_ownerships sao ON sao.self_assessment_id = sa.self_assessment_id AND sr.student_id = sao.student_id AND sr.examiner_id = sao.examiner_id
        INNER JOIN item_scores its ON its.result_id = sr.result_id
        INNER JOIN section_items st ON its.item_id = st.item_id
        INNER JOIN form_sections fs ON fs.section_id = st.section_id
    )
      GROUP BY  sr.result_id
) update_table
  ON result.result_id = update_table.resultID
  SET result.possible_score = update_table.sumPossibleValue,
      result.passed = update_table.passed,
      result.weighted_score = update_table.score,
      result.possible_weighted_score = update_table.score

-- UPDATE RESULTS FOR SELF_ASSESSMENT_OWNERSHIP
UPDATE student_results AS result INNER JOIN
  (
    SELECT sr.result_id resultID,
        SUM(st.`highest_value`) sumPossibleValue,
        IF (((sr.score / SUM(st.`highest_value`)) * 100) - 0.0 >= ss.pass_value, 1, 0) passed,
        sr.score
    FROM student_results sr
      INNER JOIN session_stations ss ON sr.station_id = ss.station_id
      INNER JOIN exam_sessions ex ON ex.session_id = ss.session_id
      INNER JOIN self_assessments sa ON sa.exam_id = ex.exam_id
      INNER JOIN self_assessment_ownerships sao ON sao.self_assessment_id = sa.self_assessment_id AND sr.student_id = sao.student_id AND sr.examiner_id = sao.examiner_id
      INNER JOIN item_scores its ON its.result_id = sr.result_id
      INNER JOIN section_items st ON its.item_id = st.item_id
      INNER JOIN form_sections fs ON fs.section_id = st.section_id
    GROUP BY  sr.result_id
  ) update_table
    ON result.result_id = update_table.resultID
    SET result.possible_score = update_table.sumPossibleValue,
        result.passed = update_table.passed,
        result.weighted_score = update_table.score,
        result.possible_weighted_score = update_table.score


-- Log that the current version of the database is 116, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (116, 0);

