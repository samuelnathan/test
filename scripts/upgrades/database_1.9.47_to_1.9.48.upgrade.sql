-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   03/07/2015
-- Omis database upgrade script version 1.9.47 => 1.9.48

-- Rename script names for printing/PDF (omis/print)
UPDATE `pages` SET `page_name`='pdf_examiners' WHERE `page_id`='102';
UPDATE `pages` SET `page_name`='pdf_examinees' WHERE `page_id`='103';
UPDATE `pages` SET `page_name`='pdf_groups' WHERE `page_id`='104';
UPDATE `pages` SET `page_name`='pdf_stations' WHERE `page_id`='105';

UPDATE `pages` SET `page_name`='preview_examinees' WHERE `page_id`='109';
UPDATE `pages` SET `page_name`='preview_groups' WHERE `page_id`='110';

-- Removing these pages as they don't seem to be used
DELETE FROM page_levels WHERE page_id = '106';
DELETE FROM pages WHERE page_id = '106';
DELETE FROM page_levels WHERE page_id = '107';
DELETE FROM pages WHERE page_id = '107';

-- Log that the current version of the database is 48, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (48);
