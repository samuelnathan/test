-- Author: David Cunningham
-- Date: 25/09/2016
-- OMIS database upgrade script version 1.9.80 => 1.9.81
-- Adds table for candidate number

DROP TABLE IF EXISTS `candidate_numbers`;
CREATE TABLE `candidate_numbers` (
  `stu_id` VARCHAR(30) NOT NULL,
  `term_id` VARCHAR(30) NOT NULL,
  `candidate_number` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`stu_id`, `term_id`),
  INDEX `candidatenumber_term_fk_idx` (`term_id` ASC),
  CONSTRAINT `candidatenumber_student_fk`
    FOREIGN KEY (`stu_id`)
    REFERENCES `students` (`stu_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `candidatenumber_term_fk`
    FOREIGN KEY (`term_id`)
    REFERENCES `terms` (`term_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Unique candidate numbers per term per student (optional)';

-- Give all existing students candidate numbers
SET @new_candidate_number := 1499;
INSERT INTO candidate_numbers (stu_id, term_id, candidate_number) 
SELECT stu_id, term_id, (@new_candidate_number := @new_candidate_number + 1) FROM (students 
INNER JOIN student_courses USING(stu_id))
INNER JOIN course_years USING(crs_yr_id)
WHERE term_id IN ("2016-17")
GROUP BY stu_id, term_id;

-- Log that the current version of the database is 81, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (81);
