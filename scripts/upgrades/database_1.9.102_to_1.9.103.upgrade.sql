-- Author: David Cunningham
-- Date: 08/06/2017
-- OMIS database upgrade script version 1.9.102 => 1.9.103
-- Adds preset field for feedback station notes option

ALTER TABLE `user_presets` 
ADD COLUMN `feedback_station_notes` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
ADD COLUMN `feedback_notes_title` TEXT DEFAULT NULL;

-- Log that the current version of the database is 103, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (103);
