-- Adding table for the labeling of weighting options for multiple examiners
-- Author: Cormac McSwiney <cormac.mcswiney@qpercom.ie>
-- Date:   15/05/2014
-- Omis database upgrade script version 1.9.18 => 1.9.19

ALTER TABLE exam_sessions ADD circuit_colour VARCHAR(30);

INSERT INTO `database` (`version`) VALUES (19);