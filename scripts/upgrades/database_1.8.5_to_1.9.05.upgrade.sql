/* Original Author: David Cunningham / Cormac McSwiney
   For Qpercom Ltd
   Date: 10/03/2014
   Â© 2014 Qpercom Limited. All rights reserved.
   
   Omis database upgrade script version 1.8.5 => 1.9.5
   Script to rename tables and fields for reasons of clarity
*/

/* Drop old foreign keys before renaming tables and fields */
ALTER TABLE osce DROP FOREIGN KEY osce_ibfk_1;
ALTER TABLE osce DROP FOREIGN KEY osce_ibfk_2;

ALTER TABLE stationset DROP FOREIGN KEY stationset_ibfk_1;
ALTER TABLE stationset DROP FOREIGN KEY stationset_ibfk_2;

ALTER TABLE station_dept_term DROP FOREIGN KEY station_dept_term_ibfk_1;
ALTER TABLE station_dept_term DROP FOREIGN KEY station_dept_term_ibfk_2;
ALTER TABLE station_dept_term DROP FOREIGN KEY station_dept_term_ibfk_3;

ALTER TABLE station_competence DROP FOREIGN KEY station_competence_ibfk_2;

ALTER TABLE station_instance DROP FOREIGN KEY station_instance_ibfk_1;
ALTER TABLE station_instance DROP FOREIGN KEY station_instance_ibfk_2;

ALTER TABLE item_scores DROP FOREIGN KEY item_scores_ibfk_3;
ALTER TABLE item_scores DROP FOREIGN KEY item_scores_ibfk_4;

ALTER TABLE competence_results DROP FOREIGN KEY competence_results_ibfk_2;

ALTER TABLE osce_session DROP FOREIGN KEY osce_session_ibfk_1;

ALTER TABLE grp DROP FOREIGN KEY grp_ibfk_1;
ALTER TABLE grp_detail DROP FOREIGN KEY grp_detail_ibfk_2;

ALTER TABLE competence_item DROP FOREIGN KEY competence_item_ibfk_1;
ALTER TABLE blank_student DROP FOREIGN KEY blank_student_ibfk_1;

ALTER TABLE session_assistant DROP FOREIGN KEY session_assistant_ibfk_1;
ALTER TABLE session_assistant DROP FOREIGN KEY session_assistant_ibfk_2;

ALTER TABLE student_feedback DROP FOREIGN KEY student_feedback_ibfk_1;
ALTER TABLE student_feedback DROP FOREIGN KEY student_feedback_ibfk_2;
ALTER TABLE student_feedback DROP FOREIGN KEY student_feedback_ibfk_3;

ALTER TABLE dept DROP FOREIGN KEY dept_ibfk_1;

ALTER TABLE examiner_dept_term DROP FOREIGN KEY examiner_dept_term_ibfk_1;
ALTER TABLE examiner_dept_term DROP FOREIGN KEY examiner_dept_term_ibfk_2;

ALTER TABLE examiner_dept_term DROP FOREIGN KEY examiner_dept_term_ibfk_3;
ALTER TABLE examiner_dept_term DROP FOREIGN KEY examiner_dept_term_ibfk_4;

ALTER TABLE item_options DROP FOREIGN KEY item_options_ibfk_1;

ALTER TABLE blank_student_inst DROP FOREIGN KEY blank_student_inst_ibfk_1;

ALTER TABLE course_year DROP FOREIGN KEY course_year_ibfk_1;
ALTER TABLE course_year DROP FOREIGN KEY course_year_ibfk_2;

ALTER TABLE course_year_module DROP FOREIGN KEY course_year_module_ibfk_1;
ALTER TABLE course_year_module DROP FOREIGN KEY course_year_module_ibfk_2;

ALTER TABLE student_crs DROP FOREIGN KEY student_crs_ibfk_1;
ALTER TABLE student_crs DROP FOREIGN KEY student_crs_ibfk_2;

ALTER TABLE student_crs_mod DROP FOREIGN KEY student_crs_mod_ibfk_1;
ALTER TABLE student_crs_mod DROP FOREIGN KEY student_crs_mod_ibfk_2;
ALTER TABLE student_crs_mod DROP FOREIGN KEY student_crs_mod_ibfk_3;

ALTER TABLE examiner_training DROP FOREIGN KEY examiner_training_ibfk_1;

ALTER TABLE feature_levels DROP FOREIGN KEY feature_levels_ibfk_1;

ALTER TABLE sms_message_recipients DROP FOREIGN KEY sms_message_recipients_ibfk_1;

ALTER TABLE user DROP FOREIGN KEY fk_level;

ALTER TABLE user_preference DROP FOREIGN KEY user_preference_ibfk_1;

/* Remove 'examiners_required' column from 'stationset' table. Can't be sure if 
   this column exists already or not in database to upgrade. 
   Anyways, it is inserted again in the query following this query.
   Original Author: Cormac McSwiney
*/
ALTER TABLE stationset DROP examiners_required;

/* Rename 'stationset' to 'session_stations' and rename table fields */
ALTER TABLE `stationset` 
CHANGE COLUMN `stationset_id` `station_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `oscetd_id` `session_id` BIGINT(10) NOT NULL DEFAULT '0',
CHANGE COLUMN `station_id` `form_id` BIGINT(10) NOT NULL DEFAULT '0',
ADD COLUMN examiners_required TINYINT UNSIGNED NOT NULL DEFAULT 1,
DROP INDEX `oscetd_id`,
ADD INDEX `session_id` (`session_id` ASC),
DROP INDEX `station_id`,
ADD INDEX `form_id` (`form_id` ASC),
RENAME TO `session_stations`;

/* Rename 'station' to 'form' and rename table fields */
ALTER TABLE `station` 
CHANGE COLUMN `station_id` `form_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `station_name` `form_name` TEXT NOT NULL,
CHANGE COLUMN `station_description` `form_description` TEXT NOT NULL, RENAME TO `forms`;


/* Rename 'osce_session' to 'exam_sessions' and rename table fields */
ALTER TABLE `osce_session`
CHANGE COLUMN `oscetd_id` `session_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `osce_id` `exam_id` BIGINT(10) NOT NULL,
CHANGE COLUMN `date` `session_date` DATE NOT NULL DEFAULT '0000-00-00',
CHANGE COLUMN `td_desc` `session_description` TEXT NOT NULL, 
CHANGE COLUMN `can_acc` `session_published` INT(1) NOT NULL DEFAULT '1', 
DROP INDEX `osce_id`,
ADD INDEX `exam_id` (`exam_id` ASC),
RENAME TO `exam_sessions`;

/* Rename table 'osce' to 'exam' and rename table fields */
ALTER TABLE `osce` 
CHANGE COLUMN `osce_id` `exam_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `osce_name` `exam_name` TEXT NOT NULL ,
CHANGE COLUMN `osce_desc` `exam_description` TEXT NOT NULL ,
CHANGE COLUMN `osce_acc` `exam_published` INT(1) NOT NULL DEFAULT '1' , RENAME TO `exams`;

/* Rename table 'grp_detail' to 'group_students' and rename table fields */
ALTER TABLE `grp_detail` 
CHANGE COLUMN `grp_id` `group_id` BIGINT(10) NOT NULL , RENAME TO `group_students`;

/* Rename table 'grp' to 'session_groups' and rename table fields */
ALTER TABLE `grp` 
CHANGE COLUMN `grp_id` `group_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `oscetd_id` `session_id` BIGINT(10) NOT NULL,
CHANGE COLUMN `grp_name` `group_name` TEXT NOT NULL,
CHANGE COLUMN `grp_order` `group_order` INT(2) NOT NULL DEFAULT '0', 
DROP INDEX `oscetd_id`,
ADD INDEX `session_id` (`session_id` ASC),
RENAME TO `session_groups`;

/* Rename table 'station_instance' to 'student_results' and rename table fields */
ALTER TABLE `station_instance` 
CHANGE COLUMN `station_instance_id` `result_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `stationset_id` `station_id` BIGINT(10) NOT NULL DEFAULT '0',
DROP INDEX `stationset_id`,
ADD INDEX `station_id` (`station_id` ASC),
RENAME TO `student_results`;

/* Rename table 'competence_results' to 'section_feedback' and rename table fields */
ALTER TABLE `competence_results` 
CHANGE COLUMN `stat_comp_id` `section_id` BIGINT(10) NOT NULL,
CHANGE COLUMN `station_instance_id` `result_id` BIGINT(10) NOT NULL,
CHANGE COLUMN `comp_comment` `feedback_text` TEXT NOT NULL, 
DROP INDEX `station_instance_id`,
ADD INDEX `result_id` (`result_id` ASC),
RENAME TO `section_feedback`;

/* Rename table 'session_assistant' to 'session_assistants' and rename table 
   field
 */
ALTER TABLE `session_assistant` 
CHANGE COLUMN `oscetd_id` `session_id` BIGINT(10) NOT NULL,
RENAME TO `session_assistants`;

/* Rename table 'station_dept_term' to 'form_department_term' and rename table
   fields
*/
ALTER TABLE `station_dept_term` 
CHANGE COLUMN `station_id` `form_id` BIGINT(10) NOT NULL, 
COMMENT = 'Form, Department and Term Relationship' , RENAME TO `form_department_term`;

/* Rename table 'station_competence' to 'form_section' and rename table fields */
ALTER TABLE `station_competence` 
CHANGE COLUMN `stat_comp_id` `section_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
CHANGE COLUMN `station_id` `form_id` BIGINT(10) NOT NULL,
CHANGE COLUMN `competence_order` `section_order` BIGINT(10) NOT NULL DEFAULT '0',
CHANGE COLUMN `addition_text` `additional_info` TEXT NOT NULL,
CHANGE COLUMN `comments_required` `feedback_required` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes, 2=Fail, 3=Borderline', 
DROP INDEX `station_id`, ADD INDEX `form_id` (`form_id` ASC),
RENAME TO `form_sections`;

/* Rename table 'competence_item' to 'section_items' and rename table fields */
ALTER TABLE `competence_item` 
CHANGE COLUMN `stat_comp_id` `section_id` BIGINT(10) NOT NULL,
DROP INDEX `stat_comp_id`,
ADD INDEX `section_id` (`section_id` ASC),
RENAME TO `section_items`;

/* Rename `blank_student` table fields */
ALTER TABLE `blank_student` 
CHANGE COLUMN `grp_id` `group_id` BIGINT(10) NOT NULL,
DROP INDEX `grp_id`,
ADD INDEX `group_id` (`group_id` ASC), 
RENAME TO `blank_students`;

/* Rename `item_scores` table fields */
ALTER TABLE `item_scores` 
CHANGE COLUMN `station_instance_id` `result_id` BIGINT(10) NOT NULL,
DROP INDEX `station_instance_id`,
ADD INDEX `result_id` (`result_id` ASC);

/* Rename `student_feedback` table fields */
ALTER TABLE `student_feedback` 
CHANGE COLUMN `osce_id` `exam_id` BIGINT(10) NOT NULL,
DROP INDEX `osce_id`,
DROP INDEX `template_id_2`,
ADD INDEX `exam_id` (`exam_id` ASC);

/* Rename table 'dept' to 'department' and rename table fields */
ALTER TABLE `dept` 
CHANGE COLUMN `faculty_id` `school_id` BIGINT(10) NOT NULL ,
DROP INDEX `faculty_id`,
ADD INDEX `school_id` (`school_id` ASC), RENAME TO `departments`;

/* Rename table 'faculty' to 'school' and rename table fields */
ALTER TABLE `faculty` 
CHANGE COLUMN `faculty_id` `school_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `faculty_description` `school_description` TEXT NOT NULL , RENAME TO `schools`;

/* Rename table 'blank_student_inst' to 'blank_students_discarded' */
ALTER TABLE `blank_student_inst` 
RENAME TO `blank_students_discarded`;

/* Rename table 'examiner_dept_term' to 'examiner_department_term' */
ALTER TABLE `examiner_dept_term` 
RENAME TO `examiner_department_term`;

/* Rename table 'course' to 'courses' */
ALTER TABLE `course` 
RENAME TO `courses`;

/* Rename table 'course_year' to 'course_years' */
ALTER TABLE `course_year` 
RENAME TO `course_years`;

/* Rename table 'competence' to 'competence_categories' */
ALTER TABLE `competence` 
RENAME TO `competencies`;

/* Rename table 'country' to 'iso_country_codes' */
ALTER TABLE `country` 
RENAME TO `iso_country_codes`;

/* Rename table 'email_template' to 'email_templates' */
ALTER TABLE `email_template` 
RENAME TO `email_templates`;

/* Rename table 'feature' to 'features' */
ALTER TABLE `feature` 
RENAME TO `features`;

/* Rename table 'module' to 'modules' */
ALTER TABLE `module` 
RENAME TO `modules`;

/* Rename table 'course_year_module' to 'year_modules' */
ALTER TABLE `course_year_module` 
RENAME TO `year_modules`;

/* Rename table 'rating_scale_default' to 'rating_scale_defaults' */
ALTER TABLE `rating_scale_default` 
RENAME TO `rating_scale_defaults`;

/* Rename table 'sms_message' to 'sms_messages' */
ALTER TABLE `sms_message` 
RENAME TO `sms_messages`;

/* Rename table 'student' to 'students' */
ALTER TABLE `student` 
CHANGE COLUMN `nationality` `nationality` CHAR(2) NOT NULL, 
RENAME TO `students`;

/* Rename table 'student_crs' to 'student_years' */
ALTER TABLE `student_crs` 
RENAME TO `student_years`;

/* Rename table 'student_crs_mod' to 'student_modules' */
ALTER TABLE `student_crs_mod` 
RENAME TO `student_modules`;

/* Rename table 'term' to 'terms' */
ALTER TABLE `term` 
RENAME TO `terms`;

/* Rename table 'user' to 'users' */
ALTER TABLE `user` 
  ADD COLUMN `pref_dept` varchar(20) NOT NULL,
  ADD COLUMN `pref_school` varchar(20) NOT NULL,
  ADD COLUMN `pref_course` varchar(20) NOT NULL,
  ADD COLUMN `pref_module` varchar(20) NOT NULL,
RENAME TO `users`;

/* Alter table 'role_category_link' change INT to TINYINT for fields 'role_id'
   & 'category_id'
 */
ALTER TABLE `role_category_link` 
CHANGE COLUMN `role_id` `role_id` TINYINT UNSIGNED NOT NULL ,
CHANGE COLUMN `category_id` `category_id` TINYINT UNSIGNED NOT NULL;

/* Alter table 'role_categories' change INT to TINYINT for field 'category_id' */
ALTER TABLE `role_categories` 
CHANGE COLUMN `category_id` `category_id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT;

/* Drop table 'user_preferences' */
DROP TABLE user_preference;

/* Put all Foreign Keys back together again with better naming conventions */
ALTER TABLE form_sections ADD CONSTRAINT section_form_fk FOREIGN KEY (form_id) REFERENCES forms(form_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE student_results ADD CONSTRAINT result_station_fk FOREIGN KEY (station_id) REFERENCES session_stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_results ADD CONSTRAINT result_student_fk FOREIGN KEY (stu_id) REFERENCES students(stu_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE item_scores ADD CONSTRAINT score_result_fk FOREIGN KEY (result_id) REFERENCES student_results(result_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE item_scores ADD CONSTRAINT score_item_fk FOREIGN KEY (item_id) REFERENCES section_items(item_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE section_feedback ADD CONSTRAINT feedback_result_fk FOREIGN KEY (result_id) REFERENCES student_results(result_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE section_feedback ADD CONSTRAINT feedback_section_fk FOREIGN KEY (section_id) REFERENCES form_sections(section_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE session_stations ADD CONSTRAINT station_session_fk FOREIGN KEY (session_id) REFERENCES exam_sessions(session_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session_stations ADD CONSTRAINT station_form_fk FOREIGN KEY (form_id) REFERENCES forms(form_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE exam_sessions ADD CONSTRAINT session_exam_fk FOREIGN KEY (exam_id) REFERENCES exams(exam_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session_groups ADD CONSTRAINT group_session_fk FOREIGN KEY (session_id) REFERENCES exam_sessions(session_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE group_students ADD CONSTRAINT student_group_fk FOREIGN KEY (group_id) REFERENCES session_groups(group_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE group_students ADD CONSTRAINT groupstudent_student_fk FOREIGN KEY (stu_id) REFERENCES students(stu_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE section_items ADD CONSTRAINT item_section_fk FOREIGN KEY (section_id) REFERENCES form_sections(section_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE blank_students ADD CONSTRAINT blank_student_group_fk FOREIGN KEY (group_id) REFERENCES group_students(group_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE session_assistants ADD CONSTRAINT assistant_session_fk FOREIGN KEY (session_id) REFERENCES exam_sessions(session_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session_assistants ADD CONSTRAINT assistant_user_fk FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE departments ADD CONSTRAINT department_school_fk FOREIGN KEY (school_id) REFERENCES schools(school_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE examiner_department_term ADD CONSTRAINT examinerdept_department_fk FOREIGN KEY (dept_id) REFERENCES departments(dept_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE examiner_department_term ADD CONSTRAINT examinerdept_term_fk FOREIGN KEY (term_id) REFERENCES terms(term_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE examiner_department_term ADD CONSTRAINT examinerdept_user_fk FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE form_department_term ADD CONSTRAINT depterm_form_fk FOREIGN KEY (form_id) REFERENCES forms(form_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE form_department_term ADD CONSTRAINT form_department_fk FOREIGN KEY (dept_id) REFERENCES departments(dept_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE form_department_term ADD CONSTRAINT form_term_fk FOREIGN KEY (term_id) REFERENCES terms(term_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE item_options ADD CONSTRAINT option_item_fk FOREIGN KEY (item_id) REFERENCES section_items(item_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE blank_students_discarded ADD CONSTRAINT discarded_blank_student_fk FOREIGN KEY (blank_stud_id) REFERENCES blank_students(blank_stud_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE course_years ADD CONSTRAINT year_course_fk FOREIGN KEY (crs_id) REFERENCES courses(crs_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE course_years ADD CONSTRAINT year_term_fk FOREIGN KEY (term_id) REFERENCES terms(term_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE year_modules ADD CONSTRAINT yearmodule_year_fk FOREIGN KEY (crs_yr_id) REFERENCES course_years(crs_yr_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE year_modules ADD CONSTRAINT yearmodule_module_fk FOREIGN KEY (mod_id) REFERENCES modules(mod_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE student_years ADD CONSTRAINT studentyear_year_fk FOREIGN KEY (crs_yr_id) REFERENCES course_years(crs_yr_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_years ADD CONSTRAINT studentyear_student_fk FOREIGN KEY (stu_id) REFERENCES students(stu_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE student_modules ADD CONSTRAINT module_student_fk FOREIGN KEY (stu_id) REFERENCES students(stu_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_modules ADD CONSTRAINT studentmodule_module_fk FOREIGN KEY (mod_id) REFERENCES modules(mod_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_modules ADD CONSTRAINT studentmodule_year_fk FOREIGN KEY (crs_yr_id) REFERENCES course_years(crs_yr_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE student_feedback ADD CONSTRAINT feedback_exam_fk FOREIGN KEY (exam_id) REFERENCES exams(exam_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_feedback ADD CONSTRAINT feedback_student_fk FOREIGN KEY (stu_id) REFERENCES students(stu_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_feedback ADD CONSTRAINT feedback_email_template_fk FOREIGN KEY (template_id) REFERENCES email_templates(template_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE exams ADD CONSTRAINT exam_department_fk FOREIGN KEY (dept_id) REFERENCES departments(dept_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE exams ADD CONSTRAINT exam_term_fk FOREIGN KEY (term_id) REFERENCES terms(term_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE examiner_training ADD CONSTRAINT examinertraining_user_fk FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE feature_levels ADD CONSTRAINT level_feature_fk FOREIGN KEY (feature_id) REFERENCES features(feature_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE role_category_link ADD CONSTRAINT rolecat_role_fk FOREIGN KEY (role_id) REFERENCES roles(role_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role_category_link ADD CONSTRAINT rolecat_category_fk FOREIGN KEY (category_id) REFERENCES role_categories(category_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE sms_message_recipients ADD CONSTRAINT recipient_sms_fk FOREIGN KEY (sms_id) REFERENCES sms_messages(sms_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE users ADD CONSTRAINT user_role_fk FOREIGN KEY (level) REFERENCES roles(role_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE page_levels ADD CONSTRAINT level_page_fk FOREIGN KEY (page_level) REFERENCES roles(role_id) ON DELETE CASCADE ON UPDATE CASCADE;

/* Insert table for Multiple Examiners feature 
   Original Author: Cormac McSwiney
*/
DROP TABLE IF EXISTS stationset_examiner;
CREATE TABLE `station_examiners` (
    `station_id` BIGINT(10) NOT NULL,
    `examiner_id` VARCHAR(30)CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `scoring_weight` INT(10) unsigned DEFAULT '1' COMMENT 'Examiners assigned to a station within a session within an exam',
    PRIMARY KEY (`station_id` , `examiner_id`),
    KEY `examiner_user_fk` (`examiner_id`),
    CONSTRAINT `examiner_station_fk` FOREIGN KEY (`station_id`)
        REFERENCES `session_stations` (`station_id`)
        ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `examiner_user_fk` FOREIGN KEY (`examiner_id`)
        REFERENCES `users` (`user_id`)
        ON DELETE CASCADE ON UPDATE CASCADE
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* 
  Drop 'university_details' table. ISO country variable will be added to the Omis config file instead.
*/
DROP TABLE university_details;

/* 
  Update page no '35' in pages table set name 'ajx_ud_sname' => 'ajx_update_form_info'
*/
UPDATE pages SET page_name='ajx_update_form_info' WHERE page_id='35';

/* 
  Alter the student_results table and add is_complete field
*/
ALTER TABLE `student_results` ADD COLUMN `is_complete` tinyint(1) unsigned NOT NULL DEFAULT '0';

 /* 
 Turn off safe mode
 */
SET SQL_SAFE_UPDATES = 0;

/* 
  Update station_instance table and set all is_complete values to 1
*/
UPDATE student_results SET `is_complete` = '1';

 /* 
 Turn safe mode back on
 */
SET SQL_SAFE_UPDATES = 1;
 
 
-- Create table to track database version for OMIS 1.9. Initial commit is 5 here
-- as this database script pushes the database version to 1.9.5.
DROP TABLE IF EXISTS `database`;
CREATE TABLE `database` (
  `version` INT NOT NULL,
  `applied` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`))
COMMENT = 'Version Control Database table.';

-- Log that the current version of the database is 5, and it was added now.
INSERT INTO `database` (`version`) VALUES (5);
