-- Page name updates & new pages for Omis
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   21/03/2014
-- Omis database upgrade script version 1.9.7 => 1.9.8

-- Update Pages

-- Page name ajx_admins => ajx_osce_assistants
UPDATE `pages` SET `page_name` = 'ajx_osce_assistants' WHERE `page_id` = 9;

-- Page name admins_osce => assistants_osce
UPDATE `pages` SET `page_name` = 'assistants_osce' WHERE `page_id` = 4;

-- Log that the current version of the database is 8, and it was added now.
INSERT INTO `database` (`version`) VALUES (8);