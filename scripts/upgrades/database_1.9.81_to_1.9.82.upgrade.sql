-- Author: David Cunningham
-- Date: 17/10/2016
-- OMIS database upgrade script version 1.9.81 => 1.9.82
-- Adds table for exam rules

DROP TABLE IF EXISTS `exam_rules`;
CREATE TABLE `exam_rules` (
  `id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `exam_id` BIGINT(10) NULL,
  `max_grs_fail` TINYINT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `exam_rules_fk_idx` (`exam_id` ASC),
  CONSTRAINT `exam_rules_fk`
    FOREIGN KEY (`exam_id`)
    REFERENCES `exams` (`exam_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Exam rules tables linked to exams table, rows populated via triggers';

DELIMITER $$
DROP TRIGGER IF EXISTS exams_AFTER_INSERT$$
CREATE TRIGGER `exams_AFTER_INSERT` AFTER INSERT ON `exams` FOR EACH ROW
BEGIN
   INSERT INTO exam_settings (exam_id) VALUES (NEW.exam_id);
   INSERT INTO exam_rules (exam_id) VALUES (NEW.exam_id);
END$$
DELIMITER ;

-- Page 'station_setup' => 'exam_rules'
UPDATE `pages` SET `page_name`='exam_rules' WHERE `page_id`='132';

-- Log that the current version of the database is 82, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (82);
