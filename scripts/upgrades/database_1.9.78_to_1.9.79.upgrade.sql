-- Author: David Cunningham
-- Date: 19/09/2016
-- OMIS database upgrade script version 1.9.78 => 1.9.79
-- Adds new exam setting show|hide countdown

-- Add countdown show|hide setting
ALTER TABLE `exams` 
ADD COLUMN `show_countdown` INT(1) NOT NULL DEFAULT '1' COMMENT 'Show or hide countdown in assessment' 
AFTER `default_examiners_station`;

-- Log that the current version of the database is 79, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (79);
