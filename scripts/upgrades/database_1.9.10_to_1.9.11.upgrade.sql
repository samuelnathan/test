-- Access control updates and a page name Dave missed in the last update...
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   26/03/2014
-- Omis database upgrade script version 1.9.10 => 1.9.11

-- Page name sts_osce => forms_osce
UPDATE `pages` SET `page_name` = 'forms_osce' WHERE `page_id` = 140;

-- Update the permissions for affected pages...
DELETE FROM `page_levels` WHERE `page_id` IN (38, 77, 90, 115, 117);

-- Updated permissions for page 'form_bank' (38)
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (38, 1, 1), (38, 2, 0), (38, 3, 1), (38, 4, 0), (38, 5, 1), (38, 6, 1);

-- Updated permissions for page 'json_stations' (77)
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (77, 1, 1), (77, 2, 0), (77, 3, 1), (77, 4, 0), (77, 5, 1), (77, 6, 1);

-- Updated permissions for page 'new_form' (90)
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (90, 1, 1), (90, 2, 0), (90, 3, 1), (90, 4, 0), (90, 5, 1), (90, 6, 1);

-- Updated permissions for page 'single_form_previous_session' (115)
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (115, 1, 1), (115, 2, 0), (115, 3, 1), (115, 4, 0), (115, 5, 1), (115, 6, 1);

-- Updated permissions for page 'radar_feedback' (117)
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (117, 1, 1), (117, 2, 0), (117, 3, 0), (117, 4, 0), (117, 5, 0), (117, 6, 1);

-- Log that the current version of the database is 11, and it was added now.
INSERT INTO `database` (`version`) VALUES (11);