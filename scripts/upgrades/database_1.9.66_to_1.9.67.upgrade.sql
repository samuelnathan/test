-- Author: David Cunningham
-- Date:   07/03/2016
-- OMIS database upgrade script version 1.9.66 => 1.9.67

-- Remove no longer required  'submit_new_item_slider' page (root/osce/)
DELETE FROM page_levels WHERE page_id = 145;
DELETE FROM pages WHERE page_id = 145;

-- Remove no longer required  'update_slider_item' page (root/osce/)
DELETE FROM page_levels WHERE page_id = 154;
DELETE FROM pages WHERE page_id = 154;

-- Update page name 'update_textbox_item' => 'update_item'
UPDATE pages SET page_name='update_item' WHERE page_id = 183;

-- Remove old competence type from form_sections 
UPDATE form_sections SET competence_id = 1 WHERE competence_id IN (2, 3, 4, 5);

-- Insert radar competencies page
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (191, 'radar_competencies');
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,2,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,4,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,6,1);

-- Add competency framework as a feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("competency-framework", "Competency Framework", 2);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("competency-framework", 1, 0), ("competency-framework", 2, 0), ("competency-framework", 3, 0), ("competency-framework", 5, 0);

-- Add competency framework related table structures
CREATE TABLE `competency_frameworks` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table that contains competency frameworks e.g. CanMeds 2015, MAAS-Global etc';

CREATE TABLE `competency_framework_levels` (
  `id` BIGINT(10) UNSIGNED NOT NULL,
  `framework_id` BIGINT(10) UNSIGNED NOT NULL,
  `name` VARCHAR(200) NULL DEFAULT NULL,
  `shortname` VARCHAR(60) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `framework_id`),
  INDEX `levels_competencyframework_fk_idx` (`framework_id` ASC),
  CONSTRAINT `levels_competencyframework_fk`
    FOREIGN KEY (`framework_id`)
    REFERENCES `competency_frameworks` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin
COMMENT = 'Table to contain the competency framework levels';

CREATE TABLE `framework_level_treepaths` (
  `ancestor` BIGINT(10) UNSIGNED NOT NULL,
  `descendant` BIGINT(10) UNSIGNED NOT NULL,
  `tree_length` BIGINT(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`ancestor`, `descendant`),
  INDEX `treepath_level_descendant_fk_idx` (`descendant` ASC),
  CONSTRAINT `treepath_level_ancestor_fk`
    FOREIGN KEY (`ancestor`)
    REFERENCES `competency_framework_levels` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `treepath_level_descendant_fk`
    FOREIGN KEY (`descendant`)
    REFERENCES `competency_framework_levels` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin
COMMENT = 'Closure table for the competency framework levels';

CREATE TABLE `item_competencies` (
  `item_id` BIGINT(10) NOT NULL,
  `competency_id` BIGINT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`item_id`, `competency_id`),
  CONSTRAINT `item_competency_fk`
    FOREIGN KEY (`item_id`)
    REFERENCES `section_items` (`item_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Relationship table that connects form items with competencies (one to many)';

-- Add competency framework related table data 
INSERT INTO `competency_frameworks` (`id`,`name`,`description`) VALUES (1,'Standard Framework',NULL);
INSERT INTO `competency_frameworks` (`id`,`name`,`description`) VALUES (2,'CanMeds 2015 Framework',NULL);
INSERT INTO `competency_framework_levels` VALUES 
(1,1,'Standard Framework','Standard','Standard Framework'),
(2,2,'CanMeds 2015','CanMeds2015','CanMeds 2015'),
(3,1,'Attitudes','Attitudes','Attitudes'),
(4,1,'Knowledge','Knowledge','Knowledge'),
(5,1,'Overall Competence','Overall','Overall Competence'),
(6,1,'Skills','Skills','Skills'),
(7,2,'Medical Expert','Medical Expert','As Medical Experts, physicians integrate all of the CanMEDS Roles, applying medical knowledge, clinical skills, and professional attitudes in their provision of high-quality and safe patient-centred care. Medical Expert is the centralphysician Role in the CanMEDS Framework and defines the physician’s clinical scope of practice.'),
(8,2,'Communicator','Communicator','As Communicators, physicians form relationships with patients and their families that facilitate the gathering and sharing of information essential for effective health care.'),
(9,2,'Collaborator','Collaborator','As Collaborators, physicians work effectively with other health care professionals to provide safe, high-quality patient-centred care.'),
(10,2,'Leader (Manager)','Leader','As Leaders, physicians develop a vision of a high-quality health care system and, in collaboration with other health care leaders, take responsibility for effecting change to move the system toward the achievement of that vision.'),
(11,2,'Health Advocate','Health Advocate','As Health Advocates, physicians contribute their expertise and influence as they work with communities or patient populations to improve health. They work with those they serve to determine and understand needs, speak on behalf of others when needed, and support the mobilization of resources to effect change.'),
(12,2,'Scholar','Scholar','As Scholars, physicians demonstrate a lifelong commitment to excellence in practice through continuous learning, the teaching of others, the evaluation of evidence, and contributions to scholarship.'),
(13,2,'Professional','Professional','As Professionals, physicians are committed to the health and well-being of individual patients and society through ethical practice, high personal standards of behaviour, dedication to the profession, profession-led regulation, and maintenance of personal health.'),
(14,2,'Practise medicine within their defined scope of practice and expertise','Practise medicine within scope of expertise',''),
(15,2,'Perform a patient-centred clinical assessment and establish management plans appropriate for their specialty','Patient-centred clinical assessment',NULL),
(16,2,'Plan and perform procedures and interventions for the purpose of assessment and/or management','Plan and perform procedures',NULL),
(17,2,'Establish plans for ongoing care and, when appropriate, timely consultation','Establish plans for ongoing care',NULL),
(18,2,'Actively participate, as an individual and as a member of a team providing care, in the continuous improvement of health care quality and patient safety','Team member providing healthcare quality',NULL),
(19,2,'Establish professional therapeutic relationships with patients and their families','Good relationship with patient and their family',NULL),
(20,2,'Elicit and synthesize accurate and relevant information, incorporating the perspectives of patients and their families','Elicit accurate relevant information',NULL),
(21,2,'Share health care information and plans with patients and their families','Share healthcare information with patient and family',NULL),
(22,2,'Engage patients and their families in developing plans that reflect the patient’s health care needs and goals','Engage patient & family in developing plans',NULL),
(23,2,'Document and share written and electronic information about the medical encounter to optimize clinical decision-making, patient safety, confidentiality, and privacy','Engage patient & family to develop plan for healthcare needs',NULL),
(24,2,'Work effectively with physicians and other colleagues in the health care professions','Work effectively with colleagues in healthcare profession',NULL),
(25,2,'Work with physicians and other colleagues in the health care professions to prevent misunderstandings, manage differences, and resolve conflicts','Prevent misunderstandings, manage differences, resolve confl',NULL),
(26,2,'Effectively and safely transfer care to another health care professional','Safely transfer care to another healthcare professional',NULL),
(27,2,'Contribute to the improvement of health care delivery in teams, organizations, and systems','Help improve healthcare delivery teams/organizations/systems',NULL),
(28,2,'Engage in the stewardship of health care resources','Engage in stewardship of healthcare resources',NULL),
(29,2,'Demonstrate leadership in professional practice','Demonstrate leadership professional practice',NULL),
(30,2,'Manage their practice and career','Manage their practice and career',NULL),
(31,2,'Respond to the individual patient’s health needs by advocating with the patient within and beyond the clinical environment','attend to patient’s health needs beyond clinical environment',NULL),
(32,2,'Respond to the needs of the communities or patient populations they serve by advocating with them for system-level change','Respond to needs of the communities or patient populations',NULL),
(33,2,'Engage in the continuous enhancement of their professional activities through ongoing learning','Continuous enhancement professional activities by learning',NULL),
(34,2,'Facilitate the learning of students, residents, the public, and other health care professionals','Facilitate the learning of students/residents/public',NULL),
(35,2,'Integrate best available evidence, contextualized to specific situations, into real-time decision-making','Integrate best evidence into real-time decision-making',NULL),
(36,2,'Critically evaluate the integrity, reliability, and applicability of health-related research and literature','Critically evaluate health-related research & literature',NULL),
(37,2,'Contribute to the dissemination and/or creation of knowledge and practices applicable to health','Contribute to creation of knowledge and practices',NULL),
(38,2,'Demonstrate a commitment to patients by applying best practices and adhering to high ethical standards','Adheres to high ethical standards',NULL),(39,2,'Demonstrate a commitment to society by recognizing and responding to the social contract in health care','Commitment to society, responds social contract healthcare',NULL),(40,2,'Demonstrate a commitment to the profession by adhering to standards and participating in physician-led regulation','Adheres to standards & regulations',NULL),(41,2,'Demonstrate a commitment to physician health and well-being to foster optimal patient care','Commitment to physician health and well-being',NULL);

INSERT INTO `framework_level_treepaths` VALUES (1,1,0),
(1,3,1),(1,4,1),(1,5,1),(1,6,1),(2,2,0),(2,7,1),(2,8,1),(2,9,1),
(2,10,1),(2,11,1),(2,12,1),(2,13,1),(3,3,0),(4,4,0),(5,5,0),(6,6,0),
(7,7,0),(7,14,2),(7,15,2),(7,16,2),(7,17,2),(7,18,2),(8,8,0),(8,19,2),
(8,20,2),(8,21,2),(8,22,2),(8,23,2),(9,9,0),(9,24,2),(9,25,2),(9,26,2),
(10,10,0),(10,27,2),(10,28,2),(10,29,2),(10,30,2),(11,11,0),(11,31,2),
(11,32,2),(12,12,0),(12,33,2),(12,34,2),(12,35,2),(12,36,2),(12,37,2),
(13,13,0),(13,38,2),(13,39,2),(13,40,2),(13,41,2),(14,14,0),(15,15,0),
(16,16,0),(17,17,0),(18,18,0),(19,19,0),(20,20,0),(21,21,0),(22,22,0),
(23,23,0),(24,24,0),(25,25,0),(26,26,0),(27,27,0),(28,28,0),(29,29,0),
(30,30,0),(31,31,0),(32,32,0),(33,33,0),(34,34,0),(35,35,0),(36,36,0),
(37,37,0),(38,38,0),(39,39,0),(40,40,0),(41,41,0);

-- Update database version number
INSERT INTO `database_versions` (`version`) VALUES (67);
