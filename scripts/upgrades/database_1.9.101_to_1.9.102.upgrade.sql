-- Author: David Cunningham
-- Date: 23/05/2017
-- OMIS database upgrade script version 1.9.101 => 1.9.102
-- Adds 2 new scripts 'json_exams.php' and 'json_sessions.php' to page control

INSERT INTO pages (page_id, page_name) VALUES (201, 'json_exams');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (201, 1, 1), (201, 2, 0), (201, 3, 1), (201, 4, 0), (201, 5, 1), (201, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (202, 'json_sessions');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (202, 1, 1), (202, 2, 0), (202, 3, 1), (202, 4, 0), (202, 5, 1), (202, 6, 1);

-- Log that the current version of the database is 102, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (102);
