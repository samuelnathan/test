-- Upgrade script to improve GRS scale handling in Omis 1.9
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   23/09/2014
-- Omis database upgrade script version 1.9.34 => 1.9.35

ALTER TABLE `session_stations` 
ADD COLUMN `rest_station` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `examiners_required`;

ALTER TABLE `session_stations` 
CHANGE COLUMN `form_id` `form_id` BIGINT(10) NULL DEFAULT NULL;

-- Log that the current version of the database is 35, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (35);
