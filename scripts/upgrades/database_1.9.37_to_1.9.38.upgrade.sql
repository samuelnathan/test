-- Upgrade script for the GRS (Global Rating Scale) Values Management Admin Tool in Omis 1.9
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   29/10/2014
-- Omis database upgrade script version 1.9.37 => 1.9.38

-- Create Global Rating Scale pages
-- Only Accessible to Super Administrators

-- Rename old Ajax/JSON script
UPDATE `pages` SET `page_name` = 'ajax_grs_types' WHERE `page_id` = 186;

-- Add new Ajax/JSON script for GRS Values
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (187, 'ajax_grs_values');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (187, 1, 0), (187, 2, 0), (187, 3, 0), (187, 4, 0), (187, 5, 0), (187, 6, 1);

-- Log that the current version of the database is 38, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (38);