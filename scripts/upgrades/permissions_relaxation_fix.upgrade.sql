-- This fix stems from the permissions mapper deciding to block all pages that 
-- are considered both "blocked" and "accessible" at the same time from 
-- different parts of OMIS, and the permissions mapper has been changed to
-- ensure that if a page is needed anywhere that it's accessible rather than 
-- being excessively draconian in how it blocks stuff.
-- DW 14-01-2014
-- 
-- This was generated using the following command in the permission mapper folder:
-- $ grep -B2 -h "\^\^" admin.sql assistant.sql exam_admin.sql examiner.sql station_manager.sql super_admin.sql
-- 
-- Pages affected: radar_feedback (117), ajx_tds (33), pdf_pexmee (103)
-- Roles affected: examiner (2), exam_admin (3), station_manager (5)
-- 
-- Map 'Root.mm'->'Root': requires page 'radar_feedback'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 117 AND page_level = 3;
-- ^^ (Was disabled, but re-enabling as is required...)
--
-- Map 'Root.mm'->'Root': requires page 'radar_feedback'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 117 AND page_level = 2;
-- ^^ (Was disabled, but re-enabling as is required...)
--
-- Map 'Osce\Perform OSCE.mm'->'Select Examination': requires page 'ajx_tds'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 33 AND page_level = 2;
-- ^^ (Was disabled, but re-enabling as is required...)
--
-- Map 'Root.mm'->'Root': requires page 'radar_feedback'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 117 AND page_level = 5;
-- ^^ (Was disabled, but re-enabling as is required...)
--
-- Map 'Osce\Management & Analysis.mm'->'Add/Edit Session': requires page 'ajx_tds'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 33 AND page_level = 5;
-- ^^ (Was disabled, but re-enabling as is required...)
--
-- Map 'Osce\Management & Analysis.mm'->'PDF & Print': requires page 'pdf_pexmee'
UPDATE page_levels SET level_enabled = 1 WHERE page_id = 103 AND page_level = 5;
-- ^^ (Was disabled, but re-enabling as is required...)