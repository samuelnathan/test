-- Author: Enda Phelan
-- Date: 18/11/2016
-- OMIS database upgrade script version 1.9.83 => 1.9.84
-- Add a flag to exam_settings to make students appear in the same order as they are set in their group

ALTER TABLE `exam_settings` 
ADD COLUMN `preserve_group_order` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'If set, the students will appear in the same order as they are set in their group' AFTER `results_column_grs`;

-- Log that the current version of the database is 84, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (84);
