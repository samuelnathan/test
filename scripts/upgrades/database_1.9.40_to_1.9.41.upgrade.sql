-- Move fields `submit_next`, `view_totals`, `view_marks`, `view_results`
-- from 'exam_sessions' table to 'exams' table   
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   09/02/2015
-- Omis database upgrade script version 1.9.40 => 1.9.41

ALTER TABLE `exam_sessions` 
DROP COLUMN `submit_next`,
DROP COLUMN `view_totals`,
DROP COLUMN `view_marks`,
DROP COLUMN `view_results`;

ALTER TABLE `exams`
ADD COLUMN `view_results` int(1) NOT NULL DEFAULT '0',
ADD COLUMN `view_marks` int(1) NOT NULL DEFAULT '0',
ADD COLUMN `view_totals` int(1) NOT NULL DEFAULT '0',
ADD COLUMN `submit_next` int(1) NOT NULL DEFAULT '0';

-- Log that the current version of the database is 41, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (41);

