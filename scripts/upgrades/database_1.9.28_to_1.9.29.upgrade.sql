-- Upgrade script to add api field to users table in database
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   18/06/2014
-- Omis database upgrade script version 1.9.28 => 1.9.29

ALTER TABLE `users` 
ADD COLUMN `api_key` VARCHAR(32) NOT NULL AFTER `pref_module`;

-- Log that the current version of the database is 29, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (29);