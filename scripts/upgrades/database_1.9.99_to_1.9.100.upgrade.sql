-- Author: David Cunningham
-- Date: 15/05/2017
-- OMIS database upgrade script version 1.9.99 => 1.9.100
-- Change width of VARCHAR template_name from 40 to 80 width

ALTER TABLE `email_templates` 
CHANGE COLUMN `template_name` `template_name` VARCHAR(80) NULL DEFAULT NULL ;

-- Log that the current version of the database is 100, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (100);