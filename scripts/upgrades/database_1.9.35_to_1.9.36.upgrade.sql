-- Upgrade script to improve GRS scale handling in Omis 1.9
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   23/09/2014
-- Omis database upgrade script version 1.9.35 => 1.9.36

-- Create global rating scale TYPES table
CREATE TABLE `rating_scale_types` (
  `scale_type_id` TINYINT UNSIGNED NOT NULL,
  `scale_type_name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
  `scale_type_colour` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
  PRIMARY KEY (`scale_type_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Global rating scale types';

-- Create global rating scale VALUES table
CREATE TABLE `rating_scale_values` (
  `scale_value_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `scale_type_id` TINYINT UNSIGNED NOT NULL,
  `scale_value` TINYINT NULL,
  `scale_value_description` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
  PRIMARY KEY (`scale_value_id`, `scale_type_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Global rating scale values';

-- Add FOREIGN KEY CONTRAINT between both 'rating_scale_values' table and 'rating_scale_values' table
ALTER TABLE `rating_scale_values` 
ADD INDEX `scalevalues_scaletypes_fk_idx` (`scale_type_id` ASC);
ALTER TABLE `rating_scale_values` 
ADD CONSTRAINT `scalevalues_scaletypes_fk`
  FOREIGN KEY (`scale_type_id`)
  REFERENCES `rating_scale_types` (`scale_type_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Add data to the global rating scale TYPES table
INSERT INTO rating_scale_types (scale_type_id, scale_type_name, scale_type_colour)
VALUES 
(1, "Type 1", "black"),
(2, "Type 2", "blue"),
(3, "Type 3", "green"),
(4, "Type 4", "red"),
(5, "Type 5", "black"),
(6, "Type 6", "brown-color-font"),
(7, "Type 7", "indigo-color-font");

-- Add data to the global rating scale VALUES table
INSERT INTO rating_scale_values (scale_type_id, scale_value, scale_value_description)
VALUES 
(1, 0, "Clear Fail"),
(1, 1, "Borderline"),
(1, 2, "Clear Pass"),
(1, 3, "Good Pass"),
(1, 4, "Excellent"),
(2, 0, "Clear Fail"),
(2, 1, "Borderline Fail"),
(2, 2, "Borderline Pass"),
(2, 3, "Clear Pass"),
(2, 4, "Excellent"),
(3, 0, "Fail"),
(3, 1, "Borderline"),
(3, 2, "Pass"),
(4, 0, "Unacceptable"),
(4, 1, "Borderline"),
(4, 2, "Okay"),
(4, 3, "Good"),
(4, 4, "Outstanding"),
(5, 0, "Fail"),
(5, 1, "Pass"),
(6, 0, "Fail"),
(6, 1, "Borderline"),
(6, 2, "Good"),
(6, 3, "Outstanding"),
(7, 0, "Very Poor"),
(7, 1, "Unsatisfactory"),
(7, 2, "Just Good Enough"),
(7, 3, "Adequate But Incomplete"),
(7, 4, "Very Good And Complete");

-- Drop old 'global_rating_scales' table
DROP TABLE `global_rating_scales`;

-- Log that the current version of the database is 36, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (36);