-- Create a simple table so that we can keep track of database updates.
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   22/04/2014
-- Omis database upgrade script version 1.9.15 => 1.9.16

-- Create the table. This update will be backported to the 1.9.3 database script
CREATE TABLE IF NOT EXISTS `database` (
  `version` INT NOT NULL,
  `applied` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`))
COMMENT = 'Version Control Database table.';

-- Log that the current version of the database is 17, and it was added now.
INSERT INTO `database` (`version`) VALUES (17);

