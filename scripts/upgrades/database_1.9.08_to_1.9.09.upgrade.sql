
-- Fixing holes in the access control mechanism for printing student results
-- and handling the slicing up of redirect_functions.php
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   24/03/2014
-- Omis database upgrade script version 1.9.8 => 1.9.9
INSERT INTO pages (page_id, page_name) VALUES (168, 'pdf_result_form');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (168, 1, 1), (168, 2, 0), (168, 3, 1), (168, 4, 0), (168, 5, 0), (168, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (169, 'analysis_redirects');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (169, 1, 1), (169, 2, 0), (169, 3, 1), (169, 4, 0), (169, 5, 0), (169, 6, 1);

INSERT INTO pages(page_id, page_name) VALUES (170, 'term_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (170, 1, 1), (170, 2, 0), (170, 3, 0), (170, 4, 0), (170, 5, 0), (170, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (171, 'examiners_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (171, 1, 1), (171, 2, 0), (171, 3, 1), (171, 4, 0), (171, 5, 0), (171, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (172, 'admins_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (172, 1, 1), (172, 2, 0), (172, 3, 0), (172, 4, 0), (172, 5, 0), (172, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (174, 'schools_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (174, 1, 1), (174, 2, 0), (174, 3, 0), (174, 4, 0), (174, 5, 0), (174, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (175, 'features_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (175, 1, 0), (175, 2, 0), (175, 3, 0), (175, 4, 0), (175, 5, 0), (175, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (176, 'tools_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (176, 1, 1), (176, 2, 0), (176, 3, 0), (176, 4, 0), (176, 5, 0), (176, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (177, 'examinees_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (177, 1, 1), (177, 2, 0), (177, 3, 1), (177, 4, 0), (177, 5, 0), (177, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (178, 'pages_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (178, 1, 1), (178, 2, 1), (178, 3, 1), (178, 4, 0), (178, 5, 1), (178, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (179, 'forms_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (179, 1, 1), (179, 2, 0), (179, 3, 1), (179, 4, 0), (179, 5, 1), (179, 6, 1);

INSERT INTO pages (page_id, page_name) VALUES (180, 'exams_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (180, 1, 1), (180, 2, 0), (180, 3, 1), (180, 4, 0), (180, 5, 1), (180, 6, 1);

-- Log that the current version of the database is 9, and it was added now.
INSERT INTO `database` (`version`) VALUES (9);