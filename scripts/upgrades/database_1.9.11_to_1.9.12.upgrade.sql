-- Renaming more pages
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   03/04/2014
-- Omis database upgrade script version 1.9.11 => 1.9.12

-- Page name assess/ajx_Osces => assess/ajx_exams
UPDATE `pages` SET `page_name` = 'ajx_exams' WHERE `page_id` = 5;

-- Page name assess/stat_conf => assess/exam_configuration
UPDATE `pages` SET `page_name` = 'exam_configuration' WHERE `page_id` = 129;

-- Page name assess/osce_test => assess/exam_assessment
UPDATE `pages` SET `page_name` = 'exam_assessment' WHERE `page_id` = 95;

-- Page name schools/ajx_crs => schools/ajx_courses
UPDATE `pages` SET `page_name` = 'ajx_courses' WHERE `page_id` = 12;

-- Page name schools/ajx_crsyrs => schools/ajx_years
UPDATE `pages` SET `page_name` = 'ajx_years' WHERE `page_id` = 13;

-- Page name schools/ajx_cymods => schools/ajx_modules
UPDATE `pages` SET `page_name` = 'ajx_modules' WHERE `page_id` = 14;

-- Page name schools/ajx_school => schools/ajx_schools
UPDATE `pages` SET `page_name` = 'ajx_schools' WHERE `page_id` = 25;

-- Page name schools/ajx_dept => schools/ajx_departments
UPDATE `pages` SET `page_name` = 'ajx_departments' WHERE `page_id` = 16;

-- Page name schools/man_crsyrs => schools/manage_years
UPDATE `pages` SET `page_name` = 'manage_years' WHERE `page_id` = 82;

-- Page name schools/man_crs => schools/manage_courses
UPDATE `pages` SET `page_name` = 'manage_courses' WHERE `page_id` = 81;

-- Page name schools/man_cymods => schools/manage_modules
UPDATE `pages` SET `page_name` = 'manage_modules' WHERE `page_id` = 83;

-- Page name schools/man_depts => schools/manage_departments
UPDATE `pages` SET `page_name` = 'manage_departments' WHERE `page_id` = 84;

-- Page name schools/man_schools => schools/manage_schools
UPDATE `pages` SET `page_name` = 'manage_schools' WHERE `page_id` = 88;

-- Log that the current version of the database is 12, and it was added now.
INSERT INTO `database` (`version`) VALUES (12);