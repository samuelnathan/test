-- Author: David Cunningham
-- Date: 30/10/2017
-- Version 1.9.106.2 => 1.9.107
-- Add new score weightings feature to Observe

ALTER TABLE `session_stations` 
ADD COLUMN `weighting` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `pass_value`;

ALTER TABLE `exam_rules` 
ADD COLUMN `grade_weightings` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `divergence_threshold`;

-- Log that the current version of the database is 107, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (107);