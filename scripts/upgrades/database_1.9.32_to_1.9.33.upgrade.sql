-- Upgrade script to add exam scheduling fields to exam sessions table 
-- refer to issue/ticket #165 in bitbucket. 
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   14/07/2014
-- Omis database upgrade script version 1.9.32 => 1.9.33

ALTER TABLE `exam_sessions` 
ADD COLUMN `start_time` TIME NULL DEFAULT '00:00:00' AFTER `session_date`,
ADD COLUMN `end_time` TIME NULL DEFAULT '00:00:00' AFTER `start_time`,
ADD COLUMN `google_calendar_event_id` VARCHAR(26) NULL AFTER `circuit_colour`;

-- Log that the current version of the database is 33, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (33);