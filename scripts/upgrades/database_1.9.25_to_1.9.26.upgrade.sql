-- Changing name of table storing version control information.
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   04/06/2014
-- Omis database upgrade script version 1.9.25 => 1.9.26

ALTER TABLE `database` RENAME TO  `database_versions`;

-- Log that the current version of the database is 26, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (26);
