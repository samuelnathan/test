-- Upgrade script for the GRS (Global Rating Scale) Management Admin Tool in Omis 1.9
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   11/10/2014
-- Omis database upgrade script version 1.9.36 => 1.9.37

-- Create Global Rating Scale pages
-- Only Accessible to Super Administrators

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (184, 'grs_types_tool');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (184, 1, 0), (184, 2, 0), (184, 3, 0), (184, 4, 0), (184, 5, 0), (184, 6, 1);

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (185, 'grs_values_tool');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (185, 1, 0), (185, 2, 0), (185, 3, 0), (185, 4, 0), (185, 5, 0), (185, 6, 1);

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (186, 'ajx_json_grs');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (186, 1, 0), (186, 2, 0), (186, 3, 0), (186, 4, 0), (186, 5, 0), (186, 6, 1);

-- Log that the current version of the database is 37, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (37);