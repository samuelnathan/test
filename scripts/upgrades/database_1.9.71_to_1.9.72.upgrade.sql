-- Author: David Cunningham
-- Date: 22/06/2016
-- OMIS database upgrade script version 1.9.71 => 1.9.72
-- Change all default timestamps from '0000-00-00 00:00:00' to NULL 

ALTER TABLE `api_tokens` 
CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ,
CHANGE COLUMN `expires_at` `expires_at` TIMESTAMP NULL ,
CHANGE COLUMN `revoked_at` `revoked_at` TIMESTAMP NULL COMMENT 'Time token was revoked, if any' ,
CHANGE COLUMN `deleted_at` `deleted_at` TIMESTAMP NULL ;

ALTER TABLE `examiner_training` 
CHANGE COLUMN `last_sent` `last_sent` TIMESTAMP NULL ;

ALTER TABLE `student_results` 
CHANGE COLUMN `first_submission` `first_submission` TIMESTAMP NULL COMMENT 'Time of first submission' ;

ALTER TABLE `student_feedback` 
CHANGE COLUMN `time_created` `time_created` TIMESTAMP NULL ,
CHANGE COLUMN `time_sent` `time_sent` TIMESTAMP NULL ;

ALTER TABLE `students` 
CHANGE COLUMN `stu_dob` `stu_dob` DATE NULL ;

ALTER TABLE `exam_sessions` 
CHANGE COLUMN `session_date` `session_date` DATE NULL ;

-- Log that the current version of the database is 72, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (72);
