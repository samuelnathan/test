-- Author: David Cunningham
-- Date: 21/08/2016
-- OMIS database upgrade script version 1.9.77 => 1.9.78
-- Add audit trail tables for tables student_results, item_scores and section_feedback
-- plus the triggers to populate them
-- PLEASE NOTE: the following tables uses TokuDB a high-performance storage engine for MySQL and MariaDB
-- It may need to be manually installed on the the MySQL server (plugin) if not done so already

DROP TABLE IF EXISTS `student_results_audit_trail`;
CREATE TABLE `student_results_audit_trail` (
  `result_id` bigint(10) NOT NULL,
  `station_id` bigint(10) NOT NULL DEFAULT '0',
  `stu_id` varchar(30) NOT NULL,
  `exmr_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `absent` int(1) NOT NULL DEFAULT '0',
  `score` decimal(9,3) NOT NULL DEFAULT '0.000',
  `first_submission` timestamp NULL DEFAULT NULL COMMENT 'Time of first submission',
  `is_complete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_updated` timestamp NULL DEFAULT NULL,
  `last_updated_child` timestamp NULL DEFAULT NULL,
  `result_uuid` varchar(36) DEFAULT NULL
) ENGINE=TokuDB DEFAULT CHARSET=utf8 COMMENT = 'Audit trail for student results table';


DROP TABLE IF EXISTS `item_scores_audit_trail`;
CREATE TABLE `item_scores_audit_trail` (
  `item_score_id` bigint(10) NOT NULL,
  `result_id` bigint(10) NOT NULL,
  `item_id` bigint(10) NOT NULL,
  `ans_option_id` bigint(10) DEFAULT NULL,
  `option_value` varchar(20) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=TokuDB DEFAULT CHARSET=utf8 COMMENT = 'Audit trail for item scores table';

DROP TABLE IF EXISTS `section_feedback_audit_trail`;
CREATE TABLE `section_feedback_audit_trail` (
  `feedback_id` bigint(10) NOT NULL,
  `section_id` bigint(10) NOT NULL,
  `result_id` bigint(10) NOT NULL,
  `feedback_text` text,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=TokuDB DEFAULT CHARSET=utf8 COMMENT = 'Audit trail for section feedback table';

DELIMITER $$

DROP TRIGGER IF EXISTS student_results_AFTER_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `student_results_AFTER_UPDATE` AFTER UPDATE ON `student_results` FOR EACH ROW
BEGIN
  IF NEW.`is_complete` = 1 THEN
     INSERT INTO student_results_audit_trail SELECT * FROM student_results WHERE `result_id` = NEW.`result_id`;
     INSERT INTO item_scores_audit_trail SELECT item_scores.item_score_id, item_scores.result_id, 
     item_scores.item_id, item_scores.ans_option_id, item_scores.option_value, item_scores.last_updated 
     FROM item_scores WHERE `result_id` = NEW.`result_id`;
     INSERT INTO section_feedback_audit_trail SELECT * FROM section_feedback WHERE `result_id` = NEW.`result_id`;
  END IF;
END
$$

DELIMITER ;

-- Log that the current version of the database is 78, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (78);
