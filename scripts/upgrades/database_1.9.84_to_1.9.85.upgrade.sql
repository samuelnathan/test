-- Author: Enda Phelan
-- Date: 18/11/2016
-- OMIS database upgrade script version 1.9.84 => 1.9.85
-- Add a flag to form section that dictates whether feedback should be sent to students or just internally

ALTER TABLE `form_sections`
ADD COLUMN `internal_feedback_only` TINYINT(1) UNSIGNED NOT NULL COMMENT 'If set to true, feedback will not be sent to students and will only be sent internally' AFTER `feedback_required`;

ALTER TABLE `form_sections`
CHANGE COLUMN `feedback_required` `feedback_required` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0=No, 1=Yes, 2=Fail, 3=GRS Borderline, 4=GRS Fail & Borderline, 5=Fail Flag Option';

ALTER TABLE `item_options`
ADD COLUMN `flag` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Flag item option, if set to 1 and clicked during assessment, it is highlighted and feedback may become compulsory' AFTER `ans_option_text`;

-- Log that the current version of the database is 85, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (85);
