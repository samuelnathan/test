-- Renaming more pages
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   03/04/2014
-- Omis database upgrade script version 1.9.12 => 1.9.13

-- Page name assess/ajx_exams => assess/ajx_assessment
UPDATE `pages` SET `page_name` = 'ajx_assessment' WHERE `page_id` = 22;

-- Log that the current version of the database is 13, and it was added now.
INSERT INTO `database` (`version`) VALUES (13);