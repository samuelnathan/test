-- Author: David Cunningham
-- Date:   30/11/2015
-- OMIS database upgrade script version 1.9.61 => 1.9.62
-- Add start and end date field to the academic term table
 
-- start and end date fields t terms tables
ALTER TABLE `terms` 
ADD COLUMN `start_date` DATE NULL COMMENT 'The start date for the academic term' AFTER `current_term`,
ADD COLUMN `end_date` DATE NULL COMMENT 'The end date for the academic term' AFTER `start_date`, 
COMMENT = 'School Academic Term';

-- Update page names
UPDATE `pages` SET `page_name`='manage_terms' WHERE `page_id`='89';
UPDATE `pages` SET `page_name`='ajx_terms' WHERE `page_id`='34';
UPDATE `pages` SET `page_name`='terms_redirect' WHERE `page_id`='170';

-- The update that follows requires safe mode to be switched off
SET SQL_SAFE_UPDATES = 0;

-- Give at least the current term a default start and end date
UPDATE `terms` SET `start_date`='2015-09-01', `end_date`='2016-08-31' WHERE `current_term` = '1';

-- Go switch safe mode back on
SET SQL_SAFE_UPDATES = 1;

-- Log that the current version of the database is 62, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (62);