-- Author: David Cunningham
-- Date: 25/08/2018
-- Version 1.9.113.0 => 1.9.113.1

ALTER TABLE `database_versions`
  CHANGE COLUMN `version` `major_version` INT(11) NOT NULL;

ALTER TABLE `database_versions`
  ADD COLUMN `minor_version` INT(11) NOT NULL DEFAULT 0 AFTER `major_version`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`major_version`, `minor_version`);

-- Improved table field naming across database
ALTER TABLE `students`
CHANGE COLUMN `stu_image` `student_image` VARCHAR(80) NULL DEFAULT '' AFTER `nationality`,
CHANGE COLUMN `stu_id` `student_id` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `stu_fname` `forename` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `stu_surname` `surname` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `stu_dob` `dob` DATE NULL DEFAULT NULL ,
CHANGE COLUMN `stu_gender` `gender` CHAR(1) NULL DEFAULT NULL ,
CHANGE COLUMN `stu_email` `email` VARCHAR(200) NULL DEFAULT NULL ;

ALTER TABLE student_courses
DROP FOREIGN KEY `module_student_fk`,
DROP FOREIGN KEY `studentmodule_module_fk`,
DROP FOREIGN KEY `studentmodule_year_fk`;
ALTER TABLE `student_courses`
CHANGE COLUMN `stu_id` `student_id` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `crs_yr_id` `year_id` BIGINT(10) NOT NULL ,
CHANGE COLUMN `mod_id` `module_id` VARCHAR(35) NOT NULL DEFAULT '' ;
ALTER TABLE `student_courses`
ADD CONSTRAINT `module_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `studentmodule_module_fk`
  FOREIGN KEY (`module_id`)
  REFERENCES `modules` (`mod_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `studentmodule_year_fk`
  FOREIGN KEY (`year_id`)
  REFERENCES `course_years` (`crs_yr_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `courses`
  CHANGE COLUMN `crs_id` `course_id` VARCHAR(20) NOT NULL ,
  CHANGE COLUMN `crs_fullname` `course_name` TEXT NULL DEFAULT NULL;

ALTER TABLE `course_years`
DROP FOREIGN KEY `year_course_fk`;
ALTER TABLE `course_years`
CHANGE COLUMN `crs_yr_id` `year_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `crs_id` `course_id` VARCHAR(20) NOT NULL ,
CHANGE COLUMN `crs_year` `year_name` VARCHAR(20) NOT NULL DEFAULT '0' ;
ALTER TABLE `course_years`
ADD CONSTRAINT `year_course_fk`
  FOREIGN KEY (`course_id`)
  REFERENCES `courses` (`course_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `modules`
CHANGE COLUMN `mod_id` `module_id` VARCHAR(35) NOT NULL DEFAULT '' ,
CHANGE COLUMN `mod_name` `module_name` TEXT NULL DEFAULT NULL ;

ALTER TABLE `candidate_numbers`
DROP FOREIGN KEY `candidatenumber_student_fk`;
ALTER TABLE `candidate_numbers`
CHANGE COLUMN `stu_id` `student_id` VARCHAR(30) NOT NULL ;
ALTER TABLE `candidate_numbers`
ADD CONSTRAINT `candidatenumber_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `self_assessment_responses`
DROP FOREIGN KEY `responses_students_stu_id_fk`;
ALTER TABLE `self_assessment_responses`
DROP INDEX `responses_students_stu_id_fk` ,
ADD INDEX `responses_students_student_id_fk` (`student_id` ASC);
;
ALTER TABLE `self_assessment_responses`
ADD CONSTRAINT `responses_students_student_id_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `self_assessment_ownerships`
DROP FOREIGN KEY `self_assessment_ownership_students_stu_id_fk`;
ALTER TABLE `self_assessment_ownerships`
DROP INDEX `self_assessment_ownership_students_stu_id_fk` ,
ADD INDEX `self_assessment_ownership_students_student_id_fk` (`student_id` ASC);
;
ALTER TABLE `self_assessment_ownerships`
ADD CONSTRAINT `self_assessment_ownership_students_student_id_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `student_feedback`
DROP FOREIGN KEY `feedback_student_fk`;
ALTER TABLE `student_feedback`
CHANGE COLUMN `stu_id` `student_id` VARCHAR(30) NOT NULL ;
ALTER TABLE `student_feedback`
ADD CONSTRAINT `feedback_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

  ALTER TABLE `student_results`
DROP FOREIGN KEY `result_student_fk`;
ALTER TABLE `student_results`
CHANGE COLUMN `stu_id` `student_id` VARCHAR(30) NOT NULL ;
ALTER TABLE `student_results`
ADD CONSTRAINT `result_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `year_modules`
DROP FOREIGN KEY `yearmodule_module_fk`,
DROP FOREIGN KEY `yearmodule_year_fk`;
ALTER TABLE `year_modules`
CHANGE COLUMN `crs_yr_id` `year_id` BIGINT(10) NOT NULL ,
CHANGE COLUMN `mod_id` `module_id` VARCHAR(35) NOT NULL DEFAULT '' ;
ALTER TABLE `year_modules`
ADD CONSTRAINT `yearmodule_module_fk`
  FOREIGN KEY (`module_id`)
  REFERENCES `modules` (`module_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `yearmodule_year_fk`
  FOREIGN KEY (`year_id`)
  REFERENCES `course_years` (`year_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

DROP TABLE `api_tokens`;

ALTER TABLE `blank_students`
CHANGE COLUMN `blank_stud_id` `blank_student_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `stu_order` `student_order` INT(10) NULL DEFAULT NULL ;

ALTER TABLE `blank_students_discarded`
DROP FOREIGN KEY `discarded_blank_student_fk`;
ALTER TABLE `blank_students_discarded`
CHANGE COLUMN `blank_stud_id` `blank_student_id` BIGINT(10) NOT NULL ;
ALTER TABLE `blank_students_discarded`
ADD CONSTRAINT `discarded_blank_student_fk`
  FOREIGN KEY (`blank_student_id`)
  REFERENCES `blank_students` (`blank_student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `group_students`
DROP FOREIGN KEY `groupstudent_student_fk`;
ALTER TABLE `group_students`
CHANGE COLUMN `stu_id` `student_id` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `stu_order` `student_order` INT(10) NULL DEFAULT NULL ;
ALTER TABLE `group_students`
ADD CONSTRAINT `groupstudent_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `users`
CHANGE COLUMN `sname` `surname` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `fname` `forename` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `user_password` `password` VARCHAR(255) NOT NULL ,
CHANGE COLUMN `contact_num` `contact_number` VARCHAR(15) NULL DEFAULT NULL ,
CHANGE COLUMN `acc_status` `activated` INT(1) NOT NULL DEFAULT '0' ;

ALTER TABLE `student_results`
CHANGE COLUMN `exmr_id` `examiner_id` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ;

ALTER TABLE `section_items`
CHANGE COLUMN `item_desc` `text` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `item_ans_type` `type` VARCHAR(20) NULL DEFAULT NULL ,
CHANGE COLUMN `highest_item_value` `highest_value` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ,
CHANGE COLUMN `lowest_item_value` `lowest_value` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ;

ALTER TABLE `item_options`
CHANGE COLUMN `ans_option_id` `option_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `ans_option_value` `option_value` VARCHAR(20) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `ans_option_order`    `option_order` BIGINT(10) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `ans_option_text` `descriptor` TEXT NULL DEFAULT NULL ;

ALTER TABLE `session_stations`
CHANGE COLUMN `station_num` `station_number` BIGINT(10) NOT NULL DEFAULT '0' ;

ALTER TABLE `blank_students_discarded`
CHANGE COLUMN `station_num` `station_number` BIGINT(10) NOT NULL DEFAULT '0' ;

ALTER TABLE `form_sections`
CHANGE COLUMN `additional_info` `section_text` TEXT NULL DEFAULT NULL ;

ALTER TABLE `item_scores`
CHANGE COLUMN `item_score_id` `score_id` BIGINT(10) NOT NULL AUTO_INCREMENT ,
CHANGE COLUMN `ans_option_id` `option_id` BIGINT(10) NULL DEFAULT NULL ;

ALTER TABLE `exams`
DROP COLUMN `last_updated_child`;

ALTER TABLE `exam_sessions`
DROP COLUMN `last_updated_child`;

ALTER TABLE `forms`
DROP COLUMN `last_updated_child`;

ALTER TABLE `form_sections`
DROP COLUMN `last_updated_child`;

ALTER TABLE `section_items` 
DROP COLUMN `last_updated_child`;

ALTER TABLE `session_groups` 
DROP COLUMN `last_updated_child`;

ALTER TABLE `student_results` 
DROP COLUMN `last_updated_child`;

SET @original_studentdata_definer = (SELECT DEFINER FROM mysql.proc WHERE db=database() AND NAME = "GetStudentData");
SET @original_feedbackdata_definer = (SELECT DEFINER FROM mysql.proc WHERE db=database() AND NAME = "SetFeedbackStatusSent");

-- Recode the stored procedures and keep existing definer
DELIMITER ;;

DROP PROCEDURE GetStudentData ;;
CREATE PROCEDURE `GetStudentData`()
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
SELECT * FROM students s
        inner join student_feedback sf
        on s.student_id = sf.student_id
        inner join email_templates ft
        on ft.template_id = sf.template_id
        where sf.is_sent = 0 
        AND sf.is_created = 1
        AND sf.is_ready = 1;
COMMIT;
        END ;;

DROP PROCEDURE SetFeedbackStatusSent ;;
CREATE  PROCEDURE `SetFeedbackStatusSent`(
        IN student_id varchar(30),
        IN osce_id bigint(10),
        IN time_sent datetime)
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;             
                        Update student_feedback 
                        Set 
                        is_sent = 1,
                        time_sent = time_sent
        where student_id = student_id
        AND exam_id = osce_id;
COMMIT;			    
END ;;

DELIMITER ;

UPDATE mysql.proc SET definer = @original_studentdata_definer WHERE db=database() AND NAME = "GetStudentData";
UPDATE mysql.proc SET definer = @original_feedbackdata_definer  WHERE db=database() AND NAME = "SetFeedbackStatusSent";

-- Triggers tidy up
ALTER TABLE `schools`
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

DROP TRIGGER IF EXISTS departments_BEFORE_UPDATE;
ALTER TABLE `departments` 
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `departments`
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER school_id; 

DROP TRIGGER IF EXISTS exams_BEFORE_UPDATE;

ALTER TABLE `exams` 
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `exams` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER grade_rule; 

DROP TRIGGER IF EXISTS exam_sessions_BEFORE_DELETE;
DROP TRIGGER IF EXISTS exam_sessions_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS exam_sessions_AFTER_INSERT;
DROP TRIGGER IF EXISTS exam_sessions_AFTER_UPDATE;

ALTER TABLE `exam_sessions` 
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `exam_sessions` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER circuit_colour; 

ALTER TABLE `exam_settings` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `exam_rules` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `exam_notifications_sms` 
CHANGE COLUMN `sms_timestamp` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `exam_notifications_sms` 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP; 

ALTER TABLE `exam_notifications` 
CHANGE COLUMN `notification_timestamp` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `exam_notifications`
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER divergence_threshold; 

ALTER TABLE `feature_roles` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `form_department_term` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

DROP TRIGGER IF EXISTS forms_BEFORE_UPDATE;

ALTER TABLE `forms` 
CHANGE COLUMN `created` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS form_sections_BEFORE_DELETE;
DROP TRIGGER IF EXISTS form_sections_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS form_sections_AFTER_INSERT;
DROP TRIGGER IF EXISTS form_sections_AFTER_UPDATE;

ALTER TABLE `form_sections` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `internal_feedback_only`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `item_competencies` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

DROP TRIGGER IF EXISTS item_options_BEFORE_DELETE;
DROP TRIGGER IF EXISTS item_options_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS item_options_AFTER_INSERT;
DROP TRIGGER IF EXISTS item_options_AFTER_UPDATE;

ALTER TABLE `item_options` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `fail`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `item_scores` 
DROP COLUMN `update_uuid`,
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `option_value`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS item_scores_BEFORE_INSERT;
DROP TRIGGER IF EXISTS item_scores_AFTER_INSERT;
DROP TRIGGER IF EXISTS item_scores_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS item_scores_AFTER_UPDATE;
DROP TRIGGER IF EXISTS item_scores_BEFORE_DELETE;

ALTER TABLE `school_admins` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `section_feedback` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `feedback_text`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS section_feedback_BEFORE_DELETE;
DROP TRIGGER IF EXISTS section_feedback_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS section_feedback_AFTER_INSERT;
DROP TRIGGER IF EXISTS section_feedback_AFTER_UPDATE; 

ALTER TABLE `section_items` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lowest_value`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS section_items_BEFORE_DELETE;
DROP TRIGGER IF EXISTS section_items_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS section_items_AFTER_INSERT;
DROP TRIGGER IF EXISTS section_items_AFTER_UPDATE; 

ALTER TABLE `self_assessment_ownerships` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `self_assessment_questions` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `self_assessment_responses` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `self_assessments`
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

DROP TRIGGER IF EXISTS session_groups_BEFORE_DELETE;
DROP TRIGGER IF EXISTS session_groups_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS session_groups_AFTER_INSERT;
DROP TRIGGER IF EXISTS session_groups_AFTER_UPDATE; 

ALTER TABLE `form_tags` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `session_groups` 
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `session_groups` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER group_order; 

DROP TRIGGER IF EXISTS group_students_BEFORE_DELETE;
DROP TRIGGER IF EXISTS group_students_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS group_students_AFTER_INSERT;
DROP TRIGGER IF EXISTS group_students_AFTER_UPDATE;

ALTER TABLE `group_students` 
DROP COLUMN `last_updated_child`,
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `student_order`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;


DROP TRIGGER IF EXISTS session_stations_BEFORE_DELETE;
DROP TRIGGER IF EXISTS session_stations_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS session_stations_AFTER_INSERT;
DROP TRIGGER IF EXISTS session_stations_AFTER_UPDATE;

ALTER TABLE `session_stations` 
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `exclude_from_grading` ;

ALTER TABLE `session_stations` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `exclude_from_grading` ; 

ALTER TABLE `courses` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `course_years` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `student_courses` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `modules` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `grade_rules` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `grade_values` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `session_assistants` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `sms_messages` 
CHANGE COLUMN `time_sent` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

ALTER TABLE `sms_messages` 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `sms_message_recipients` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `station_examiners` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `student_exam_data` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `student_feedback` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `student_results` 
DROP COLUMN `result_uuid`,
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `flag_count`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS student_results_BEFORE_INSERT;
DROP TRIGGER IF EXISTS student_results_BEFORE_UPDATE;

ALTER TABLE `students` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `student_image`,
CHANGE COLUMN `student_image` `student_image` VARCHAR(80) NULL DEFAULT '' AFTER `nationality`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS students_BEFORE_DELETE;
DROP TRIGGER IF EXISTS students_BEFORE_UPDATE;
DROP TRIGGER IF EXISTS students_AFTER_INSERT;
DROP TRIGGER IF EXISTS students_AFTER_UPDATE;

ALTER TABLE `tags` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `terms` 
CHANGE COLUMN `created` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

DROP TRIGGER IF EXISTS terms_BEFORE_UPDATE;

ALTER TABLE `user_presets` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

DROP TRIGGER IF EXISTS users_BEFORE_UPDATE;

ALTER TABLE `users` 
CHANGE COLUMN `created` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `last_active`,
CHANGE COLUMN `last_updated` `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP ;

ALTER TABLE `year_modules` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `blank_students` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `blank_students_discarded` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `candidate_numbers` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `email_templates` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `exam_competency_rules` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `examiner_department_term` 
ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Migrate timestamps overs to new 'created_at' field and drop the old 'first_submission' timestamp

SET SQL_SAFE_UPDATES = 0;
UPDATE student_results SET created_at = first_submission;
SET SQL_SAFE_UPDATES = 1;

ALTER TABLE `student_results`
DROP COLUMN `first_submission`;

-- Log that the current version of the database is 113, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (113, 1);
