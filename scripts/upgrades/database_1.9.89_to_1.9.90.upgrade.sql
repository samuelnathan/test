-- Author: David Cunningham
-- Date: 01/12/2016
-- OMIS database upgrade script version 1.9.89 => 1.9.90
-- Adds new 'student_competencies' page for analysis section
-- Adds new 'exam_competency_rules' table for analysis section

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (197, 'student_competencies');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (197, 1, 1), (197, 2, 0), (197, 3, 1), (197, 4, 0), (197, 5, 1), (197, 6, 1);

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (198, 'export_competencies');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (198, 1, 1), (198, 2, 0), (198, 3, 1), (198, 4, 0), (198, 5, 1), (198, 6, 1);

CREATE TABLE `exam_competency_rules` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier per exam competency rule',
  `exam_id` BIGINT(10) NOT NULL COMMENT 'examination identifier that links back to exams table',
  `competency_id` BIGINT(10) UNSIGNED NOT NULL COMMENT 'Competency identifier that links back to the competency_framework_levels table',
  `pass` DECIMAL(9,3) NOT NULL DEFAULT '0.000' COMMENT 'Pass value (%) per competency in exam',
  PRIMARY KEY (`id`, `exam_id`, `competency_id`),
  INDEX `competencyrule_framework_idx` (`competency_id` ASC),
  INDEX `competencyrule_exam_idx` (`exam_id` ASC),
  CONSTRAINT `competencyrule_framework_fk`
    FOREIGN KEY (`competency_id`)
    REFERENCES `competency_framework_levels` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `competencyrule_exam`
    FOREIGN KEY (`exam_id`)
    REFERENCES `exams` (`exam_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Rules per competency in an exam';

-- Log that the current version of the database is 90, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (90);
