-- Author: David Cunningham
-- Date: 25/04/2018
-- Version 1.9.111 => 1.9.112

  -- new page
  INSERT IGNORE INTO `pages` (`page_name`) VALUES ('ajx_json_stations');

  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,1,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,2,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,3,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,4,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,5,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,6,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (212,7,1);

  -- new page
  INSERT IGNORE INTO `pages` (`page_name`) VALUES ('ajx_json_forms');

  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,1,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,2,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,3,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,4,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,5,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,6,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (213,7,1);

  -- New grading feature
  CREATE TABLE `grade_rules` (
  `grade_rule_id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'The grade rule identifier',
  `grade_rule_name` TEXT NOT NULL COMMENT 'The grade rule name',
  `grade_rule_description` TEXT NOT NULL COMMENT 'The grade rule description',
  PRIMARY KEY (`grade_rule_id`))
  ENGINE = InnoDB 
  COMMENT = 'This tables contains the grade rules that can be applied to particular exam sessions (circuit/day) and down to specific stations';
  
  ALTER TABLE `grade_rules` 
  ADD COLUMN `grade_rule_short` VARCHAR(50) NOT NULL COMMENT 'The grade rule short name' AFTER `grade_rule_name`;

  CREATE TABLE `grade_values` (
  `grade_value_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Grade value unique identifier',
  `grade_rule_id` BIGINT(10) UNSIGNED NOT NULL COMMENT 'grade_rules table foreign key',
  `grade_value` VARCHAR(40) NULL COMMENT 'Grade value e.g. Pass/Fail or A, B, C, D',
  PRIMARY KEY (`grade_value_id`))
  ENGINE = InnoDB
  COMMENT = 'Grade values e.g. Pass/Fail or A, B, C, D';
  
  ALTER TABLE `grade_values` 
  ADD INDEX `fk_grade_values_rules_idx` (`grade_rule_id` ASC);
  ALTER TABLE `grade_values` 
  ADD CONSTRAINT `fk_grade_values_rules`
    FOREIGN KEY (`grade_rule_id`)
    REFERENCES `grade_rules` (`grade_rule_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

  INSERT INTO `grade_rules` (`grade_rule_id`,  `grade_rule_name`, `grade_rule_description`) VALUES ('1',  'default', 'Standard pass/fail based on accumulated pass mark set of stations assessed');
  INSERT INTO `grade_rules` (`grade_rule_id`,  `grade_rule_name`, `grade_rule_description`) VALUES ('2',  'no more than three scores of a 2', 'No score lower than 3 => Appointable\nAny score of 1 => Not appointable\nFour or more scores of a 2 => Not appointable\nThree or fewer scores of a 2 => Panel to decide');
  INSERT INTO `grade_rules` (`grade_rule_id`,  `grade_rule_name`, `grade_rule_description`) VALUES ('3',  'no more than two scores of a 2', 'No score lower than 3 => Appointable\nAny score of 1 => Not appointable\nThree or more scores of a 2 =>  Not appointable\nOne or two scores of a 2 => Panel to decide');
  INSERT INTO `grade_rules` (`grade_rule_id`,  `grade_rule_name`, `grade_rule_description`) VALUES ('4',  'based on score only', 'No score lower than 3 => Appointable\nRaw interview score less than 36 => Not appointable\nAny score of 1 => Not appointable\nThree or more scores of a 2 => Not appointable\nUp to two scores of 2 and raw score 36 or more => Appointable');

  UPDATE `grade_rules` SET `grade_rule_short`='NO' WHERE `grade_rule_id`='1';
  UPDATE `grade_rules` SET `grade_rule_short`='&le; 3 x 2' WHERE `grade_rule_id`='2';
  UPDATE `grade_rules` SET `grade_rule_short`='&le; 2 x 2' WHERE `grade_rule_id`='3';
  UPDATE `grade_rules` SET `grade_rule_short`='SCORE ONLY' WHERE `grade_rule_id`='4';

  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('1', 'Fail');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('1', 'Pass');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('2', 'Appointable');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('2', 'Not appointable');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('2', 'Panel to decide');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('3', 'Appointable');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('3', 'Not appointable');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('3', 'Panel to decide');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('4', 'Appointable');
  INSERT INTO `grade_values` (`grade_rule_id`, `grade_value`) VALUES ('4', 'Not appointable');

  ALTER TABLE `exam_sessions`
  DROP COLUMN `google_calendar_event_id`;

  ALTER TABLE `exams`
  ADD COLUMN `grade_rule` BIGINT(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Link to grade_rules table' AFTER `show_countdown`,
  ADD INDEX `exam_grade_rule_fk_idx` (`grade_rule` ASC);
  ALTER TABLE `exams`
  ADD CONSTRAINT `exam_grade_rule_fk`
    FOREIGN KEY (`grade_rule`)
    REFERENCES `grade_rules` (`grade_rule_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE;
  
  ALTER TABLE `session_stations` 
  ADD COLUMN `exclude_from_grading` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Exclude this station and linked scoresheet from appointability or grading rules' AFTER `rest_station`;

  -- Student grade data
  ALTER TABLE `student_exam_data` 
  ADD COLUMN `grade` TEXT NULL DEFAULT NULL AFTER `notes_updated`,
  ADD COLUMN `grade_author` VARCHAR(45) NULL DEFAULT NULL AFTER `grade`,
  ADD COLUMN `grade_trigger` TEXT NULL DEFAULT NULL AFTER `grade_author`,
  ADD COLUMN `grade_updated` TIMESTAMP NULL DEFAULT NULL AFTER `grade_trigger`;
  
  -- Remove no longer required session scheduler as feature to enable and disable
  DELETE FROM `features` WHERE `feature_id`='session-scheduler';  

  -- Add field to capture the last set of scores combined to determine the grade/appointability
  ALTER TABLE `student_exam_data`
  ADD COLUMN `grade_scores_string` TEXT NULL DEFAULT NULL COMMENT 'Snapshot of last set of scores combined to determine the grade/appointability' AFTER `grade_trigger`;

  ALTER TABLE self_assessment_ownerships
  DROP FOREIGN KEY `self_assessment_ownership_users_user_id_fk`;
  ALTER TABLE self_assessment_ownerships
  ADD CONSTRAINT `self_assessment_ownership_users_user_id_fk`
    FOREIGN KEY (`examiner_id`)
    REFERENCES users (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE;

  -- Log that the current version of the database is 112, and it was added now.
  INSERT IGNORE INTO `database_versions` (`version`) VALUES (112);
