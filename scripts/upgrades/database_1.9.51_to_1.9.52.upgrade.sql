-- Author: David Cunningham
-- Date:   27/08/2015
-- Omis database upgrade script version 1.9.51 => 1.9.52
-- Make sure that all fields have a default value, null or otherwise
-- (Similar to upgrade script 1.9.49 => 1.9.50)

ALTER TABLE `api_tokens`
CHANGE COLUMN `token_uuid` `token_uuid` CHAR(36) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
COMMENT 'Unique ID for token (128bit GUID)';

ALTER TABLE `blank_students`
CHANGE COLUMN `stu_order` `stu_order` INT(10) NULL ;

ALTER TABLE `blank_students_discarded`
CHANGE COLUMN `station_num` `station_num` BIGINT(10) NULL ;

ALTER TABLE `email_templates`
CHANGE COLUMN `template_name` `template_name` VARCHAR(40) NULL ;

ALTER TABLE `form_sections`
CHANGE COLUMN `additional_info` `additional_info` TEXT NULL ;

ALTER TABLE `forms`
CHANGE COLUMN `author` `author` VARCHAR(20) NULL ;

ALTER TABLE `group_students`
CHANGE COLUMN `stu_order` `stu_order` INT(10) NULL ;

ALTER TABLE `iso_country_codes`
CHANGE COLUMN `iso_name` `iso_name` VARCHAR(80) NULL ,
CHANGE COLUMN `printable_name` `printable_name` VARCHAR(80) NULL ;

ALTER TABLE `item_scores`
CHANGE COLUMN `ans_option_id` `ans_option_id` BIGINT(10) NULL ;

ALTER TABLE `login_tracking`
CHANGE COLUMN `ip` `ip` VARCHAR(39) NULL ,
CHANGE COLUMN `attempts` `attempts` INT(11) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `last_login` `last_login` DATETIME NULL ;

ALTER TABLE `pages`
CHANGE COLUMN `page_name` `page_name` VARCHAR(45) NULL ;

ALTER TABLE `rating_scale_defaults`
CHANGE COLUMN `rsd_iso` `rsd_iso` VARCHAR(2) NULL ,
CHANGE COLUMN `rcd_lang` `rcd_lang` VARCHAR(50) NULL ;

ALTER TABLE `roles`
CHANGE COLUMN `role_name` `role_name` VARCHAR(80) NULL ,
CHANGE COLUMN `role_shortname` `role_shortname` VARCHAR(45) NULL ,
CHANGE COLUMN `role_importexport_field` `role_importexport_field` VARCHAR(45) NULL ;

ALTER TABLE `section_items`
CHANGE COLUMN `item_ans_type` `item_ans_type` VARCHAR(20) NULL ;

ALTER TABLE `session_stations`
CHANGE COLUMN `station_tag` `station_tag` VARCHAR(100) NULL COMMENT 'Group stations by tag' ,
CHANGE COLUMN `station_order` `station_order` INT(10) NULL ;

ALTER TABLE `sms_message_recipients`
CHANGE COLUMN `mobile_num` `mobile_num` VARCHAR(15) NULL ;

ALTER TABLE `sms_messages`
CHANGE COLUMN `sent_from` `sent_from` VARCHAR(30) NULL ;

ALTER TABLE `student_results`
CHANGE COLUMN `exmr_id` `exmr_id` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ;

ALTER TABLE `students`
CHANGE COLUMN `stu_gender` `stu_gender` CHAR(1) NULL ,
CHANGE COLUMN `stu_email` `stu_email` VARCHAR(200) NULL ,
CHANGE COLUMN `nationality` `nationality` CHAR(2) NULL ;

ALTER TABLE `users`
CHANGE COLUMN `email` `email` VARCHAR(200) NULL ,
CHANGE COLUMN `contact_num` `contact_num` VARCHAR(15) NULL ,
CHANGE COLUMN `pref_dept` `pref_dept` VARCHAR(20) NULL ,
CHANGE COLUMN `pref_school` `pref_school` VARCHAR(20) NULL ,
CHANGE COLUMN `pref_course` `pref_course` VARCHAR(20) NULL ,
CHANGE COLUMN `pref_module` `pref_module` VARCHAR(20) NULL ;

ALTER TABLE `weighting`
CHANGE COLUMN `weight_type` `weight_type` VARCHAR(30) NULL ;

-- Log that the current version of the database is 52, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (52);
