/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 26/02/2014
   © 2014 Qpercom Limited. All rights reserved.
   Omis database upgrade script version 1.9.2 => 1.9.3
   Rename field 'session_station_id' in table 'session_stations' to 'station_id' 
   and field 'form_section_id' in table 'form_sections' to 'section_id'
 */

/* 
  Drop foreign keys linked to 'session_station_id' field in table 'session_stations' 
*/
ALTER TABLE station_examiners DROP FOREIGN KEY examiner_station_fk;
ALTER TABLE student_results DROP FOREIGN KEY result_station_fk;

/* 
  Drop foreign keys linked to 'form_section_id' field in table 'form_sections' 
*/
ALTER TABLE section_items DROP FOREIGN KEY item_section_fk;
ALTER TABLE section_feedback DROP FOREIGN KEY feedback_section_fk;

/* 
  Rename field 'session_station_id' in table 'session_stations' to 'station_id'
*/
ALTER TABLE session_stations
CHANGE COLUMN session_station_id station_id BIGINT(10) NOT NULL AUTO_INCREMENT;

/* 
  Rename field 'session_station_id' in table 'station_examiners' to 'station_id'
*/
ALTER TABLE station_examiners
CHANGE COLUMN session_station_id station_id BIGINT(10) NOT NULL;

/* 
  Rename field 'session_station_id' in table 'student_results' to 'station_id'
*/
ALTER TABLE student_results
CHANGE COLUMN session_station_id station_id BIGINT(10) NOT NULL,
DROP INDEX session_station_id,
ADD INDEX station_id (station_id ASC);

/* 
  Rename field 'form_section_id' in table 'form_sections' to 'section_id'
*/
ALTER TABLE form_sections
CHANGE COLUMN form_section_id section_id BIGINT(10) NOT NULL AUTO_INCREMENT;

/* 
  Rename field 'form_section_id' in table 'section_items' to 'section_id'
*/
ALTER TABLE section_items
CHANGE COLUMN form_section_id section_id BIGINT(10) NOT NULL,
DROP INDEX form_section_id,
ADD INDEX section_id (section_id ASC);

/* 
  Rename field 'form_section_id' in table 'section_feedback' to 'section_id'
*/
ALTER TABLE section_feedback
CHANGE COLUMN form_section_id section_id BIGINT(10) NOT NULL;

/* 
  Restore foreign keys
*/
ALTER TABLE station_examiners ADD CONSTRAINT examiner_station_fk FOREIGN KEY (station_id) REFERENCES session_stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE student_results ADD CONSTRAINT result_station_fk FOREIGN KEY (station_id) REFERENCES session_stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE section_items ADD CONSTRAINT item_section_fk FOREIGN KEY (section_id) REFERENCES form_sections(section_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE section_feedback ADD CONSTRAINT feedback_section_fk FOREIGN KEY (section_id) REFERENCES form_sections(section_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE section_feedback ADD CONSTRAINT feedback_result_fk FOREIGN KEY (result_id) REFERENCES student_results(result_id) ON DELETE CASCADE ON UPDATE CASCADE;

-- Log that the current version of the database is 3, and it was added now.
INSERT INTO `database` (`version`) VALUES (3);