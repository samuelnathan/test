-- Author: David Cunningham
-- Date: 12/05/2016
-- OMIS database upgrade script version 1.9.70 => 1.9.71

-- Adds the swap tool to the feature list, new feature group 'Admin Tools'
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES (5, 'Admin Tools');
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("swap-tool", "Swap Tool", 5);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("swap-tool", 1, 0), ("swap-tool", 2, 0), ("swap-tool", 3, 0), ("swap-tool", 5, 0);

-- Moves the station import/export tool and the GRS management tool to the new admin tools feature group
UPDATE `features` SET `feature_group_id`='5' WHERE `feature_id`='grs-management';
UPDATE `features` SET `feature_group_id`='5' WHERE `feature_id`='import-export-forms';

-- Log that the current version of the database is 71, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (71);

