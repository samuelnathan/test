-- Add new features and feature groups table
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   01/04/2015
-- Omis database upgrade script version 1.9.42 => 1.9.43

-- Create feature groups table
CREATE TABLE `feature_groups` (
  `feature_group_id` TINYINT(3) UNSIGNED NOT NULL COMMENT 'Group ID',
  `feature_group_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL COMMENT 'Group name',
  PRIMARY KEY (`feature_group_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Table of group names for system features';

-- Fill feature groups table with group names
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('1', 'Exam Administration');
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('2', 'Analysis');
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('3', 'Reporting');
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('4', 'Feedback');

-- Add new group field to features table
ALTER TABLE `features` 
ADD COLUMN `feature_group_id` TINYINT(3) UNSIGNED NOT NULL COMMENT 'Link to feature_groups table' AFTER `feature_desc`;

-- Update new field with group IDs, linking features to the groups table 
UPDATE `features` SET `feature_group_id`='1' WHERE `feature_id`='advancedsettings';
UPDATE `features` SET `feature_group_id`='2' WHERE `feature_id`='bgm';
UPDATE `features` SET `feature_group_id`='3' WHERE `feature_id`='bse';
UPDATE `features` SET `feature_group_id`='3' WHERE `feature_id`='cba';
UPDATE `features` SET `feature_desc`='Cronbach''s Alpha Analysis', `feature_group_id`='2' WHERE `feature_id`='cronbach';
UPDATE `features` SET `feature_desc`='Delete Results', `feature_group_id`='2' WHERE `feature_id`='dr1';
UPDATE `features` SET `feature_desc`='Examiner Feedback Column', `feature_group_id`='2' WHERE `feature_id`='efc';
UPDATE `features` SET `feature_group_id`='1' WHERE `feature_id`='eis';
UPDATE `features` SET `feature_group_id`='1' WHERE `feature_id`='ess';
UPDATE `features` SET `feature_group_id`='4' WHERE `feature_id`='fc';
UPDATE `features` SET `feature_desc`='Global Rating Scale Column ', `feature_group_id`='2' WHERE `feature_id`='grs';
UPDATE `features` SET `feature_desc`='PDF Assessment Forms', `feature_group_id`='1' WHERE `feature_id`='po1';
UPDATE `features` SET `feature_group_id`='1' WHERE `feature_id`='sd';
UPDATE `features` SET `feature_group_id`='3' WHERE `feature_id`='seh';
UPDATE `features` SET `feature_group_id`='4' WHERE `feature_id`='sfs';

-- Add foreign key linking both tables using the 'feature_group_id'
ALTER TABLE `features` 
ADD INDEX `feature_group_fk_idx` (`feature_group_id` ASC);
ALTER TABLE `features` 
ADD CONSTRAINT `feature_group_fk`
  FOREIGN KEY (`feature_group_id`)
  REFERENCES `feature_groups` (`feature_group_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Change page name from 'man_fetr' to 'manage_features' 
UPDATE `pages` SET `page_name`='manage_features' WHERE `page_id`='87';

-- Change the terminology: 'levels' to 'roles'
ALTER TABLE `feature_levels` 
CHANGE COLUMN `feature_level` `feature_role` TINYINT(3) UNSIGNED NOT NULL ,
CHANGE COLUMN `level_enabled` `role_enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' , 
COMMENT = 'The user roles that can access the feature' , RENAME TO `feature_roles`;

ALTER TABLE `features` 
CHANGE COLUMN `feature_desc` `feature_description` TEXT NOT NULL ;

-- [Add outstanding new features for 1.9]
-- Multiple examiners
   INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("multi-examiners", "Multiple Examiners", 1);
   INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES 
   ("multi-examiners", 1, 0), ("multi-examiners", 2, 0), ("multi-examiners", 3, 0), ("multi-examiners", 5, 0);

-- Student images
   INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("student-images", "Student Profile Images", 1);
   INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES 
   ("student-images", 1, 0), ("student-images", 2, 0), ("student-images", 3, 0), ("student-images", 5, 0);

-- Global Rating Scale management (enable for admins)
   INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("grs-management", "Global Rating Scale Management", 2);
   INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES 
   ("grs-management", 1, 0), ("grs-management", 2, 0), ("grs-management", 3, 0), ("grs-management", 5, 0);

-- Circuit colours
   INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("circuit-colours", "Examination Circuit Colours", 1);
   INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES 
   ("circuit-colours", 1, 0), ("circuit-colours", 2, 0), ("circuit-colours", 3, 0), ("circuit-colours", 5, 0);

-- Change feature ID's
UPDATE `features` SET `feature_id`='advanced-settings' WHERE `feature_id`='advancedsettings';
UPDATE `features` SET `feature_id`='bl-group' WHERE `feature_id`='bgm';
UPDATE `features` SET `feature_id`='bl-station-export' WHERE `feature_id`='bse';
UPDATE `features` SET `feature_id`='bl-complete-export' WHERE `feature_id`='cba';
UPDATE `features` SET `feature_id`='cronbach-analysis' WHERE `feature_id`='cronbach';
UPDATE `features` SET `feature_id`='delete-results' WHERE `feature_id`='dr1';
UPDATE `features` SET `feature_id`='feedback-column' WHERE `feature_id`='efc';
UPDATE `features` SET `feature_id`='import-export-forms' WHERE `feature_id`='eis';
UPDATE `features` SET `feature_id`='session-scheduler' WHERE `feature_id`='ess';
UPDATE `features` SET `feature_id`='compulsory-feedback' WHERE `feature_id`='fc';
UPDATE `features` SET `feature_id`='grs-column' WHERE `feature_id`='grs';
UPDATE `features` SET `feature_id`='pdf-forms' WHERE `feature_id`='po1';
UPDATE `features` SET `feature_id`='station-notes' WHERE `feature_id`='sd';
UPDATE `features` SET `feature_id`='student-history' WHERE `feature_id`='seh';
UPDATE `features` SET `feature_id`='feedback-system' WHERE `feature_id`='sfs';

-- Log that the current version of the database is 43, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (43);
