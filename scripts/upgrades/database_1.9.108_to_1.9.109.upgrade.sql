-- Author: David Cunningham
-- Date: 26/11/2017
-- Version 1.9.108 => 1.9.109
-- RFC 7: Free text notes section at base of scoresheets
-- RFC 8: Adds weighted scores release option to feedback tool

CREATE TABLE `student_exam_data` (
  `student_id` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NOT NULL,
  `exam_id` BIGINT(10) NOT NULL,
  `notes` TEXT NOT NULL DEFAULT '',
  `notes_author` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL,
  `notes_updated` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`student_id`, `exam_id`))
ENGINE = InnoDB
COMMENT = 'Stores exam related data for student e.g. admin notes';

ALTER TABLE `student_exam_data` 
ADD CONSTRAINT `examdata_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`stu_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `student_exam_data` 
ADD CONSTRAINT `examdata_exam_fk`
  FOREIGN KEY (`exam_id`)
  REFERENCES `exams` (`exam_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `user_presets` 
ADD COLUMN `feedback_finalscore_weighted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `feedback_finalscore_raw`,
ADD COLUMN `feedback_finalscore_weighted_percent` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `feedback_finalscore_percent`;
    
-- Log that the current version of the database is 109, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (109);