-- Author: Domhnall Walsh
-- Date:   12/11/2015
-- OMIS database upgrade script version 1.9.60 => 1.9.61
-- Adding a bunch of update timestamp fields as well as a chunk of trigger logic
-- to be able to ripple update timestamps up through certain nested data
-- structures defined in the database to simplify querying for new data.
--  
-- For example, knowing which forms have been updated since a specific timestamp
-- because the form table knows when any of the data connected to it was last
-- updated as it gets propagated item options -> items -> sections -> form.

-- Need to change term_id to have a default of something (in this case an empty
-- string) because otherwise it implodes in strict mode.
-- Also removing the 'ON UPDATE CURRENT_TIMESTAMP' from the default for 
-- last_updated as it's non-standard syntax that some tools (including MySQL 
-- Workbench) really don't like.

-- *************************
-- *** DEPARTMENTS TABLE ***
-- *************************
-- Add an 'last_updated' timestamp column to the departments table 
ALTER TABLE `departments` 
    ADD COLUMN `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `school_id`;

-- *******************
-- *** EXAMS TABLE ***
-- *******************
-- * "Fix" last_updated field defaults
-- * Add last_updated_child
ALTER TABLE `exams` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated',
    ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `last_updated`;

-- ***************************
-- *** EXAM_SESSIONS TABLE ***
-- ***************************
-- * Change last_updated field to a timestamp
-- * Add last_updated_child
ALTER TABLE `exam_sessions` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated',
    ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `last_updated`;

-- ****************************
-- *** SESSION_GROUPS TABLE ***
-- ****************************
-- * Change default for last_updated field in session_groups to the current
--   timestamp.
-- * Add last_updated_child
ALTER TABLE `session_groups` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated' ,
    ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `last_updated`;

-- ****************************
-- *** GROUP_STUDENTS TABLE ***
-- ****************************
-- * Add last_updated and last_updated_child fields
ALTER TABLE `group_students` 
    ADD COLUMN `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `stu_order` ,
    ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `last_updated` ;

-- *****************************
-- *** STUDENT_RESULTS TABLE ***
-- *****************************
-- * Fix last_updated timestamp field for student_results
-- * Add a last_updated_child timestamp field to enable propogation of updates 
--   from child tables.
ALTER TABLE `student_results` 
    CHANGE COLUMN `is_complete` `is_complete` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '' AFTER `first_submission`,
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ,
    ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `last_updated`;

-- *************************
-- *** ITEM_SCORES TABLE ***
-- *************************
-- Add timestamp field to the item_scores table.
ALTER TABLE `item_scores` 
    ADD COLUMN `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `option_value`;

-- ******************************
-- *** SECTION_FEEDBACK TABLE ***
-- ******************************
-- Add new primary key (feedback_id) and a new last_updated timestamp field.
-- Add a composite index to the two fields (section_id, result_id) that used to
-- to be the primary key.
ALTER TABLE `section_feedback` 
    ADD COLUMN `feedback_id` BIGINT(10) NOT NULL AUTO_INCREMENT COMMENT '' FIRST,
    ADD COLUMN `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' AFTER `feedback_text`,
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (`feedback_id`)  COMMENT '',
    ADD INDEX `section_result` (`section_id` ASC, `result_id` ASC)  COMMENT '';

-- *******************
-- *** TERMS TABLE ***
-- *******************
ALTER TABLE `terms` 
    CHANGE COLUMN `created` `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ,
    CHANGE COLUMN `term_id` `term_id` VARCHAR(20) NULL DEFAULT '' COMMENT '' ,
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- *******************
-- *** USERS TABLE ***
-- *******************
-- Alter the users table so that it's update timestamp is consistent with the
-- rest of the timestamps used in the system - 'last_updated' rather than 
-- 'updated_at' which would be the default for Laravel's Eloquent.
ALTER TABLE `users` 
    CHANGE COLUMN `last_login` `last_login` TIMESTAMP NULL DEFAULT NULL COMMENT '' ,
    CHANGE COLUMN `updated_at` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ,
    ADD COLUMN `last_active` TIMESTAMP NULL DEFAULT NULL COMMENT '' AFTER `pref_module` ;

-- **********************
-- *** STUDENTS TABLE ***
-- **********************
-- Remove non-standard default syntax for last_updated field.
ALTER TABLE `students` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- ******************************
-- *** SESSION_STATIONS TABLE ***
-- ******************************
-- Ensure that the last_updated field of the session_stations table defaults to
-- CURRENT_TIMESTAMP.
ALTER TABLE `session_stations` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- *******************
-- *** FORMS TABLE ***
-- *******************
-- Ensure that the timestamp fields of the forms table default to CURRENT_TIMESTAMP.
ALTER TABLE `forms` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ,
    CHANGE COLUMN `last_updated_child` `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ,
    CHANGE COLUMN `created` `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- ***************************
-- *** FORM_SECTIONS TABLE ***
-- ***************************
-- Ensure that the timestamp fields of the form_sections table default to
-- CURRENT_TIMESTAMP
ALTER TABLE `form_sections` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated' ,
    CHANGE COLUMN `last_updated_child` `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of most recent child record update' ;

-- ***************************
-- *** SECTION_ITEMS TABLE ***
-- ***************************
-- Fix the section_items table so that the initial states of last_updated and
-- last_updated_child are at least valid dates...
ALTER TABLE `section_items` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated' ,
    CHANGE COLUMN `last_updated_child` `last_updated_child` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of most recent child record update' ;

-- **************************
-- *** ITEM_OPTIONS TABLE ***
-- **************************
-- Set correct initial value for item_options.last_updated timestamp
ALTER TABLE `item_options` 
    CHANGE COLUMN `last_updated` `last_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When record was last updated' ;

DELIMITER $$

-- ****************************
-- *** DEPARTMENTS TRIGGERS ***
-- ****************************
-- Add a before update trigger to keep it current if the data is changed.
DROP TRIGGER IF EXISTS `departments_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `departments_BEFORE_UPDATE` BEFORE UPDATE ON `departments` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- **********************
-- *** EXAMS TRIGGERS ***
-- **********************

-- Replace previous default value with a trigger.
DROP TRIGGER IF EXISTS `exams_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `exams_BEFORE_UPDATE` BEFORE UPDATE ON `exams` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- ******************************
-- *** EXAM_SESSIONS TRIGGERS ***
-- ******************************
-- Add AFTER INSERT trigger to exam_sessions to keep exams.last_updated_child current
DROP TRIGGER IF EXISTS `exam_sessions_AFTER_INSERT` $$
CREATE DEFINER = CURRENT_USER TRIGGER `exam_sessions_AFTER_INSERT` AFTER INSERT ON `exam_sessions` FOR EACH ROW
BEGIN
    UPDATE `exams` SET `last_updated_child` = NOW() WHERE `exam_id` = NEW.`exam_id`;
END $$

-- Add BEFORE DELETE trigger to exam_sessions to keep exams.last_updated_child current
DROP TRIGGER IF EXISTS `exam_sessions_BEFORE_DELETE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `exam_sessions_BEFORE_DELETE` BEFORE DELETE ON `exam_sessions` FOR EACH ROW
BEGIN
    UPDATE `exams` SET `last_updated_child` = NOW() WHERE `exam_id` = OLD.`exam_id`;
END $$

-- Ensure that there's a BEFORE UPDATE trigger on the exam_sessions table that
-- keeps the last_updated field up to date
DROP TRIGGER IF EXISTS exam_sessions_BEFORE_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `exam_sessions_BEFORE_UPDATE` BEFORE UPDATE ON `exam_sessions` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Ensure that there's an AFTER UPDATE trigger on the exam_updates table that
-- updates the `last_parent_child` field in exams (the parent table) 
DROP TRIGGER IF EXISTS exam_sessions_AFTER_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `exam_sessions_AFTER_UPDATE` AFTER UPDATE ON `exam_sessions` FOR EACH ROW
BEGIN
    UPDATE `exams` SET `last_updated_child` = NOW() WHERE exam_id = NEW.exam_id;
END $$

-- **********************
-- *** FORMS TRIGGERS ***
-- **********************
-- Trigger on forms BEFORE UPDATE to ensure that the last_updated field is correct.
DROP TRIGGER IF EXISTS `forms_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `forms_BEFORE_UPDATE` BEFORE UPDATE ON `forms` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- ******************************
-- *** FORM_SECTIONS TRIGGERS ***
-- ******************************
-- Trigger on form_sections BEFORE DELETE so that the last_updated_child field of the
-- exam_sessions table is updated to mark the change; in this case, the removal of the
-- station.
DROP TRIGGER IF EXISTS form_sections_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `form_sections_BEFORE_DELETE` BEFORE DELETE ON `form_sections` FOR EACH ROW
BEGIN
    UPDATE `forms` SET `last_updated_child` = NOW() WHERE `form_id` = OLD.`form_id`;
END $$

-- Trigger on form_sections AFTER INSERT so that when a new form section is 
-- added, the forms table's last_updated_child field gets updated.
DROP TRIGGER IF EXISTS form_sections_AFTER_INSERT$$
CREATE DEFINER = CURRENT_USER TRIGGER `form_sections_AFTER_INSERT` AFTER INSERT ON `form_sections` FOR EACH ROW
BEGIN
    UPDATE `forms` SET `last_updated_child` = NOW() WHERE `form_id` = NEW.`form_id`;
END $$

-- Trigger on form_sections AFTER INSERT so that when a form section is updated,
-- the forms table's last_updated_child field gets updated.
DROP TRIGGER IF EXISTS form_sections_AFTER_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `form_sections_AFTER_UPDATE` AFTER UPDATE ON `form_sections` FOR EACH ROW
BEGIN
    UPDATE `forms` SET `last_updated_child` = NOW() WHERE `form_id` = NEW.`form_id`;
END $$

-- Trigger on form_sections BEFORE UPDATE so that it can set the current record's
-- last_updated timestamp to be the same as the one set on last_updated_child field of the
-- forms table, essentially propagating the notification of the change.
DROP TRIGGER IF EXISTS form_sections_BEFORE_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `form_sections_BEFORE_UPDATE` BEFORE UPDATE ON `form_sections` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- *******************************
-- *** GROUP_STUDENTS TRIGGERS ***
-- *******************************
-- Add AFTER INSERT trigger to `group_students` table to keep `session_groups`.`last_updated` current.
DROP TRIGGER IF EXISTS group_students_AFTER_INSERT$$
CREATE DEFINER=CURRENT_USER TRIGGER `group_students_AFTER_INSERT` AFTER INSERT ON `group_students` FOR EACH ROW
BEGIN
    UPDATE `session_groups` SET `last_updated_child` = NOW() WHERE `group_id` = NEW.`group_id`;
END $$

-- Add AFTER UPDATE trigger to `group_students` table to keep `session_groups`.`last_updated` current.
DROP TRIGGER IF EXISTS group_students_AFTER_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `group_students_AFTER_UPDATE` AFTER UPDATE ON `group_students` FOR EACH ROW
BEGIN
    UPDATE `session_groups` SET `last_updated_child` = NOW() WHERE `group_id` = NEW.`group_id`;
END $$

-- Add BEFORE DELETE trigger to `group_students` table to keep `session_groups`.`last_updated` current.
DROP TRIGGER IF EXISTS group_students_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `group_students_BEFORE_DELETE` BEFORE DELETE ON `group_students` FOR EACH ROW
BEGIN
    UPDATE `session_groups` SET `last_updated_child` = NOW() WHERE `group_id` = OLD.`group_id`;
END $$

-- Add BEFORE UPDATE trigger to `group_students` table to keep `last_updated`
-- field current.
DROP TRIGGER IF EXISTS group_students_BEFORE_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `group_students_BEFORE_UPDATE` BEFORE UPDATE ON `group_students` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- *****************************
-- *** ITEM_OPTIONS TRIGGERS ***
-- *****************************
-- Trigger on item_options BEFORE UPDATE that sets the `last_updated` timestamp
-- field correctly.
DROP TRIGGER IF EXISTS item_options_BEFORE_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_options_BEFORE_UPDATE` BEFORE UPDATE ON `item_options` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Trigger on item_options AFTER_UPDATE that sets the `last_updated_child`
-- timestamp on the relevant records in the `section_items` table when an item 
-- option has been updated.
DROP TRIGGER IF EXISTS item_options_AFTER_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_options_AFTER_UPDATE` AFTER UPDATE ON `item_options` FOR EACH ROW
BEGIN
    UPDATE `section_items` SET `last_updated_child` = NEW.`last_updated` WHERE `item_id` = NEW.`item_id`;
END $$

-- Create an AFTER INSERT trigger for item options that updates the last_updated_child
-- field on section_items.
DROP TRIGGER IF EXISTS item_options_AFTER_INSERT$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_options_AFTER_INSERT` AFTER INSERT ON `item_options` FOR EACH ROW
BEGIN
    UPDATE `section_items` SET `last_updated_child` = NEW.`last_updated` WHERE `item_id` = NEW.`item_id`;
END $$

-- Create an BEFORE DELETE trigger for item options that updates the last_updated_child
-- field on section_items.
DROP TRIGGER IF EXISTS item_options_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_options_BEFORE_DELETE` BEFORE DELETE ON `item_options` FOR EACH ROW
BEGIN
    UPDATE `section_items` SET `last_updated_child` = NOW() WHERE `item_id` = OLD.`item_id`;
END $$

-- ****************************
-- *** ITEM_SCORES TRIGGERS ***
-- ****************************
-- Set the BEFORE UPDATE trigger on item_scores to keep `item_scores`.`last_updated`
-- current
DROP TRIGGER IF EXISTS item_scores_BEFORE_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `item_scores_BEFORE_UPDATE` BEFORE UPDATE ON `item_scores` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Set the AFTER UPDATE trigger on item_scores to update student_results.last_updated_child
DROP TRIGGER IF EXISTS item_scores_AFTER_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_scores_AFTER_UPDATE` AFTER UPDATE ON `item_scores` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NOW() WHERE `result_id` = NEW.`result_id`;
END $$

-- Set the AFTER INSERT trigger on item_scores to update student_results.last_updated_child
DROP TRIGGER IF EXISTS item_scores_AFTER_INSERT$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_scores_AFTER_INSERT` AFTER INSERT ON `item_scores` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NOW() WHERE `result_id` = NEW.`result_id`;
END $$

-- Set the BEFORE DELETE trigger on item_scores to update student_results.last_updated_child
DROP TRIGGER IF EXISTS item_scores_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `item_scores_BEFORE_DELETE` BEFORE DELETE ON `item_scores` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NOW() WHERE `result_id` = OLD.`result_id`;
END $$

-- *********************************
-- *** SECTION_FEEDBACK TRIGGERS ***
-- *********************************
-- Add a BEFORE UPDATE trigger to section_feedback to keep the `last_updated`
-- field current.
DROP TRIGGER IF EXISTS section_feedback_BEFORE_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `section_feedback_BEFORE_UPDATE` BEFORE UPDATE ON `section_feedback` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Add an AFTER INSERT trigger to section_feedback to keep the 
-- `student_results`.`last_updated_child` current.
DROP TRIGGER IF EXISTS section_feedback_AFTER_INSERT$$
CREATE DEFINER = CURRENT_USER TRIGGER `section_feedback_AFTER_INSERT` AFTER INSERT ON `section_feedback` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NEW.`last_updated` WHERE `result_id` = NEW.`result_id`;
END $$

-- Add an AFTER UPDATE trigger to section_feedback to keep the 
-- `student_results`.`last_updated_child` current.
DROP TRIGGER IF EXISTS section_feedback_AFTER_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `section_feedback_AFTER_UPDATE` AFTER UPDATE ON `section_feedback` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NEW.`last_updated` WHERE `result_id` = NEW.`result_id`;
END $$

-- Add an BEFORE DELETE trigger to section_feedback to keep the 
-- `student_results`.`last_updated_child` current.
DROP TRIGGER IF EXISTS section_feedback_BEFORE_DELETE$$
CREATE DEFINER = CURRENT_USER TRIGGER `section_feedback_BEFORE_DELETE` BEFORE DELETE ON `section_feedback` FOR EACH ROW
BEGIN
    UPDATE `student_results` SET `last_updated_child` = NOW() WHERE `result_id` = OLD.`result_id`;
END $$

-- *******************************
-- *** SESSION_GROUPS TRIGGERS ***
-- *******************************
-- Add a BEFORE UPDATE trigger to the `session_groups` table to keep its 
-- `updated_at` field current.
DROP TRIGGER IF EXISTS `session_groups_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `session_groups_BEFORE_UPDATE` BEFORE UPDATE ON `session_groups` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Add an AFTER INSERT trigger to the `session_groups` table to keep 
-- `exam_sessions`.`last_updated_child` current.
DROP TRIGGER IF EXISTS session_groups_AFTER_INSERT$$
CREATE DEFINER = CURRENT_USER TRIGGER `session_groups_AFTER_INSERT` AFTER INSERT ON `session_groups` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE `session_id` = NEW.`session_id`;
END $$

-- Add an AFTER UPDATE trigger to the `session_groups` table to keep 
-- `exam_sessions`.`last_updated_child` current.
DROP TRIGGER IF EXISTS session_groups_AFTER_UPDATE$$
CREATE DEFINER = CURRENT_USER TRIGGER `session_groups_AFTER_UPDATE` AFTER UPDATE ON `session_groups` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE `session_id` = NEW.`session_id`;
END $$

-- Add an BEFORE DELETE trigger to the `session_groups` table to keep 
-- `exam_sessions`.`last_updated_child` current.
DROP TRIGGER IF EXISTS session_groups_BEFORE_DELETE$$
CREATE DEFINER = CURRENT_USER TRIGGER `session_groups_BEFORE_DELETE` BEFORE DELETE ON `session_groups` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE `session_id` = OLD.`session_id`;
END $$

-- ******************************
-- *** SECTION_ITEMS TRIGGERS ***
-- ******************************
-- AFTER INSERT trigger on `section_items` to keep `form_sections`.`last_updated_child` up to date.
DROP TRIGGER IF EXISTS section_items_AFTER_INSERT$$
CREATE DEFINER=CURRENT_USER TRIGGER `section_items_AFTER_INSERT` AFTER INSERT ON `section_items` FOR EACH ROW
BEGIN
    UPDATE `form_sections` SET `last_updated_child` = NEW.`last_updated` WHERE `section_id` = NEW.`section_id`;
END $$

-- BEFORE UPDATE trigger to keep `form_sections`.`last_updated_child` up to date.
DROP TRIGGER IF EXISTS section_items_BEFORE_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `section_items_BEFORE_UPDATE` BEFORE UPDATE ON `section_items` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- BEFORE DELETE trigger on `section_items` to keep `form_sections`.`last_updated_child` up to date.
DROP TRIGGER IF EXISTS section_items_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `section_items_BEFORE_DELETE` BEFORE DELETE ON `section_items` FOR EACH ROW
BEGIN
    UPDATE `form_sections` SET `last_updated_child` = NOW() WHERE `section_id` = OLD.`section_id`;
END $$

-- AFTER UPDATE trigger on `section_items` to keep `form_sections`.`last_updated_child` up to date.
DROP TRIGGER IF EXISTS section_items_AFTER_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `section_items_AFTER_UPDATE` AFTER UPDATE ON `section_items` FOR EACH ROW
BEGIN
    UPDATE `form_sections` SET `last_updated_child` = NEW.`last_updated` WHERE `section_id` = NEW.`section_id`;
END $$

-- *********************************
-- *** SESSION_STATIONS TRIGGERS ***
-- *********************************
-- Remove this trigger if it exist as we're no longer using it.
DROP TRIGGER IF EXISTS `session_stations_BEFORE_INSERT` $$

-- Add a BEFORE UPDATE on session_stations to keep `last_updated` field current.
DROP TRIGGER IF EXISTS `session_stations_BEFORE_UPDATE`$$
CREATE DEFINER=CURRENT_USER TRIGGER `session_stations_BEFORE_UPDATE` BEFORE UPDATE ON `session_stations` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Add a BEFORE DELETE trigger on `session_stations` to keep 
-- `exam_sessions`.`last_updated_child` current
DROP TRIGGER IF EXISTS `session_stations_BEFORE_DELETE`$$
CREATE DEFINER=CURRENT_USER TRIGGER `session_stations_BEFORE_DELETE` BEFORE DELETE ON `session_stations` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE session_id = OLD.session_id;
END $$

-- Add an AFTER INSERT trigger on `session_stations` to keep 
-- `exam_sessions`.`last_updated_child` current
DROP TRIGGER IF EXISTS `session_stations_AFTER_INSERT`$$
CREATE DEFINER = CURRENT_USER TRIGGER `session_stations_AFTER_INSERT` AFTER INSERT ON `session_stations` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE session_id = NEW.session_id;
END $$

-- Add a AFTER UPDATE trigger on `session_stations` to keep 
-- `exam_sessions`.`last_updated_child` current
DROP TRIGGER IF EXISTS `session_stations_AFTER_UPDATE`$$
CREATE DEFINER = CURRENT_USER TRIGGER `session_stations_AFTER_UPDATE` AFTER UPDATE ON `session_stations` FOR EACH ROW
BEGIN
    UPDATE `exam_sessions` SET `last_updated_child` = NOW() WHERE session_id = NEW.session_id;
END $$

-- *************************
-- *** STUDENTS TRIGGERS ***
-- *************************
-- Add BEFORE UPDATE trigger to `students` to keep `last_updated` field current.
DROP TRIGGER IF EXISTS students_BEFORE_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `students_BEFORE_UPDATE` BEFORE UPDATE ON `students` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- Add AFTER INSERT trigger to `students` to keep `group_students`.`last_updated_child`
-- current.
DROP TRIGGER IF EXISTS students_AFTER_INSERT$$
CREATE DEFINER=CURRENT_USER TRIGGER `students_AFTER_INSERT` AFTER INSERT ON `students` FOR EACH ROW
BEGIN
    UPDATE `group_students` SET `last_updated_child` = NOW() WHERE `stu_id` = NEW.`stu_id`;
END $$

-- Add AFTER UPDATE trigger to `students` to keep `group_students`.`last_updated_child`
-- current.
DROP TRIGGER IF EXISTS students_AFTER_UPDATE$$
CREATE DEFINER=CURRENT_USER TRIGGER `students_AFTER_UPDATE` AFTER UPDATE ON `students` FOR EACH ROW
BEGIN
    UPDATE `group_students` SET `last_updated_child` = NOW() WHERE `stu_id` = NEW.`stu_id`;
END $$

-- Add BEFORE DELETE trigger to `students` to keep `group_students`.`last_updated_child`
-- current.
DROP TRIGGER IF EXISTS students_BEFORE_DELETE$$
CREATE DEFINER=CURRENT_USER TRIGGER `students_BEFORE_DELETE` BEFORE DELETE ON `students` FOR EACH ROW
BEGIN
    UPDATE `group_students` SET `last_updated_child` = NOW() WHERE `stu_id` = OLD.`stu_id`;
END $$

-- ********************************
-- *** STUDENT_RESULTS TRIGGERS ***
-- ********************************
-- Fix last_updated timestamp field for student_results and adding a last_updated_child
-- timestamp field to enable propogation of updates from child tables.
-- Create a BEFORE UPDATE trigger for student_results that can update the last_updated
-- field for any records that are changed.
DROP TRIGGER IF EXISTS `student_results_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `student_results_BEFORE_UPDATE` BEFORE UPDATE ON `student_results` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- **********************
-- *** TERMS TRIGGERS ***
-- **********************
-- Add a trigger to be able to force an update of the last_updated field for the terms
-- table after update, i.e when a value has been changed.
DROP TRIGGER IF EXISTS `terms_BEFORE_UPDATE` $$
CREATE DEFINER=CURRENT_USER TRIGGER `terms_BEFORE_UPDATE` BEFORE UPDATE ON `terms` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

-- **********************
-- *** USERS TRIGGERS ***
-- **********************
-- Add a before update trigger to the users table to keep its updated_at field current.
DROP TRIGGER IF EXISTS `users_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `users_BEFORE_UPDATE` BEFORE UPDATE ON `users` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
END $$

DELIMITER ;

-- Log that the current version of the database is 61, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (61);