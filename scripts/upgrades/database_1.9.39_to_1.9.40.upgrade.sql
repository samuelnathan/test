-- Change 'start_time' and 'end_time' fields defaults to NULL
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   16/12/2014
-- Omis database upgrade script version 1.9.39 => 1.9.40


ALTER TABLE `exam_sessions` 
CHANGE COLUMN `start_time` `start_time` TIME NULL DEFAULT NULL ,
CHANGE COLUMN `end_time` `end_time` TIME NULL DEFAULT NULL ;

-- Log that the current version of the database is 40, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (40);
