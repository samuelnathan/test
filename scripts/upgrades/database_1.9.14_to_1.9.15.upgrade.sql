-- Removing access to the Role tool to all but Super Admins
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   15/04/2014
-- Omis database upgrade script version 1.9.14 => 1.9.15

DELETE FROM `page_levels` WHERE `page_id` IN (157, 158);
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (157, 1, 0), (157, 2, 0), (157, 3, 0), (157, 4, 0), (157, 5, 0), (157, 6, 1);
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (158, 1, 0), (158, 2, 0), (158, 3, 0), (158, 4, 0), (158, 5, 0), (158, 6, 1);

-- Log that the current version of the database is 15, and it was added now.
INSERT INTO `database` (`version`) VALUES (15);