-- Author: David Cunningham
-- Date: 15/09/2017
-- Version 1.9.106.1 => 1.9.106.2 
-- Change import field examiner_admin => exam_admin 

-- Nshcs Only (Important)
-- UPDATE roles
-- SET role_importexport_field='assessor'
-- WHERE role_id=2;

-- Others
UPDATE roles
SET role_importexport_field='exam_admin'
WHERE role_id=3;
