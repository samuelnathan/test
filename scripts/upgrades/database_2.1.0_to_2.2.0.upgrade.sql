-- Author: David Cunningham
-- Date: 09/01/2020
--  SSCL Customisations - Multimedia Centre
-- Version 2.1.0 => 2.1.0

-- Change rest column to station type

ALTER TABLE `session_stations` 
CHANGE COLUMN `rest_station` `rest_station` VARCHAR(20) NOT NULL DEFAULT '0';

SET SQL_SAFE_UPDATES = 0;
UPDATE session_stations SET rest_station = "rest" WHERE rest_station = "1";
UPDATE session_stations SET rest_station = "assess" WHERE rest_station = "0";
SET SQL_SAFE_UPDATES = 1;

ALTER TABLE `session_stations` 
CHANGE COLUMN `rest_station` `type` ENUM('assess', 'rest', 'object') NOT NULL DEFAULT 'assess';

-- Add 2 new tables to track AWS S3 uploads and relationship between media file, station and student.
CREATE TABLE IF NOT EXISTS `multimedia_uploaded` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `multimedia_key` BINARY(16) NOT NULL COMMENT "Uniquely identify object on AWS S3 or equivalent, use UUID_TO_BIN(UUID()) 
    for inserting and BIN_TO_UUID(id) to selecting in human readable format",
  PRIMARY KEY (`id`),
  `multimedia_link` TEXT NULL COMMENT "e.g. S3 full path",
  `uploaded_by` VARCHAR(30) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)
ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS `station_multimedia` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `multimedia_id` BIGINT(10) UNSIGNED NOT NULL COMMENT "Link to multimedia uploaded record", 
  PRIMARY KEY (`id`),
  `station_id` BIGINT(10) NULL,
  `student_id` VARCHAR(30) NOT NULL,
  `updated_by` VARCHAR(30) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX `multimedia_station_fk_idx` (`station_id` ASC),
  INDEX `multimedia_student_fk_idx` (`student_id` ASC),
  INDEX `multimedia_uploaded_fk_idx` (`multimedia_id` ASC),
  CONSTRAINT `multimedia_station_fk`
    FOREIGN KEY (`station_id`)
    REFERENCES `session_stations` (`station_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `multimedia_student_fk`
    FOREIGN KEY (`student_id`)
    REFERENCES `students` (`student_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `multimedia_uploaded_fk`
    FOREIGN KEY (`multimedia_id`)
    REFERENCES `multimedia_uploaded` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

-- Log that the current version of the database is 2.0.0, and it was added now.
INSERT INTO `database_versions` (`major_version`, `minor_version`, `patch_version`) VALUES ('2', '2', '0');
