-- Author: Domhnall Walsh
-- Date:   07/03/2016
-- OMIS database upgrade script version 1.9.65 => 1.9.66
-- Adds unique update identifier to the item_scores table.

-- UUID4 strings are 36 characters long.
ALTER TABLE `item_scores` 
ADD COLUMN `update_uuid` VARCHAR(36) NULL DEFAULT NULL COMMENT '' AFTER `last_updated`;

-- Define a trigger so that we can get MySQL to inject a randomly-generated UUID
-- (well, technically a UUID4) in as the default value in the update_uuid field.
--
-- If no UUID is set, assign one on INSERT or UPDATE. The API should provide a
-- UUID with each transaction, but OMIS 'proper' won't, so this injects one to
-- allow for consistency going forward in the event that somehow the same result
-- is being edited via OMIS and the app (or another API consumer) simultaneously
DELIMITER $$

DROP TRIGGER IF EXISTS item_scores_BEFORE_INSERT$$

CREATE DEFINER = CURRENT_USER TRIGGER `item_scores_BEFORE_INSERT` BEFORE INSERT ON `item_scores` FOR EACH ROW
BEGIN
    IF NEW.`update_uuid` IS NULL OR NEW.`update_uuid` = '' THEN
        SET NEW.`update_uuid` = UUID();
    END IF;
END
$$

DROP TRIGGER IF EXISTS item_scores_BEFORE_UPDATE$$

CREATE DEFINER = CURRENT_USER TRIGGER `item_scores_BEFORE_UPDATE` BEFORE UPDATE ON `item_scores` FOR EACH ROW
BEGIN
    SET NEW.`last_updated` = NOW();
    IF NEW.`update_uuid` IS NULL OR NEW.`update_uuid` = '' THEN
        SET NEW.`update_uuid` = UUID();
    END IF;
END$$
DELIMITER ;

-- Turn off safe updates so that the following update runs in MySql Workbench.
SET sql_SAFE_UPDATES = 0;

-- Inject UUIDs into existing records where there are none currently.
UPDATE `item_scores` SET `update_uuid` = UUID() WHERE `update_uuid` IS NULL;

-- Turn safe updates back on.
SET sql_SAFE_UPDATES = 1;

-- Log that the current version of the database is 66, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (66);
