# Creating Database Upgrade Scripts

The idea of creating database update scripts is to allow for the staged migration of a database to the latest version as required by the current build of the code at any given time. As such, it needs to be treated as something that can "only go forward"; what this means, in practice, is that once a database script is pushed to version control, its functionality **should not** be changed for any reason, except the following:

 * It deletes or modifies something in an unrecoverable way that it shouldn't.
  * It contains syntax errors that will cause it not to run.

  In all other cases, errors in the upgrade script should be fixed by pushing a new script containing the corrections.

## Why is this important?

In short, it's because it's possible that someone applies a script to an important database that has no backups _before someone realizes there's a problem with it_.

If the fix is pushed in the next DB update script working off the assumption that the faulty script has been applied, then this unfortunate situation is recoverable; otherwise, Houston, we have a problem.
