-- Adding Global rating scale table
-- Author: Cormac Mc Swiney <cormac.mcswiney@qpercom.ie>
-- Date:   04/06/2014
-- Omis database upgrade script version 1.9.24 => 1.9.25


-- Create Table
CREATE TABLE global_rating_scales
(
scale_type varchar(10),
scale_value int(10),
value_description varchar(255)
);


-- Add Values
INSERT INTO global_rating_scales (scale_type, scale_value, value_description)
VALUES 
('type1', 0, "Clear Fail"),
('type1', 1, "Borderline"),
('type1', 2, "Clear Pass"),
('type1', 3, "Good Pass"),
('type1', 4, "Excellent"),
('type2', 0, "Clear Fail"),
('type2', 1, "Borderline Fail"),
('type2', 2, "Borderline Pass"),
('type2', 3, "Clear Pass"),
('type2', 4, "Excellent"),
('type3', 0, "Fail"),
('type3', 1, "Borderline"),
('type3', 2, "Pass"),
('type4', 0, "Unacceptable"),
('type4', 1, "Borderline"),
('type4', 2, "Okay"),
('type4', 3, "Good"),
('type4', 4, "Outstanding"),
('type5', 0, "Fail"),
('type5', 1, "Pass");

-- Log that the current version of the database is 25, and it was added now.
INSERT INTO `database` (`version`) VALUES (25);
