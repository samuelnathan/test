-- Short script to create a table linking JSON Web Tokens for Authentication
-- to users in the database.
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   03/07/2015
-- Omis database upgrade script version 1.9.46 => 1.9.47

-- Create API tokens table
CREATE TABLE IF NOT EXISTS `api_tokens` (
      `id` BIGINT NOT NULL AUTO_INCREMENT,
      `token_uuid` CHAR(36) NOT NULL COMMENT 'Unique ID for token (128bit GUID)',
      `user_id` VARCHAR(30) NOT NULL COMMENT '',
      `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
      `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
      `expires_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
      `revoked_at` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time token was revoked, if any',
      `deleted_at` TIMESTAMP NOT NULL COMMENT '',
      PRIMARY KEY (`id`)  COMMENT '',
      UNIQUE INDEX `token_uuid_UNIQUE` (`token_uuid` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin
COMMENT = 'Table to store per-user access tokens to API';

-- Create foreign key linking users to API tokens
ALTER TABLE `api_tokens` 
ADD INDEX `fk_api_token_user_idx` (`user_id` ASC)  COMMENT '';
ALTER TABLE `api_tokens` 
ADD CONSTRAINT `fk_api_token_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Log that the current version of the database is 47, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (47);

