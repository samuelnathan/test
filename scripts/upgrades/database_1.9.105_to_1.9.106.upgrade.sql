-- Author: David Cunningham
-- Date: 13/08/2017
-- New features and roles

INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("training-status", "Training Status Indicator", 7);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("training-status", 1, 1), ("training-status", 2, 0), ("training-status", 3, 1), ("training-status", 5, 1);

INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('8', 'Content Administration');

INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("assess-content", "Add/Edit Assessment (Scoresheet) Content", 8);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("assess-content", 1, 1), ("assess-content", 2, 0), ("assess-content", 3, 1), ("assess-content", 5, 1);

-- New School Admin Role
INSERT INTO .`roles` (`role_id`, `role_name`, `role_shortname`, `role_description`, `role_importexport_field`, `model`) 
VALUES ('7', 'School Admin', 'School Adm.', 'School Administrators', 'school_admin', 'SchoolAdmin');

INSERT INTO `role_category_link` (`role_id`, `category_id`) VALUES ('7', '1');
INSERT INTO role_category_link (role_id,category_id) VALUES ('7','5');

-- HEE ONLY ROLE
-- UPDATE .`roles` SET `role_name`='Recruitment Office Admin', 
-- `role_shortname`='Recruit. Office Adm.', `role_description`='Recruitment Office Administrators',
-- `role_importexport_field`='recruitment_office_admin', `model`='RecruitmentOfficeAdmin' WHERE `role_id`='7';

CREATE TABLE `school_admins` (
  `school_id` BIGINT(10) NOT NULL COMMENT 'Identifier of School',
  `user_id` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'User Identifier of the Administrator',
  PRIMARY KEY (`school_id`, `user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Link between schools and admins';

ALTER TABLE `school_admins` 
ADD CONSTRAINT `schooladmins_schools_fk`
  FOREIGN KEY (`school_id`)
  REFERENCES `schools` (`school_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `schooladmins_users_fk`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

UPDATE `page_levels` SET `level_enabled`= 1 WHERE `page_id`='184' AND `page_level`='1';
UPDATE `page_levels` SET `level_enabled`= 1 WHERE `page_id`='185' AND `page_level`='1';
UPDATE `page_levels` SET `level_enabled`= 1 WHERE `page_id`='186' AND `page_level`='1';
UPDATE `page_levels` SET `level_enabled`= 1 WHERE `page_id`='187' AND `page_level`='1';

INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (1,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (2,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (3,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (4,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (5,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (6,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (7,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (8,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (9,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (10,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (11,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (12,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (13,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (14,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (15,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (16,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (17,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (18,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (19,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (20,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (21,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (22,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (23,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (24,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (25,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (26,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (27,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (28,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (33,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (34,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (35,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (36,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (37,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (38,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (39,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (40,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (41,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (42,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (43,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (44,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (45,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (46,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (47,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (48,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (49,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (50,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (51,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (52,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (53,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (54,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (55,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (56,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (57,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (58,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (59,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (60,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (61,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (62,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (63,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (64,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (65,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (66,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (67,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (68,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (69,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (70,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (71,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (72,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (73,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (74,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (75,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (76,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (77,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (78,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (79,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (80,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (81,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (82,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (83,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (84,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (85,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (86,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (87,7,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (88,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (89,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (90,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (91,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (92,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (93,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (94,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (95,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (96,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (97,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (98,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (99,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (100,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (101,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (102,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (103,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (104,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (105,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (108,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (109,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (110,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (111,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (112,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (113,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (114,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (115,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (116,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (117,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (118,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (119,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (120,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (121,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (122,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (123,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (124,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (125,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (126,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (127,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (128,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (129,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (130,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (131,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (132,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (133,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (134,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (135,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (136,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (137,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (138,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (139,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (140,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (141,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (142,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (143,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (144,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (146,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (147,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (148,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (149,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (150,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (151,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (152,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (153,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (156,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (157,7,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (158,7,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (159,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (160,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (161,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (162,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (163,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (164,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (165,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (166,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (167,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (168,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (169,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (170,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (171,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (172,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (174,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (175,7,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (176,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (177,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (178,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (179,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (180,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (181,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (182,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (183,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (184,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (185,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (186,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (187,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (188,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (189,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (190,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (191,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (193,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (194,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (195,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (196,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (197,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (198,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (199,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (200,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (201,7,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (202,7,1);

INSERT INTO pages (page_id, page_name) VALUES (203, 'json_admin');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (203, 1, 1), (203, 2, 0), (203, 3, 0), (203, 4, 0), (203, 5, 0), (203, 6, 1), (203, 7, 1);

INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('advanced-settings',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('assess-content',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('bl-complete-export',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('bl-group',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('bl-station-export',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('candidate-number',7,0);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('circuit-colours',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('competency-framework',7,0);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('compulsory-feedback',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('cronbach-analysis',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('delete-results',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('exam-matrix',7,0);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('exam-number',7,0);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('exam-team',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('feedback-column',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('feedback-download',7,0);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('feedback-system',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('grs-column',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('grs-management',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('import-export-forms',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('multi-examiners',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('pdf-forms',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('session-scheduler',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('station-notes',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('student-courses',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('student-history',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('student-images',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('student-manage',7,1);
INSERT INTO `feature_roles` (`feature_id`,`feature_role`,`role_enabled`) VALUES ('swap-tool',7,0);

-- UPDATE `roles` SET `role_importexport_field`='centre_admin' WHERE `role_id`='3';
-- UPDATE `roles` SET `role_importexport_field`='panellist' WHERE `role_id`='2';

-- Log that the current version of the database is 106, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (106);

