-- Author: Domhnall Walsh
-- Date:   14/12/2015
-- OMIS database upgrade script version 1.9.64 => 1.9.65
-- Adds field to record start date of API token validity period
SET SQL_SAFE_UPDATES = 0;
-- Add the 'valid_at' column to the `api_tokens` table. Temporarily set the
-- default timestamp to '0000-00-00 00:00:00'.
ALTER TABLE `api_tokens` 
ADD COLUMN `valid_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '' AFTER `deleted_at`;

-- Update the `api_tokens` table to set the valid_at field to mirror the 
-- `created_at` field where it's not been set already. This mirrors the existing
-- behaviour of the system and so should cause no issues.
-- Apparently it's tricky/impossible to directly use timestamps as part of 
-- query definitions in MySQL so we have to improvise a bit.
UPDATE `api_tokens` SET `valid_at` = `created_at` where (YEAR(`valid_at`) = 0);

-- Once we've done this, set the default value of valid_at for future records
-- to be CURRENT_TIMESTAMP to make sure that the tokens stay consistent without
-- programmatical help. 
ALTER TABLE `api_tokens` 
CHANGE COLUMN `valid_at` `valid_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- Log that the current version of the database is 65, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (65);
SET SQL_SAFE_UPDATES = 1;