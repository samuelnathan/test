-- Author: David Cunningham
-- Date:   17/09/2015
-- OMIS database upgrade script version 1.9.57 => 1.9.58
-- Slight adjustment to the exam_settings table, makes exam_id INDEX unique
-- so as to prevent duplicate exams being added

ALTER TABLE `exam_settings` 
DROP INDEX `settings_exam_fk_idx` ,
ADD UNIQUE INDEX `settings_exam_fk_idx` (`exam_id` ASC);

-- Log that the current version of the database is 58, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (58);
