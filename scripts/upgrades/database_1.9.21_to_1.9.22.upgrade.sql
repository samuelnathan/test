-- page 'assistants_redirect' missing in pages table
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   21/05/2014
-- Omis database upgrade script version 1.9.21 => 1.9.22

DELETE FROM pages WHERE page_id = 173;
DELETE FROM page_levels WHERE page_id = 173;
INSERT INTO pages (page_id, page_name) VALUES (182, 'assistants_redirect');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (182, 1, 1), (182, 2, 0), (182, 3, 1), (182, 4, 0), (182, 5, 1), (182, 6, 1);

-- Log that the current version of the database is 22, and it was added now.
INSERT INTO `database` (`version`) VALUES (22);
