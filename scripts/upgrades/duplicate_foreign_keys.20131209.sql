# Remove Duplicate foreign keys Omis 1.8.5 and 1.9
ALTER TABLE `competence_results` DROP FOREIGN KEY competence_results_ibfk_3;
ALTER TABLE `student_feedback` DROP FOREIGN KEY student_feedback_ibfk_4;
ALTER TABLE `student_feedback` DROP FOREIGN KEY student_feedback_ibfk_5;
ALTER TABLE `student_feedback` DROP FOREIGN KEY student_feedback_ibfk_6;