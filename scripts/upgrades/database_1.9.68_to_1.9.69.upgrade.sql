-- Author: David Cunningham
-- Date: 30/03/2016
-- OMIS database upgrade script version 1.9.68 => 1.9.69
-- The `blank_student_group_fk` foreign key found in the `blank_students` table was incorrectly
-- referring to the `group_id` field in the `group_students`. The script rectifies this
-- by pointing it towards the `group_id` in the `session_groups` table

ALTER TABLE `blank_students` 
DROP FOREIGN KEY `blank_student_group_fk`;
ALTER TABLE `blank_students` 
DROP INDEX `group_id`;

ALTER TABLE `blank_students` 
ADD INDEX `blank_student_group_fk_idx` (`group_id` ASC);
ALTER TABLE `blank_students` 
ADD CONSTRAINT `blank_student_group_fk`
  FOREIGN KEY (`group_id`)
  REFERENCES `session_groups` (`group_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Log that the current version of the database is 69, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (69);
