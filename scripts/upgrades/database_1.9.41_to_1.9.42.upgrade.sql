-- Add advanced settings feature for exams 
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   17/02/2015
-- Omis database upgrade script version 1.9.41 => 1.9.42

INSERT IGNORE INTO features (feature_id, feature_desc) VALUES ("advancedsettings", "Exam Advanced Settings");
INSERT IGNORE INTO feature_levels (feature_id, feature_level, level_enabled) VALUES ("advancedsettings", 1, 0), ("advancedsettings", 2, 0), ("advancedsettings", 3, 0), ("advancedsettings", 5, 0);

ALTER TABLE `exams`
ADD COLUMN `results_identifier` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_surname` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_forename` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_session` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_date` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_group` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_form` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_number` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_pass` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_score` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_percentage` int(1) NOT NULL DEFAULT '1',
ADD COLUMN `results_grs` int(1) NOT NULL DEFAULT '1';

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (188, 'settings_osce');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (188, 1, 1), (188, 2, 0), (188, 3, 1), (188, 4, 0), (188, 5, 1), (188, 6, 1);

INSERT INTO `pages` (`page_id`, `page_name`) VALUES (189, 'ajx_settings');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (189, 1, 1), (189, 2, 0), (189, 3, 1), (189, 4, 0), (189, 5, 1), (189, 6, 1);

-- Log that the current version of the database is 42, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (42);
