-- Author: Domhnall Walsh
-- Date:   20/10/2015
-- OMIS database upgrade script version 1.9.58 => 1.9.59
-- Users table: 'last_active' field is a hangover from a previous attempt to
-- implement API security keys, is no longer used. However, it would be nice
-- to have an updated timestamp field, so we'll change it to that instead.

ALTER TABLE `users` 
CHANGE COLUMN `last_active` `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;

-- Log that the current version of the database is 59, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (59);

