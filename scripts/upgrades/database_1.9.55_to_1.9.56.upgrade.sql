-- Author: David Cunningham
-- Date:   14/09/2015
-- Omis database upgrade script version 1.9.55 => 1.9.56
-- Removes no longer required field 'multi_forms_station'. We can figure out
-- from the setup in each station if multiple forms (scenarios) are used 

ALTER TABLE `exams` 
DROP COLUMN `multi_forms_station`;

-- Log that the current version of the database is 56, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (56);
