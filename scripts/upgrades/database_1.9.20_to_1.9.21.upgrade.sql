-- Adding 'dresults' page to protection system to allow for delete results pages
-- to go without a hitch.
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (181, 'dresults');

INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (181, 1, 1), (181, 2, 0), (181, 3, 1), (181, 4, 0), (181, 5, 1), (181, 6, 1);

INSERT INTO `database` (`version`) VALUES (21);
