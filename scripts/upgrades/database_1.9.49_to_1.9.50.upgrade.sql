-- Author: David Cunningham
-- Date:   12/08/2015
-- Omis database upgrade script version 1.9.49 => 1.9.50
-- Removes no longer used fields from the database

-- Remove department description field
ALTER TABLE `departments` 
DROP COLUMN `dept_description`;

-- Remove course description field
ALTER TABLE `courses` 
DROP COLUMN `crs_description`;

-- Remove module description field
ALTER TABLE `modules` 
DROP COLUMN `mod_description`;

-- Set text type fields to 'null' as default
ALTER TABLE `competencies` 
CHANGE COLUMN `competence_desc` `competence_desc` TEXT NULL;

ALTER TABLE `email_templates` 
CHANGE COLUMN `email_subject` `email_subject` TINYTEXT NULL,
CHANGE COLUMN `email_body` `email_body` TEXT NULL;

ALTER TABLE `courses` 
CHANGE COLUMN `crs_fullname` `crs_fullname` TEXT NULL;

ALTER TABLE `departments` 
CHANGE COLUMN `dept_name` `dept_name` TEXT NULL;

ALTER TABLE `exam_sessions` 
CHANGE COLUMN `session_description` `session_description` TEXT NULL;

ALTER TABLE `exams` 
CHANGE COLUMN `exam_name` `exam_name` TEXT NULL, 
CHANGE COLUMN `exam_description` `exam_description` TEXT NULL;

ALTER TABLE `features` 
CHANGE COLUMN `feature_description` `feature_description` TEXT NULL;

ALTER TABLE `forms` 
CHANGE COLUMN `form_name` `form_name` TEXT NULL,
CHANGE COLUMN `form_description` `form_description` TEXT NULL;

ALTER TABLE `item_options` 
CHANGE COLUMN `ans_option_text` `ans_option_text` TEXT NULL;

ALTER TABLE `modules` 
CHANGE COLUMN `mod_name` `mod_name` TEXT NULL;

ALTER TABLE `rating_scale_defaults` 
CHANGE COLUMN `rcd_text` `rcd_text` TEXT NULL;

ALTER TABLE `schools` 
CHANGE COLUMN `school_description` `school_description` TEXT NULL;

ALTER TABLE `section_feedback` 
CHANGE COLUMN `feedback_text` `feedback_text` TEXT NULL;

ALTER TABLE `section_items` 
CHANGE COLUMN `item_desc` `item_desc` TEXT NULL;

ALTER TABLE `session_groups` 
CHANGE COLUMN `group_name` `group_name` TEXT NULL;

ALTER TABLE `sms_messages` 
CHANGE COLUMN `sms_message` `sms_message` TEXT NULL;

ALTER TABLE `students` 
CHANGE COLUMN `stu_fname` `stu_fname` TEXT NULL,
CHANGE COLUMN `stu_surname` `stu_surname` TEXT NULL;

ALTER TABLE `terms` 
CHANGE COLUMN `term_desc` `term_desc` TEXT NULL;

ALTER TABLE `users` 
CHANGE COLUMN `sname` `sname` TEXT NULL,
CHANGE COLUMN `fname` `fname` TEXT NULL;

-- Log that the current version of the database is 50, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (50);
