-- Author: David Cunningham
-- Date:   15/12/2015
-- OMIS database upgrade script version 1.9.63 => 1.9.64

-- Adds borderline and fail type flags to the rating scale values
ALTER TABLE `rating_scale_values` 
ADD COLUMN `scale_value_borderline` TINYINT(1) UNSIGNED NULL DEFAULT 0 COMMENT 'Is scale value a borderline type' AFTER `scale_value_description`,
ADD COLUMN `scale_value_fail` TINYINT(1) UNSIGNED NULL DEFAULT 0 COMMENT 'Is scale value a fail type' AFTER `scale_value_borderline`;

-- Clean up time. Remove obsolete feature records if they exist
DELETE FROM `feature_roles` WHERE `feature_id`='advancedsettings' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='advancedsettings' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='advancedsettings' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='advancedsettings' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='bgm' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='bgm' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='bgm' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='bgm' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='bse' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='bse' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='bse' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='bse' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='cba' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='cba' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='cba' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='cba' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='cronbach' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='cronbach' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='cronbach' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='cronbach' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='dr1' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='dr1' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='dr1' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='dr1' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='efc' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='efc' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='efc' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='efc' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='eis' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='eis' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='eis' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='eis' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='ess' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='ess' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='ess' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='ess' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='fc' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='fc' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='fc' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='fc' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='grs' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='grs' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='grs' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='grs' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='po1' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='po1' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='po1' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='po1' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='sd' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='sd' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='sd' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='sd' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='seh' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='seh' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='seh' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='seh' and`feature_role`='5';
DELETE FROM `feature_roles` WHERE `feature_id`='sfs' and`feature_role`='1';
DELETE FROM `feature_roles` WHERE `feature_id`='sfs' and`feature_role`='2';
DELETE FROM `feature_roles` WHERE `feature_id`='sfs' and`feature_role`='3';
DELETE FROM `feature_roles` WHERE `feature_id`='sfs' and`feature_role`='5';

-- Link feature_roles table with the roles and features table
ALTER TABLE `feature_roles` 
ADD INDEX `feature_roles_roles_fk_idx` (`feature_role` ASC);
ALTER TABLE `feature_roles` 
ADD CONSTRAINT `feature_roles_features_fk`
  FOREIGN KEY (`feature_id`)
  REFERENCES `features` (`feature_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `feature_roles_roles_fk`
  FOREIGN KEY (`feature_role`)
  REFERENCES `roles` (`role_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Log that the current version of the database is 64, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (64);
