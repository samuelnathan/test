-- Author: David Cunningham
-- Date: 27/11/2018
-- HEE RFC 19 - new feedback per item feature
-- Version 1.9.114 => 1.9.115

CREATE TABLE `item_feedback` (
  `item_feedback_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `result_id` BIGINT(10) NOT NULL,
  `item_id` BIGINT(10) NOT NULL,
  `feedback_text` TEXT NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`item_feedback_id`),
  INDEX `item_feedback_result_fk_idx` (`result_id` ASC),
  INDEX `item_feedback_item_fk_idx` (`item_id` ASC),
  CONSTRAINT `item_feedback_result_fk`
    FOREIGN KEY (`result_id`)
    REFERENCES `student_results` (`result_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `item_feedback_item_fk`
    FOREIGN KEY (`item_id`)
    REFERENCES `section_items` (`item_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'Table to capture feedback per scoresheet item (optional to feedback per section)';

ALTER TABLE `form_sections`
ADD COLUMN `item_feedback` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Show feedback field per item yes/no' AFTER `section_feedback`,
CHANGE COLUMN `feedback_entry` `section_feedback` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Show feedback field per section yes/no' ;

-- Deleted backup table for item feedback
CREATE TABLE item_feedback_deleted LIKE item_feedback;

-- Make null default for both fields
ALTER TABLE `item_feedback_deleted`
  CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ,
  CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `item_feedback_deleted`
  CHANGE COLUMN `item_feedback_id` `item_feedback_id` BIGINT(10) NOT NULL ,
  COMMENT = 'Backup table to capture feedback per scoresheet item deleted' ;

ALTER TABLE `item_feedback_deleted`
  CHANGE COLUMN `item_id` `item_id` BIGINT(10) NULL DEFAULT NULL ;

ALTER TABLE `item_feedback_deleted`
  ADD CONSTRAINT `item_feedback_deleted_result_fk`
    FOREIGN KEY (`result_id`)
      REFERENCES `student_results_deleted` (`result_id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  ADD CONSTRAINT `item_feedback_deleted_item_fk`
  FOREIGN KEY (`item_id`)
  REFERENCES `section_items` (`item_id`)
  ON DELETE SET NULL
ON UPDATE CASCADE;

-- Re-adjust foreign key restraints, on delete now sets null

ALTER TABLE `section_feedback_deleted`
DROP FOREIGN KEY `feedback_deleted_section_fk`;
ALTER TABLE `section_feedback_deleted`
  CHANGE COLUMN `section_id` `section_id` BIGINT(10) NULL ;
ALTER TABLE `section_feedback_deleted`
  ADD CONSTRAINT `feedback_deleted_section_fk`
    FOREIGN KEY (`section_id`)
      REFERENCES `form_sections` (`section_id`)
      ON DELETE SET NULL
      ON UPDATE CASCADE;

ALTER TABLE `student_results_deleted`
DROP FOREIGN KEY `results_delete_examiner_fk`,
DROP FOREIGN KEY `results_deleted_by_fk`,
DROP FOREIGN KEY `results_deleted_station_fk`,
DROP FOREIGN KEY `results_deleted_student_fk`;
ALTER TABLE `student_results_deleted`
  CHANGE COLUMN `station_id` `station_id` BIGINT(10) NULL DEFAULT '0' ,
  CHANGE COLUMN `student_id` `student_id` VARCHAR(30) NULL ,
  CHANGE COLUMN `deleted_by` `deleted_by` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL ;
ALTER TABLE `student_results_deleted`
  ADD CONSTRAINT `results_delete_examiner_fk`
    FOREIGN KEY (`examiner_id`)
      REFERENCES `users` (`user_id`)
      ON DELETE SET NULL
      ON UPDATE CASCADE,
  ADD CONSTRAINT `results_deleted_by_fk`
  FOREIGN KEY (`deleted_by`)
  REFERENCES `users` (`user_id`)
  ON DELETE SET NULL
ON UPDATE CASCADE,
     ADD CONSTRAINT `results_deleted_station_fk`
     FOREIGN KEY (`station_id`)
     REFERENCES `session_stations` (`station_id`)
   ON DELETE SET NULL
ON UPDATE CASCADE,
     ADD CONSTRAINT `results_deleted_student_fk`
     FOREIGN KEY (`student_id`)
     REFERENCES `students` (`student_id`)
   ON DELETE SET NULL
ON UPDATE CASCADE;

ALTER TABLE `item_scores_deleted`
DROP FOREIGN KEY `scores_deleted_item_fk`;
ALTER TABLE `item_scores_deleted`
  CHANGE COLUMN `item_id` `item_id` BIGINT(10) NULL ;
ALTER TABLE `item_scores_deleted`
  ADD CONSTRAINT `scores_deleted_item_fk`
    FOREIGN KEY (`item_id`)
      REFERENCES `section_items` (`item_id`)
      ON DELETE SET NULL
      ON UPDATE CASCADE;

-- Self assessment ownership adjustment
ALTER TABLE `self_assessment_ownerships`
DROP FOREIGN KEY `self_assessment_ownership_users_user_id_fk`;
ALTER TABLE self_assessment_ownerships`
DROP INDEX `self_assessment_ownership_users_user_id_fk`;

-- Log that the current version of the database is 115, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (115, 0);

