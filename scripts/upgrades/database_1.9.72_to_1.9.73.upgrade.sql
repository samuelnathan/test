-- Author: David Cunningham
-- Date: 22/06/2016
-- OMIS database upgrade script version 1.9.72 => 1.9.73

-- Adds page: export/export_student_groups.php to access control
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (193, 'export_student_groups');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (193, 1, 1), (193, 2, 0), (193, 3, 1), (193, 4, 0), (193, 5, 1), (193, 6, 1);

-- Log that the current version of the database is 73, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (73);
