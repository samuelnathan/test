-- Author: David Cunningham
-- Date: 15/11/2018
-- delete archived table
-- Version 1.9.113.1 => 1.9.113.2

CREATE TABLE student_results_deleted LIKE student_results;

ALTER TABLE `student_results_deleted`
ADD COLUMN `deleted_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `updated_at`,
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL ,
CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ;

-- Make null default for both fields
ALTER TABLE `student_results_deleted`
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ,
CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ;


-- Adds deleted by column to record identifier of person who deleted the results records
ALTER TABLE `student_results_deleted`
ADD COLUMN `deleted_by` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL AFTER `deleted_at`,
ADD INDEX `results_deleted_by_fk_idx` (`deleted_by` ASC);
;

-- Remove AI from primary key
ALTER TABLE `student_results_deleted`
CHANGE COLUMN `result_id` `result_id` BIGINT(10) NOT NULL ;

ALTER TABLE `student_results_deleted`
ADD INDEX `results_delete_examiner_fk_idx` (`examiner_id` ASC),
ADD INDEX `results_deleted_station_fk_idx` (`station_id` ASC),
ADD INDEX `results_deleted_student_fk_idx` (`student_id` ASC);
;
ALTER TABLE `student_results_deleted`
ADD CONSTRAINT `results_deleted_by_fk`
  FOREIGN KEY (`deleted_by`)
  REFERENCES `users` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `results_delete_examiner_fk`
  FOREIGN KEY (`examiner_id`)
  REFERENCES `users` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `results_deleted_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE,
ADD CONSTRAINT `results_deleted_station_fk`
  FOREIGN KEY (`station_id`)
  REFERENCES `session_stations` (`station_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

-- Now for the item scores deleted table
CREATE TABLE `item_scores_deleted` LIKE item_scores;

-- Make null default for both fields
ALTER TABLE `item_scores_deleted`
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ,
CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ;

-- Remove AI from primary key
ALTER TABLE `item_scores_deleted`
CHANGE COLUMN `score_id` `score_id` BIGINT(10) NOT NULL ;

ALTER TABLE `item_scores_deleted`
ADD CONSTRAINT `scores_deleted_result_fk`
  FOREIGN KEY (`result_id`)
  REFERENCES `student_results_deleted` (`result_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `scores_deleted_item_fk`
  FOREIGN KEY (`item_id`)
  REFERENCES `section_items` (`item_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

-- Now for the section_feedback deleted table
CREATE TABLE `section_feedback_deleted` LIKE section_feedback;

-- Make null default for both fields
ALTER TABLE `section_feedback_deleted`
CHANGE COLUMN `created_at` `created_at` TIMESTAMP NULL DEFAULT NULL ,
CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL DEFAULT NULL ;

-- Remove AI from primary key
ALTER TABLE `section_feedback_deleted`
CHANGE COLUMN `feedback_id` `feedback_id` BIGINT(10) NOT NULL ;

ALTER TABLE `section_feedback_deleted`
ADD CONSTRAINT `feedback_deleted_result_fk`
  FOREIGN KEY (`result_id`)
  REFERENCES `student_results_deleted` (`result_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `feedback_deleted_section_fk`
  FOREIGN KEY (`section_id`)
  REFERENCES `form_sections` (`section_id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

-- Log that the current version of the database is 113.2, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (113, 2);

