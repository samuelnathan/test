-- Author: David Cunningham
-- Date: 01/07/2019
-- NSHCS RFC - Exam Merge
-- Version 1.9.116 (HEE RFC) => 2.0.0 (NSHCS RFC and new UI)

-- Switch to semantic versioning (Major, Minor and Patch)
ALTER TABLE `database_versions` 
CHANGE COLUMN `major_version` `minor_version` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `minor_version` `patch_version` INT(11) NOT NULL DEFAULT '0' ;

ALTER TABLE `database_versions` 
ADD COLUMN `major_version` INT(11) NOT NULL DEFAULT '1' FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`major_version`, `minor_version`, `patch_version`);

-- PDF instructions related changes to database
ALTER TABLE `email_templates` 
ADD COLUMN `pdf_instructions` TEXT NULL DEFAULT NULL AFTER `email_body`,
ADD COLUMN `pdf_instructions_on_fail` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `pdf_instructions`,
ADD COLUMN `pdf_instructions_enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `pdf_instructions_on_fail`;

-- CC (carbon copy email) related changes to database
ALTER TABLE `email_templates` 
ADD COLUMN `email_cc` VARCHAR(320) NULL DEFAULT NULL  AFTER `template_name`;

-- Keep email address fields consistant (VARCHAR size: 320)
ALTER TABLE `users` 
CHANGE COLUMN `email` `email` VARCHAR(320) NULL DEFAULT NULL ;

ALTER TABLE `students` 
CHANGE COLUMN `email` `email` VARCHAR(320) NULL DEFAULT NULL ;

-- Bring 3 potential states for the pdf_instructions into one field using an ENUM data type.
ALTER TABLE `email_templates` 
DROP COLUMN `pdf_instructions_on_fail`,
CHANGE COLUMN `pdf_instructions_enabled` `pdf_instructions_enabled` ENUM("no", "yes", "fail") NOT NULL DEFAULT 'no' ;

SET SQL_SAFE_UPDATES = 0;
UPDATE email_templates  SET pdf_instructions_enabled = "no";
SET SQL_SAFE_UPDATES = 1;

-- Add pdf instructions field using the ENUM data type.
ALTER TABLE `student_feedback` 
ADD COLUMN `receives_instructions` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_sent`;

-- Form section user preset for the feedback tool
ALTER TABLE `user_presets` 
ADD COLUMN `feedback_section_titles` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `feedback_observer_ratings`,
ADD COLUMN `user_presetscol` VARCHAR(45) NULL AFTER `updated_at`;

-- Adds the merge option (Qpercom Analyse) as a feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("qpercom-analyse", "Qpercom Analyse - Advanced Analysis", 2);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("qpercom-analyse", 1, 1), ("qpercom-analyse", 2, 0), ("qpercom-analyse", 3, 0), ("qpercom-analyse", 5, 0), ("qpercom-analyse", 7, 1);

-- Log that the current version of the database is 2.0.0, and it was added now.
INSERT INTO `database_versions` (`major_version`, `minor_version`, `patch_version`) VALUES ('2', '0', '0');

