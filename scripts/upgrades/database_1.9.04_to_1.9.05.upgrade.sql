-- Original Author: David Cunningham
-- For Qpercom Ltd
-- Date: 03/03/2014
-- © 2014 Qpercom Limited. All rights reserved.
-- Omis database upgrade script version 1.9.4 => 1.9.5

-- Update page no '35' in pages table set name 'ajx_ud_sname' => 'ajx_update_form_info'
UPDATE pages SET page_name='ajx_update_form_info' WHERE page_id='35';

-- Log that the current version of the database is 5, and it was added now.
INSERT INTO `database` (`version`) VALUES (5);