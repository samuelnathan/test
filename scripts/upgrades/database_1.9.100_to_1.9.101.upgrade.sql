-- Author: David Cunningham
-- Date: 16/05/2017
-- OMIS database upgrade script version 1.9.100 => 1.9.101
-- Add new feature to allow for the user to download copies of the PDFs containing
-- the student/candidate feedback
-- Add new page 'download_feedback.php'

-- Feredback download feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("feedback-download", "PDF Files Download Feedback System", 4);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("feedback-download", 1, 0), ("feedback-download", 2, 0), ("feedback-download", 3, 0), ("feedback-download", 5, 0);

INSERT INTO pages (page_id, page_name) VALUES (200, 'download_feedback');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (200, 1, 1), (200, 2, 0), (200, 3, 1), (200, 4, 0), (200, 5, 1), (200, 6, 1);

-- Log that the current version of the database is 101, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (101);
