-- Author: David Cunningham
-- Date: 16/11/2017
-- Version 1.9.107 => 1.9.108
-- Adds new field to a scoresheet section enabling/disabling feedback entry to Observe

ALTER TABLE `form_sections` 
ADD COLUMN `feedback_entry` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Show feedback field yes/no' AFTER `feedback_required`;

-- Drop compulsory feedback feature, it's just silly. Feature should come as standard with application 
DELETE FROM `features` WHERE `feature_id`='compulsory-feedback';

-- Add page required
INSERT INTO `pages` (`page_name`) VALUES ('checkbox_option_edit');
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,2,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,4,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,6,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (206,7,1);

INSERT INTO `pages` (`page_name`) VALUES ('checkbox_option_add');
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,2,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,4,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,6,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (207,7,1);

INSERT INTO `pages` (`page_name`) VALUES ('checkbox_option_save');
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,2,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,4,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,6,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (208,7,1);

-- Log that the current version of the database is 108, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (108);
