-- Author: David Cunningham
-- Date:   17/09/2015
-- OMIS database upgrade script version 1.9.56 => 1.9.57

-- Removes results column settings from the exams table and places them 
-- in a new table named 'exam_settings' to store specific settings per exam.

-- Adds new column 'results_examiner' to the 'exam_settings' table. Note: these fields can 
-- be configured via the advanced settings in the exam section.

-- Adds an 'AFTER_INSERT' trigger to the exams table to automatically create a 
-- new settings record in the new 'exam_settings' for each new exam record  


CREATE TABLE `exam_settings` (
  `exam_setting_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `exam_id` BIGINT(10) NOT NULL COMMENT 'ID number for the exam, links back to the exams table',
  `results_column_identifier` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show student identifier column in the results section during the exam',
  `results_column_surname` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the student surname column in the results section during the exam',
  `results_column_forename` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the student forename column in the results section during the exam',
  `results_column_session` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the session name column in the results section during the exam',
  `results_column_date` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the session date column in the results section during the exam',
  `results_column_group` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the student group column in the results section during the exam',
  `results_column_form` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the form name column in the results section during the exam',
  `results_column_number` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the station number column in the results section during the exam',
  `results_column_examiner` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the examiner column in the results section during the exam',
  `results_column_pass` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the pass score column in the results section during the exam',
  `results_column_score` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the score column in the results section during the exam',
  `results_column_percentage` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the score percentage column in the results section during the exam',
  `results_column_grs` INT(1) NOT NULL DEFAULT 1 COMMENT 'Show the Global Rating Scale (GRS) column in the results section during the exam',
  PRIMARY KEY (`exam_setting_id`),
  INDEX `settings_exam_fk_idx` (`exam_id` ASC),
  CONSTRAINT `settings_exam_fk`
    FOREIGN KEY (`exam_id`)
    REFERENCES `exams` (`exam_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'This table contains more specific settings per exam';

INSERT INTO exam_settings (exam_id, results_column_identifier, results_column_surname, results_column_forename, 
             results_column_session, results_column_date, results_column_group, results_column_form, results_column_number,
             results_column_pass, results_column_score, results_column_percentage, results_column_grs)
SELECT exam_id, results_identifier, results_surname, results_forename, results_session, 
       results_date, results_group, results_form, results_number, 
       results_pass, results_score, results_percentage, results_grs
FROM exams;

ALTER TABLE `exams` 
DROP COLUMN `results_grs`,
DROP COLUMN `results_percentage`,
DROP COLUMN `results_score`,
DROP COLUMN `results_pass`,
DROP COLUMN `results_number`,
DROP COLUMN `results_form`,
DROP COLUMN `results_group`,
DROP COLUMN `results_date`,
DROP COLUMN `results_session`,
DROP COLUMN `results_forename`,
DROP COLUMN `results_surname`,
DROP COLUMN `results_identifier`;

DELIMITER $$

DROP TRIGGER IF EXISTS exams_AFTER_INSERT$$
CREATE TRIGGER `exams_AFTER_INSERT` AFTER INSERT ON `exams` FOR EACH ROW
BEGIN
   INSERT INTO exam_settings (exam_id) VALUES (NEW.exam_id);
END
$$

DELIMITER ;

-- Log that the current version of the database is 57, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (57);
