-- Upgrade script for the default feature permissions in Omis 1.9
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   12/12/2014
-- Omis database upgrade script version 1.9.38 => 1.9.39

-- Allow Station Managers to change station options (including notes) by default
DELETE FROM `feature_levels` WHERE `feature_id` = 'sd' AND `feature_level` = 5;
INSERT INTO `feature_levels` (`feature_id`, `feature_level`, `level_enabled`) VALUES ('sd', 5, 1);

-- Log that the current version of the database is 39, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (39);