-- Author: David Cunningham
-- Date: 10/02/2016
-- OMIS database upgrade script version 1.9.93 => 1.9.94
-- Adds "sticky" fields for the feedback option panel (presets)

CREATE TABLE `user_presets` (
  `user_preset_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `user_id` VARCHAR(30) CHARACTER SET 'utf8' COLLATE utf8_bin NOT NULL,
  `filter_department` VARCHAR(20) NULL DEFAULT NULL,
  `filter_school` VARCHAR(20) NULL DEFAULT NULL,
  `filter_course` VARCHAR(20) NULL DEFAULT NULL,
  `filter_year` VARCHAR(20) NULL DEFAULT NULL,
  `filter_module` VARCHAR(20) NULL DEFAULT NULL,
  `feedback_score_text` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_score_descriptors` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_score_values` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_nonscore_text` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_nonscore_descriptors` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_nonscore_values` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_grs_text` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_grs_descriptors` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_comments` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_finalscore_raw` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_finalscore_percent` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_summary` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_summary_scores` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_summary_grs` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_summary_outcomes` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_summary_overall_score` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
  `feedback_summary_overall_outcome` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_radarplot_comparison` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_radarplot_competency` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_multiexaminer_ratings` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_observer_ratings` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_preset_id`, `user_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE utf8_general_ci;

ALTER TABLE `user_presets` 
ADD INDEX `user_presets_fk_idx` (`user_id` ASC);
ALTER TABLE `user_presets` 
ADD CONSTRAINT `user_presets_fk`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`user_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `users` 
DROP COLUMN `pref_module`,
DROP COLUMN `pref_course`,
DROP COLUMN `pref_school`,
DROP COLUMN `pref_dept`;

-- Add an AFTER INSERT trigger on `users` table so that 'users_presets' 
-- table is populated with a matching record per user 
DELIMITER $$
DROP TRIGGER IF EXISTS users_AFTER_INSERT$$
CREATE TRIGGER `users_AFTER_INSERT` AFTER INSERT ON `users` FOR EACH ROW
BEGIN
   INSERT INTO user_presets (user_id) VALUES (NEW.user_id);
END
$$
DELIMITER ;

-- Log that the current version of the database is 94, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (94);
