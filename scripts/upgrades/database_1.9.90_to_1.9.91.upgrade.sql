-- Author: David Cunningham
-- Date: 05/12/2016
-- OMIS database upgrade script version 1.9.90 => 1.9.91

-- Renames graph page from 'radar_exmee' to 'radar_student_stations' 
UPDATE `pages` SET `page_name`='radar_student_stations' WHERE `page_id`='116';

-- Renames graph page from 'stat_items_mean' to 'station_items_mean'
UPDATE `pages` SET `page_name`='station_items_mean' WHERE `page_id`='130';

-- 2 new visual (flashing) alarm fields for the station timer
ALTER TABLE `session_stations` 
ADD COLUMN `visual_alarm1` TIME NOT NULL DEFAULT '00:02:00' AFTER `station_time`,
ADD COLUMN `visual_alarm2` TIME NOT NULL DEFAULT '00:00:00' AFTER `visual_alarm1`;

-- Log that the current version of the database is 91, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (91);
