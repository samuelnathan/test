-- Author: David Cunningham
-- Date:   01/09/2015
-- Omis database upgrade script version 1.9.52 => 1.9.53
-- Add new page 'requestAssistance' for the request for assistance
-- feature in the assessment tool. Also remove no longer required pages for same feature

-- Add new page 'requestAssistance'
INSERT INTO pages (page_id, page_name) VALUES (190, 'requestAssistance');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (190, 1, 1), (190, 2, 1), (190, 3, 1), (190, 4, 1), (190, 5, 1), (190, 6, 1);

-- Remove page 'ajx_support_ie'
DELETE FROM `page_levels` WHERE `page_id`='29';
DELETE FROM `pages` WHERE `page_id`='29';

-- Remove page 'ajx_support_be'
DELETE FROM `page_levels` WHERE `page_id`='30';
DELETE FROM `pages` WHERE `page_id`='30';

-- Remove page 'ajx_support_sg'
DELETE FROM `page_levels` WHERE `page_id`='31';
DELETE FROM `pages` WHERE `page_id`='31';

-- Remove page 'ajx_support_uk'
DELETE FROM `page_levels` WHERE `page_id`='32';
DELETE FROM `pages` WHERE `page_id`='32';

-- Log that the current version of the database is 53, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (53);
