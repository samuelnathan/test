-- Author: David Cunningham
-- Date: 11/08/2016
-- OMIS database upgrade script version 1.9.75 => 1.9.76

-- Adds page: export/export_student_groups.php to access control
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (194, 'json_department_exams');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (194, 1, 1), (194, 2, 0), (194, 3, 1), (194, 4, 0), (194, 5, 1), (194, 6, 1);

-- Colour Matching
ALTER TABLE `colour_matching` 
CHARACTER SET = DEFAULT ,
ADD COLUMN `circuit_colour_light` VARCHAR(30) NULL DEFAULT NULL AFTER `circuit_colour`;

-- Populate light colours
SET SQL_SAFE_UPDATES = 0;
UPDATE colour_matching SET circuit_colour_light = "FF4C4C" WHERE colour_name = "Red";
UPDATE colour_matching SET circuit_colour_light = "3399FF" WHERE colour_name = "Blue";
UPDATE colour_matching SET circuit_colour_light = "FFFF66" WHERE colour_name = "Yellow";
UPDATE colour_matching SET circuit_colour_light = "B2FF66" WHERE colour_name = "Green";
UPDATE colour_matching SET circuit_colour_light = "FFB266" WHERE colour_name = "Orange";
UPDATE colour_matching SET circuit_colour_light = "FF66FF" WHERE colour_name = "Purple";
UPDATE colour_matching SET circuit_colour_light = "FF9999" WHERE colour_name = "Pink";
UPDATE colour_matching SET circuit_colour_light = "6BBBAE" WHERE colour_name = "Teal";
UPDATE colour_matching SET circuit_colour_light = "FFE766" WHERE colour_name = "Gold";
UPDATE colour_matching SET circuit_colour_light = "E6E6E6" WHERE colour_name = "Silver";
SET SQL_SAFE_UPDATES = 1;

-- Log that the current version of the database is 76, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (76);
