-- Adding table for the labeling of weighting options for multiple examiners
-- Author: Cormac McSwiney <cormac.mcswiney@qpercom.ie>
-- Date:   15/05/2014
-- Omis database upgrade script version 1.9.19 => 1.9.20


CREATE TABLE colour_matching (circuit_colour VARCHAR(30), lang VARCHAR(10), colour_name VARCHAR(30));

INSERT INTO colour_matching VALUES ('FF0000', 'en', 'Red');
INSERT INTO colour_matching VALUES ('0000FF', 'en', 'Blue');
INSERT INTO colour_matching VALUES ('FFFF00', 'en', 'Yellow');
INSERT INTO colour_matching VALUES ('008000', 'en', 'Green');
INSERT INTO colour_matching VALUES ('FFA500', 'en', 'Orange');
INSERT INTO colour_matching VALUES ('800080', 'en', 'Purple');
INSERT INTO colour_matching VALUES ('FFC0CB', 'en', 'Pink');
INSERT INTO colour_matching VALUES ('008080', 'en', 'Teal');
INSERT INTO colour_matching VALUES ('FFD700', 'en', 'Gold');
INSERT INTO colour_matching VALUES ('C0C0C0', 'en', 'Silver');

INSERT INTO `database` (`version`) VALUES (20);