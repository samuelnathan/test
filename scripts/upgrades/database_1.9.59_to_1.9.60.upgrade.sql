-- Author: Domhnall Walsh
-- Date:   21/10/2015
-- OMIS database upgrade script version 1.9.59 => 1.9.60
-- Roles table: To facilitate identifying user types (i.e. roles) by name, so
-- that we can map that to a specific Eloquent model, I'm adding a new field 
-- called 'model' to the roles table and populating it with the names of the
-- models that correspond with each user type.

-- Add the new column to the table.
ALTER TABLE `roles` 
ADD COLUMN `model` VARCHAR(20) NULL COMMENT 'Name of matching database model' AFTER `role_importexport_field`,
ADD UNIQUE INDEX `model_UNIQUE` (`model` ASC)  COMMENT '';

-- Populate the new column with the relevant data.
UPDATE `roles` SET `model`='Admin' WHERE `role_id`='1';
UPDATE `roles` SET `model`='Examiner' WHERE `role_id`='2';
UPDATE `roles` SET `model`='ExamAdmin' WHERE `role_id`='3';
UPDATE `roles` SET `model`='Assistant' WHERE `role_id`='4';
UPDATE `roles` SET `model`='StationManager' WHERE `role_id`='5';
UPDATE `roles` SET `model`='SuperAdmin' WHERE `role_id`='6';

-- Log that the current version of the database is 60, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (60);

