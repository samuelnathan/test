-- Adding table for the labeling of weighting options for multiple examiners
-- Author: Cormac McSwiney <cormac.mcswiney@qpercom.ie>
-- Date:   13/05/2014
-- Omis database upgrade script version 1.9.17 => 1.9.18

CREATE TABLE weighting
(
scoring_weight int(10) NOT NULL,
weight_type varchar(30) NOT NULL,
PRIMARY KEY (scoring_weight)
);

INSERT INTO weighting (scoring_weight, weight_type)
VALUES ('0','Observer');
INSERT INTO weighting (scoring_weight, weight_type)
VALUES ('1','Low Weighting');
INSERT INTO weighting (scoring_weight, weight_type)
VALUES ('2','Regular Weighting');
INSERT INTO weighting (scoring_weight, weight_type)
VALUES ('3','High Weighting');

INSERT INTO `database` (`version`) VALUES (18);