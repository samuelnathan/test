-- Last update script (1.9.42 -> 1.9.43) was extensively modified AFTER being
-- committed, this makes maintaining database integrity unreliable when we're
-- depending on the fact that when the script has been applied, it has been
-- applied IN FULL.
--
-- This may seem redundant, but if an update script has its functionality
-- changed after it's applied to a live system, then those "new" database
-- operations might never get run and potentially put the database in a state
-- where stuff will break when later database scripts are run that assume they
-- have been, and break the update chain. This is very important.
--
-- As such, we need to ensure that those changes are run (but only once) to
-- ensure that the DB is valid and correct; in a stroke of luck, the changed
-- bits can be run safely a second time with no corruption/loss of data.
--
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   29/04/2015
-- Omis database upgrade script version 1.9.43 => 1.9.44

-- [Add outstanding new features for 1.9]
-- Multiple examiners
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("multi-examiners", "Multiple Examiners", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("multi-examiners", 1, 0), ("multi-examiners", 2, 0), ("multi-examiners", 3, 0), ("multi-examiners", 5, 0);

-- Student images
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("student-images", "Student Profile Images", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("student-images", 1, 0), ("student-images", 2, 0), ("student-images", 3, 0), ("student-images", 5, 0);

-- Global Rating Scale management (enable for admins)
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("grs-management", "Global Rating Scale Management", 2);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("grs-management", 1, 0), ("grs-management", 2, 0), ("grs-management", 3, 0), ("grs-management", 5, 0);

-- Circuit colours
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("circuit-colours", "Examination Circuit Colours", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("circuit-colours", 1, 0), ("circuit-colours", 2, 0), ("circuit-colours", 3, 0), ("circuit-colours", 5, 0);

-- Change feature ID's
UPDATE `features` SET `feature_id`='advanced-settings' WHERE `feature_id`='advancedsettings';
UPDATE `features` SET `feature_id`='bl-group' WHERE `feature_id`='bgm';
UPDATE `features` SET `feature_id`='bl-station-export' WHERE `feature_id`='bse';
UPDATE `features` SET `feature_id`='bl-complete-export' WHERE `feature_id`='cba';
UPDATE `features` SET `feature_id`='cronbach-analysis' WHERE `feature_id`='cronbach';
UPDATE `features` SET `feature_id`='delete-results' WHERE `feature_id`='dr1';
UPDATE `features` SET `feature_id`='feedback-column' WHERE `feature_id`='efc';
UPDATE `features` SET `feature_id`='import-export-forms' WHERE `feature_id`='eis';
UPDATE `features` SET `feature_id`='session-scheduler' WHERE `feature_id`='ess';
UPDATE `features` SET `feature_id`='compulsory-feedback' WHERE `feature_id`='fc';
UPDATE `features` SET `feature_id`='grs-column' WHERE `feature_id`='grs';
UPDATE `features` SET `feature_id`='pdf-forms' WHERE `feature_id`='po1';
UPDATE `features` SET `feature_id`='station-notes' WHERE `feature_id`='sd';
UPDATE `features` SET `feature_id`='student-history' WHERE `feature_id`='seh';
UPDATE `features` SET `feature_id`='feedback-system' WHERE `feature_id`='sfs';

UPDATE `feature_roles` SET `feature_id`='advanced-settings' WHERE `feature_id`='advancedsettings';
UPDATE `feature_roles` SET `feature_id`='bl-group' WHERE `feature_id`='bgm';
UPDATE `feature_roles` SET `feature_id`='bl-station-export' WHERE `feature_id`='bse';
UPDATE `feature_roles` SET `feature_id`='bl-complete-export' WHERE `feature_id`='cba';
UPDATE `feature_roles` SET `feature_id`='cronbach-analysis' WHERE `feature_id`='cronbach';
UPDATE `feature_roles` SET `feature_id`='delete-results' WHERE `feature_id`='dr1';
UPDATE `feature_roles` SET `feature_id`='feedback-column' WHERE `feature_id`='efc';
UPDATE `feature_roles` SET `feature_id`='import-export-forms' WHERE `feature_id`='eis';
UPDATE `feature_roles` SET `feature_id`='session-scheduler' WHERE `feature_id`='ess';
UPDATE `feature_roles` SET `feature_id`='compulsory-feedback' WHERE `feature_id`='fc';
UPDATE `feature_roles` SET `feature_id`='grs-column' WHERE `feature_id`='grs';
UPDATE `feature_roles` SET `feature_id`='pdf-forms' WHERE `feature_id`='po1';
UPDATE `feature_roles` SET `feature_id`='station-notes' WHERE `feature_id`='sd';
UPDATE `feature_roles` SET `feature_id`='student-history' WHERE `feature_id`='seh';
UPDATE `feature_roles` SET `feature_id`='feedback-system' WHERE `feature_id`='sfs';

-- Log that the current version of the database is now 44, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (44);