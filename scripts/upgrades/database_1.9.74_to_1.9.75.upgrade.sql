-- Author: David Cunningham
-- Date: 02/07/2016
-- OMIS database upgrade script version 1.9.74 => 1.9.75

-- Adds Exam / Student Matrix
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("exam-matrix", "Exam / Student Matrix", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("exam-matrix", 1, 0), ("exam-matrix", 2, 0), ("exam-matrix", 3, 0), ("exam-matrix", 5, 0);

-- Log that the current version of the database is 75, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (75);
