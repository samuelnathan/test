-- Author: David Cunningham
-- Date: /12/2016
-- OMIS database upgrade script version 1.9.95 => 1.9.96

-- Add new exam rule to database to set the divergence threshold
ALTER TABLE `exam_rules` 
ADD COLUMN `divergence_threshold` DECIMAL(9,3) UNSIGNED NULL DEFAULT NULL COMMENT 'divergence threshold decimal value' AFTER `multi_examiner_results_averaged`;

-- Log that the current version of the database is 96, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (96);
