-- Author: David Cunningham
-- Date: 17/01/2018
-- Version 1.9.109 => 1.9.110
-- RFC3: Virtual section to allow only one set of scores to be input based on agreement from panellists on that section - Delivery for UAT Dec 18th
-- RFC14: Self assessment upload of scores from Oriel via .csv file - Delivery for UAT Dec 18th

CREATE TABLE `self_assessments` (
  `self_assessment_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `exam_id` BIGINT(10) NOT NULL,
  `student_id` VARCHAR(30) NOT NULL,
  `title` VARCHAR(45) NULL,
  `max_score` INT(11) NULL,

  PRIMARY KEY (`self_assessment_id`),

  UNIQUE INDEX `exam_id_student_id_UNIQUE` (`exam_id` ASC, `student_id` ASC),

  CONSTRAINT `fk_selfassessments_exams`
    FOREIGN KEY (`exam_id`)
    REFERENCES `exams` (`exam_id`)
      ON DELETE CASCADE
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_selfassessments_students`
    FOREIGN KEY (`student_id`)
    REFERENCES `students` (`stu_id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE);

CREATE TABLE `self_assessment_items` (
  `self_assessment_item_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
  `self_assessment_id` BIGINT(10) NOT NULL,
  `question` TEXT NOT NULL,
  `score` INT NOT NULL,
  `response` TEXT NOT NULL,

  PRIMARY KEY (`self_assessment_item_id`),

  INDEX `fk_self_assessment_items_self_assessments_idx` (`self_assessment_id` ASC),
  CONSTRAINT `fk_self_assessment_items_self_assessments`
    FOREIGN KEY (`self_assessment_id`)
    REFERENCES `self_assessments` (`self_assessment_id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE);

-- Drop feature access to circuit-colours, now comes with base package
DELETE FROM `features` WHERE `feature_id`='circuit-colours';

  -- Add new page for cloning form assessment (scoresheet)
  INSERT IGNORE INTO `pages` (`page_name`) VALUES ('clone_redirect');

  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,1,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,2,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,3,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,4,0);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,5,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,6,1);
  INSERT IGNORE INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (209,7,1);

-- Add self assessment scoresheet option management
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES 
("self-assessments", "Import Self Assessment Scoresheets", 8);

INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES 
("self-assessments", 1, 0),
("self-assessments", 2, 0),
("self-assessments", 3, 0),
("self-assessments", 5, 0),
("self-assessments", 7, 0);

-- Change column width of modules (35 from 20)
ALTER TABLE `modules` 
CHANGE COLUMN `mod_id` `mod_id` VARCHAR(35) NOT NULL ;

ALTER TABLE `year_modules` 
DROP FOREIGN KEY `yearmodule_module_fk`;
ALTER TABLE `year_modules` 
CHANGE COLUMN `mod_id` `mod_id` VARCHAR(35) NOT NULL ;
ALTER TABLE `year_modules` 
ADD CONSTRAINT `yearmodule_module_fk`
  FOREIGN KEY (`mod_id`)
  REFERENCES `modules` (`mod_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `student_courses` 
DROP FOREIGN KEY `studentmodule_module_fk`;
ALTER TABLE `student_courses` 
CHANGE COLUMN `mod_id` `mod_id` VARCHAR(35) NOT NULL ;
ALTER TABLE `student_courses` 
ADD CONSTRAINT `studentmodule_module_fk`
  FOREIGN KEY (`mod_id`)
  REFERENCES `modules` (`mod_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Log that the current version of the database is 110, and it was added now.
INSERT IGNORE INTO `database_versions` (`version`) VALUES (110);

