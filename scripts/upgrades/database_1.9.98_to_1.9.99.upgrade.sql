-- Author: David Cunningham
-- Date: 10/05/2017
-- OMIS database upgrade script version 1.9.98 => 1.9.99
-- Default for auto config should be 1

ALTER TABLE `exam_settings` 
CHANGE COLUMN `auto_config_station` `auto_config_station` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 
COMMENT 'If set to on and examiner is assigned to station, then auto configure station when logged in'; 

-- Log that the current version of the database is 99, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (99);