-- Author: David Cunningham
-- Date: 28/01/2019
-- HEE RFC 18 - item weighting
-- Version 1.9.115 => 1.9.116

ALTER TABLE section_items
  ADD weighted_value DOUBLE(3, 2) DEFAULT 1.00 NOT NULL;

ALTER TABLE forms
  ADD total_weighted_value DECIMAL(9, 3) NULL;

ALTER TABLE user_presets
  ADD feedback_item_weighting tinyint(1) unsigned DEFAULT 0 NOT NULL;

CREATE TABLE `student_station_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `station_number` SMALLINT(5) UNSIGNED NOT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `station_number`, `session_id`),
   INDEX `outcome_student_fk_idx` (`student_id` ASC),
   CONSTRAINT `outcome_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcome_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE)
  ENGINE = InnoDB
  COMMENT = 'Table to store the station outcomes for students';

ALTER TABLE `session_stations`
    CHANGE COLUMN `station_number` `station_number` SMALLINT(5) UNSIGNED NOT NULL ;

ALTER TABLE item_scores ADD option_weighted_value DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `option_value`;

ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_raw_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `grade_updated`,
  ADD COLUMN `overall_weighted_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_raw_score`,
  ADD COLUMN `overall_possible_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_weighted_score`;

ALTER TABLE `student_results`
  ADD COLUMN `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
  ADD COLUMN `possible_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `weighted_score`,
  ADD COLUMN `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `possible_score`;

ALTER TABLE `student_results_deleted`
  ADD COLUMN `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
  ADD COLUMN `possible_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `weighted_score`,
  ADD COLUMN `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `possible_score`;

ALTER TABLE `item_scores_deleted` ADD option_weighted_value DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `option_value`;

-- Overall possible pass
ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_pass` DECIMAL(9,3) NULL DEFAULT NULL AFTER `overall_possible_score`;

ALTER TABLE `student_results` ADD examiner_grade TEXT DEFAULT '' AFTER `examiner_id`;
ALTER TABLE `student_results_deleted` ADD examiner_grade TEXT DEFAULT '' AFTER `examiner_id`;

ALTER TABLE `student_station_outcomes`
  ADD COLUMN `station_grade` TEXT DEFAULT '' AFTER `station_number`,
  ADD COLUMN `station_pass` DECIMAL(9,3) NULL DEFAULT NULL AFTER `station_grade`;

ALTER TABLE `student_exam_data`
  ADD COLUMN up_to_date TINYINT(1) DEFAULT 0 NOT NULL AFTER `overall_pass`;

CREATE TABLE `student_form_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `form_id`  BIGINT(10) NOT NULL,
   `passed` TINYINT(1) UNSIGNED DEFAULT NULL,
   `weighted_passed` TINYINT(1) UNSIGNED DEFAULT NULL,
   `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `form_id`, `session_id`),
   INDEX `outcomeform_student_fk_idx` (`student_id` ASC),
   CONSTRAINT `outcomeform_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeform_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeform_form_fk`
     FOREIGN KEY (`form_id`)
       REFERENCES `forms` (`form_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE
       )
  ENGINE = InnoDB
  COMMENT = 'Table to store the form outcomes for students';

-- Change name of 'station_tag' field to 'station_code' makes more sense when talking about
-- scenario codes/group codes
ALTER TABLE `session_stations`
  CHANGE COLUMN `station_tag` `station_code` VARCHAR(100) NULL DEFAULT NULL COMMENT 'Group stations by tag' ;

CREATE TABLE `student_code_outcomes` (
     `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
     `student_id` VARCHAR(30) NOT NULL,
     `session_id` BIGINT(10) NOT NULL,
     `station_code` VARCHAR(100) NULL DEFAULT NULL,
     `passed` TINYINT(1) UNSIGNED DEFAULT NULL,
     `weighted_passed` TINYINT(1) UNSIGNED DEFAULT NULL,
     `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL,
     `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `possible_raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `possible_weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
     `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     PRIMARY KEY (`outcome_id`),
     UNIQUE KEY (`student_id`, `session_id`, `station_code`),
     INDEX `outcomecode_student_fk_idx` (`student_id` ASC),
     CONSTRAINT `outcomecode_student_fk`
       FOREIGN KEY (`student_id`)
         REFERENCES `students` (`student_id`)
         ON DELETE CASCADE
         ON UPDATE CASCADE,
     CONSTRAINT `outcomecode_session_fk`
       FOREIGN KEY (`session_id`)
         REFERENCES `exam_sessions` (`session_id`)
         ON DELETE CASCADE
         ON UPDATE CASCADE
)
  ENGINE = InnoDB
  COMMENT = 'Table to store the station code outcomes for students';

-- Pass fields type/name changes (just run these to amend)
ALTER TABLE `student_results`
  CHANGE COLUMN `examiner_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `student_results_deleted`
  CHANGE COLUMN `examiner_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `student_station_outcomes`
  ADD COLUMN `weighted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `passed`,
  CHANGE COLUMN `station_grade` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
  CHANGE COLUMN `station_pass` `pass_possible` DECIMAL(9,3) NULL DEFAULT NULL ;

ALTER TABLE `student_exam_data`
  ADD COLUMN `weighted_grade` TEXT NULL DEFAULT '' AFTER `grade`;

ALTER TABLE `student_exam_data`
  ADD COLUMN `overall_possible_weighted_score` DECIMAL(9,3) NULL DEFAULT NULL AFTER overall_possible_score;

ALTER TABLE `student_exam_data`
  ADD COLUMN `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `grade_updated`,
  ADD COLUMN `weighted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `passed`;

CREATE TABLE `student_item_outcomes` (
   `outcome_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
   `student_id` VARCHAR(30) NOT NULL,
   `session_id` BIGINT(10) NOT NULL,
   `item_id` BIGINT(10) NOT NULL,
   `raw_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `weighted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000',
   `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY (`outcome_id`),
   UNIQUE KEY (`student_id`, `session_id`, `item_id`),
   INDEX `outcomeitem_student_fk_index` (`student_id` ASC, `item_id` ASC),
   CONSTRAINT `outcomeitem_student_fk`
     FOREIGN KEY (`student_id`)
       REFERENCES `students` (`student_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeitem_session_fk`
     FOREIGN KEY (`session_id`)
       REFERENCES `exam_sessions` (`session_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE,
   CONSTRAINT `outcomeitem_item_fk`
     FOREIGN KEY (`item_id`)
       REFERENCES `section_items` (`item_id`)
       ON DELETE CASCADE
       ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Table to store the item outcomes for students';


SET SQL_SAFE_UPDATES = 0;
SET @grace_value = '0.0';

-- UPDATE RESULTS FOR NOT SELF_ASSESSMENTS_OWNERSHIP
UPDATE student_results AS result INNER JOIN
(
  SELECT sr.result_id AS resultID,
        SUM(st.`highest_value`) sumPossibleValue,
        IF (((sr.score / SUM(st.`highest_value`)) * 100) - @grace_value >= ss.pass_value, 1, 0) passed,
        sr.score,
        (sr.score / SUM(st.`highest_value`) * 100) pecentage,
         ss.pass_value,
         @grace_value
  FROM student_results sr
    INNER JOIN session_stations ss ON sr.station_id = ss.station_id
    INNER JOIN item_scores its ON its.result_id = sr.result_id
    INNER JOIN section_items st ON its.item_id = st.item_id
    INNER JOIN form_sections fs ON fs.section_id = st.section_id AND fs.self_assessment = 0
  WHERE sr.result_id NOT IN
    (
      SELECT sr.result_id FROM student_results sr
        INNER JOIN session_stations ss ON sr.station_id = ss.station_id
        INNER JOIN exam_sessions ex ON ex.session_id = ss.session_id
        INNER JOIN self_assessments sa ON sa.exam_id = ex.exam_id
        INNER JOIN self_assessment_ownerships sao ON sao.self_assessment_id = sa.self_assessment_id AND sr.student_id = sao.student_id AND sr.examiner_id = sao.examiner_id
        INNER JOIN item_scores its ON its.result_id = sr.result_id
        INNER JOIN section_items st ON its.item_id = st.item_id
        INNER JOIN form_sections fs ON fs.section_id = st.section_id
    )
      GROUP BY  sr.result_id
) update_table
  ON result.result_id = update_table.resultID
  SET result.possible_score = update_table.sumPossibleValue,
      result.passed = update_table.passed,
      result.weighted_score = update_table.score,
      result.possible_weighted_score = update_table.score;

-- UPDATE RESULTS FOR SELF_ASSESSMENT_OWNERSHIP
UPDATE student_results AS result INNER JOIN
  (
    SELECT sr.result_id AS resultID,
        SUM(st.`highest_value`) sumPossibleValue,
        IF (((sr.score / SUM(st.`highest_value`)) * 100) - 0.0 >= ss.pass_value, 1, 0) passed,
        sr.score
    FROM student_results sr
      INNER JOIN session_stations ss ON sr.station_id = ss.station_id
      INNER JOIN exam_sessions ex ON ex.session_id = ss.session_id
      INNER JOIN self_assessments sa ON sa.exam_id = ex.exam_id
      INNER JOIN self_assessment_ownerships sao ON sao.self_assessment_id = sa.self_assessment_id AND sr.student_id = sao.student_id AND sr.examiner_id = sao.examiner_id
      INNER JOIN item_scores its ON its.result_id = sr.result_id
      INNER JOIN section_items st ON its.item_id = st.item_id
      INNER JOIN form_sections fs ON fs.section_id = st.section_id
    GROUP BY  sr.result_id
  ) update_table
    ON result.result_id = update_table.resultID
    SET result.possible_score = update_table.sumPossibleValue,
        result.passed = update_table.passed,
        result.weighted_score = update_table.score,
        result.possible_weighted_score = update_table.score;
SET SQL_SAFE_UPDATES = 1;

-- Log that the current version of the database is 116, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (116, 0);


-- Author: David Cunningham
-- Date: 01/07/2019
-- NSHCS RFC - Exam Merge
-- Version 1.9.116 (HEE RFC) => 2.1.0 (NSHCS RFC and new UI)

-- Switch to semantic versioning (Major, Minor and Patch)
ALTER TABLE `database_versions` 
CHANGE COLUMN `major_version` `minor_version` INT(11) NOT NULL DEFAULT '0' ,
CHANGE COLUMN `minor_version` `patch_version` INT(11) NOT NULL DEFAULT '0' ;

ALTER TABLE `database_versions` 
ADD COLUMN `major_version` INT(11) NOT NULL DEFAULT '1' FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`major_version`, `minor_version`, `patch_version`);

-- PDF instructions related changes to database
ALTER TABLE `email_templates` 
ADD COLUMN `pdf_instructions` TEXT NULL DEFAULT NULL AFTER `email_body`,
ADD COLUMN `pdf_instructions_on_fail` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `pdf_instructions`,
ADD COLUMN `pdf_instructions_enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `pdf_instructions_on_fail`;

-- CC (carbon copy email) related changes to database
ALTER TABLE `email_templates` 
ADD COLUMN `email_cc` VARCHAR(320) NULL DEFAULT NULL  AFTER `template_name`;

-- Keep email address fields consistant (VARCHAR size: 320)
ALTER TABLE `users` 
CHANGE COLUMN `email` `email` VARCHAR(320) NULL DEFAULT NULL ;

ALTER TABLE `students` 
CHANGE COLUMN `email` `email` VARCHAR(320) NULL DEFAULT NULL ;

-- Bring 3 potential states for the pdf_instructions into one field using an ENUM data type.
ALTER TABLE `email_templates` 
DROP COLUMN `pdf_instructions_on_fail`,
CHANGE COLUMN `pdf_instructions_enabled` `pdf_instructions_enabled` ENUM("no", "yes", "fail") NOT NULL DEFAULT 'no' ;

SET SQL_SAFE_UPDATES = 0;
UPDATE email_templates  SET pdf_instructions_enabled = "no";
SET SQL_SAFE_UPDATES = 1;

-- Add pdf instructions field using the ENUM data type.
ALTER TABLE `student_feedback` 
ADD COLUMN `receives_instructions` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_sent`;

-- Form section user preset for the feedback tool
ALTER TABLE `user_presets` 
ADD COLUMN `feedback_section_titles` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' AFTER `feedback_observer_ratings`,
ADD COLUMN `user_presetscol` VARCHAR(45) NULL AFTER `updated_at`;

-- Adds the merge option (Qpercom Analyse) as a feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("qpercom-analyse", "Qpercom Analyse - Advanced Analysis", 2);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("qpercom-analyse", 1, 1), ("qpercom-analyse", 2, 0), ("qpercom-analyse", 3, 0), ("qpercom-analyse", 5, 0), ("qpercom-analyse", 7, 1);

-- Log that the current version of the database is 2.0.0, and it was added now.
INSERT INTO `database_versions` (`major_version`, `minor_version`, `patch_version`) VALUES ('2', '0', '0');


-- Changes required for the NUS RFC go here and possibly the re-ajustments for the HEE RFC 18

DROP TABLE `student_form_outcomes`;
DROP TABLE `student_code_outcomes`;

ALTER TABLE `student_station_outcomes` 
RENAME TO  `student_results_outcomes` ;

ALTER TABLE `student_results_outcomes` 
CHANGE COLUMN `station_number` `combined_by` SMALLINT(5) UNSIGNED NOT NULL ;

ALTER TABLE `student_results_outcomes` 
CHANGE COLUMN `combined_by` `combined_by` VARCHAR(100) NOT NULL ,
ADD UNIQUE INDEX `combined_by_UNIQUE` (`combined_by` ASC),
ADD UNIQUE INDEX `session_id_UNIQUE` (`session_id` ASC),
ADD UNIQUE INDEX `student_id_UNIQUE` (`student_id` ASC);

ALTER TABLE `student_results_outcomes` 
CHANGE COLUMN `combined_by` `combined_by` VARCHAR(100) NOT NULL COMMENT 'Combination key either by the station number, form id, station code and in future possibly the session_id?' ;

ALTER TABLE `exam_rules` 
ADD COLUMN `combine_results_by` ENUM('station', 'form', 'code') NOT NULL DEFAULT 'station' COMMENT 'Combination rule by either by the station number, form id, station code and in future possibly the session_id?' AFTER `grade_weightings`;

-- Rename 'weighted' to 'adjusted' columns where relevant
ALTER TABLE `student_results_outcomes` 
CHANGE COLUMN `raw_score` `score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `combined_by`,
CHANGE COLUMN `possible_raw_score` `possible` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
CHANGE COLUMN `weighted_passed` `adjusted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `adjusted_possible`,
CHANGE COLUMN `weighted_score` `adjusted_score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ,
CHANGE COLUMN `possible_weighted_score` `adjusted_possible` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ;

ALTER TABLE `student_exam_data` 
CHANGE COLUMN `overall_raw_score` `score` DECIMAL(9,3) NULL DEFAULT NULL AFTER `grade_updated`,
CHANGE COLUMN `overall_possible_score` `possible` DECIMAL(9,3) NULL DEFAULT NULL AFTER `score`,
CHANGE COLUMN `overall_pass` `pass` DECIMAL(9,3) NULL DEFAULT NULL AFTER `passed`,
CHANGE COLUMN `weighted_passed` `adjusted_passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `adjusted_possible`,
CHANGE COLUMN `weighted_grade` `adjusted_grade` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `overall_weighted_score` `adjusted_score` DECIMAL(9,3) NULL DEFAULT NULL ,
CHANGE COLUMN `overall_possible_weighted_score` `adjusted_possible` DECIMAL(9,3) NULL DEFAULT NULL ;

ALTER TABLE `student_item_outcomes` 
CHANGE COLUMN `raw_score` `score` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ;

-- Global rating scale changes
ALTER TABLE `section_items` 
CHANGE COLUMN `scale` `grs_ref_id` TINYINT(3) UNSIGNED NULL COMMENT 'Identifier of rating scale type at time of creation' ;

ALTER TABLE `section_items` 
ADD COLUMN `grs` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'GRS scale enabled or disabled' AFTER `type`,
ADD COLUMN `grs_ref_name` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Name of rating scale type at time of creation' AFTER `grs_ref_id`;

SET SQL_SAFE_UPDATES = 0;
UPDATE section_items SET grs = 1 WHERE grs_ref_id > 0 AND type = "radiotext";
SET SQL_SAFE_UPDATES = 1;

SET SQL_SAFE_UPDATES = 0;
UPDATE section_items SET grs_ref_id = null WHERE grs_ref_id = 0;
SET SQL_SAFE_UPDATES = 1;

SET SQL_SAFE_UPDATES = 0;
UPDATE section_items 
INNER JOIN rating_scale_types 
ON section_items.grs_ref_id =  rating_scale_types.scale_type_id
SET section_items.grs_ref_name = rating_scale_types.scale_type_name;
SET SQL_SAFE_UPDATES = 1;

SET SQL_SAFE_UPDATES = 0;

 UPDATE item_options
   INNER JOIN section_items USING(item_id)
 SET item_options.borderline = 1    
 WHERE section_items.type = "radiotext" 
  AND section_items.grs = 1
  AND lower(item_options.descriptor) 
  IN (SELECT lower(rating_scale_values.scale_value_description) FROM rating_scale_values WHERE scale_value_borderline = 1 AND section_items.grs_ref_id = rating_scale_values.scale_type_id);

SET SQL_SAFE_UPDATES = 1;

SET SQL_SAFE_UPDATES = 0;

 UPDATE item_options
   INNER JOIN section_items USING(item_id)
 SET item_options.fail = 1    
 WHERE section_items.type = "radiotext" 
  AND section_items.grs = 1
  AND lower(item_options.descriptor) 
  IN (SELECT lower(rating_scale_values.scale_value_description) FROM rating_scale_values WHERE scale_value_fail = 1 AND section_items.grs_ref_id = rating_scale_values.scale_type_id);

SET SQL_SAFE_UPDATES = 1;

-- PLEASE DONT FORGET TO CHECK missed Global Rating Scale updates
SELECT * FROM item_options 
INNER JOIN section_items USING(item_id)
WHERE section_items.grs = 1 AND  item_options.borderline = 0 AND item_options.option_value IN (0,1) AND  item_options.descriptor LIKE "%borderline%";

SELECT * FROM item_options 
INNER JOIN section_items USING(item_id)
WHERE section_items.grs = 1 AND  item_options.fail = 0 AND item_options.option_value IN (0,1,2) AND  item_options.descriptor LIKE "%fail%";
-- PLEASE DONT FORGET TO CHECK missed Global Rating Scale updates

-- Update student results outcome table 

-- INSERT INTO student_results_outcomes (student_id, session_id, combine_by, score, possible, passed, pass_possible) 

ALTER TABLE `student_results_outcomes` 
DROP FOREIGN KEY `outcome_student_fk`,
DROP FOREIGN KEY `outcome_session_fk`;
ALTER TABLE `student_results_outcomes` 
DROP INDEX `session_id_UNIQUE` ,
DROP INDEX `student_id`;

ALTER TABLE `student_results_outcomes` 
DROP INDEX `outcome_student_fk_idx` ,
DROP INDEX `student_id_UNIQUE` ,
DROP INDEX `combined_by_UNIQUE` ;

ALTER TABLE `student_results_outcomes` 
ADD UNIQUE INDEX `outcome_combined_unique` (`student_id` ASC, `session_id` ASC, `combined_by` ASC);

ALTER TABLE `student_results_outcomes` 
ADD INDEX `outcome_session_fk_idx` (`session_id` ASC);

ALTER TABLE `student_results_outcomes` 
ADD CONSTRAINT `outcome_student_fk`
  FOREIGN KEY (`student_id`)
  REFERENCES `students` (`student_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `outcome_session_fk`
  FOREIGN KEY (`session_id`)
  REFERENCES `exam_sessions` (`session_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;  

-- Fill student_results_outcomes table, DO NOT FORGET TO SET THE @grace_value (if not already above)
-- SET @grace_value = '0.0';

-- Determine student outcomes grouped by 'station', refer to 'combine_results_by' field in the exam_rules table
INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, station_number, AVG(score), AVG(possible_score), AVG(pass_value), 
IF((AVG(score)/AVG(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'station' AND multi_examiner_results_averaged = 1
GROUP BY student_id, session_id, station_number;

INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, form_id, AVG(score), AVG(possible_score), AVG(pass_value), 
IF((AVG(score)/AVG(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'form' AND multi_examiner_results_averaged = 1
GROUP BY student_id, session_id, form_id;

INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, station_code, AVG(score), AVG(possible_score), AVG(pass_value), 
IF((AVG(score)/AVG(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'code' AND multi_examiner_results_averaged = 1
GROUP BY student_id, session_id, station_code;

INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, station_number, SUM(score), SUM(possible_score), AVG(pass_value), 
IF((SUM(score)/SUM(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'station' AND multi_examiner_results_averaged = 0
GROUP BY student_id, session_id, station_number;

INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, form_id, SUM(score), SUM(possible_score), AVG(pass_value), 
IF((SUM(score)/SUM(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'form' AND multi_examiner_results_averaged = 0
GROUP BY student_id, session_id, form_id;

INSERT INTO student_results_outcomes (student_id, session_id, combined_by, score, possible, pass_possible, passed) 
SELECT student_id, session_id, station_code, SUM(score), SUM(possible_score), AVG(pass_value), 
IF((SUM(score)/SUM(possible_score) * 100) - @grace_value >= AVG(pass_value), 1, 0) AS passed
FROM student_results
    INNER JOIN session_stations USING (station_id)    
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id) 
    WHERE combine_results_by = 'code' AND multi_examiner_results_averaged = 0
GROUP BY student_id, session_id, station_code;

INSERT INTO student_item_outcomes (student_id, session_id, item_id, score) 
SELECT student_id, session_id,  item_id, AVG(score) 
FROM item_scores
    INNER JOIN student_results USING (result_id) 
    INNER JOIN section_items USING (item_id) 
    INNER JOIN session_stations USING (station_id)
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id)     
WHERE grs = 0 AND type IN ("radio", "slider", "text") AND multi_examiner_results_averaged =1
GROUP BY student_id, session_id, item_id;

INSERT INTO student_item_outcomes (student_id, session_id, item_id, score) 
SELECT student_id, session_id, item_id, SUM(score) 
FROM item_scores
    INNER JOIN student_results USING (result_id) 
    INNER JOIN section_items USING (item_id) 
    INNER JOIN session_stations USING (station_id)
    INNER JOIN exam_sessions USING (session_id) 
    INNER JOIN exams USING (exam_id) 
    INNER JOIN exam_rules USING(exam_id)     
WHERE grs = 0 AND type IN ("radio", "slider", "text") AND multi_examiner_results_averaged = 0 
GROUP BY student_id, session_id, item_id;

-- TESTING PURPOSES ONLY RUN: TRUNCATE `student_exam_data`;
-- SET @grace_value = '0.0';
INSERT INTO student_exam_data (student_id, exam_id, score, possible, pass, passed, grade) 
SELECT o.student_id, o.exam_id, o.score, o.possible, o.pass, o.passed, o.grade 
FROM
        (SELECT student_id, exam_id, SUM(score) AS score, SUM(possible) AS possible, AVG(pass_possible) AS pass, 
		IF((SUM(score)/SUM(possible) * 100) - @grace_value >= AVG(pass_possible), 1, 0) AS passed,
		IF((SUM(score)/SUM(possible) * 100) - @grace_value >= AVG(pass_possible), "pass", "fail") AS grade
		FROM student_results_outcomes
		INNER JOIN exam_sessions USING(session_id)
		GROUP BY student_id, exam_id) AS o
ON DUPLICATE KEY UPDATE 
   student_id = o.student_id, exam_id = o.exam_id, 
   score = o.score, possible = o.possible,
   pass = o.pass, passed = o.passed,
   grade = o.grade;

SET SQL_SAFE_UPDATES = 0;
UPDATE student_exam_data SET up_to_date = 1;    
SET SQL_SAFE_UPDATES = 1;

ALTER TABLE `student_results` 
CHANGE COLUMN `possible_score` `possible` DECIMAL(9,3) NOT NULL DEFAULT '0.000' AFTER `score`,
CHANGE COLUMN `passed` `passed` TINYINT(1) UNSIGNED NULL DEFAULT NULL AFTER `possible`,
CHANGE COLUMN `flag_count` `flag_count` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `weighted_possible`,
CHANGE COLUMN `possible_weighted_score` `weighted_possible` DECIMAL(9,3) NOT NULL DEFAULT '0.000' ;

ALTER TABLE `exams` 
ADD COLUMN `results_updated_at` TIMESTAMP NULL DEFAULT '0000-00-00' AFTER `updated_at`;

SET SQL_SAFE_UPDATES = 0;
UPDATE exams
SET results_updated_at = "2019-12-16";
SET SQL_SAFE_UPDATES = 1;

ALTER TABLE `exam_rules` 
ADD COLUMN `results_published` TINYINT(1) NOT NULL DEFAULT '0' AFTER `combine_results_by`;

SET SQL_SAFE_UPDATES = 0;
UPDATE exam_rules
SET results_published = 1;
SET SQL_SAFE_UPDATES = 1;

-- Adds adjusted source enum
ALTER TABLE `student_exam_data` 
ADD COLUMN `adjusted_source` ENUM('analyse', 'weighted') NULL DEFAULT NULL AFTER `adjusted_passed`;

   -- Log that the current version of the database is 2.0.0, and it was added now.
 INSERT INTO `database_versions` (`major_version`, `minor_version`, `patch_version`) VALUES ('2', '1', '0');