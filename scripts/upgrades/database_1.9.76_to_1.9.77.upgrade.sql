-- Author: David Cunningham
-- Date: 17/08/2016
-- OMIS database upgrade script version 1.9.76 => 1.9.77

-- Rename page name = 'manageform_osce' => 'formbuilder' more appropriate
UPDATE `pages` SET `page_name`='formbuilder' WHERE `page_id`='138';

-- Adds alias page: examform for page 'formbuilder'
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (195, 'examform');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (195, 1, 1), (195, 2, 0), (195, 3, 1), (195, 4, 0), (195, 5, 1), (195, 6, 1);

-- Adds alias page: 'bankform' for page 'formbuilder'
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (196, 'bankform');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (196, 1, 1), (196, 2, 0), (196, 3, 1), (196, 4, 0), (196, 5, 1), (196, 6, 1);

-- Log that the current version of the database is 77, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (77);
