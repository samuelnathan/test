-- Author: Domhnall Walsh
-- Date:   23/07/2015
-- Omis database upgrade script version 1.9.48 => 1.9.49

-- Drop the api_key from the users table. API Keys are being stored in a separate table.
ALTER TABLE `users` DROP COLUMN `api_key`;

-- Change the "double" fields in the DB to "decimal" to avoid rounding errors
-- in scoring results.
ALTER TABLE `forms` 
CHANGE COLUMN `total_value` `total_value` DECIMAL(9,3) NOT NULL DEFAULT 0.0 COMMENT '' ;

ALTER TABLE `section_items`
CHANGE COLUMN `highest_item_value` `highest_item_value` DECIMAL(9,3) NOT NULL DEFAULT 0.0 COMMENT '';

ALTER TABLE `section_items`
CHANGE COLUMN `lowest_item_value` `lowest_item_value` DECIMAL(9,3) NOT NULL DEFAULT 0.0 COMMENT '';

ALTER TABLE `session_stations` 
CHANGE COLUMN `pass_value` `pass_value` DECIMAL(9,3) NOT NULL DEFAULT 0.0 COMMENT '' ;

ALTER TABLE `student_results`
CHANGE COLUMN `score` `score` DECIMAL(9,3) NOT NULL DEFAULT 0.0 COMMENT '' ;

-- Log that the current version of the database is 49, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (49);