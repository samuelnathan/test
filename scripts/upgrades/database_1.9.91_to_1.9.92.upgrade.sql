-- Author: David Cunningham
-- Date: 09/12/2016
-- OMIS database upgrade script version 1.9.91 => 1.9.92

-- Add new exam rule to database, that if is set summates the multiple results in a station, default average is calculated
ALTER TABLE `exam_rules` 
ADD COLUMN `multi_examiner_results_averaged` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `min_stations_to_pass`;

-- Log that the current version of the database is 92, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (92);
