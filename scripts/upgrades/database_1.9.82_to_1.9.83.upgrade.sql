-- Author: David Cunningham
-- Date: 05/11/2016
-- OMIS database upgrade script version 1.9.82 => 1.9.83
-- Allows exam admins to have access to the php pages in the import function

UPDATE page_levels SET level_enabled = 1
WHERE page_id IN (15, 72, 73, 74, 75) AND page_level = 3;

-- Log that the current version of the database is 83, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (83);
