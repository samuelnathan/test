-- Author: David Cunningham
-- Date:   25/11/2015
-- OMIS database upgrade script version 1.9.62 => 1.9.63
-- Adds default number of examiners per station field to the exams table

ALTER TABLE `exams` 
ADD COLUMN `default_examiners_station` INT(1) UNSIGNED
COMMENT 'Set the default number of examiners for each of the stations in an exam'
NOT NULL DEFAULT '1' AFTER `submit_next`;

-- Log that the current version of the database is 63, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (63);