-- Author: David Cunningham
-- Date: 19/09/2016
-- OMIS database upgrade script version 1.9.79 => 1.9.80
-- Adds new exam and candidate fields to OMIS, see JIRA tickets "SHEF-6" and "SGUL-3"
-- Also adds new exam flag field to set which identifier (student, candidate or exam) should be displayed in the assessment tool 

ALTER TABLE `exam_settings` 
ADD COLUMN `exam_student_id_type` CHAR(1) CHARACTER SET 'utf8' NOT NULL DEFAULT 'S' 
COMMENT 'Which type of identifier to display during assessment:\nS => Student Identifier, E => Exam Number (anonymised), C => Candidate Number' 
AFTER `exam_id`;

ALTER TABLE `group_students` 
ADD COLUMN `student_exam_number` VARCHAR(30) CHARACTER SET 'utf8' NULL 
COMMENT 'Anonymous examination number for student, alternative to student identifier (optional)' 
AFTER `stu_id`;

-- Adds exam number as feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("exam-number", "Exam (Anonymous) Number", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("exam-number", 1, 0), ("exam-number", 2, 0), ("exam-number", 3, 0), ("exam-number", 5, 0);

-- Adds candidate number as feature
INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("candidate-number", "Candidate Number", 1);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("candidate-number", 1, 0), ("candidate-number", 2, 0), ("candidate-number", 3, 0), ("candidate-number", 5, 0);

-- Give all current students (who have been assessed) unique exam numbers
SET SQL_SAFE_UPDATES = 0;
UPDATE group_students SET student_exam_number=CONCAT("A", group_id, stu_order);
SET SQL_SAFE_UPDATES = 1;

-- GRS borderline fail defaults
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='1' and`scale_type_id`='1';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='2' and`scale_type_id`='1';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='6' and`scale_type_id`='2';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1', `scale_value_fail`='1' WHERE `scale_value_id`='7' and`scale_type_id`='2';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='8' and`scale_type_id`='2';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='11' and`scale_type_id`='3';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='12' and`scale_type_id`='3';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='14' and`scale_type_id`='4';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='15' and`scale_type_id`='4';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='19' and`scale_type_id`='5';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='21' and`scale_type_id`='6';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='22' and`scale_type_id`='6';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='25' and`scale_type_id`='7';
UPDATE `rating_scale_values` SET `scale_value_fail`='1' WHERE `scale_value_id`='26' and`scale_type_id`='7';
UPDATE `rating_scale_values` SET `scale_value_borderline`='1' WHERE `scale_value_id`='27' and`scale_type_id`='7';

-- Log that the current version of the database is 80, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (80);