-- Changing "multi_results_station" field in exams table to "multi_forms_station"
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   10/06/2014
-- Omis database upgrade script version 1.9.26 => 1.9.27
ALTER TABLE `exams` CHANGE COLUMN `multi_results_station` `multi_forms_station` INT(1) UNSIGNED NOT NULL DEFAULT '0' ;

-- Log that the current version of the database is 27, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (27);
