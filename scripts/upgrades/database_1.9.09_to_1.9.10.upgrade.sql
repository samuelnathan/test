-- Page name updates & new pages for Omis
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   25/03/2014
-- Omis database upgrade script version 1.9.9 => 1.9.10

-- Update Page Names - Found in the OSCE Directory

-- Page name ajx_tds => ajx_sessions
UPDATE `pages` SET `page_name` = 'ajx_sessions' WHERE `page_id` = 33;

-- Page name ajx_stationset => ajx_stations
UPDATE `pages` SET `page_name` = 'ajx_stations' WHERE `page_id` = 28;

-- Page name ajx_stations => ajx_forms
UPDATE `pages` SET `page_name` = 'ajx_forms' WHERE `page_id` = 27;

-- Page name ajx_grps => ajx_groups
UPDATE `pages` SET `page_name` = 'ajx_groups' WHERE `page_id` = 21;

-- Page name ajx_osce => ajx_exams
UPDATE `pages` SET `page_name` = 'ajx_exams' WHERE `page_id` = 22;

-- Page name ajx_exmrs => ajx_station_examiners
UPDATE `pages` SET `page_name` = 'ajx_station_examiners' WHERE `page_id` = 167;

-- Page name ajx_add_comp_item => ajx_add_item
UPDATE `pages` SET `page_name` = 'ajx_add_item' WHERE `page_id` = 6;

-- Page name ajx_comp_buttons => ajx_section_buttons
UPDATE `pages` SET `page_name` = 'ajx_section_buttons' WHERE `page_id` = 11;

-- Page name ajx_edit_comp_dets => ajx_edit_section_details
UPDATE `pages` SET `page_name` = 'ajx_edit_section_details' WHERE `page_id` = 17;

-- Page name ajx_edit_comp_item => ajx_edit_item
UPDATE `pages` SET `page_name` = 'ajx_edit_item' WHERE `page_id` = 18;

-- Page name new_station => new_form
UPDATE `pages` SET `page_name` = 'new_form' WHERE `page_id` = 90;

-- Page name bank_station => form_bank
UPDATE `pages` SET `page_name` = 'form_bank' WHERE `page_id` = 38;

-- Page name previous_station => single_form_previous_session
UPDATE `pages` SET `page_name` = 'single_form_previous_session' WHERE `page_id` = 115;

-- Page name clone_testday => copy_stations_previous_session
UPDATE `pages` SET `page_name` = 'copy_stations_previous_session' WHERE `page_id` = 45;

-- Page name delete_competence_items => delete_items
UPDATE `pages` SET `page_name` = 'delete_items' WHERE `page_id` = 46;

-- Page name edit_ajx_radio_fields => ajx_edit_radio_fields
UPDATE `pages` SET `page_name` = 'ajx_edit_radio_fields' WHERE `page_id` = 50;

-- Page name ajx_radio_fields => ajx_add_radio_fields
UPDATE `pages` SET `page_name` = 'ajx_add_radio_fields' WHERE `page_id` = 23;

-- Page name edit_radio_ans_options => edit_radio_options
UPDATE `pages` SET `page_name` = 'edit_radio_options' WHERE `page_id` = 52;

-- Page name edit_slider_ans_option => edit_slider_option
UPDATE `pages` SET `page_name` = 'edit_slider_option' WHERE `page_id` = 54;

-- Page name edit_tb_ans_option => edit_textbox_option
UPDATE `pages` SET `page_name` = 'edit_textbox_option' WHERE `page_id` = 55;

-- Page name grps_osce => groups_osce
UPDATE `pages` SET `page_name` = 'groups_osce' WHERE `page_id` = 71;

-- Page name json_tdstations => json_stations
UPDATE `pages` SET `page_name` = 'json_stations' WHERE `page_id` = 77;

-- Page name pr_osce => manage_osce
UPDATE `pages` SET `page_name` = 'manage_osce' WHERE `page_id` = 111;

-- Page name previewstat_osce => previewform_osce
UPDATE `pages` SET `page_name` = 'previewform_osce' WHERE `page_id` = 114;

-- Page name radio_ans_options => radio_options
UPDATE `pages` SET `page_name` = 'add_radio_options' WHERE `page_id` = 118;

-- Page name slider_ans_option => add_slider_option
UPDATE `pages` SET `page_name` = 'add_slider_option' WHERE `page_id` = 126;

-- Page name text_box_ans_option => add_textbox_option
UPDATE `pages` SET `page_name` = 'add_textbox_option' WHERE `page_id` = 150;

-- Page name update_text_item => update_textbox_item
UPDATE `pages` SET `page_name` = 'update_textbox_option' WHERE `page_id` = 155;

-- Page name statitems_osce => manageform_osce
UPDATE `pages` SET `page_name` = 'manageform_osce' WHERE `page_id` = 138;

-- Page name stats_osce => stations_osce
UPDATE `pages` SET `page_name` = 'stations_osce' WHERE `page_id` = 139;

-- Page name tds_osce => sessions_osce
UPDATE `pages` SET `page_name` = 'sessions_osce' WHERE `page_id` = 148;

-- Log that the current version of the database is 10, and it was added now.
INSERT INTO `database` (`version`) VALUES (10);