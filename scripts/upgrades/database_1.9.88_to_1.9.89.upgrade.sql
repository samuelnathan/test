-- Author: David Cunningham
-- Date: 30/11/2016
-- OMIS database upgrade script version 1.9.88 => 1.9.89
-- FIX: cascading on delete and on update seems to be missing for the candidate_numbers table

ALTER TABLE `candidate_numbers` 
DROP FOREIGN KEY `candidatenumber_student_fk`;
ALTER TABLE `candidate_numbers` 
ADD CONSTRAINT `candidatenumber_student_fk`
  FOREIGN KEY (`stu_id`)
  REFERENCES `students` (`stu_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- Log that the current version of the database is 89, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (89);
