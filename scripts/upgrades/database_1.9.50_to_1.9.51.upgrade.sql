-- Author: Domhnall Walsh
-- Date:   12/08/2015
-- Omis database upgrade script version 1.9.50 => 1.9.51
-- Keeps iso_country_codes table consistent with ISO

-- Remove ISO3166-2 entries that are optional or withdrawn that were on the list
DELETE FROM iso_country_codes WHERE iso IN ('AN', 'CS', 'XB', 'XH');

-- Add in entries that were missing...
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('AX', 'ÅLAND ISLANDS', 'Åland Islands', 'ALA', 248);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('BL', 'SAINT BARTHÉLEMY', 'Åland Islands', 'BLM', 652);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('BQ', 'BONAIRE, SINT EUSTATIUS AND SABA', 'Bonaire, Sint Eustatius and Saba', 'BES', 535);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('CW', 'CURAÇAO', 'Curaçao', 'CUW', 531);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('GG', 'GUERNSEY', 'Guernsey', 'GGY', 831);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('IM', 'ISLE OF MAN', 'Isle Of Man', 'IMN', 833);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('JE', 'JERSEY', 'Jersey', 'JEY', 832);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('ME', 'MONTENEGRO', 'Montenegro', 'MNE', 499);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('RS', 'SERBIA', 'Serbia', 'SRB', 688);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('SS', 'SOUTH SUDAN', 'South Sudan', 'SSD', 728);
INSERT INTO iso_country_codes (iso, iso_name, printable_name, iso3, numcode) VALUES ('SX', 'SINT MAARTEN (DUTCH PART)', 'Sint Maarten (Dutch part)', 'SXM', 534);

-- Update entries missing their ISO3 and Numeric Codes for completeness.
UPDATE iso_country_codes SET iso3 = 'ATA', numcode = 10 WHERE iso = 'AQ';
UPDATE iso_country_codes SET iso3 = 'BVT', numcode = 74 WHERE iso = 'BV';
UPDATE iso_country_codes SET iso3 = 'CCK', numcode = 166 WHERE iso = 'CC';
UPDATE iso_country_codes SET iso3 = 'CXR', numcode = 162 WHERE iso = 'CX';
UPDATE iso_country_codes SET iso3 = 'SGS', numcode = 239 WHERE iso = 'GS';
UPDATE iso_country_codes SET iso3 = 'HMD', numcode = 334 WHERE iso = 'HM';
UPDATE iso_country_codes SET iso3 = 'IOT', numcode = 86 WHERE iso = 'IO';
UPDATE iso_country_codes SET iso3 = 'PSE', numcode = 275 WHERE iso = 'PS';
UPDATE iso_country_codes SET iso3 = 'ATF', numcode = 260 WHERE iso = 'TF';
UPDATE iso_country_codes SET iso3 = 'TLS', numcode = 626 WHERE iso = 'TL';
UPDATE iso_country_codes SET iso3 = 'UMI', numcode = 581 WHERE iso = 'UM';
UPDATE iso_country_codes SET iso3 = 'MYT', numcode = 175 WHERE iso = 'YT';

-- Log that the current version of the database is 51, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (51);