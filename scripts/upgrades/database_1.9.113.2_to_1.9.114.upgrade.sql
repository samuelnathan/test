
-- Add an auto increment column to the student table 
ALTER TABLE `students` ADD COLUMN `id` INT AUTO_INCREMENT UNIQUE FIRST;

-- Log that the current version of the database is 112, and it was added now.
INSERT IGNORE INTO `database_versions` (`major_version`, `minor_version`) VALUES (114, 0);