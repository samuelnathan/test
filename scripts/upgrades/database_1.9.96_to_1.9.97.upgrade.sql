-- Author: David Cunningham
-- Date: 09/03/2017
-- OMIS database upgrade script version 1.9.96 => 1.9.97
-- Adds new amber and red flag fields to student results table to maintain count on
-- red and amber flags submitted at station per student

 ALTER TABLE `student_results`
 ADD COLUMN `amberflag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_complete`,
 ADD COLUMN `redflag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `amberflag_count`;

 -- Add red and amber flag count to results audit table if it is installed
 ALTER IGNORE TABLE `student_results_audit_trail` 
 ADD COLUMN `amberflag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_complete`,
 ADD COLUMN `redflag_count` INT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `amberflag_count`;

 -- Log that the current version of the database is 97, and it was added now.
 INSERT INTO `database_versions` (`version`) VALUES (97);
