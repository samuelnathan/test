-- Short script to rename some scripts in the manage examiners section
-- Author: David Cunnigham <david.cunningham@qpercom.ie>
-- Date:   22/05/2015
-- Omis database upgrade script version 1.9.44 => 1.9.45

UPDATE `pages` SET `page_name`='ajx_examiners' WHERE `page_id`='20';
UPDATE `pages` SET `page_name`='edit_examiners' WHERE `page_id`='51';
UPDATE `pages` SET `page_name`='manage_examiners' WHERE `page_id`='86';
UPDATE `pages` SET `page_name`='sms_examiners' WHERE `page_id`='127';
UPDATE `pages` SET `page_name`='sms_examiners_protocol' WHERE `page_id`='128';

-- Log that the current version of the database is 43, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (45);

