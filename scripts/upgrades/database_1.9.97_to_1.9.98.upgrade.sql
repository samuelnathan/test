-- Author: David Cunningham
-- Date: 03/04/2017
-- OMIS database upgrade script version 1.9.97 => 1.9.98
-- Adds 2 new exam settings to OMIS, to set if feedback fields should appear underneath or alongside of scoresheet section
-- and auto configuration of stations for assigned examiners already assigned to station (on/off)

ALTER TABLE `exam_settings` 
ADD COLUMN `feedback_to_right` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 
COMMENT 'If set to on, feedback fields will appear alongside (to right) sections of scoresheet during assessment rather than underneath', 
ADD COLUMN `auto_config_station` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 
COMMENT 'If set to on and examiner is assigned to station, then auto configure station when logged in'; 

-- Log that the current version of the database is 98, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (98);
