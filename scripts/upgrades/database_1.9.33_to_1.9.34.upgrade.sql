-- Upgrade script to add the Exam Session Scheduler as a feature that we can enable and disable in the manage features section
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   25/06/2014
-- Omis database upgrade script version 1.9.33 => 1.9.34

INSERT IGNORE INTO features (feature_id, feature_desc) VALUES ("ess", "Exam Session Scheduler");

INSERT IGNORE INTO feature_levels (feature_id, feature_level, level_enabled) VALUES ("ess", 1, 1), ("ess", 2, 0), ("ess", 3, 1), ("ess", 5, 0);

-- Log that the current version of the database is 34, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (34);