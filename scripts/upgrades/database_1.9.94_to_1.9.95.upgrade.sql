-- Author: David Cunningham
-- Date: 20/02/2016
-- OMIS database upgrade script version 1.9.94 => 1.9.95
-- Add new page 'poll_feedback.php'

INSERT INTO pages (page_id, page_name) VALUES (199, 'poll_feedback');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) 
VALUES (199, 1, 1), (199, 2, 0), (199, 3, 1), (199, 4, 0), (199, 5, 1), (199, 6, 1);

-- Log that the current version of the database is 95, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (95);
