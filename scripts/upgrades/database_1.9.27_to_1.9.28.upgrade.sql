-- Changing page name "export_exmrs" to "export_examiner_list"
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   16/06/2014
-- Omis database upgrade script version 1.9.27 => 1.9.28

-- Page name export/export_exmrs => export/export_examiner_list
UPDATE `pages` SET `page_name` = 'export_examiner_list' WHERE `page_id` = 65;


-- Log that the current version of the database is 28, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (28);
