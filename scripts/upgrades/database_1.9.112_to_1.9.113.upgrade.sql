-- Author: David Cunningham
-- Date: 25/08/2018
-- Version 1.9.112 => 1.9.113

ALTER TABLE user_presets
ADD COLUMN `filter_term` VARCHAR(20) NULL DEFAULT NULL COMMENT 'User preset for academic year/term' AFTER `user_id`;

ALTER TABLE terms
CHANGE COLUMN `current_term` `default_term` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'True or False' ;

INSERT INTO pages (`page_id`, `page_name`) VALUES (214, 'ajx_depts');

INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,2,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,4,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,6,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (214,7,1);

-- Log that the current version of the database is 113, and it was added now.
INSERT IGNORE INTO `database_versions` (`version`) VALUES (113);
