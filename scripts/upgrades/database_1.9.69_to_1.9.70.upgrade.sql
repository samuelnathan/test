-- Author: David Cunningham
-- Date: 26/04/2016
-- OMIS database upgrade script version 1.9.69 => 1.9.70
-- Add new radar pages 'radar_student_competency'

-- Insert radar competencies page
INSERT INTO `pages` (`page_id`, `page_name`) VALUES (192, 'radar_student_competency');
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,1,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,2,0);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,3,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,4,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,5,1);
INSERT INTO `page_levels` (`page_id`,`page_level`,`level_enabled`) VALUES (192,6,1);

-- Log that the current version of the database is 70, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (70);
