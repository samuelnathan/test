-- Page "update_textbox_item" missing from pages table used by access control
-- system. In one case, it was mistakenly titled "update_textbox_option", so
-- fixing that too.
-- Also fixing naming error in pages table:
-- 'change_examiners' -> 'change_examiner'
-- 
-- Author: Domhnall Walsh <domhnall.walsh@qpercom.ie>
-- Date:   27/05/2014
-- Omis database upgrade script version 1.9.23 => 1.9.24

-- Change page 'change_examiners' to 'change_examiner';
UPDATE `pages` SET `page_name` = 'change_examiner' WHERE `page_id` = 162;

-- This creates a new item, should be ID #183. As this command stands, it should
-- allow Admins, Exam Admins, Station Managers and Super Admins to use this page.
INSERT INTO `pages` (`page_name`) VALUES ('update_textbox_item');
INSERT INTO `page_levels` (`page_id`, `page_level`, `level_enabled`) VALUES (183, 1, 1), (183, 2, 0), (183, 3, 1), (183, 4, 0), (183, 5, 1), (183, 6, 1);

-- Old "update_textbox_option" item was ID #155...
DELETE FROM `page_levels` WHERE (`page_id` = 155);

-- Log that the current version of the database is 24, and it was added now.
INSERT INTO `database` (`version`) VALUES (24);
