-- Upgrade script to add api expiry field 'last_active' to users table in database
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   20/06/2014
-- Omis database upgrade script version 1.9.28 => 1.9.29

ALTER TABLE `users` 
ADD COLUMN `last_active` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `api_key`;

-- Log that the current version of the database is 30, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (30);