-- Upgrade script to add the student results history as a feature that we can enable and disable in the manage features section
-- refer to issue/ticket #165 in bitbucket. 
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   25/06/2014
-- Omis database upgrade script version 1.9.30 => 1.9.31

INSERT IGNORE INTO features (feature_id, feature_desc) VALUES ("seh", "View Student Exam History");

INSERT IGNORE INTO feature_levels (feature_id, feature_level, level_enabled) VALUES ("seh", 1, 1), ("seh", 2, 0), ("seh", 3, 1), ("seh", 5, 0);

-- Log that the current version of the database is 31, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (31);