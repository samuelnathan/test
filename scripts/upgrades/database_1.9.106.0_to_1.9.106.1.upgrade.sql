-- Author: David Cunningham
-- Date: 15/09/2017
-- Version 1.9.106.0 => 1.9.106.1 
-- New pages for session (test day/circuit)

INSERT INTO pages (page_id, page_name) VALUES (204, 'ajx_session_view');

INSERT INTO pages (page_id, page_name) VALUES (205, 'json_session_update');

INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 1, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 2, 0);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 3, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 4, 0);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 5, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 6, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(204, 7, 1);

INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 1, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 2, 0);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 3, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 4, 0);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 5, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 6, 1);
INSERT INTO page_levels
(page_id, page_level, level_enabled)
VALUES(205, 7, 1);

-- Couple departments with courses
-- INSERT INTO courses (crs_id, crs_fullname) 
-- SELECT dept_id, dept_name
-- FROM departments 
-- WHERE dept_id 
-- NOT IN (SELECT crs_id FROM courses);
