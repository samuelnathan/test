-- Upgrade script to colour column to Global rating scales table and adds two new scales.
-- Author: Cormac McSwiney <cormac.mcswiney@qpercom.ie>
-- Date:   09/07/2014
-- Omis database upgrade script version 1.9.31 => 1.9.32


-- Turn off safe mode
SET SQL_SAFE_UPDATES = 0;

ALTER TABLE global_rating_scales ADD column type_colour varchar(30) ;

UPDATE global_rating_scales SET type_colour = 'black' WHERE scale_type = 'type1';
UPDATE global_rating_scales SET type_colour = 'black' WHERE scale_type = 'type5';
UPDATE global_rating_scales SET type_colour = 'blue' WHERE scale_type = 'type2';
UPDATE global_rating_scales SET type_colour = 'green' WHERE scale_type = 'type3';
UPDATE global_rating_scales SET type_colour = 'red' WHERE scale_type = 'type4';
UPDATE global_rating_scales SET type_colour = 'black' WHERE scale_type = 'type5';

INSERT INTO global_rating_scales (scale_type, scale_value, value_description, type_colour)
VALUES 
('type6', 0, "Fail", "brown-color-font"),
('type6', 1, "Borderline", "brown-color-font"),
('type6', 2, "Good", "brown-color-font"),
('type6', 3, "Outstanding", "brown-color-font"),
('type7', 0, "Very Poor", "indigo-color-font"),
('type7', 1, "Unsatisfactory", "indigo-color-font"),
('type7', 2, "Just Good Enough", "indigo-color-font"),
('type7', 3, "Adequate But Incomplete", "indigo-color-font"),
('type7', 4, "Very Good And Complete", "indigo-color-font");

-- Log that the current version of the database is 32, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (32);

-- Turn on safe mode
SET SQL_SAFE_UPDATES = 1;