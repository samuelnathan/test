-- Author: Domhnall Walsh
-- Date:   14/09/2015
-- Omis database upgrade script version 1.9.53 => 1.9.54
-- Adding last_updated fields to form-related tables to help keep track of when
-- stuff gets updated.
-- Also added last_updated_child field for form-related tables and triggers so
-- that the every tier in the exam structure is automagically updated with the
-- date of its most recent update.

-- Add some "last updated" fields to these records to be used as the basis for ETags.
-- That we we know what's been updated and what hasn't.
ALTER TABLE `exams`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `results_grs`;

ALTER TABLE `exam_sessions`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `google_calendar_event_id`;

ALTER TABLE `session_groups`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `group_order`;

ALTER TABLE `form_sections`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `feedback_required`;

ALTER TABLE `section_items`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `lowest_item_value`;

ALTER TABLE `item_options`
ADD COLUMN `last_updated` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'When record was last updated' AFTER `ans_option_text`;

-- Create extra columns in the forms-sections-items-options relationship so that we can
-- record when subrecords are updated...
ALTER TABLE `section_items`
ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL COMMENT 'Timestamp of most recent child record update' AFTER `last_updated`;

ALTER TABLE `form_sections`
ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL COMMENT 'Timestamp of most recent child record update' AFTER `last_updated`;

ALTER TABLE `forms`
ADD COLUMN `last_updated_child` TIMESTAMP NOT NULL COMMENT 'Timestamp of most recent child record update' AFTER `last_updated`;

-- Okay, the big one. Add triggers so that the last_updated_child field of the parent
-- table gets updated each time a record is changed.
DELIMITER $$

DROP TRIGGER IF EXISTS item_options_AFTER_UPDATE$$
CREATE TRIGGER `item_options_AFTER_UPDATE` AFTER UPDATE ON `item_options` FOR EACH ROW
BEGIN
	UPDATE `section_items` SET `last_updated_child` = NEW.`last_updated` WHERE `item_id` = NEW.`item_id`;
END
$$

DROP TRIGGER IF EXISTS item_options_AFTER_INSERT$$
CREATE TRIGGER `item_options_AFTER_INSERT` AFTER INSERT ON `item_options` FOR EACH ROW
BEGIN
	UPDATE `section_items` SET `last_updated_child` = NEW.`last_updated` WHERE `item_id` = NEW.`item_id`;
END
$$

DROP TRIGGER IF EXISTS section_items_AFTER_UPDATE$$
CREATE TRIGGER `section_items_AFTER_UPDATE` AFTER UPDATE ON `section_items` FOR EACH ROW
BEGIN
	UPDATE `form_sections` SET `last_updated_child` = NEW.`last_updated` WHERE `section_id` = NEW.`section_id`;
END
$$

DROP TRIGGER IF EXISTS section_items_AFTER_INSERT$$
CREATE TRIGGER `section_items_AFTER_INSERT` AFTER INSERT ON `section_items` FOR EACH ROW
BEGIN
	UPDATE `form_sections` SET `last_updated_child` = NEW.`last_updated` WHERE `section_id` = NEW.`section_id`;
END
$$

DROP TRIGGER IF EXISTS form_sections_AFTER_UPDATE$$
CREATE TRIGGER `form_sections_AFTER_UPDATE` AFTER UPDATE ON `form_sections` FOR EACH ROW
BEGIN
	UPDATE `forms` SET `last_updated_child` = NEW.`last_updated` WHERE `form_id` = NEW.`form_id`;
END
$$

DROP TRIGGER IF EXISTS form_sections_AFTER_INSERT$$
CREATE TRIGGER `form_sections_AFTER_INSERT` AFTER INSERT ON `form_sections` FOR EACH ROW
BEGIN
	UPDATE `forms` SET `last_updated_child` = NEW.`last_updated` WHERE `form_id` = NEW.`form_id`;
END
$$
DELIMITER ;

-- Log that the current version of the database is 54, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (54);
