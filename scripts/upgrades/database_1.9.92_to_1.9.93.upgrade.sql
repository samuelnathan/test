-- Author: David Cunningham
-- Date: 16/01/2016
-- OMIS database upgrade script version 1.9.92 => 1.9.93
-- Competencies for Karolinksa Institutet
INSERT INTO `competency_frameworks` (`id`, `name`) VALUES ('4', 'Karolinksa Institutet Competency Framework');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('51', '4', 'Karolinksa Institutet Competency Framework', 'Karolinksa Institutet Competency Framework');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('52', '4', 'Patientkommunikation', 'Patientkommunikation', '#fa8072');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('53', '4', 'Anamnes', 'Anamnes', '#90ee90');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('54', '4', 'Status', 'Status', '#ffa500');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('55', '4', 'Differentialdiagnostiskt Resonemang', 'Differentialdiagnostiskt Resonemang', '#00c0ff');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('56', '4', 'Bedömning Och Åtgärd', 'Bedömning Och Åtgärd', '#ffC0cb');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`, `level_colour_code`) VALUES ('57', '4', 'Patientkommunikation Bedömning Och Åtgärd', 'Patientkommunikation Bedömning Och Åtgärd', '#d799fe');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('58', '4', 'Sub Level Competency', 'Sub Level Competency');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('59', '4', 'Sub Level Competency', 'Sub Level Competency');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('60', '4', 'Sub Level Competency', 'Sub Level Competency');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('61', '4', 'Sub Level Competency', 'Sub Level Competency');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('62', '4', 'Sub Level Competency', 'Sub Level Competency');
INSERT INTO `competency_framework_levels` (`id`, `framework_id`, `name`, `shortname`) VALUES ('63', '4', 'Sub Level Competency', 'Sub Level Competency');


-- Log that the current version of the database is 93, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (93);
