-- Renaming more pages
-- Author: David Cunningham <david.cunningham@qpercom.ie>
-- Date:   22/04/2014
-- Omis database upgrade script version 1.9.15 => 1.9.16

-- Page name analysis/statexamdet => analysis/statexamdet
UPDATE `pages` SET `page_name` = 'studentresult' WHERE `page_id` = 131;

-- Page name analysis/exmeehi => analysis/studenthistory
UPDATE `pages` SET `page_name`='studenthistory' WHERE `page_id`=58;

-- Page name analysis/exmeers => analysis/studentexam
UPDATE `pages` SET `page_name`='studentexam' WHERE `page_id`=59;

-- Log that the current version of the database is 16, and it was added now.
INSERT INTO `database` (`version`) VALUES (16);