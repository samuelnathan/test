-- Author: David Cunningham
-- Date: 16/07/2017
-- Early warning system for divergence and red flags, tables to store
-- report and status information to assistants via SMS (text message)
-- Add tagging for scoresheets to help group/filter scoresheets in scoresheet bank

-- Add exam notifications table
CREATE TABLE `exam_notifications` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identifier of notification record', 
  `station_id` BIGINT(10) NOT NULL COMMENT 'Identifier of station where notification was triggered',
  `student_id` VARCHAR(30) CHARACTER SET 'utf8' NOT NULL COMMENT 'Identifier of student for whom notification was triggered',
  `examiner_id` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL COMMENT 'Identifier of examiner who triggered the notification',
  `flag_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `flag_count` TINYINT(2) UNSIGNED NOT NULL DEFAULT 0,
  `divergence_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `divergence_value` DECIMAL(9,3) UNSIGNED NULL DEFAULT NULL,
  `divergence_threshold` DECIMAL(9,3) UNSIGNED NULL DEFAULT NULL,
  `notification_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp, when notification was triggered',
  PRIMARY KEY (`id`),
  INDEX `notification_station_fk_idx` (`station_id` ASC),
  CONSTRAINT `notification_station_fk`
    FOREIGN KEY (`station_id`)
    REFERENCES `session_stations` (`station_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'This table keeps record of notifications triggered by early warning systems such as red flags, divergence score detection etc';

-- Add SMS notifications table
CREATE TABLE `exam_notifications_sms` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Notification unique identifier (primary key)',
  `notification_id` BIGINT(10) UNSIGNED NULL,
  `recipient_id` VARCHAR(30) CHARACTER SET 'utf8' NOT NULL COMMENT 'The identifier of the recipient',
  `sms_message` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL COMMENT 'The SMS message itself',
  `sms_number` VARCHAR(20) CHARACTER SET 'utf8' NULL DEFAULT NULL COMMENT 'The SMS number',
  `sms_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When SMS was sent',
  PRIMARY KEY (`id`),
  INDEX `sms_examnotifications_fk_idx` (`notification_id` ASC),
  CONSTRAINT `sms_examnotifications_fk`
    FOREIGN KEY (`notification_id`)
    REFERENCES `exam_notifications` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'This table contains a list of SMS messages sent to exam administrators and assistants. It is linked to exam_notfications table by notification_id';

CREATE TABLE `tags` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Uniquely identify tag',
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL DEFAULT '' COMMENT 'This is the unique tag string',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table to store unique tags to help with taxonomy for system wide content';

CREATE TABLE `form_tags` (
  `id` BIGINT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Uniquely identify form tag relationship',
  `form_id` BIGINT(10) NOT NULL COMMENT 'The assessment form identifier',
  `tag_id` BIGINT(10) UNSIGNED NOT NULL COMMENT 'The tag identifier',
  PRIMARY KEY (`id`),
  CONSTRAINT `formtags_forms_fk`
    FOREIGN KEY (`form_id`)
    REFERENCES `forms` (`form_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `formtags_tags_fk`
    FOREIGN KEY (`tag_id`)
    REFERENCES `tags` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'Table that will contain a 1-to-many relationship between forms and tags';

ALTER TABLE `form_tags` ADD UNIQUE `formtag_UNIQUE`(`form_id`, `tag_id`);

-- Student management and features
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('6', 'Student Administration');

INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("student-manage", "Manage Students", 6);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("student-manage", 1, 1), ("student-manage", 2, 0), ("student-manage", 3, 1), ("student-manage", 5, 0);

INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("student-courses", "Manage Student Courses & Modules", 6);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("student-courses", 1, 1), ("student-courses", 2, 0), ("student-courses", 3, 1), ("student-courses", 5, 0);

UPDATE `features` SET `feature_group_id`='6' WHERE `feature_id`='student-images';
UPDATE `features` SET `feature_group_id`='6' WHERE `feature_id`='candidate-number';

-- Exam team management and features
INSERT INTO `feature_groups` (`feature_group_id`, `feature_group_name`) VALUES ('7', 'Exam Team Administration');

INSERT IGNORE INTO features (feature_id, feature_description, feature_group_id) VALUES ("exam-team", "Import / Manage Exam Team", 7);
INSERT IGNORE INTO feature_roles (feature_id, feature_role, role_enabled) VALUES ("exam-team", 1, 1), ("exam-team", 2, 0), ("exam-team", 3, 0), ("exam-team", 5, 0);

-- Log that the current version of the database is 105, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (105);
