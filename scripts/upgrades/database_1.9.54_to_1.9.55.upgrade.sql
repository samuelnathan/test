-- Author: Domhnall Walsh
-- Date:   15/09/2015
-- Omis database upgrade script version 1.9.54 => 1.9.55
-- In order to be complete, the triggers added in the previous script should
-- also include triggers for DELETE events; they're included here..

-- Add trigger to update parent tables when rows are deleted as this also counts
-- as an update...
-- These triggers are BEFORE delete so that the old record's data is still
-- available in each case.
DELIMITER $$

DROP TRIGGER IF EXISTS item_options_BEFORE_DELETE$$
CREATE TRIGGER `item_options_BEFORE_DELETE` BEFORE DELETE ON `item_options` FOR EACH ROW
BEGIN
	UPDATE `section_items` SET `last_updated_child` = NOW() WHERE `item_id` = OLD.`item_id`;
END
$$

DROP TRIGGER IF EXISTS section_items_BEFORE_DELETE$$
CREATE TRIGGER `section_items_BEFORE_DELETE` BEFORE DELETE ON `section_items` FOR EACH ROW
BEGIN
	UPDATE `form_sections` SET `last_updated_child` = NOW() WHERE `section_id` = OLD.`section_id`;
END
$$

DROP TRIGGER IF EXISTS form_sections_BEFORE_DELETE$$
CREATE TRIGGER `form_sections_BEFORE_DELETE` BEFORE DELETE ON `form_sections` FOR EACH ROW
BEGIN
	UPDATE `forms` SET `last_updated_child` = NOW() WHERE `form_id` = OLD.`form_id`;
END
$$

DELIMITER ;

-- Log that the current version of the database is 55, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (55);
