/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 03/03/2014
 * © 2014 Qpercom Limited. All rights reserved.
 * Omis database upgrade script version 1.9.3 => 1.9.4
 */

/* Drop 'university_details' table. ISO country variable will be added to the 
 * Omis config file instead.
 */

DROP TABLE university_details;

-- Log that the current version of the database is 4, and it was added now.
INSERT INTO `database` (`version`) VALUES (4);