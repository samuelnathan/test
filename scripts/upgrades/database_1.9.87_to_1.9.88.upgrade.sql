-- Author: David Cunningham
-- Date: 28/11/2016
-- OMIS database upgrade script version 1.9.87 => 1.9.88
-- Adds 2 new flag fields to options table that contains the borderline and fail information for the global rating scales items

ALTER TABLE `item_options` 
ADD COLUMN `borderline` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Does item option when clicked constitute a borderline, used primarily for global rating scale' AFTER `flag`;

ALTER TABLE `item_options` 
ADD COLUMN `fail` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Does item option when clicked constitute a fail, used primarily for global rating scale' AFTER `borderline`;

SET SQL_SAFE_UPDATES = 0;
UPDATE item_options SET borderline = 1 WHERE ans_option_text LIKE "%borderline%";
UPDATE item_options SET fail = 1 WHERE ans_option_text LIKE "%fail%";
SET SQL_SAFE_UPDATES = 1;

-- Log that the current version of the database is 88, and it was added now.
INSERT INTO `database_versions` (`version`) VALUES (88);
