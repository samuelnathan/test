<?php
/* Original Author: David Cunningham / Domhnall Walsh
 * For Qpercom Ltd.
 * Date: 07/01/2014
 * © 2014 Qpercom Limited. All rights reserved.
 * Installation ID Generator
 */
echo str_pad(dechex(mt_rand(0, 0xFFFFFFF)), 7, "0", STR_PAD_LEFT);
?>