<?php
define("_OMIS", 1);
include_once "vendor/autoload.php";
use OMIS\Utilities\Path as Path;

/**
 * Tool to test and view database version upgrades for OMIS
 * Domhnall Walsh
 * © Qpercom 2014
 */

// Ensure that the script only runs from the command line.
if (php_sapi_name() !== "cli") {
    die ("Run me only from the command line!");
}

// Get OMIS' configuration settings
$config = new OMIS\Config;

echo "Database is `" . $config->db['schema'] . "`@" . $config->db['host'] . "\n";

// Get the version of the current revision of OMIS.
$omis_version_text = file_get_contents('version.txt');
if (empty($omis_version_text)) {
    die("Can't get OMIS version number. Please check version.txt\n");
} else {
    $omis_version = explode('@', $omis_version_text)[0];
}

echo "Stated OMIS version is $omis_version\n";

// Glob pattern to match database update scripts.
$pattern = Path::join($config->base_path, "scripts/upgrades/database_$omis_version*.sql");

echo ("Looking for update scripts matching this pattern:\n\t$pattern\n");

// Regular expression to extract database version numbers from filenames.
$regex_pattern = '/' . str_replace('.', '\.', $omis_version) . '\.([0-9]+)/';
print_r($regex_pattern);

$update_scripts = [];

$paths = glob($pattern);
print_r($paths);

foreach ($paths as $path) {
    $path = basename($path);
    preg_match_all($regex_pattern, $path, $matches, PREG_PATTERN_ORDER);
    print_r($matches);
    list($from, $to) = array_map("intval", $matches[1]);
    $update_scripts[] = ['script' => $path, 'from' => $from, 'to' => $to];
}

$max_version_available = max(array_column($update_scripts, "to"));

echo "Most recent version appears to be " . $max_version_available . "\n";

echo "Getting a database instance... ";

$db = \OMIS\Database\CoreDB::getInstance();
echo "?";
var_dump($db);

echo "ok.\n";

// Uncomment when finished working out the execution logic.
$versions_installed = $db->getDatabaseVersions();

$min_version_installed = min($versions_installed);
$max_version_installed = max($versions_installed);

$expected_versions_installed = range($min_version_installed, $max_version_installed);

// Determine if there are any missing versions...
$missing_versions = array_diff($expected_versions_installed, $versions_installed);
if (!empty($missing_versions)) {
    echo ("Updates have been installed in the wrong sequence! Missing updates: " . implode(", ", $missing_versions) . "\n");
    exit(1);
}

/* If the current database version is newer than the maximum we can update, then
 * we're missing an update script. Report as much and die().
 */
if ($max_version_available < $max_version_installed) {
    echo("Most recent upgrade script is for version $max_version_available, yet your database is at $max_version_installed.\n Are there missing upgrade script(s)?\n");
    exit(1);
}

// If we're at the latest version, jolly good...
if ($max_version_available == $max_version_installed) {
    echo("Current version is $max_version_installed. No updates available at this time.\n");
    exit(0);
}

echo "Latest version is $max_version_available / latest currently installed is $max_version_installed.\n";

echo "Determining script update sequence...\n";

// If we get to here, then we have potential for updates.
$incremental_version = $max_version_installed;
while ($incremental_version < $max_version_available) {
    $script_sequence = [];
    $possible_scripts = [];
    
    foreach ($update_scripts as $script) {
        if ($script['from'] == $incremental_version) {
            $possible_scripts[] = $script;
        }
    }
    
    if (count($possible_scripts) == 0) {
        echo "Can't find a file to upgrade from version $incremental_version to a higher version :(\n";
        exit(1);
    } elseif (count($possible_scripts) > 1) {
        echo "Multiple possible options!\n";
        exit(1);
    }
    
    echo " * '" . $possible_scripts[0]['script'] . "' upgrades v$incremental_version to v"
        . $possible_scripts[0]['to'] . "\n";
    $incremental_version = $possible_scripts[0]['to'];
}

echo "...done.\n";