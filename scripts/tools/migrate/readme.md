# Database Migration Tool

## What is it?
This tool is intended to automate updating the OMIS database to whatever patch level is current. To do this:

 * It scans the list of available database patches to see what versions are available, and
 * It connects to the database configured for this instance of OMIS to see what the current version there is.

If the versions available via patch files are newer than the current database patch level, it attempts to determine a chain of patches to apply in order get from the current patch level to the most recent.

If there are two patch files that can provide a forward path from a given patch level, it chooses the one that progresses the largest number of patch levels at once - for example, it will prefer an upgrade script that migrates two patch levels over one that migrates only one level.

## Before you start

This tool introduces additional Composer dependencies, specifically require-dev dependencies. Before you run the tool for the first time, it would be wise to run the following command to get composer to install the requirements:

```sh
$ composer update --dev
```

## How to run it.
Until more options get added (for example, a rollback feature), simply run the script. From the root directory of OMIS, that would be:

```sh
$ php scripts/tools/migrate/migrate.php
```
