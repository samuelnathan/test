<?php

/**
 * Takes an array of numbers and condenses them into a list of groups as much as
 * is possible
 * 
 * @param int[] $numbers Numbers to group
 * @return string[] Range definitions
 */
function getRanges($numbers) {
    sort($numbers);
    
    $ranges = [];
    
    for ($i = 0, $len = count($numbers); $i < $len; $i++) {
        $rangeStart = $numbers[$i];
        $rangeEnd = $rangeStart;
        
        while (isset($numbers[$i + 1]) && ($numbers[$i + 1] - $numbers[$i] == 1)) {
            $rangeEnd = $numbers[++$i];
        }
        
        if ($rangeStart == $rangeEnd) {
            $ranges[] = $rangeStart;
        } else {
            $ranges[] = sprintf("%d-%d", $rangeStart, $rangeEnd);
        }
    }
    
    return $ranges;
}

/**
 * Parses an SQL file, removes blank lines and comments, and then combines any
 * statements spread across multiple lines into single-line statements. Returns
 * an array of SQL statements (if any exist in the file), otherwise FALSE.
 * 
 * @global League\CLImate\CLImate $climate CLI helper object
 * @param string $path Path to the SQL file to parse
 * @return string[]\boolean FALSE on failure, otherwise an array of SQL statements as strings.
 */
function parseSQLFile($path) {
    global $climate;
    if (!file_exists($path) || !is_readable($path)) {
        $climate->error("File $path does not exist or is not readable");
        return \False;
    }
    
    $data = file_get_contents($path);
    
    /* Remove blank lines and comments from the loaded SQL data to leave behind
     * just SQL. Statements may be split across multiple lines.
     */
    $multiline_statements = array_values(array_filter(
        preg_split('/\r?\n/', $data),
        function ($line) {
            // Lambda function to eliminate blank lines and comments.
            $line = trim($line);
            return !((trim($line) == "") || (substr($line, 0, 2) == '--'));
        }
    ));
    
    // If there are no executable statements, then there's no point in continuing.
    if (count($multiline_statements) == 0) {
        $climate->error("Update file " . basename($patch['path']) . " appears to not contain SQL statements.\n");
        return \False;
    }
    
    /* Gather up any statements split across multiple lines and combine them so
     * that we end up with an array of discrete statements.
     */
    $statements = [];
    $statement_startIndex = 0;
    $statement_lineCount = 0;
    for ($i = 0; $i < count($multiline_statements); $i++) {
        $line = trim($multiline_statements[$i]);
        $statement_lineCount++;
        /* If we're not seeing a semicolon denoting the end of a statement at
         * the end of the line, then it's not the end of the statement.
         */
        if (substr($line, -1) != ';') {
            continue;
        }
        
        /* If we get to here, then we've found a semicolon, indicating the end of
         * a statement. Let's gather up the rows we've iterated over since we set
         * the start index and combine them to make a discrete statement, then add
         * that to the list of discrete statements we've identified.
         */
        if ($i == $statement_startIndex) {
            // One-line statement.
            $statements[] = $line;
        } else {
            // Multiline_statement
            $statements[] = implode(
                " ",
                array_map(
                    "trim", 
                    array_slice($multiline_statements, $statement_startIndex, $statement_lineCount)
                )
            );
        }
        /* Move the index of the statement start forward to just after the
         * statement we've just extracted.
         */
        $statement_startIndex = $i + 1;
        $statement_lineCount = 0;
    }
    
    return $statements;
}
