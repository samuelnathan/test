<?php
use Illuminate\Database\Capsule\Manager as Capsule;

$connection = [
    'driver'    => 'mysql',
    'host'      => $system_configuration->db['host'],
    'database'  => $system_configuration->db['schema'],
    'username'  => $system_configuration->db['username'],
    'password'  => $system_configuration->db['password'],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
];

// Fire up Eloquent with the settings we've sent it.
$capsule = new Capsule();
$capsule->addConnection($connection);
$capsule->bootEloquent();

class PatchLevel extends Illuminate\Database\Eloquent\Model {
    // Set name of table
    protected $table = 'database_versions';
    
    // Set id field
    protected $primaryKey = 'version';
    
    /* Don't try to save default Eloquent timestamps, we'll be managing our own
     * here.
     */
    public $timestamps = false;
}