#!/usr/bin/env php
<?php
define('_OMIS', 1);
/* which indexes returned by preg_match_all match which parts of the version
 * number...
 */
define('SEMVER_FULL', 0);
define('SEMVER_MAJOR', 1);
define('SEMVER_MINOR', 2);
define('SEMVER_PATCH', 3);
define('VERSION_FILE', 'version.txt');

// Prevent people from running this from a browser.
if (php_sapi_name() !== 'cli') {
    die("Run me only from the command line!\n");
}

// Include the autoloader so that we get the classes we need.
include_once(__DIR__ . '/../../../vendor/autoload.php');
use OMIS\Utilities\Path as Path;
use GetOptionKit\OptionCollection;
use GetOptionKit\OptionParser;

// Set up the pretty printer for the output.
$climate = new League\CLImate\CLImate;

// Set up the argument parser.

/* Get this instance's OMIS configuration settings and put them in a form that
 * Eloquent can understand.
 */
$system_configuration = new \OMIS\Config();

// Import local helper files.
include 'database.php';
include 'helpers.php';

// Get all the patches, sort them by the "version" field.
$patches = PatchLevel::all()->sort(function ($a, $b) {
    $a = $a->version;
    $b = $b->version;

    if ($a === $b) {
        return 0;
    }

    return ($a > $b) ? 1 : -1;
});

// Get the latest database patch "on file".
$db_patchlevel = (int) $patches->last()->version;
$climate->info("Database version is <bold>$db_patchlevel</bold>");

/* See if there are any patch updates missing from the database update history
 * table. These will be versions prior to $db_version that we'd expect to see
 * getting applied as the database is being updated.
 */
$missing_patches = [];
$expected_patches = range(1, $db_patchlevel);

foreach ($expected_patches as $version_id) {
    $version = Patchlevel::find($version_id);
    if ($version === NULL) {
        $missing_patches[] = $version_id;
    }
}

// If there are missing versions, try to make sense of them in terms of whether
// the update script should continue or not.
if (!empty($missing_patches)) {
    $climate->comment("The following database versions appear to be missing: "
    . "<bold>" . implode(",", getRanges($missing_patches)) . "</bold>");
}

/* Try to get the "major.minor" version of OMIS (e.g. 1.x, 2.x) to determine
 * which database update scripts are worth applying. First, find version.txt in
 * the root OMIS folder if we can.
 */
$version_file = Path::join($system_configuration->base_path, VERSION_FILE);
if (!file_exists($version_file)) {
    $climate->error("Failed to locate /version.txt to identify OMIS major version");
    die();
}

// Get the major.minor version number of OMIS.
$omis_version = explode('@', file_get_contents($version_file))[0];
list($omis_major, $omis_minor, $omis_patch) = explode('.', $omis_version);

// Just in case there's a sanity issue...
if ((int) $omis_patch != $db_patchlevel) {
    $climate->error("Patch level reported in database ($db_patchlevel) doesn't match data in version.txt ($omis_patch)!");
    // Need to check for upgrade scripts based on the patch level not what's in version.txt
    $omis_version = implode(".", [$omis_major, $omis_minor, $db_patchlevel]);
}

if (empty($omis_version)) {
    $climate->error("Failed to extract <major>.<minor> OMIS version number from /version.txt");
    die();
}

// Build the relevant search pattern for glob to find DB update scripts.
$pattern = Path::join(
    $system_configuration->base_path,
    "scripts/upgrades/database_$omis_version*.sql"
);

$climate->whisper("Looking for update scripts matching this pattern:\n\t<bold>$pattern</bold>");

/* This regex matches a version number as defined by the semantic versioning
 * standard 2.0.0
 * @see http://semver.org/
 */
$semver_regex = '/([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?/';

$paths = glob($pattern);
if (empty($paths)) {
    // Glob operation didn't find any files.
    $climate->error("No database update scripts found. Exiting.");
    die();
}

/* We've found one or more potential database update files. Iterate through
 * these to find out what versions they upgrade from and to, and put these in
 * an array we can iterate through and simplify to provide the shortest upgrade
 * path.
 */
$unapplied_patches = [];
$max_patchlevel = 0;
foreach ($paths as $path) {
    // Find all instances of what looks like a semantic version number in the
    preg_match_all(
        $semver_regex,
        basename($path),
        $matches,
        PREG_PATTERN_ORDER
    );

    list($from_patch, $to_patch) = array_map("intval", $matches[SEMVER_PATCH]);
    $climate->whisper("$path updates from version $from_patch to $to_patch");

    if ($from_patch < $db_patchlevel) {
        echo " * Skipping " . basename($path) .", database already at patch $db_patchlevel  \n";
        continue;
    } else {
        $max_patchlevel = $to_patch;
    }

    $unapplied_patches[] = [
        'path'  => $path,
        'from'  => $from_patch,
        'to'    => $to_patch
    ];
}

// If there are no unapplied patches, we're golden.
if (count($unapplied_patches) == 0) {
    $climate->info("No database patches to apply. Exiting.");
    exit(0);
}

$climate->info("Greatest patchlevel detected from upgrade scripts is <bold>$max_patchlevel</bold>");
$climate->whisper("Searching for patchset to get from patchlevel <bold>$db_patchlevel</bold> to <bold>$max_patchlevel</bold>...");

$patch_sequence = [];
/* If there's more than one patch that hasn't been applied, we need to put
 * together what order they should be run.
 */
$current_patchlevel = $db_patchlevel;
while ($current_patchlevel < $max_patchlevel) {
    /* Filter the unapplied patches to see which ones start at the desired
     * patch level.
     */
    $possibles = array_filter(
        $unapplied_patches,
        function ($pl) {
            global $current_patchlevel;
            return ($pl['from'] == $current_patchlevel);
        }
    );

    /* If there's one, choose that. If not, choose the one with the highest
     * destination ("to") patchlevel.
     */
    if (count($possibles) == 0) {
        $climate->error("Can't seem to find any patch files that can update from patchlevel $current_patchlevel");
        die();
    } elseif (count($possibles) == 1) {
        $possible = current($possibles);
        $patch_sequence[] = $possible;
        $climate->whisper("<bold>" . basename($possible['path']) . "</bold> upgrades from "
            . "patchlevel <bold>$current_patchlevel</bold> to <bold>" . $possible['to'] . "</bold>");
        $current_patchlevel = $possible['to'];
    } else {
        // Find out what the largest possible jump is.
        $greatest_patchlevel = max(array_map("intval", array_column($possibles, 'to')));
        $found_patch = \FALSE;
        foreach ($possibles as $possible) {
            if ($possible['to'] == $greatest_patchlevel) {
                $climate->whisper("<bold>" . basename($possibles['path']) . "<bold> upgrades from "
                    . "patchlevel <bold>$current_patchlevel</bold> to <bold>" . $possible['to'] . "</bold>");
                $patch_sequence[] = $possible;
                $current_patchlevel = $possible['to'];
                $found_patch = \TRUE;
                break;
            }
        }

        // In case we can't find a patch for this step..
        if (!$found_patch) {
            $climate->error("Can't seem to find any patch files that can update from patchlevel $current_patchlevel");
            die();
        }
    }
}

// Get the database connection as we'll need it to do the transaction(s).
$connection = $capsule->getConnection();

$climate->info("Executing upgrade scripts...");
foreach ($patch_sequence as $patch) {
    $climate->lightBlue($patch['path']);
    // Parse the SQL file, extract all the statements.
    $statements = parseSQLFile($patch['path']);

    if (!is_array($statements) && ($statements === \False)) {
        /* If we couldn't read the script, then something went wrong. We can't
         * assume that it's safe to run subsequent patches until this error is
         * fixed, so we need to abort the operation.
         */
        die();
    }

    $pdo = $connection->getPdo();
    // Okay, time to start making changes.
    try {
        // Within a transaction, execute all SQL statements for this patch level.
        $pdo->beginTransaction();
        $climate->out("Applying patch...");
        foreach ($statements as $statement) {
            $climate->whisper($statement);
            $pdo->exec($statement);
            $climate->out("-> done.");
        }
        $climate->info("Committing changes...");
        $pdo->commit();

        /* If the script ran, then check to see if it's been logged that any patch
         * levels covered by this script have been logged in the database as
         * having been applied.
         */
        $applied_patchlevels = range($from_patch + 1, $to_patch);
        foreach ($applied_patchlevels as $patchlevel) {
            $pl = PatchLevel::find($patchlevel);
            if ($pl === NULL) {
                $pdo->beginTransaction();
                // Log this patch level as having been applied.
                $sql = "INSERT INTO `database_versions` (`version`) VALUES ($patchlevel)";
                $pdo->exec($statement);
                $pdo->commit();
                $climate->info("Patchlevel <bold>$patchlevel</bold> marked as applied to database as of now.");
            } else {
                /* We've already marked this patch levels as being applied - it
                 * probably happened inside the DB update script. No further
                 * action needed then.
                 */
                $climate->comment("Patchlevel <bold>$patchlevel</bold> already marked as applied to database.");
            }
        }
    } catch (\PDOException $e) {
        list($sqlstate, $error_code, $error_message) = $pdo->errorInfo();
        $climate->error(sprintf("SQLSTATE %s: Error %s - %s\n", $sqlstate, $error_code, $error_message));
        $climate->info("Rolling back database transaction.");
        $success = $pdo->rollBack();
        if (!$success) {
            $climate->error("Database Rollback failed");
        } else {
            $climate->info("Database Rollback succeeded.");
        }
        die();
    }
}

$climate->info("Done.");

//This should hopefully update version.txt to reflect the current database version.

// Ask git when the last update script was pushed.
$git_cmd = sprintf("git log %s | grep \"Date:\" | cut -f2- -d\" \" | sed -e 's/^[ \t]*//;s/[ \t]*$//'", $path);
$last_update_date = \Carbon\Carbon::parse(shell_exec($git_cmd));

// Format the string to put in version.txt
$version_file_contents = sprintf(
    "%d.%d.%d@released %s\n",
    $omis_major,
    $omis_minor,
    $patchlevel,
    $last_update_date->toFormattedDateString()
);

// Replace the version file with the new file.
file_put_contents($version_file, $version_file_contents);
