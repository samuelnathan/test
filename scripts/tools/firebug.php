<?php
/**
 * Instantiates a FirePHP instance that can be used for debugging.
 * FirePHP ( http://www.firephp.org ) allows for PHP variables and other data to
 * be outputted to the Firebug console in Firefox, and consists of two parts:
 * 
 * 1. A server-side component. Installation instructions are available at
 *    http://www.firephp.org/HQ/Install.htm (do it with PEAR, it's easier), and
 *    basic usage is at http://www.firephp.org/HQ/Learn.htm .
 * 2. A Firefox extension (which in turn requires the Firebug extension to be
 *    loaded as well). See https://addons.mozilla.org/en-US/firefox/addon/firephp/
 * 
 * To use this script:
 * 
 * 1. Require/include this file.
 * 2. Instantiate FirePHP using something like this:
 *    
 *    $firephp = startFirePHP();
 * 
 * @global FirePHP|none $firephp
 * @return FirePHP
 */
function startFirePHP()
{
    global $firephp;
    if (!isset($firephp)) {
        if (!@include_once('FirePHPCore/FirePHP.class.php')) {
            // Firebug is not installed on this system...
            $error = "Tried to load FirePHP, but it doesn't seem to be installed";
            error_log(__FILE__ . ": $error...");
            die($error);
        }

        /* If we get to here, then we have FirePHP. Next, start output_buffering if
         * required...
         */
        if (ob_get_level() == 0) {
            ob_start();
        }

        return FirePHP::getInstance(true);
    } else {
        return $firephp;
    }
}
