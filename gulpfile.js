const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const plugins = require('gulp-load-plugins')();
const uglify = require('gulp-uglify-es').default;
const javascriptObfuscator = require('gulp-js-obfuscator');
const modifyFile = require('gulp-modify-file');
const plumber = require('gulp-plumber');
//const del = require('del');


function updateVersion(content) {
    var versionObj = JSON.parse(content);
    var nextBuild = parseInt(versionObj.assetsBuild) + 1;
    versionObj.assetsBuild = `${nextBuild}`;

    return JSON.stringify(versionObj, null, 2);
}


/**
 * BUILD minify all of our CSS
 */
gulp.task('build:css', function () {
    return gulp.src('assets/src/css/*.css')
        .pipe(plugins.sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('assets/css'));
});

/**
 * BUILD minify all JS
 */
gulp.task('build:js', function () {
    return gulp.src('assets/src/js/*.js')
        .pipe(plugins.sourcemaps.init())
        .pipe(plumber({
            errorHandler: function (err) {
                if (err.message) {
                    console.log(err.message);
                    console.log('filename: ', err.filename);
                    console.log('line: ', err.line);
                    console.log('col: ', err.col);
                    console.log('pos: ', err.pos);
                }else{
                    console.log(err);
                }
            }
        }))
        .pipe(uglify({
            mangle: true
        }))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('assets/js'));
});

/**
 * BUILD PRODUCTION minify all JS (no source maps)
 */
gulp.task('build-production:js', function () {
    return gulp.src('assets/src/js/*.js')
        .pipe(plumber({
            errorHandler: function (err) {
                if (err.message) {
                    console.log(err.message);
                    console.log('filename: ', err.filename);
                    console.log('line: ', err.line);
                    console.log('col: ', err.col);
                    console.log('pos: ', err.pos);
                }else{
                    console.log(err);
                }
            }
        }))
        .pipe(uglify({
            mangle: true
        }))
        .pipe(gulp.dest('assets/js'));
});


/**
 * BUILD PRODUCTION minify all of our CSS (no source maps)
 */
gulp.task('build-production:css', function () {
    return gulp.src('assets/src/css/*.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('assets/css'));
});

/**
 * DEPLOY (Experimental) minify all JS (no source maps and js code obscured)
 */
gulp.task('deploy:js', function () {
    return gulp.src('assets/src/js/*.js')
        .pipe(javascriptObfuscator({}, {}))
        .pipe(gulp.dest('assets/js'));
});

/**
 * WATCH mode minify JS and CSS
 */
gulp.task('watch', function() {
    gulp.watch('assets/src/css/*.css', gulp.series('build:css', 'update-version'));
    gulp.watch('assets/src/js/*.js', gulp.series('build:js', 'update-version'));
});

gulp.task('update-version', function() {
    return gulp.src('version.json')
        .pipe(modifyFile(updateVersion))
        .pipe(gulp.dest('.', { overwrite: true }));
});

// Build dev mode
gulp.task('build', gulp.series('build:js', 'build:css', 'update-version'));

// Build production mode
gulp.task('build-production', gulp.series('build-production:js', 'build-production:css', 'update-version'));

// Deploy mode - Experimental - (difference from build - no source maps and js code obscured)
gulp.task('deploy', gulp.series('deploy:js', 'build-production:css'));

// Watch mode
gulp.task('default', gulp.series('watch'));