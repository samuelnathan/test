<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

  define('_OMIS', 1);

  $loader = require 'vendor/autoload.php';
  $config = new OMIS\Config;
  $session = new OMIS\Session;

  // Check if page variable is set
  if (isset($_REQUEST['page'])) {

      $pageFilter = filter_var($_REQUEST['page'], FILTER_SANITIZE_STRING);
      $page = ($pageFilter === false) ? "" : $pageFilter;

  } else {

      $page = "";
  }

 // Set to do session variable
 if (isset($_SESSION['todo']) && $_SESSION['todo'] != '1') {
    $_SESSION['todo'] = '1';
 }

 // DW: Use an array to activate buffering for the image processing step.
 $bufferedPages = ['dresults'];
 if (in_array($page, $bufferedPages)) {
    ob_start();
 }

 // Allow these pages to be accessed when logged out.
 if (!in_array($page, array('main', 'request'))) {
    $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
 }

 // Page with menus. turned off by default
 $pageWithMenus = false;

 // Non-Menu Popup Pages
 switch ($page) {

    // Feedback PDF Preview Tool
    case 'prevfb_tool':
        require_once 'extra/header_html.php';      
        require_once 'pdfgen/frame_feedback.php';
        break;

    // Template Tool [Related to Feedback Tool]
    case 'template_tool':
        require_once 'extra/header_html.php';
        require_once 'tools/template_tool.php';
        break;

    // SMS Examiners [Related to Manage Examiners Section]
    case 'sms_examiners':
        require_once 'extra/header_html.php';
        require_once 'examiners/sms_examiners.php';
        break;

    // Print Groups Section
    case 'preview_groups':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/preview_groups.php';
        break;

    // PDF Groups Section
    case 'pdf_groups':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/frame_groups.php';
        break;

    // Print Examinees
    case 'preview_examinees':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/preview_examinees.php';
        break;

    // Examinee list in PDF format
    case 'pdf_examinees':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/frame_examinees.php';
        break;

    // PDF Session Stations Section
    case 'pdfstations':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/frame_stations.php';
        break;

    // PDF Examinee Results Section
    case 'pdfresults':
        require_once 'extra/header_html.php';
        require_once 'pdfgen/frame_results.php';
        break;

    // Preview Session
    case 'previewform_osce': 
        require_once 'extra/header_html.php';
        require_once 'forms/previewform_osce.php';
        break;

    // Analysis print
    case 'out_analysis':
        require_once 'extra/header_html.php';
        require_once 'analysis/out_analysis.php';
        break;

    // All Examinee Results
    case 'studentexam':
        require_once 'extra/header_html.php';
        require_once 'analysis/studentexam.php';
        break;

    // All Examinee Results History
    case 'studenthistory':
        require_once 'extra/header_html.php';
        require_once 'analysis/studenthistory.php';
        break;

    // Details of Station/Examinee Examination
    case 'studentresult':
        require_once 'extra/header_html.php';
        require_once 'analysis/studentresult.php';
        break;

    // Delete results
    case 'dresults':
        require_once 'extra/header_html.php';
        require_once 'analysis/delete_results.php';
        break;

    // Results detailed
    case 'resdets':
        require_once 'extra/header_html.php';
        require_once 'analysis/results_detailed.php';
        break;

    // The Default Action
    default:
        // Display menusm set to true and passed to header_html.php
        $pageWithMenus = true;
        require_once 'extra/header_html.php';
        require_once 'pages/page_locator.php';
        break;
 }

?>
</body>
</html>
