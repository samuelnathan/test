<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 23/03/2014
 * © 2014
 * Redirect file for Administrators section
 */
/* Defined to tell any included files that they have been included rather than
 * being invoked directly from the browser. Need to declare it anywhere that a
 * file is directly invoked by the user (via AJAX).
 */
define('_OMIS', 1);

use \OMIS\Session\FlashList as FlashList;
use \OMIS\Session\FlashMessage as FlashMessage;

$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

$flashlist = new FlashList();

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = array(
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    );
    $template->render($data);
    return;
}

//Administrator Section Redirect
if (isset($_POST['go_admin'])) {
    if ($_POST['todo'] == "delete") { //If Delete
        //Delete administrators from database
        $db->users->deleteAdmins($_POST['admin_check']);

        //Current User Warnings Regarding Deletion, Disabling etc
        if (in_array($_SESSION['user_identifier'], $_POST['admin_check'], true)) {
            $flashlist->add(
                "The logged on user ". $_SESSION['user_identifier']
                    . " is not permitted to delete, disable or change the access "
                    . "level of their own account",
                FlashMessage::SESSION_FLASH_ERROR
            );
        }
    }
    header('Location: ../manage.php?page=man_admin');
} else {
    //If none of the above return to index page    
    header('Location: ../');
}
