<?php

define('_OMIS', 1);
require_once __DIR__ . '/../extra/essentials.php';

function getResultCount(\OMIS\Database\CoreDB $db) {
    $dateFrom = filter_input(INPUT_GET, 'dateFrom', FILTER_SANITIZE_NUMBER_INT) ?: null;
    $dateTo = filter_input(INPUT_GET, 'dateTo', FILTER_SANITIZE_NUMBER_INT) ?: null;

    if ($dateFrom !== null)
        $dateFrom = \Carbon\Carbon::createFromTimestamp($dateFrom);
    if ($dateTo !== null)
        $dateTo = \Carbon\Carbon::createFromTimestamp($dateTo);

    return [
        'count' => $db->results->getCompleteResultsCount($dateFrom, $dateTo)
    ];
}

function getLastActiveUsers(\OMIS\Config $config) {
    $offset = filter_input(INPUT_GET, 'offset', FILTER_SANITIZE_NUMBER_INT) ?: 0;
    $length = filter_input(INPUT_GET, 'length', FILTER_SANITIZE_NUMBER_INT) ?: 20;

    return array_map(function (array $user) {
        return [
            'date' => $user['date']->timestamp,
            'ip' => $user['ip']
        ];
    }, $config->lastActiveUsers()
        ->getLastActiveUsers($length, $offset));
}

$q = filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING);

switch ($q) {
    case 'results_count':
        $result = getResultCount($db);
        break;

    case 'last_active_users':
        $result = getLastActiveUsers($config);
        break;
}

echo json_encode($result);





