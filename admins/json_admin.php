<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 const ADMINS_SCHOOLS = false;
 const RETURN_SCHOOLS_AS_ARRAY = true;
 const RETURN_MYSQLIRESULT = false;

 define("_OMIS", 1);
 ob_start();

 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
 use \OMIS\Utilities\JSON as JSON;
 use \OMIS\Auth\Role as Role;  
 
 $isfor = filter_input(INPUT_POST, "isfor", FILTER_SANITIZE_STRIPPED);

 if (!is_null($isfor)) {
   
    require_once __DIR__ . "/../extra/essentials.php";
    
    // Do a session check to see if the user's logged in or not.
    if (!isset($session)) {
        $session = new OMIS\Session();
        $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
    }
    
    if (!Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    
 } else {
   
    http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
    exit();
    
 }

 $jsonData = [];

 //  Add Students JSON
 switch ($isfor) {

   /**
   * Save Admin Schools
   */
   case "save_admin_schools":

     $user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
     $schoolsString = filter_input(INPUT_POST, 'schools', FILTER_SANITIZE_STRING);

     if (is_null($user) || is_null($schoolsString) || strlen($schoolsString) == 0) {
       http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
       exit(); 
     }

     // Remove schools, except these (School Admins Only)
     if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {

        $adminSchools = $db->schools->getAdminSchools(
            $_SESSION['user_identifier'],
            RETURN_SCHOOLS_AS_ARRAY,
            ADMINS_SCHOOLS
        );

        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));

     } else {

        $schoolIDs = null;

     }

     $db->schools->removeAdminFromSchools($user, $schoolIDs);
     $db->schools->addAdminToSchools(
         $user, 
         explode(',', $schoolsString)
     );

     break;
    
   /**
   * Get Admin Schools 
   */
   case "get_admin_schools":
     
      $user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
      $adminSchools = $db->schools->getAdminSchools(
         $user,
         RETURN_SCHOOLS_AS_ARRAY,
         ADMINS_SCHOOLS
      );
      $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
           
      /** 
       * If admin is a school admin then only return
       * linked schools
       */
      if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {
      
         $schools = $db->schools->getAdminSchools(
             $_SESSION['user_identifier'],
             RETURN_MYSQLIRESULT,
             ADMINS_SCHOOLS
          );        
        
      // All other admin roles
      } else {
        
         $schools = $db->schools->getAllSchools();
        
      }
      
      while ($school = $db->fetch_row($schools)) {
         
          $jsonData[] = [
             'id' => $school['school_id'],
             'name' => $school['school_description'],
             'linked' => (in_array($school['school_id'], $schoolIDs))
          ];

      }
        
      break;

    // Bad Request
    default:
       http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
       exit();
       break;
 }

 header("Content-type: application/json");
 try {
    $json = JSON::encode($jsonData);
 } catch (Exception $ex) {
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    print_r_log($jsonData);
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
 }

 // Data to be sent
 echo $json;
