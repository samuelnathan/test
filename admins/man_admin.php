<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

use OMIS\Auth\Role as Role;
// Page Access Check / Can User Access this Section?
if (!Role::loadID($_SESSION['user_role'])->canAccess($page)) {
    return false;
}

include __DIR__ . '/../extra/helper.php';

// Define variables
$rowCount = 0;
$admins = [];
$columns = [
   'user_id',
   'forename',
   'surname',
   'email',
   'password',
   'user_role',
   'last_login',
   'activated'
];
$url = "manage.php?page=man_admin";
list($orderType, $orderIndex, $orderMap) = prepareListOrdering('desc', 0, $columns);

// Get admin list from database
$adminsDB = $db->users->getAllAdmins();
$count = mysqli_num_rows($adminsDB);

// Gather Admin Data
while ($admin = $db->fetch_row($adminsDB)) {
    $admins[] = $admin;
}
?>
<div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Manage Administrators
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      In this section you can manage system admins and <?=gettext('school')?> admins
    </div>   
  </div> 
</div> 
</div>
<div class="contentdiv">
<form name="admin_list_form" id="admin_list_form" method="post" action="admins/admins_redirect.php" onsubmit="return(ValidateForm(this))" autocomplete="false">
 <input style="display:none;" type="text" name="disable_chrome_autfill"/>
 <input style="display:none;" type="password" name="disable_chrome_autfill"/>
<table id="admin_table" class="table-border">
 <?php
   
   // Delete warning
   $deleteWarning = "The logged on user <span class='ul'>".
                    $_SESSION['user_identifier']. 
                    "</span> is not permitted to delete, disable or change the access level of their own account";
   caption_Session_Warn('editingadmin_warn', $deleteWarning);
   
   $disabledProperty = ($count == 0) ? ' disabled="disabled"' : '';
   
  ?>
  
  <tr id="title-row" class="title-row">
  <td class="all-checkbox-td">
    <input type="checkbox" id="check-all"<?=$disabledProperty?>/>
  </td>
  <th><?=makeColumnOrderable('Identifier', $orderMap[$orderIndex], 'user_id', 1, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Forenames', $orderMap[$orderIndex], 'forename', 2, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Surname', $orderMap[$orderIndex], 'surname', 3, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Email Address', $orderMap[$orderIndex], 'email', 4, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Password', $orderMap[$orderIndex], 'password', 5, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('User Role', $orderMap[$orderIndex], 'user_role', 6, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Last Login', $orderMap[$orderIndex], 'last_login', 7, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th><?=makeColumnOrderable('Account', $orderMap[$orderIndex], 'activated', 8, $orderType, $admins, $url, 'standard', NULL, NULL)?></th>
  <th id="options-th">&nbsp;</th>
  <td class="title-edit-top">&nbsp;</td>
  </tr>
 
 <?php
   
  // Render admin records if we have a count greater than 0
  if ($count > 0) {

    foreach ($admins as $admin) {
      $userID = $admin['user_id'];
      $rowCount++;
     ?>
     <tr id="admin_row_<?=$rowCount?>" class="datarow">
     <td class="checkb">
       <input type="checkbox" name="admin_check[]" id="admin-check-<?=$rowCount?>" value="<?=$userID?>"/>
     </td>
     <td><?=$userID?></td>	
     <td><?=$admin['forename']?></td>
     <td><?=$admin['surname']?></td>
     <td><?=$admin['email']?></td>
     <td>***************</td>
     <td><?=$admin['user_role']?></td>
     <td><?=format_DateTime($admin['last_login'])?></td>
     <?php 
      // Account enabled or disabled ?
      if ($admin['activated'] == 1) {
     ?>
       <td class="c green">Enabled</td>
     <?php 
      } else {
     ?>
       <td class="c red">Disabled</td>
     <?php
      } 
     ?>
     <td class="options-td">
       <?php if ($admin['user_role_id'] == Role::USER_ROLE_SCHOOL_ADMIN) { ?>
         <img class="admin-schools" 
              title="Click to manage linked Admin <?=ucwords(gettext('schools'))?>" 
              alt="office" src="assets/images/link.png">
       <?php } ?>
       &nbsp;
       </td>
     <td class="titleedit2">
    <?php 
      // Edit button rendering
      editButton('edit_'.$rowCount, 'Edit', 'Edit', 'editb', 'editba');
    ?>
     </td>
  </tr>
 <?php
  }

  // Render base row if we have more than 0 rows
  if ($rowCount > 0) {
  ?>
   <tr id="base-tr">
   <td colspan="11" class="base-button-options" id="base-button-td">
   <div class="fl">
    <select name="todo" id="todo" disabled="disabled" class="custom-select custom-select-sm">
      <option value="delete">delete from system</option>
    </select>
    <input type="submit" value="go" name="go_admin" id="go-admin" disabled="disabled" class="btn btn-outline-default btn-sm" />
   </div>	
   <div class="fr" id="add-div">
    <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
    <input type="hidden" value="<?=$rowCount?>" name="count" id="count"/>
   </div>
   </td>
   </tr>
	  
 <?php
  }   
 } else {
 // Add an administrator message    
 $message = "No Administrators found, click the 'add' button below to add a new Administrator";   
 ?> 
 
 <tr id="first_admin_row">
 <td colspan="11" class="mess">
  <div id="admin_row_div" class="tablestatus2"><?=$message?></div>
 </td>
 </tr>
 <tr id="base-tr">
  <td colspan="11" class="base-button-options" id="base-button-td">	
    <div class="fr" id="add-div">
     <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
    </div>			 
   </td>
  </tr>
<?php
 }
?> 
 </table>
</form>
</div>
<div id="dialog-school-admins" title="Administrator <?=ucwords(gettext('schools'))?>" style="display:none">
  <form>
    <div id="dialog-message">
        Link administrator 
           <span class="red"><span id="admin-surname"></span>,
               <span id="admin-forenames"></span>
           </span> to <?=ucwords(gettext('schools'))?>
    </div>
    <span style="display:none" id="link-error"></span>
    <span style="display:none" id="link-loading">Loading data....</span>
    <ul id="linked-schools" data-scroll-scope style="display: none">
        <li>
            <input type="checkbox" id="all-schools"/><?php
            ?><label for="all-schools" class="bold">All <?=ucwords(gettext('schools'))?>
         </label>
        </li>
    </ul>
  </form>
</div>
<?php
  /* Password strength settings for the password fields
   * Plugin: pwstrength bootstrap Jquery
   */
   if (isset($config->passwords)) {
?>
    <script>
     // pass PHP variable declared above to JavaScript variable
     var verdictList = <?=json_encode(['weak', 'normal', 'medium', 'strong', 'very strong'])?>;
     var passwordSettings = <?=json_encode($config->passwords)?>;
    </script>
<?php
   }
?>
