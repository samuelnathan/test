<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 const RETURN_SCHOOLS_AS_ARRAY = true;
 const ADMINS_SCHOOLS = false;

 define('_OMIS', 1);
 require_once __DIR__ . '/../extra/essentials.php';

 use \OMIS\Auth\Role as Role;
 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Template as Template;
 use \OMIS\Session\FlashList as FlashList;
 use \OMIS\Session\FlashMessage as FlashMessage;

 // Page Access Check / Can User Access this Section?
 if (!Role::loadID($_SESSION['user_role'])->canAccess($page)) {
    return false;
 }

 // what is the action
 $mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

 // If no action is defined then exit
 if (empty($mode)) {
    include __DIR__ . '/../extra/noaccess.php';
 }

 // User roles 
 $adminRoles = [];
 $userRoleID = $_SESSION['user_role'];
 $adminRoleIDs = (new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS))->role_ids;
   
 
 foreach ($adminRoleIDs as $roleID) {
   
    /*
     * Only show school admin role as option if user is school admin
     */
     if ($userRoleID == Role::USER_ROLE_SCHOOL_ADMIN) {
        $adminRoles[] = Role::loadID($userRoleID)->toArray();
        break;
     }
   
    /* Only include the "Super Admin" role here if the user themselves are a 
     * super admin.
     */   
    if ($roleID != Role::USER_ROLE_SUPER_ADMIN || $userRoleID == Role::USER_ROLE_SUPER_ADMIN) {
        $adminRoles[] = Role::loadID($roleID)->toArray();
    }
 }
 
 // What action is required
 switch ($mode) {
    
    // Add administrator
    case 'add_admin':
        $template = new Template("ajx_admin_add.html.twig");
        $data = [
          'show_change_checkbox' => false,
          'enable_password_fields' => true,
          'roles' => $adminRoles
        ];
        $template->render($data);
        break;
    
    // Edit administrator
    case 'edit_admin':
        $username = trim(filter_input(INPUT_POST, 'id'));
        $userData = $db->users->getUser($username, 'user_id', true);
        $data = [
           'show_change_checkbox' => true,
           'enable_password_fields' => false,
           'roles' => $adminRoles,
           'user' => $userData
        ];

        if (is_null($data['user'])) {
            return 'USER_NON_EXIST';
        }

        $template = new Template("ajx_admin_edit.html.twig");
        $template->render($data);
        break;
    
    // Submit administrator
    case 'submit_admin':
        $username = trim(filter_input(INPUT_POST, 'id'));
        
        // Invalid Identifier, then abort
        if (!identifierValid($username)) {
           error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
           exit;
        }
       
        if (!$db->users->doesUserExist($username)) {

            echo $db->users->insertAdmin(
                $username,
                $_POST['fn'],
                $_POST['sn'],
                $_POST['em'],
                $_POST['pass'],
                $_POST['lv'],
                $_POST['ac']
            );
              
            // Link admin to schools (School Admins Only)
            if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {

                $adminSchools = $db->schools->getAdminSchools(
                    $_SESSION['user_identifier'],
                    RETURN_SCHOOLS_AS_ARRAY,
                    ADMINS_SCHOOLS
                );

                $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));

            } else {

                $schoolIDs = null;

            }
        
            $db->schools->addAdminToSchools($username, $schoolIDs);

        } else {
            echo 'USER_EXISTS';
        }
        break;
        
    // Update administrator
    case 'update_admin':
        $orginalID = trim(filter_input(INPUT_POST, 'orgid'));
        
        if ($db->users->doesUserExist($orginalID)) {
            $editID = trim(filter_input(INPUT_POST, 'id'));
            
            // Invalid Identifier, then abort
            if (!identifierValid($editID)) {
                 error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
                 exit;
            }        
           
            if ($editID != $orginalID && $db->users->doesUserExist($editID)) {
                echo 'USER_ALREADY_USED';
            } elseif ($db->users->updateAdmin($editID, $orginalID, $_POST['fn'], $_POST['sn'], $_POST['em'],
                    $_POST['pass'], $_POST['passc'], $_POST['lv'], $_POST['ac']) == true) {
                // If user is admin set edited warning flag and update user details
                if ($_SESSION['user_identifier'] == $orginalID) {
                    $flashlist = new FlashList();
                    $flashlist->add(
                        "The logged on user ". $_SESSION['user_identifier']
                            . " is not permitted to delete, disable or change the access "
                            . "level of their own account",
                        FlashMessage::SESSION_FLASH_ERROR
                    );
                    $_SESSION['user_forename'] = $_POST['fn'];
                    $_SESSION['user_surname'] = $_POST['sn'];
                    $_SESSION['user_email'] = $_POST['em'];
                    $current_user = true;
                } else {
                    $current_user = false;
                }

                // If new ID is not the same as old ID
                if ($orginalID != $editID) {
                    if ($current_user) {
                        $_SESSION['user_identifier'] = $editID;
                    }
                    $db->users->updateExaminerID($editID, $orginalID);
                    $db->forms->changeAuthorID($editID, $orginalID);
                }

            }
        } else {
            echo 'USER_NON_EXIST';
        }
        break;
 }
