## Stitch Data Tool | OMIS | NSHCS

## Description
Allows user to export (.xls) student results from exams across multiple departments found in OMIS (OSCE Management Information System). 
In other words the tool "stitches" the station results for each student for all selected exams.

## Installation
Simply place the 'stitch/' directory in the root of OMIS

## Compatibility 
OMIS version <= 1.9.x

## How to use ?
 * Navigate to (instance_url)/stitch and login with OMIS credentials (username and password).
 * Choose 1 or more exams from the department list on the left hand side and add them to 
   list on the right hand using the add ">" button. This is the export list.
 * when you're happy with the selection, click the 'export' button. An Excel sheet (.xls) containing the desired results 
   will download.
 * To logout, simply click the 'logout' button.
 
 ## Future developments
 * Allow user to choose fields to export (student name, exam name, session, date etc.) 



