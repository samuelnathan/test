<?php 
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 28/09/2015
 * For OMIS version <= 1.9.57
 * � 2015 Qpercom Limited.  All rights reserved.
 */
 session_start();

 define('_OMIS', 1);
 
 // Make sure we have a working autoloader without declaring it twice...
 global $loader;
 if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
 }
 
 /*
  * Load system configuration
  */
 $config = new OMIS\Config();

 /*
  * Load help functions
  */
 require_once '../extra/usefulphpfunc.php';
 require_once 'DatabaseHelper.php';
 require_once 'HtmlHelper.php';
 $dbHelper = new DatabaseHelper();
 $htmlHelper = new HtmlHelper();

 /*
  * Connect to system database
  */
 $db = \OMIS\Database\CoreDB::getInstance();
 
 /*
  * What mode dafault(login screen), login and logout
  */
  // $mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
  $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : '';
  switch ($mode) {
   
  // Validate login 
  case 'login':
  
  // Sanitize 'username' and 'password'
  $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  
  // Check 'username' and 'password'
  if ($db->users->checkPassword($username, $password)) {
     $_SESSION['verified_user'] = $username;
     $htmlHelper->renderHeader();
     $formAction = "export.php";
     $htmlHelper->renderForm($formAction);
     
     // Get current academic term
     $termRecord = $db->academicterms->getDefaultTerm();
     $termID = $termRecord['term_id'];
    
     // Get exam records
     $exams = $dbHelper->getExams($db, $termID);     
     $htmlHelper->renderOptions($exams, $termID);
     $htmlHelper->renderClose();
  } else {
     $_SESSION['username_stick'] = $username;
     echo "Login failed. Please try again...";
     header("refresh: 1; index.php");
  }
  break;
  
  // Logout
  case 'logout':

  // If there's a session then logout
  if (isset($_SESSION['verified_user'])) {
    unset($_SESSION['verified_user']);
    session_destroy();
  }
  
  echo "logging out..";
  header("refresh: 1; index.php");
  break;
  
  // Start page / login fields
  default: 
  $htmlHelper->renderHeader();
  $formAction = "index.php";
  $htmlHelper->renderForm($formAction);
  $htmlHelper->renderLoginFields();
  $htmlHelper->renderClose();
  
 }
