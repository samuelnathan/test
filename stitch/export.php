<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 session_start();
 
// User not verified then exit
if (!isset($_SESSION['verified_user'])) {
    echo "You do not have permission to continue";
    header("refresh: 2; index.php");
    exit();
}

 define('_OMIS', 1);
 
 // Make sure we have a working autoloader without declaring it twice...
 global $loader;
 if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
 }
 
 /*
  * Load system configuration
  */
 $config = new OMIS\Config();

 /*
  * Load help functions
  */
 require_once '../extra/usefulphpfunc.php';
 require_once 'DatabaseHelper.php';
 require_once 'HtmlHelper.php';
 $dbHelper = new DatabaseHelper();
 $htmlHelper = new HtmlHelper();

 /*
  * Connect to system database
  */
 $db = \OMIS\Database\CoreDB::getInstance();
 
 /*
  * Load help functions
  */
 require_once 'DatabaseHelper.php';
 $dbHelper = new DatabaseHelper();
 
  // Get list of exams chosen by user
  if (isset($_POST['exams']) && is_array($_POST['exams']) && count($_POST['exams']) > 0) {
     $exams = $_POST['exams'];
  // If we have no exams then exit
  } else {
     echo "No exams found...returning to step 1";
     header("refresh: 2; index.php");
     exit();
  }
     
  // Get all student results for chosen exams
  list($maxStationCount, $results) = $dbHelper->getStudentResults($db, $exams);
  
  
  // Complete result
  $studentResults = [];
  
  foreach ($results as $studentID => &$stations) {
     
     // Template student row
     $studentRow = [
        'total_score' => "",
        'total_score_percentage' => "",
        'overall_outcome' => "",
        'stations' => []
     ];
      
     // Exam variables for one student      
     $possibleScore = 0;
     $overallPass = 0;
     $totalScore = 0;
     $stationCount = 0;
     
     // Exam results per station
     foreach ($stations as &$station) {
        
        // Accumulate values
        $possibleScore += $station['total_value'];
        $overallPass += $station['pass_value'];
        $totalScore += $station['score'];   
        $percentageScore = (($station['score'] / $station['total_value']) * 100);
        $passedStation = ($percentageScore >= $station['pass_value']) ? 'pass' : 'fail';
        
        // Station percentage score and outcome
        $station['percentage_score'] = round($percentageScore, 1);
        $station['station_outcome'] = $passedStation;
        
        // Sanitize form name
        $station['form_name'] = html_entity_decode($station['form_name'], ENT_QUOTES, 'UTF-8');
        
        // Station feedback
        $resultID = $station['result_id'];
        $feedback = $dbHelper->getResultFeedback($db, $resultID);
         
        if ($feedback != false) {
           // Decode any html entities for export to excel
           $station['feedback'] = html_entity_decode($feedback, ENT_QUOTES, 'UTF-8');
        } else {
           $station['feedback'] = "";
        }
       
        $stationCount++;
        
     }
     
     // Overall exam results per student
     if ($stationCount > 0) {
                 
       $totalPercentageScore = (($totalScore / $possibleScore) * 100);
       $studentRow['stations'] = $stations;
       $studentRow['total_score'] = $totalScore;
       $studentRow['total_score_percentage'] = round($totalPercentageScore, 1);
       $studentRow['overall_outcome'] = ($totalPercentageScore >= ($overallPass/$stationCount)) ? 'pass' : 'fail';
     
       // Final results for each student
       $studentResults[$studentID] = $studentRow;
     
     }
    
  }
  
  
   
  // Default date
  date_default_timezone_set('Europe/London');
  
  // Template reader
  $templateReader = PHPExcel_IOFactory::createReader("Excel2007");
  
  // Load template file
  $templateObject = $templateReader->load("results.xlsx");
  $templateSheet = $templateObject->getSheet(0);
   
  // Write title row to cell sheet
  $columnIndex = 5;
   
  /**
   * Output titles (for each station)
   */
  $titles = ["Number", "Scenario", "Score", "Score%", "Result", "Feedback"];
  $columnIndex = 4;
  for ($stationNum=1; $stationNum <= $maxStationCount; $stationNum++) {
            
     // Loop through titles  
     foreach ($titles as $title) {
       $templateSheet->setCellValueExplicitByColumnAndRow(
         $columnIndex,
         1,
         $title,
         PHPExcel_Cell_DataType::TYPE_STRING2
       );
       $columnIndex++;
     }
     
  }
       
    
   /**
    * Output data rows
    */
     
     // Row index
     $rowIndex = 2;
     
     // Loop through student result records
     foreach ($studentResults as $studentID => $data) {
       
       // Default column index
       $columnIndex = 0;
       
       // Student ID cell
       $templateSheet->setCellValueExplicitByColumnAndRow(
           $columnIndex++,
           $rowIndex,
           $studentID,
           PHPExcel_Cell_DataType::TYPE_STRING2
       );
       
       // Total cell
       $templateSheet->setCellValueExplicitByColumnAndRow(
           $columnIndex++,
           $rowIndex,
           $data['total_score'],
           PHPExcel_Cell_DataType::TYPE_NUMERIC
       );
       
       // Total Percentage cell
       $templateSheet->setCellValueExplicitByColumnAndRow(
           $columnIndex++,
           $rowIndex,
           $data['total_score_percentage'],
           PHPExcel_Cell_DataType::TYPE_NUMERIC
       );
       
       // Overall
       $templateSheet->setCellValueExplicitByColumnAndRow(
           $columnIndex++,
           $rowIndex,
           $data['overall_outcome'],
           PHPExcel_Cell_DataType::TYPE_STRING2
       );
       
       // Station results linked to the student  
       foreach ($data['stations'] as $eachStation) {
           
         // Station number cell        
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['station_number'],
            PHPExcel_Cell_DataType::TYPE_NUMERIC
         );
         
         // Station name cell
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['form_name'],
            PHPExcel_Cell_DataType::TYPE_STRING2
         );
         
         // Station score cell
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['score'],
            PHPExcel_Cell_DataType::TYPE_NUMERIC
         );
         
         // Station score % cell
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['percentage_score'],
            PHPExcel_Cell_DataType::TYPE_NUMERIC
         );
         
         // Station result cell
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['station_outcome'],
            PHPExcel_Cell_DataType::TYPE_STRING2
         );
         
         // Station feedback cell
         $templateSheet->setCellValueExplicitByColumnAndRow(
            $columnIndex++,
            $rowIndex,
            $eachStation['feedback'],
            PHPExcel_Cell_DataType::TYPE_STRING2
         );
           
       }
       $rowIndex++;
     }
    
  
   // Form file name
   $fileName = "stitched_results_" . date('Ymdhis') . ".xlsx";
   
   // Output file to browser
   header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
   header("Content-Disposition: attachment;filename=\"$fileName\"");
   header("Cache-Control: max-age=0");
   header("Cache-Control: private");
   header("Pragma: private");
   
   $excelWriter = PHPExcel_IOFactory::createWriter($templateObject, 'Excel2007');
   $excelWriter->save('php://output');

?>