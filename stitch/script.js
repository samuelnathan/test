/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 22/07/2015
 * � 2015 Qpercom Limited.  All rights reserved.
 */
 
 $(document).ready(function() {
    
   /* 
    * Add button event
    */ 
   if ($("#add").length) {
      $("#add").click(function() {
         
        var selected = $("#choose-exams option:selected");
        
        if (selected.length) {
           selected.remove();
           selected.appendTo('#export-exams')
           selected.removeAttr('selected');
        }
        enableDisableExport();
     
      });
   }
   
   /* 
    * Remove button event
    */
   if ($("#remove").length) {
      $("#remove").click(function() {
        
        var selected = $("#export-exams option:selected");
        
        if (selected.length) {
           selected.remove();
           selected.prependTo('#choose-exams')
           selected.removeAttr('selected');
        }
        enableDisableExport();
        
      });
   }
   
   /* 
    * Select all exams before exporting 
    */
   if ($("#export").length) {
      $("#export").click(function() {
         $('#export-exams option').prop('selected', true);
      });
   }
   
   /* 
    * Action for logout button
    */
   if ($("#logout").length) {
      $("#logout").click(function() {
          parent.location = "index.php?&mode=logout";
      });
   }
   
    
  /*
   * Enable/Disable export button
   */
  function enableDisableExport() {
    if ($("#export").length) {
      var examsToExport = $("#export-exams option").length;
      var areExams = (examsToExport > 0);
      $('#export').prop('disabled', !areExams);
    }
  }
      
 });
 