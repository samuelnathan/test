<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 28/09/2015
 * For OMIS version <= 1.9.57
 * © 2015 Qpercom Limited.  All rights reserved.
 */
 
class DatabaseHelper
{
    
 // Constructor
 public function __construct()
 { 
 }

 /*
  * Get exams from database
  * @param object $db database connection 
  */
 public function getExams($db, $termID) {
    
    $exams = [];
        
    $sql = "SELECT * FROM exams "
         . "INNER JOIN departments "
         . "USING (dept_id) "
         . "WHERE term_id = '$termID' "
         . "ORDER BY dept_name, exam_name";
    $resultset = $db->query($sql);
    
    // Put exam records into an array
    while ($examRecord = $db->fetch_row($resultset)) {
       
       $deptID = $examRecord['dept_id'];
       $exams[$deptID][] = $examRecord;
    }
    
    return $exams;
    
 }
 
 /*
  * Get student results
  * @param object $db     database connection 
  * @param array $exams   list of exam identifiers in an array
  */
 public function getStudentResults($db, $exams) {
            
    $examString = implode(",",$exams);
    $results = [];
    
    $sql = "SELECT student_results.student_id, session_stations.station_number, "
         . "forms.form_name, forms.total_value, student_results.score, "
         . "session_stations.pass_value, student_results.result_id "
         . "FROM (((student_results "
         . "INNER JOIN session_stations USING (station_id)) "
         . "INNER JOIN exam_sessions USING (session_id)) "
         . "INNER JOIN exams USING (exam_id)) "
	     . "INNER JOIN forms USING (form_id) "
         . "WHERE exams.exam_id IN (". $examString . ") "
         . "AND absent = '0' "
         . "GROUP BY student_results.result_id "
         . "ORDER BY student_id, station_number";
             
    $resultset = $db->query($sql);
    
    // Loop variables
    $maxStationCount = 0;
    $studentStationCount = 1;
    $previousStudent = null;
    
    // Put results records into an array
    while ($resultRecord = $db->fetch_row($resultset)) {
       
      // Student ID
      $studentID = $resultRecord['student_id'];
       
      // New student, reset station count
      if ($previousStudent != $studentID) {
          // If we find a higher station count then record it
          if ($studentStationCount > $maxStationCount) {
            $maxStationCount = $studentStationCount;
          }
          $studentStationCount = 1;
          $previousStudent = $studentID;
          
      // Continue for the same student
      } else {
         $studentStationCount++;
      }
       
      // Remove student id element from the array 
      unset($resultRecord['student_id']);
      
      // Add data to return array      
      $results[$studentID][] = $resultRecord;
      
    }
    
    // Return data, max station count and results
    $returnData = [$maxStationCount, $results];
    
    return $returnData;
    
 }
 
 /*
  * Get feedback
  * @param object $db      database connection 
  * @param int $resultID   ID number for result
  */
 public function getResultFeedback($db, $resultID) {

   $db->query("SET SESSION group_concat_max_len = 10000");
   $sql = "SELECT group_concat(feedback_text SEPARATOR ' ') as competence_comments "
        . "FROM section_feedback "
        . "INNER JOIN form_sections "  
        . "USING(section_id) "
        . "WHERE result_id = '" . $resultID . "' "
        . "ORDER BY form_sections.section_order";

      return $db->single_result($db->query($sql));
 }
 
}

?>