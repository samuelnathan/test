<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 28/09/2015
 * For OMIS version <= 1.9.57
 * � 2015 Qpercom Limited.  All rights reserved.
 */
 
class HtmlHelper
{
    
 // Constructor
 public function __construct()
 { 
 }
    
 /*
  * Render Html Header
  */
 public function renderHeader() {
  
 ?>
  <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
  <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
  <head>
  <meta http-equiv='X-UA-Compatible' content='IE=8'/>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
  <title>Stitch Data Tool | NSHCS</title>
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="style.css"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="script.js"></script>
  </head>
  <body>
 <?php
 }
 
 /*
  * Render html form
  */
 public function renderForm($actionName) {
 
 ?>
 <form action="<?=$actionName?>" method="post" autocomplete="off">
 <?php
 
 }
 

 /*
  * Render login fields
  */
 public function renderLoginFields() {
 
  // Stick username for failed logins
  $usernameDefault = (isset($_SESSION['username_stick'])) ? $_SESSION['username_stick']: "";
 
  ?>
 <fieldset id="login">
  <legend>Stitch Data Tool | NSHCS</legend>
  <div id="username-div">
   <label for="username">Username </label>
   <input type="input" id="username" name="username" value="<?=$usernameDefault?>"/>
  </div>
  <div id="password-div">
   <label for="password">Password </label>
   <input type="password" id="password" name="password" value=""/>
  </div>
  <div id="submit-div">
   <input type="submit" id="submit" value="continue"/>
   <input type="hidden" name="mode" value="login"/>
  </div>
 </fieldset>
  <?php  
  
 }
 
 /*
  * Render options
  * @param array $exams       list of exams to render
  * @param array $termID      current academic term
  */
 public function renderOptions($exams, $termID) {

   //  Array of departments
   $departments = [];
   
  ?>
 <fieldset id="options">
  <legend>Choose Exams to Export - <span id="term"><?=$termID?></span></legend>
  
  <div id="options-div">
    
   <div id="list-div">
    <label for="choose-exams">Exams By Department</label>
    <select id="choose-exams" multiple="multiple" size="20">
    <?php
     
     // Loop through exams and render an option for each
     foreach ($exams as $deptID => $list) {
       
       $deptName = $list[0]['dept_name'];
       $departments[] = [$deptID, $deptName];
       
       ?>
       <optgroup label="<?=$deptID?> - <?=$deptName?>">
       <?php

       foreach ($list as $each) {           
        $examID = $each['exam_id'];
        $examName = $each['exam_name'];
        ?>
        <option value="<?=$examID?>"><?=$examName?></option>
        <?php
       }
      ?> 
      </optgroup>
      <?php
     }
    ?>
    </select>
   </div>   
  
   <div id="buttons-div">
    <input type="button" id="add" title="add" value=">"/>
    <input type="button" id="remove" title="remove" value="<"/>
   </div>
  
   <div id="export-div">
    <label for="export-exams">Exams To Export</label>
    <select id="export-exams" name="exams[]" multiple="multiple" size="20">
   <?php
      
      // Render empty department opt groups
      /*
       foreach ($departments as $dept) {
        $deptID = $dept[0];
        $deptName = $dept[1];        
         <optgroup label="<?=$deptID?> - <?=$deptName?>"></optgroup>  
      } 
      */
 
    ?>
    </select>
   </div>
  </div>
    
  <div id="submit-export-div">
   <input type="button" id="logout" value="logout"/>
   <input type="submit" id="export" value="export" disabled="disabled"/>
   <input type="hidden" name="mode" value="export"/>
  </div>
 </fieldset>
  <?php  
  
 }
 

 /*
  * Render Html bottom
  */
 public function renderClose() {
  ?>
   </form>
   </body>
   </html>
 <?php
 }

}
