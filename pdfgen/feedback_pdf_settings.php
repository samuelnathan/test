<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader))
    $loader = require __DIR__ . '/../vendor/autoload.php';

global $config;
if (!isset($config))
    $config = new OMIS\Config();

use OMIS\Utilities\Path as Path;

/* This points the base folder for TCPDF's images to be the root of this OMIS
 * instance. K_PATH_IMAGES *must* include a trailing directory separator because
 * the people because TCPDF's path handling is stupid.
 */
define('K_PATH_IMAGES', $config->base_path . DIRECTORY_SEPARATOR);

// Get TCPDF to throw an exception if it fails.
define('K_TCPDF_PARSER_THROW_EXCEPTION_ERROR', true);

$db->set_Convert_Html_Entities(false); #PDFs Don't Like Html Entities

$pdf_logo = $config->tcpdf_environment['logo']['path'];
$logo_width_mm = $config->tcpdf_environment['logo']['width_mm'];

#Tags
$tagvs = array(
    'p' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => 0.0001, 'n' => 1)),
    'ul' => array(0 => array('h' => 0.0001, 'n' => 1), 1 => array('h' => '', 'n' => -1)),
    'ol' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => '', 'n' => -1))
);
