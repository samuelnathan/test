<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 06/05/2012
 * © 2012 Qpercom Limited.  All rights reserved.
 */
// create new PDF document
/* This points the base folder for TCPDF's images to be the root of this OMIS
 * instance. K_PATH_IMAGES *must* include a trailing directory separator because
 * the people because TCPDF's path handling is stupid.
 */
define('K_PATH_IMAGES', $config->base_path . DIRECTORY_SEPARATOR);

use OMIS\Reporting\PDF as OMISPDF;

$db->set_Convert_Html_Entities(false); #PDFs Don't Like Html Entities
$pdf = new OMISPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Qpercom LTD');
$pdf->SetKeywords('Qpercom LTD, Objective, Structured, Clinical, Examinations');
$pdf->setHeaderFont(array('times', '', 9));
$pdf->setFooterFont(array('times', '', 9));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(2, 11, 2);
$pdf->SetHeaderMargin(1);
$pdf->setFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintFooter(true);
$pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setLanguageArray($config->tcpdf_environment['language']);
$pdf->setFontSubsetting(false);
$pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
$pdf->SetFillColorArray(array(232, 232, 232), false); #E8E8E8

$tagvs = array(
    'p' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => 0.0001, 'n' => 1)),
    'ul' => array(0 => array('h' => 0.0001, 'n' => 1), 1 => array('h' => '', 'n' => -1)),
    'ol' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => '', 'n' => -1))
);

$pdf->setHtmlVSpace($tagvs);

#Main Logo NEED TO CHECK THESE VALUES
$pdf_logo = $config->tcpdf_environment['logo']['path'];
$logo_width_mm = $config->tcpdf_environment['logo']['width_mm'];
