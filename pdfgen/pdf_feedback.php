<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 define('_OMIS', 1);
 defined('EXCLUDE_INTERNAL_FEEDBACK') or define('EXCLUDE_INTERNAL_FEEDBACK', true);

 require_once __DIR__ . '/../extra/essentials.php';
 use \OMIS\Utilities\Path as Path;

 global $config, $displayForms;

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;
 
 // Load the PDF-related stuff.
 require_once 'feedback_pdf_settings.php';    
 require_once 'feedback_functions.php';
 
 // Close Session To Allow Other PHP Scripts To Run
 session_write_close();
 
 // Unlimited Script Time
 set_time_limit(0);
 
 // Initial Values
 $radarMeanScoresDB = $returnData = $presets = [];
 $displayForms = false;
 
 /**
  *  Flag params passed from feedback options panel
  */
 $postIndices = array_keys($_POST);
 
 foreach ($postIndices as $postIndex) {
    
     //  Ignore variables that are not flags
     if (substr($postIndex, 0, strlen('var_')) === 'var_') 
       continue;
 
     // Param value   
     $value = filter_input(INPUT_POST, $postIndex, FILTER_SANITIZE_NUMBER_INT); 
 
     // Remove 'content_' prefix and do value checks to display form content yes/no
     if (substr($postIndex, 0, strlen('content_')) === 'content_')  {
  
        // Remove 'content_' prefix
        $postIndex = substr($postIndex, strlen('content_'));
  
        // Do we need to display the all forms content?
        if (!is_null($value) && $value == 1) $displayForms = true;
 
     }
    
     // Set each preset setting
     $presets['feedback_' . $postIndex] = $value;
 
     // Remove '_' from index to make CamelCase
     $postIndex = str_replace('_', '', lcfirst(ucwords($postIndex, '_')));
     
     // Double dollar sign creates a variable named after the variable supplied
     $$postIndex = (!is_null($value) && $value == 1);
     
 }
 
 // Notes title text
 $notesTitle = filter_input(INPUT_POST, 'var_notesTitle', FILTER_SANITIZE_STRING);
 
 // Update presets
 $presets['feedback_notes_title'] = $notesTitle;
 $db->userPresets->update($presets);
 $examID = filter_input(INPUT_POST, 'var_exam', FILTER_SANITIZE_NUMBER_INT);
 $templateID = filter_input(INPUT_POST, 'var_template', FILTER_SANITIZE_STRING);
 
 // List of stations to exclude
 $excludeStationNumsParam = filter_input(INPUT_POST, 'var_excludedStations', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
 $excludeStationNums = (!is_null($excludeStationNumsParam) && is_array($excludeStationNumsParam)) ? $excludeStationNumsParam : [];
 
 // List of students to include
 $includeStudentsParam = filter_input(INPUT_POST, 'var_includedStudents', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
 $includeStudents = (!is_null($includeStudentsParam) && is_array($includeStudentsParam)) ? $includeStudentsParam : [];
 
 // Stations overalls
 $stationOveralls = ($summaryOverallScore ||  $summaryOverallOutcome);
 
 // Exam summary table show (yes|no)
 $showExamSummary = ($summaryScores || ($summaryOutcomes || $summaryGrs));
 
 // Weightings choosen
 $examHasWeightedStations = $db->exams->examHasWeightingStations($examID);
 $applyStationWeightings = ($finalscoreWeighted || $finalscoreWeightedPercent);// && $examHasWeightedStations;
 
 // Define the path to store feedback PDFs in.
 $feedbackFolder = Path::join($config->tmp_path, 'feedback');
 $examDB = $db->exams->getExam($examID);
 $templateDB = $db->feedback->getEmailTemplates($templateID, true);
 
 // Pre-flight checks (fails then exit script)
 if (!generateFeedbackReady($feedbackFolder, $examDB, $templateDB))
    exit;

 // Get unique stations by exam
 $stations = $db->exams->getStationsByExam(
         $examID, 
         $excludeStationNums
 );
 
  // Form weightings
 if ($applyStationWeightings && $examHasWeightedStations) {
   
    $formWeightings = array_column($stations, 'weighting', 'form_id');
    $examTotals = $db->results->getOverallScore(
         $examID
    );
       
 }
 
 // Get exam station numbers
 $stationNumbers = array_unique(array_column($stations, 'station_number'));
 
 // Get Global Rating Scale Data
 $grsData = $db->results->getGlobalRatingScaleData($examID);
 
 // Get exam rules 
 $rules = $db->examRules->getRules($examID);
 $maxGrsFailRule = $rules['max_grs_fail'];
 $multiExaminerAvgRule = (bool) $rules['multi_examiner_results_averaged'];
 
 // Get form IDs
 $formIDs = array_unique(array_column($stations, 'form_id'));
 
 // Get forms (Indexed by form ID)
 $forms = $db->forms->getMultipleForms($formIDs);
 
 // Station mean score for radar plot (if graph output required by user)
 if ($radarplotComparison) {
 
     foreach ($formIDs as $eachFormID) {
 
         $avgScoreRecord = $db->results->getOverallScore(
                 $examID,
                 $eachFormID, 
                 'form_id'
         );
        
         // Station weighted scores required to displayed on radar plot
         if ($applyStationWeightings && $examHasWeightedStations) {

             $calcWeightedScore = ((($avgScoreRecord['score'] / $avgScoreRecord['possible']) * $formWeightings[$eachFormID]) / 100) * $examTotals['possible'];
             $radarMeanScoresDB[$eachFormID] = percent(
                 $calcWeightedScore,
                 $avgScoreRecord['possible']
             );

         // Standard Scoring    
         } else {
           
             $radarMeanScoresDB[$eachFormID] = percent(
                    $avgScoreRecord['score'], 
                    $avgScoreRecord['possible']
             );
           
         }
         
     }
     
 }

 // Get all form sections
 $sectionIndices = ['form_id', 'section_id'];
 $sections = $db->forms->getFormSections($formIDs, true, $sectionIndices);
 
 // Gather section IDs from the sections data
 $sectionIDs = [];
 foreach ($sections as $form) {
   
   foreach ($form as $sectionID => $data) {
     
       $sectionIDs[] = $sectionID;
       
   }
   
 }
 
 $itemIndices = ['section_id', 'item_id'];
 $items = $db->forms->getSectionItems($sectionIDs, true, $itemIndices);
 
 // Examiner weighting values
 $stationIDs = array_keys($stations);
 $examinerWeightings = $db->exams->getExaminerWeightings($stationIDs);
 
 // Get feedback records
 $feedbackRecords = $db->feedback->getStudentFeedbackRecords($examID);
 $recordCount = mysqli_num_rows($feedbackRecords);
 
 // Walk through each student feedback row
 while ($eachRecord = $db->fetch_row($feedbackRecords)) {
     
     $studentID = $eachRecord['student_id'];
 
     // Initial variables per student
     $stationArray = $stationGraphLabels = 
     $studentStationScores = $examSummary = [];
     $grsFailCount = 0;
     $grsFail = false;
 
     // File_name stations [JPG]
     $fileNameRadar = Path::join($feedbackFolder, $studentID . '_' . $examID . '.png');
     
     // File_name competencies [JPG]
     $fileNameCompetenciesRadar = Path::join($feedbackFolder, $studentID . '_' . $examID . '_competencies.png');    
     
     /* Define the name of the file in which this particular piece of feedback 
      * is being stored and if it already exists, delete it.
      */
     $feedbackFile = Path::join($feedbackFolder, $studentID . '_' . $examID . '.pdf');

     if (file_exists($feedbackFile)) {
       
         if (!unlink($feedbackFile)) {
           
             error_log(__FILE__ . ": Failed to delete existing feedback file $feedbackFile");
             exit;
             
         }
         
     }
 
     if (in_array($studentID, $includeStudents)) {
         
         // Configure PDF Document and pass in main and inner titles
         $pdf = configurePDFDoc(
             $examDB['exam_name'] . ' - ' . $examDB['exam_description'], 
             $eachRecord['surname'] . ', ' . $eachRecord['forename']
         );

         // Determine outcomes
         $outcomesService = new \OMIS\Outcomes\OutcomesService($db);
         $examOutcome = $outcomesService->getOutcomesForStudentAndExam($studentID, $examID);

         // Walk Through Station Results
         $studentResultRecords = $db->results->getResultsByExam(
                 $studentID, $examID
         );
        
         // Group student results by station number and station_id
         $studentResults = [];
         while ($stationResult = $db->fetch_row($studentResultRecords)) {
           
             $groupIndex = $stationResult['station_number'].$stationResult['station_id'];
             $studentResults[$groupIndex][] = $stationResult;
             
         }
         
          /**
          * Gather weighted score data
          * per student
          */
         if ($applyStationWeightings) {

              $studentExamPossible = $examOutcome['adjustedPossible'];
              
         }
         
         // Loop through student results
         foreach ($studentResults as $results) {
             
            /**
             * If the station is not to be displayed then
             * continue to next result record
             */
             $stationNum = $results[0]['station_number'];
             
             if (!in_array($stationNum, $stationNumbers)) {
                continue;
             }

             $sessionID = $results[0]['session_id'];

             $studentOutcome = $outcomesService->getOutcomesForStudentAndStation($studentID, $sessionID, $stationNum);
             $stationOutcome = $studentOutcome['stationScores']['combined_by'][$stationNum];
                                                                        
             // Accumulated score, possible and multi result count
             $accScore = $studentOutcome['raw']['score'];
             $accPossible = $studentOutcome['raw']['possible'];

             // Accumulated weighted score
             $accWeightedScore = $studentOutcome['weighted']['score'];
             $accWeightedPossible = $studentOutcome['weighted']['possible'];
 
             // Number of multiple results
             $numMultiResults = count($results);            
 
             // Get form ID from first student result
             $formID = $results[0]['form_id'];                
 
             // Get station ID for first student result
             $stationID = $results[0]['station_id'];
 
             // Examiner Scoring Weight
             $weightings = isset($examinerWeightings[$stationID]) ? $examinerWeightings[$stationID] : [];                   
 
             // Form name
             $formName = $forms[$formID]['form_name'];
 
             // Pass percentage
             $passPercentage = $stations[$stationID]['pass_value'];
 
             // Render station header
             renderStationHeader($pdf, $stationNum, $formName);
             
             // Render station notes / instructions
             if ($stationNotes && strlen($forms[$formID]['form_description']) > 0) {
               
                renderStationNotes(
                    $pdf, $forms[$formID]['form_description'], $notesTitle
                );
                
             }
             
             // Get number of observers
             $weightingCounts = array_count_values($weightings);
             if (isset($weightingCounts[\OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER])) {
               
                $observerCount = $weightingCounts[\OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER];
                
             } else {
               
                $observerCount = 0;
                
             }
 
             // Render form sections and items
             foreach ($sections[$formID] as $sectionID => $section) {

                // It's possible that there are no graded results for this student in this section, so check it and
                // set the value to null if there aren't
                $sectionOutcome = array_key_exists($sectionID, ($stationOutcome['section_id'] ?: [])) ?
                    $stationOutcome['section_id'][$sectionID] : null;
               
                $isScale = false;
                $sectionMarks = $sectionFeedback = $itemsFeedback = [];
                $pdf->SetFont('times', 'b', 8);
 
                // Deduct observer score title columns if we're requested not to include them
                $numScoreColumns = (!$observerRatings) ? ($numMultiResults - $observerCount) : $numMultiResults;

                $renderItemWeightings = $itemWeighting && $db->results->sectionHasWeightedItems($sectionID);
 
                // Render section header
                if ($sectionTitles) renderSectionHeader(
                    $pdf, $section['section_text'], $numScoreColumns,
                    $multiexaminerRatings, $scoreValues, $renderItemWeightings, $sectionOutcome === null
                );
 
                /**
                 * Gather all results for this section, there can be multiple
                 * results per item in the case of multiple examiners
                 */
                $resultIndex = 0;
                foreach ($results as &$result) {
                  
                    $resultIndex++;
 
                    // Result ID
                    $resultID = $result['result_id'];
 
                    // Examiner ID
                    $examinerID = $result['examiner_id'];                       
 
                    if (in_array($examinerID, array_keys($weightings))) {
                      
                       /* The examiner is "known" to the system, so we should be able to
                        * get that examiner's pre-defined weighting.
                        */
                        $examinerWeighting = (int) $weightings[$examinerID];
                        
                    } else {
                      
                        // If we don't know what weighting to assign to this examiner for any 
                        // reason, then go with a default weighting.
                        $examinerWeighting = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;
                        
                    }
 
                    if (!$observerRatings && $examinerWeighting == \OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER) {
                      
                          continue;
                          
                    }

                    // Section Feedback
                    $sectionFeedback[] = $db->feedback->getSectionFeedback(
                        $sectionID,
                        $resultID,
                        EXCLUDE_INTERNAL_FEEDBACK
                    );


                    // Section item results
                    $sectionItemResults = $db->results->getSectionItemMarks(
                        $resultID,
                        $sectionID
                    );

                    // Item marks and final weightings
                    while ($itemResults = $db->fetch_row($sectionItemResults)) {
 
                       // Item ID
                       $itemID = $itemResults['item_id'];

                        // Items feedback
                       $itemsFeedback[$itemID][] = $db->feedback->getItemFeedback(
                           $itemID,
                           $resultID,
                           EXCLUDE_INTERNAL_FEEDBACK
                       );

                       // Examiner weighting
                       $itemResults['examiner_weighting'] = $examinerWeighting;
 
                       // Section marks
                       $sectionMarks[$sectionID][$itemID]['raw_values'][$resultIndex] = $itemResults;
 
                    }
 
                }
 
                // Render items (if items exist)
                if (isset($items[$sectionID])) {
                  
                    foreach ($items[$sectionID] as $itemID => &$item) {

                       $itemOutcome = $sectionOutcome ? $sectionOutcome['item_id'][$itemID] : null;
                      
                       // Defaults
                       $enableText = true;
                       $enableDescriptors = true;
                       $enableValues = true;
                       $isScale = false;

                       // Item marks
                       $itemMarks = isset($sectionMarks[$sectionID][$itemID]) ? 
                       $sectionMarks[$sectionID][$itemID] : null;

                       // Item type (radio, slider, textfield input)
                       $itemType = $item['type'];

                       $itemWeight = $renderItemWeightings ? (float)$item['weighted_value'] : 1.0;

                       // Is item graded
                       $isItemGraded = true;

                       // Output item and answer rows
                       $pdf->SetFont('times', '', 8);                      

                       // Switch for item type
                       switch ($itemType) {

                          // Score item cases  
                          case 'radio':
                          case 'text':
                          case 'slider':

                             // Flag values
                             $enableText = $scoreText;
                             $enableDescriptors = $scoreDescriptors;
                             $enableValues = $scoreValues;

                            break;

                          // Non-score item
                          case 'checkbox':
                          case 'radiotext':
                             $isItemGraded = false;
                             if ($item['grs'] == 1) {

                                 $enableText = $grsText;
                                 $enableDescriptors = $grsDescriptors;
                                 $enableValues = false;
                                 $isScale = true;

                             } else {

                                 $enableText = $nonscoreText;
                                 $enableDescriptors = $nonscoreDescriptors;
                                 $enableValues = $nonscoreValues;

                             }
                             break;

                          default:
                         /* @TODO Figure out if there's something that
                          * should be done here - throw an error perhaps?
                          */

                      }

                      // Render section item
                      if ($enableText || $enableDescriptors || $enableValues) {
                        
                         // Self assessment question in place of item text
                         selfAssessmentQuestion(
                                $item,
                                $section['self_assessment'],
                                $examID,
                                $studentID
                         );
                        
                         // Render the section item
                         renderSectionItem(
                                 $pdf,
                                 $item,
                                 $itemMarks,
                                 $enableText,
                                 $enableDescriptors,
                                 $enableValues,
                                 $isScale,
                                 $isItemGraded,
                                 $multiexaminerRatings,
                                 $scoreValues,
                                 $itemOutcome,
                                 $renderItemWeightings
                         );

                         // Item Comment
                         if ($comments) {

                            renderComment($pdf, $itemsFeedback[$itemID]);

                         }

                     }

                   }
                 
               }
 
               // Section Comment
               if ($comments) {
                 
                  renderComment($pdf, $sectionFeedback, $isScale);
                  
               }
 
             }
 
             $stationScorePercentage = percent($accScore, $accPossible);
             $stationScorePercentageWeighted = percent($accWeightedScore, $accWeightedPossible);

             $summaryStationScore = $accScore;
             $summaryStationPercent = $stationScorePercentage;

             $summaryStationScoreWeighted = $accWeightedScore;
             $summaryStationPercentWeighted = $stationScorePercentageWeighted;
 
             $stationGraphLabels[] = wordwrap($formName, 12);
 
             /**
              *  Global Rating Scale (Clear Fails Only)
              */
             if (isset($grsData[$resultID])) {
               
                 if (is_numeric($maxGrsFailRule) && $grsData[$resultID]["fail"] == 1 &&
                     $grsData[$resultID]["borderline"] == 0) {
                   
                     $grsFailCount++;
                     $grsFail = ($grsFailCount > $maxGrsFailRule);
                     
                 }
 
                 $summaryGrsValue = $grsData[$resultID]["descriptor"];
                 
             } else {
               
                 $summaryGrsValue = " - ";
                 
             }
             
             /**
              * Apply Weighted Scores
              */
             if ($applyStationWeightings && $examHasWeightedStations) {

                 $calcWeightedScore = ((($accWeightedScore / $accWeightedPossible) * $results[0]['weighting']) / 100) * $studentExamPossible;
                 $weightedScorePercent = percent($calcWeightedScore, $accWeightedPossible);

                 $stationWeightedScore = roundAndFormat(
                     $calcWeightedScore, 2
                 );

                 $summaryStationScore = $calcWeightedScore;
                 $summaryStationPercent = $weightedScorePercent;
                 $studentStationScores[$formID] = $weightedScorePercent;

             } else if ($applyStationWeightings) {
                 $calcWeightedScore = $accWeightedScore;
                 $weightedScorePercent = percent($calcWeightedScore, $accWeightedPossible);

                 $stationWeightedScore = roundAndFormat(
                     $calcWeightedScore, 2
                 );

                 $summaryStationScore = $calcWeightedScore;
                 $summaryStationPercent = $weightedScorePercent;
                 $studentStationScores[$formID] = $weightedScorePercent;
               
             } else {
             
                $stationWeightedScore = $weightedScorePercent = '';
                $studentStationScores[$formID] = $stationScorePercentage;
               
             }
             
             // Add station summary to exam summary array
             if ($showExamSummary || $templateDB['pdf_instructions_enabled'] == "fail") {
               
                 $examSummary[] = [
                     'station_number' => $stationNum,
                     'form_name' => $formName,
                     'score' => $summaryStationScore,
                     'station_pass' => $passPercentage,
                     'station_possible' => $accPossible,
                     'station_grs_value' => $summaryGrsValue,
                     'percentage_score' => $summaryStationPercent
                 ];
                
             }
 
             // Render station bottom and scores
             renderStationBottom($pdf); 
                          
             renderStationScore(
                 $pdf, 
                 [
                   "total" => $accScore,
                   "possible" => $accPossible,
                   "percent" => $stationScorePercentage,
                 ],
                 [
                   "showTotal" => $finalscoreRaw, 
                   "showPercent" => $finalscorePercent,
                 ],
                 ((!$multiExaminerAvgRule && $numMultiResults > 1) ? "Aggregate Score" : "")
             );
            
             renderStationScore(
                 $pdf, 
                 [
                   "total" => $stationWeightedScore,
                   "possible" => $accWeightedPossible,
                   "percent" => $weightedScorePercent,
                 ],
                 [
                   "showTotal" => $finalscoreWeighted,
                   "showPercent" => $finalscoreWeightedPercent,
                 ],
                 (($finalscoreRaw || $finalscorePercent) ? gettext('Weighted') . " Final Score" : "")
             );
             
         }
 
         /********************* Summary Table and Radar Plots ************************/
         
         // Get current page number
         $formsLastPageNum = $pdf->getNumPages();
         
         if (count($studentStationScores) > 0) {
             
             // Competency results per student
             $competencyResults = $db->competencies->getExamCompetencyResults(
                     $stationIDs, 
                     $studentID
             );
 
             // Calculate percentage score
             foreach ($competencyResults as &$competencyResult) {
               
                 $competencyResult['percent'] = percent($competencyResult['score'], $competencyResult['possible']);
                 $competencyResult['name'] .= ' ('.$competencyResult['percent'] .'%)';
                 
             }            
             
             // Can display radar plot and there are competency results
             $displayCompetenciesRadar = (sizeof($competencyResults) > 0 && $radarplotCompetency);
             
             // Add graph and table to new page no matter what.
             if ($displayCompetenciesRadar || $radarplotComparison || $showExamSummary) {
               
                 $pdf->AddPage();
                 
             }
 
             // Render Exam Summary Table
             if ($showExamSummary) {

                 renderStudentExamSummary(
                     $pdf, $examSummary, $rules, $summaryScores,
                     $summaryOutcomes, $summaryOverallScore, 
                     $summaryOverallOutcome, $stationOveralls,
                     $summaryGrs, $grsFail, $examOutcome, $applyStationWeightings || $itemWeighting
                 );
                 
             }
 
             // Render Stations Radar Plot
             if ($radarplotComparison) {
                 
                 include __DIR__ . '/../graphs/radar_feedback.php';
 
                 $pdf->Image(
                    $fileNameRadar, 15, $pdf->GetY(), 140, 115, 'PNG', '', 'N',
                    false, 150, 'L', false, false, 0, true, false, true
                 );
                 
             }
             
             // Render Competency Radar Plot
             if ($displayCompetenciesRadar) {
                 
                 include __DIR__ . '/../graphs/radar_student_competency.php';
 
                 $pdf->Image(
                    $fileNameCompetenciesRadar, 15, $pdf->GetY(), 140, 115, 'PNG', '', 'N',
                    false, 150, 'L', false, false, 0, true, false, true
                 );
                 
             }        
             
             /*
              *  Move the last page containing the summary table and 
              *  the radar plots to the beginning of the document (if any)
              */
             if ($displayCompetenciesRadar || $radarplotComparison || $showExamSummary) {
                
                $newPageIndex = 1;
                $currentPageNum = $pdf->getNumPages();
                for ($pageNum=$formsLastPageNum+1; $pageNum<=$currentPageNum; $pageNum++) {
                  
                   $pdf->movePage($pageNum, $newPageIndex);
                   $newPageIndex++;
                   
                }
                 
             }
             
         }

         // Render PASS/Fail Instructions if meets criteria
         $receivesInstructions = renderPDFInstructions($pdf, $templateDB, $examSummary, $rules);
        
         $dateCreated = date("Y-m-d H:i:s");

         $db->feedback->setFeedbackCreated(
             $studentID, $examID, 
             $templateID, 
             (int) $receivesInstructions,
             $dateCreated, 1
            );

         $pdf->Output($feedbackFile, 'F');

         $returnData[] = [
             'student_id' => $studentID,
             'surname' => $eachRecord['surname'],
             'forename' => $eachRecord['forename'],
             'email' => $eachRecord['email'],
             'file' => $studentID . '_' . $examID,
             'time_created' => $dateCreated,
             'template_name' => $templateDB['template_name'],
             'receives_instructions' => $receivesInstructions
         ];
     }
 
     // Delete Feedback PNG (stations) if it Exists
     if (file_exists($fileNameRadar)) {
       
         unlink($fileNameRadar);
         
     }
     
     // Delete Feedback PNG (competencies) if it exists
     if (file_exists($fileNameCompetenciesRadar)) {
       
         unlink($fileNameCompetenciesRadar);
         
     }

 }
 
 try {
   
     $json = \OMIS\Utilities\JSON::encode($returnData);
     
 } catch (Exception $ex) {
   
     // Something went wrong encoding the data to JSON.
     error_log(__FILE__ . ": JSON Encode failed. Data follows:");
     error_log(print_r($returnData, true));
     /* The only correct thing to do here is return an internal server error
      * message.
      */
     http_response_code(\OMIS\Utilities\HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
     
 }
 
 echo $json;
 