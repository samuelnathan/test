<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * 
 * PDF for examinee results export
 */

use OMIS\Reporting\PDF as OMISPDF;

define('SHOW_MULTI_RESULTS', true);
define('SHOW_SCORE_VALUES', true);
define('SHOW_NONSCORE_VALUES', true);
define('SHOW_OPTION_DESCRIPTORS', true);
define('SHOW_ITEM_TEXT', true);
define('_OMIS', 1);

// Database connection
require_once __DIR__ . '/../extra/essentials.php';

// Page access check / can user access this section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    die("You do not have permission to access this feature");
}

// PDF settings
require_once 'feedback_pdf_settings.php';

/* A chunk of functions have been created to handle key parts of this module and
 * have been hived off into feedback_functions.php to make this thing easier to 
 * manage and easier to read.
 */
require_once 'feedback_functions.php';

// Make sure that the necessary keys are in the request...
$required_keys = ['selected', 'student', 'exam'];
$matching_keys = array_filter($required_keys, function ($value) {
  
    return filter_has_var(\INPUT_GET, $value);
    
});

// Begin validating parameters
if (count($matching_keys) < count($required_keys)) {
  
    die("Could not process request, one or more of the required parameters are missing");
    
}

// Student and exam identifier params
$studentID = filter_input(INPUT_GET, 'student', FILTER_SANITIZE_STRING);
$examID = filter_input(INPUT_GET, 'exam', FILTER_SANITIZE_NUMBER_INT);
$includeObserversParam = filter_input(INPUT_GET, 'observers', FILTER_SANITIZE_NUMBER_INT);
$includeObservers = (!is_null($includeObserversParam) && $includeObserversParam  == 1);

// Stations selected parameter
$stationsSelectedParam = filter_input(INPUT_GET, 'selected');
$stationsSelected = explode(",", $stationsSelectedParam);

// If the either the student and exam identifiers don't exist then abort
if (!$db->students->doesStudentExist($studentID)) {
  
    die("Student no longer exists in the system");
    
}

if (!$db->exams->doesExamExist($examID)) {
  
    die("Exam no longer exists in the system");
    
}

if (is_null($stationsSelected) || count($stationsSelected) == 0) {
  
   die("No stations selected");
   
}

// Get required records
$examRecord = $db->exams->getExam($examID);
$sessionRecord = $db->fetch_row($db->exams->getStudentSession($studentID, $examID));
$studentRecord = $db->students->getStudent($studentID);
$rules = $db->examRules->getRules($examID);

// Main and inner titles
$mainTitle = $examRecord['exam_name'] . ' ' 
           . formatDate($sessionRecord['session_date']) . ' ' 
           . $sessionRecord['session_description'];
$innerTitle = $studentRecord['surname'] . ', '
            . $studentRecord['forename'];

#######################[PREPARE PDF]#############################

// Create New PDF
$pdf = new OMISPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Author & Keywords
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Qpercom LTD');
$pdf->SetKeywords('Qpercom LTD, ' . ucwords(gettext('examination')));

// Document Title & Subject
$pdf->SetTitle(ucwords(gettext('examination')) . ' Results');
$pdf->SetSubject($mainTitle);
$pdf->setHeaderFont(['times', '', 9]);
$pdf->setFooterFont(['times', '', 9]);
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(2, 14, 2);
$pdf->setCellHeightRatio(1.25);
$pdf->SetHeaderMargin(1);
$pdf->setFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setPrintFooter(true);
$pdf->SetAutoPageBreak(true, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($config->tcpdf_environment['language']);
$pdf->setFontSubsetting(false);
$pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
$pdf->setHtmlVSpace($tagvs);

// Add Title
$pdf->SetHeaderData($pdf_logo, $logo_width_mm, $mainTitle, $innerTitle);

// Add Page
$pdf->AddPage();

// Get the students results for this exam
$studentResultRecords = $db->results->getResultsByExam(
    $studentID, 
    $examID
);

/*
 *  Group student results by station number and station_id
 *  and gather unique station identifiers
 */
$studentResults = [];
$stationIDs = [];

while ($stationResult = $db->fetch_row($studentResultRecords)) {

    $groupIndex = $stationResult['station_number'].$stationResult['station_id'];
    $studentResults[$groupIndex][] = $stationResult;
    
    if (!in_array($stationResult['station_id'], $stationIDs, true)) {
      
        array_push($stationIDs, $stationResult['station_id']);
        
    }
    
}

// Get examiner weightings by station identifiers
$examinerWeightings = $db->exams->getExaminerWeightings($stationIDs);

#################[Render results forms]####################

// Loop through student results
foreach ($studentResults as $results) {

   /**
    * If the station is not to be generated then
    * continue to next result record
    */
    $stationNum = $results[0]['station_number'];
    if (!in_array($stationNum, $stationsSelected)) {
      continue; 
    }
     
    // Accumulated score, possible and multi result count
    $accScore = 0;
    $accPossible = 0;
 
    // Get station ID for first student result
    $stationID = $results[0]['station_id'];

    // Examiner Scoring Weight
    $weightings = isset($examinerWeightings[$stationID]) ? $examinerWeightings[$stationID] : [];               

    // Number of multiple results
    $numMultiResults = count($results);

    // Get form ID from first student result
    $formID = $results[0]['form_id'];    
        
    // Get form name from first student result
    $formName = $results[0]['form_name'];  

    // Pass percentage
    $passPercentage = $results[0]['pass_value'];

    // Render station header
    renderStationHeader($pdf, $stationNum, $formName);

    // Get number of observers
    $weightingCounts = array_count_values($weightings);
    if (isset($weightingCounts[\OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER])) {
      
       $observerCount = $weightingCounts[\OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER];
       
    } else {
      
       $observerCount = 0;
       
    }

    // Get form sections
    $sections = $db->forms->getFormSections(
            [$formID],
            RETURN_RESULTS_AS_ARRAY,
            ['section_id']
    );

    // Render form sections and items
    foreach ($sections as $sectionID => $section) {
      
       $isScaleItem = false;
       $sectionMarks = $sectionFeedback = $itemsFeedback = [];
       $sectionTitle = $section['section_text'];  
       $pdf->SetFont('times', 'b', 8);

       // Deduct observer score title columns if we're requested not to include them
       $numScoreColumns = (!$includeObservers) ? ($numMultiResults - $observerCount) : $numMultiResults;

       // Render section header
       renderSectionHeader($pdf, $sectionTitle, $numScoreColumns, SHOW_MULTI_RESULTS, SHOW_SCORE_VALUES);

       /**
        * Gather all results for this section, there can be multiple
        * results per item in the case of multiple examiners
        */
       $resultIndex = 0; 
       foreach ($results as &$result) {
         
           $resultIndex++;

           // Result ID
           $resultID = $result['result_id'];               

           // Examiner ID
           $examinerID = $result['examiner_id'];                       

           if (in_array($examinerID, array_keys($weightings))) {
             
              /* The examiner is "known" to the system, so we should be able to
               * get that examiner's pre-defined weighting.
               */
               $examinerWeighting = (int) $weightings[$examinerID];
               
           } else {
             
               // If we don't know what weighting to assign to this examiner for any 
               // reason, then go with a default weighting.
               $examinerWeighting = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;
               
           }

           if (!$includeObservers && $examinerWeighting == \OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER) {
             
                 continue;
                 
           }                             

           // Section Feedback
           $sectionFeedback[] = $db->feedback->getSectionFeedback(
               $sectionID,
               $resultID
           );

           // Section item results
           $sectionItemResults = $db->results->getSectionItemMarks(
               $resultID,
               $sectionID
           );

           // Item marks and final weightings
           while ($itemResults = $db->fetch_row($sectionItemResults)) {

              // Item ID
              $itemID = $itemResults['item_id'];

              // Items feedback
              $itemsFeedback[$itemID][] = $db->feedback->getItemFeedback(
                   $itemID,
                   $resultID
              );

              // Examiner weighting
              $itemResults['examiner_weighting'] = $examinerWeighting;

              // Section marks
              $sectionMarks[$sectionID][$itemID]['raw_values'][$resultIndex] = $itemResults;

           }

       }
      

       // Get section items
       $sectionItems = $db->forms->getSectionItems(
               [$sectionID],
               RETURN_RESULTS_AS_ARRAY,
               ['item_id']
       );

       // Render items
       foreach ($sectionItems as $itemID => $item) {

          // Defaults
          $enableValues = SHOW_SCORE_VALUES;
          $isScaleItem = false;

          // Is item graded
          $isItemGraded = true;              

          // Item marks
          $itemMarks = isset($sectionMarks[$sectionID][$itemID]) ? 
          $sectionMarks[$sectionID][$itemID] : null;

          // Output item and answer rows
          $pdf->SetFont('times', '', 8);                   

          // Switch for item type (radio, slider, textfield input)
          switch ($item['type']) {

             // Score item cases  
             case 'radio':
             case 'text':
             case 'slider':

                // Flag values
                $enableValues = SHOW_SCORE_VALUES;

                // Weighted scores
                if ($numMultiResults > 1) {

                     // Calculate item weighted score
                     $itemScore = calculateItemWeightedScore(
                             $itemMarks['raw_values'],
                             (bool) $rules['multi_examiner_results_averaged']
                     );
                     
                     $itemMarks['item_weighted_score'] = $itemScore;

                } else {

                     // Item raw score
                     $itemScore = empty($itemMarks['raw_values']) ? "" : 
                            reset($itemMarks['raw_values'])['option_value'];

                }

                // Item possible
                $itemPossible = empty($itemMarks['raw_values']) ? "" :
                        reset($itemMarks['raw_values'])['item_possible'];

                // Accumulated scores and possible scores
                $accScore += $itemScore;

                // Self assessments (only 1 possible)
                if ($section['self_assessment']) {
                  
                   $accPossible += $itemPossible;

                } else {
                  
                   $accPossible += (!$rules['multi_examiner_results_averaged']) ? 
                             ($numMultiResults * $itemPossible) : $itemPossible; 

                }
                

               break;

             // Non-score item
             case 'checkbox':
             case 'radiotext':
                $isItemGraded = false;
                if ($item['grs'] == 1) {
                    $enableValues = false;
                    $isScaleItem = true;
                } else {
                    $enableValues = SHOW_NONSCORE_VALUES;
                }
                break;

             default:
               
         }
         
         // Self assessment question in place of item text
         selfAssessmentQuestion(
            $item,
            $section['self_assessment'],
            $examID,
            $studentID
         );
         
         // Render section item
         renderSectionItem(
               $pdf,
               $item,
               $itemMarks,
               SHOW_ITEM_TEXT,
               SHOW_OPTION_DESCRIPTORS,
               $enableValues,
               $isScaleItem,
               $isItemGraded,
               SHOW_MULTI_RESULTS,
               SHOW_SCORE_VALUES
         );

         renderComment($pdf, $itemsFeedback[$itemID]);

       }

       // Competence feedback (comments)
       renderComment($pdf, $sectionFeedback, $isScaleItem);

    }

    // Draw blank cell with a border as a separator.
    $pdf->Multicell($pdf->getAvailableWidth(), 0, '', 'T', 'C',
                    false, 1, '', '', true, 0, false, false, 0,
                    'M', true);

    /*************************[SCORE CELL]************************/

    $stationScorePercentage = percent($accScore, $accPossible);
    $message = (!$rules['multi_examiner_results_averaged'] && $numMultiResults > 1) ? "Aggregate Score" : "";
    
    renderStationScore(
          $pdf,
          [
              "total" => $accScore,
              "possible" => $accPossible,
              "percent" => $stationScorePercentage
          ],
          [
              "showTotal" => true,
              "showPercent" => true,
          ],
         $message
    );

}

// Close and output PDF document
$pdf->Output($studentRecord['student_id'] . '.pdf', 'I');
