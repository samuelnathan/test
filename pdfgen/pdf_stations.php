<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

/* Defined to tell any included files that they have been included rather than
 * being invoked directly from the browser. Need to declare it anywhere that a
 * file is directly invoked by the user (via AJAX).
 */
define('_OMIS', 1);

/* (DW) Rather than relying on magic numbers, let's give them names so that
 * we're a little clearer on what's going on here.
 */
define("ACTION_TEMPLATE_FORMS", 1);
define("ACTION_STUDENT_FORMS", 2);

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

use OMIS\Reporting\PDF as PDF;

require_once __DIR__ . '/../extra/essentials.php';
//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once 'pdf_settings.php';
require_once 'station_functions.php';

#####################[PREPARE INFORMATION]######################
if (!isset($_GET['session']) || !$db->exams->doesSessionExist($_GET['session'])) {
    return false;
}

// Stations or Station to Output
$station_id = (isset($_GET['sta']) && $db->exams->doesStationExist($_GET['sta'])) ? trim($_GET['sta']) : '';
$action = filter_input(INPUT_GET, 'act', FILTER_SANITIZE_NUMBER_INT);
if (is_null($action)) {
    // Set a default behaviour (just in case)...
    $action = ACTION_TEMPLATE_FORMS;
}

// Show Marks True or False
$showMarks = (isset($_GET['mar'])) ? trim($_GET['mar']) : 0; 

// Show Exam Title True or False
$showExamTitle = (isset($_GET['examtitle'])) ? trim($_GET['examtitle']) : 0;

// Show Feedback text area
$showFeedback = (isset($_GET['fa'])) ? trim($_GET['fa']) : 0;
$feedbackLines = (isset($_GET['fh'])) ? trim($_GET['fh']) : 0;

// Calculate height of the feedback text area
$feedbackHeight = $showFeedback ? $feedbackLines * 5 : 0;

$session_id = filter_input(INPUT_GET, 'session', FILTER_SANITIZE_NUMBER_INT);

// Exam title
if ($showExamTitle == 1) { 
    $sessionDB = $db->exams->getSession($session_id);
    $sessionInformation = ', ' . formatDate($sessionDB['session_date']) . ' ' . $sessionDB['session_description'];
} else {
    $sessionInformation = '';
}

if (is_null($station_id) || strlen($station_id) == 0) {
    // Get all the stations for this session if no station has been specified.
    $stations = $db->exams->getStations([$session_id]);
} else {
    $stations = $db->exams->getStation($station_id, false);
}

if ($action == ACTION_TEMPLATE_FORMS) {
    $students = ['no_prefill' => []];
} else {
    $students = $db->exams->getStudents(null, $session_id);
}

// (DW) This is a bit of a hack, allow a few (3) seconds per station for the
// script to generate its output. This could probably do with time profiling to
// determine typical execution time.
$student_count = count($students);
$station_count = mysqli_num_rows($stations);
PDF::guessTimeLimit($station_count * $student_count, 2);
PDF::guessMemoryRequirements($station_count * $student_count, 200);

#######################[PREPARE PDF]#############################
// Document Title & Subject
$pdf->SetTitle(ucwords(gettext('form')) . " Print Outs");
$pdf->SetSubject($sessionInformation);
$pdf->SetMargins(2, 13, 2);

#################[Go Through Each Station]####################
while ($eachStation = $db->fetch_row($stations)) {
    // Grab Data from the Database
    $sections = $db->forms->getFormSections([$eachStation['form_id']], true);
    $items = $db->forms->getFormItems($eachStation['form_id'], true, true);
    $answers = $db->forms->getFormItemOptions($eachStation['form_id']);
    $sectionAnswerTitles = [];
    $pdf->SetFont('times', 'b', 9);

    ##############[Find Items With Most Answering Options]###############
    foreach ($answers as $sectionID => &$rows) {
        setA(
            $sectionAnswerTitles,
            $sectionID,
            [
                'item_id' => 0,
                'answer_count' => 0,
                'predicted_height' => 0,
                'titles' => []
            ]
        );

        // Go Through ITEMS
        $rowCount = 1;
        foreach ($rows as $itemID => &$answerRows) {
            $rowCount = count($answerRows);
            $hasMoreItems = ($rowCount > $sectionAnswerTitles[$sectionID]['answer_count']);

            // Has More Items
            if ($hasMoreItems) {
                $sectionAnswerTitles[$sectionID]['item_id'] = $itemID; // Item Instance
                $sectionAnswerTitles[$sectionID]['answer_count'] = $rowCount; // Answer Count
                $sectionAnswerTitles[$sectionID]['titles'] = $answerRows; // Add Titles
            }

            // Go Through Rows
            foreach ($answerRows as &$eachAnswer) {
                // Has More Items
                if ($hasMoreItems) {
                    $height = $pdf->getStringHeight(
                        (WIDTH_FULL_PAGE - WIDTH_TITLE_COLUMN) / $rowCount,
                        $eachAnswer['descriptor'],
                        true,
                        false,
                        0.6,
                        'RLB'
                    ) + 1;

                    // Find Maximum Height
                    if ($height > $sectionAnswerTitles[$sectionID]['predicted_height']) {
                        $sectionAnswerTitles[$sectionID]['predicted_height'] = $height;
                    }
                }

                // Clean Text For Comparison
                $eachAnswer['descriptor'] = stripTitle($eachAnswer['descriptor']);

            }
        }
    }

    #####[Cycle Through Student Data]#####
    foreach ($students as $key => $student) {
        $totalPossible = 0;

        if ($key === 'no_prefill') {
            list($innerTitle, $candidate) = ['', '       Candidate:'];
        } else {
            list($innerTitle, $candidate) = [
                $student['surname'] . ', ' . $student['forename'] . ' ' . $student['student_id'],
                ''
            ];
        }

        $formName = ucfirst($eachStation['form_name']);
        $mainTitle = "Station " . $eachStation['station_number'] . " $formName$sessionInformation$candidate";
        $pdf->SetHeaderData($pdf_logo, $logo_width_mm, $mainTitle, $innerTitle);
        $pdf->setHeaderTemplateAutoreset(true);
        $pdf->AddPage(); // Add a Page
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(0.6, 0.6, 0.6, 0.6);

        // Competency Title
        foreach ($sections as $section) {
            
            /**
             *  If no items found then simply just render 
             *  the title and continue
             */
            if (!isset($items[$section['section_id']]) || count($items[$section['section_id']]) == 0) {
               renderSectionHeader($pdf, $section, [], []);
               continue;
            }
            
            // Strip any newlines or whitespace from the section titles.
            $justSectionTitles = array_map(
                'stripTitle',
                array_column($sectionAnswerTitles[$section['section_id']]['titles'], 'descriptor')
            );

            // Are Answer Titles Numeric?
            $answerTitlesNumeric = is_numeric(str_replace('.', '', implode('', $justSectionTitles)));


            $itemIndex = 0;
            foreach ($items[$section['section_id']] as $item) {

                $itemIndex++;

                $trueHighValue = (float)max($item['lowest_value'], $item['highest_value']);
                $totalPossible += !in_array($item['type'], ['radiotext', 'checkbox']) ? $trueHighValue : 0;

                /**
                 * [DC 08/12/2015] Temp (quick) fix for locating the share image directory. 
                 * Need to have another look at this
                 */
                $searchRegEx = "(src=\"storage/app/share\/)";
                $replaceString = 'src="' . K_PATH_IMAGES . 'storage/app/share/';
                $itemDescription = preg_replace(
                    $searchRegEx,
                    $replaceString,
                    trim($item['text'])
                );                
                
                // Self assessment question in place of item text
                if ($section['self_assessment']) {

                    $question = $db->selfAssessments->getQuestion(
                        $db->exams->getSessionExamID($session_id), 
                        $item['item_order']
                    );
                    
                    if (!empty($question)) $itemDescription = "<p>" . $question['question'] . "</p>";
                
                }          
                
                $currentPage = $pdf->getPage();
                
                /* Get the height of the item description text as it would
                 * be when injected into a box of the appropriate width...
                 */
                $pdf->SetFont('times', '', 9);
                $chunkHeight = PDF::calculateHtmlHeight($pdf, WIDTH_TITLE_COLUMN, $itemDescription);
                if ($itemIndex == 1) {
                    /* If this is the first item in the list, include the height
                     * of the section header as well. To get this we'll need to
                     * create a copy of the header on a new page and see how 
                     * tall it gets.
                     */
                    $pdf2 = clone $pdf;
                    $pdf2->addPage();
                    $pdf2_startY = $pdf2->getY();
                    renderSectionHeader(
                        $pdf2,
                        $section,
                        $items[$section['section_id']],
                        $sectionAnswerTitles[$section['section_id']]
                    );
                    $headerRowHeight = $pdf2->getY() - $pdf2_startY;
                    unset($pdf2);
                } else {
                    $headerRowHeight = 0;
                }

                /* If injecting this row would cause it to flow over the page,
                 * then inject a new page first...
                 */
                if (($pdf->getY() + $chunkHeight + $headerRowHeight + 6 + ($section['section_feedback'] ? $feedbackHeight : 0)) > ($pdf->getPageHeight() - $pdf->getFooterMargin())) {
                    $pdf->addPage();
                    $newPage = true;
                }

                /* If we're at the beginning of a section (or at the top of a
                 * page), inject the header now.
                 */
                if (($itemIndex == 1) || (isset($newPage) && $newPage)) {
                    /* Render the section header and get a list of "cleaned"
                     * section Titles.
                     */
                    renderSectionHeader(
                        $pdf,
                        $section,
                        $items[$section['section_id']],
                        $sectionAnswerTitles[$section['section_id']],
                        ($itemIndex > 1)
                    );
                    /* This is to make sure that when a new page is loaded and
                     * the headings are printed at the top of the page, 
                     */
                    if (isset($newPage)) {
                        unset ($newPage);
                    }
                }

                $rowY = $pdf->getY();
                
                $pdf->writeHTMLCellAutoWrap(WIDTH_TITLE_COLUMN, $chunkHeight, '', $rowY, $itemDescription, 'LB', 0, false, true, 'L', true);

                if (in_array($item['type'], ['radio', 'radiotext'])) {
                    // Answer Options
                    $totalTitleCount = count($sectionAnswerTitles[$section['section_id']]['titles']);
                    $answerCount = 0;

                    $columnWidth = (WIDTH_FULL_PAGE - WIDTH_TITLE_COLUMN) / (count($justSectionTitles));
                    // Loop Through Competence titles
                    foreach ($justSectionTitles as $title) {
                        $toInsert = false;
                        $insertValue = "";
                        $arrayCount = 0;

                        // Answer Titles are not Numeric
                        if (!$answerTitlesNumeric) {
                            // Loop Through Answers and If Found Then Output
                            foreach ($answers[$section['section_id']][$item['item_id']] as $answer) {
                                if ($title == $answer['descriptor']) {
                                    $insertValue = $answer['option_value'];
                                    array_splice(
                                        $answers[$section['section_id']][$item['item_id']],
                                        $arrayCount,
                                        1
                                    );
                                    $toInsert = true;
                                    break;
                                }

                                $arrayCount++;
                            }
                        } else {
                            $toInsert = false;
                        }

                        // If Not Found Print Out Next Answer
                        if ($toInsert == false && count($answers[$section['section_id']][$item['item_id']]) > 0) {
                            // Get First Answer
                            $firstAnswer = reset($answers[$section['section_id']][$item['item_id']]);
                            // Search ahead
                            $isFound = array_search($firstAnswer['descriptor'], $justSectionTitles);

                            // If not in remaining Titles just print out
                            if ($isFound === false || !($isFound > $answerCount)) {
                                $shifted = array_shift($answers[$section['section_id']][$item['item_id']]);
                                $insertValue = $shifted['option_value'];
                            }
                        }

                        $answerOptionValue = ($showMarks == 0 || $item['grs'] == 1) ? '' : $insertValue;

                        /* There's probably an easier way to do this, possibly
                         * by combining this with the above statement?
                         */
                        if (($item['grs'] == 0) && (trim($insertValue) == "")) {
                            $answerOptionValue = "- NA -";
                        }

                        $newLine = ($totalTitleCount == ++$answerCount) ? 1 : 0;

                        $answerBorder = ($pdf->getPage() > $currentPage) ? 'TRLB' : 'RLB';

                        $pdf->Multicell($columnWidth, $chunkHeight, $answerOptionValue, $answerBorder, 'C', false, $newLine, '', '', true, 0, false, true, $chunkHeight, 'M', true);
                    }
                    
                } else {
                    if ($showMarks == 0) {
                        $answerOption = '';
                    } else {
                        $answerOption = ((float)$item['lowest_value']) . ' - ' . ((float)$item['highest_value']);
                    }

                    $pdf->Multicell(
                        (WIDTH_FULL_PAGE - WIDTH_TITLE_COLUMN), $chunkHeight, $answerOption, 'RLB', 'C', false, 1, '', '', true, 0, false, true, $chunkHeight, 'M', true
                    );
                }

            }

            // Add Feedback area after the section if it's enabled and the user selected it
            if ($section['section_feedback'] && $showFeedback) {
                $pdf->MultiCell(WIDTH_FULL_PAGE, $feedbackHeight, 'Feedback..' . ($section['feedback_required'] == 1 ? ' (Compulsory)' : ''), 'RLB', 'L', false, 1, '', '', true, 0, false, true, $feedbackHeight, 'T', false);
            }
        }

        // BOTTOM LINE OF TABLE
        $pdf->Multicell(WIDTH_FULL_PAGE, 0, '', 'T', 'C', false, 1, '', '', true, 0, false, false, 0, 'B', true);

        // SIGNATURE
        $pdf->SetFont('times', 'b', 8);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPadding(0, 0, 0, 0);
        $pdf->Multicell(45, '', "PRINT " . strtoupper(gettext('examiner')) ." NAME", 0, 'L', false, 0, '', '', true, 0, false, false, 0, 'T', true);
        $pdf->Multicell(110, '', '', 'B', 'L', false, 0, '', '', true, 0, false, false, 0, 'M', true);

        // TOTAL BOX
        if ($showMarks == 1) {
            $pdf->setCellPaddings(1, 1, 1, 1);
            $pdf->setCellMargins(5, 1, 0, 0);
            $pdf->Cell(16, 6, 'TOTAL', 0, 0, 'C', false, '', 0, false, 'C', 'C');
            $pdf->setCellMargins(0, 1, 0, 0);
            $pdf->Cell(30, 6, '/' . $totalPossible, 1, 1, 'R', false, '', 0, false, 'C', 'C');
        }
    }

    // TIDY UP
    unset($sections);
    unset($items);
    unset($answers);
    unset($sectionAnswerTitles);
}

// Close and output PDF document
$pdf->Output('marking grid print.pdf', 'I');
