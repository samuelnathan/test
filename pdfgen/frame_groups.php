<?php
/** Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/08/2016
 * © 206 Qpercom Limited.  All rights reserved
 * PDF Groups, loaded using an iframe with option panel
 */
require_once __DIR__ . '/../extra/essentials.php';

// Critical Session Check
$session = new OMIS\Session;
$session->check();
 
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
  return false;
}
    
// Sanitize session ID
$sessionID = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

// Abort if session param does not exist in system
if (!$db->exams->doesSessionExist($sessionID)) {
  return false;
}

// Can continue with PDF processing?
$canContinue = true;

// Get session record
$sessionRecord = $db->exams->getSession($sessionID);
$sessionDate = formatDate($sessionRecord['session_date']);
$sessionName = $sessionRecord['session_description'];
$examID = $sessionRecord['exam_id'];
$examRecord = $db->exams->getExam($examID);
$examName = ucfirst($examRecord['exam_name']);

// Return Variables
$formVars = [
  ['name' => 'page', 'value' => 'preview_groups'],
  ['name' => 'session', 'value' => $sessionID],
  ['id' => 'exam-name', 'value' => $examName],
  ['id' => 'session-name', 'value' => $sessionName],
  ['id' => 'session-date', 'value' => $sessionDate]
];

require_once __DIR__ . '/../pdfgen/pdf_groups.php';
    
?>
<form id="back" action="manage.php" method="get">
<?php
foreach ($formVars as $input) {
    $hiddenInput = ' <input type="hidden"';
    if (count($input) > 0) {
      foreach ($input as $key => $value) {
         $hiddenInput .= ' ' . $key . '="' . $value . '"';
      }
    }
    $hiddenInput .= "/>\n";
    echo $hiddenInput;
}

// Append current timestamp to the filename.
$pdfUrl .= "?t=" . date('sihymd');
?>
<input type="submit" id="backb" value="Click to Return"/>
</form>
<iframe src="<?=$pdfUrl?>" id="pdframe" width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0">
    <p>Your browser does not support iframes.</p>
</iframe>
