<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  * PDF Shell to Preview PDF Feedback 
  */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

#Critical Session Check
$session = new OMIS\Session;
$session->check();
 
#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

$pdforename = $_GET['n'];
list($pdfstu, ) = explode('_', $pdforename);
?>
<input type="hidden" id="student-reference" value = "<?=$pdfstu; ?>"/><iframe src="storage/app/tmp/feedback/<?=$pdforename; ?>.pdf?t=<?=date('sihymd'); #current time; ?>" id="pdframe" width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0">
    <p>Your browser does not support iframes.</p>
</iframe>
