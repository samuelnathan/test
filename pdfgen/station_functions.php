<?php
/* Original Author: Domhnall Walsh
 * For Qpercom Ltd
 * Date: 27/07/2015
 * © 2015 Qpercom Limited.  All rights reserved
 * Helper functions for station forms to PDF
 */

use \OMIS\Reporting\PDF as PDF;

/* (DW) Define some page width constants rather than hard-coding values - these
 * are column widths in mm
 */
define("WIDTH_FULL_PAGE", 206);
define("WIDTH_TITLE_COLUMN", 120);

/**
 * Render a section's header into the TCPDF object $pdf.
 * 
 * @param TCPDF     $pdf        PDF document to render into.
 * @param mixed[]   $section    Description of the Section.
 * @param mixed[]   $items      Description of the Section's items.
 * @param mixed[]   $answers    List of the sections' answer headings.
 * @param boolean   $continued  If true, appends ' (contd.)' to section title.
 * @return boolean
 */
function renderSectionHeader($pdf, $section, $items, $answers, $continued = \FALSE)
{
    // Initial font size
    $pdf->SetFont('times', 'b', 9);

    $titleText = trim($section['competence_desc'] . ' ' . $section['section_text']);
    if ($continued) {
        $titleText .= ' (contd.)';
    }

    // If there are no items, then just print the section description across the
    // full width of the page.
    if (count($items) == 0) {
        $headerRowHeight = PDF::calculateHtmlHeight($pdf, WIDTH_FULL_PAGE, $titleText);
        $pdf->Multicell(WIDTH_FULL_PAGE, $headerRowHeight, $titleText, 'RLBT', 'L', true, 1, '', '', true, 0, false, false, 0, 'T', true);
        return true;
    } else {
        // Calculate the height of the title description as would be needed for
        // a width of 120mm...
        $headerRowHeight = PDF::calculateHtmlHeight($pdf, WIDTH_TITLE_COLUMN, $titleText);
    }

    $columnWidth = (WIDTH_FULL_PAGE - WIDTH_TITLE_COLUMN) / $answers['answer_count'];

    // Figure out the tallest element in the row.
    foreach (array_column($answers['titles'], 'descriptor') as $title) {
        $headerRowHeight = max(
            $headerRowHeight,
            PDF::calculateHtmlHeight($pdf, $columnWidth, trim($title))
        );
    }

    // Render the title of the given competence.
    $pdf->Multicell(WIDTH_TITLE_COLUMN, $headerRowHeight, $titleText, 'TLB', 'L', true, 0, '', '', true, 0, false, false, 0, 'T', true);

    // Answer Titles
    $answerTitleCount = 0;

    /* Render the headings for the responses to the competence items within a
     * given competence.
     */
    foreach ($answers['titles'] as $title) {
        //error_log(__METHOD__ . ": Text of option is " . $title['descriptor']);
        $pdf->Multicell(
            $columnWidth,
            $headerRowHeight,
            trim($title['descriptor']),
            'TLRB',
            'C',
            true,
            ($answers['answer_count'] == ++$answerTitleCount) ? 1 : 0,
            '',
            '',
            true,
            0,
            false,
            false,
            0,
            'T',
            true
        );
    }
    $pdf->SetFont('times', '', 9);

    return true;
}

function stripTitle($titleString)
{
    return strtolower(
        str_replace(
            ["\t", "\r", "\n", " "],
            "",
            trim(preg_replace('/\s+/', ' ', $titleString))
        )
    );

}
