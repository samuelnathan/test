<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use OMIS\Utilities\Path as Path;

defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

require_once "pdf_settings.php";

/****************************PREPARE INFORMATION*******************************/
if (!isset($canContinue) || !$canContinue) {
    return false;
}

// Session Data
$sessionRecord = $db->exams->getSession($sessionID);
$termID = $sessionRecord["term_id"];
$date = formatDate($sessionRecord["session_date"]);
$dateFile = formatDate($sessionRecord["session_date"], "NOTHING", "SHORT_YEAR");
$departmentName = $db->departments->getDepartmentName($sessionRecord["dept_id"]);
$sessionDesc = $sessionRecord["session_description"];
$examName = $sessionRecord["exam_name"];

// Get exam settings
$examSettings = $db->examSettings->get($sessionRecord["exam_id"]);
$examStudentIDType = $examSettings["exam_student_id_type"];

// Groups
$groups = $db->exams->getSessionGroups($sessionID);

// Spacing
$spacing = [
    "title" => 171,
    "student_order" => 15,
    "student_id" => 24,
    "surname" => 55,
    "forename" => 55,
    "gender" => 22,
    "blank" => 156
];

/***************************PDF FILE NAME**************************************/
// Changed space to underscore between the date and the description to 
$pdfNameFormat = "%s_%s.pdf";

// PDF file name before sanitization
$pdfFileNameRaw = sprintf($pdfNameFormat, $dateFile, $sessionDesc);

// Remove illegal characters from the file name
$pdfFileName = preg_replace("/[^0-9a-z\.\_\-]/i","", $pdfFileNameRaw);

$pdfPath = Path::join(
    $config->paths["temp"],
    $pdfFileName
);

// Remove the file if it already exists...
if (file_exists($pdfPath) === true) {
    unlink($pdfPath);
}

/**************************PREPARE PDF*****************************************/
// Prepare Titles
$mainTitle = $date . " - " . $sessionDesc;
$subTitle = $examName . ", " . $departmentName;

// Document Title & Subject
$pdf->SetTitle($mainTitle);
$pdf->SetSubject($subTitle);
$pdf->SetMargins(2, 13, 2);

// Set default header data
$pdf->SetHeaderData($pdf_logo, $logo_width_mm, $mainTitle, $subTitle);
$pdf->AddPage();
$pdf->setCellMargins(0, 0, 0, 0);
$pdf->setCellPaddings(1, 1, 1, 1);

/**
 * Render a student group. Using a function to render this because we'll need to
 * render each group twice - once to establish how tall it is to determine if we
 * need a page break or not, and a second time to actually render the student
 * group to the PDF we're trying to output.
 * 
 * @global \OMIS\Database\CoreDB $db
 * @global TCPDF $pdf object
 * @param array $studentGroup Array of student data
 * @param array $cellSpacings Cell spacings array.
 * @param int $examIDType assessment ID for student during exam
 * @param string $termID term ID to get student candidate number 
 */
function renderStudentGroup(&$pdf, $studentGroup, $cellSpacings, $examIDType, $termID)
{
    global $db;
    $pdf->SetFont("times", "b", 10);
    $pdf->Multicell($cellSpacings["title"], 0, $studentGroup["group_name"], "TRL", "C", true, 1, "", "", true, 0, false, false, 0, "M", true);

    // Examinee titles
    $pdf->SetFont("times", "b", 9);
    $pdf->Multicell($cellSpacings["student_order"], 0, "Order", "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
    
    $pdf->Multicell($cellSpacings["student_id"], 0, "Identifier", "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
    
    $pdf->Multicell($cellSpacings["surname"], 0, "Surname", "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
    $pdf->Multicell($cellSpacings["forename"], 0, "Forenames", "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
    $pdf->Multicell($cellSpacings["gender"], 0, "Gender", "TRL", "C", false, 1, "", "", true, 0, false, false, 0, "M", true);

    // Examinee rows
    $students = $db->exams->getStudentsAndBlanks(
            $studentGroup["group_id"],
            $termID
    );
    $studentOrder = 1;
    foreach ($students as $student) {
        // Student order
        $pdf->SetFont("times", "b", 9);
        $pdf->Multicell($cellSpacings["student_order"], 0, $studentOrder, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);

        // Additional rows
        $pdf->SetFont("times", "", 9);
        if ($student["type"] == 1) {
            $pdf->Multicell($cellSpacings["blank"], 0, "BLANK STUDENT", "TRL", "C", true, 1, "", "", true, 0, false, false, 0, "M", true);
        } else {
            
            if ($examIDType == EXAM_NUMBER_TYPE) {
              $examIdentifier = $student["exam_number"];  
            } else if ($examIDType == CANDIDATE_NUMBER_TYPE) {
              $examIdentifier = $student["candidate_number"];    
            } else {
              $examIdentifier = $student["student_id"];
            }
            
            $pdf->Multicell($cellSpacings["student_id"], 0, $examIdentifier, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
            $pdf->Multicell($cellSpacings["surname"], 0, $student["surname"], "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
            $pdf->Multicell($cellSpacings["forename"], 0, $student["forename"], "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
            $pdf->Multicell($cellSpacings["gender"], 0, $student["gender"], "TRL", "C", false, 1, "", "", true, 0, false, false, 0, "M", true);
        }
        $studentOrder++;
    }

    // Bottom row
    $pdf->Multicell($cellSpacings["title"], 6, "", "T", "C", false, 1, "", "", true, 0, false, false, 0, "M", true);
}
/****************************CYCLE THROUGH DATA********************************/
while ($group = $db->fetch_row($groups)) {
    /* Try to render the group into a temporary TCPDF object to see how tall it
     * is. Then use this to decide if we need to inject a new page or not.
     */
    $pdf2 = clone $pdf;
    $pdf2->addPage();
    $startingY = $pdf2->getY();
    renderStudentGroup($pdf2, $group, $spacing, $examStudentIDType, $termID);
    $groupHeight = $pdf2->getY() - $startingY;
    unset($pdf2);
    
    /* (DW) Rough and ready "new page" calculation. Assuming that if the end of
     * the student group box would be more than 285mm from the top of the page
     * then it'll probably run off the end of the printable area, so inject a
     * page break first so that it gets rendered at the top of the next page
     * instead.
     */
    if (($pdf->getY() + $groupHeight) > 285) {
        $pdf->addPage();
    }
    
    // Draw the group in the actual final TCPDF object.
    renderStudentGroup($pdf, $group, $spacing, $examStudentIDType, $termID);
}

// Close and output PDF document
$pdf->Output($pdfPath, "F");

// Get the relative path to the tmp directory
$relPathDir = $config->getRelativePath("tmp_path");

// Glue the file name to the tmp path
$completePath = $relPathDir . "/" . $pdfFileName;

// Forward slashes only as it's a url path
$pdfUrl = Path::setSeparator($completePath);
