<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 12/06/2016
 * @PDF Shell Examinee Results Export
 * © 2016 Qpercom Limited.  All rights reserved
 */
use \OMIS\Template as Template;

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');
define('EXCLUDE_ABSENTS', true);

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// What action to take 
$action = filter_input(\INPUT_POST, 'action', FILTER_SANITIZE_STRING);

// Student identifer
$studentID = filter_input(\INPUT_POST, 'student', FILTER_SANITIZE_STRING);
if (empty($studentID) || !$db->students->doesStudentExist($studentID)) {
    error_log(__FILE__ . ": Student ID not specified or doesn't exist.");
    return;
}

// Exam identifier
$examID = filter_input(\INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
if (empty($examID) || !$db->exams->doesExamExist($examID)) {
    error_log(__FILE__ . ": Exam ID not specified or doesn't exist.");
    return;
}

// Stations selected
if (filter_has_var(\INPUT_POST, 'stations')) {
    $stationsSelected = filter_input(
        \INPUT_POST,
        'stations',
        FILTER_SANITIZE_NUMBER_INT,
        FILTER_REQUIRE_ARRAY
    );
} else {
    $stationsSelected = [];
}

// Do sanity checking based on the HTTP method used to push the form data.
switch ($action) {
    case GENERATE_INDIVIDUAL_STATION:
                
        // Include observer scores
        $includeObserverScores = null; 
        
        // All results page
        $allResultsPage = false;
        
        // Generate button clicked
        $genClicked = true;
       
        // Station ID
        $stationID = filter_input(\INPUT_POST, 'station_id', FILTER_SANITIZE_NUMBER_INT);
        
        // Encode student ID and Station ID into Json
        $resultsJsonID = base64_encode(
            json_encode([
                "station_id" => $stationID,
                "student_id" => $studentID
            ])
        );        
                 
        break;
    case GENERATE_ALL_STATIONS:
        
        // Include observer scores
        $includeObserverScores = filter_input(\INPUT_POST, 'observers', FILTER_SANITIZE_NUMBER_INT);
        
        // All results page
        $allResultsPage = true;
        
        // Generate button clicked
        $genClicked = filter_has_var(\INPUT_POST, 'gen');
               
        // Result json ID
        $resultsJsonID = "";
        
        break;
    default:
        error_log(__FILE__ . ": Unexpected input method detected or no form data supplied");
        return;
}

// Get Examinee Details
$studentDB = $db->students->getStudent($studentID);

// Get Exam Details
$examDB = $db->exams->getExam($examID);

// Get student stations
$sessionDB = $db->fetch_row($db->exams->getStudentSession($studentID, $examID));
$results = $db->results->bySession(
        $studentID,
        $sessionDB['session_id'],
        RETURN_RESULTS_AS_ARRAY, 
        'station_number',
        EXCLUDE_ABSENTS
);

// Additional Variables
$studentName = $studentDB['surname'] . ', ' . $studentDB['forename'];

/* Use a template to render the form and the iframe in which the output is
 * rendered...
 */
$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->data = [
    'studentID' => $studentID,
    'studentName' => $studentName,
    'studentDB' => $studentDB,
    'examID' => $examID,
    'observerScores' => (is_null($includeObserverScores)? 0 : 1),
    'examDB' => $examDB,
    'results' => $results,
    'stationsSelected' => $stationsSelected,
    'genClicked' => $genClicked,
    'allResultsPage' => $allResultsPage
];

if (GENERATE_INDIVIDUAL_STATION) {
    $template->data['resultsID'] = $resultsJsonID;
}

$template->render();
