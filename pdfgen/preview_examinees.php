<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 // This file is only included, not invoked directly. Ensure it only runs then...
 defined("_OMIS") or die("Restricted Access");

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

     return false;

 }

 require_once __DIR__ . "/../extra/helper.php";

 // Sanitize request params
 $course = filter_input(INPUT_GET, "c", FILTER_SANITIZE_STRING);
 $year = filter_input(INPUT_GET, "y", FILTER_SANITIZE_NUMBER_INT);
 $module = filter_input(INPUT_GET, "m", FILTER_SANITIZE_STRING);
 $searchString = filter_input(INPUT_GET, "s", FILTER_SANITIZE_STRING);
 $yearTitle = "ALL";
 $rowCount = 0;

 // default index : students.updated_at
 $defaultSortIndex = 9;

 // Candidate number feature
 $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);

 // Prepare ordering
 $columns = [
     "students.student_id",
     "students.surname",
     "students.forename",
     "students.dob",
     "students.gender",
     "students.nationality",
     "students.email",
     "course_years.year_name",
     "students.updated_at"
 ];

 // Add column if feature enabled
 if ($candidateNumberFeature) {

     array_splice($columns, 1, 0, ["candidate_numbers.candidate_number"]);
     $defaultSortIndex++;

 }

 // Count the columns
 $columnCount = count($columns);

 // Ordering parameters
 list($orderType, $orderIndex, $orderMap) = prepareListOrdering("desc", $defaultSortIndex, $columns);


 // Get list of examinees
 $examinees = $db->students->searchStudents(
             $course,
             $year,
             $module,
             $searchString,
             "NL",
             "NL",
             $orderMap[$orderIndex],
             $orderType,
             $_SESSION["cterm"]
  );

 // Number of examinees found
 $examineeCount = mysqli_num_rows($examinees);
 $courseRecord = $db->courses->getCourse($course);

 // Course record found
 if (!is_null($courseRecord)) {

   // Course name
   $courseName = $courseRecord["course_name"];

   // Get course year record
   if ($year != "") {

       $yearRecord = $db->courses->getCourseYear($year);
       $yearTitle = $yearRecord["year_name"];

   }

 ?>
 <form id="print-action" action="manage.php" method="get">
 <table id="examinee-print-list">
  <caption>
   <input type="hidden" name="page" value="pdf_examinees"/>
   <input type="hidden" name="c" value="<?=$course?>"/>
   <input type="hidden" name="y" value="<?=$year?>"/>
   <input type="hidden" name="m" value="<?=$module?>"/>
   <input type="hidden" name="s" value="<?=$searchString?>"/>
   <input type="hidden" name="i" value="<?=$orderIndex?>"/>
   <input type="hidden" name="o" value="<?=$orderType?>"/>
   <input type="submit" name="print" class="fr" id="printslist" value="PDF &amp; Print"/>
  </caption>

  <tr>
   <th class="titlet2"><?=gettext("Course")?> Name</th>
   <td colspan="<?=$columnCount-1?>" class="nbr boldgreen">&nbsp;
     <?=$courseName?>
     <input type="hidden" id="course-name" value="<?=$courseName?>"/>
   </td>
  </tr>

  <tr>
   <th class="titlet2"><?=gettext("Course Year")?></th>
   <td colspan="<?=$columnCount-1?>" class="nbr boldgreen">&nbsp;
     <?=$yearTitle?>
     <input type="hidden" id="year-value" value="<?=$yearTitle?>"/>
   </td>
  </tr>

  <tr>
   <th class="titlet2"><?=gettext("Module")?></th>
   <td colspan="<?=$columnCount-1?>" class="nbr boldgreen">&nbsp;
     <?php
      // Render module name
      if ($module == "") {

         echo "ALL";

      } else {

         $moduleRecord = $db->courses->getModule($module);
         echo $moduleRecord["module_name"];

      ?>
      - <span class="boldgreen"><?=$module?></span>
     <?php

      }
     ?>
   </td>
  </tr>

 <tr id="title-row" class="title-row">
   <th>Identifier</th>
  <?php
   if ($candidateNumberFeature) {

     ?><th>Candidate #</th><?php

   }
  ?>
   <th>Surname</th>
   <th>Forenames</th>
   <th>DOB</th>
   <th>Gender</th>
   <th>Nationality</th>
   <th>Email</th>
   <th class="nbr">Year</th>
  </tr>

  <?php

   // Render examinee rows
   if ($examineeCount > 0) {

     // Iterate through records
     while ($examinee = $db->fetch_row($examinees)) {

       $rowCount++;
       $nbClass = ($rowCount == $examineeCount) ? " nbbtr" : "";

       $examineeID = $examinee["student_id"];
       $candidateNumber = $examinee["candidate_number"];
       $surname = $examinee["surname"];
       $forenames = $examinee["forename"];
       $dob = formatDate($examinee["dob"]);
       $gender = $examinee["gender"];
       $email = $examinee["email"];
       $examineeYear = $examinee["year_name"];
       $nationality = $examinee["nationality"];
       $isoName = "";

       // Nationality name retrieved using iso code
       if (strlen($nationality) > 0) {

         $isoRecord = $db->iso->getISOCountryCode($nationality);
         if (is_array($isoRecord)) {

           $isoName = $isoRecord["iso_name"];

         }

       }

      ?>
       <tr id="examinee_row_<?=$rowCount?>" class="datarow<?=$nbClass?>">
         <td class="c">&nbsp;<?=$examineeID?>&nbsp;</td>
        <?php
         if ($candidateNumberFeature) {

            ?><td>&nbsp;<?=$candidateNumber?>&nbsp;</td><?php

         }
         ?>
         <td>&nbsp;<?=$surname?>&nbsp;</td>
         <td>&nbsp;<?=$forenames?>&nbsp;</td>
         <td>&nbsp;<?=$dob?>&nbsp;</td>
         <td class="c">&nbsp;<?=$gender?>&nbsp;</td>
         <td class="c">&nbsp;<span title="<?=$isoName?>"><?=$nationality?></span>&nbsp;</td>
         <td>&nbsp;<?=$email?>&nbsp;</td>
         <td class="c nbr"><?=$examineeYear?></td>
       </tr>
      <?php

     }

    } else {

    ?>
     <tr id="first_examinee_row">
      <td colspan="<?=$columnCount?>" class="nbb nbr">
       <div id="examinee_row_div" class="tablestatus2">No <?=gettext('students')?> found</div>
      </td>
    </tr>
    <?php

     }
 ?>
  </table>
  </form>
 <br/><br/><br/>
 <?php

  } else {

   ?>
    <div class="errordiv"><?=gettext("Course")?> ID was not found in the system</div>
   <?php

  }
