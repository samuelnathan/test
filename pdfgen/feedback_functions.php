<?php
 /**
  * Helper functions for pdfgen/pdf_feedback.php
  * @copyright Qpercom Ltd. 2019
  * @see pdf_feedback.php
  */
 use OMIS\Database\Results as Results;

 /**
  * Render the row at the top of a table describing a station and its underlying
  * competencies.
  *
  * @param \OMIS\Reporting\PDF $pdf_object     PDF Rendering object
  * @param int                 $station_number Number of station
  * @param string              $station_name   Name of station
  *
  */
 function renderStationHeader(&$pdf_object, $station_number, $station_name)
 {

     if (abortContentRender()) return;

     $pdf_object->SetFont('times', 'b', 9);
     $pdf_object->setCellMargins(0, 0, 0, 0);
     $pdf_object->setCellPaddings(1, 1, 1, 1);
     $pdf_object->SetFillColorArray([192, 192, 192], false);
     $station_title = "Station $station_number: $station_name";

     // Get available height
     $availableWidth = $pdf_object->getAvailableWidth();

     // Get string height
     $stringHeight = $pdf_object->getStringHeight(
             $availableWidth, $station_title,
             true, true, "", 'TRLB'
     );

     // Get available page height
     list($availableHeight) = $pdf_object->getYValues();

     /**
      * If the header height (plus first section title height, average 8px)
      * exceeds the available page height then skip
      * to the next page
      */
     $firstSectionTitleHeight = 8;
     if (($stringHeight + $firstSectionTitleHeight) > $availableHeight) {
         $pdf_object->writeContinueFooter();
         $pdf_object->AddPage();
     }

     // Render station title cell
     $pdf_object->MultiCell(
             $availableWidth, '', $station_title,
             'TRLB', 'C', true, 1, '', '',
             true, 0, false, true, 0, 'T', true
     );

 }

 /**
  * Render station notes cell
  *
  * @param \OMIS\Reporting\PDF $pdfObject     PDF Rendering object
  * @param string   $text                     Text containing station notes or instructions
  * @param string   $notesTitle            Text for title (optional)
  *
  */
 function renderStationNotes(&$pdfObject, $text, $notesTitle = '')
 {
     
    if (abortContentRender()) return;  

     $pdfObject->SetFont('times', '', 8);
     $pdfObject->setCellMargins(0, 0, 0, 0);
     $pdfObject->setCellPaddings(1, 1, 1, 1);
     $pdfObject->SetFillColorArray([192, 192, 192], false);

     // Get available height
     $availableWidth = $pdfObject->getAvailableWidth();

     // Get string height for notes and optional title
     $stringHeight = $pdfObject->getStringHeight(
             $availableWidth, $text,
             true, true, '', 'RL'
     );

     if (strlen($notesTitle) > 0) {
           $pdfObject->SetFont('times', 'b', 8);
           $stringHeight =+ $pdfObject->getStringHeight(
               $availableWidth, $notesTitle,
               true, true, '', 'RL'
           );
     }

     // Get available page height
     list($availableHeight) = $pdfObject->getYValues();

     /**
      * If the string height exceeds the available
      * page height then skip to the next page
      */
     if ($stringHeight > $availableHeight) {
         $pdfObject->writeContinueFooter();
         $pdfObject->AddPage();
     }

     // Render station notes cell
     if (strlen($notesTitle) > 0) {
         $pdfObject->MultiCell(
                 $availableWidth, '', $notesTitle,
                 'RL', 'L', false, 1, '', '',
                 true, 0, false, true, 0, 'T', true
         );
     }

     // Render text cell
     $pdfObject->SetFont('times', '', 8);
     $pdfObject->MultiCell(
             $availableWidth, '', $text,
             'RL', 'L', false, 1, '', '',
             true, 0, false, true, 0, 'T', true
     );

 }

/**
 * Render the header for a competence within a station.
 *
 * @param \OMIS\Reporting\PDF $pdf_object PDF Rendering object
 * @param string $competence_title Title of competence.
 * @param int $numOfMultipleResults Number of multiple results
 * @param boolean $showMultiExaminerScores show multiple examiner scores?
 * @param boolean $scoreValues Display score values column
 * @param $itemWeighting boolean Display item weighting headers
 */
 function renderSectionHeader(&$pdf_object, $competence_title, $numOfMultipleResults = 1, $showMultiExaminerScores, $scoreValues, $itemWeighting, $onlyTitle = false)
 {
     
    if (abortContentRender()) return; 
    
     $itemWeighting = $itemWeighting && $scoreValues;

     // Render the Competence Title
     $pdf_object->SetFont('times', 'b', 8);
     $pdf_object->SetFillColorArray([232, 232, 232], false);
     $competenceTitle = mb_convert_case($competence_title, MB_CASE_TITLE, 'UTF-8');

     $doubleRowHeader = $numOfMultipleResults > 1 && $showMultiExaminerScores && $itemWeighting;

     list($_, $sectionTitleWidth, $panellistWidth) = $onlyTitle ? [0, $pdf_object->getAvailableWidth(), 0] : calculateItemCellsWidth(
         $pdf_object->getAvailableWidth(),
         $numOfMultipleResults,
         $showMultiExaminerScores,
         $scoreValues,
         $itemWeighting
     );

     // We need at least one column
     if (!($numOfMultipleResults > 0)) {
        $numOfMultipleResults = 1;
     }

     $lastCellInRow = $onlyTitle || (($numOfMultipleResults == 1 || !$showMultiExaminerScores) && !$itemWeighting);

     $stringHeightCandidates = [];

     $stringHeightCandidates[] = $pdf_object->getStringHeight(
         $sectionTitleWidth, $competenceTitle,
         true, true, '', 'TRLB'
     );

     $panellistCells = ($numOfMultipleResults > 1 && $showMultiExaminerScores) ?
         $numOfMultipleResults : 1;

     $bottomHeaderHeight = 0.0;
     for ($num = 1; $num <= $panellistCells; $num++) {
         $panellistHeight = $pdf_object->getStringHeight($panellistWidth, gettext("Examiner") . $num, true, true, '', 'TRLB');

         if ($itemWeighting) {
             $bottomCell = max(
                 $pdf_object->getStringHeight($panellistWidth / 2, 'Raw', true, true, '', 'TRLB'),
                 $pdf_object->getStringHeight($panellistWidth / 2, 'Weighted', true, true,  '', 'TRLB')
             );

             $panellistHeight += $bottomCell;
             $bottomHeaderHeight = max($bottomHeaderHeight, $bottomCell);
         }

         $stringHeightCandidates[] = $panellistHeight;
     }

     $stringHeight = max($stringHeightCandidates);

     // Get available page height
     list($availableHeight) = $pdf_object->getYValues();

     /**
      * If the section title header height (plus grace height)
      * exceeds the available page height then skip
      * to the next page
      */
     $graceHeight = 2;
     if (($stringHeight + $graceHeight) > $availableHeight) {
         $pdf_object->writeContinueFooter();
         $pdf_object->AddPage();
     }

     // Write full-width header.
     $pdf_object->MultiCell($sectionTitleWidth,
                            0, $competenceTitle,
                            'TRL' . (!$doubleRowHeader ? 'B' : ''), 'L', true, (int) $lastCellInRow,
                            '', '', true, 0, false, true,
                            0, 'T', true);

     // Render examiner titles if we have multiple results
     if (!$onlyTitle && $numOfMultipleResults > 1 && $showMultiExaminerScores) {

        for ($num=1; $num<=$numOfMultipleResults; $num++) {

         $lastCell = (int)($num == $numOfMultipleResults && !$scoreValues);

          $pdf_object->MultiCell(
                 $panellistWidth, '', gettext("Examiner") . $num, 'TRLB', 'C', true, $lastCell,
                 '', '', true, 0, false, true, 0, 'T', true
          );

       }

       // Only display score column if the score values are to be displayed
       if ($scoreValues) {
         // Weighting score title
         $pdf_object->MultiCell(
              $panellistWidth, '', "Score", 'TRL' . (!$doubleRowHeader ? 'B' : ''), 'C', true, 1,
              '', '', true, 0, false, true, 0, 'T', true
         );
       }

     }

     if ($itemWeighting) {
         if ($numOfMultipleResults > 1 && $showMultiExaminerScores) {
             $pdf_object->MultiCell($sectionTitleWidth, $bottomHeaderHeight, '', 'LB', 'C', true, 0,
                 '', '', true, 0, false, true, 0, 'T', true);
         }

         for ($num = 1; $num <= $panellistCells; $num++) {
             $pdf_object->MultiCell(
                 $panellistWidth/2, $bottomHeaderHeight, 'Raw', 'TRLB', 'C', true, 0,
                 '', '', true, 0, false, true, 0, 'T', true
             );

             $pdf_object->MultiCell(
                 $panellistWidth/2, $bottomHeaderHeight, 'Weighted', 'TRLB', 'C', true, (int)($num === $panellistCells && !($numOfMultipleResults > 1 && $showMultiExaminerScores && $scoreValues)),
                 '', '', true, 0, false, true, 0, 'T', true
             );
         }

         if ($numOfMultipleResults > 1 && $showMultiExaminerScores && $scoreValues) {
             $pdf_object->MultiCell($panellistWidth, $bottomHeaderHeight, '', 'LBR', 'C', true, 1,
                 '', '', true, 0, false, true, 0, 'T', true);
         }
     }


 }

 /**
  * Calculate item weighted score (for multiple examiner results)
  *
  * @param array $itemMarks                       Item Marks
  * @param bool  $multiExaminerResultsAveraged    Multiple examiner result averaged
  * @return int   $weightedItemScore               Weighted Item Score
  */
 function calculateItemWeightedScore($itemMarks, $multiExaminerResultsAveraged = true) {


     // Calculate weighted item score
     $itemScoresWeightings = [];

     // Accumulate item score weightings
     foreach ($itemMarks as $itemMark) {

       $itemScoresWeightings[] = [
          'score' => $itemMark['option_value'],
          'weighting' => $itemMark['examiner_weighting']
       ];

     }

     /**
      * We're requested to summate the results rather
      * than calculating an average
      */
     if (!$multiExaminerResultsAveraged) {
         $sumScore = array_sum(array_column($itemScoresWeightings, 'score'));
         return $sumScore;
     }

     $product = array_map('array_product', $itemScoresWeightings);
     $sum = array_sum($product);
     $weightedItemScore = (float) $sum /
             array_sum(array_column($itemScoresWeightings, 'weighting'));
     unset($itemScoresWeightings);

     return $weightedItemScore;    
     
 }


/**
 * Render the details for a competence item
 *
 * @param \OMIS\Reporting\PDF $pdfObj PDF Rendering object
 * @param mixed $item Competence Item
 * @param mixed $results Item results
 * @param boolean $enableText Print Text?
 * @param boolean $enableDescs Print descriptions?
 * @param boolean $enableValues Print values?
 * @param boolean $isScale Is the item a scale?
 * @param boolean $isGraded Is the item a score item?
 * @param boolean $showMultiExaminerScores show multiple examiner scores?
 * @param boolean $scoreValues score values option selected
 * @param $enableItemWeight boolean Render item weighting?
 */
 function renderSectionItem(&$pdfObj, $item, $results, $enableText, $enableDescs, $enableValues, $isScale, $isGraded, $showMultiExaminerScores, $scoreValues, $itemOutcome, $enableItemWeight)
 {
     
    if (abortContentRender()) return;

    /**
     * [DC 08/02/2016] Logic in this function needs to be refactored.
     * Can't do now due to time constraints
     */

     // Get number of current page
     $originalPageNum = $pdfObj->PageNo();

     global $config;
     global $numMultiResults;
 
     // Load config settings
     if (isset($config->feedback_tool)) {
         $defaultGRSText = $config->feedback_tool['default_grs_text'];
     } else {
         $defaultGRSText = "Global judgement";
     }

     // Set cell padding
     $pdfObj->setCellPaddings(0.6, 0.6, 0.6, 0.6);

     $enableItemWeight = $enableItemWeight && $scoreValues;

     // Display item text (yes/no)
     if ($enableText) {
         // If scale item display related item text
         $itemTextSrc = ($isScale) ? $defaultGRSText : $item['text'];
     } else {
         $itemTextSrc = "";
     }
     
     // Rewrite image paths in the HTML content to please TCPDF :(
     $itemText = tcpdfImagesPath($itemTextSrc);
     
     // Default score cell width
     $scoreCellWidth = 90;

     /**
      * If we have multiple results and we have only the value descriptors to display then
      * set to show multi examiner scores option
      */
     if ($numMultiResults > 1 && $enableDescs && !$enableValues && !$scoreValues) {
         $showMultiExaminerScores = true;
     }

     // Get the dimensions of the cells for this item
     list($titleWidth, $_, $scoreCellWidth) = calculateItemCellsWidth(
         $pdfObj->getAvailableWidth(),
         $numMultiResults,
         $showMultiExaminerScores,
         $scoreValues,
         $enableItemWeight
     );

     // Calculate html cell height
     $pdfObj->setCellPaddings(0.6, 0.6, 0.6, 0.6);
     $htmlCellHeight = $pdfObj->calculateHtmlHeight($pdfObj, $titleWidth, $itemText);

     // Set cell padding values
     $pdfObj->setCellPaddings(0.6, 0.6, 2, 0.6);

     // Outcome of the item indexed by examiner order. Check for nullity if the item is non-scorable
     $indexedOutcome = $itemOutcome !== null ? array_values($itemOutcome['examiner_id']) : null;

     // Gather complete answer output
     $itemAnswersOutput = [];

     if (isset($results['raw_values'])) {
         $i = 0;
         foreach ($results['raw_values'] as $resultIndex => $result) {

             $fullAnswerParts = [];
             if ($enableDescs) {
                 $fullAnswerParts[] = $result['descriptor'];
             }

             if ($enableValues && $indexedOutcome !== null) {
                 $fullAnswerParts[] = ($enableDescs ? "\nScore: " : '') . $indexedOutcome[$i]['option_value'];
             }

             // Remove any empty strings
             $fullAnswerParts = array_filter($fullAnswerParts, "strlen");

             $fullAnswerText = implode("\n", $fullAnswerParts);

             $itemAnswersOutput[$resultIndex] = $fullAnswerText;

             $i++;

         }

    }

     // Get max string height
     $maxCellHeight = array_reduce(
         $itemAnswersOutput,
         function ($currentMaxHeight, $text) use ($pdfObj, $scoreCellWidth, $enableItemWeight) {

             // Get string height for multi cell
             $stringHeight = $pdfObj->getStringHeight(
                  $scoreCellWidth / ($enableItemWeight ? 2 : 1), $text,
                  false, true, '', 'RLB'
             );

             // If string height is greater than current then return
             if ($stringHeight > $currentMaxHeight) {
                return $stringHeight;
             } else {
                return $currentMaxHeight;
             }

         },
         $htmlCellHeight
     );

     // Render item text
     $pdfObj->setCellPaddings(0.6, 0.6, 0.6, 0.6);
     $pdfObj->writeHTMLCellAutoWrap($titleWidth, $maxCellHeight, '', '', $itemText, 'RLB', 0, false, true, 'L', true);

     // Reset padding again for multi cell rendering
     $pdfObj->setCellPaddings(0.6, 0.6, 2, 0.6);

     /**
      * If we have moved to a new page then
      * add a top border to all the multicells
      */
     $newPageNum = $pdfObj->PageNo();
     if ($newPageNum > $originalPageNum) {
       $cellBorder = "RLTB";
     } else {
       $cellBorder = "RLB";
     }

     /**
      * If it's just required to display the item text only then
      * render empty answer cell
      */
     $itemTextOnly = ($enableText && !$enableDescs && !$enableValues);
     if ($itemTextOnly) {
          $pdfObj->MultiCell(
               $pdfObj->getAvailableWidth() - $titleWidth, $maxCellHeight, "",
               $cellBorder, 'L', false, 1, '', '',
               true, 0, false, true, 0, 'T', false
          );
        return;
     }

     // Render item results
     for ($resultNr=1; $resultNr<=$numMultiResults; $resultNr++) {

         // Get the outcome for the examiner in the current iteration
         $examinerOutcome = ($indexedOutcome !== null && array_key_exists($resultNr - 1, $indexedOutcome)) ? $indexedOutcome[$resultNr - 1] : null;

        // Get full answer text
        $fullAnswerText = isset($itemAnswersOutput[$resultNr]) ? $itemAnswersOutput[$resultNr] : "";

        /**
         *  1 = next cell next line
         *  0 = next cell to the right
         */
        $position = (int) ($numMultiResults == 1 || (!$enableValues && !$scoreValues && $resultNr == $numMultiResults));

        // Render examiners scores if setting is set
        if ($numMultiResults == 1 || ($numMultiResults > 1 && $showMultiExaminerScores)) {

            $pdfObj->MultiCell(
                $scoreCellWidth / ($enableItemWeight ? 2 : 1), $maxCellHeight, $fullAnswerText,
                $cellBorder, 'C', false, $enableItemWeight ? 0 : $position, '', '',
                true, 0, false, true, 0, 'T', false
            );

            if ($enableItemWeight) {
                $weightedValue = ($examinerOutcome && $scoreValues) ? $examinerOutcome['option_weighted_value'] : '';

                $pdfObj->MultiCell($scoreCellWidth/2, $maxCellHeight, $weightedValue,
                $cellBorder, 'C', false, $position, '', '',
                true, 0, false, true, 0, 'T', false);
            }

         }

     }

     // Multiple results? then we needed a weighted score cell
     if (($numMultiResults > 1 || $numMultiResults == 0) && ($enableValues || $scoreValues)) {

         $cellOutput = "";
         $cellOutputWeighted = '';
         // If there are multiple results but the user selected not to display them, and the item has a weighting,
         // the item outcome raw and weighted must be displayed
         $showWeightedOutcome = $numMultiResults > 1 && !$showMultiExaminerScores && $enableItemWeight;

         // If the item is scorable then calculate weighted score
         if ($isGraded) {

           $cellOutput = sprintf("%.3g", ($showWeightedOutcome || !$enableItemWeight) ? $itemOutcome['score'] : $itemOutcome['weighted_score']);
           $cellOutputWeighted = sprintf('%.3g', $itemOutcome['weighted_score']);
         }

         // Render multi-cell (final cell in the row)
         $pdfObj->MultiCell(
            $scoreCellWidth / ($showWeightedOutcome ? 2 : 1), $maxCellHeight, $cellOutput,
            $cellBorder, 'C', false, $showWeightedOutcome ? 0 : 1, '', '', true, 0, false, true, 0, 'T', false
         );

         if ($showWeightedOutcome) {
             $pdfObj->MultiCell(
                 $scoreCellWidth/2, $maxCellHeight, $cellOutputWeighted,
                 $cellBorder, 'C', false, 1, '', '', true, 0, false, true, 0, 'T', false
             );
         }

     }

 }

/**
 * Calculates the width of the different cells to render an item
 *
 * @param $availableWidth int
 * @param $numberOfExaminers int
 * @param $showMultiExaminerScores bool
 * @param $scoreValues bool
 * @param $showItemWeightings bool
 * @return array
 */
 function calculateItemCellsWidth($availableWidth, $numberOfExaminers, $showMultiExaminerScores, $scoreValues, $showItemWeightings)
 {
     $defaultScoresWidth= 90;
     $limitNumExaminers = 5;

     // If the space for scores must fit more than 2 examiners with the item weighting, dynamically calculate
     // the width
     if ($showItemWeightings && $showMultiExaminerScores && $numberOfExaminers > 2) {
         // Maximum size that the scores cell can grow from the default
         $m = ($availableWidth - $defaultScoresWidth) / 2.0;

         if ($numberOfExaminers > $limitNumExaminers) {
             $scoresWidth = $defaultScoresWidth + $m;
         } else {
             $scoresWidth = $defaultScoresWidth + ($m / $limitNumExaminers) * $numberOfExaminers;
         }
     // Otherwise, fix it to 90
     } else {
         $scoresWidth = $defaultScoresWidth;
     }

     $numberOfScoreCells = ($showMultiExaminerScores && $numberOfExaminers > 1) ? ($numberOfExaminers + ($scoreValues ? 1 : 0)) : 1;

     $itemText = $availableWidth - $scoresWidth;
     $scoreCellsWidth = $scoresWidth / $numberOfScoreCells;
     $headerWidth = (!$showMultiExaminerScores || $numberOfExaminers < 2) && !$showItemWeightings ?
         $availableWidth : $itemText;

    return [$itemText, $headerWidth, $scoreCellsWidth];
 }

 /**
  * Render the (full-width) comment field for a competence
  *
  * @param \OMIS\Reporting\PDF $pdf_object PDF Rendering object
  * @param array               $feedback  Array of strings, feedback submitted by examiners for each section
  * @param boolean             $is_scale   Is the item a scale?
  */
 function renderComment(&$pdf_object, $feedback, $is_scale = false)
 {
     
    if (abortContentRender()) return;

     // Let's start with an empty string for the feedback output
     $feedbackText = "";
     $i=0;
     // Loop through all examiners feedback and santize
     foreach ($feedback as $examinerFeedback) {

         $i++;
         if (strlen($examinerFeedback) > 0) {
             $examinerFeedback =  gettext("Examiner") . $i . ": " . $examinerFeedback;

             $santitizedFeedback = preg_replace(
                 ["/\"/", "/\r\n/", "/\t/"], ["'", " ", " "], $examinerFeedback
             );
             $feedbackText .= "\"$santitizedFeedback\" ";
         }

     }

     // We have comments? then render
     if ((trim($feedbackText) != '') || $is_scale) {
         $pdf_object->SetFont('times', 'BI', 7);
         $pdf_object->setCellPaddings(1, 1, 1, 1);

         // Comment height
         $commentHeight = $pdf_object->getStringHeight(191, $feedbackText, true, true, "", 'RB');

         // Get available page height
         list($availableHeight) = $pdf_object->getYValues();

         /**
          * If the comment height (plus grace height)
          * exceeds the available page height then skip
          * to the next page
          */
         $graceHeight = 2;
         if (($commentHeight + $graceHeight) > $availableHeight) {
             $pdf_object->writeContinueFooter();
             $pdf_object->AddPage();
         }

         // Draw the comment header
         $pdf_object->Multicell(15, $commentHeight, 'Comments:', 'LB', 'L',
                                false, 0, '', '', true, 0, false, true, 0, 'T', true);

         // Render the comment itself
         $pdf_object->SetFont('times', 'I', 7);
         $pdf_object->Multicell(191, $commentHeight, $feedbackText, 'RB', 'L',
                               false, 1, '', '', true, 0, false, true, 0, 'T', true);
     }

 }

 /**
  * Render station score
  *
  * @param object $pdf
  * @param float $accScore
  * @param float $accPossible
  * @param float $percentScore
  * @param bool $showRaw
  * @param bool $showPercent
  * @param string $message
  */

 function renderStationScore(&$pdf, $scoring, $switches, $message)
 {
     
     if (abortContentRender()) return;

     list($accScore, $accPossible, $percentScore) = array_values($scoring);
     list($showRaw, $showPercent) = array_values($switches);

     if (!$showRaw && !$showPercent) {

        return;

     }

     $pdf->SetFont('times', 'B', 8);
     $pdf->setCellPaddings(0, 0, 2, 0);
     $pdf->SetFillColorArray([232, 232, 232], false);

     // Get available page height
     list($availableHeight) = $pdf->getYValues();

     /**
      * If the cell height (plus grace height)
      * exceeds the available page height then skip
      * to the next page
      */
     $graceHeight = 2;
     $cellHeight = 6;
     if (($cellHeight + $graceHeight) > $availableHeight) {

         $pdf->writeContinueFooter();
         $pdf->AddPage();

     }

     $cellValue = ($showRaw) ? "$accScore/$accPossible" : "";
     $cellValue .= ($showRaw && $showPercent) ? " | " : "";
     $cellValue .= ($showPercent) ? "$percentScore %" : "";

     // Cell message
     if (strlen($message) > 0) {

       $pdf->setCellMargins(146, 1, 0, 0);
       $pdf->Cell(32, $cellHeight, $message, 0, 0, 'R', false, '', 1, false, 'C', 'C');

     }

     // Cell values
     $pdf->setX(2);
     $pdf->setCellMargins(178, 1, 0, 0);
     $pdf->Cell(28, $cellHeight, $cellValue, 1, 1, 'R', true, '', 1, false, 'C', 'C');

     // New line
     $pdf->Ln(3);

 }

 /**
  * Render the Student Exam Summary Table
  * @param \OMIS\Reporting\
  * PDF $pdf_object  PDF Rendering object
  * @param mixed[]             $examSummary Student Exam Summary
  * @param mixed[]             $examRules Student Exam Rules
  * @param boolean             $stationScores Show Scores
  * @param boolean             $stationOutcomes Show Outcomes
  * @param boolean             $stationScoreOverall Show Overall Score
  * @param boolean             $stationOutcomeOverall Show Overall Outcome
  * @param boolean             $stationOveralls Show Station Overalls
  * @param boolean             $stationGRS Show Station global rating scale
  * @param boolean             $grsOverallFail Global Rating Scale Overall Fail
  */
 function renderStudentExamSummary(&$pdf_object, $examSummary, $examRules, $stationScores, $stationOutcomes, $stationScoreOverall,
                                    $stationOutcomeOverall, $stationOveralls, $stationGRS, $grsOverallFail, array $examOutcome, $displayWeightedOutcome = false)
 {

     // Exam Rules
     $minStationsPass = $examRules['min_stations_to_pass'];

     // Load Config File
     global $config;

     // Main Title for Summary Table (Config Setting)
     if (isset($config->feedback_tool)) {
         $summaryMainTitle = $config->feedback_tool['summary_table_title'];
     } else { // Default
         $summaryMainTitle = "Exam Summary Table";
     }

     // Accumulator for all station scores
     $accumulatedScore = $examOutcome[$displayWeightedOutcome ? 'adjustedScore' : 'rawScore'];

     // Accumulator for all station possible scores
     $accumulatedPossible = $examOutcome[$displayWeightedOutcome ? 'adjustedPossible' : 'possibleScore'];

     // Title cells
     $pdf_object->setCellMargins(0, 0, 0, 0);
     $pdf_object->setCellPaddings(1, 1, 1, 1);

     // Is station scores last cell in row
     $islastCell = (!$stationOutcomes && !$stationGRS) ? 1 : 0;

     // Work out top cell Width
     if (($stationOutcomes || $stationGRS) && $stationScores) {
         $topCellWidth = 206;
     } else if ($stationScores) {
         $topCellWidth = 184;
     } else {
         $topCellWidth = 162;
     }

     // Set font size for first title
     $pdf_object->SetFont('times', 'B', 10);

     $pdf_object->Multicell($topCellWidth, '', $summaryMainTitle, '', 'C', false, 1, '', '', true, 0, false, true, 0, 'T', true);

     $pdf_object->SetFont('times', 'B', 9);

     // E0E0E0
     $pdf_object->SetFillColorArray(array(232, 232, 232), false);

     /* Summary table titles */
     //Station Number
     $pdf_object->Multicell(15, '', 'Station', 'LBTR', 'C', true, 0, '', '', true, 0, false, true, 0, 'T', true);

     //Form Name
     $pdf_object->Multicell(125, '', 'Station Title', 'BTR', 'L', true, 0, '', '', true, 0, false, true, 0, 'T', true);

     // Display Station Scores Cells
     if ($stationScores) {
         
         // Raw Score
         $rawColTitle = ($displayWeightedOutcome ? gettext('Weighted') : 'Score');
         $pdf_object->Multicell(22, '', $rawColTitle, 'BTR', 'C', true, 0, '', '', true, 0, false, true, 0, 'T', true);

         // Score %
         $rawPerColTitle = ($displayWeightedOutcome ? gettext('Weighted') : 'Score') . ' %';
         $pdf_object->Multicell(22, '', $rawPerColTitle, 'BTR', 'C', true, $islastCell, '', '', true, 0, false, true, 0, 'T', true);
     }

     // Display Station Outcome Cells
     if ($stationOutcomes || $stationGRS) {
         $pdf_object->Multicell(22, '', 'Outcome', 'BTR', 'C', true, 1, '', '', true, 0, false, true, 0, 'T', true);
     }

     // Data cells
     $pdf_object->SetFont('times', '', 8);

     // Station Count
     $stationPassCount = 0;

     /* Loop through data rows (Exam summary) */
     foreach ($examSummary as $eachStation) {
         // Station Number
         $pdf_object->Multicell(15, '', $eachStation['station_number'], 'LBTR', 'C', false, 0, '', '', true, 0, false, true, 0, 'T', true);
         $stationOutcome = $examOutcome['stations'][$eachStation['station_number']];

         // Form Name
         $pdf_object->Multicell(125, '', $eachStation['form_name'], 'BTR', 'L', false, 0, '', '', true, 0, false, true, 0, 'T', true);

         // Display Station Scores Cells
         if ($stationScores) {
             $score = $stationOutcome[$displayWeightedOutcome ? 'adjusted_score' : 'score'];
             $possibleScore = $stationOutcome[$displayWeightedOutcome ? 'adjusted_possible' : 'possible'];

             // Raw Score
             $rawScoreText = roundAndFormat($score, 2) . " / " . $possibleScore;
             $pdf_object->Multicell(22, '', $rawScoreText, 'BTR', 'C', false, 0, '', '', true, 0, false, true, 0, 'T', true);

             // Score %
             $scorePercent = percent($score, $possibleScore, 2);
             $scorePercentText = "$scorePercent %";
             $pdf_object->Multicell(22, '', $scorePercentText, 'BTR', 'C', false, $islastCell, '', '', true, 0, false, true, 0, 'T', true);
         }

         // Display Station Outcome Cells
         if ($stationOutcomes || $stationGRS) {
             $stationPassed = $stationOutcome[$displayWeightedOutcome ? 'adjusted_passed' : 'passed'];
             $outcome = $stationPassed ? 'Pass' : 'Fail';
             $outcome = ($stationGRS) ? $eachStation['station_grs_value'] : $outcome;

             $pdf_object->Multicell(22, '', $outcome, 'BTR', 'C', false, 1, '', '', true, 0, false, true, 0, 'T', true);
             if ($stationPassed) {
               $stationPassCount++;
             }

         }
     }

     // Overalls row
     if ($stationOveralls) {

     /* Summary Totals */
     $pdf_object->SetFont('times', 'B', 8);

     // Empty cell spanning from to the left
     $pdf_object->Multicell(140, '', 'Overall', 'LBTR', 'R', false, 0, '', '', true, 0, false, true, 0, 'T', true);

     // Overall percentage value
     $overallPercentageValue = percent($accumulatedScore, $accumulatedPossible);

     // Station scores column shown ?
     if ($stationScores) {

       // Display station scores values
       if ($stationScoreOverall) {

         $overallPercentageScore = roundAndFormat($overallPercentageValue) . " %";
         $totalRawScoreText = roundAndFormat($accumulatedScore, 2) . " / " . $accumulatedPossible;

       } else {

         $overallPercentageScore = "";
         $totalRawScoreText = "";

       }

       // Total raw score cell
       $pdf_object->Multicell(22, '', $totalRawScoreText, 'BTR', 'C', false, 0, '', '', true, 0, false, true, 0, 'T', true);

       // Total score % cell
       $pdf_object->Multicell(22, '', $overallPercentageScore, 'BTR', 'C', false, $islastCell, '', '', true, 0, false, true, 0, 'T', true);

     }

     // Display overall outcome cell
     if ($stationOutcomes || $stationGRS) {

       // Fail on minimum no. of stations to pass
       $failOnMinimumStationsPass = false;
       $overallOutcome = "";

       // Display overall station outcome text
       if ($stationOutcomeOverall) {

         // Overall Outcome
         $failOnMinimumStationsPass = (is_numeric($minStationsPass) && $stationPassCount < $minStationsPass);

         if ($failOnMinimumStationsPass || $grsOverallFail) {

            $overallOutcome = "Fail *";
         } elseif ($grsOverallFail) {
            $overallOutcome = "Fail *";
         } else {
            $overallOutcome = $examOutcome['grade'];
         }

       }

       $pdf_object->Multicell(22, '', $overallOutcome, 'BTR', 'C', false, 1, '', '', true, 0, false, true, 0, 'T', true);

       /**
        *  Display footnote if there is a fail on minimum no.
        *  of stations to pass
        */
       if ($failOnMinimumStationsPass) {

         $message = "* Minimum number of stations to pass = " . $minStationsPass;
         $pdf_object->SetFont('times', '', 8);
         $pdf_object->Multicell(206, '', $message, '', 'L', false, 1, '', '', true, 0, false, true, 0, 'T', true);
         $pdf_object->SetFont('times', 'B', 8);

       }

       if ($stationOutcomeOverall && $grsOverallFail) {

         $message = "* Minimum number of stations fails = " . $examRules["max_grs_fail"];
         $pdf_object->SetFont('times', '', 8);
         $pdf_object->Multicell(206, '', $message, '', 'L', false, 1, '', '', true, 0, false, true, 0, 'T', true);
         $pdf_object->SetFont('times', 'B', 8);

       }
       
     }

    }
     // Line break
     $pdf_object->Ln(3);

 }


 /**
  * Get student exam possible
  *
  * @deprecated Not used anymore since RFC18: These data are pre-calculated and stored in the database
  * @param mixed[] $data                   Student results
  * @param int[]   $stationNumbers         Station numbers selected
  * @param boolean $avgRule                Average scores
  * @param mixed[] $examinerWeightings     Examiner weightings
  *
  * @return mixed[]                         Student weighting data
  */
 function studentExamPossible($data, $stationNumbers, $avgRule, $examinerWeightings)
 {

    $examPossible = 0;

    foreach ($data as $key => $stationScores) {

        $stationNum = $stationScores[0]['station_number'];
        $stationID = $stationScores[0]['station_id'];

        if (!in_array($stationNum, $stationNumbers)) {

           continue;

        }

        /*
         *  Scan for observer scores and then exclude if any found
         */
        if  (isset($examinerWeightings[$stationID])) {

           $weightings = $examinerWeightings[$stationID];
           $stationScores = array_filter($stationScores, function($score) use ($weightings) {

              $examinerID = $score['examiner_id'];
              return (!isset($weightings[$examinerID]) || $weightings[$examinerID] != 0);

           });

        }

        $formPossibles = array_column($stationScores, 'total_weighted_value');
        $examPossible += $avgRule ? max($formPossibles) : array_sum($formPossibles);

    }

    return $examPossible;

 }

  /**
  * Self Assessment Question
  *
  * @param mixed[] $item          Item record
  * @param bool $selfAssessment   Is self assessment section
  * @param int $exam              Number identifier for exam
  * @param int $student           Number identifier for student
  *
  * return void
  */
  function selfAssessmentQuestion(&$item, $selfAssessment, $exam, $student)
  {
     
      if (!$selfAssessment || abortContentRender()) return;

      global $db;

      $question = $db->selfAssessments->getQuestion($exam, $item['item_order']);

      $response = $db->selfAssessments->getQuestionByOrderAndStudent(
        $exam, $item['item_order'], $student
      );

      if (!empty($question)) {

         $item['text'] = "<p>" . $question['question'] . "</p>";

      }

      if (!empty($response)) {

         $item['text'] .= "<p></p><p><b>"
            . gettext('Student') . " Response:</b> <i>\"" . $response['response'] . "\"</i></p>";
         $item['text'] .= "<p><b>"
            . gettext('Student') . " Score:</b> " . $response['score'] . "</p><p></p>";

      } else {

         $item['text'] .= "<p></p><p>no " . gettext('student')
                            . " response found</p><p></p>";

      }

  }
  

 /**
  * Render Fail/Pass Instructions for the student
  * @param \OMIS\Reporting\PDF $pdf  PDF Rendering object
  * @param $template           Record for template in use
  * @param $failGrade          Has student failed
  * @param mixed[] $examSummary   Exam summary data
  * @param mixed[] $examRules     Exam rules data
  *
  * return bool (true|false) instructions received (yes/no)
  */
 function renderPDFInstructions(&$pdf, $template, $examSummary, $examRules)
 {
    
    // Instructions enabled or (instructions on fail and student passed exam), then don't render!
    if ($template['pdf_instructions_enabled'] == "no" || 
       ($template['pdf_instructions_enabled'] == "fail" && passedExam($examSummary, $examRules)))
       return false;

    $pdf->AddPage();
    $pdf->SetFont('times', '', 8);

    // Space before content
    $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 3);

    /* NOTE: rewriting image paths e.g. storage/app/share/sample-sig.png 
    in the HTML content to please TCPDF :( */
    $pdf->writeHTML(
        tcpdfImagesPath($template['pdf_instructions'])
    );

    return true;

 }

 /**
  * Temp (quick) fix for locating the shared image directory. 
  * Need to have another look at this
  * @param $html         Original HTML text
  *
  * return html
  */
  function tcpdfImagesPath($html)
  {
     
    $searchRegEx = "(src=\"storage\/app\/share\/)";
    $replaceString = 'src="' . K_PATH_IMAGES . 'storage/app/share/';

    return preg_replace(
          $searchRegEx,
          $replaceString,
          $html
    );

  }

  /** (DC: THIS WILL BE REPLACED) Determine Overall student Pass/Fail grade
   * Stopgap function until we merge centralised stored grades implementation
   * & outcomes Re. RFC 18
   * @param mixed[] $examSummary         Exam summary data
   * @param mixed[] $examRules           Exam rules data
   */
   function passedExam($examSummary, $examRules) 
   {
       
      $failOnMinStationsPass = false;
      
      // Count number of passed stations
      $stationsPassed = array_reduce($examSummary, function ($count, $each) {

            if (Results::determinePassOrFail(
                $each['score'], 
                $each['station_pass'], 
                $each['station_possible']))
               $count++;

         return $count;

      }, 0);

      // Overall Passed/Failed Outcome 
      $failOnMinStationsPass = is_numeric($examRules['min_stations_to_pass']) && $stationsPassed < $examRules['min_stations_to_pass'];

      $passedByScore = Results::determinePassOrFail(
           array_sum(array_column($examSummary, 'score')), 
           (array_sum(array_column($examSummary, 'station_pass')) / count($examSummary)), 
           array_sum(array_column($examSummary, 'station_possible'))
      );

      return ($failOnMinStationsPass ? false : $passedByScore);

   }

   /**
    * Generate Feedback Ready
    * @param string $feedbackFolder   Path to feedback folder
    * @param mixed[] $exam            Exam data
    * @param mixed[] $template        Template data
    */
   function generateFeedbackReady($feedbackFolder, $exam, $template) 
   {

        global $db; 

        // Prepare Feedback Folder
        if (!is_dir($feedbackFolder)) {
        
            if (!mkdir($feedbackFolder)) {
            
                error_log(__FILE__ . ": Could not create folder $feedbackFolder. Exiting.");
                return false;
            }
            
        }

        if (empty($exam)) {
        
            error_log(__FILE__ . ": Exam does not seem to exist in order to send feedback. Exiting.");
            return false;
            
        }

        if (empty($template)) {
        
            error_log(__FILE__ . ": Template does not seem to exist in order to send feedback. Exiting.");
            return false;
            
        }

        // Delete feedback records
        if (!$db->feedback->deleteFeedbackRecords($exam['exam_id'])) {
        
            error_log(__FILE__ . ": Failed to delete feedback records for exam: {$exam['exam_id']} Exiting.");
            
        }

        return true;

   }

   /**
    * Configure PDF Document (Initiate Document Object)
    * @param string $mainTitle           PDF Document Main Title
    * @param string $innerTitle          PDF Document Inner Title
    */
   function configurePDFDoc($mainTitle, $innerTitle) 
   {

         global $config;
        
         // Create New PDF
         $pdf = new \OMIS\Reporting\PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
 
         // Author & Keywords
         $pdf->SetCreator(PDF_CREATOR);
         $pdf->SetAuthor('Qpercom LTD');
         $pdf->SetKeywords('Qpercom LTD, Objective, Structured, Clinical, Examinations');
 
         // Document Title & Subject
         $pdf->SetTitle('Feedback ' . $mainTitle);
         $pdf->SetSubject($innerTitle);
         $pdf->setHeaderFont(['times', '', 9]);
         $pdf->setFooterFont(['times', '', 9]);
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
         $pdf->SetMargins(2, 14, 2);
         $pdf->setCellHeightRatio(1.25);
         $pdf->SetHeaderMargin(1);
         $pdf->setFooterMargin(PDF_MARGIN_FOOTER);
         $pdf->setPrintFooter(true);
         $pdf->SetAutoPageBreak(true, 0);
         $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
         $pdf->setLanguageArray($config->tcpdf_environment['language']);
         $pdf->setFontSubsetting(false);
         $pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
         
         $pdf->setHtmlVSpace(array(
            'p' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => 0.0001, 'n' => 1)),
            'ul' => array(0 => array('h' => 0.0001, 'n' => 1), 1 => array('h' => '', 'n' => -1)),
            'ol' => array(0 => array('h' => '', 'n' => -1), 1 => array('h' => '', 'n' => -1))
        ));
 
         $pdf->SetHeaderData(
             $config->tcpdf_environment['logo']['path'],
             $config->tcpdf_environment['logo']['width_mm'], 
             $mainTitle,
             $innerTitle
         );
 
         // Abort adding new page if we're not going to add content
         if (!abortContentRender()) $pdf->AddPage();

         return $pdf;

   }

  /**
   * Render station bottom (empty cell + border)
   * @param \OMIS\Reporting\PDF $pdf  PDF Rendering object
   */   
  function renderStationBottom($pdf) 
  {

        if (abortContentRender()) return;

        // Draw blank cell with a border as a separator.
        $pdf->Multicell(
            $pdf->getAvailableWidth(), 0, '', 'T', 'C',
            false, 1, '', '', true, 0, false, false, 0,
            'M', true
        );

  }

  /**
   * Abort form content rendering
   */   
  function abortContentRender() 
  {

     global $displayForms;

     return (isset($displayForms) && !$displayForms);

  }


