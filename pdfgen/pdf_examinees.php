<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

require_once __DIR__ . "/../extra/essentials.php";
use OMIS\Utilities\Path as Path;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

require_once "pdf_settings.php";

/****************************[Prepare for rendering]*********************************/
if (!isset($canContinue) || !$canContinue) {
    return false;
}

// Set auto break on (Override pdf_settings.php)
$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);

// Initial variables
$yearValue = "ALL YEARS";

// Get Examinee Data
$examinees = $db->students->searchStudents(
    $course,
    $year,
    $module,
    $search,
    "NL",
    "NL",
    $orderMap[$orderIndex],
    $orderType,
    $_SESSION["cterm"]
);

$examineeCount = mysqli_num_rows($examinees);
$courseRecord = $db->courses->getCourse($course);
$courseName = $courseRecord["course_name"];
if ($year != "") {
    $yearRecord = $db->courses->getCourseYear($year);
    $yearValue = $yearRecord["year_name"];
}

/****************************[PDF File Name]**************************************/

// PDF file name format
$pdfNameFormat = "list_".$course.".pdf";

// PDF file name before sanitization
$pdfFileNameRaw = sprintf($pdfNameFormat, $course);

// Remove illegal characters from the file name
$pdfFileName = preg_replace('/[^0-9a-z\.\_\-]/i','', $pdfFileNameRaw);

$pdfPath = Path::join(
     $config->paths["temp"],
     $pdfFileName
);

if (file_exists($pdfPath)) {
    unlink($pdfPath);
}

/***************************[Prepare PDF]*****************************************/
// Prepare Titles
$mainTitle = gettext('Student') . " List: " . $courseName;
$subTitle = "Year: " . $yearValue;

// Document Title & Subject
$pdf->SetTitle($mainTitle);
$pdf->SetSubject($subTitle);
$pdf->SetMargins(2, 13, 2);

// Set default header data
$pdf->SetHeaderData($pdf_logo, $logo_width_mm, $mainTitle, $subTitle);
$pdf->AddPage();
$pdf->setCellMargins(0, 0, 0, 0);
$pdf->setCellPaddings(1, 1, 1, 1);

/****************************[Render data]******************************/
// Examinee titles
$pdf->SetFont("times", "b", 8);

// Define column headings and their spacings
$columns = [
    "Identifier" => 24,
    "Candidate #" => 18,
    "Surname" => 43,
    "Forenames" => 40,
    "DOB" => 16,
    "Gender" => 14,
    "Nat." => 12,
    "Email" => 48,
    "Year" => 9
];

// Add candidate column if feature enabled
if ($candidateNumberFeature) {
   $columns["Email"] -= 6;
   $columns["Surname"] -= 6;
   $columns["Forenames"] -= 6;
} else {
   array_splice($columns, 1, 1); 
}

// Render column headings.
foreach ($columns as $key => $value) {
    @$eol = ($key == end(array_keys($columns))) ? 1 : 0;
    $pdf->Multicell($value, 0, $key, "RL", "C", true, $eol, "", "", true, 0, false, false, 0, "M", true);
}

// Examinee rows
if ($examineeCount > 0) {
   while ($examinee = $db->fetch_row($examinees)) {

      $examineeID = $examinee["student_id"];
      $pdf->SetFont("times", "b", 8);
      $pdf->Multicell($columns["Identifier"], 0, $examineeID, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->SetFont("times", "", 8);
      
      if ($candidateNumberFeature) {
        $candidateNumber = $examinee["candidate_number"];
        $pdf->Multicell($columns["Candidate #"], 0, $candidateNumber, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
      }
      
      $surname = mb_substr($examinee["surname"], 0, 20, "UTF-8");
      $forenames = mb_substr($examinee["forename"], 0, 20, "UTF-8");
      $dob = formatDate($examinee["dob"]);
      $gender = $examinee["gender"];
      $nationality = mb_substr($examinee["nationality"], 0, 2, "UTF-8");
      $email = mb_substr($examinee["email"], 0, 40, "UTF-8");
      $yearLabel = $examinee["year_name"];
     
      $pdf->Multicell($columns["Surname"], 0, $surname, "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["Forenames"], 0, $forenames, "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["DOB"], 0, $dob, "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["Gender"], 0, $gender, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["Nat."], 0, $nationality, "TRL", "C", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["Email"], 0, $email, "TRL", "L", false, 0, "", "", true, 0, false, false, 0, "M", true);
      $pdf->Multicell($columns["Year"], 0, $yearLabel, "TRL", "C", false, 1, "", "", true, 0, false, false, 0, "M", true);
      
   }
}

// Bottom line
$pdf->Multicell(206, 12, "", "T", "C", false, 1, "", "", true, 0, false, false, 0, "M", true);

// Close and output PDF document
$pdf->Output($pdfPath, "F");

// Get the relative path to the tmp directory
$relPathDir = $config->getRelativePath("tmp_path");

// Glue the file name to the tmp path
$completePath = $relPathDir . "/" . $pdfFileName;

// Forward slashes only as it's a url path
$pdfUrl = Path::setSeparator($completePath);
