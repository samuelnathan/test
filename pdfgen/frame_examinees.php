<?php
/**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  * PDF Examinees, loaded using an iframe with option panel
  */
require_once __DIR__ . "/../extra/essentials.php";

// Critical Session Check
$session = new OMIS\Session;
$session->check();
 
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
  return false;
}

// Get the course param which is required
$course = filter_input(INPUT_GET, "c", FILTER_SANITIZE_STRING);

// Abort if course param does not exist in system
if (!$db->courses->doesCourseExist($course)) {
  return false;
}

// Can continue with PDF processing?
$canContinue = true;

// default index : students.updated_at
$defaultSortIndex = 9;

// Candidate number feature
$candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);

// Remaining passed in values
$year = filter_input(INPUT_GET, "y", FILTER_SANITIZE_NUMBER_INT);
$module = filter_input(INPUT_GET, "m", FILTER_SANITIZE_STRING);
$search = filter_input(INPUT_GET, "s", FILTER_SANITIZE_STRING);

// Prepare Ordering
$fields = [
    "students.student_id",
    "students.surname",
    "students.forename",
    "students.dob",
    "students.gender",
    "students.nationality",
    "students.email",
    "course_years.year_name",
    "students.updated_at"
];

// Add column if feature enabled
if ($candidateNumberFeature) {
    array_splice($fields, 1, 0, ["candidate_numbers.candidate_number"]);
    $defaultSortIndex++;
}

list($orderType, $orderIndex, $orderMap) = prepareListOrdering("desc", $defaultSortIndex, $fields);

// Return Variables
$formVars = [
    ["name" => "page", "value" => "preview_examinees"],
    ["name" => "c", "value" => $course],
    ["name" => "y", "value" => $year],
    ["name" => "m", "value" => $module],
    ["name" => "s", "value" => $search],
    ["name" => "o", "value" => $orderType],
    ["name" => "i", "value" => $orderIndex]
];
require_once __DIR__ . "/../pdfgen/pdf_examinees.php";

// Include course name and year value has hidden variables
if (isset($courseName)) {
  $formVars[] = ["id" => "course-name", "value" => $courseName];
}

if (isset($yearValue)) {
  $formVars[] = ["id" => "year-value", "value" => $yearValue];
}

?>
<form id="back" action="manage.php" method="get">
<?php
foreach ($formVars as $input) {
    $hiddenInput = ' <input type="hidden"';
    if (count($input) > 0) {
      foreach ($input as $key => $value) {
         $hiddenInput .= ' ' . $key . '="' . $value . '"';
      }
    }
    $hiddenInput .= "/>\n";
    echo $hiddenInput;
}

// Append current timestamp to the filename.
$pdfUrl .= "?t=" . date('sihymd');

?>
<input type="submit" id="backb" value="Click to Return"/>
</form>
<iframe src="<?=$pdfUrl?>" id="pdframe" width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0">
    <p>Your browser does not support iframes.</p>
</iframe>
