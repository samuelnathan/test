<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined("_OMIS") or die("Restricted Access");
define("RETURN_AS_ARRAY", true);
defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . "/../vendor/autoload.php";
}

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

// Assume session doesn't exist by default
$sessionExists = false;

// Other defaults
$examSettings = [];
$examinees = [];
$groups = [];
$deptID = "";
$deptName = "";
$examName = "";
$sessionDate = "";
$sessionDesc = "";

/**
 * Sanitize the session parameter and get the exam session record
 */
$sessionParam = filter_input(INPUT_GET, "session", FILTER_SANITIZE_NUMBER_INT);
if (strlen($sessionParam) > 0 && is_numeric($sessionParam)) {
   $sessionRecord = $db->exams->getSession($sessionParam);
   if (!is_null($sessionRecord)) {
       
      $sessionExists = true;
      $sessionDate = formatDate($sessionRecord["session_date"]);
      $sessionDesc = $sessionRecord["session_description"];
      $examName = $sessionRecord["exam_name"];
      $examSettings = $db->examSettings->get($sessionRecord["exam_id"]);
      $deptID = $sessionRecord["dept_id"];
      $deptName = $db->departments->getDepartmentName($deptID);
      
      // Get list of groups
      $groups = $db->exams->getSessionGroups(
           $sessionParam,
           RETURN_AS_ARRAY
      );
      
      foreach ($groups as $group) {
          $groupID = $group["group_id"];
          $examineeRecords = $db->exams->getStudentsAndBlanks(
                  $groupID, 
                  $sessionRecord["term_id"]
          );
          foreach ($examineeRecords as $examinee) {
             $examinees[$groupID][] = $examinee;
          }
      }
      
   }
}

// Template data
 $data = [
    "sessionExists" => $sessionExists,
    "sessionID" => $sessionParam,
    "sessionDate" => $sessionDate,
    "sessionDesc" => $sessionDesc,
    "examDesc" => ucfirst($examName),
    "examSettings" => $examSettings,
    "deptID" => $deptID,
    "deptName" => $deptName,
    "groups" => $groups,
    "examinees" => $examinees
 ];

 // Settings template      
 $template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__)); 
 $template->render($data);
