<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once 'pdf_settings.php';

// Passed in parameters
$todo = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING);
$examinersSelected = filter_input(INPUT_POST, 'examiner_check', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
$examinerDataParam = filter_input(INPUT_POST, 'examiners_data', FILTER_SANITIZE_STRING);
$deptSelected = filter_input(INPUT_POST, 'dept_sel', FILTER_SANITIZE_STRING);

if (is_null($todo) || $todo != 'exportpdf' || is_null($examinersSelected) || is_null($examinerDataParam)) {
    return false;
}

// Spacing Array
$spacing = [
    'identifier' => 27,
    'forenames' => 30,
    'surname' => 40,
    'email' => 50,
    'contact' => 21,
    'user_role' => 22,
    'training' => 16
];

// Examiners Data
$examinerData = unserialize(gzuncompress(base64_decode($examinerDataParam)));

// Time Stamp
$timeStamp = date('dmyHis');

/***************************[Prepare PDF]***************************************/
// Prepare Titles
$mainTitle = 'Exam Team List';

// Subtitle = Department Name
if (strlen($deptSelected) > 0 && $db->departments->existsById($deptSelected)) {
  $subTitle = $db->departments->getDepartmentName($deptSelected);
} else {
  $subTitle = "";  
}

// Document Title & Subject
$pdf->SetTitle($mainTitle);
$pdf->SetSubject($subTitle);

// Set auto page breaks 
$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);

// Set default header data
$pdf->SetHeaderData($pdf_logo, $logo_width_mm, $mainTitle, $subTitle);
$pdf->AddPage();
$pdf->setCellMargins(0, 0, 0, 0);
$pdf->setCellPaddings(1, 1, 1, 1);
$pdf->SetFont('times', 'b', 8);

/* * **************************CYCLE THROUGH DATA******************************* */
// EXAMINER TITLES
$pdf->Multicell($spacing['identifier'], 4, 'Identifier', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['forenames'], 4, 'Forenames', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['surname'], 4, 'Surname', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['email'], 4, 'Email Address', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['contact'], 4, 'Contact #', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['user_role'], 4, 'User Role', 'TRL', 'C', true, 0, '', '', true, 0, false, false, 0, 'M', true);
$pdf->Multicell($spacing['training'], 4, 'Training', 'TRL', 'C', true, 1, '', '', true, 0, false, false, 0, 'M', true);

// Keep a list of all of the Roles we've come across.
$roles = [];

// Examiner rows 
$pdf->SetFont('times', '', 8);
foreach ($examinersSelected as $examinerID) {
    // Data Row
    $data = $examinerData[$examinerID];
    $pdf->Multicell($spacing['identifier'], 7, adjustHTML($examinerID), 'TRL', 'L', false, 0, '', '', true, 0, false, false, 0, 'M', true);
    $pdf->Multicell($spacing['forenames'], 7, adjustHTML($data['forename']), 'TRL', 'L', false, 0, '', '', true, 0, false, false, 0, 'M', true);
    $pdf->Multicell($spacing['surname'], 7, adjustHTML($data['surname']), 'TRL', 'L', false, 0, '', '', true, 0, false, false, 0, 'M', true);
    $pdf->Multicell($spacing['email'], 7, adjustHTML($data['email']), 'TRL', 'L', false, 0, '', '', true, 0, false, false, 0, 'M', true);
    $pdf->Multicell($spacing['contact'], 7, adjustHTML($data['contact_number']), 'TRL', 'C', false, 0, '', '', true, 0, false, false, 0, 'M', true);

    // (DW) Make sure we have the name of the row, not its ID.
    $roleID = $data['user_role'];
    $thisRole = null;
    // Check if we know it already.
    foreach ($roles as $role) {
        if ($role->role_id == $roleID) {
            $thisRole = $role;
            break;
        }
    }
    // If not, load it and add it to the list of known Roles.
    if (is_null($thisRole)) {
        $thisRole = OMIS\Auth\Role::loadID($roleID);
        $roles[] = $thisRole;
    }

    $pdf->Multicell($spacing['user_role'], 7, gettext(adjustHTML($thisRole->name)), 'TRL', 'C', false, 0, '', '', true, 0, false, false, 0, 'M', true);
    $pdf->Multicell($spacing['training'], 7, adjustHTML($data['trained']), 'TRL', 'C', false, 1, '', '', true, 0, false, false, 0, 'M', true);
}

// Bottom line
$pdf->Multicell(206, 12, '', 'T', 'C', false, 1, '', '', true, 0, false, false, 0, 'M', true);

// Department file name
if (strlen($deptSelected) > 0) {
  $deptFileName = $deptSelected . '_';   
} else {
  $deptFileName = "";
}

// Close and output PDF document
$pdf->Output( gettext('Exam') . '_Team_' . $deptFileName . $timeStamp . '.pdf', 'D');
