<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
	
require_once __DIR__ . '/../extra/essentials.php';

// Critical Session Check
$session = new OMIS\Session;
$session->check();
 
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Print Session Station Forms
if ($page == 'pdfstations' && isset($_REQUEST['session']) && $db->exams->doesSessionExist($_REQUEST['session'])) { 
    $session_id = trim($_REQUEST['session']); #Session ID
    $sessionDB = $db->exams->getSession($session_id);
    $stations = $db->exams->getStations([$session_id]);

    $sta = (isset($_POST['sta']) && $db->exams->doesStationExist($_POST['sta'])) ? trim($_POST['sta']) : ''; #Stations or Station to Output  
    $act = (isset($_POST['act'])) ? trim($_POST['act']) : 1; #Action to Take
    $mar = (isset($_POST['mar'])) ? trim($_POST['mar']) : 0; #Show Marks True or False
    $examTitle = (isset($_POST['examtitle'])) ? trim($_POST['examtitle']) : 0; #Show Exam Title True or False
    $feedbackArea = (isset($_POST['fa'])) ? trim($_POST['fa']) : 0;     // Show feedback text area
    $feedbackLines = (isset($_POST['fh'])) ? trim($_POST['fh']) : 0;    // Number of lines of the feedback text area

    $num_stations = mysqli_num_rows($stations); #Station Count
    $sel_station_cnt = (strlen($sta) > 0) ? 1 : $num_stations;
    $predicted_form_cnt = ($act == 1) ? $sel_station_cnt : $db->exams->getStudentCountInSession($session_id) * $sel_station_cnt; #Predicted Count
    ?><form id="pdform" action="manage.php" method="post">
	   <div id="opt-div">
		<input type="hidden" id="sessioninfo" value="<?=formatDate($sessionDB['session_date'])." ".$sessionDB['session_description']?>"/>
		<input type="hidden" name="page" value="pdfstations"/>
		<input type="hidden" name="session" value="<?=$session_id?>"/>
		<select id="act" name="act">
		  <option value="1">Template <?=ucwords(gettext('forms'))?></option>
		  <option value="2" <?php if($act == 2){ ?>selected="selected"<?php } ?>>Individual <?=ucwords(gettext('student'))?> <?=ucwords(gettext('forms'))?></option>
		</select>
	    <select id="examtitle" name="examtitle">
		  <option value="0">Hide <?=gettext('OSCE')?> Name &amp; Date</option>
		  <option value="1" <?php if($examTitle == 1){ ?>selected="selected"<?php } ?>>Show <?=gettext('OSCE')?> Name &amp; Date</option>
		</select>
		<select id="smarks" name="mar">
		  <option value="0">Hide Item Marks</option>
		  <option value="1" <?php if($mar == 1){ ?>selected="selected"<?php } ?>>Show Item Marks</option>
		</select>
       <select id="feedback-area" name="fa" title="Hide/Show the feedback area below the sections where feedback is enabled">
           <option value="0">Hide Feedback Area</option>
           <option value="1" <?php if($feedbackArea == 1){ ?>selected="selected"<?php } ?>>Show Feedback Area</option>
       </select>
       <fieldset id="feedback-height-fieldset" style="display:<?php if($feedbackArea == 1){ ?>inline<?php } else { ?>none<?php } ?>;border: none;">
           <label for="feedback-height" title="Height of the feedback area in lines">Number of lines:</label>
           <input id="feedback-height" name="fh" type="number" value="<?php if ($feedbackLines == 0) { ?>2<?php } else { echo $feedbackLines; } ?>" min="1" max="10" />
       </fieldset>
		<select id="sta" name="sta">
		 <option value="">ALL STATIONS</option><?php 
		   while($st = $db->fetch_row($stations)){
			 ?><option value="<?=$st['station_id']?>" <?php if($sta == $st['station_id']){ ?>selected="selected"<?php } ?>><?=$st['station_number']." - ".$st['form_name']?></option><?php
		   }
		?></select>
		<input type="submit" id="gen" name="gen" value="Generate PDF" class="btn btn-success btn-sm"/>
	   </div>
	 </form>
        <?php
	 if (isset($_POST['gen'])) {
	   if($num_stations > 0 && $predicted_form_cnt > 0){
	       $src = "pdfgen/pdf_stations.php?"
                    . "session=" . $session_id
                    . "&amp;act=" . $act
                    . "&amp;sta=" . $sta
                    . "&amp;examtitle=" . $examTitle
                    . "&amp;mar=" . $mar
                    . "&amp;fa=" . $feedbackArea
                    . "&amp;fh=" . $feedbackLines
                    . "&amp;t=" . date('sihymd');
	   ?><div id='pdfwait'>Please Wait...... <span>[<span class="red">producing <?=$predicted_form_cnt . " " . ngettext('form', "forms", $predicted_form_cnt)?></span>]</span></div>
	     <iframe src="<?=$src?>" id="pdframe-bulk" onreadystatechange="removeWait()" width="100%" height="100%" frameborder="0" marginheight="0" marginwidth="0">
           <p>Your browser does not support iframes.</p>
         </iframe><?php
	   } else if($num_stations == 0){
	       ?><div class="bold">[<span class="red">Session has no Stations</span>]</div><?php
	   } else if($predicted_form_cnt == 0){
	       ?><div class="bold">[<span class="red">No <?=ucwords(gettext('forms'))?> to Export</span>]</div><?php
	   }
	}
 }
