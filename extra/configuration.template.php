<?php
use \OMIS\Utilities\Path as Path;

/**
 * This file contains configuration settings for Observe. It should not be called
 * directly but instead should be included by /classes/Config.php.
 */
defined('_OMIS') or die('Restricted Access');

// Only load .env file(s) for development environments
if (!in_array(getenv('APP_ENV'), ['production', 'staging']))
  (Dotenv\Dotenv::create(__DIR__)->load());

/**
 * Database connection settings
 * docker compose dev mode ip: 172.17.0.1
 */
$this->db = array(
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'host' => getenv('DB_HOST'),
    'schema' => getenv('DB_DATABASE'),
    'couple_depts_courses' => false
);


/*
 * E-Mail Server settings for sending e-mail.
 * Currently only used for password resets.
 */
$this->mailserver = [
    'host' => 'qpersoft.com',
    'port' => 587,
    'username' => 'noreply@qpersoft.com',
    'password' => '',
    'from' => [
        'name' => 'Support Team',
        'email' => 'noreply@qpersoft.com'
    ]
];

/* Style sheet & localization options.
 * Set the appropriate values for the stylesheet and whether a logo should be
 * displayed below, if they're different from the default.
 * Known styles at the time of writing of this comment:
 * "reg", "nus", "nus_training", "psy".
 * Actual CSS is stored in css/style_<style_name>.css
 * @TODO: Write a tool to check if a stylesheet exists for this, perhaps?
 */
$this->localization = array(
    'skin' => array(
        'name' => getenv('SKIN_NAME'),                // $config->style
        'app_name' => getenv('SKIN_APP_NAME'),           // $config->login_app_name
        'app_name_full' => getenv('SKIN_APP_NAME_FULL'),
        'icon_filename' => getenv('SKIN_ICON_FILENAME'),       // $config->icon_filename (including extension) location assets/images
        'primary_color' => getenv('SKIN_PRIMARY_COLOR'),
        'login' => array(
          'fields_title' => getenv('SKIN_LOGIN_FIELDS_TITLE'),        // $config->fields_title
          'title_text' => getenv('SKIN_LOGIN_TITLE_TEXT'),                 // $config->login_titlebar_text
          'title_colour' => getenv('SKIN_LOGIN_TITLE_COLOUR')        // $config->login_titlebar_fontcolour
        )     
    ),
    'client' => array (
        'full_name' => getenv('CLIENT_FULL_NAME'),
        'analyse_url' => getenv('CLIENT_ANALYSE_URL') // url for analyse
    ),
    'language' => getenv('LOCALE_LANGUAGE'),                 // $config->lang
    'country' => getenv('LOCALE_COUNTRY'),                  // $config->country_iso_default
    'domain' => getenv('LOCALE_DOMAIN'),              // Domain or name of GetText Portable Object (PO) file that contain the custom translations
    'timezone' => getenv('LOCALE_TIMEZONE'),      // $config->timezone - refer to https://developers.google.com/adwords/api/docs/appendix/timezones
    'pagination' => array(
        'items_per_page' => 10          // $config->exmr_page_len
    ),
    'modes' => array(
        'production' => false,          // $config->prod_mode
        'offline' => false,             // $config->offline (No internet connection, LAN web server)
        'debug' => true                 // $config->debug_mode
    ),
    'instance_name' => getenv('INSTANCE_NAME') // $config->instance_name
);

/**
 * System Notifications
 * Fill out the parameters in relation to the clients details and what application they will be using (recruit or observe) 
 * Set if the notification system is enabled or disabled for the entire application for all users
 */

$this->systemNotifications = [
    'enabled' => true,                  //$systemNotifications->enabled
    'client_code' => 'nuig',            //$systemNotifications->client_code    
    'client_name' => 'nuig',            //$systemNotifications->client_name
    'mode' => 'recruit'                 //$systemNotifications->mode
];

$this->paths = [
    'logs' => "storage/logs",
    'twig' => "compilation_cache",
    'templates' => "templates",
    'temp' => "storage/app/tmp",
    'borderline_templates' => "files/export_templates",  // Customised versions => files/export_templates/customised
    'import_files' => 'storage/custom/import',
    'manuals' => 'storage/custom/manuals'
];

/**
 * Content Delivery Network CDN Settings
 */
$this->cdn = [
    'cloudflare' => [
        'url' => 'https://cdnjs.cloudflare.com',       // $config->url
        'fallback_local' => 'cdn_fallback/cloudflare'  // $config->fallback_copy
    ]
];

$this->page_titles = [
    'default' => 'Qpercom | Observe',
    'assessment' => 'Qpercom | Observe'
];

/**
 * TCPDF Environment settings.
 */
$this->tcpdf_environment = array(
    'language' => array(
        'a_meta_charset' => 'UTF-8',
        'a_meta_dir' => 'ltr', # Left-To-Right, not "US Letter"!
        'a_meta_language' => 'en',
        'w_page' => 'page'
    ),
    'page_format' => 'A4',
    'logo' => array(
        'path' => Path::join('storage/logo', 'sample.png'),
        'width_mm' => 11
    )
);

/* Default Station settings.
 * - 'passvalue' is the default pass percentage (%) for a station.
 * - 'timelimit' is the default time limit (in hh:mm:ss format) for a station.
 * - 'visualalarm1' default visual (flashing) alarm time 1 (in hh:mm:ss format) for a station.
 * - 'visualalarm2' default visual (flashing) alarm time 2 (in hh:mm:ss format) for a station.
 * - 'alarmduration' alarm duration seconds (both visual alarm 1 and 2)
 * - 'popupnotes' is a boolean stating whether the notes should pop up
 *   automatically by default (Introduced in OMIS 1.9)
 *   Optional parameters:
 * - 'compulsory_feedback' (if set) sets whether the default feedback settings
 *   for new stations is set or not. Set to one of the
 *   \OMIS\Config::FEEDBACK_GRS_* constants.
 * - 'grs_scale' (if present) sets a default value for which GRS scale will be chosen.
 *   This value must be a number that matches a GRS Type ID field in Main > Admin Tools > Manage GRS Types.
 * - 'grace_percent' What grace interval (if any) should be applied to the pass grade.
 */
$this->station_defaults = [
    'passvalue' => 70,
    'timelimit' => '00:05:00',
    'visualalarm1' => '00:02:00',
    'visualalarm2' => '00:00:00',
    'alarmduration' => '10',
    'show_notes' => true,
    'compulsory_feedback' => \OMIS\Config::FEEDBACK_GRS_FAIL,
    'grs_scale' => 2,
    'grace_percent' => 0.0
];

/* Default Exam settings.
 * The exam settings below contain the results column defaults (assessment tool) when creating and exam
 * and the setting: 'view_results' is enabled
 */
$this->exam_defaults = [
    'result_columns' => [
        'identifier' => [
            'title' => "identifier",
            'enabled' => true
        ],
        'surname' => [
            'title' => "surname",
            'enabled' => true
        ],
        'forename' => [
            'title' => "forenames",
            'enabled' => true
        ],
        'session' => [
            'title' => "session",
            'enabled' => true
        ],
        'date' => [
            'title' => "date",
            'enabled' => true
        ],
        'group' => [
            'title' => "group",
            'enabled' => true
        ],
        'form' => [
            'title' => "station",
            'enabled' => true
        ],
        'number' => [
            'title' => "number",
            'enabled' => true
        ],
        'examiner' => [
            'title' => "examiner",
            'enabled' => true
        ],
        'pass' => [
            'title' => "pass %",
            'enabled' => true
        ],
        'score' => [
            'title' => "score / possible",
            'enabled' => true
        ],
        'percentage' => [
            'title' => "score %",
            'enabled' => true
        ],
        'grs' => [
            'title' => "grs",
            'enabled' => true
        ]],

    /**
     * Which type of identifier to display during assessment:
     * NOTE: codes are case sensitive
     *  S => Student Identifier (default)
     *  E => Exam Number
     *  C => Candidate Number
     */
    'exam_student_id_type' => 'S',
    'show_countdown' => true,
    'preserve_group_order' => false,
    'feedback_to_right' => false,
    'default_text_scale' => "medium",
    /* options: smallest, very small, small, medium,
     large, very large, largest */
    'multiple_candidate' => false,

];

/* Analysis section defaults
 * Default values for the results & analysis section e.g. options panel
 * 'hide_combine_by_number' - hide 'combine_by_number' from drop-down
 * 'combine_by_number' - set as default in the combine stations by number drop-down
 * 'student_order' - display student order column by default
 */
$this->analysis_defaults = [
    'combine_by_number' => true,
    'hide_combine_by_number' => false,
    'student_order' => false,
    'alerts_tab' => false
];

/* Exam Rules Defaults
 * Default values for the exam rules, configurable in analysis exam rules tab
 * 'multi_examiner_results_averaged' - multi examiner results averaged (1 form per station)
 * more to add...
 */
$this->exam_rules = [
    'multi_examiner_results_averaged' => true
];

/* Settings related to the image cache for student records.
 * - 'maxsize' is the maximum size (in bytes) any image can be.
 * - 'thumbnail.width' is the target width (in pixels) for thumbnails being
 *   generated.
 * - 'thumbnail.height' is the target height (in pixels) for thumbnails
 *   being generated. The system will attempt to create the largest
 *   thumbnail that will fit within these dimensions.
 */
$this->imagecache = array(
    'maxfilesize' => 204800,
    'thumbnail' => array(
        'width' => 30,
        'height' => 40
    ),
    'images' => array(
        'default' => 'default.jpg', // These will be in /images/caches.
        'error' => 'error.jpg'
    ),
    'caches' => array(
        'students' => '/storage/cache/student_images'
    )
);

/**
 * E-mail sending app paths for various platforms.
 * Services: .net services engine harness for student feedback tool
 * Refer to tools/email_feedback.php for settings application
 *
 * Note: Reply-to settings 'StudentFeedbackJob.ReplyToEmailAddress' and 'StudentFeedbackJob.ReplyToFriendlyName'
 * MUST exist (not necessarily complete) in the .NET harness config file so that the Reply-to
 * settings below can override them.
 */
$this->email = [
    'services' => [
        'windows' => '',
        'linux' => 'mono /opt/Qpercom/Opercom.OMIS.ServicesEngine.clientHarness.exe',
        'from' => 'feedback@qpersoft.com', // If set, overrides from address parameter (user's email address)
        'from_name' => '',
        'reply' => '', // If set, overrides reply address parameter (user's email address)
        'reply_name' => ''
    ],
    'from_address' => 'noreply@qpersoft.com'
];

/**
 * Feedback Tool Settings (See issue #265 for reference) NSHCS - Feedback Customisation
 */
$this->feedback_tool = array(
    'summary_table_title' =>  "Exam Summary Table", // Feedback summary table title
    'default_grs_text' => "Global judgement", // Default GRS Text (global judgement instructional text)
    'validate_recipient_emails' => true // Validate recipient emails before sending
);

/**
 * Login attempts settings
 * - 'attempts' is the number of attempts the user is locked out.
 * - 'timeout' is the lock out period (in minutes) if a user takes too many
 *   attempts to log in.
 */
$this->login = array(
    'attempts' => 5,    // $config->attempts_g
    'timeout' => 5      // $config->time_delay_g
);


/* Password-related settings
 * JQuery plugin - Bootstrap 2 Password Strength Meter
 * https://github.com/ablanco/jquery.pwstrength.bootstrap
 *
 * Plugin Specific Settings:
 * - 'minChar' sets the minimum required of characters
 *      for a password to not be considered too weak.
 * - 'raisePower' the value used to modify the final score, based
 *      on the password length, allows you to tailor your results.
 * - 'rules' it's possible to activate or deactivate rules and set
 *      their score values.
 *
 * Other Settings:
 * - 'minStrength' the minimum strength to pass password validation
 *      5 options: ['weak', 'normal', 'medium', 'strong', 'very strong'] not advisable to use 'weak'.
 * - 'hash_cost' denotes the algorithmic cost that should be used when hashing the
 *      password in the database. Refer to http://php.net/manual/en/function.crypt.php for more info.
 *
 */
$this->passwords = [
    'minChar' => 6,
    'raisePower' => 1.6,
    'rules' => [
        'activated' => [
            'wordNotEmail' => true,
            'wordLength' => true,
            'wordSimilarToUsername' => true,
            'wordSequences' => true,
            'wordTwoCharacterClasses' => true,
            'wordRepetitions' => true,
            'wordLowercase' => true,
            'wordUppercase' => true,
            'wordOneNumber' => true,
            'wordThreeNumbers' => true,
            'wordOneSpecialChar' => true,
            'wordTwoSpecialChar' => true,
            'wordUpperLowerCombo' => true,
            'wordLetterNumberCombo' => true,
            'wordLetterNumberCharCombo' => true
        ],
        'scores' => [
            'wordNotEmail' => -100,
            'wordLength' => -50,
            'wordSimilarToUsername' => -100,
            'wordSequences' => -50,
            'wordTwoCharacterClasses' => 2,
            'wordRepetitions' => -25,
            'wordLowercase' => 1,
            'wordUppercase' => 3,
            'wordOneNumber' => 3,
            'wordThreeNumbers' => 5,
            'wordOneSpecialChar' => 3,
            'wordTwoSpecialChar' => 5,
            'wordUpperLowerCombo' => 2,
            'wordLetterNumberCombo' => 2,
            'wordLetterNumberCharCombo' => 2
        ]],
    'minStrength' => 'medium',
    'hash_cost' => 10,
];

/* Settings for sending SMS via an online SMS service.
 * - 'country' is the ISO country code of the country being sent to
 * - 'library' is the mechanism/service being used. Should map to a class.
 * - 'username' and 'password' are obvious.
 *
 * Bulktext accepts sender ID e.g. Observe, Recruit etc as a parameter
 */
$this->sms = array(
    'country' => 'IE',          // $config->mobile_country_code (UPPERCASE e.g. IE)
    'library' => 'bulktext',    // $config->sms_library
    'username' => 'davec747',   // $config->sms_user
    'password' => 'cu48Idda'    // $config->sms_pass
);

/**
 *  TextMagic Sender ID e.g. Observe, Recruit etc must be specified in the TextMagic control panel
 */
/**
 * Other SMS username/password combinations in case they get lost somehow
 * qpercomsmsuk/4TizwHTnl
 * annickdermine/HeDka3SZg1HTQRH
 * QpercomSingapore2013/ABOeHgFcSe4VjEX
 */

/* @TODO (DW) This needs to be somewhere else. In some feature-management system,
 * preferably behing some i18n engine so that we can sell OMIS in multiple
 * languages. When we already have strings like this thatare injected
 * programmatically we'd be fools not to make use of it as the basis for our
 * initial internationalization efforts.
 */
$this->feature_disabled_message = 'This feature is disabled. Please contact Qpercom for more information';

/* Assessment configuration
 * - Autosave internal time
 * - Help pdf and video links
 * - Early warning system SMS
 */
$this->assessment = [
    'autosave_interval' => 10000,
    'help' => [
        'pdf' => '/storage/custom/manuals/Examiner Assessment Tool Manual Generic Version.pdf',
        'video' => ''
    ],
    'warning_system_sms' => false
];

/**
 * Specific settings for support features like pubble.io
 */
$this->support = [
    'pubble' => [
        'appID' => '21052',
        'identifier' => '21052',
        'source' => 'https://cdn.pubble.io/javascript/loader.js',
        'enabled' => true
    ]
];

/**
 * Settings for twitter on the home page
 */
$this->twitter = [
    'enabled' => true,
    'name' => 'qpercomobserve'
];

/**
 * Specific settings for the competency framework
 */
$this->competency_frameworks = [
    'competence_framework_id' => 2,
    'display_sublevels' => false // Display sub levels yes|no
];

/**
 * Compatibility settings
 */
$this->compatibility = [
    // Set the cut-off point for browsers Observe does not support.
    // Browsers less than or equal to the value set will not be supported.
    'browser' => [
        'ie' => 10,
        'chrome' => 58,
        'safari' => 10,
        'firefox' => 50,
        'opera' => 42
    ]
];

/**
 * Enable or disable Single Sign-On. If it's enabled, a configuration file
 * for the Service Provider must be located in extra/saml_settings.php, and a
 * template for the SSO login form must be located in
 * custom/templates/sso_form.html.twig
 **/
$this->ssoEnabled = false;

/**
 * Configuration of the Event Queue where the new exams and sessions are notified
 */
$this->eventQueue = [
    'enabled' => false,
    'host' => 'queue',
    'port' => 5672,
    'userName' => 'guest',
    'password' => 'guest',
    'queueName' => 'test',

    // Optional value. If present, must have the format #rrggbb
    'eventColour' => '#000000'
];

/**
 * Configuration of the notification system for the rotation monitor
 */
$this->monitorNotification = [
    'enabled' => false,
    'httpEndpoint' => 'http://monitor:8080',
    'wsEndpoint' => 'ws://localhost:8080',
    'apiKey' => '1234',
    'httpTimeout' => 1,

    'auth' => [
        'threshold' => 7
    ]
];

/**
 * Configuration of the audit trail format
 */
$this->auditTrail = [
    /**
     * Flag that specifies if the user is being logged as a separate field or as part of the log message
     */
    'userAsField' => true
];

/**
 * Early warning system configuration (return false = disabled)
 */
$this->earlyWarning = [
    /**
     * Optional. Predicate that, if satisfied, the exam will be check for inconsistencies
     * If this field is not present the checks listed in the 'checks' field will always be performed
     */
    'when' => function (array $exam, \Carbon\Carbon $startTime, \Carbon\Carbon $endTime, \Carbon\Carbon $now) {
    return false;
    },

    /**
     * Items in this array represent the checks to make against an exam. Possible
     * values are:
     *   - grs              Check inconsistencies in the Global Rating Scales of the exam scoresheets
     *   - emptyForms       Check if there are empty forms associated to the exam
     *   - emptyGroups      Check if there are empty groups associated to the exam
     *   - noGroups         Check if there are sessions with no groups associated to the exam
     *   - noStations       Check if there are sessions with no stations associated to the exam
     */
    'checks' => [
        'grs',
        'emptyForms',
        'emptyGroups',
        'noGroups',
        'noStations'
    ]
    ];


/**
 * Import facility specific settings
 * - password_by_file import password by .csv file rather than using the default password UI (default: false)
 */
$this->import = [
    "password_by_file" => false
];


/**
 * Import facility specific settings
 * - password_by_file import password by .csv file rather than using the default password UI (default: false)
 */
$this->import = [
    "password_by_file" => false
];

