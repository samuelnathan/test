<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2020, Qpercom Limited
  */
 header("Content-Type: text/html; charset=utf-8");
 // This file is only included, not invoked directly. Ensure it only runs then...
 defined("_OMIS") or die("Restricted Access");

 // Make sure we have a working autoloader without declaring it twice...
 global $loader;
 if (!isset($loader)) {
    $loader = require __DIR__ . "/../vendor/autoload.php";
 }

 /* 2014-08-19 (DW) extra/header_nocache.php was an unnecessary require call
 * when it's only used once and is so trivial. Merged in at the only point it's
 * called from below.
 */
 $uncachedPages = ["pdf_groups", "pdf_examinees"];
 if (in_array(strtolower(trim($page)), $uncachedPages)) {
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
 }

 require_once "extra/usefulphpfunc.php";

 global $config;
 global $page;

 use \OMIS\Database\CoreDB as CoreDB;

 global $db;
 if (!isset($db)) {
    $db = CoreDB::getInstance();
 }

 require_once "extra/critical_variables_check.php";

 // Assets Source
 $css_src = "assets/css";
 $js_src = "assets/js";
 $tiny_src = "assets/js/tinymce";

 //Style to use
 $style = $config->localization["skin"]["name"];
 $primary_color = $config->localization["skin"]["primary_color"];

 /*
  * If we are in offline mode (no internet connection), then
  * revert back to local copies of js and css from the CDN
  */
 if ($config->inOfflineMode()) {
   $fallback = $config->getCDNSetting("cloudflare", "fallback_local");
   $cloudFlareJS = $js_src . "/" . $fallback;
   $cloudFlareCSS = $css_src . "/" . $fallback;

 /*
  * If we are online (internet connection), then use the CDN
  */
 } else {
  $cloudFlareJS = $cloudFlareCSS = $config->getCDNSetting("cloudflare", "url");
 }

 $splitPageArray = preg_split("[_]", $page);

 if (count($splitPageArray) >= 2) {
    $pageFilter = end($splitPageArray);

    // First part of the page name split
    $firstSubName = $splitPageArray[0];
 } else {
    $pageFilter = $splitPageArray[0];
 }

 ?><!DOCTYPE html>
 <html lang="en">
 <head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
 <meta name="viewport" content="width=768"/>
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <?php
 // No cache meta for PDF pages
 require "./extra/meta_nocache.php";
 ?>
 <title><?=$config->page_titles["default"]?></title>
 <link rel="icon" href="favicon.ico"/>
     <script>
         var $buoop = {
             vs: {
                 i:<?=$config->compatibility['browser']['ie'] ?>,f:<?=$config->compatibility['browser']['firefox'] ?>,o:<?=$config->compatibility['browser']['opera'] ?>,s:<?=$config->compatibility['browser']['safari'] ?>,c:<?=$config->compatibility['browser']['chrome']?>
             },
             api:4,
             reminder: 0
         };
         function $buo_f(){
             var e = document.createElement("script");
             e.src = "//browser-update.org/update.min.js";
             document.body.appendChild(e);
         }
         try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
         catch(e){window.attachEvent("onload", $buo_f)}
     </script>

 <?php

 include_External_File("global-admin-manage", "css", $css_src);
 include_External_File("global-custom", "css", 'storage/custom/css');
 include_External_File("styling_" . $style, "css", $css_src);
 include_External_FILE("flash", "css", $css_src);

 // Load jQuery framework. Loading whatever we can from CDNJS for speed.
 // jQuery 3.2.1 and jQuery ui 1.12.1 framework
 include_External_File(
    "jquery.min",
    "js",
    $cloudFlareJS . "/ajax/libs/jquery/3.2.1/"
 );

 include_External_File(
    "jquery-ui.min",
    "css",
    $cloudFlareCSS . "/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/"
 );

 include_External_File("jqueryui_fixwidgets", "css", $css_src);

 include_External_File(
    "jquery-ui.min",
    "js",
    $cloudFlareJS . "/ajax/libs/jqueryui/1.12.1/"
 );

 include_External_File(
    "popper.min",
    "js",
    $cloudFlareJS . "/ajax/libs/popper.js/1.14.6/umd/"
 );

 include_External_File(
    "tinycolor.min",
    "js",
    $cloudFlareJS . "/ajax/libs/tinycolor/1.4.1/"
 );

 include_External_File(
    "bootstrap.min",
    "js",
    $cloudFlareJS . "/ajax/libs/twitter-bootstrap/4.2.1/js/"
 );

 include_External_File("bootstrap.min", "css", $css_src);
 include_External_File("bootstrap-helper", "css", $css_src);
 include_External_File("css-vars-ponyfill", "js", $js_src);
 include_External_File("bootstrap-helper", "js", $js_src);
 
 /**
 * better scroll management using jQuery plugin
 */
 include_External_File("jquery-scrollscope", "js", $js_src);

 // Global Functions
 include_External_File("global-helper-functions", "js", $js_src);

 // Menu 
 include_External_File("menu-administrate", "css", $css_src);

 // IE related hacks :(
 include_External_File("ie_manage", "css", $css_src, "IE");

 // Switch through pages to load the required js/css files
 switch ($pageFilter) {

    // Login Page
    case "login":

    // Request
    case "request":
        include_External_File("login-screen", "css", $css_src);
        include_External_File("miscellaneous", "js", $js_src);
        break;

    // Main Page
    case "main":
        include_External_File("admin-data", "css", $css_src);
        include_External_File("twitter", "css", $css_src);
        include_External_File("notification", "css", "assets/src/css"); 
        include_External_File("miscellaneous", "js", $js_src); 
        include_External_File("notification","js", "assets/src/js"); 
        break;

    // Account Section
    case "account":

        // Include bootstrap password strength meter files (js and css)
        include_External_File(
            "jquery-bootstrap2-pwstrength-1.2.5",
            "css",
            $css_src);

        include_External_File(
            "jquery-bootstrap2-pwstrength-1.2.5",
            "js",
            $js_src);

        include_External_File(
            "jquery-password-fields",
            "js",
            $js_src);

        include_External_File("account", "css", $css_src);

        include_External_File("account", "js", $js_src);

        break;

    // Features Section
    case "features":
        include_External_File("features", "css", $css_src);
        include_External_File("features", "js", $js_src);
        break;

    // Tools Section
    case "tool":
        switch ($page) {
            case "prevfb_tool":
                include_External_File("popup_pdf", "css", $css_src);
                include_External_File("feedback-utility", "js", $js_src);
                break;
            case "template_tool":
                include_External_File("tinymce.min", "js", $tiny_src);
                include_External_File("template", "js", $js_src);
                include_External_File("tinymce-style", "css", $css_src);
                include_External_File("template", "css", $css_src);
                break;
            case "feedback_tool":
                include_External_File("tools-section", "css", $css_src);
                include_External_File("feedback-utility", "js", $js_src);
                include_External_File('feedback-options', 'js', $js_src);
                break;
            case "station_tool":
                include_External_File("impexpsts", "css", $css_src);
                include_External_File("export-import-stations-section", "js", $js_src);
                break;
            case "role_tool":
                include_External_File("roles", "js", $js_src);
                include_External_File("tools-section", "css", $css_src);
                include_External_File("tools", "js", $js_src);
                break;
            // Global Rating Scale Pages in Admin Tools Section
            case "grs_types_tool":
                include_External_File("tools-section", "css", $css_src);
                include_External_File("global-functions-jq", "js", $js_src);
                include_External_File("tools-grs-types", "js", $js_src);
                break;
            case "grs_values_tool":
                include_External_File("tools-section", "css", $css_src);
                include_External_File("global-functions-jq", "js", $js_src);
                include_External_File("tools-grs-values", "js", $js_src);
                break;
            case "swap_tool":
                include_External_File("tools-section", "css", $css_src);
                include_External_File("swap-examinees", "css", $css_src);
                include_External_File("swap-examinees", "js", $js_src);
                break;
            default:
                include_External_File("tools-section", "css", $css_src);
                include_External_File("tools", "js", $js_src);
            break;
        }
        break;


    // Term Section
    case "terms":
        include_External_File("term-administration", "js", $js_src);
        break;

    // Multimedia    
    case "multimedia":
        include_External_File("multimedia", "css", $css_src);
        include_External_File("multimedia", "js", $js_src);
        break;      

    // Manage Examinee Pages
    case "examinees":

        // Preview examinee list
        if ($firstSubName == "preview") {
         include_External_File("popup_print", "css", $css_src);
         include_External_File("students-view", "js", $js_src);

        // Examinee list in PDF format
        } else if ($firstSubName == "pdf") {
         include_External_File("popup_pdf", "css", $css_src);
         include_External_File("students-view", "js", $js_src);

        // Examinee list view
        } else if ($firstSubName == "manage") {

          // Include the lightbox2 jQuery plugin
          include_External_File(
             "lightbox.min",
             "css",
              $cloudFlareCSS . "/ajax/libs/lightbox2/2.9.0/css/"
          );
          include_External_File(
             "lightbox.min",
             "js",
             $cloudFlareJS . "/ajax/libs/lightbox2/2.9.0/js/"
          );

          include_External_File("examinees-all-pages", "css", $css_src);
          include_External_File("students-view", "js", $js_src);


        // Examinee list update
        } else if ($firstSubName == "update") {

          // Include the lightbox2 jQuery plugin
          include_External_File(
             "lightbox.min",
             "css",
              $cloudFlareCSS . "/ajax/libs/lightbox2/2.9.0/css/"
          );
          include_External_File(
             "lightbox.min",
             "js",
             $cloudFlareJS . "/ajax/libs/lightbox2/2.9.0/js/"
          );

          include_External_File("examinees-all-pages", "css", $css_src);
          include_External_File("students-update", "js", $js_src);
        }

        break;

    // School section pages
    case "schools":
        include_External_File("school-administration", "css", $css_src);
        include_External_File("schools-admin", "js", $js_src);
        break;

    // Department section
    case "departments":
        include_External_File("school-administration", "css", $css_src);
        include_External_File("departments-admin", "js", $js_src);
        break;

    // Courses and modules section
    case "courses":
    case "years":
    case "modules":
        include_External_File("school-administration", "css", $css_src);
        include_External_File("course-administrate", "js", $js_src);
        break;

    // Importing section
    case "importingStep1":
        include_External_File("import-tool", "css", $css_src);
        include_External_File("import-section", "js", $js_src);
        require_once "import/ImportFileFields.php";
        break;

    case "importingStep2":
        include_External_File("import-tool", "css", $css_src);
        include_External_File("import-section", "js", $js_src);
        require_once "import/ImportFile.php";
        break;

    case "importingStep3":
        include_External_File("import-tool", "css", $css_src);
        include_External_File("import-section", "js", $js_src);

        // Include bootstrap password strength meter files (js and css)
        include_External_File(
            "jquery-bootstrap2-pwstrength-1.2.5",
            "css",
            $css_src);

        include_External_File(
            "jquery-bootstrap2-pwstrength-1.2.5",
            "js",
            $js_src);

        include_External_File(
            "jquery-password-fields",
            "js",
            $js_src);

        include_External_File("chosen.jquery.min", "js", $js_src);
        include_External_File("chosen.min", "css", $css_src);

        require_once "import/ImportFile.php";
        require_once "import/ImportMapping.php";
        break;

    case "importingStep4":
        include_External_File("import-tool", "css", $css_src);
        include_External_File("import-section", "js", $js_src);
        require_once "import/ImportFile.php";
        require_once "import/ImportMapping.php";
        require_once "import/ImportProcessor.php";
        require_once "import/ImportMatrixProcessor.php";
        break;

    // User account settings section
    case "account":
        include_External_File("account", "js", $js_src);
        break;

    // Examiner Pages
    case "examiners":
        //SMS Examiners Popup
        if ($page == "sms_examiners") {
            include_External_File("examiners_sms", "css", $css_src);
            include_External_File("sms-examiners", "js", $js_src);
            include_External_File("examiner-administrate", "js", $js_src);
            //Other Examiner Pages
        } else {

            // Include bootstrap password strength meter files (js and css)
            include_External_File(
                    "jquery-bootstrap2-pwstrength-1.2.5",
                    "css",
                    $css_src);

            include_External_File(
                    "jquery-bootstrap2-pwstrength-1.2.5",
                    "js",
                    $js_src);

            include_External_File(
                    "jquery-password-fields",
                    "js",
                    $js_src);

            include_External_File("examiner-administrate", "js", $js_src);
            include_External_File("examiner-administrate", "css", $css_src);

            // Include plugin for autocomplete in <select>
            include_External_File("chosen.jquery.min", "js", $js_src);
            include_External_File("chosen.min", "css", $css_src);
        }
        break;

    // Admin & Super Admins pages
    case "admin":

       // Include bootstrap password strength meter files (js and css)
       include_External_File(
              "jquery-bootstrap2-pwstrength-1.2.5",
              "css",
              $css_src);

       include_External_File(
              "jquery-bootstrap2-pwstrength-1.2.5",
              "js",
              $js_src);

       include_External_File(
              "jquery-password-fields",
              "js",
              $js_src);

       include_External_File("admin-page", "js", $js_src);
       include_External_File("admin-page", "css", $css_src);

       break;

    // Form builder section
    case "bankform":
    case "examform":
       include_External_File("exam-administrate-style", "css", $css_src);
       include_External_File("tinymce.gzip", "js", $tiny_src);

       // Load the TinyMCE compressor class
       require_once("assets/js/tinymce/tinymce.gzip.php");

       // Renders script tag with compressed scripts (make sure to set cache_dir in tinymce.gzip.php file)
       TinyMCE_Compressor::renderTag([
            "url" => "assets/js/tinymce/tinymce.gzip.php",
            "plugins" =>
            "advlist,autolink,lists,link,image,charmap,preview,hr,anchor,pagebreak," .
            "searchreplace,visualblocks,visualchars,fullscreen," .
            "insertdatetime,nonbreaking,save,table,contextmenu,directionality," .
            "paste,textcolor,colorpicker,textpattern,imagetools",
            "themes" => "modern",
            "languages" => "en",
            "disk_cache" => true,
            "source" => true
       ]);

       include_External_File("jquery-tag-combobox", "js", $js_src);
       include_External_File("form-builder-load", "js", $js_src);
       include_External_File("form-builder-edit", "js", $js_src);
       include_External_File("form-builder-other", "js", $js_src);
       include_External_File("tinymce-style", "css", $css_src);

       break;
        
           
    //upload media
    case "upload":
    include_External_File("upload-media", "js", $js_src);
    include_External_File("miscellaneous", "js", $js_src);
    break;


    //Exam pages
    case "osce":
        include_External_File("exam-administrate-style", "css", $css_src);
        switch ($page) {
            case "manage_osce":
                include_External_File("exam-administration", "js", $js_src);
                include_External_File("chosen.jquery.min", "js", $js_src);
                include_External_File("chosen.min", "css", $css_src);
                include_External_File('early-warning', 'js', $js_src);
                break;
            case "settings_osce":
                include_External_File("global-functions-jq", "js", $js_src);
                include_External_File("exam-settings19", "js", $js_src);
                break;
            case "sessions_osce":
                include_External_File("colorPicker", "css", $css_src);
                include_External_File("session-day-administration", "js", $js_src);
                include_External_File("jquery.colorPicker", "js", $js_src);

                include_External_File(
                    "jquery-ui-timepicker-addon.min",
                    "css",
                     $cloudFlareCSS . "/ajax/libs/jquery-ui-timepicker-addon/1.6.3/"
                );

                include_External_File(
                    "jquery-ui-timepicker-addon.min",
                    "js",
                     $cloudFlareJS . "/ajax/libs/jquery-ui-timepicker-addon/1.6.3/"
                );

                break;
            case "stations_osce":
                include_External_File("session-stations-admin", "js", $js_src);
                break;
            case "groups_osce":
                include_External_File("groups-administrate", "js", $js_src);
                break;
            case "assistants_osce":
                include_External_File("session-assistants-administration", "js", $js_src);
                break;
            case "examiner_osce":

                // Include chosen dropdown search plugin
                include_External_File(
                    "chosen.jquery.min",
                    "js",
                     $cloudFlareJS . "/ajax/libs/chosen/1.7.0/"
                );

                include_External_File(
                    "chosen.min",
                    "css",
                     $cloudFlareCSS . "/ajax/libs/chosen/1.7.0/"
                );


                include_External_File("station-examiners", "js", $js_src);
                break;

            // Form Bank Section
            case "forms_osce":
                include_External_File("jquery-tag-combobox", "js", $js_src);
                include_External_File("bank-administrate", "js", $js_src);
                include_External_File("chosen.jquery.min", "js", $js_src);
                include_External_File("chosen.min", "css", $css_src);
                break;

            case "previewform_osce":
                include_External_File("slider-plugin", "css", $css_src);
                include_External_File("form-builder-preview", "css", $css_src);
                include_External_File("countdown", "js", $js_src);
                include_External_File("preview-scoresheet", "js", $js_src);
                break;
        }
        break;

    // Print or PDF of exam groups
    case "groups":

        // Preview exam groups
        if ($firstSubName == "preview") {
          include_External_File("popup_print", "css", $css_src);

        // Exam groups in PDF format
        } else if ($firstSubName == "pdf") {
          include_External_File("popup_pdf", "css", $css_src);

        }
        include_External_File("groups-administrate", "js", $js_src);
        break;

    // Analysis section
    case "analysis":
        include_External_File("analyse-section", "css", $css_src);
        include_External_File("treemenu-plugin", "css", $css_src);
        include_External_File("popup_analysis", "css", $css_src);

        if ($page == "out_analysis") {

            include_External_File("treeMenu-plugin", "js", $js_src);
            include_External_File("easyTable.v03", "js", $js_src);
            include_External_File("analysis-facility", "js", $js_src);
            include_External_File("selfassess-results-view", "js", $js_src);

        }
        break;

    // Exam results and exam history
    case "studentexam":
    case "studenthistory":
        include_External_File("popup_examinee_results", "css", $css_src);
        include_External_File("examinee-exam-results", "js", $js_src);
        break;

    // Exam station results
    case "studentresult":
        include_External_File("popup_examinee_results", "css", $css_src);
        include_External_File("examinee-results", "js", $js_src);
        break;

    // Delete results section
    case "dresults":
        include_External_File("analyse-section", "css", $css_src);
        include_External_File("treemenu-plugin", "css", $css_src);
        include_External_File("popup", "css", $css_src);
        include_External_File("treeMenu-plugin", "js", $js_src);
        include_External_File("easyTable.v03", "js", $js_src);
        include_External_File("analysis-facility", "js", $js_src);
        break;

    // Detailed results section
    case "resdets":

        include_External_File(
            "FileSaver.min",
            "js",
            $cloudFlareJS . "/ajax/libs/FileSaver.js/1.3.8/"
        );

        include_External_File(
            "xlsx.core.min",
            "js",
            $cloudFlareJS . "/ajax/libs/xlsx/0.14.1/"
        );

        include_External_File(
             "tableexport.min",
             "css",
             $cloudFlareCSS . "/ajax/libs/TableExport/5.2.0/css/"
        );

        include_External_File(
            "tableexport.min",
            "js",
            $cloudFlareJS . "/ajax/libs/TableExport/5.2.0/js/"
        );

        include_External_File("popup_detailed", "css", $css_src);
        include_External_File("easyTable.v03", "js", $js_src);
        include_External_File("detailed-analysis", "js", $js_src);
        break;

    // Assistants section
    case "assist":
        include_External_File("assistants", "css", $css_src);
        include_External_File("assistant-administration", "js", $js_src);
        break;

    // Stations in PDF format
    case "pdfstations":
        include_External_File("popup_pdf", "css", $css_src);
        include_External_File("session-day-administration", "js", $js_src);
        break;

    // Output examinees results PDF format
    case "pdfresults":
        include_External_File("popup_pdf", "css", $css_src);
        include_External_File("pdf-examinee-results", "js", $js_src);
        break;

    // Image import section
    case "processimages":
        include_External_File("image_import", "js", $js_src);
        include_External_File("miscellaneous", "js", $js_src);
        break;

    // The default action
    default:
        include_External_File("miscellaneous", "js", $js_src);
        break;
 }
 ?>
 <script>
 <?php
 // Set login status for system login session management
 $loggedIn = !(!isset($_SESSION['instance_identifier']) || $_SESSION['instance_identifier'] != $config->installation_id);
 $subDomain = basename(realpath(__DIR__ . '/..'));
 if ($loggedIn) {
 ?>

    localStorage.setItem('<?=$subDomain?>_login_state', 'logged-in');
    jQuery(window).bind('storage', function() {
        if (localStorage.getItem('<?=$subDomain?>_login_state') == "logged-out") {
           parent.location = './';
        }
    });

 <?php
  } else {
 ?>
   localStorage.setItem('<?=$subDomain?>_login_state', 'logged-out');
 <?php
  }
 ?>
 </script>
 <style id="default-colors">
 :root {
     --primary-main: #2f8040;
     --primary-dark-main: #256432;
     --primary-border-main: #215b2d;
     --primary-opacity-main: rgba(78,147,93,.5);
     --primary-opacity-2-main: rgba(47,128,64,.08);
     --primary-control-main: #67c77b;
     --primary-opacity-3-main: rgba(47,128,64,.25);
     --primary-opacity-4-main: rgba(47,128,64,.5);
     --primary-active-main: #8dd59c;
     --primary-group-dark-main: #184321;
     --primary-group-main: #c5dbca;
     --primary-group-2-main: #b5d1bc;
     --primary-success-main: #1a4824;
}
 </style>
 <script>
     getColorTheme("<?= $primary_color ?>");
 </script>
 </head>
 <body>
