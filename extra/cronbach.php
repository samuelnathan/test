<?php

/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/06/2010
 * © 2010 Qpercom Limited. All rights reserved.
 * ---------------------------Cronbach Alpha----------------------------------
 */
require_once 'Math/Stats.php';

class cronbach
{
    /* ---------------------Cronbach Alpha Functions------------------------------ */
    function calc_Alpha($matrix)
    {
        $errors = array("EMPTY_MATRIX", "UNEVEN_MATRIX", "NULLIN_MATRIX", "STRINGIN_MATRIX"); # Errors

        if (empty($matrix)) { # If Matrix is Empty
            return $errors[0];
        }

        $s = new Math_Stats();
        $N = count($matrix); # Number of Items
        $size = count($matrix[0]); # Length of Item
        $vBar = 0;
        $cBar = 0;

        # If array items don't have same number or contain null or string values
        foreach ($matrix as $e) {
            if (count($e) == 1 || count($e) != $size) { # Item same size?
                return $errors[1];
            }

            foreach ($e as $v) {
                if (is_null($v)) { # is value null?
                    return $errors[2];
                }
                if (!is_numeric($v)) { # is value NOT Numeric?
                    return $errors[3];
                }
            }

            /*             * ******* Average Variance Per Item *********** */
            $s->setData($e);
            $vBar = $vBar + $s->variance();
            /*             * ******* Average Variance Per Item *********** */
        }
        $vBar = $vBar / $N;

        /***************** Average Inter-Item Covariance **********************/
        $combnr = 0;
        for ($i = 0; $i < $N; $i++) {
            for ($j = 1 + $i; $j < $N; $j++) {
                $combnr++;
                $cBar += $this->calc_Covariance($matrix[$i], $matrix[$j]);
            }
        }
        if ($combnr != 0) {
            $cBar = $cBar / $combnr;
        }

        /****************** Average Inter-Item Covariance ********************/
        /* Formula Cronbach Alpha */
        $ba = ($vBar + ($N - 1) * $cBar);
        if ($ba == 0) {
            return "NA";
        }
        $alpha = ($N * $cBar) / $ba;
        return roundAndFormat($alpha, 3);
    }

    function calc_Covariance($a, $b)
    {
        $v = new Math_Stats();
        $v->setData($a);
        $mean1 = $v->mean($a);
        $covar = 0;

        $v->setData($b);
        $mean2 = $v->mean($b);

        $cnt = 0;
        foreach ($a as $val) {
            $temp1 = $val - $mean1;
            $temp2 = $b[$cnt] - $mean2;
            $covar += ($temp1 * $temp2) / (count($a) - 1);
            $cnt++;
        }

        return $covar;
    }
}
