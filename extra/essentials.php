<?php
/* Original Author: David Cunningham
  For Qpercom Ltd
  Date: 11/03/2014
  © 2014 Qpercom Limited. All rights reserved.
  @DB Connection and other includes
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require_once __DIR__ . '/../vendor/autoload.php';
}

//Configuration file
global $config;
if (!isset($config)) {
    $config = new OMIS\Config();
}

//Critical session check
$session = new OMIS\Session;

/* In some cases, we want to return something other than HTML, and in this case 
 * we don't want to send any more that could confuse matters. This check is
 * being done primarily so that we don't want to seem to be changing our minds 
 * and rather than saying "I'm sending JSON" coming back and saying "Well, maybe
 * I actually want to send HTML"
 */
if (!headers_sent()) {
    //Utf-8 content type header
    header("Content-Type: text/html; charset=utf-8");
}

//Redirect on session time out [Yes/No]
if (isset($redirectOnSessionTimeOut)) {
    $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
} else {
    $session->check();
}

//Useful functions
require_once __DIR__ . '/usefulphpfunc.php';

//Database extra
use \OMIS\Database\CoreDB as CoreDB;

global $db;
if (!isset($db)) {
    $db = CoreDB::getInstance();
}

//Critical variables check 
require_once __DIR__ . '/critical_variables_check.php';
