<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Critical Variable Checks 
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

global $db;
if (!isset($db)) {
    $db = \OMIS\Database\CoreDB::getInstance();
}

// Omis Identifier Set
if (isset($_SESSION['instance_identifier'])) {
    
    /* The 'MoxieManager' (file/image manager) plugin for tinymce (rich text editor)
     * requires this session variable to be set for authentication
     */
    $_SESSION['tinymce_moxiemanager_auth'] = true;
    
    /* If no term is specified or the currently-specified term doesn't exist
     * then use the current term as a default that should "work"...
     */
    if (!isset($_SESSION['cterm']) || !$db->academicterms->existsById($_SESSION['cterm'])) {
        $cterm_arr = $db->academicterms->getDefaultTerm();
        $_SESSION['cterm'] = (!is_null($cterm_arr)) ? $cterm_arr['term_id'] : '';
        unset($cterm_arr); #Tidy Up
    }

    // If User Details are not set
    $user_details_keys = array('forename', 'surname', 'email', 'user_role');
    $user_not_set = false;
    foreach ($user_details_keys as $key) {
        $user_not_set &= isset($_SESSION["user_$key"]);
    }
    if ($user_not_set) {
        $user_dets = $db->users->getUser($_SESSION['user_identifier'], 'user_id', true);
        foreach ($user_details_keys as $key) {
            $_SESSION["user_$key"] = $user_dets[$key];
        }
    }
}

// Set the base directory...
if (!isset($_SESSION['base']) || empty($_SESSION['base'])) {
    $_SESSION['base'] = basename(realpath(__DIR__ . '/..'));
}

/* Unset certain critical session variables if we're on a page that they
 * shouldn't be defined for...
 */
if (isset($page)) {
    $unwanted_session_vars = array(
        'update_examinees' => 'examinees_to_update',    // For Examinee Section
        'edit_examiners' => 'examiners_edit',           // For Examiners Section
        'station_tool' => 'import_report'               // For Station Import Section
    );
    
    foreach ($unwanted_session_vars as $allowed_page => $session_var) {
        if (($page != $allowed_page) && isset($_SESSION[$session_var])) {
            unset($_SESSION[$session_var]);
        }
    }
}
