<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */
session_start();

 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Auth\Role as Role;

 define('_OMIS', 1) or define("_OMIS", 1);
 defined("MANAGEMENT_TOOL") or define("MANAGEMENT_TOOL", 1);
 defined("ASSESSMENT_TOOL") or define("ASSESSMENT_TOOL", 2);

 // Make sure we have a working autoloader without declaring it twice...
 require __DIR__ . '/../vendor/autoload.php';


 $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS);

 // Variables to start with
 $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING);
 $userInputID = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRIPPED);
 $loginSuccessful = false;
 $url = "../";

 /* Coming from continue button click on index page when there is a user session.
  * Continue to management/admin section or continue to assessment tool depending
  * on 'todo' session variable.
  */
 if (filter_has_var(INPUT_POST, 'continue')) {

     // Continue to assessment tool
     if ($_SESSION['todo'] == ASSESSMENT_TOOL) {
         $url = '../assess.php?p=exam_configuration';
     } else {
         $url = '../manage.php?page=main';
     }

 /**
  * USER Logging action
  */
 } elseif (filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRIPPED) == 'log in') {

     /* While Analog can (and will) create a log file where one doesn't exist,
      * the path you provide it into which to place that file has to exist. Since
      * initial.php is the first page loaded by OMIS *after* the user logs in,
      * here is a good place to check that the log folder exists to prevent
      * problems in relation to logging.
      */
     if (!is_dir($config->log_folder)) {
         // If we can't create the folder, die.
         try {
             if (!mkdir($config->log_folder, true)) {
                 die("ERROR: Cannot create log folder. Please contact an administrator.");
             }
         } catch (ErrorException $ex) {
             if (!mkdir($config->log_folder, true)) {
                 echo $ex->getMessage();
                 die("ERROR: Cannot create log folder. Please contact an administrator.");
             }
         }
     }

     $db = \OMIS\Database\CoreDB::getInstance();

     // Setting variables
     $timeDelay = $config->login['timeout'];
     $attemptRecords = $db->users->getLoginAttempts($ipAddress, $userInputID);

     if ($attemptRecords instanceof mysqli_result) {
         $row = $attemptRecords->fetch_array($resulttype = MYSQLI_ASSOC);
         $currentAttempt = (int) $row['attempts'];
     } else {
         $currentAttempt = 0;
     }

     $_SESSION['userinputid'] = $userInputID;
     $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRIPPED);

     $ipConfirmed = $db->users->confirmIPAddress(
             $ipAddress,
             $config->login['attempts'],
             $timeDelay,
             $userInputID
     );

     // Check activity from ip address
     if ($ipConfirmed) {
         $_SESSION['loginerror'] = "<p class='loginfo'>Login attempts limit reached. "
                 . "Please wait $timeDelay minutes<br/>to try again or contact the Administrator</p>";
     } else {
         $db->users->addLoginAttempt($ipAddress, $userInputID);

         // User check
         if ($db->users->checkPassword($userInputID, $password)) {
             $resultSet = $db->users->getUser($userInputID);
             $rows = mysqli_num_rows($resultSet);
             $userRecord = $db->fetch_row($resultSet);
         } else {
             $rows = 0;
         }

         // Calculated attempt number
         $attemptNumber = $config->login['attempts'] - $currentAttempt;

         // Set session variable todo for use on other parts of the website
         $_SESSION['todo'] = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_NUMBER_INT);

         if ($password == "" || $userInputID == "" || $rows == 0) {
             \Analog::notice("$userInputID attempt #" . ($currentAttempt + 1)
                     . " of " . $config->login['attempts'] . " failed");

             // Check if the person is locked out and not back from a lock out period
             if (($attemptNumber <= 0) && $ipConfirmed) {

                 $_SESSION['loginerror'] = "<p class='loginfo'>Login attempts limit reached.<br/>Please wait $timeDelay "
                         . "minutes and try again, or contact an Administrator</p>";
                 \Analog::notice("$userInputID account locked for $timeDelay minutes");

             // Warn the use on the last attempt
             } else {

                 $_SESSION['loginerror'] = "<p class='loginfo'>ID / Password "
                          . "incorrect with <span>". $attemptNumber . "</span> "
                          . ngettext('attempt', 'attempts', $attemptNumber) . " remaining</p>";
             }

         // Account is disabled.
         } elseif (!$db->users->doesUserAccountExist($userInputID)) {

             \Analog::notice("$userInputID attempted to log into inactive account");
             $_SESSION['loginerror'] = "<p class='loginfo'>Your account is "
                     . "currently inactive.<br/>Please contact your Administrator<br/> "
                     . "for more information</p>";

         /*
          * Account is enabled but the user is an examiner/exam admin but not linked to any department
          * (Block them, this is a security measure)
          */
         } elseif (!$db->users->departmentLinkExists($userInputID, $userRecord['user_role'])) {

             \Analog::notice(
                 "$userInputID denied access due to not having a link to any "
                 . gettext('department') . " as an " . gettext('assessment') . " team member"
             );

             $_SESSION['loginerror'] = "<p class='loginfo'>Denied Access.<br/>
                 Your account lacks sufficient privileges to access the system.<br/>
                 Please contact your administrator for more information </p>";

         // Login Successful
         } else {

             $loginSuccessful = true;
             $userID = $userRecord['user_id'];

             // First Section
             switch ($_SESSION['todo']) {
                 case MANAGEMENT_TOOL:
                     // Quick database cleaning when a 'ADMIN' or 'SUPER ADMIN' logs in
                     if ($adminRoles->containsRole($userRecord['user_role'])) {
                         $db->students->purgeUnusedStudentRecords();
                     }
                     $url = "../manage.php?page=main";
                     break;
                 case ASSESSMENT_TOOL:
                     $url = "../assess.php?p=exam_configuration";
                     break;
                 default:
                     continue;
             }

         }
     }

 /**
  *  USER Logging Out
  */
 } elseif (isset($_REQUEST['logout'])) {

     $ssoLogout = isset($_SESSION['sso_login']) ? "?logout=sso" : "";

     if (isset($_SESSION['instance_identifier'])) {
         $loggedInUser = $_SESSION['user_identifier'];
         if (session_destroy()) {
             \Analog::info($loggedInUser . ' logged out');
         }

         $config->getMonitorUpdateNotifier(-1)
                ->logout($loggedInUser);
     } else {
         session_destroy();
     }

     $url = "../index.php" . $ssoLogout;

 }


 /**
  * If login was successful then we must
  * perform the following actions
  */
 if ($loginSuccessful == true) {

     // Set user session information
     $_SESSION['user_identifier'] = $userID;
     $_SESSION['instance_identifier'] = $config->installation_id;
     $_SESSION['user_forename'] = $userRecord['forename'];
     $_SESSION['user_surname'] = $userRecord['surname'];
     $_SESSION['user_email'] = $userRecord['email'];
     $_SESSION['user_role'] = $userRecord['user_role'];
     $_SESSION['selected_dept'] = '';
     $_SESSION['selected_school'] = '';
     $_SESSION['selected_course'] = '';
     $_SESSION['selected_year'] = '';
     $_SESSION['selected_module'] = '';
     $_SESSION['loginerror'] = '';

     $dates = $db->users->setTimeStampLogin($userID);
     $_SESSION['lastlogin'] = $dates[1];

     /**
      * Logged in USER presets, now that user is logged in, set these
      */
     $presets = $db->userPresets->get();

     // Prepare term selection preset
     if (!empty($presets['filter_term']) && $db->academicterms->existsById($presets['filter_term'])) {

         $_SESSION['cterm'] = $presets['filter_term'];

     } else {

         $termData = $db->academicterms->getDefaultTerm();
         $_SESSION['cterm'] = !is_null($termData) ? $termData['term_id'] : '';

     }

     // Save term prefix for next time
     $db->userPresets->update(['filter_term' => $_SESSION['cterm']]);

     // Prepare Department Selection Preset
     if (!empty($presets['filter_department']) && $db->departments->existsById($presets['filter_department'])) {
         $_SESSION['selected_dept'] = $presets['filter_department'];

     // If Admin or Super Admin, get last active department
     } elseif (in_array($userRecord['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
         $_SESSION['selected_dept'] = $db->departments->getLastActiveDepartment();

     // If Examiner or Exam Admin
     } elseif (in_array($userRecord['user_role'], [Role::USER_ROLE_EXAMINER, Role::USER_ROLE_EXAM_ADMIN])) {
         $_SESSION['selected_dept'] = $db->departments->getExaminerDepartmentIDs($userID)[0];
     }

     // Save department prefix for next time
     $db->userPresets->update(['filter_department' => $_SESSION['selected_dept']]);

     // Prepare School Selection Prefix
     if (!empty($presets['filter_school']) && $db->schools->doesSchoolExist($presets['filter_school'])) {
         $_SESSION['selected_school'] = $presets['filter_school'];
     }

     // Prepare Course Selection Prefix
     if (!empty($presets['filter_course']) && $db->courses->doesCourseExist($presets['filter_course'])) {
         $_SESSION['selected_course'] = $presets['filter_course'];
     }

     // Prepare Year Selection Prefix
     if (!empty($presets['filter_year']) && $db->courses->doesCourseYearExist($presets['filter_year'])) {
         $_SESSION['selected_year'] = $presets['filter_year'];
     }

     // Prepare Module Selection Prefix
     if (!empty($presets['filter_module']) && $db->courses->doesModuleExist($presets['filter_module'])) {
         $_SESSION['selected_module'] = $presets['filter_module'];
     }

     // Etc..
     $db->users->clearLoginAttempts($ipAddress, $userID);
     \Analog::info($userID . " logged in successfully");

 }

 // Where do we go from here?
 header('Location: ' . $url);
