<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */
    
 // This file is only included, not invoked directly. Ensure it only runs then...
 defined("_OMIS") or die("Restricted Access");
    
 // Make sure we have a working autoloader without declaring it twice...
 global $loader;
 if (!isset($loader)) {
    $loader = require __DIR__ . "/../vendor/autoload.php";
 }
    
 use \OMIS\Utilities\Path as Path;
    
 ini_set("session.cache_limiter", "private");
 header("Content-Type: text/html; charset=utf-8");

 require_once "extra/usefulphpfunc.php";

 global $config;
 global $page;
 global $db;

 if (!isset($db)) {
    $db = \OMIS\Database\CoreDB::getInstance();
 }

 require_once "critical_variables_check.php";

 // Assets Source
 $css_src = "assets/css";
 $js_src = "assets/js";
 $primary_color = $config->localization["skin"]["primary_color"];
 
 /*
  * If we are in offline mode (no internet connection), then
  * revert back to local copies of js and css from the CDN
  */
 if ($config->inOfflineMode()) {
   
  $fallback = $config->getCDNSetting("cloudflare", "fallback_local");
  $cloudFlareJS = $js_src . "/" . $fallback;
  $cloudFlareCSS = $css_src . "/" . $fallback;

 /*
  * If we are online (internet connection), then use the CDN
  */
 } else {
   $cloudFlareJS = $cloudFlareCSS = $config->getCDNSetting("cloudflare", "url");
 }
    
 ?>
 <!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width">
<title><?=$config->page_titles["assessment"]?></title>
<link rel="icon" href="favicon.ico"/>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<?php
// Global Functions
include_External_File("global-helper-functions", "js", $js_src);
include_External_File("global-assessment-tool", "css", $css_src);
include_External_File("global-admin-manage", "css", $css_src);
include_External_File("global-assessment-custom", "css", 'storage/custom/css');
include_External_File("menu-assessment", "css", $css_src);
include_External_File("styling_reg", "css", $css_src);

// IE related hacks :(
include_External_File("ie_assess", "css", $css_src, "IE");

// Load jQuery framework. Loading whatever we can from CDNJS for speed.
// jQuery 3.2.1 and jQuery ui 1.12.1 framework
include_External_File(
    "jquery.min",
    "js",
    $cloudFlareJS . "/ajax/libs/jquery/3.2.1/"
 );
    
 include_External_File(
    "jquery-ui.min",
    "js",
    $cloudFlareJS . "/ajax/libs/jqueryui/1.12.1/"
 );
 
 // JQuery Cookie Plugin
 include_External_File(
    "jquery.cookie.min",
    "js",
    $cloudFlareJS . "/ajax/libs/jquery-cookie/1.4.1/"
 );
 
 include_External_File("assessment-text-scaling", "js", $js_src);
 
 include_External_File(
    "jquery-ui.min",
    "css",
    $cloudFlareCSS . "/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/"
 );

 include_External_File(
    "popper.min",
    "js",
    $cloudFlareJS . "/ajax/libs/popper.js/1.14.6/umd/"
 );
 
 include_External_File(
    "tinycolor.min",
    "js",
    $cloudFlareJS . "/ajax/libs/tinycolor/1.4.1/"
 );

 include_External_File(
    "bootstrap.min",
    "js",
    $cloudFlareJS . "/ajax/libs/twitter-bootstrap/4.2.1/js/"
 );

 include_External_File("bootstrap.min", "css", $css_src);
 include_External_File("bootstrap-helper", "css", $css_src);
 include_External_File("css-vars-ponyfill", "js", $js_src);
 include_External_File("bootstrap-helper", "js", $js_src);

 /**
  * better scroll management using jQuery plugin
  */
 include_External_File("jquery-scrollscope", "js", $js_src);
    
 /**
  * Add plugin: Offline.js for offline detection
  * automatically alert your users when they've lost internet connectivity
  */
 include_External_File(
     "offline.min",
     "js",
     $cloudFlareJS . "/ajax/libs/offline-js/0.7.17/"
 );
    
 include_External_File(
     "offline-language-english-indicator",
     "css",
     $cloudFlareCSS . "/ajax/libs/offline-js/0.7.17/themes/"
 );

 include_External_File(
     "offline-theme-chrome-indicator",
     "css",
     $cloudFlareCSS . "/ajax/libs/offline-js/0.7.17/themes/"
 );
    
 include_External_File("assessment-helpers", "js", $js_src);
    
 switch ($page) {
    //port to JQuery....
    // Exam Configuration page
    case "exam_configuration":
        include_External_File("assessment-configure-options", "js", $js_src);
        break;
    
    // Student Lit page
    case "student_selection":
        include_External_File("assessment-student-select", "js", $js_src);
        break;
    
    // Assessment form section
    case "exam_assessment":
        /**
         *  Include jQuery UI Touch Punch library. It"s a hack that
         *  enables the use of touch events on sites using the jQuery UI
         *  user interface library. In this case it allows the user to move the popup dialog
         *  whilst assessing on a mobile or tablet.
         */
        include_External_File(
             "jquery.ui.touch-punch.min",
             "js",
             $cloudFlareJS . "/ajax/libs/jqueryui-touch-punch/0.2.3/"
        );

        if ($config->exam_defaults['multiple_candidate']) {
          include_External_File("multiple-student-assessment-sheet", "js", $js_src);
        } else {
          include_External_File("assessment-sheet", "js", $js_src);
        }

        include_External_File("assessment-view", "js", $js_src);
        include_External_File("slider-plugin", "css", $css_src);

        include_External_File("assessment-configuration", "js", $js_src);

        include_External_File('interact', 'js', $js_src);
        include_External_File('assessment-view', 'css', $css_src);
        include_External_File('assessment-view', 'js', $js_src);
        break;
    
    // login page
    case "examiners":
        include_External_File("assessment-examiner", "js", $js_src);
        break;
    
    // Results page
    case "results":
        include_External_File("assessment-results", "js", $js_src);
        break;
    
    // Confirm identity of next student.
    case "next":
    
        // Include the js code for the student confirmation page
        include_External_File("assessment-next-confirm", "js", $js_src);
    
        // Include the lightbox2 jQuery plugin
        include_External_File(
             "lightbox.min",
             "css",
             $cloudFlareCSS . "/ajax/libs/lightbox2/2.9.0/css/"
        );
        include_External_File(
             "lightbox.min",
             "js",
             $cloudFlareJS . "/ajax/libs/lightbox2/2.9.0/js/"
        );

        break;
    
    // The default action
    default:
        break;
 }

 ?>
 <script>
 <?php
 // Set login status for system login session management
 $loggedIn = !(!isset($_SESSION['instance_identifier']) || $_SESSION['instance_identifier'] != $config->installation_id);
 $subDomain = basename(realpath(__DIR__ . '/..'));
 
 if ($loggedIn) {
     $subDomain = basename(realpath(__DIR__ . '/..'));
 ?>
 
    localStorage.setItem('<?=$subDomain?>_login_state', 'logged-in');
    jQuery(window).bind('storage', function() {
        if (localStorage.getItem('<?=$subDomain?>_login_state') == "logged-out") {
           parent.location = './';
        }
    });
 
 <?php 
  } else {
 ?>
   localStorage.setItem('<?=$subDomain?>_login_state', 'logged-out');
 <?php 
  }
 ?>
 </script>
  <style id="default-colors">
 :root {
     --primary-main: #2f8040;
     --primary-dark-main: #256432;
     --primary-border-main: #215b2d;
     --primary-opacity-main: rgba(78,147,93,.5);
     --primary-opacity-2-main: rgba(47,128,64,.08);
     --primary-control-main: #67c77b;
     --primary-opacity-3-main: rgba(47,128,64,.25);
     --primary-opacity-4-main: rgba(47,128,64,.5);
     --primary-active-main: #8dd59c;
     --primary-group-dark-main: #184321;
     --primary-group-main: #c5dbca;
     --primary-group-2-main: #b5d1bc;
     --primary-success-main: #1a4824;
}
 </style>
 <script>
     getColorTheme("<?= $primary_color ?>");
 </script>
 </head>
 <body id="assessment-body">
