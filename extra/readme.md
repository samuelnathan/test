First-Time Configuration & Setup
=========

Because each instance of OMIS is different (including what each developer has on their desktops), we need to remove the actual configuration file as used on each system and replace it with a generic template for that configuration file that we can adapt to suit the needs of the specific installation.

How to configure OMIS on a new system
----------
 1. Copy `configuration.php.template` to `configuration.php`
 1. Edit `configuration.php` to reflect the actual database connection settings, file paths (if required - should "just work"), etc.
 1. Run OMIS to test it all :)

Database Configuration settings
----------
 - `$db_user`: Database username
 - `$db_pass`: Database password
 - `$db_host`: Address of database server (if running on the same machine as the web server hosting OMIS, this will be `localhost`)
 - `$db_name`: The name of the database/schema being used for this instance of OMIS.

Setting Write permissions
----------
On some systems (particularly under Linux), it may be necessary to specifically set write permissions on certain folders so that files can be created there (either temporarily or permanently).

It's important that those permissions are not required for the user who logs into the server (i.e. you/root/whoever), but for whatever user is accessing them while the system is running, i.e. the web server process's username. This is typically one of the following for apache-based systems, but YMMV:

 -  `apache` (Typical on CentOS)
 -  `apache2`
 -  `httpd`
 -  `www-data` (Many debian-based systems, including Ubuntu, use this one)

Typically, rather than explicitly setting permissions, the web server user is given ownership of the files/folders in the web root; that way the apache user can create folders as required in the folder. 

The secure way to do this is to only do this for the folders that the web server process may need to create files in, and let it only read the rest; however, for speed, it may be easier to do it in a more 'blanket' fashion.

To do this, you'll need to figure out what group the user belongs to as well. That can be done with the `groups` command, e.g.:

    user@server:~$ groups www-data
    www-data : www-data list svn

In this case, `www-data` is a member of three groups: `www-data`, `list` and `svn`. The obvious group is the same as the username, e.g. `www-data`. To change the ownership of a folder to this user in this group:

    user@server:~$ chown -R <user>:<group> <path>

(replacing `<user>` and `<group>` with the values you found above, and `<path>` with the path to the folder you want to apply this change to)

Folders that might need write permissions
----------
All paths relative to the OMIS root folder:

 - `/storage/cache/student_images`
 - `/storage/app/tmp`
 - `/storage/logs`
 - `/storage/cache/twig_compilation_cache` (for Twig templating)