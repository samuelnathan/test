<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 09/01/2012
 * © 2012 Qpercom Limited.  All rights reserved. 
 * NO Cache Meta
 */
if ($page == 'pdf_groups' || $page == 'pdf_examinees') {
    ?><meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" /><?php
}