<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
use \OMIS\Auth\Role as Role;
use \OMIS\Template as Template;

// Page Access Check / Can User Access this Section?
if (!Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

/**
 * Render list filter options
 * 
 * @param string $selectedDepartment
 * @param string $selectedTerm
 * @param string $selectedAccessible
 * @return array department name
 */
function renderFilterOptions($selectedDepartment, $selectedTerm, $accessibleOption = false, $selectedAccessible = null)
{
    // Get database instance
    $db = OMIS\Database\CoreDB::getInstance(); 
    // Get department data
    if (in_array($_SESSION["user_role"], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
        
        $deptRecords = $db->departments->getAllDepartments(\TRUE);

    } else if ($_SESSION["user_role"] == Role::USER_ROLE_SCHOOL_ADMIN) {
        
        $adminSchools = $db->schools->getAdminSchools($_SESSION["user_identifier"]);
        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
        $deptRecords = $db->departments->getDepartments(null, true, $schoolIDs);

    } else {

        $deptRecords = $db->departments->getExaminerDepartments(
            trim($_SESSION["user_identifier"]),
            \NULL,
            \TRUE
        );

    }

    // Group by school
    $schools = OMIS\Database\CoreDB::group(
        $deptRecords,
        ['school_description'],
        true
    );
    
    // Get terms
    $terms = $db->academicterms->getAllTerms();
    $selectedDeptName = "";

    
    $template = new Template("filter_Options.html.twig");
    $template->render([
        "schools"               => $schools,
        "selected_department"   => $selectedDepartment,
        "selected_accessible"   => $selectedAccessible,
        "accessible_option"     => $accessibleOption,
        "terms"                 => $terms,
        "selected_term"         => $selectedTerm
    ]);

    // Returned selected details
    return ["dept_name" => $selectedDeptName];
}

/**
 * Validate filter options
 */
function validateFilterOptions()
{
    // Get database instance
    $db = OMIS\Database\CoreDB::getInstance();
    $filterReady = false;
    $selectedDepartment = "";
    $selectedAccessible = "";
    $selectedTerm = "";

    // Department checks
    $deptParam = filter_input(INPUT_GET, "dept");
    if (!is_null($deptParam)) {
        $selectedDepartment = trim($deptParam);
        $_SESSION["selected_dept"] = $selectedDepartment;
    } elseif (isset($_SESSION["selected_dept"])) {
        $selectedDepartment = trim($_SESSION["selected_dept"]);
    }

    // Term selected
    $termParam = filter_input(INPUT_GET, "term");
    if (!is_null($termParam)) {

        $selectedTerm = trim($termParam);

        if ($db->academicterms->existsById($termParam)) {

            $_SESSION["cterm"] = $selectedTerm;

        }

    // Original current term
    } elseif (isset($_SESSION["cterm"])) {

        $selectedTerm = trim($_SESSION["cterm"]);

    }
    
    // Accessible checks
    $accessParam = filter_input(INPUT_GET, "access");
    if (!is_null($accessParam)) {
        $selectedAccessible = trim($accessParam);
        $_SESSION["selected_access"] = $selectedAccessible;
    } elseif (isset($_SESSION["selected_access"])) {
        $selectedAccessible = trim($_SESSION["selected_access"]);
    }
    
    // Final checks & user preference
    $departmentExists = (strlen($selectedDepartment) > 0 && $db->departments->existsById($selectedDepartment));
    $termExists = (strlen($selectedTerm) > 0 && $db->academicterms->existsById($selectedTerm));
    if ($departmentExists && $termExists) {

        $db->userPresets->update([
            'filter_department' => $selectedDepartment,
            'filter_term' => $selectedTerm
        ]);

        $filterReady = true;

    }

    return [
        $selectedDepartment,
        $selectedTerm,
        $filterReady,
        $selectedAccessible
   ];
}

/**
 * Get selected Term
 */
function selectedTerm($paramName = "term") {

    $db = OMIS\Database\CoreDB::getInstance();
    $selectedTerm = "";

    // Term selected
    if (isset($_REQUEST[$paramName]) && strlen($_REQUEST[$paramName]) > 0 && $db->academicterms->existsById(trim($_REQUEST[$paramName]))) {

        $selectedTerm = trim($_REQUEST[$paramName]);
        $_SESSION["cterm"] = $selectedTerm;
     
    // Temp page working term
    } elseif (isset($_SESSION["cterm"]) && strlen($_SESSION["cterm"]) > 0 && $db->academicterms->existsById(trim($_SESSION["cterm"]))) {

        $selectedTerm = trim($_SESSION["cterm"]);

    }

    return $selectedTerm;

}

/**
 * Caption Warning or Errors
 */
function caption_Session_Warn($var_set, $message) {
    if (!isset($_SESSION[$var_set])) {
        return;
    }

    printf('<caption class="npl"><div class="errordiv mb20">%s</div></caption>', $message);
    unset($_SESSION[$var_set]);
}

/**
 * Hash Reference
 */
function hashIdentifier($id) {
    printf('<span id="X%s" class="frag-ref"></span>', $id);
}

/**
 * Edit button output
 */
function editButton($anchor_id = "", $alt_text = "Edit", $title_text = "Edit", $image_class = "editb", $anchor_class = "") {
    $template = new Template("editButton.html.twig");
    $template->render([
        "anchor_class"  => $anchor_class,
        "anchor_id"     => $anchor_id,
        "image_class"   => $image_class,
        "alt"           => $alt_text,
        "title"         => $title_text
    ]);
}
