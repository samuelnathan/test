<?php
 use \OMIS\Utilities\Path as Path;

 /**
  * Collection of assorted useful functions
  *
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
 */

 /**
  * This function generates a password
  *
  * @param int $length Size of password to generate
  * @return string Generated password.
  */
 function genPassword($length)
 {
     /**
      * This is to seed the random number generator so that we get properly
      * unique password
      *
      * @return float Seed value for a PRNG.
      */
     function makeSeed()
     {
         list($usec, $sec) = explode(' ', microtime());

         return (float) $sec + ((float) $usec * 100000);
     }

     // Seed the random number generator.
     mt_srand(makeSeed());

     $thepassword = "";
     // All digits plus all letters, upper and lower case.
     $characters = implode(range('0', '9')) . implode(range('a', 'z'))
             . implode(range('A', 'Z'));

     while (strlen($thepassword) < $length) {
         $char = substr($characters, mt_rand(0, strlen($characters) - 1), 1);
         if (!strstr($thepassword, $char)) {
             $thepassword .= $char;
         }
     }

     return $thepassword;
 }

 /**
  * This function used to send an email
  *
  * @param string $to      Destination address
  * @param string $subject Subject line of e-mail
  * @param string $message Message body
  * @param string $from    Sender address
  * @return boolean success yes|no
  */
 function sendemail($to, $subject, $message, $from)
 {
     global $config;

     // Assemble the headers programmatically to help prevent errors.
     $header_names = ['Return-Path', 'Reply-To', 'From'];

     $headers = "";
     foreach ($header_names as $header) {
         $headers .= sprintf("%s: %s admin <%s>\r\n", $header, $config->getName(), $from);
     }

     try {

         // turn off error reporting
         error_reporting(0);
         /* to catch error that if occurs a suitable error message will be
          * displayed.
          */
         if (!mail($to, $subject, $message, $headers, "-f " . $from)) {
             throw new Exception();
         } else {

           // Turn error reporting back on
           error_reporting(E_ALL);
           return true;
         }

     } catch (Exception $e) {
         $generalMessage = "Could not send email: ";
         error_log($generalMessage . __FILE__ . ": " . __METHOD__ . ": " . $e->getCode() . " = " . $e->getMessage());

         // Turn error reporting back on
         error_reporting(E_ALL);
         return false;
     }

 }

 /**
  * This function formats the date  e.g. 05/05/1983
  * @todo $year_type setting as a string is less than idea.
  *
  * @param string $date      Date as string in YYYY-MM-DD format.
  * @param string $separator Separator character to use to construct output Date
  * @param string $year_type Either 'LONG_YEAR' or 'SHORT_YEAR'
  * @return string
  */
 function formatDate($date, $separator = 'SLASH', $year_type = 'LONG_YEAR')
 {

     /*
      * If date is null then return empty string
      */
     if (empty($date) || $date == '0000-00-00') {
         return '';
     }

     if ($separator == 'UNDERSCORE') {
         $separator = '_';
     } elseif ($separator == 'NOTHING') {
         $separator = '';
     } else {
         $separator = '/';
     }
     list($year, $month, $day) = explode('-', $date);
     if ($year_type == 'SHORT_YEAR') {
         $year = substr($year, -2);
     }

     return $day . $separator . $month . $separator . $year;
 }

 /*
  *  Format Date & Time
  */
 function format_DateTime($dateTime, $separator = '.', $shortYear = true)
 {
     /*
      * If timestamp is null then return empty string
      */
     if (empty($dateTime)) {
         return '';
     }

     // Short Year With Time
     list($date, $time) = explode(' ', $dateTime);
     list($year, $month, $day) = explode('-', $date);

     return $time . ' ' . $day . $separator . $month . $separator
            . ($shortYear ? substr($year, -2) : $year);
 }

 /**
  * Validate date
  *
  * @return bool true|false returns valid yes|no
  */
 function dateValid($date, $separator = '-') {
   list ($year, $month, $day) = explode($separator, $date);
   return checkdate($month, $day, $year);
 }

 /**
  * Validate identifier (Department ID, Student ID, Course ID etc)
  *
  * @param string $identifier  identifier string to validate
  * @return bool true|false returns valid yes|no
  */
 function identifierValid($identifier) {
   $regEx = "/^([\.\-\_0-9a-z]+)$/i";
   return (boolean) preg_match($regEx, $identifier);
 }

 // Make array class
 function setA(&$ar, $value, $default)
 {
     // Was array value set in this call
     $was_set = false;

     if (!isset($ar[$value])) {
         $ar[$value] = $default;
         $was_set = true;
     }

     return $was_set;
 }

 /* ISSET Function */
 /**
  * Checks if a key is present in an array, and if so returns a trim()med copy.
  * Otherwise, it returns an empty string
  *
  * @param mixed[] $arr Array to search
  * @param mixed   $val Key to search for in $arr
  * @return string
  */
 function iS($arr, $val)
 {
     if (isset($arr[$val])) {
         return trim($arr[$val]);
     } else {
         return '';
     }
 }

 // Does Array have this Value
 function hasArrayValue($arr, $index, $value)
 {
     return (isset($arr[$index]) && trim($arr[$index]) === trim($value));
 }

 /**
  * Prepare ordering data
  *
  * @param string $defaultOrderType   default order type
  * @param int $defaultOrderIndex     default order index
  * @param array $fields              fields to order
  * @param array $paramNames          request param names
  * @return array $orderingData
  */
 function prepareListOrdering($defaultOrderType, $defaultOrderIndex, $fields, $paramNames = null)
 {

     // Prepare ordering fields
     $orderMap = [
       "DISABLED" => null,
       0 => "UNORDERED"
     ];
     $mapIndex = 0;
     $maxMapIndex = count($fields);
     foreach ($fields as $field) {
         $mapIndex++;
         $orderMap[$mapIndex] = $field;
     }

     // Set default param names
     if (empty($paramNames)) {
        $paramNames = [
           "index" => "order_index",
           "type" => "order_type"
        ];
     }

     // Get order index and order type from the input url
     $orderIndexParam = filter_input(INPUT_GET, $paramNames["index"], FILTER_SANITIZE_NUMBER_INT);
     $orderTypeParam = filter_input(INPUT_GET, $paramNames["type"], FILTER_SANITIZE_STRING);

     // If the order index is outside the boundary then set the default
     $invalidOrderIndex = (!is_numeric($orderIndexParam) || $orderIndexParam < 0 || $orderIndexParam > $maxMapIndex);
     $orderIndex =  $invalidOrderIndex ? $defaultOrderIndex : $orderIndexParam;

     // If not 'ASC' nor 'DESC' then set default
     $orderType = (!in_array($orderTypeParam, ["asc", "desc"])) ? $defaultOrderType : $orderTypeParam;

     // Return order data
     $orderingData = [
         $orderType,
         $orderIndex,
         $orderMap
     ];

     return $orderingData;
 }

 /**
  * Order array column Descending / Ascending
  *
  * @param mixed[] $data - the array of data to be ordered
  * @param string $orderIndex - the column to be ordered
  * @param string $orderingMethod - 'standard' or 'grouped' (numerical and string values are grouped and ordered separately)
  * @param string $orderType - order type
  * @return mixed[] Ordered array
  */
 function orderArrayColumn(&$data, $orderIndex, $orderingMethod, $orderType)
 {
   usort($data, function ($a, $b) use ($orderIndex, $orderingMethod, $orderType) {

    // Ascending order
    if ($orderType == 'ASC') {

        // 'standard' ordering
        if ($orderingMethod == 'standard') {
           return strnatcasecmp($a[$orderIndex], $b[$orderIndex]);

        // 'grouped' ordering
        } elseif (is_numeric($a[$orderIndex]) && is_numeric($b[$orderIndex])) {
           return $a[$orderIndex]==$b[$orderIndex]? 0:($a[$orderIndex]>$b[$orderIndex]?-1:1);
        }

     // Descending order
     } else {

        // 'standard' ordering
        if ($orderingMethod == 'standard') {
            return strnatcasecmp($b[$orderIndex], $a[$orderIndex]);

        // 'grouped' ordering
        } elseif (is_numeric($a[$orderIndex]) && is_numeric($b[$orderIndex])) {
            return $a[$orderIndex]==$b[$orderIndex]? 0:($a[$orderIndex]<$b[$orderIndex]?-1:1);
        }

     }

     return strnatcasecmp($a[$orderIndex], $b[$orderIndex]);

    });
 }

 function checkout($val)
 { #Check Value and Output Space If Blank
     if (is_array($val) || strlen(trim($val)) > 0) {
         return $val;
     } else {
         return "&nbsp;";
     }
 }


 /**
  * Is mobile device
  * @return true|false status
  */
 function isMobileDevice()
 {
     $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
     return (strpos($userAgent, 'ipad') !== false || stripos($userAgent,'android') !== false);
 }


 /* Column Ordering for a table of data, columns can be ordered via the title link
 *
 * @param $text - title link text
 * @param $orderIndex - order index
 * @param $arrayIndex - array index (numerical or associative)
 * @param $alterIndex - alternative index
 * @param $orderType - ASC or DESC
 * @param $data - the actual array of data to sort
 * @param $url - the sort link url
 * @param $orderingMethod - 'standard' or 'grouped' (numerical and string values are grouped and ordered separately)
 * @param $hover - hover message
 * @param $jsTrigger - sort triggered from JavaScript
 *
 */
 function makeColumnOrderable($text, $orderIndex, $arrayIndex, $alterIndex = null, $orderType, &$data, $url, $orderingMethod, $hover = null, $jsTrigger = null)
 {
      // Hover Message
      list($tiptitle, $tipbody) = (is_null($hover)) ? array('Click To Sort', ' ') : explode('::', $hover);
           $hoverClass  = (is_null($hover)) ? '' : 'hovermessage ';

      // If Not Sort
      if (is_null($orderIndex)) {
         echo $text;
      } else {

       // Use Alternative Index If Requested
       $urlIndex = (is_null($alterIndex)) ? $arrayIndex : $alterIndex;
       ?><a <?php
        if ($orderIndex == $arrayIndex) {

           // Sort ASC
          if ($orderType == 'asc') {
            orderArrayColumn($data, $arrayIndex, $orderingMethod, 'DESC');
            $href = (is_null($jsTrigger)) ? $url.'&amp;order_index='.$urlIndex.'&amp;order_type=desc' : 'javascript:void(0)';

            ?> href = "<?=$href; ?>" class = 'title_au orderlink' title = "<?=$tiptitle; ?>" rel = "<?=$tipbody; ?>" <?php

          // Sort DESC
          } elseif ($orderType == 'desc') {
            orderArrayColumn($data, $arrayIndex, $orderingMethod, 'ASC');
            $href = (is_null($jsTrigger)) ? $url.'&amp;order_index='.$urlIndex.'&amp;order_type=asc' : 'javascript:void(0)';
            ?> href = "<?=$href; ?>" class = 'title_ad orderlink' title = "<?=$tiptitle; ?>" rel = "<?=$tipbody; ?>" <?php
          }
          // Specified Sort
          } else {
            $href = (is_null($jsTrigger)) ? $url.'&amp;order_index='.$urlIndex.'&amp;order_type='.$orderType : 'javascript:void(0)';
            ?> href = "<?=$href; ?>" class = '<?=$hoverClass; ?>orderlink' title = "<?=$tiptitle; ?>" rel = "<?=$tipbody; ?>" <?php
          }

         ?>><?=$text; ?></a><?php

           // JavaScript Ordering Trigger
           if (!is_null($jsTrigger)) {
             $nextSortType = ($orderType=='asc') ? "desc" : "asc";
             ?><input type="hidden" class="sort-index" value="<?=$arrayIndex; ?>"/><?php
             ?><input type="hidden" class="next-sort-type" value="<?=$nextSortType; ?>"/><?php
           }
      }
 }

 #HTML table to be orderable:hover:JS
 function makeColumnOrderableHover_JS($title, $hover)
 {
      ?><span><a href = 'javascript:void(0)' title = "<?=$hover; ?>"><?=$title; ?></a></span><?php
 }

 function is_Array_Numeric($arr)
 {
     foreach ($arr as $val) {
         if (!is_numeric($val)) {
             return false;
         }
     }

     return true;
 }

 /**
  * Check if all of the elements in an array are integer. If $signed is true,
  * then it also checks if they're all >= 0.
  *
  * @param int[] $array  Array to check
  * @param bool  $signed Should all numbers in the array also be positive?
  * @return bool
  */
 function isArrayInteger($array, $signed = false)
 {
     function isIntCallback($value)
     {
         return is_int($value);
     }

     function isPositiveIntCallback($value)
     {
         return (is_int($value) && $value >= 0);
     }

     if ($signed) {
         $array = array_map("isIntCallback", $array);
     } else {
         $array = array_map("isPositiveIntCallback", $array);
     }

     return (!in_array("false", $array));
 }

 #Get Column From Array
 function get_From_Array($arr, $column, $search_values, $condition)
 {
     $last_index = count($search_values); #Last Index
     $retarr = array_fill(0, $last_index, array()); #Create empty array
     #Loop through passed in array
     foreach ($arr as $key => $row) {
         $found = false;
         foreach ($search_values as $return_index => $value) {
             #Equals condition and value check
             if ($condition == "=" && $row[$column] == $value) {
                 $retarr[$return_index][$key] = $row;
                 $found = true;
             }
         }

         if (!$found) { #Remainder/ Other Values added to last index
             $retarr[$last_index][$key] = $row;
         }
     }

     return $retarr;
 }

 /**
  * Return the percentage $amount is of $total, accurate to $decimalPlaces.
  *
  * @param int|float $amount         Fractional quantity
  * @param int|float $total          Total quantity
  * @param int       $decimalPlaces  Number of decimal places to round to.
  * @param bool      $format         Keep trailing zeros after decimal point
  *
  * @return float The calculated percentage $amount is of $total.
  */
 function percent($amount, $total, $decimalPlaces = 1, $format = true)
 {
     $calculatedPercent = 0;
     if (($total != 0) && is_int($decimalPlaces) && ($decimalPlaces >= 0)) {
         $calculatedPercent = round(($amount * 100) / $total, $decimalPlaces);
     }

     // Make sure that the percent calculation doesn't break boundaries
     if ($calculatedPercent < 0) {
         $calculatedPercent = 0.0;
     } elseif ($calculatedPercent > 100 || $calculatedPercent == 100.0) {
         $calculatedPercent = 100;

     // If set, keep trailing zeros after decimal point
     } else if ($format) {
        $calculatedPercent = number_format($calculatedPercent, $decimalPlaces);
     }

     return $calculatedPercent;
 }

 // Exit php script and close off HTML
 function errorExitandEnd($error) {
    ?><div class = 'errordiv'><?=$error?></div></body></html><?php
    exit;
 }

 /**
  * Sanitize file name
  * @param string $filename
  * @return string sanitize string
  */
 function sanitizeFileName($filename)
 {

     $cleanfilename = html_entity_decode($filename, ENT_QUOTES, 'UTF-8');
     return utf8_decode(
         preg_replace(
             "/\s\s+/", " ",
             str_replace(["/", "'", ":"], "", $cleanfilename))

     );

 }


 // remove fada or other language variants
 function replaceCharacter($filename)
 {
     $fadaarray = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ',
         'Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é',
         'ê','ë','ì','í','î','ї','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','Ā','ā','Ă',
         'ă','Ą','ą','Ć','ć','Ĉ','ĉ','Ċ','ċ','Č','č','Ď','ď','Đ','đ','Ē','ē','Ĕ','ĕ','Ė','ė','Ę',
         'ę','Ě','ě','ё','Ĝ','ĝ','Ğ','ğ','Ġ','ġ','Ģ','ģ','Ĥ','ĥ','Ħ','ħ','Ĩ','ĩ','Ī','ī','Ĭ','ĭ',
         'Į','į','İ','ı','ї','Ĳ','ĳ','Ĵ','ĵ','Ķ','ķ','Ĺ','ĺ','Ļ','ļ','Ľ','ľ','Ŀ','ŀ','Ł','ł','Ń',
         'ń','Ņ','ņ','Ň','ň','ŉ','Ō','ō','Ŏ','ŏ','Ő','ő','Œ','œ','Ŕ','ŕ','Ŗ','ŗ','Ř','ř','Ś','ś',
         'Ŝ','ŝ','Ş','ş','Š','š','Ţ','ţ','Ť','ť','Ŧ','ŧ','Ũ','ũ','Ū','ū','Ŭ','ŭ','Ů','ů','Ű','ű',
         'Ų','ų','Ŵ','ŵ','Ŷ','ŷ','Ÿ','Ź','ź','Ż','ż','Ž','ž','ſ','ƒ','Ơ','ơ','Ư','ư','Ǎ','ǎ','Ǐ',
         'ǐ','Ǒ','ǒ','Ǔ','ǔ','Ǖ','ǖ','Ǘ','ǘ','Ǚ','ǚ','Ǜ','ǜ','ϋ','Ǻ','ǻ','Ǽ','ǽ','Ǿ','ǿ','΄','ό',
         'Α','ϊ','ฺB','Η','Ḩ','ā','ţ','ḯ','ố','ạ','ẖ','ộ','Ḩ','ḩ','H̱’');
     $cleanChar = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N',
         'O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e',
         'e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A',
         'a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E',
         'e','E','e','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i',
         'I','i','I','i','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N',
         'n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s',
         'S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u',
         'U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I',
         'i','O','o','U','u','U','u','U','u','U','u','U','u','u','A','a','AE','ae','O','o','','o',
         'a','i','b','h','h','a','t','i','o','a','h','o','h','h','h');
     return
     str_replace($fadaarray, $cleanChar, $filename)
     ;
 }
 // Strip quotes
 function cleanout($arg)
 {
     return htmlspecialchars($arg, ENT_QUOTES, 'UTF-8');
 }

 // Adjust HTML
 function adjustHTML($html, $quotes = ENT_QUOTES)
 {
     return html_entity_decode($html, $quotes, 'UTF-8');
 }

 // Strip specific tags
 function strip_only($str, $tags)
 {
     if (!is_array($tags)) {
         $tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
         if (end($tags) === '') {
             array_pop($tags);
         }
     }

     foreach ($tags as $tag) {
         $str = preg_replace('#</?' . $tag . '[^>]*>#is', '', $str);
     }

     return $str;
 }

 function array_splice_assoc(&$old_arr, $new_item, $offset)
 {
     $old_arr = array_slice($old_arr, 0, $offset, true) + $new_item + array_slice($old_arr, $offset, null, true);
 }

 if (!function_exists('mb_ucwords')) {
     function mb_ucwords($str)
     {
         return $str;
     }
 }

 /*
  * Removes non(alpa numeric and _) characters,
  * supports unicode
  */
 function remove_alt_char($str)
 {
     return preg_replace(("/[^\w]/u"), '', $str);
 }

 // Print array format
 function print_r_format($t_array)
 {
     ?><pre><?php print_r($t_array); ?></pre><?php
 }

 // Print array log file
 function print_r_log($data)
 {
     error_log(print_r($data, true));
 }

 // Var dump log
 function var_dump_log($data)
 {

     ob_start();
     var_dump($data);
     error_log(ob_get_clean());

 }

 // Round and Format
 function roundAndFormat($number, $decimalPlaces = 1)
 {

     return number_format(round($number, $decimalPlaces), $decimalPlaces);
 }

 // Created TDs
 function fillerTD($total_cnt, $small_cnt, $css_class)
 {
     $tot = $total_cnt - $small_cnt;
     for ($i = 1; $i <= $tot; $i++) {
 ?><td class = '<?= $css_class; ?>'>&nbsp;</td><?php
     }
 }

 /**
  * Strip Tags OMIS
  * @param string $string
  * @return boolean sucess true|false
  */
 function strip_Tags_Omis($string)
 {
     return strip_tags(str_replace(array("\t", "<br/>", "\r", "\r\n", "\n", "&nbsp;", "&amp;", "(&alpha;)"), " ", $string));
 }

 /**
  * Include external file into head of Html page
  *
  * @param string $file   File name
  * @param string $type   Type of file (css, js)
  * @param string $path   Path to file
  * @param bool $ie_only  Internet Explorer (conditional hack)
  */
 function include_External_File($file, $type = '', $path = '', $ie_only = false)
 {
     $release = \OMIS\Release::getInstance();
     $buildNr = $release::$build;

     if (filter_var($path, FILTER_VALIDATE_URL)) {
         // $path is the start of a URL (e.g. pointing to a CDN or something). Assemble
         // the path direct, no messing.
         $file_path = rtrim($path, "/") . "/" . $file;
     } else {
         // If the path isn't a URL, fix the separators...
         $file_path = Path::setSeparator(Path::join($path, $file));
     }

     switch (strtolower($type)) {

         case 'css':
             $css_tag = "<link rel=\"stylesheet\" type=\"text/css\" href=\"$file_path.css?v=$buildNr\"/>\n";

             if ($ie_only === false) {
                 echo $css_tag;
             } else {
                 echo "<!--[if  $ie_only]>";
                 echo $css_tag;
                 echo "<![endif]-->\n";
             }
             break;
         case 'js':
             echo "<script src=\"$file_path.js?v=$buildNr\"></script>\n";
             break;

     }

 }

 /**
  * Takes a file size (or other quantity, measured in bytes) and returns a more
  * "human-readable" version of it.
  *
  * Defaults to returning base 1000 (SI prefixes).
  *
  * @param int     $bytes      File size in bytes
  * @param boolean $force_unit (optional) "i" to force IEC (binary) units
  * @param boolean $format     (optional) format string to sprintf() the results
  * @param boolean $si         (optional) TRUE = use SI (*1000); FALSE = IEC (*1024)
  * @return string Formatted string containing file size.
  */
 function formatFileSize($bytes, $force_unit = null, $si = true, $format = '%01.2f %s')
 {
     if (!$si || strpos($force_unit, 'i')) {
         // IEC prefixes (binary)
         $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
         $mod   = 1024;
     } else {
         // SI prefixes (decimal)
         $units = array('B', 'kB', 'MB', 'GB', 'table-border', 'PB');
         $mod   = 1000;
     }

     // Determine unit to use
     if (($power = array_search((string) $force_unit, $units)) === false) {
         $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
     }

     return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
 }

 /**
  * Attempt to exec GNU file in the shell to get an accurate MIME type for a file
  *
  * @param string $filepath Path to the file
  * @return string|boolean FALSE on failure, otherwise the Mime type as a string.
  */
 function getFileMimeType($filepath)
 {
     // Make sure there's a file path and that it refers to a path that exists...
     if ((trim ($filepath) === '') || !file_exists ( $filepath)) {
         return \FALSE;
     }

     // See if we can invoke shell_exec...
     if (function_exists('shell_exec') !== \TRUE) {
         return \FALSE;
     }

     // Try to invoke GNU file to see what happens.
     $dump =  shell_exec(sprintf('file -bi %s', $filepath));
     if (trim ( $dump) === '') {
         // No output. Crap.
         return \FALSE;
     }

     // Attempt to catch error messages.
     $first_word = strtok($dump, ' :;');
     switch (strtolower($first_word)) {
         case 'error':
         case 'usage':
         case 'file':
         case 'cannot':
             // In all of these cases, it's gone boom.
             return \FALSE;
         default:
             // Assuming the thing worked if we get to here. MIME types aren't case sensitive.
             return $first_word;
     }
 }

 /**
  * Windows has an issue with file modification times (affects PHP and a bunch
  * of other languages too) where daylight savings time is applied to timestamps
  * for when files were last modified. This causes a couple of invalid conditions
  * where the current time may or may not be DST, the file itself could have been
  * last modified during DST (or not), and every combination of same.
  *
  * (See http://www.codeproject.com/Articles/1144/Beating-the-Daylight-Savings-Time-bug-and-getting)
  *
  * This code is from http://php.net/manual/en/function.filemtime.php and purports
  * to fix the problem.
  *
  * @param string $filePath Path to file to be checked
  * @return int Modification time in UNIX timestamp format (seconds since Jan 1 1970)
  */
 function getCorrectMTime($filePath)
 {
     $time = filemtime($filePath);

     $isDST = (date('I', $time) == 1);
     $systemDST = (date('I') == 1);

     $adjustment = 0;

     if ($isDST == false && $systemDST == true) {
         $adjustment = 3600;
     } elseif ($isDST == true && $systemDST == false) {
         $adjustment = -3600;
     } else {
         $adjustment = 0;
     }

     return ($time + $adjustment);
 }

 /**
  * Convert data into a minimal ascii form for injection into a web form.
  *
  * @param mixed $data Source data to compress. Can be anything you can shovel into a PHP variable
  * @return string Serialized, compressed, base64'd version of $data.
  */
 function pack_data($data)
 {

     return base64_encode(gzcompress(serialize($data)));

 }
 
/**
  * Turn all URLs in clickable links, [copyright and credit goes to https://gist.github.com/jasny]
  * 
  * @param string $value
  * @param array  $protocols  http/https, ftp, mail, twitter
  * @param array  $attributes
  * @return string
  */
  function linkify($value, $protocols = array('http', 'mail'), array $attributes = array())
  {
     // Link attributes
     $attr = '';
     foreach ($attributes as $key => $val) {
         $attr = ' ' . $key . '="' . htmlentities($val) . '"';
     }
     
     $links = array();
     
     // Extract existing links and tags
     $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);
     
     // Extract text links for each protocol
     foreach ((array)$protocols as $protocol) {
         switch ($protocol) {
             case 'http':
             case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$protocol://$link</a>") . '>'; }, $value); break;
             case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
             case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
             default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
         }
     }
     
     // Insert all link
     return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
  
  }
