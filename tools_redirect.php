<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @Redirect file for Admin Tools section
  */
 
 define('_OMIS', 1);
 define('SWAP_STUDENTS_EXAM', 1);
 define('SWAP_STUDENTS_COURSE', 2);
 define('SWAP_STATION_RESULTS', 3);
 
 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     $template = new \OMIS\Template("noaccess.html.twig");
     $data = [
         'logged_in' => true,
         'root_url' => $config->base_path_url,
         'username' => $_SESSION['user_identifier'],
         'page' => \OMIS\Utilities\Request::getPageID()
     ];
     $template->render($data);
     return;
     
 }
 
 // Swap students
 $swapStudents = filter_input(INPUT_POST, 'swap_students', FILTER_SANITIZE_STRIPPED);
 
 // Swap students
 if (!is_null($swapStudents)) {
     
     // Swap out values
     $swapOutGroup = filter_input(INPUT_POST, 'swapout_group', FILTER_SANITIZE_NUMBER_INT);
     $swapOutStation = filter_input(INPUT_POST, 'swapout_station', FILTER_SANITIZE_NUMBER_INT);
     $swapOutStudent = filter_input(INPUT_POST, 'swapout_student', FILTER_SANITIZE_STRIPPED);
     
     $swapInGroup = filter_input(INPUT_POST, 'swapin_group', FILTER_SANITIZE_NUMBER_INT);
     $swapInExamStudent = filter_input(INPUT_POST, 'swapin_exam_student', FILTER_SANITIZE_STRIPPED);
     $swapInCourseStudent = filter_input(INPUT_POST, 'swapin_course_student', FILTER_SANITIZE_STRIPPED);
     
     $swapInCourse = filter_input(INPUT_POST, 'swapin_course', FILTER_SANITIZE_STRIPPED);
     $swapInYear = filter_input(INPUT_POST, 'swapin_year', FILTER_SANITIZE_NUMBER_INT);
     
     $swapPassword = filter_input(INPUT_POST, 'swap_password');
     $swapAction = filter_input(INPUT_POST, 'swap_action', FILTER_SANITIZE_STRIPPED);
     
     $returnUrl = "../manage.php?page=swap_tool";
     
     // First validate the password
     $userID = $_SESSION['user_identifier'];
     
     // Get swap in student (either student to swap in from course or same exam)
     $swapInStudent = ($swapAction == SWAP_STUDENTS_COURSE) ? $swapInCourseStudent : $swapInExamStudent;
     
     // Return swap criterial / stick selections
     $_SESSION['swap_criteria'] = [
           $swapOutGroup,
           $swapOutStudent,
           $swapInGroup,
           $swapInExamStudent,
           $swapInCourseStudent,
           $swapInCourse,
           $swapInYear,
           $swapAction,
           $swapOutStation //(optional)
     ];
     
     
     if (is_null($swapPassword) || !$db->users->checkPassword($userID, $swapPassword)) {
              
        $_SESSION['swap_student'] = [
           'error' => true,
           'message' => 'The password that you entered is incorrect, please try again' 
        ];
        
        header("Location: $returnUrl");
        exit;
     }
     
     // Check swap out student id required
     if (empty($swapOutStudent)) {
        $_SESSION['swap_student'] = [
           'error' => true,
           'message' => 'Swap out student undefined' 
        ];
        header("Location: $returnUrl");
        exit;
     }
     
     // Check swap out group id required
     if (empty($swapOutGroup)) {
        $_SESSION['swap_student'] = [
           'error' => true,
           'message' => 'Swap out group undefined' 
        ];
        header("Location: $returnUrl");
        exit;
     }
         
     // Does this group and student combination exist? If not abort
     if (!$db->exams->doesStudentExistInGroup($swapOutStudent, $swapOutGroup)) {
        $_SESSION['swap_student'] = [
           'error' => true,
           'message' => 'Swap out student does not exist' 
        ];
        header("Location: $returnUrl");
        exit;
     }    
 
     // Check that both students to swap are not the same
     if ($swapOutStudent == $swapInStudent) {
        $_SESSION['swap_student'] = [
           'error' => true,
           'message' => 'Swap in and swap out student identifiers are the same' 
        ];
        header("Location: $returnUrl");
        exit;
     }
        
     // Swap in student from course
     if ((!is_null($swapInCourseStudent) && strlen($swapInCourseStudent) > 0) && $swapAction == SWAP_STUDENTS_COURSE) {
        
        // Does student exist
        if ($db->students->doesStudentExist($swapInCourseStudent)) {
          
           /**
            *  Does exam already contain a student, if so that's a no no.
            *  You cannot have the student assigned to the exam multiple times
            */
           $groupSessionID = $db->exams->getSessionIDByGroupID($swapOutGroup);
           $examID = $db->exams->getSessionExamID($groupSessionID);
           $examContainsStudent = $db->exams->doesExamContainStudent($examID, $swapInCourseStudent);
           if ($examContainsStudent) {
               $_SESSION['swap_student'] = [
                   'error' => true,
                   'message' => 'Exam already contains student you wish to swap in' 
               ];
               header("Location: $returnUrl");
               exit;
           }
            
           $swappedStatus = $db->students->swapStudents(
                        $swapOutStudent,
                        $swapOutGroup,
                        $swapInCourseStudent
           );
           
           // Swap success
           if ($swappedStatus['success']) {
               
            // Get report information
            $studentSwapOutRecord = $db->students->getStudent($swapOutStudent);
            $studentSwapOutName = $studentSwapOutRecord['surname'] . ', ' . $studentSwapOutRecord['forename'];
           
            $swapOutGroupRecord = $db->exams->getGroup($swapOutGroup);
            $swapOutGroupName = $swapOutGroupRecord['group_name'];
           
            $swapOutSessionRecord = $db->exams->getSession($groupSessionID);
            $swapOutSessionName = $swapOutSessionRecord['session_description'] . ' - ' 
                                . formatDate($swapOutSessionRecord['session_date']);             
           
            $studentSwapInRecord = $db->students->getStudent($swapInCourseStudent);
            $studentSwapInName =  $studentSwapInRecord['surname']. ', ' .$studentSwapInRecord['forename'];
            
            // Get swap in course and year information
            $yearRecord = $db->courses->getCourseYear($swapInYear);
           
            // Swap information for user
            $swapinData = [
                'id' => $swapInCourseStudent,
                'name' => $studentSwapInName,
                'course' => $yearRecord['course_name'],
                'year' => $yearRecord['year_name']
            ];
            
            $swapoutData = [
                'id' => $swapOutStudent,
                'name' => $studentSwapOutName,
                'group' => $swapOutGroupName,
                'session' => $swapOutSessionName
            ];           
               
            $_SESSION['swap_student'] = [
               'error' => false,
               'message' => 'Student successfully swapped',
               'swapin' => $swapinData,
               'swapout' => $swapoutData, 
               'swaptype' => SWAP_STUDENTS_COURSE
            ];
            
            // Since we've had a successful swap, then swap the students selected
            $_SESSION['swap_criteria'][1] = $swapInCourseStudent;
            $_SESSION['swap_criteria'][4] = $swapOutStudent;
            
            
          } else {
            $_SESSION['swap_student'] = [
               'error' => true,
               'message' => 'Failed to swap students, please try again' 
            ];
          }
        } else {
           $_SESSION['swap_student'] = [
               'error' => true,
               'message' => 'Swap in student from course not found' 
           ];
        }
        
     // Swap in student from same exam    
     } elseif (in_array($swapAction, [SWAP_STUDENTS_EXAM, SWAP_STATION_RESULTS]) && !is_null($swapInExamStudent)) {
        
        // Stations selected and exist
        $swapOutStationExists = $db->exams->doesStationExist($swapOutStation);
        $swapStationResults = ($swapAction == SWAP_STATION_RESULTS && $swapOutStationExists);
        
        /**
         * Use swapout group for swap in group if we are swapping
         * results within a station
         */
        if ($swapStationResults) {
            $swapInGroup = $swapOutGroup;
        }
        
        // Does student exist and group exist
        if ($db->exams->doesStudentExistInGroup($swapInExamStudent, $swapInGroup)) {
           $swappedStatus = $db->students->swapStudents(
                                $swapOutStudent,
                                $swapOutGroup,
                                $swapInExamStudent,
                                $swapInGroup,    //(optional)
                                $swapOutStation //(optional)
                             );
           
           // To be used in return status/error messages
           $placeHolderMessage = ($swapStationResults) ? 'Station results' : 'Students';
           
           // No results found
           if ($swapStationResults && $swappedStatus['no_results_found']) {
             
             $_SESSION['swap_student'] = [
               'error' => true,
               'message' => "No results found in stations to swap" 
             ];              
               
           } else if ($swappedStatus['success']) {
           
             // Get report information
             $studentSwapOutRecord = $db->students->getStudent($swapOutStudent);
             $studentSwapInRecord = $db->students->getStudent($swapInExamStudent);
           
             $swapOutGroupRecord = $db->exams->getGroup($swapOutGroup);
             $swapOutGroupName = $swapOutGroupRecord['group_name'];
           
             $outGroupSessionID = $db->exams->getSessionIDByGroupID($swapOutGroup);
             $swapOutSessionRecord = $db->exams->getSession($outGroupSessionID);
             $swapOutSessionName = $swapOutSessionRecord['session_description'] . ' - ' 
                                 . formatDate($swapOutSessionRecord['session_date']);       
         
             $swapInGroupRecord = $db->exams->getGroup($swapInGroup);
             $swapInGroupName = $swapOutGroupRecord['group_name'];
           
             $inGroupSessionID = $db->exams->getSessionIDByGroupID($swapOutGroup);
             $swapInSessionRecord = $db->exams->getSession($inGroupSessionID);
             $swapInSessionName = $swapInSessionRecord['session_description'] . ' - ' 
                                . formatDate($swapInSessionRecord['session_date']);             
           
             $studentSwapOutName = $studentSwapOutRecord['surname'].', '.$studentSwapOutRecord['forename'];
             $studentSwapInName = $studentSwapInRecord['surname'].', '.$studentSwapInRecord['forename'];
           
             // Swap information for user            
             $swapinData = [
                'id' => $swapInExamStudent,
                'name' => $studentSwapInName,
                'group' => $swapInGroupName,
                'session' => $swapInSessionName
             ];
            
             $swapoutData = [
                'id' => $swapOutStudent,
                'name' => $studentSwapOutName,
                'group' => $swapOutGroupName,
                'session' => $swapOutSessionName
             ];
             
             // Station swap has taken place
             if ($db->exams->doesStationExist($swapOutStation)) {
                $swapType = SWAP_STATION_RESULTS;
                
                $outStationRecord = $db->exams->getStation($swapOutStation);
                $swapoutData['station'] = $outStationRecord['station_number'].' - '.$outStationRecord['form_name'];
                $swapinData['station'] = $swapoutData['station'];
             } else {
                $swapType = SWAP_STUDENTS_EXAM;
              
                // Since we've had a successful swap, then swap the students selected
                $_SESSION['swap_criteria'][1] = $swapInExamStudent;
                $_SESSION['swap_criteria'][3] = $swapOutStudent;
                
             }
             
             $_SESSION['swap_student'] = [
                'error' => false,
                'message' => "$placeHolderMessage successfully swapped",
                'swapin' => $swapinData,
                'swapout' => $swapoutData,
                'swaptype' => $swapType
             ];            
             
          } else {
             $_SESSION['swap_student'] = [
               'error' => true,
               'message' => "Failed to swap " . strtolower($placeHolderMessage) . ", please try again" 
             ];
          }
          
        } else {
           $_SESSION['swap_student'] = [
               'error' => true,
               'message' => 'Swap in student not found' 
           ];
        }
         
     } else {
        $_SESSION['swap_student'] = [
             'error' => true,
             'message' => 'Swap out group/student undefined' 
        ];
     }
     
     header("Location: $returnUrl");
 
 // Template Cancel / Update
 } elseif (isset($_POST['template_submit'])) {

     // Update template in database
     if (isset($_POST['template_update'])) {

         $templateUpdated = false;
         
         // Email body HTML wrapper code
         $emailBody = "<html><head><style> p { font-family: Verdana; font-size: 12px; } </style></head><body>" .
                 "<p>" . $_POST['hello'] . " [forename] [surname],</p>" .
                 $_POST['body_content'] .
                 "</body></html>";
 
         // Update template in database
         $templateUpdated = $db->feedback->updateEmailTemplate(
                 $_POST['template'],
                 [
                    'name' => $_POST['tempname'],
                    'cc' => $_POST['cc'],
                    'subject' => "[student_id] " . $_POST['subject'],
                    'body' => $emailBody,
                    'instructions' => $_POST['instructions_content'],
                    'instructions_enabled' => $_POST['instructions_enabled']
                 ]
         );
 
         header('Location: ../manage.php?page=template_tool' 
              . '&updated=' . $templateUpdated 
              . '&template=' . $_POST['template']
         );

     // Edit template redirect
     } elseif (isset($_POST['template_edit'])) {

         header(
            'Location: ../manage.php?page=template_tool&mode=edit&template=' 
                . $_POST['template']
         );

     } else {
       // If none of the above return to index page      
       header('Location: ../');
     }
 } else {
     // If none of the above return to index page      
     header('Location: ../');
 }
 