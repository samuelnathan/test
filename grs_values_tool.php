<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 15/12/2015
 * © 2015 Qpercom Limited. All rights reserved.
   Manage Global Rating Scale Values
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

//Use Role Auth Class
use \OMIS\Auth\Role as Role;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
$user_role = Role::loadID($_SESSION['user_role']);
if (!$user_role->canAccess()) {
    return false;
}

// GRS Management feature enabled? Then allow user access to feature controls
$grsManagementFeature = $db->features->enabled('grs-management', $_SESSION['user_role']);
$returnButton = $grsManagementFeature;

// Scale Type ID
$scaleTypeID = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRIPPED);

// Scale type information
$scaleTypeDB = $db->forms->getRatingScaleTypes($scaleTypeID);

// If empty array is returned then Scale Type does not exist
if (empty($scaleTypeDB)) {
  echo "<br/><br/>Scale Type does not exist, please return to previous section";
  return;
}

$sectionTitle = "Manage Global Rating Scale Values";        
$sectionDescription = "Add, edit and remove Global Rating Scale values for assessment forms";

$additionalInfoData = ["GRS ID" => $scaleTypeDB['scale_type_id'],
                       "GRS Name" => $scaleTypeDB['scale_type_name']];

$recordListTitles = ['GRS Value', 'Description', 'Borderline', 'Fail'];

/* 
 * Two types of fields [text, link]
 * Text: field is displayed as plain text
 * Link: field value is displayed as a URL link to another (sub) section
 */
$field1 = ['type' => 'text-center']; 
$field2 = ['type' => 'text'];
$field3 = ['type' => 'yes-icon'];
$field4 = ['type' => 'yes-icon'];
$fieldTypes = [$field1, $field2, $field3, $field4];

// Pass to template some additional hidden fields
$hiddenFields = ['scale_type_id' => $scaleTypeID];

// Get ratings values from database
$grsValuesDB = $db->forms->getRatingScaleValues(
        $scaleTypeID,
        NULL,
        false,
        'scale_value_id'
);

$editMessage = "Edit global rating scale value";
$listEmptyMessage = "No Global Rating Scale Values found, " .
                    "please click the 'add' button to<br/>add " .
                    "a new Global Rating Scale Value.";
        
// Load and render the section description template
$sectionDescriptionData = ["title" => $sectionTitle,
                           "description" => $sectionDescription,
                           "section_text" => $additionalInfoData,
                           "return_button" => $returnButton];        
        
$sectionDescriptionTemplate = new OMIS\Template('section_top_description.html.twig');
$sectionDescriptionTemplate->render($sectionDescriptionData);


// Only if feature is enabled
if ($grsManagementFeature) {

   // Load and render the records list template
   $recordListData = ["titles" => $recordListTitles,
                      "records" => $grsValuesDB,
                      "field_types" => $fieldTypes,
                      "additional_hidden_fields" => $hiddenFields,
                      "edit_message" => $editMessage,
                      "list_empty_message" => $listEmptyMessage];

   $recordListTemplate = new OMIS\Template('records_list.html.twig');
   $recordListTemplate->render($recordListData);

// Appropriate error message
} else {
   
   $message = "You do not have access to this feature.<br/>" .
              "Please contact your administrator for further assistance";
   
   $returnUrl = "manage.php?page=admin_tool";
   
   $templateData = ['message' => $message,
                    'return_url' => $returnUrl,
                    'content_wrapper' => true]; 
   $errorTemplate = new OMIS\Template('error.html.twig');
   $errorTemplate->render($templateData);
   
}
