<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Template as Template;
 
defined("ASSESSMENT_COMPLETE") or define("ASSESSMENT_COMPLETE", 1);
defined("INCLUDE_ABSENT_RESULTS") or define("INCLUDE_ABSENT_RESULTS", false);
defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("STUDENT_ID_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");
defined("TOASSESS_LIST") or define("TOASSESS_LIST", 'toassess');
defined("INCOMPLETE_LIST") or define("INCOMPLETE_LIST", 'incomplete');
defined("ASSESSED_LIST") or define("ASSESSED_LIST", 'assessed');


// Critical Session Check
$loginSession = new OMIS\Session;
$loginSession->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

 return false;

}

// Is admin role
$adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
$userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);

require_once __DIR__ . "/assess_func.php";

// Is logged in user assigned to current exam
$userAssignedToSession = $db->exams->isUserAssignedSession(
     $_SESSION['user_identifier'],
     $_SESSION['session_id']
);

// Get current examiner ID
$examinerID = getCurrentExaminerID($userAssignedToSession);

// Student selection section return url
$studentSelectionUrl = "assess.php?p=student_selection";

// Concatenate examiner url with student url
$studentSelectionUrl .= examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID);

// Check if exam session variables are set
if (!isset($_SESSION['session_id']) && !isset($_SESSION['station_id'])) {
    Analog::info(
        __FILE__ . ": Session and/or station ID not in session. Aborting."
    );
    header("Location: ./assess.php?p=exam_configuration");
    exit();
}

// form content
$content = null;
$optionCount = null;
$hasItems = null;
$sessionID = null;
$stationID = null;
$groupID = null;
$formID = null;
$studentIDs = [];
$studentID = null;
$isMultipleCandidate = false;
$updateMode = null;

// state of the page
$listType = null;

// Get session id, station id and group id from the session
$sessionID = $_SESSION['session_id'];
$stationID = $_SESSION['station_id'];
$groupID = $_SESSION['group_id'];

/**
* Sets the state of the form
*
* Const Definitions:
* TOASSESS_LIST = toassess
* INCOMPLETE_LIST = incomplete
* ASSESSED_LIST = assessed
*/
if (isset($_REQUEST['dotest'])) {

 $listType = TOASSESS_LIST;
 $assessmentState = 'unsubmitted';
 $studentID = $_REQUEST["toassess"];

} else if (isset($_REQUEST['complete'])) {

 $listType = INCOMPLETE_LIST;
 $assessmentState = 'submitted';
 $studentID = $_REQUEST["incomplete"];

} else if (isset($_REQUEST['retest'])) {

 $listType = ASSESSED_LIST;
 $assessmentState = 'submitted';
 $studentID = $_REQUEST["assessed"];

} else {

 error_log(__FILE__ . ": Don't know what this script is meant to do. Aborting.");
 header("Location: $studentSelectionUrl");
 exit();

}

$isMultipleCandidate = $config->exam_defaults['multiple_candidate'];

$studentID = array_map(function ($studentID) {
    return trim(filter_var($studentID, FILTER_SANITIZE_STRING));
}, $studentID);

if (!$isMultipleCandidate) {
    $studentID = $studentID[0];
}
  
// checks whether the state of the form is set to update mode
$updateMode = (
    in_array(
        $listType,
        [INCOMPLETE_LIST, ASSESSED_LIST]
    )
);

// get station from the station id set in $_SESSION
$station = $db->exams->getStation($stationID);
if (!$station) {
 error_log(__FILE__ . ": Can't get station details. Aborting.");
 header("Location: ./assess.php?p=exam_configuration");
 exit();
}

// get session data from the session id set in $_SESSION;
$session = $db->exams->getSession($sessionID);
if (!$session) {
    error_log(__FILE__ . ": Can't get day/circuit details. Aborting.");
    header("Location: ./assess.php?p=exam_configuration");
    exit();
}

// get form id from station
$formID = $station['form_id'];

// check if popup have notes or video enable
$get_notes_video = $db->forms->getForm($formID);
$notes_video = $get_notes_video['notes_video'];

// check if the station has notes
$hasNotes = (strlen($station['form_description']) > 0);

$session['circuit_colour_name'] = strlen($session['circuit_colour']) > 0 ?
    $db->exams->getColourName($session['circuit_colour']) : "";
 
// Get exam record
$exam = $db->exams->getExam($session['exam_id']);

// Get student group
$group = $db->groups->getGroup($groupID);
 
// More exam settings
$settings = $db->examSettings->get($session['exam_id']);
$examStudentIDType = $settings['exam_student_id_type'];

// get form content from the form ID
list($content, $optionCount, $hasItems) = getFormContent($formID);

// Add grid positions
// *adds position key to the content array
$content = addGridPositions($content);

/**
 * Visual Alarm Times/settings
 */
$alarm1Time = $station['visual_alarm1'];
$alarm2Time = $station['visual_alarm2'];
$alarmDuration = $config->station_defaults['alarmduration'];
$alarm1Stop = date("H:i:s", strtotime("-$alarmDuration seconds", strtotime($alarm1Time)));
$alarm2Stop = date("H:i:s", strtotime("-$alarmDuration seconds", strtotime($alarm2Time)));

 if ($isMultipleCandidate) {
     $cache = new \OMIS\Imaging\Cache($config->imagecache["caches"]["students"]);

     $state = $listType;
     $studentIDs = $studentID;
     $resultID = null;
     $absentVal = null;
     $autoSave = $config->assessment['autosave_interval'];
     $showNotes = ($station['desc_auto_popup'] == 1) && ($state == TOASSESS_LIST) ? 1 : 0;

     $assessmentFormService = new \OMIS\Services\AssessmentFormService(
         $db->students,
         $db->groups,
         $db->results,
         $db->stations
     );

     if (!is_null($assessmentFormService->existStudents($studentIDs))) {
         header("Location: $studentSelectionUrl");
         exit();
     }

     $students = [];
     // get students name according to the exam setting
     //TODO: test each case
     switch ($examStudentIDType) {
         case EXAM_NUMBER_TYPE:

            $students = $assessmentFormService->getStudentsExamNumber(
                $studentIDs,
                $groupID
            );
            break;
         case CANDIDATE_NUMBER_TYPE:

             $students = $assessmentFormService->getStudentsCandidateNumber(
                 $studentIDs,
                 $_SESSION["cterm"]
             );
             break;
         default:

             $students = $assessmentFormService->getStudentsName(
                 $studentIDs,
                 $_SESSION["cterm"]
             );
             break;
     }

     $students = array_map(function ($student) use ($config, $cache) {
         $studentImage = $student["student_image"];

         $imagePath = trim($studentImage) != '' && $cache->contains($studentImage) ?
             $studentImage :
             basename($config->imagecache['images']['default']);

         $thumbnailPath = $cache->getThumbnail($imagePath);

         // Make sure the student details know what's going on.
         $thumbnailSrc = \OMIS\Imaging\Image::getImageAsInline($thumbnailPath);

         $student['thumbnail_src'] = $thumbnailSrc;
         return $student;
     }, $students);


     // Get self assessment
     foreach ($students as $student){
       list($content, $ownership, $hasSelfAssessmentQuestions, $isSelfAssessment) = getSelfAssessmentItems(
         $content, $student['studentId'], $session['exam_id']
       );
     }

     // retrieving data according with the state
     switch ($state) {
         case TOASSESS_LIST:

             $haveResult = $assessmentFormService->doesStudentsHaveResult(
                 $studentIDs,
                 $sessionID,
                 $station['station_number'],
                 $stationID,
                 $examinerID,
                 INCLUDE_ABSENT_RESULTS,
                 ASSESSMENT_COMPLETE
             );

             if ($haveResult) {
                 header("Refresh: 3; url='$studentSelectionUrl'");
                 echo ucwords(gettext('student')) . " already " . gettext('student') . ", redirecting in 3 seconds...";
                 exit();
             }

             foreach ($students as &$student) {
               list($scoreData, $score) = getScoreData($formID, null);
               $student['scores'] = $scoreData;
               $student['score'] = $score;
               // remove the reference of student
               unset($student);
             }

             break;
         default:

            $studentsWithManyResults = [];
            foreach ($students as &$student) {

                 $resultsQuery = $db->results->getResults(
                     $student['studentId'],
                     $stationID,
                     $examinerID
                 );

                if ($resultsQuery && (mysqli_num_rows($resultsQuery) == 1)) {
                    $results = $db->fetch_row($resultsQuery);
                } else {
                  $studentsWithManyResults[] = $student['id'];
                }

                // First submission
                $firstSubmission = explode(" ", $results['created_at']);
                $firstSubmission = formatDate($firstSubmission[0]) . " " . $firstSubmission[1];

                // Last Updated
                $lastUpdated = explode(" ", $results['updated_at']);
                $lastUpdated = formatDate($lastUpdated[0]) . " " . $lastUpdated[1];

                $result = [
                    'id' => $results['result_id'],
                    'absent' => $results['absent'],
                    'firstSubmission' => $firstSubmission,
                    'lastUpdated' => $lastUpdated
                ];

                list($scoreData, $score) = getScoreData($formID, $results['result_id']);

                $student['result'] = $result;
                $student['scores'] = $scoreData;
                $student['score'] = $score;

                // remove the reference of student
                unset($student);
            }

            if (!empty($studentsWithManyResults)) {
                Analog::info(
                    "Students " . json_encode($studentsWithManyResults) . "have no or too many results. aborting"
                );
                header("Location: $studentSelectionUrl");
                exit();
            }

            break;
     }

    $template = new Template("multiple_candidate_scoresheet.html.twig");
    $template->render([
     'session' => $session,
     'content' => $content,
     'optionCount' => $optionCount,
     'examiner' => $examinerID,
     'station' => $station,
     'group' => $group,
     'exam' => $exam,
     'updateMode' => $updateMode,
     'state' => $assessmentState,
     'possible' => round($station['total_value']),
     'examiner' => $examinerID,
     'sectionCount' => count($content),
     'settings' => [
         'hasNotes' => $hasNotes,
         'showNotes' => $showNotes,
         'listType' => $state,
         'feedback_to_right' => $settings['feedback_to_right'],
         'preserve_group_order' => $settings['preserve_group_order']
     ],
     'stationTime' => $station['station_time'],
     'alarm1Time' => $alarm1Time,
     'alarm2Time' => $alarm2Time,
     'alarm1Stop' => $alarm1Stop,
     'alarm2Stop' => $alarm2Stop,
     'students' => $students,
     'hasItems' => $hasItems,
     'firstSubmission' => isset($firstSubmission) ? $firstSubmission : "",
     'lastUpdated' => isset($lastUpdated) ? $lastUpdated : "",
     'autosaveInterval' => $autoSave,
      'selfAssessment' => [
        'ownership' => $ownership,
        'hasQuestions' => $hasSelfAssessmentQuestions,
        'isSelfAssessment' => $isSelfAssessment
      ]
    ]);

 } else {

 
 // If student does not exist then return to list ?
 if (in_array($studentID, ["none", ""]) || !$db->students->doesStudentExist($studentID)) {
   
     error_log(__FILE__ . ": Student ID is not set or student doesn't exist. Aborting.");
     header("Location: $studentSelectionUrl");
     exit();
     
 } else {
   
     // Get Student Details
     $student = $db->students->getStudent(
         $studentID, 
         $_SESSION["cterm"]
     );

     $_SESSION['current_student_id'] = $studentID;
          
     $idSeparated = strlen($student['surname']) > 0 || strlen($student['forename']) > 0 ? " - " : "";
     $nameSeparated = strlen($student['surname']) > 0 && strlen($student['forename']) > 0 ? ", " : "";
     $fullName = $student['surname'] . $nameSeparated . $student['forename'];
     $studentGroup = $db->exams->getStudentGroupRecord($studentID, $groupID);
         
     if ($examStudentIDType == EXAM_NUMBER_TYPE) {

        $nameAndID = $studentGroup["student_exam_number"];
        
     } else if ($examStudentIDType == CANDIDATE_NUMBER_TYPE) {
       
        $nameAndID = $fullName . $idSeparated . 
        (empty($student["candidate_number"]) ? "unknown" : $student["candidate_number"]); 
        
     } else {
       
        $nameAndID = $fullName . $idSeparated . $studentID;
        
     }
     
 }
 
 // To Assess List
 if ($listType == TOASSESS_LIST) {
     
     // If to filter by station_id
     $multiScenarioStation = $db->exams->multiScenarioStation($stationID);
     $filterByStationID = ($multiScenarioStation) ? $stationID : NULL;
     
     /**
      *  Has a student already got a submitted result for this station?
      *  If so then abort the assessment
      */
     $studentHasResults = $db->results->doesStudentHaveResult(
           $studentID,
           $sessionID,
           $station['station_number'],
           $filterByStationID,
           $examinerID,
           INCLUDE_ABSENT_RESULTS,
           ASSESSMENT_COMPLETE
     );
     
     if ($studentHasResults) {
       
         error_log(__FILE__ . ": Student already examined. Aborting..");
         header("Refresh: 3; url='$studentSelectionUrl'");
         echo ucwords(gettext('student')) . " already " . gettext('student') . ", redirecting in 3 seconds...";
         exit();
         
     }

     $resultID = null;
     $absentVal = null;
     
 // Assessed/Incomplete List
 } else {
 
     // Get result row
     $results = $db->results->getResults(
         $studentID,
         $stationID, 
         $examinerID
     );
 
     if ($results && (mysqli_num_rows($results) == 1)) {
       
         $results = $db->fetch_row($results);
         
     } else {
       
         error_log(__FILE__ . ": Student has no or too many results. Aborting.");
         header("Location: $studentSelectionUrl");
         exit();
         
     }
 
     $resultID = $results['result_id'];
     $absentVal = $results['absent'];
     
     // First submission
     $firstSubmission = explode(" ", $results['created_at']);
     $firstSubmission = formatDate($firstSubmission[0]) . " " . $firstSubmission[1];
 
     // Last Updated
     $lastUpdated = explode(" ", $results['updated_at']);
     $lastUpdated = formatDate($lastUpdated[0]) . " " . $lastUpdated[1];
     
 }
 
// Assessed and Absent
$assessedAndAbsent = ($updateMode && isset($absentVal) && $absentVal == 1);

// Show Popup true/false = 1/0 when specified or when reviewing form and student is absent
$showNotes = ($station['desc_auto_popup'] == 1) && ($listType == TOASSESS_LIST || $assessedAndAbsent) ? 1 : 0;

// Get form score data
list($scoreData, $score) = getScoreData($formID, $resultID);

 // Get self assessment
list($content, $ownership, $hasSelfAssessmentQuestions, $isSelfAssessment) = getSelfAssessmentItems(
    $content, $studentID, $session['exam_id']
);

 list($scoreData, $score) = getScoreData($formID, $resultID);

 // Examiner ID
 $currentExaminerID = $listType == TOASSESS_LIST ? $examinerID : $results['examiner_id'];

 ob_end_flush();

 $get_station_type = $db->sessions->getStationType($station['station_id']);
$station_type = $get_station_type['station_type'];

$get_student_media = $db->sessions->get_student_media($formID,$studentID);
$ext = pathinfo($get_student_media, PATHINFO_EXTENSION);

// if($ext == "txt"  AND $get_student_media != ""){
//         $file_text = fopen("media/upload_media/".$get_student_media,"r");
// while(! feof($file_text))
//   {
//   $text_data[] = fgets($file_text). "<br />";
//   }
// fclose($file_text);
// }

$rand = rand();

 $panelTemplate = new Template("exam_assessment_TopPanel.html.twig");

 // Render presentation layer
 $panelTemplate->render([
    'updateMode' => $updateMode,
    'exam' => $exam,
    'session' => $session,
    'station' => $station,
    'examiner' => $currentExaminerID,
    'group' => $group,
    'station_type' => $station_type,
    'student' => [
        'id' => $studentID,
        'comboName' => $nameAndID
     ],   
     'rand' => $rand, 
    'state' => $assessmentState,
    'assessedAbsent' => $assessedAndAbsent,
    'settings' => [
       'hasNotes' => $hasNotes,
       'showNotes' => $showNotes,
       'listType' => $listType,
       'notes_video' => $notes_video,
       'upload_media_file' => $get_student_media,
       'upload_media_ext' => $ext,
       'upload_media_txt' => $text_data,
    ],
    'firstSubmission' => isset($firstSubmission) ? $firstSubmission : "",
    'lastUpdated' => isset($lastUpdated) ? $lastUpdated : ""
 ]);

 $formTemplate = new Template("exam_assessment_Form.html.twig");

 $formTemplate->render([
    'updateMode' => $updateMode,
    'absent' => $absentVal,
    'exam' => $exam,
    'session' => $session,  
    'station' => $station,
    'group' => $group,
    'student' => [
       'id' => $studentID,
       'comboName' => $nameAndID
    ],
    'content' => $content,
    'scores' => $scoreData,
    'examiner' => $currentExaminerID,
    'settings' => [
       'hasNotes' => $hasNotes,
       'showNotes' => $showNotes,
       'listType' => $listType, 
       'feedback_to_right' => $settings['feedback_to_right'],
       'preserve_group_order' => $settings['preserve_group_order']      
    ],
    'state' => $assessmentState,
    'assessedAbsent' => $assessedAndAbsent,
    'optionCount' => $optionCount,
    'selfAssessment' => [
        'ownership' => $ownership,
        'hasQuestions' => $hasSelfAssessmentQuestions,
        'isSelfAssessment' => $isSelfAssessment
    ]
 ]);


 // Get the NEXT list of students (for footer)
 $students = getNextList(
     $sessionID, $groupID, $station,
     $examinerID, $studentID
 );

 $footerTemplate = new Template("exam_assessment_Footer.html.twig");

 $footerTemplate->render([
     'exam' => $exam,
     'updateMode' => $updateMode,
     'state' => $assessmentState,
     'optionCount' => $optionCount,
     'stationTime' => $station['station_time'],
     'alarm1Time' => $alarm1Time,
     'alarm2Time' => $alarm2Time,
     'alarm1Stop' => $alarm1Stop,
     'alarm2Stop' => $alarm2Stop,
     'listType' => $listType,
     'hasItems' => $hasItems,
     'autosaveInterval' => $config->assessment['autosave_interval'],
     'score' => $score,
     'possible' => round($station['total_value']),
     'sectionCount' => count($content),
     'examStudentIDType' => $examStudentIDType,
     'studentID' => $studentID,
     'students' => $students
 ]);

 }
 ?>
