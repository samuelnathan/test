<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

use \OMIS\Template as Template;

define('_OMIS', 1);

$exam_id = filter_input(INPUT_POST, 'exam_id', FILTER_SANITIZE_NUMBER_INT);
if (is_null($exam_id)) {
    exit();
}

include __DIR__ . '/../extra/essentials.php';

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

$sessionsResult = $db->exams->getExamSessions($exam_id);
$sessions = [];
if (mysqli_num_rows($sessionsResult) > 0) {
  
    while ($session = $db->fetch_row($sessionsResult)) {
      
        if ($session['session_published'] == 1) {
            
            // Colour from DB
            $colour = $session['circuit_colour'];
            
            // Colour circuit feature enabled and colour defined
            if (!empty($colour)) {
              
                // Colour from DB
                $colourName = $db->exams->getColourName($colour);
                
                $sessions[] = [
                    'id' => $session['session_id'],
                    'date' => formatDate($session['session_date']),
                    'description' => $session['session_description'],
                    'colour_name' => $colourName, 
                    'colour_hex' => $colour
                ];
                
            } else {
              
                $sessions[] = [
                    'id' => $session['session_id'],
                    'date' => formatDate($session['session_date']),
                    'description' => $session['session_description']
                ];
                
            }
        }
    }
}

$data = [
    'sessions' => $sessions
];

/* Adding an exception handling mechanism in case there's an issue loading the
 * template.
 */

try {
    $template = new Template(Template::findMatchingTemplate(__FILE__));
} catch (RuntimeException $e) {
    $error_message = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error_message");
}

$template->render($data);
