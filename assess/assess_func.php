<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
 use \OMIS\Auth\RoleCategory as RoleCategory;
 
 /**
  * Order Students For Station
  *
  * @param type $divider
  * @param type $areswithout_arr
  */
 function orderStudentsStation($divider, &$areswithout_arr)
 {
     $firstarr = [];
     $secondarr = [];
 
     foreach ($areswithout_arr as $rw) {
         if ($rw['student_order'] <= $divider) {
             $firstarr[] = $rw;
         } else {
             $secondarr[] = $rw;
         }
     }
     $areswithout_arr = array_merge($firstarr, $secondarr);
 }
 
 function getItemWithMaxOptions(&$options, $maxSectionOptions)
 {
     
     foreach ($options as $option) {
       
         if (count($option['options']) == $maxSectionOptions) {
           
             return [
                'type' => $option['type'], 
                'options' => $option['options']
             ];
             
         }
         
     }
     
 }
 
 function compareAnsOptions($option1, $option2)
 {
     return (strcasecmp(trim($option1), trim($option2)) == 0);
 }
 
 function inMainArray($main_arr, $ansrw, $main_cnt)
 {
     $new_cnt = 0;
     $foundcnt = 0;
     foreach ($main_arr as $eachr) {
         $new_cnt++;
         if ($new_cnt <= $main_cnt) {
             continue;
         }
         if (compareAnsOptions($eachr['descriptor'], $ansrw['descriptor'])) {
             $foundcnt++;
         }
     }
 
     return ($foundcnt == 1);
 }
 
 function inMainArray_tempFix($descriptors, $optionDescriptor, $mainCount)
 {
     
     $newCount = 0;
     $foundCount = 0;
     
     foreach ($descriptors as $descriptor) {
       
         $newCount++;
         
         if ($newCount <= $mainCount) {
           
             continue;
             
         }
         
         if (compareAnsOptions($descriptor, $optionDescriptor)) {
           
             $foundCount++;
             
         }
         
     }
 
     return ($foundCount == 1);
     
 }
 
 function repeatedAns_tempFix($options, $currentDescriptor)
 {
     
     $repeatCount = 0;
     foreach ($options as $option) {
       
         if (compareAnsOptions($option['descriptor'], $currentDescriptor)) {
           
             $repeatCount++;
             
         }
         
     }
 
     return ($repeatCount > 1);
     
 }
 
 function repeatedAns($q_anrs, $option_text)
 {
     $repeatcnt = 0;
     foreach ($q_anrs as $q_ans) {
         if (compareAnsOptions($q_ans['descriptor'], $option_text)) {
             $repeatcnt++;
         }
     }
 
     return ($repeatcnt > 1);
 } 
 
 /*
  * Load assistance call box
  */
 function renderAssistantCallBox($config)
 {
     $data = [
         'assistants' => [],
         'station_id' => $_SESSION['station_id']
     ];
 
     // Get database instance
     $db = \OMIS\Database\CoreDB::getInstance();
     $sessionAssistants = $db->exams->getSessionAssistants($_SESSION['session_id']);
 
     while ($assistant = $db->fetch_row($sessionAssistants)) {
         $data['assistants'][] = $assistant;
     }
 
     $template = new \OMIS\Template("renderAssistantCallBox.html.twig");
     $template->render($data);
 }
 
 /**
  * Prepare form content
  * @param int $id number identifier for form
  * @param int $id number identifier for result (if exists)
  * 
  * @return mixed[] the form scoring data 
  */
 function getScoreData($id, $resultID) 
 {
   
    global $db;
    $totalScore = "";
    $scoreData = []; 

    $sections = $db->forms->getFormSections([$id], true, ['section_id']);

    foreach ($sections as $id => $section) {

        // Get section feedback
        $scoreData[$id]['feedback'] = $db->feedback->getSectionFeedback(
              $id, 
              $resultID
        );

        // Section Default
        $scoreData[$id]['total'] = 0;
        $scoreData[$id]['gradeable'] = false;
        $scoreData[$id]['items'] = [];

        // Get section items
        $items = $db->forms->getSectionItems([$id]);

        // Adding items per section
        $itemsTemp = [];
        
        while ($item = $db->fetch_row($items)) {

            $itemID = $item['item_id'];

            // Get item feedback
            $itemsTemp[$itemID]['feedback'] = $db->feedback->getItemFeedback(
                $itemID, $resultID
            );

            $itemsTemp[$itemID]['high'] = (float)$item['highest_value'];
            $itemsTemp[$itemID]['low'] = (float)$item['lowest_value'];

            $itemsTemp[$itemID]['trueHigh'] = max(
                $itemsTemp[$itemID]['low'], 
                $itemsTemp[$itemID]['high']
            );

            $itemsTemp[$itemID]['trueLow'] = min(
                $itemsTemp[$itemID]['low'], 
                $itemsTemp[$itemID]['high']
            );
            
            $itemScore = $db->results->getItemScore($itemID, $resultID);
            $itemsTemp[$itemID]['gradeable'] = !in_array($item['type'], ["checkbox", "radiotext"]);
            $itemsTemp[$itemID]['marked'] = isset($itemScore['option_value']);
            $itemsTemp[$itemID]['value'] = isset($itemScore['option_value']) ? $itemScore['option_value'] : "";
            $itemsTemp[$itemID]['optionID'] = isset($itemScore['option_id']) ? $itemScore['option_id'] : "";
                      
            /**
             * Scale (slider) percent
             */
            if ($item['type'] == "slider") {
              
                $sliderPossible = ($itemsTemp[$itemID]['high'] - $itemsTemp[$itemID]['low']);
                $itemsTemp[$itemID]['scalePercent'] = percent($itemsTemp[$itemID]['value'], $sliderPossible, 0);    
                
            }
            
            /**
             * Section is gradeable (can score)
             */
            if ($itemsTemp[$itemID]['gradeable']) {
              
               $scoreData[$id]['gradeable'] = true;
              
            }
                      
            /*
             * Accumulated the score per section and per form/scoresheet
             */
            if ($itemsTemp[$itemID]['gradeable'] && $itemsTemp[$itemID]['marked']) {
              
               $totalScore += $itemsTemp[$itemID]['value'];
               $scoreData[$id]['total'] += $itemsTemp[$itemID]['value'];
              
            }
            
            // Add items to section
            $scoreData[$id]['items'] = $itemsTemp;
            
        }

    }

    return [$scoreData, $totalScore];
   
 }
 
 /**
  * Prepare form content
  * @param int $id number identifier for form
  * 
  * @return mixed[] the form content (sections, items and options) 
  */
 function getFormContent($id) 
 {
 
     global $db;
 
     $content = $db->forms->getFormSections([$id], true);

     $hasItems = false;
     $maxFormOptions = 1;
     
     // Compile list of sections and items
     foreach ($content as &$section) {
         
         $maxSectionOptions = 0;
         $section['items'] = [];
              
         $section['competence_desc'] = ucfirst($section['competence_desc']);
         $section['section_text'] = ucfirst($section['section_text']);
         $section['title'] = implode(' : ', array_filter([
             $section['competence_desc'], 
             $section['section_text']
         ]));
         
         // Get items
         $items = $db->forms->getSectionItems([$section['section_id']]);    

         // Adding items per section
         $itemsTemp = [];
 
         while ($item = $db->fetch_row($items)) {
 
             $hasItems = true;
             $itemID = $item['item_id'];
             $itemsTemp[$itemID]['options'] = [];
             $itemsTemp[$itemID]['order'] = $item['item_order'];
             $itemsTemp[$itemID]['type'] = $item['type'];
             $itemsTemp[$itemID]['grs'] = $item['grs'];

             /**
              * If this is a GRS item then we need to get the type
              * and the values with their borderline and fail information used 
              * in the compulsory feedback feature
              */
             if ($item['grs'] == 1) {
 
                 $itemsTemp[$itemID]['grsValues'] = $db->forms->getRatingScaleValues(
                     $item['grs'],
                     NULL,
                     false,
                     'scale_value'
                 );
                                 
             }
 
             $itemsTemp[$itemID]['text'] = adjustHTML($item['text']);
 
             // Adding answering options per item
             $options = $db->forms->getItemOptions($item['item_id']);
     
             $optionsTemp = [];
     
             while ($option = $db->fetch_row($options)) {
     
                 $optionsTemp[] = [
                     'optionID' => $option['option_id'],
                     'value' => $option['option_value'],
                     'descriptor' => $option['descriptor'],
                     'order' => $option['option_order'],
                     'flag' => $option['flag'],
                     'covered' => 0
                 ];
     
             }
     
             // Record item with max num of options
             if (mysqli_num_rows($options) > $maxSectionOptions) {
                 
                 $maxSectionOptions = mysqli_num_rows($options);
                 $section['maxOptions'] = [
                     'type' => $item['type'],
                     'descriptors' => array_column($optionsTemp, 'descriptor')
                 ];
 
                 // Max options for form count
                 if ($maxSectionOptions > $maxFormOptions) {
                     $maxFormOptions = $maxSectionOptions;     
                 }
                 
             }
     
             $itemsTemp[$itemID]['options'] = $optionsTemp;
             $section['items'] = $itemsTemp;
 
         }
                 
     } 
     
     return [$content, $maxFormOptions, $hasItems];
 
 }
 
 /**
  * Add grid positions
  * @param mixed[]  original content data
  * 
  * @return mixed[] content with grid info added
  */
 function addGridPositions($content) 
 {

      /**
      * Radio Grid (matched with descriptors) for form
      */ 
      foreach ($content as &$section) {
        
        foreach ($section['items'] as $itemID => &$eachItem) {
            
             // Ignore non-radio items
             if (!in_array($eachItem['type'], ['radio', 'radiotext'])) {
               
                continue;
                
             }
            
             $position = 0;  
             
             if ($section['self_assessment']) {
              
               foreach ($eachItem['options'] as &$option) {
                               
                 $option['position'] = ++$position;
                 
               }
               
             } else {
            
             // Max options (descriptors)
             foreach ($section['maxOptions']['descriptors'] as $descriptor) {
               
                 $position++;
             
                 $itemDescriptors = $section['items'][$itemID]['options'];
                 
                 // Add postion to option 
                 foreach ($eachItem['options'] as &$option) {
                   
                     if ($option['covered']) {
                   
                       continue;

                     }
                     
                     // Add position
                     $notePosition = (compareAnsOptions($descriptor, $option['descriptor']) ||
                                    !inMainArray_tempFix($section['maxOptions']['descriptors'], $option['descriptor'], $position) || 
                                     repeatedAns_tempFix($itemDescriptors, $option['descriptor']));
                     
                     if ($notePosition) {
                       
                         $option['covered'] = 1;
                         $option['position'] = $position;
                         
                     }

                     continue 2;
                     
                }
                 
                    
           }
           
        }   
             
      }
      
     }
           
   return $content;  
     
 }
 
 /**
  * Get the next list of student
  *
  * @param int     $id        number identifier for session
  * @param int     $id        number identifier for group
  * @param mixed[] $station   station data
  * @param int     $id        number identifier for examiner
  * @param int     $id        number identifier for student
  * 
  * @return mixed[] list of students
  */
 function getNextList($session, $group, $station, $examiner, $student) 
 {
 
     global $db, $settings;
     $minStationNum = $db->exams->getLowestStationNum($session);
     if (!is_numeric($minStationNum) || $minStationNum < 0) {
       
         $minStationNum = 1;
         
     }
     
     $minStationNum--;
     $divider = $station['station_number'] - $minStationNum;
 
     // Get composite list of students (all)
     $compositeStudentList = $db->exams->getStationExaminerStudents(
         $station['station_id'],
         $group,
         $examiner
     );
 
     // Do we have a composite list? If not then exit.
     if (empty($compositeStudentList)) {
       
        error_log(__FILE__ . ": Student list is empty. This shouldn't happen.");
        return [];
        
     }
 
     // Pull list of students to assess from composite list
     $withoutResults = $compositeStudentList['unexamined'];
 
     // Filter the list, remove the current student
     $finalList = array_filter($withoutResults, function($eachStudent) use($student) {
        
        return($eachStudent['student_id'] !== $student);
        
    });
 
    /**
     * If preserve group ordering is disabled then sort students
     * (by expected order) in group at station
     */
     if ($settings['preserve_group_order']) {
       
             orderArrayColumn($finalList, 'student_order', 'standard', 'ASC');
         
     } else {
       
         // Reorder list of students to assess
         orderArrayColumn($finalList, 'student_order', 'standard', 'DESC');
         orderStudentsStation($divider, $finalList); 
         
     }

     return $finalList;  
 
 }
 
 /**
  * Generate slider
  * 
  * @param type $start
  * @param type $end
  * @param type $id
  * @param type $anscount
  * @param type $offset
  * @param type $sliderType
  * @param type $frameWork Generate Slider
  */
 function generateSlider($start, $end, $id, $anscount, $offset, $sliderType = 'numeric', $frameWork = 'jquery')
 {
     // Standard numeric scoring slider
     if ($sliderType == 'numeric') {
       
         $displayValue = $offset;
 
         // width
         $value_range = ($end - $start);
 
         if ($value_range <= 5) {
             $class = 'slider1';
         } elseif ($value_range <= 10) {
             $class = 'slider2';
         } elseif ($value_range <= 20) {
             $class = 'slider3';
         } elseif ($value_range <= 50) {
             $class = 'slider4';
         } elseif ($value_range <= 100) {
             $class = 'slider5';
         } else {
             $class = 'slider6';
         }
 
         $displayStart = $start;
         $displayEnd = $end;
         $leftParen = "(";
         $rightParen = ")";
 
     // Percentage scoring slider
     } else {
       
         if ($offset == 0) {
           
             $displayValue = "0%";
             
         } else {
           
             $displayValue = percent($offset, ($end - $start), 0) . "%";
             
         }
 
         $class = "perslider";
         $displayStart = "&nbsp;";
         $displayEnd = "&nbsp;";
         $leftParen = "";
         $rightParen = "";
         
     }
 
     $template = new OMIS\Template('generateSlider.html.twig');
 
     $data = [
         'answerCount' => $anscount,
         'leftBracket' => $leftParen,
         'rightBracket' => $rightParen,
         'class' => $class,
         'leftLabel' => $displayStart,
         'rightLabel' => $displayEnd,
         'value' => $displayValue,
         'start' => $start,
         'end' => $end,
         'id' => $id,
         'framework' => $frameWork,
         'offset' => $offset
     ];
 
     $template->render($data);
 }
 
 /**
  * When an admin logs on to the assessment tool and navigates to the student 
  * selection section (station already configured), they have the option
  * to choose an examiner associated with that station and examine as them, these being 
  * examiners that have been assigned and examiners that have assessed
  * at this station. What this function does is return the ID for the examiner selected.
  * 
  * @param bool  $userIsAssigned   User is assigned to station in session true|false
  * @return string                 returns the selected examiner ID, or if none selected, logged in user ID 
  */
 function getCurrentExaminerID($userIsAssigned)
 {
     // Start with the logged in user ID
     $examinerID = $_SESSION['user_identifier'];
 
     // If it's not an exam admin user then abort, return their ID
     $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
     $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);
     
     if (!$userIsAdmin && ($userIsAdmin || $userIsAssigned)) {
         return $examinerID;
     }
 
     // An examiner has been selected from the dropdown (students section) by the admin
     if (isset($_SESSION['station_examiner'])) {
         $db = \OMIS\Database\CoreDB::getInstance();
         if ($db->users->doesUserExist($_SESSION['station_examiner'])) {
             $examinerID = $_SESSION['station_examiner'];
         }
     }
 
     return $examinerID;
 }
 
 /**
  * Set current examiner
  * @param array $examiners        List of associated examiners
  * @param bool  $userIsAssigned   User is assigned to station in session true|false
  * @return string                 returns the selected examiner ID, or if none selected, logged in user ID 
  */
 function setCurrentExaminerID($examiners, $userIsAssigned)
 {
     /* If the current logged in user is an admin (super, system admin or exam admin) then we
      * need to provide them with a list of examiners associated with this station
      * so that they can examine as. Only examiners assigned to the station or 
      * that have submitted results at this station should appear in this list.
      */
 
     // Start with the logged in user ID
     $examinerID = $_SESSION['user_identifier'];
 
     // If it's not an exam admin user then abort, return their ID
     $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
     $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);   
 
     if (!$userIsAdmin || ($userIsAdmin && $userIsAssigned)) {
         return $examinerID;
     }
 
     // Get selected examiner (get type param)
     $examinerParam = filter_input(INPUT_GET, 'examiner', FILTER_SANITIZE_STRING);
 
     // Set the selected examiner if found in list of associate examiners
     if (!is_null($examinerParam) && isset($examiners[$examinerParam])) {
         $examinerID = $examinerParam;
         $_SESSION['station_examiner'] = $examinerID;
     } else {
         unset($_SESSION['station_examiner']);
     }
 
     return $examinerID;
 }
 
 /**
  * Concatenate examiner Url
  * Examiner ID passing and examiner selection only available to exam admins
  * (includes super and system admins). Student lists section.
  * 
  * @param bool  $userIsAdmin      User is exam admin (includes super and system admins) true|false
  * @param bool  $userIsAssigned   User is assigned to station in session true|false
  * @param string  $examinerID     Examiner choosen to 'exam as'
  * @return string                 examiner url
  */
 function examinerUrl($userIsAdmin, $userIsAssigned, $examinerID) {
     
     if ($userIsAdmin && !$userIsAssigned) {
         return "&examiner=" . $examinerID;
     } else {
         return "";
     }
     
 }


function getSelfAssessmentItems($content, $studentID, $examID)
{

    global $db;

    $ownership = null;
    $selfAssessmentID = null;
    $hasSelfAssessmentQuestions = false;
    $isSelfAssessment = false;

    foreach ($content as &$section) {

        foreach ($section['items'] as $itemID => $item) {

            $section['items'][$itemID]['selfAssessmentQuestion'] = [];

            // Adding self assessment question to an item
            if ($section['self_assessment'] == 1) {

                $isSelfAssessment = true;
                $selfAssessmentQuestion = $db->selfAssessments->getQuestionByOrderAndStudent(
                    $examID, $item['order'], $studentID
                );
                $section['items'][$itemID]['selfAssessmentQuestion'] = $selfAssessmentQuestion;

                if ($selfAssessmentQuestion) {

                    $selfAssessmentID = $selfAssessmentQuestion['self_assessment_id'];
                    $hasSelfAssessmentQuestions = true;

                }

            }

        }

        if ($selfAssessmentID) {

            $ownership = $db->selfAssessments->getOwnership($studentID, $selfAssessmentID);

        }

    }

    return [$content, $ownership, $hasSelfAssessmentQuestions, $isSelfAssessment];

}
