<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Miscellaneous\Assessment as Assessment;
use \OMIS\Auth\Role as Role;
define('INCLUDE_OBSERVER_RECORDS', true);

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Get station list
$stationGroups = (new Assessment())->stations($currentSessionID);

// Pass in the relevant data and render the output.
$templateData = ["stations" => $stationGroups];

// Station ID is already set via a session variable
if (isset($_SESSION['station_id'])) {
    $templateData['station'] = $_SESSION['station_id'];
}

try {
    $template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__));
} catch (RuntimeException $e) {
    $error = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error");
}

// Render the template complete with the data it's meant to display.
$template->render($templateData);
