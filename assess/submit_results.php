<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  */

 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
 use \OMIS\Auth\RoleCategory as RoleCategory;

 define('_OMIS', 1);
 define('COMPLETE_FORM', 1);
 define('INCOMPLETE_FORM', 0);
 define('SELF_ASSESSMENT_FORM_OWNERSHIP_NOT_TAKEN', 2);
 define('SELF_ASSESSMENT_FORM_NOT_SUBMITTED', 3);
 define('ABSENT', 1);
 define('ATTENDED', 0);
 define('ASSISTANTS_AS_ARRAY', true);

 $redirectOnSessionTimeOut = true;
 require_once __DIR__ . '/../extra/essentials.php';
 require_once __DIR__ . "/assess_func.php";

 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 if (!isset($config)) {

     $config = \OMIS\Config;

 }
 // Is admin role
 $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
 $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);
 $ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
 $outcomesService = new \OMIS\Outcomes\OutcomesService($db);
 // If Exam form has been Saved | Submitted | Updated
 if (!isset($_POST['submit_action'])) {

     exit();

 } else {

     $submitAction = trim($_POST['submit_action']);
     $autoSave = ($submitAction == 'autosave');

 }

 // If station exists and is assigned to session then get details
 if (!isset($_SESSION['station_id'])) {

     $station = null;

 } else {

     $stationID = $_SESSION['station_id'];
     $station = $db->exams->getStation($stationID, true);

 }

 // No station specified, abort
 if (empty($station)) {

     if (!$autoSave) {

         header("Refresh: 3; url='../assess.php?p=exam_configuration'");
         echo "Station does not exist, redirecting in 3 seconds...";

     }

     exit();

 }

 // No form specified, abort
 if (!$db->forms->doesFormExist($station['form_id'])) {

     if (!$autoSave) {

         header("Refresh: 3; url='../assess.php?p=exam_configuration'");
         echo gettext('Scoresheet') . " does not exist, redirecting in 3 seconds...";

     }
     exit();

 }

 // If to filter by station_id / Multi Scenario Results
 $examId = $_SESSION['exam_id'];
 $exam = $db->exams->getExam($examId);
 $multiScenarioStation = $db->exams->multiScenarioStation($stationID);
 $stationIDFilter = ($multiScenarioStation) ? $stationID : null;
 $studentID = iS($_POST, 'student_id');
 $formStatus = COMPLETE_FORM;
 $logData = [];
 $flagCount = 0;

 // Is logged in user assigned to current exam
 $userAssignedToSession = $db->exams->isUserAssignedSession(
         $_SESSION['user_identifier'],
         $_SESSION['session_id']
 );

 // Get current examiner ID
 $examinerID = getCurrentExaminerID($userAssignedToSession);

 // Return URLs
 $assessmentUrl = "../assess.php?p=exam_assessment&complete=&incomplete=";
 $studentSelectionUrl = "../assess.php?p=student_selection" .
         examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID);

 // Student exists check
 if ($db->students->doesStudentExist($studentID)) {

     // Absent
     $attendance = (is_null(filter_input(INPUT_POST, 'absent', FILTER_DEFAULT)) ? ATTENDED : ABSENT);

     // Total Result
     $totalResult = 0;
     if ($attendance != ABSENT && isset($_POST['total_result']) && is_numeric($_POST['total_result'])) {
         $totalResult = trim($_POST['total_result']);
     }

     // Determine if there's already a record for this student/station/examiner combo.
     $studentResultCount = $db->results->doesStudentHaveResult(
         $studentID,
         $station['session_id'],
         $station['station_number'],
         null,
         $examinerID
     );

     // Adding New Result
     if ($studentResultCount == 0) {

            //updated call to add new student_results with new fields
         $db->results->newInsertResult(
             $stationID,
             $studentID,
             $examinerID,
             $attendance,
             INCOMPLETE_FORM
             );
     // Updating old result
     } else {

         // Get the result ID
         $resultID = $db->results->getResultID(
             $studentID,
             $station['session_id'],
             $station['station_number'],
             $stationID,
             $examinerID
         );

         // Get old result before update
         $resultRecord = $db->results->getResultByID($resultID);
         $oldScore = (float) $resultRecord['score'];

         /**
          * If we have a complete result and somehow a delayed auto-save arrives
          * Then ignore the autosave request and respond with HTTP_FORBIDDEN
          */
         if ($autoSave && $resultRecord['is_complete'] == COMPLETE_FORM) {

             http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
             exit();

         }

         // Remove old item results and feedback
         $db->results->deleteResult($resultID);
         $formId = $station['form_id'];
         //new result update call incorporating new fields
         $possibleWeightedScore = $db->results->getTotalPossibleWeightedScore($formId);
         $score = $db->forms->getForm($formId);
         $possibleRawScore = $score['total_value'];
         $db->results->newUpdateResult(
             $attendance,
             $possibleRawScore,
             $possibleWeightedScore,
             $resultID,
             INCOMPLETE_FORM,
             $examinerID
         );
         $outcomesService->selfAssessmentAdjustment($resultID);

         $db->examdata->setUpToDate($studentID, $examId, false);
     }

     // Not an absent result
     if ($attendance != ABSENT) {

         // Capture feedback for the form per section
         $sections = $db->forms->getFormSections([$station['form_id']]);
         while ($section = $db->fetch_row($sections)) {

             // Save Feedback
             $sectionFeedback = (isset($_POST['comments'][$section['section_id']])) ?
                     trim($_POST['comments'][$section['section_id']]) : "";

             $db->feedback->insertExaminerFeedback(
                     $section['section_id'],
                     $resultID,
                     $sectionFeedback
             );

         }

         // Capture item scores
         $items = $db->forms->getFormItems($station['form_id']);

         while ($item = $db->fetch_row($items)) {

             $itemID = $item['item_id'];

             // Capture feedback/comments per item, if any
             $itemFeedback = (isset($_POST['item_comments'][$itemID])) ?
                     trim($_POST['item_comments'][$itemID]) : "";

             $db->feedback->insertItemFeedback(
                 $itemID, $resultID, $itemFeedback
             );

             // Skip if the item is not self assessment and the examiner is not the ownership
             if ($item['self_assessment']) {

                 $question = $db->selfAssessments->getQuestionByOrderAndStudent(
                     $exam['exam_id'],
                     $item['item_order'],
                     $studentID
                 );

                 if (!empty($question)) {

                     $ownership = $db->selfAssessments->getOwnership($studentID, $question['self_assessment_id']);

                     // Check if there is not ownership for the self assessment and don't let submit
                     if (empty($ownership)) {

                         $formStatus = SELF_ASSESSMENT_FORM_OWNERSHIP_NOT_TAKEN;
                         continue;

                     } else if ($ownership['examiner_id'] !== $examinerID) {

                         // checks whether the result has been submitted to skip to validate the item if the examiner
                         // is not the ownership of the self assessment item
                         if ($db->selfAssessments->hasScoresheetBeenSubmitted($stationID, $studentID)) {
                             continue;
                         }
                         else {
                             $formStatus = SELF_ASSESSMENT_FORM_NOT_SUBMITTED;
                         }

                  }

               }

             }

             // Item score doesn't exist, then overall status of assessment form as 'INCOMPLETE'
             if ($item['type'] == "checkbox" && !isset($_POST['scores'][$itemID])) {
                continue;
             } else if ($item['type'] != "checkbox" && (!isset($_POST['scores'][$itemID]) || strlen($_POST['scores'][$itemID]) == 0)) {
                 $formStatus = INCOMPLETE_FORM;
                continue;
             }

             switch ($item['type']) {
                case 'radio':
                case 'radiotext':
                     $optionID = trim($_POST['scores'][$itemID]);
                     $optionRecord = $db->forms->getOption($optionID);
                     $optionValue = $optionRecord['option_value'];
                     $flagCount += ($optionRecord['flag'] == 1 ? 1 : 0);
                     break;
                case 'checkbox':
                     $optionID = $db->forms->getItemOptionID($itemID);
                     $optionRecord = $db->forms->getOption($optionID);
                     $optionValue = $optionRecord['option_value'];
                     break;
                case 'text':
                case 'slider':
                     $optionID = $db->forms->getItemOptionID($itemID);
                     $optionValue = trim($_POST['scores'][$itemID]);
                     break;
             }

             // Insert answer
             $db->results->insertItemMark(
                 $resultID,
                 $itemID,
                 $optionID,
                 $optionValue
             );
            //set the item scores for weighted items
             $db->results->setItemWeightedScore($itemID, $optionValue, $resultID);
             // Add to log data
             $logData[] = $optionValue;
         }
     }

     // Recalculate Exam Total
     $db->results->recalcStudentResult($resultID);
     $submitAction = strtolower($submitAction);

     $newProgress = $db->exams->getScoringProgressByStationNum($studentID, $station['session_id'], $station['station_number']);
     $config->getMonitorUpdateNotifier($station['session_id'])
            ->updateProgress($studentID, $stationID, $newProgress);

     // Form has been completed and submitted then set the complete field to 1
     if (in_array(strtolower($submitAction), ['submit', 'submit & next', 'update'])) {

         switch ($formStatus) {

          /**
           * Complete assessment, log results
           */
           case COMPLETE_FORM:

             // Update database

             //call to new insert complete result set


             $resultRecord = $db->results->getResultByID($resultID);
             $stationRecord = $db->exams->getStation($stationID);
             $latestScore = (float) $resultRecord['score'];//This is ITEM weighted scores NOT taking into account Interviewers or station weightings for student_results table
             $adjustedScore = $db->results->getStationWeightedItemScores($resultID);
             $stationPossible = $resultRecord['possible'];
             $passValue = $stationRecord['pass_value'];

             $examinerGrade=$outcomesService->UpdateCompleteResults($latestScore, $passValue, $stationPossible, $examId);

             $db->results->insertCompleteResult($examinerGrade,
                 $weightedScore,
                 COMPLETE_FORM,
                 $flagCount,
                 $resultID);
             $db->examdata->setUpToDate($studentID, $examId, false);
             // Log very important update|submit information
             $resultRecord = $db->results->getResultByID($resultID);
             $stationRecord = $db->exams->getStation($stationID);
             $latestScore = (float) $resultRecord['score'];

             if ($submitAction == "update") {
                $resultString = "$oldScore => $latestScore";
                $logTerm = "updated";
             } else {
                $resultString = "$latestScore";
                $logTerm = "submitted";
             }

             // Item values to string
             $valuesString = (count($logData) > 0) ? implode(" ", $logData) : 'no data';

             // Item scores log entry
             \Analog::info(
                  $_SESSION['user_identifier'] . " $logTerm result(ID:$resultID) item values [" . $valuesString
                . "] for student $studentID at station " . $stationRecord['station_number']
                . "-" .$stationRecord['form_name']
             );

             // Overall result log entry
             \Analog::info(
                  $_SESSION['user_identifier'] . " $logTerm result(ID:$resultID) " . $resultString
                . " for student $studentID at station " . $stationRecord['station_number']
                . "-" .$stationRecord['form_name']
             );

             // Notify to the monitor system that the scoring has completed
             $config->getMonitorUpdateNotifier($stationRecord['session_id'])
                    ->updateCompletion($studentID, $stationID, $examinerID, true);

             // Ready sms service
             $sms = new \OMIS\ShortMessageService;
             $recipients = $db->exams->getSessionAssistants(
                   $_SESSION['session_id'],
                   ASSISTANTS_AS_ARRAY
             );

             $numbers = array_map(function($recipient) {
                 return $recipient['contact_number'];
             }, $recipients);


             $canSend = (isset($config->assessment['warning_system_sms'])
             && $config->assessment['warning_system_sms'] && count($numbers) > 0);

             // Log flags in log file and exam notifications
             if ($flagCount > 0) {

                 \Analog::info(
                     $_SESSION['user_identifier'] . " $logTerm result(ID:$resultID) $flagCount "
                   . ngettext('flag', "flags", $flagCount)
                   . " for student $studentID at station " . $stationRecord['station_number']
                   . "-" .$stationRecord['form_name']
                );

                $db->examNotifications->notify($stationID, $studentID, $examinerID, [
                     'flag_type' => 1,
                     'flag_count' => $flagCount
                ]);

                // Send SMS
                $message = $flagCount . " " . ngettext('flag', "flags", $flagCount)
                            . " registered at station #" . $stationRecord['station_number']
                            . " for " . gettext('student') . ": " . $studentID;

                if ($canSend && $sms->send($numbers, $message)) {
                   $db->examNotifications->logSMS(
                        $recipients,
                        $message
                   );
                }

             }

             // Divergence exam notifications
             $divergenceData = $db->examRules->divergenceThresholdExceeded(
                   $_SESSION['exam_id'],
                   $stationID,
                   $studentID
             );

             if (!empty($divergenceData) && $divergenceData['exceeds']) {
                 $db->examNotifications->notify($stationID, $studentID, NULL, [
                     'divergence_type' => 1,
                     'divergence_value' => $divergenceData['divergence'],
                     'divergence_threshold' => $divergenceData['threshold']
                 ]);

                 // Send SMS
                 $message = "divergence threshold of " . round($divergenceData['threshold'], 1)
                          . " exceeded at station #" . $stationRecord['station_number']
                          . " for " . gettext('student') . ": " . $studentID;

                 if ($canSend && $sms->send($numbers, $message)) {
                    $db->examNotifications->logSMS(
                        $recipients,
                        $message
                    );
                 }

             }

             // Grade calculation trigger
             $db->gradeRules->calc($_SESSION['session_id'], $studentID);

             break;

          /**
           * Incomplete assessment, send user back to assessment screen
           */
          case INCOMPLETE_FORM:
             $message = "You have not completed the assessment for " . gettext('student')
                . " with ID: $studentID, returning you to review " . gettext('form') . "...";
             header("Refresh: 4; url='$assessmentUrl$studentID'");
             echo $message;

             error_log(__DIR__ ." $message (Station ID $stationID)");
             exit();
             break;

             case SELF_ASSESSMENT_FORM_OWNERSHIP_NOT_TAKEN:
                 $message = "This is a self assessment " . gettext('form') . " and "
                 . "the ownership has not been taken, in order to submit the scoresheet please take ownership first.";
                 header("Refresh: 4; url='$assessmentUrl$studentID'");
                 echo $message;
                 error_log(__DIR__ ." $message (Station ID $stationID)");
                 exit();
                 break;

             case SELF_ASSESSMENT_FORM_NOT_SUBMITTED:
                 $message = "This is a self assessment " . gettext('form') . " and "
                     . "the ownership has not submitted results yet, please wait the ownership submit result first.";
                 header("Refresh: 4; url='$assessmentUrl$studentID'");
                 echo $message;
                 error_log(__DIR__ ." $message (Station ID $stationID)");
                 exit();
                 break;
        }
   }
 } else {
     // Not Auto Save Mode
     if (!$autoSave) {
         header("Refresh: 3; url='" . $studentSelectionUrl . "'");
         echo "The " . gettext('student') . " you just assessed no longer exists in the system,"
            . " redirecting to " . gettext('student') . " selection section in 3 seconds...";
     }
     exit();
 }

 // Next steps, if to go to next student
 if ($submitAction == "submit & next") {

     $toExamine = trim($_POST['next_student']);

     if (strpos($toExamine, "blank::") !== false) {
        // No checking to be done
     } else if (!$db->students->doesStudentExist($toExamine)) {
         header("Refresh: 3; url='" . $studentSelectionUrl . "'");
         echo "CURRENT " . strtoupper(gettext('student')) . " RESULT SUBMITTED. The next " . gettext('student') . " to be assessed does not exist, "
              . "redirecting to " . gettext('student') . " selection section in 3 seconds...";
         exit();
     } else if ($db->results->doesStudentHaveResult($toExamine, $station['session_id'], $station['station_number'], $stationIDFilter)) {
         header("Refresh: 3; url='" . $studentSelectionUrl . "'");
         echo "CURRENT " . strtoupper(gettext('student')) . " RESULT SUBMITTED. The next " . gettext('student') . " to be assessed has result already, "
            . "redirecting to " . gettext('student') . " selection section in 3 seconds...";
         exit();
     }

     $UL = "../assess.php?p=next&dotest=dotest"
         . "&sexamined=" . $studentID
         . "&toassess[]=" . $toExamine
         . "&station_id=" . $stationID;
     header("Location: $UL");
     exit();

 }
 // Not auto save mode | return to student selection section
 if (!$autoSave && !$ajax) {
     header('Location: ' . $studentSelectionUrl);
 } else if ($ajax) {

   $studentSelectionUrl = "assess.php?p=student_selection" .
     examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID);

   echo json_encode(["url" => $studentSelectionUrl]);
 }
