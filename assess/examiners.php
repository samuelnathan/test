<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Use roles and templating
use \OMIS\Auth\Role as Role;
use \OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

// Page access check / can user access this section?
if (!Role::loadID($_SESSION['user_role'])->canAccess($page)) {
    return false;
}

// Define the roles which we're deeming to be allowed to examine a station.
$examinerRoles = [Role::USER_ROLE_EXAMINER, Role::USER_ROLE_EXAM_ADMIN];

require_once __DIR__.'/assess_func.php';

// Render assessment menu
$enableMenus = ['config', 'change_examiners'];

// Support enabled
$supportEnabled = (isset($_SESSION['sms_support']) && $_SESSION['sms_support'] == 1);

if ($supportEnabled) {
  $enableMenus[] = "support";
}

$templateMenus = new Template("assessment_menu.html.twig");
$templateMenus->render([
   'enable_menus' => $enableMenus,
   'selected_menu' => 'change_examiners',
   "skin" => $config->localization['skin'],
   "name" => [
     "short" => $config->getName(),
     "long" => $config->getName(\TRUE)
   ]
  ] 
);

ob_end_flush();

// Get selected department
$deptExists = $db->departments->existsById($_SESSION['dept_id']);
$selectedDept = (isset($_SESSION['dept_id']) && $deptExists) ? $_SESSION['dept_id'] : null;

/*
 *  Examiners filtering criteria to be passed to the database
 */
$examinersFilters = [
    $selectedDept,       // assessment department
    $_SESSION['cterm'],  // current academic term
    $examinerRoles,      // examiner roles
    true,                // enabled accounts only 
    "",                  // don't filter by forenames 
    "",                  // don't filter by surname
    ""                   // don't filter by search term  
];

// Get list of examiners from the database
$examiners = $db->users->getExaminers(
    $examinersFilters,
    "users.forename"
);

$tryChange = (isset($_REQUEST['error']) && isset($_REQUEST['sel']));
$areErrors = (isset($_REQUEST['error']) && $_REQUEST['error'] == 1);
$success = ($tryChange && !$areErrors);
$examinerSelected = (isset($_REQUEST['sel']) ? $_REQUEST['sel'] : '');

// Render assistant call box if support available
$assistants = [];
if (isset($_SESSION['station_id']) && $supportEnabled) {
    
    $smsSupport = true;
    
    // Gather linked assistances
    $sessionAssistants = $db->exams->getSessionAssistants($_SESSION['session_id']);    
    while ($assistant = $db->fetch_row($sessionAssistants)) {
        $assistants[] = $assistant;
    }
    
} else {
    $smsSupport = false;  
}

// Current examiner name
$examinerName = $_SESSION['user_forename'] . " " 
              . $_SESSION['user_surname'] . " - "
              . $_SESSION['user_identifier'];

$pageTemplate = new Template(Template::findMatchingTemplate(__FILE__));
$pageTemplate->render([
   'enable_menus' => $enableMenus,
   'selected_menu' => 'change_examiners',
   'station_id' => $_SESSION['station_id'],
   'sms_support' => $smsSupport,
   'assistants' => $assistants,
    
   'examiners' => $examiners, 
   'examiner_id' => $_SESSION['user_identifier'],
   'examiner_name' => $examinerName, 
   'examiner_selected' => $examinerSelected,
  
   
   'errors' => $areErrors,
   'success' => $success 
]);
