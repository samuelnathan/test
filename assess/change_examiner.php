<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('_OMIS', 1);
include_once('../extra/essentials.php');

use OMIS\Session as Session;

// Critical Session Check
$session = new Session;
$session->check(Session::SESSION_EXIT_REDIRECT);

require_once __DIR__ . '/../extra/usefulphpfunc.php';

if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

$currentExaminer = filter_input(INPUT_POST, 'cexaminer', FILTER_SANITIZE_STRING);
$newExaminer = filter_input(INPUT_POST, 'new_examiner', FILTER_SANITIZE_STRING);
$examinerPassword = filter_input(INPUT_POST, 'newexmnr_pass', FILTER_SANITIZE_STRING);
$baseUrl = "../assess.php?p=examiners";

if (is_null($currentExaminer) && is_null($newExaminer) && is_null($examinerPassword)) {
    header("Location: $baseUrl&error=1&sel=$newExaminer");
    exit();
}

// Change Examiner Action Perform Tool
if (!in_array("", [$newExaminer, $examinerPassword]) && $db->users->checkPassword($newExaminer, $examinerPassword)) {
    $resultSet = $db->users->getUser($newExaminer);
}

// User record found, set the relevant session variables
if (isset($resultSet) && mysqli_num_rows($resultSet) == 1) {
    $examinerRecord = $db->fetch_row($resultSet);
    $_SESSION['user_identifier'] = $newExaminer;
    $_SESSION['station_examiner'] = $newExaminer;
    $_SESSION['lastlogin'] = $db->users->setTimeStampLogin($newExaminer)[1];
    $_SESSION['user_forename'] = $examinerRecord['forename'];
    $_SESSION['user_surname'] = $examinerRecord['surname'];
    $_SESSION['user_email'] = $examinerRecord['email'];
    $_SESSION['user_role'] = $examinerRecord['user_role'];    
    header("Location: $baseUrl&error=0&sel=$newExaminer");
} else {
    header("Location: $baseUrl&error=1&sel=$newExaminer"); 
}
