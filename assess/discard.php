<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

define('_OMIS', 1);

use \OMIS\Auth\RoleCategory as RoleCategory;

// Critical session check
$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

// Page access check, can user access this section
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Is admin role
$adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
$userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);

// Assessment helper functions
require_once __DIR__ . "/assess_func.php";

$db->startTransaction();

// Is logged in user assigned to current exam
$userAssignedToSession = $db->stationExaminers->isUserAssignedSession(
        $_SESSION['user_identifier'],
        $_SESSION['session_id']
);

// Get the current examiner ID
$examinerID = getCurrentExaminerID($userAssignedToSession);

// If the following session variables are not set then go back to   dent list
if (isset($_SESSION['station_id'])) {
    $stationID = (int) $_SESSION['station_id'];
    if ($stationID == 0) {
        error_log(__FILE__ . ": Specified station ID '" . $_SESSION['station_id'] . "' is invalid");
        $station = \FALSE;
    } else {
        $station = $db->stations->getStation($stationID, true);
    }
}

/** 
 * Retrieve Student ID, it is either from the session or
 * from a POST param, selected student Incomplete student results list
 */
$studentsExist = \FALSE;

// Student result chosen by user to discard (student lists section)
$discardParam = filter_input(
       INPUT_POST,
       'discard_result',
       FILTER_SANITIZE_STRING
);

if (!is_null($discardParam)) {
   $studentIDs = [$discardParam];
// Current selected student ID from session
} elseif (isset($_SESSION['current_student_id'])) {
   $studentIDs = [$_SESSION['current_student_id']];
} elseif (isset($_SESSION['current_student_ids'])) {
    $studentIDs = $_SESSION['current_student_ids'];
}

if ($studentIDs !== '' && count($studentIDs) !== 0) {
  $studentsExist = array_reduce($studentIDs, function ($allExist, $studentID) use ($db) {
      return $allExist && $db->students->doesStudentExist($studentID);
  }, true);
}

if ((empty($station)) || (!$studentsExist)) {
    // We have a problem - either the station or the student doesn't exist, or 
    // both.
    // @TODO (DW) Add to in-application logging as well.
    error_log(__FILE__ . ": Station ID and/or Student ID not specified/available ");

    $db->rollback();
} else {
    // Station and student both exist. Whoop-de-doo.
    // Session variables
    // Session ID
    $sessionID = $station['session_id'];

    // Does Station and Student exist
    if ($db->forms->doesFormExist($station['form_id'])) {

        // If to filter by station_id / Multi Scenario Results
        $sessionExamID = $db->sessions->getSessionExamID($station['session_id']);
        $exam = $db->exams->getExam($sessionExamID);
        $multiScenarioStation = $db->stations->multiScenarioStation($stationID);

        if ($multiScenarioStation) {

            $filterByStationID = $stationID;

        } else {

            $filterByStationID = null;

        }

        $success = true;
        $updatedProgress = [];

        foreach ($studentIDs as $studentID) {

            // Attempt to get an ID for the result record that we expect to have.
            $resultID = $db->results->getResultID(
                $studentID,
                $sessionID,
                $station['station_number'],
                $filterByStationID,
                $examinerID
            );

            // If we have a valid result ID then continue
            if (!empty($resultID)) {
            
                // Get result record
                $resultRecord = $db->results->getResultByID($resultID);
                
                /**
                 * If complete (or submitted) result found then we cannot discard it 
                 */
                if ($resultRecord['is_complete']) {

                    // Result cannot be discarded as it's a submitted result
                    error_log(__FILE__ . ": Result with ID " . $resultID 
                            . " could not be discarded as it's a complete result"); 
                    
                    $db->rollback();
                    $success = false;
                    break;       

                } else { 

                    $result = $db->results->deleteResult($resultID, false);
                    

                    /**
                      * if selfAssessmsent deletes the ownership of the scoresheet
                      */
                    if ($selfAssessment = $db->selfAssessments->getByExamID($sessionExamID)) {

                        $db->selfAssessments->cancelOwnership($studentID, $examinerID, $selfAssessment['self_assessment_id']);
                        
                    }

                    // Send event to rotations monitor
                    $config->getMonitorUpdateNotifier($station['session_id'])
                        ->updateDiscard($studentID, $station['station_id']);

                    \Analog::info(
                        $_SESSION['user_identifier']. " discarded result (id:$resultID) for " 
                        . gettext('student') . " (id:$studentID) at station #{$station['station_number']} day/circuit (id:$sessionID)" 
                    );             
                    
                }

            } else {

                error_log(__FILE__ . ": Failed to locate a result to delete "
                    . "(Student $studentID / Session $sessionID / Station Number "
                    . $station['station_number'] . " / Station ID $filterByStationID /"
                    . "Examiner " . $examinerID . ")");

                $db->rollback();
                $success = false;
                break;

        }

    }

    /** If all of the student results were discarded successfully, notify the update to the Rotations Monitor and
     * commit the transaction */
    if ($success) {

        $notifier = $config->getMonitorUpdateNotifier($station['session_id']);

        foreach ($updatedProgress as $progress) {

            $notifier->updateProgress($progress['studentID'], $stationID, $progress['newProgress']);

        }

        // Update outcome for this student
        (new \OMIS\Outcomes\OutcomesService($db))->setUpToDate($studentID, $sessionExamID, false);

        $db->commit();
    }

  } else {

        error_log(__FILE__ . ": Form doesn't exist "
                . "(Student $studentID / Session $sessionID / Station Number "
                . $station['station_number'] . " / Station ID $filterByStationID /"
                . "Examiner " . $examinerID . ")");

        $db->rollback();
  }

 }

// Return to student selection section
header(
  'Location: ../assess.php?p=student_selection'.
  examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID)
);
