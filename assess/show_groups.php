<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 
use \OMIS\Template as Template;
use \OMIS\Miscellaneous\Assessment as Assessment;

// Critical session check
$session = new OMIS\Session;
$session->check();

// Page access check / Can user access this session
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Get list of groups
$groups = (new Assessment())->groups($currentSessionID);

// $data will contain the data to be rendered by the template.
$data = [
    'groups' => $groups
];

if (isset($_SESSION['group_id'])) {
    $data['group'] = $_SESSION['group_id'];
}

/* Adding an exception handling mechanism in case there's an issue loading the
 * template.
 */
try {
    $template = new Template(Template::findMatchingTemplate(__FILE__));
} catch (RuntimeException $e) {
    $error = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error");
}

$template->render($data);
