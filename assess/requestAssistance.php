<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Request Assistance (via SMS messages)
 */

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \libphonenumber\PhoneNumberUtil as PhoneNumberUtil;

// Define OMIS
define('_OMIS', 1);

// Content type expected in the response is json
header('Content-Type: application/json');

// Include database connection / config file etc
include __DIR__ . '/../extra/essentials.php';

// If the user doesn't have a session then prevent access to this page
$session = new OMIS\Session;
$session->check();

// Deny user access to this page if they don't have the permissions
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
  
    reportErrorandExit(HttpStatusCode::HTTP_FORBIDDEN);
    
}

/*
 * Grab the library, username and password from the config file
 * and check that they are configured
 */
$notConfigured = !isset(
    $config->sms['country'],
    $config->sms['library'],
    $config->sms['username'],
    $config->sms['password']
);

if ($notConfigured) {
  
  reportErrorandExit(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
}

// Set library variables
$country = $config->sms['country'];
$library = $config->sms['library'];
$username = $config->sms['username'];
$password = $config->sms['password'];
$senderID = isset($config->localization['skin']['app_name']) ? 
       $config->localization['skin']['app_name'] : "Qpercom";

$contacts = filter_input(
        INPUT_POST,
        'contacts',
        FILTER_SANITIZE_STRING,
        FILTER_REQUIRE_ARRAY
);

// Get station record
$stationID = filter_input(
      INPUT_POST,
      'station_id_support',
      FILTER_SANITIZE_NUMBER_INT
);

$stationRecord = $db->exams->getStation($stationID, true);
if (!is_array($stationRecord) || count($stationRecord) == 0) {
  reportErrorandExit(HttpStatusCode::HTTP_BAD_REQUEST);
}

// Get exam session record
$sessionID = $stationRecord['session_id'];
$sessionRecord = $db->exams->getSession($sessionID);

// Compile a ist of mobile numbers to send the message to
$mobileNumbers = [];

// Run through contacts and send SMS message for each
foreach ($contacts as $userID) {

   // If user doesn't exist then skip
   if (!$db->users->doesUserExist($userID)) {
      continue;
   }

   // Get user contact number
   $userMobileNum = $db->users->getUserContactNumber($userID);
   
   // Remove any non numeric characters
   $mobileNum = preg_replace('/\D/', '', $userMobileNum);
   
   /**
    *  Parce mobile number while including the country ISO (config file)
    */
   $phoneUtil = PhoneNumberUtil::getInstance();
   try {
      $mobileNumObj = $phoneUtil->parse($mobileNum, $country);
   } catch (\libphonenumber\NumberParseException $e) {
      error_log($e);
      reportErrorandExit(HttpStatusCode::HTTP_BAD_REQUEST);
   }
  
   // Valid mobile number?
   $mobileNumValid = $phoneUtil->isValidNumber($mobileNumObj);
   
   // Include valid mobile numbers only
   if ($mobileNumValid) {
      $mobileNumbers[] = $mobileNum;
   }
   
}

// If we've got no valid mobile numbers then abort
if (count($mobileNumbers) == 0) {
  reportErrorandExit(HttpStatusCode::HTTP_BAD_REQUEST);
}

// Switch between SMS libraries
switch (strtolower($library)) {
    
    // Text magic library
    case 'textmagic':
      $isUnicode = true;

      // Protocol connection / authentication
      $params = ['username' => $username, 'password' => $password];
      $api = new TextMagicSMS\TextMagicAPI($params);

      // No credits, then report back that service is unavailable
      $creditsLeft = $api->getBalance();
      if ($creditsLeft == 0) {
         reportErrorandExit(HttpStatusCode::HTTP_SERVICE_UNAVAILABLE);
      }
     
      // SMS Message format
      $format = "Assistance Needed\nStation No: %s\nStation Name: %s\n"
               . "Exam Name: %s\nDescription: %s";
        
      // Form SMS message
      $message = sprintf(
            $format,
            $stationRecord['station_number'],
            $stationRecord['form_name'],
            $sessionRecord['exam_name'],
            $sessionRecord['session_description']
      );
       
      // Send message to assistants
      $api->send($message, $mobileNumbers, $isUnicode);
       
      break;
        
    // The 'bulk text' or 'mymobileapi' library. They are one and the same.  
    case 'bulktext':
    case 'mymobileapi':
               
       // Initiate api
       $api = new \OMIS\APIs\MyMobileAPI($username, $password, $senderID);
       
       // No credits, then report back that service is unavailable
       $creditsLeft = $api->checkCredits();
       if ($creditsLeft == 0) {
          reportErrorandExit(HttpStatusCode::HTTP_SERVICE_UNAVAILABLE);
       }
         
       // SMS Message format
       $format = "Assistance Needed|Station No: %s|Station Name: %s|"
               . "Exam Name: %s|Description: %s";
        
       // Form SMS message
       $message = sprintf(
           $format,
           $stationRecord['station_number'],
           $stationRecord['form_name'],
           $sessionRecord['exam_name'],
           $sessionRecord['session_description']
       );
                
       // Send SMS message 
       $api->sendSms(implode(',', $mobileNumbers), $message);
       break;
       
    default:
        reportErrorandExit(HttpStatusCode::HTTP_NOT_IMPLEMENTED);
        
}

// If we've go this far then return a success message in json
echo json_encode('success');

/*
 *  Set the http status code and die()
 */
function reportErrorandExit ($httpStatusCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR) {
    http_response_code($httpStatusCode);
    error_log(
      "PHP error: failed to send SMS message in the request for support feature (http status code: " .
      $httpStatusCode . ")."
    );
    exit(); 
}
