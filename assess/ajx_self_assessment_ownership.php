<?php
/**
 * Created by PhpStorm.
 * User: josesilva
 * Date: 23/01/2018
 * Time: 11:04
 */
include_once __DIR__ . '/../vendor/autoload.php';

define("_OMIS", 1);
ob_start();

const TAKE_OWNERSHIP = 1;
const DONT_TAKE_OWNERSHIP = 0;
const ADD_REMOVE_OWNERSHIP = 'add_remove';
const CHECK_OWNERSHIP = 'check';

// JSON Helper functions.
use OMIS\Utilities\HttpStatusCode;
use OMIS\Utilities\JSON;

$take = filter_input(INPUT_POST, "take", FILTER_SANITIZE_STRIPPED);
$selfAssessmentID = filter_input(INPUT_POST, "selfAssessmentID", FILTER_SANITIZE_STRIPPED);
$action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRIPPED);

if (null !== $take && null !== $selfAssessmentID && null !== $action) {

    require_once __DIR__ . "/../extra/essentials.php";
    require_once __DIR__ . "/assess_func.php";

    // Do a session check to see if the user's logged in or not.
    if (!isset($session)) {

        $session = new OMIS\Session();
        $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
    }

} else {

    http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
    exit();

}

switch ($action) {

    case ADD_REMOVE_OWNERSHIP:

        $studentID = $_SESSION['current_student_id'];
        $stationID = $_SESSION['station_id'];

        // Is logged in user assigned to current exam
        $userAssignedToSession = $db->stationExaminers->isUserAssignedSession(
            $_SESSION['user_identifier'],
            $_SESSION['session_id']
        );

        // Get current examiner ID
        $examinerID = getCurrentExaminerID($userAssignedToSession);
    
        if ($take == TAKE_OWNERSHIP) {

            if (!$db->selfAssessments->getOwnership($studentID, $selfAssessmentID)) {

                $db->selfAssessments->takeOwnership($studentID, $examinerID, $selfAssessmentID);

            } else {

                header('Content-Type: application/json');
                http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
                echo json_encode('Sorry, Ownership has been taken.');

            }
        }
        elseif ($take == DONT_TAKE_OWNERSHIP) {

            $db->selfAssessments->cancelOwnership($studentID, $examinerID, $selfAssessmentID);

            $stationRecord = $db->exams->getStation($stationID);
            $sectionRecord = $db->selfAssessments->getSection($stationRecord['form_id']);
            $items = $db->selfAssessments->getItems($selfAssessmentID, $sectionRecord['section_id']);
            $itemsID = array_map(function($item){ return $item['item_id']; }, $items);
            
            if ($resultID = $db->selfAssessments->getResultID($studentID, $stationID, $examinerID)) {
              
                $db->selfAssessments->deleteItemsScore($itemsID, $resultID);
                $db->results->recalcStudentResult($resultID);
                
            }

        }

    break;
    case CHECK_OWNERSHIP:
        header('Content-Type: application/json');
        $studentID = $_SESSION["current_student_id"];

        $exist = (bool)($db->selfAssessments->getOwnership($studentID, $selfAssessmentID));
        echo json_encode(['isTaken' => $exist]);

    break;
}

