<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Database\Results as Results;
use \OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Is admin role
$adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
$userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);

// Check if the exam is configured
$configuration = ['exam_id', 'session_id', 'station_id', 'group_id'];
foreach ($configuration as $setting) {
  // Check if it is configured
  if (isset($_SESSION[$setting])) {
    $value = $_SESSION[$setting];
    $$setting = $value;
  } else {
    header("Refresh: 2; url='assess.php?p=exam_configuration'");
    die("Exam is not configured, redirecting to configuration page...");
  }
}

// If the exam doesn't exist, then abort
if (!$db->exams->doesExamExist($exam_id)) {
    header("Refresh: 2; url='assess.php?p=exam_configuration'");
    die("Exam does not exist, redirecting to configuration page...");
}

// Get exam record
$examRecord = $db->exams->getExam($exam_id);

// Is logged in user assigned to current exam
$userAssignedToSession = $db->exams->isUserAssignedSession(
        $_SESSION['user_identifier'],
        $_SESSION['session_id']
);

// Can user have access to these results
if ((!$userIsAdmin || ($userIsAdmin && $userAssignedToSession)) && $examRecord['view_results'] == 0) {
    header("Refresh: 2; url='assess.php?p=exam_configuration'");
    die("You do not have access to this feature, redirecting to configuration page...");
}

// Render assessment menu
$enableMenus = ['config', 'students', 'results', 'change_examiners'];
$templateMenus = new Template("assessment_menu.html.twig");
$templateMenus->render([
   'enable_menus' => $enableMenus,
   'selected_menu' => 'results',
   "skin" => $config->localization['skin'],
   "name" => [
     "short" => $config->getName(),
     "long" => $config->getName(\TRUE)
   ]   
]);

// Examiner Name
$loggedInUser = $_SESSION['user_forename'] . " " . $_SESSION['user_surname']
              . " - " . $_SESSION['user_identifier'];

// Administrators can login and view all results for this station
if ($userIsAdmin && !$userAssignedToSession) {

  $scenarios = $db->exams->getStationScenarios($station_id, $session_id);
  $scenarioKeys = array_keys($scenarios);
  $examinerID = null;
  
// Get results only for this examiner  
} else {
    
  $scenarioKeys = [];
  $examinerID = $_SESSION['user_identifier'];
  
}

  $results = $db->results->getAllResults(
      $exam_id,
      $examinerID,
      $scenarioKeys
  );

// Get Global Rating Scale Data
$ratingScaleData = $db->results->getGlobalRatingScaleData($exam_id, $examinerID);
$areRatingData = (is_array($ratingScaleData) && count($ratingScaleData) > 0);

// Add score percent column (There's probably a more efficient way of doing this)
foreach($results as &$result) {
    
   // Result ID
   $resultID = $result['result_id'];
   
   // Figure out score percentage and grade for each result 
   $result['score_percentage'] = percent($result['score'], $result['total_value']);
   
   // (DW) 13-02-2015: Fix for issue #286.
   $passOrFail = Results::determinePassOrFail(
           $result['score'],
           $result['pass_value'],
           $result['total_value']
   );
   $result['grade'] =  $passOrFail ? "pass" : "fail";
   
   // If there is a rating scale value then add it     
   if ($areRatingData) {
       
    if (isset($ratingScaleData[$resultID])) {
      $result['grs'] = $ratingScaleData[$resultID]['descriptor'];  
    } else {
      $result['grs'] = "NA";
   }
  
  }
}
$settingsConfig = [];
 if (isset($config->exam_defaults['result_columns']) 
         && sizeof($config->exam_defaults['result_columns']) > 0) {
    $settingsConfig = $config->exam_defaults['result_columns'];
 }

 $columns = [];
 
 // Get required settings fields
 $examSettings = $db->examSettings->get($exam_id);
 foreach ($examSettings as $field => $value) {
   // We are only concerned about the result section fields
   if (strpos($field, 'results_column_') !== false) {
       $type = substr($field, 15);
       
       // If enabled
       if ($value) {
           
        // Column Title
        if (isset($settingsConfig[$type])) {
           $title = $settingsConfig[$type]['title'];
        } else {
           $title = $type;
        }
        
        // If GRS values then add required title
        if (!($type == 'grs' && !$areRatingData)) {
            $columns[$type] = $title;
        }
        
       // Show examiner column in admin mode
       } else if ($type == 'examiner' && $userIsAdmin && !$userAssignedToSession) {
           $columns[$type] = "examiner";
       }
       
   }
   
 }
 
// Template Data
$templateData = [
    'examination_name' => $examRecord['exam_name'],
    'columns' => $columns,
    'results' => $results
];

/* Adding an exception handling mechanism in case there's an issue loading the
 * template.
 */
try {
    $template = new Template(Template::findMatchingTemplate(__FILE__));
} catch (RuntimeException $e) {
    $error_message = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error_message");
}

$template->render($templateData);
