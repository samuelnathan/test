<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
use \OMIS\Template as Template;

$cache = new \OMIS\Imaging\Cache($config->imagecache["caches"]["students"]);

$withThumbnail = function($students) use ($config, $cache) {
    return array_map(function ($student) use ($config, $cache) {
        $studentImage = $student["student_image"];

        $imagePath = trim($studentImage) != '' && $cache->contains($studentImage) ?
            $studentImage :
            basename($config->imagecache['images']['default']);

        $thumbnailPath = $cache->getThumbnail($imagePath);

        // Make sure the student details know what's going on.
        $thumbnailSrc = \OMIS\Imaging\Image::getImageAsInline($thumbnailPath);

        $student['thumbnail_src'] = $thumbnailSrc;
        return $student;
    }, $students);
};

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

/**
 * Const Definitions:
 * TOASSESS_LIST = toassess
 * INCOMPLETE_LIST = incomplete
 * ASSESSED_LIST = assessed
 */
// Prepare Variables
/**
 * isMobileDevic() Function Below:
 * Disable multiple drop down for IPAD and Andriod
 * Does Not Deal With the Multi Select Box Correctly
 */
$templateData = [
    "list_type" => $listType,
    "station_id" => $currentStationID,
    "is_mobile" => isMobileDevice(),
    "student_number" => $settingsRecord["exam_student_id_type"],
    'multiple_selection' => $config->exam_defaults['multiple_candidate'] && ($listType === TOASSESS_LIST || $listType === INCOMPLETE_LIST)
];

switch ($listType) {
    // To Assess List
    case TOASSESS_LIST:
        $templateData["form_method"] = "get";
        $templateData["next_page"] = "next";
        $templateData["legend"] = "Next " . ucwords(gettext('student'));
        $templateData["students"] = $withThumbnail($toAssessResults);
        $templateData["select_first"] = true;
        break;

    // Incomplete List
    case INCOMPLETE_LIST:
        $templateData["form_method"] = "post";
        $templateData["next_page"] = "exam_assessment";
        $templateData["legend"] = "Incomplete";
        $templateData["students"] = $withThumbnail($incompleteResults);
        $templateData["select_first"] = false;
        break;

    // Assessed List
    case ASSESSED_LIST:
        $templateData["form_method"] = "post";
        $templateData["next_page"] = "exam_assessment";
        $templateData["legend"] = "Submitted";
        $templateData["students"] = $withThumbnail($submittedResults);
        $templateData["select_first"] = false;
        break;
}

try {
    $templatePath = "student_list_" . $listType . ".html.twig";

    $template = new Template($templatePath, [ $config->base_path . '/assess/student_lists' ]);
} catch (RuntimeException $e) {
    $error = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error");
}

$template->render($templateData);
