 <?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 define('_OMIS', 1);

 use \OMIS\Auth\RoleCategory as RoleCategory;

 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';

 // Page access check, can user access this section
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Exam admin roles
 $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
 $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);

 if (!isset($config)) $config = \OMIS\Config;

 // Assessment helper functions
 require_once __DIR__ . "/assess_func.php";

 // Check for a valid station ID
 $stationID = $_SESSION['station_id'];
 $station = $db->stations->getStation($stationID, true);

 if (empty($station))
     header("Location: ../assess.php?p=exam_configuration");

 $absentSetting = filter_input(INPUT_POST, 'markabs', FILTER_SANITIZE_STRIPPED);
 $studentID = trim(filter_input(INPUT_POST, 'abs', FILTER_SANITIZE_STRIPPED));

 // Is logged in user assigned to current exam
 $userAssignedToSession = $db->stationExaminers->isUserAssignedSession(
         $_SESSION['user_identifier'],
         $_SESSION['exam_id']
 );

 // Get the current examiner ID
 $examinerID = getCurrentExaminerID($userAssignedToSession);

 // Student selection section return url
 $studentSelectionUrl = "../assess.php?p=student_selection";

 // Concatenate examiner url to student url
 $studentSelectionUrl .= examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID);

 /* If the user hasn't asked to mark a student as absent, then redirect back to
  * the student selector screen.
  */
 if (empty($absentSetting))
     header("Location: " . $studentSelectionUrl);

 $stationNumber = $station['station_number'];
 $sessionID = $station['session_id'];

 // Remove blank student
 if (strpos($studentID, "blank::") !== false) {

     $blankStudentID = trim(explode("::", $studentID)[1]);
     $db->blankStudents->ignoreBlankStudent($blankStudentID, $stationNumber);

     // Notify the discard to the monitor system
     $config->getMonitorUpdateNotifier($sessionID)
            ->updateDiscardBlank($blankStudentID, $stationNumber, true);

 } else {

     // Get exam record
     $examID = $db->sessions->getSessionExamID($sessionID);

     /*
      * If to filter by station_id / multi-scenario station
      * does the selected station contain multiple scenarios ?
      */
     $multiScenario = $db->stations->multiScenarioStation($stationID);
     $filterByStationID = ($multiScenario) ? $stationID : NULL;

     // Check if result does not exist then insert one...
     $studentHasResult = $db->results->doesStudentHaveResult(
           $studentID,
           $sessionID,
           $stationNumber,
           $filterByStationID,
           $examinerID
     );

     if (!$studentHasResult) {

         $db->results->insertResult(
             $examinerID, $studentID, $stationID, 1, 0, 1
         );

     } else { // Overwrite

         $resultID = $db->results->getResultID(
             $studentID,
             $sessionID,
             $stationNumber,
             $filterByStationID,
             $examinerID
         );

         $db->results->deleteResult($resultID);
         $db->results->setAbsentScoreValue($resultID, 1, 0);

     }

     // Notify the marked absence to the monitor system
     $config->getMonitorUpdateNotifier($sessionID)
            ->updateAbsence($studentID, $stationID, true);

     // Update outcome for this student
     (new \OMIS\Outcomes\OutcomesService($db))->setUpToDate($studentID, $examID, false);

 }

 // Return to student selection section
 header("Location: $studentSelectionUrl");
