<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Utilities\Path as Path;
use \OMIS\Session as Session;
use \OMIS\Auth\Role as Role;
use \OMIS\Imaging\Cache as Cache;
use \OMIS\Imaging\Image as Image;
use \OMIS\Template as Template;

defined("IMAGE_PREVIEW_HEIGHT") or define("IMAGE_PREVIEW_HEIGHT", 180);
defined("ASSESSMENT_COMPLETE") or define("ASSESSMENT_COMPLETE", 1);
defined("INCLUDE_ABSENT_RESULTS") or define("INCLUDE_ABSENT_RESULTS", false);
defined("INCLUDE_OBSERVERS") or define("INCLUDE_OBSERVERS", true);
defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

// Critical Session Check
$session = new Session();
$session->check(Session::SESSION_EXIT_REDIRECT);

// Page Access Check / Can User Access this Section?
if (!Role::loadID($_SESSION["user_role"])->canAccess($page)) {
    return false;
}

// Exam admin roles
$adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
$userIsAdmin = $adminRoles->containsRole($_SESSION["user_role"]);

// Assessment helper functions
require_once __DIR__ . "/assess_func.php";

$db->startTransaction();

// Is logged in user assigned to current exam
$userAssignedToSession = $db->exams->isUserAssignedSession(
        $_SESSION["user_identifier"],
        $_SESSION["session_id"]
);

// Get current examiner ID
$examinerID = getCurrentExaminerID($userAssignedToSession);

// Student selection section return url
$studentSelectionUrl = "assess.php?p=student_selection";

// Concatenate examiner url with student url
$studentSelectionUrl .= examinerUrl($userIsAdmin, $userAssignedToSession, $examinerID);

// Student images feature enabled for the current user role ?
$studentImagesAllowed = $db->features->enabled("student-images", $_SESSION["user_role"]);

if (!isset($config)) {
    $config = \OMIS\Config;
}

// Image cache.
$cache = new Cache($config->imagecache["caches"]["students"]);

// Get the important attributes, both from the query string and from the session
$nextStudentIDs =  array_map(function ($studentID) {
    return trim(filter_var($studentID, FILTER_SANITIZE_STRING));
}, $_GET['toassess']);
$stationID = $_SESSION["station_id"];
$groupID = $_SESSION["group_id"];

// Partition the student IDs into two arrays: One with the blank students selected and another with the non-blanks
$parted = array_reduce($nextStudentIDs, function (&$result, $studentID) {
    if (strpos($studentID, 'blank::') !== false)
        $result['blanks'][] = $studentID;
    else
        $result['students'][] = $studentID;

    return $result;
}, ['blanks' => [], 'students' => []]);

$nonBlankIDs = $parted['students'];
$blankIDs = $parted['blanks'];

// Don't continue if there's more than a student selected, and they are all blanks
if (count($nextStudentIDs) > 1 && count($nonBlankIDs) === 0) {
    $db->rollback();
    header("Refresh: 3; url='$studentSelectionUrl'");
    echo "No " . ucwords(gettext('student')) . " selected to assess";
    exit;
}


foreach ($nonBlankIDs as $studentID) {
    if (!$db->students->doesStudentExist($studentID)) {
        $db->rollback();
        header("Refresh: 3; url='" . $studentSelectionUrl . "'");
        echo ucwords(gettext('student')) . " to " . gettext('examine') . " does not exist, redirecting in 3 seconds...";
        exit;
    }


    if (!$db->groups->doesStudentExistInGroup($studentID, $groupID)) {
        $db->rollback();
        header("Refresh: 3; url='" . $studentSelectionUrl . "'");
        echo ucwords(gettext('student')) . " to " . gettext('examine') . " does not exist in current group, redirecting in 3 seconds...";
        exit;
    }
}

// Attempt to get the station from its ID, if it doesn't exist, abort.
$station = $db->stations->getStation($stationID);
if (empty($station)) {
    $db->rollback();
    header("Refresh: 3; url='assess.php?p=exam_configuration'");
    echo "Station does not exist, redirecting in 3 seconds...";
    exit;
}

// Get the exam. If it doesn't exist, then abort.
$examID = $db->sessions->getSessionExamID($station["session_id"]);
if (empty($examID)) {
    $db->rollback();
    header("Refresh: 3; url='assess.php?p=exam_configuration'");
    echo ucwords(gettext('exam')) . " does not exist, redirecting in 3 seconds...";
    exit;
}

// Get exam settings
$settingsRecord = $db->examSettings->get($examID);
$multipleCandidateEnabled = $config->exam_defaults['multiple_candidate'];

// Validate that multiple student selection is allowed for the exam when more than one student has been selected to
// assess
if (!$multipleCandidateEnabled && count($nextStudentIDs) > 1) {
    $db->rollback();
    header("Refresh: 3; url='assess.php?p=exam_configuration'");
    echo ucwords(gettext('exam')) . " does not allow multiple " . gettext('student') . " selection, redirecting in 3 seconds...";
    exit;
}

// If to filter by station_id / Multi Scenario Results
$multiScenarioStation = $db->stations->multiScenarioStation($stationID);
if ($multiScenarioStation) {
    $filterByStationID = $stationID;
} else {
    // If to filter by station_id
    $filterByStationID = null;
}

// Set current student to the session
$_SESSION["current_student_ids"] = $nonBlankIDs;
$_SESSION['current_student_id'] = null;

foreach ($nonBlankIDs as $nextStudentID) {
    // Has the examiner already submitted a result for this student
    $studentResultCountExaminer = $db->results->doesStudentHaveResult(
        $nextStudentID,
        $station["session_id"],
        $station["station_number"],
        $stationID,
        $examinerID,
        INCLUDE_ABSENT_RESULTS,
        ASSESSMENT_COMPLETE
    );

    // If the examiner has already submitted a result for this student then abort
    if ($studentResultCountExaminer > 0) {
        $db->rollback();
        header("Refresh: 3; url='$studentSelectionUrl'");
        echo "You have already submitted a result for this " . gettext('student') . " at this station, "
            . "redirecting to list in 3 seconds...";
        exit;
    }
}

// Examiners required at this station
$examinersRequired = $db->stations->getExaminersRequired(
        $stationID,
        INCLUDE_OBSERVERS
);

foreach ($nonBlankIDs as $nextStudentID) {
    // Does student already have a result
    $studentResultCount = $db->results->doesStudentHaveResult(
        $nextStudentID,
        $station["session_id"],
        $station["station_number"],
        $stationID
    );

    /* If all slots have been filled for this student at this station
     * then get the hell out of here
     */
    if ($examinersRequired == $studentResultCount) {
        $db->rollback();
        header("Refresh: 3; url='$studentSelectionUrl'");
        echo ucwords(gettext('student')) . " already " . gettext('assessed') . " or result incomplete, redirecting to list in 3 seconds...";
        exit;

    /*
     * If the student doesn't have a result then grab hold of
     * the available slot and make it the examiner's
     */
    } else {

        /**
         * Does current examiner have incomplete results for this student,
         * then don't do anything, leave it as it is
         */
        $studentResultIncompleteCountExaminer = $db->results->doesStudentHaveResult(
                $nextStudentID,
                $station["session_id"],
                $station["station_number"],
                $stationID,
                $examinerID
        );

        if ($studentResultIncompleteCountExaminer == 0) {
            $db->results->insertResult($examinerID, $nextStudentID, $stationID, 0, 0);
        }

    }
}

if (filter_has_var(INPUT_GET, "dotest") && filter_has_var(INPUT_GET, "toassess")) {

    foreach ($blankIDs as $nextStudentID) {

        // Remove the first 7 characters (i.e. "blank::")
        $blankStudent_id = substr($nextStudentID, 7);

        // Discard the blank
        $db->exams->ignoreBlankStudent($blankStudent_id, $station["station_number"]);

        // Notify the discard to the monitor system
        $config->getMonitorUpdateNotifier($station['session_id'])
               ->updateDiscardBlank($blankStudent_id, $station['station_id'], true);

        // If there are no students to examine, return to the selection page
        if (count($nonBlankIDs) === 0) {
            $db->commit();
            header("Location: " . $studentSelectionUrl);
            exit;
        }

    }

    ob_end_flush();

    // This will store the data being rendered in the template.
    $templateData = [
        "station" => $station,
        "students" => [],
        "exam_settings" => $settingsRecord,
        'multiple_candidate' => $multipleCandidateEnabled
    ];

    /* If there's a "last" student, i.e. the examiner has just finished examining
     * someone, get the necessary info to be able to display that person"s
     * identity for additional confirmation.
     */
    if (filter_has_var(INPUT_GET, "sexamined")) {
        $lastStudentID = trim(filter_input(INPUT_GET, "sexamined", FILTER_SANITIZE_STRING));

        // Make sure that there are results for that student.
        $lastStudent_resultCount = $db->results->doesStudentHaveResult(
            $lastStudentID,
            $station["session_id"],
            $station["station_number"],
            $filterByStationID
        );

        /* Make sure that the student has results before incorporating them into
         * the output.
         */
        if ($lastStudent_resultCount > 0) {
            $lastStudent = $db->students->getStudent($lastStudentID, $_SESSION["cterm"]);
            $lastStudentGroup = $db->groups->getStudentGroupRecord($lastStudentID, $groupID);
            $lastStudent["exam_number"] = $lastStudentGroup["student_exam_number"];

            if (!empty($lastStudent)) {
                $templateData["students"]["last"] = [$lastStudent];
            }
        } else {
            // This shouldn't happen, but just in case...
            error_log(
                __FILE__ . ": " . gettext('student') . " $lastStudentID has been identified as "
                . "having previously been " . gettext('examined') . ", but there are no results "
                . "on file. Why?"
            );
        }
    }

    $templateData['students']['next'] = [];
    foreach ($nonBlankIDs as $nextStudentID) {
        $nextStudent = $db->students->getStudent($nextStudentID, $_SESSION['cterm']);
        $nextStudentGroup = $db->groups->getStudentGroupRecord($nextStudentID, $groupID);
        $nextStudent['exam_number'] = $nextStudentGroup['student_exam_number'];

        $templateData['students']['next'][] = $nextStudent;
    }

    // Now, go through the students and figure out their images (both full size
    // and preview/thumbnail).
    foreach (array_keys($templateData["students"]) as $student_index) {
        $students = $templateData["students"][$student_index];

        foreach ($students as $index => $student) {
            $image = null;
            if (isset($student["student_image"])) {
                // Okay, there's something in the field. Let's find out what.
                $image = $student["student_image"];
                unset($student["student_image"]);
            }

            // If students images feature is disabled or there's no image for the student, use the placeholder.
            if (!$studentImagesAllowed || empty($image) || !$cache->contains($image)) {
                $image = basename($config->imagecache["images"]["default"]);
            }

            // Okay, we have an image. Let's see if we can make a thumbnail of it.
            try {
                $imageObject = new Image($cache->get($image));
                $preview_height = (int)IMAGE_PREVIEW_HEIGHT;
                $preview_width = (int)IMAGE_PREVIEW_HEIGHT * $imageObject->getAspectRatio();
                unset($imageObject);
            } catch (Exception $e) {
                error_log(
                    __METHOD__ . "Failed to get dimensions of image " . $image
                    . "in Student Image cache. Falling back on defaults..."
                );
                // These are "default" fallback values. Probably won't be pretty but
                // at least there might be an image...
                $preview_width = (int)round(IMAGE_PREVIEW_HEIGHT * 0.75);
                $preview_height = (int)IMAGE_PREVIEW_HEIGHT;
            }

            // Ask the image cache to retrieve (or generate) a version of the image
            // at the appropriate height and width.
            $preview = $cache->get($image, $preview_width, $preview_height);

            // We need the URL of the image to render it on-screen.
            $student["image"] = Path::toURL($cache->get($image));
            $student["preview"] = Path::toURL($preview);

            // Now that we've got the student image data, re-inject the "fixed" record
            // back into the data to be rendered by the template.
            $templateData["students"][$student_index][$index] = $student;
        }
    }

    // Add session information
    $sessionRecord = $db->exams->getSession($station['session_id']);
    $templateData['session'] = formatDate($sessionRecord['session_date'])." "
    .$sessionRecord['session_description'];

    // Add circuit colour if set
    $colourCode = $sessionRecord['circuit_colour'];
    $colourName = $db->exams->getColourName($sessionRecord['circuit_colour']);
    $templateData['circuit_colour'] = [
       'code' => $colourCode,
       'name' => $colourName
    ];

    // Commit the saves
    $db->commit();

    // Notify the blank students being discarded to the rotations monitor
    $notifier = $config->getMonitorUpdateNotifier($station['session_id']);
    foreach ($blankIDs as $blankStudent_id) {
        $notifier->updateDiscardBlank($blankStudent_id, $station['station_id'], true);
    }

    // ...and we're done.
    $template = new Template("next.html.twig");
    $template->render($templateData);
} else {
    header("Location: " . $studentSelectionUrl);
}
