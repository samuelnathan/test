<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 define('_OMIS', 1);

 use \OMIS\Template as Template;

 if (isset($_POST['term'])) {

    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

    $_SESSION['cterm'] = trim($_POST['term']);
    $db->userPresets->update(['filter_term' => $_SESSION['cterm']]);

    $template = new Template(Template::findMatchingTemplate(__FILE__));
    $template->render([
        'departments' => (new \OMIS\Miscellaneous\Assessment())->departments($_POST['term'])
    ]);

 }

