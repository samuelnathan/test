<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

$currentSessionID = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_NUMBER_INT);
if (!is_null($currentSessionID)) {
    include __DIR__ . '/../extra/essentials.php';
    
    // Page access check / can user access this section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    
    /**
     * Capture selected station ID (default null)
     */
     $selectedStationID = null;

    /* @TODO (DW) including scripts like this and hoping they pick up on 
     * variables they inherit from this script is dangerous as the ripple 
     * effects of any changes or refactoring are unclear. The stations and 
     * groups should really be included as method calls or something where the 
     * flow of data from one script to the next is clearly identified.
     */
    
    include 'show_stations.php';
    ?>[!*$*!]<?php
    include 'show_groups.php';
         
}
