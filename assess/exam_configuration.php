<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

  defined("RETURN_AS_ARRAY") or define("RETURN_AS_ARRAY", true);
  defined("PUBLISHED") or define("PUBLISHED", 1);

  use \OMIS\Auth\Role as Role;
  use \OMIS\Template as Template;
  use \OMIS\Miscellaneous\Assessment as Assessment;
  use \OMIS\Database\CoreDB as CoreDB;

  // Critical Session Check
  $session = new OMIS\Session;
  $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

  // Page Access Check / Can User Access this Section?
  if (Role::USER_ROLE_STATION_MANAGER == $_SESSION['user_role']) {
    // Station manager cannot access so redirect them to home page
    header("location: manage.php?page=main");
  } elseif (!Role::loadID($_SESSION['user_role'])->canAccess()) {
      return false;
  }

  require_once __DIR__ . '/assess_func.php';

  // Assessment Manual and Video Location
  $manual = $config->assessment['help']['pdf'];
  $video = $config->assessment['help']['video'];
  
  // Set todo variable
  if (isset($_SESSION['todo']) && $_SESSION['todo'] != '2') {
      $_SESSION['todo'] = '2';
  }

  // Reset session variables if coming from management section
  if (in_array('frmtop', array_keys($_REQUEST))) {
      unset(
        $_SESSION['dept_id'], 
        $_SESSION['exam_id'],
        $_SESSION['session_id'], 
        $_SESSION['station_id'],
        $_SESSION['group_id']
      );
  }

  // Set current logged in examiner as station examiner
  $_SESSION['station_examiner'] = $_SESSION['user_identifier'];

  // Current academic term
  $currentTerm = $_SESSION['cterm'];

  // Session variables check (already set?)
  if (isset($_SESSION['exam_id'])) {

      $currentDeptID = iS($_SESSION, 'dept_id');
      $currentExamID = iS($_SESSION, 'exam_id');
      $currentSessionID = iS($_SESSION, 'session_id');
      $currentStationID = iS($_SESSION, 'station_id');
      $currentGroupID = iS($_SESSION, 'group_id');
      $assignedStations = [];
      
  /**
   * Auto-config feature upon logging in 
   */   
  } else {

      $currentDeptID = "";
      $currentExamID = "";
      $currentSessionID = "";
      $currentStationID = "";
      $currentGroupID = "";

      // Get list of assigned stations TODAY
      $assignedStations = $db->stationExaminers->getActiveStationAssignment(
          $_SESSION['user_identifier']
      );

  }

  // Can we request for asistance
  $canSupport = isset($_SESSION['station_id'], $_SESSION['sms_support']);
  $supportEnabled = ($canSupport && $_SESSION['sms_support'] == 1);

  // Render assessment menu
  $enableMenus = ['config'];
  if ($supportEnabled) {
      $enableMenus[] = 'support';
  }

  /**
   * If the station is set (known) then find out if it's a multi-scenario station
   */
  if (isset($_SESSION['station_id']) && $db->exams->doesStationExist($_SESSION['station_id'])) {
    $multiScoresheets = $db->exams->multiScenarioStation($_SESSION['station_id']);
  } else {
    $multiScoresheets = false;
  }
  
  // Get all departments with live exams in current term
  $departments = (new Assessment())->departments($currentTerm);
  
  // Selected department in assessment
  if (isset($_SESSION['dept_id'])) {
    
    $selectedDepartment = $currentDeptID;
    
  // Selected department in management (if any) 
  } elseif (isset($_SESSION['selected_dept']))  {
    
    $selectedDepartment = $_SESSION['selected_dept'];
  
  // None selected  
  } else {
    
    $selectedDepartment = '';
    
  }
     
  // Get exams
  $exams = CoreDB::into_array(
     $db->exams->getList(
         [$selectedDepartment], 
         $currentTerm,
         'exam_id', 
         PUBLISHED
     )
  );
       
  // Get sessions
  $sessions = $db->exams->getExamSessions(
        $currentExamID, 
        RETURN_AS_ARRAY,
        PUBLISHED
  );

  // Does session exist then get list of groups and stations
  if ($db->exams->doesSessionExist($currentSessionID)) {
    
    // Get group list
    $groups = (new Assessment())->groups($currentSessionID);

    // Get station list
    $stations = (new Assessment())->stations($currentSessionID);

  } else {
    
    $groups = $stations = [];
     
  }
  
   /**
   * If for whatever reason the department/exam or session is switched to unpublished
   * during the exam, then this is the check to reset the drop downs at
   * the config screen
   */
  if (!in_array($selectedDepartment, array_column($departments, 'dept_id'))) {
    
      unset(
        $_SESSION['dept_id'],
        $_SESSION['exam_id'],
        $_SESSION['session_id'],
        $_SESSION['station_id'],
        $_SESSION['group_id']
      );
      
      $currentExamID = $currentSessionID = '';
      $currentGroupID = $currentStationID = '';
      $exams = $sessions = [];
      $groups = $stations = [];
        
  } else if (!in_array($currentExamID, array_column($exams, 'exam_id'))) {
    
      unset(
        $_SESSION['exam_id'],
        $_SESSION['session_id'],
        $_SESSION['station_id'],
        $_SESSION['group_id']
      );
      
      $currentSessionID = $currentGroupID = $currentStationID = '';
      $sessions = $groups = $stations = [];
    
  } else if (!in_array($currentSessionID, array_column($sessions, 'session_id'))) {
    
      unset(
        $_SESSION['session_id'],
        $_SESSION['station_id'],
        $_SESSION['group_id']
      );
      
      $currentGroupID = $currentStationID = '';
      $groups = $stations = [];
      
  }
  
  // Get circuit colours
  $colourMap = $db->exams->getAllCircuitColours('circuit_colour');
    
  ob_end_flush();
  
  // Only show the change examiner menu option if the station is configured
  if (isset($_SESSION['station_id'])) {
     $enableMenus[] = 'change_examiners';
  }

  $template = new Template("assessment_menu.html.twig");
  $template->render([
     'enable_menus' => $enableMenus,
     'selected_menu' => 'config',
     "skin" => $config->localization['skin'],
     "name" => [
       "short" => $config->getName(),
       "long" => $config->getName(\TRUE)
     ]
  ]); 
  
  $scaleConfig = isset($config->exam_defaults['default_text_scale']) ? 
  $config->exam_defaults['default_text_scale'] : 'medium';
 
  $template = new Template("exam_configuration.html.twig");
  $template->render([
     'term' => $currentTerm,
     'department' => $selectedDepartment,
     'exam' => $currentExamID,
     'session' => $currentSessionID,
     'station' => $currentStationID,
     'group' => $currentGroupID,
     'multiScoresheets' => $multiScoresheets,
     'terms' => $db->academicterms->getAllTerms(),
     'departments' => $departments,
     'exams' => $exams,
     'sessions' => $sessions,
     'groups' => $groups,
     'stations' => $stations,
     'assigned_stations' => $assignedStations,
     'fontScales' => [
         "smallest" => 11,
         "very small" => 13,
         "small" => 14,
         "medium" => 15,
         "large" => 16,
         "very large" => 17,
         "largest" => 20
     ],
     'fontScaleSet' => $scaleConfig,
     'colours' => $colourMap,
     'user' => [
        'id' => $_SESSION['user_identifier'],
        'name' => $_SESSION['user_forename']." ".$_SESSION['user_surname']
     ],
     'manual' => file_exists($manual) ? $manual : "",  
     'video' =>  file_exists($video) ? $video : "",
     'sms_support' => $supportEnabled
  ]);

?>
