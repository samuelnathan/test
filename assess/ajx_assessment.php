<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 define('_OMIS', 1);

 use \OMIS\Template as Template;

 if (isset($_POST['dept_id'])) {

    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }
    
    $exams = $db->exams->getList([iS($_POST, 'dept_id')], $_SESSION['cterm']);
    $data = ['exams' => []];

    if (mysqli_num_rows($exams) > 0) {

        while ($exam = $db->fetch_row($exams)) {

            $data['exams'][] = $exam;

        }

    }

    $template = new Template(Template::findMatchingTemplate(__FILE__));
    $template->render($data);

 }

