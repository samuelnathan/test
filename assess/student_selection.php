<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */
use \OMIS\Session as Session;
use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Template as Template;

// Const Definitions
defined("TOASSESS_LIST") or define("TOASSESS_LIST", "toassess");
defined("INCOMPLETE_LIST") or define("INCOMPLETE_LIST", "incomplete");
defined("ASSESSED_LIST") or define("ASSESSED_LIST", "assessed");
defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");
defined("VIEW_RESULTS") or define("VIEW_RESULTS", 1);

// Critical Session Check
(new Session())->check(Session::SESSION_EXIT_REDIRECT);

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

    return false;

}

// Assessment helper functions
require_once __DIR__ . "/assess_func.php";

// Exam admin roles
$adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
$userIsAdmin = $adminRoles->containsRole($_SESSION["user_role"]);

/* Settings for FILTER_VALIDATE_INT that will ensure that it doesn't let
 * negative numbers through...
 */
$filterOptionsPositiveInt = ["options" => ["min" => 1]];

if (filter_has_var(INPUT_POST, "fix_station")) {

    // Validate term
    $autoConfigTerm = filter_input(INPUT_POST, 'term_id', FILTER_SANITIZE_STRING);
    if (!is_null($autoConfigTerm) && $db->academicterms->existsById($autoConfigTerm)) {

        $_SESSION['cterm'] = $autoConfigTerm;

    }

    $parameters = [
        "exam_id" => FILTER_VALIDATE_INT,
        "session_id" => FILTER_VALIDATE_INT,
        "station_id" => FILTER_VALIDATE_INT,
        "group_id" => FILTER_VALIDATE_INT,
        "dept_id" => FILTER_SANITIZE_STRING
    ];
    
    foreach ($parameters as $parameter => $filter) {
        /* If we're looking for an int, add extra options to the filter to make
         * sure that the result handed back is actually a positive int (rather
         * than -2 or something) as the database record IDs won't be less than 1.
         */
        if ($filter == FILTER_VALIDATE_INT) {
            $_SESSION[$parameter] = filter_input(
                INPUT_POST,
                $parameter,
                $filter,
                $filterOptionsPositiveInt
            );
        } else {
            $_SESSION[$parameter] = filter_input(INPUT_POST, $parameter, $filter);
        }
    }
    
    // Request for assistance support
    $assistantsCount = $db->exams->getSessionAssistantsCount($_SESSION["session_id"]);
    $_SESSION["sms_support"] = ($assistantsCount > 0) ? 1 : 0;

    // Redirect on First POST from Config Section to prevent "Webpage Expired"
    header("Location: ./assess.php?p=student_selection");
}

// (DW) This is where the actual rendering of the student lists happens.
$currentSessionID = $_SESSION["session_id"];
$loggedInUserID = $_SESSION["user_identifier"];
$examRecord = $db->exams->getExam($_SESSION["exam_id"]);

// Get exam settings
$settingsRecord = $db->examSettings->get($_SESSION["exam_id"]);

// Support enabled
$supportEnabled = (isset($_SESSION["sms_support"]) && $_SESSION["sms_support"] == 1);

// Get Session Information
$sessionDetails = $db->exams->getSession($_SESSION["session_id"]);

// If we don't have an exam or session then abort
if (is_null($examRecord) || is_null($sessionDetails)) {
    header("Location: ./assess.php?p=exam_configuration");
    exit;
}

// Does the selected station contain multiple scenarios ?
$multiScenario = $db->exams->multiScenarioStation($_SESSION["station_id"]);
        
// Multi Scenarios Selection Order
if ($multiScenario) {
    $firstSelectList = "groups";
    $firstListTitle = "Group";
    $secondSelectList = "stations";
    $secondListTitle = "Station";
} else {
    $firstSelectList = "stations";
    $firstListTitle = "Station";
    $secondSelectList = "groups";
    $secondListTitle = "Group";
}

// Format Date
$date = formatDate($sessionDetails["session_date"]);
$currentSession = $db->exams->getSession($currentSessionID);

// Is logged in user assigned to current exam
$userAssignedToSession = $db->exams->isUserAssignedSession(
        $loggedInUserID, 
        $_SESSION["session_id"]
);

// Show Results Menu Item
$viewResults = $examRecord["view_results"];
if ($userIsAdmin && !$userAssignedToSession) {
    $viewResults = VIEW_RESULTS;
}

// Render assessment menu
$enableMenus = ["config", "students", "change_examiners"];
if ($supportEnabled) {
  $enableMenus[] = "support";
}

if ($viewResults) {
  $enableMenus[] = "results";
}

$template = new Template("assessment_menu.html.twig");
$template->render([
   "enable_menus" => $enableMenus,
   "selected_menu" => "students",
   "skin" => $config->localization['skin'],
   "name" => [
     "short" => $config->getName(),
     "long" => $config->getName(\TRUE)
   ]
]);

if (isset($_SESSION["session_id"]) && isset($_SESSION["station_id"])) {
    $stationSelected = filter_input(
        INPUT_GET,
        "stat_sel",
        FILTER_VALIDATE_INT,
        $filterOptionsPositiveInt
    );
    
    if ($db->exams->doesStationExist($stationSelected, $currentSessionID)) {
        $_SESSION["station_id"] = $stationSelected;
    }
}

// Group Re-Selection
$groupSelected = filter_input(INPUT_GET, "grp_sel", FILTER_VALIDATE_INT, $filterOptionsPositiveInt);
if (isset($_SESSION["session_id"]) && isset($_SESSION["group_id"]) && (!empty($groupSelected))) {
    if ($db->groups->doGroupsExist([$groupSelected], $currentSessionID)) {
        $_SESSION["group_id"] = $groupSelected;
    }
}

// Current Station ID and Group ID
$currentStationID = $_SESSION["station_id"];
$currentGroupID = $_SESSION["group_id"];

/* If the current logged in user is an admin (super or system admin)
 * then we need to provide them with a list of examiners associated with
 * this station so that they can examine as. Only examiners assigned to the station 
 * or that have submitted results at this station should appear in this list.
 */

// Admin mode only and not assigned to exam
 if ($userIsAdmin && !$userAssignedToSession) {

   // Get all examiners associated with the station
   $examiners = $db->exams->getStationAssociatedExaminers(
      $currentStationID,
      $currentGroupID    
   );
   
   // Get assigned examiners
   $assignedExaminers = $db->exams->getExaminersForStation($currentStationID, true);
  
   // Set the current examiner ID
   $examinerID = setCurrentExaminerID($examiners, $userAssignedToSession);
  
 } else {
   $examinerID = $loggedInUserID;  
 }
 
 /* Get composite list of students. Returns unassessed,
  * submitted and incomplete students
  */
 $compositeStudentList = $db->exams->getStationExaminerStudents(
     $currentStationID,
     $currentGroupID,
     $examinerID
 );

if (empty($compositeStudentList)) {
    error_log(__FILE__ . ": Student list is empty. This shouldn't happen.");
    die("<span class='fl clearv2'>Error: Can't find any " . gettext('student') . " you have "
         . gettext('examined') . " or could " . gettext('examine') ." for this " . gettext('form') . "</span>");
}

// Get Station Information
$station = $db->exams->getStation($currentStationID);
if (empty($station)) {
    header("Location: ./assess.php?p=exam_configuration");
    exit;
}

$stationNum = $station["station_number"];
$stationName = $station["form_name"];

// Get Lowest Station Number
$min = $db->exams->getLowestStationNum($currentSessionID);
if (!is_numeric($min) || $min < 0) {
   $min = 0;
} else {
   $min--;
}
$divider = $stationNum - $min;

$toAssessResults = $compositeStudentList["unexamined"];
$areStudentsToAssess = (count($toAssessResults) > 0);

/**
 * If preserve group ordering is disabled then sort students
 * (by expexcted order) in group at station
 */
if ($settingsRecord['preserve_group_order']) {
   orderArrayColumn($toAssessResults, "student_order", "standard", "ASC");
} else {
   orderArrayColumn($toAssessResults, "student_order", "standard", "DESC");
   orderStudentsStation($divider, $toAssessResults);   
}

$incompleteResults = $compositeStudentList["incomplete"];
$areStudentsIncomplete = (count($incompleteResults) > 0);

$submittedResults = $compositeStudentList["submitted"];
$areStudentsAssessed = (count($submittedResults) > 0);

// Sort submitted results by last updated
orderArrayColumn($submittedResults, "updated_at", "standard", "DESC");

ob_end_flush();

?>
  <div class="container-fluid pt-4" id="maincontdiv"><?php
    if ($supportEnabled) {
        renderAssistantCallBox($config);
    }
?>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<form id="abs_student" method="post" action="assess/absent.php">
  <input type="hidden" id="abs" name="abs" value=""/>
  <input type="hidden" id="markabs" name="markabs" value="yes"/>
</form>
      
<form id="discard-form" method="post" action="assess/discard.php">
  <input type="hidden" id="discard-result" name="discard_result" value=""/>
</form>

<div class="card d-inline-block align-top mr-1 mb-5">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/students-assess.svg" style="width: 40px" class="card-title-icon">
        <span class="card-title-text pl-2" style="width: calc(100% - 40px)">
          Choose <?=ucwords(gettext('student'))?>
        </span>
      </div>
    </div>
  </div>
  <div class="card-body" style="font-size: 92%">
    
<div class="card-text" style="max-width: 400px">
    <?=$examRecord['exam_name']?> -  
         <?=linkify($examRecord['exam_description'], ['https', 'http'], ['target' => "_blank"])?>
</div>
<table id="find_student_table" class="border-0 mt-3">
<input type="hidden" id="student-id-mode" value="<?=$settingsRecord["exam_student_id_type"]?>"/>
<tr> 
  <th class="title2" style="padding-top: 6px; padding-bottom: 6px">Day/Circuit</th>
  <td class="td0" colspan="2">
  <?php 

  // Add circuit colour if set
  if (strlen($currentSession["circuit_colour"]) > 0) {
    
    $colourName = $db->exams->getColourName($currentSession["circuit_colour"]); 
    ?>
      <div class="circuit-color-selected" title="<?=$colourName?> circuit" 
          style="background-color:#<?=$currentSession["circuit_colour"]?>">
      </div>
    <?php
    
  }

  // Session description
  echo $date." ".$sessionDetails["session_description"];         

 ?>
  </td>
</tr>

<?php 
     
    /* If the user is an exam admin then allow them to choose from 
     * an examiner to examine as
     */
    if ($userIsAdmin && !$userAssignedToSession) {
      
        $warning = "Choose " . gettext('an examiner') . " to " . gettext('examine') . ", this would be " . gettext('an examiner') . " who is "
                 . "associated with (assigned or " . gettext("assessed") . ") the station "
                 . "(feature only available to " . gettext("osce") . " administrators)";
        
        $adminName = $_SESSION["user_surname"]. ", " . $_SESSION["user_forename"];
        
    ?>
<tr>  
  <th class="examiner-th white"><?=ucwords(gettext('examine'))?> as</th>
  <td class="td0grp" style="min-width: 400px" colspan="2">      
   <div id="examiner-div">   
    <select id="examiner-as">
     <option value="<?=$loggedInUserID?>"><?=$adminName?> (you)</option>
     <?php
     
     foreach ($examiners as $id => $examiner) {

       /* Examiner is admin, then skip as we
        * already have the admin listed in the dropdown
        */ 
       if ($id == $loggedInUserID) {
         continue;
       }

       $eachName = $examiner["surname"].", ".$examiner["forename"];
       $optionText = $eachName." - ".$id;
       $selected = ($examinerID == $id) ? 'selected="selected"' : '';
       
       // Render weighting title
       if (isset($assignedExaminers[$id])) {
         $weightingTitle = $assignedExaminers[$id]["weight_type"];
       } else {
         $weightingTitle = "Regular Weighting";  
       }

       ?>
       <option value="<?=$id?>"<?=$selected?>><?=$optionText?> (<?=$weightingTitle?>)</option>
       <?php
     }
    ?>
     </select>
     <i class="material-icons" style="vertical-align: middle; cursor: help" 
       data-toggle="tooltip" data-placement="right" title="<?=$warning?>">help_outline</i>
    </div>
   <?php 
    
    } else {
      
      // Examiner weighting
      $examinerWeight = $db->exams->examinerStationWeighting(
              $loggedInUserID,
              $currentStationID
      ); 
     
      $examinerWeightLabel = "";
      
      // Examiner weight label
      if (!empty($examinerWeight))
         $examinerWeightLabel = " (" . $examinerWeight["weight_type"] . ")";
      
?>
<tr>
  <th class="title2"><?=ucwords(gettext('examiner'))?></th>
  <td class="td0grp" style="min-width: 400px" colspan="2">
   <?=$_SESSION["user_surname"]?>, <?=$_SESSION["user_forename"]?> - <?=$loggedInUserID?>
        <?=$examinerWeightLabel?>
<?php

 }
 
 ?>
  </td>
</tr>

<tr> 
  <th class="title2 white"><?=$firstListTitle?></th>
  <td class="td0grp"><?php include "show_".$firstSelectList.".php"; ?></td>
 </tr>
<tr> 
  <th class="title2 white"><?=$secondListTitle?></th>
  <td class="td0grp"><?php include "show_".$secondSelectList.".php"; ?></td>
  <td class="p-0">
    <div class="searchdiv">
     <div><?php
      ?><input required="required" type="text" class="form-control form-control-sm" name="st" id="st"
          placeholder="search lists.."/><?php
      ?>
         <input type="button" id="resetser" class="material-icons" value="clear">
         <input type="button" id="searchicon" class="material-icons" value="search">
     <?php
    ?></div>
    </div> 
 </td>  
</tr>

<?php
    
   /**
    * Render student lists
    */
   foreach([TOASSESS_LIST, INCOMPLETE_LIST, ASSESSED_LIST] as $listType) {
      
     // Only show incomplete list if there are students in it
     if ($listType == INCOMPLETE_LIST && count($incompleteResults) == 0) {  
        continue;
     }
     include "student_lists.php";
      
   }
    
?>    
</table>
   </div> 
  </div>
</div>
