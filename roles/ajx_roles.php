<?php
define('_OMIS', 1);

session_start();

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
  
    $loader = require __DIR__ . '/../vendor/autoload.php';
    
}

define("AJAX_MESSAGE_FIELD_NAME", "message");
define("AJAX_STATUS_FIELD_NAME", "result");
define("AJAX_RECORDID_FIELD_NAME", "record_id");

require_once '../extra/usefulphpfunc.php';

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Auth\Role as Role;
use \OMIS\Utilities\JSON as JSON;

function abort($http_code, $message)
{
  
    http_response_code($http_code);
    $response = array(AJAX_STATUS_FIELD_NAME => false, AJAX_MESSAGE_FIELD_NAME => $message);
    
    try {
      
        $json = JSON::encode($response);
        
    } catch (Exception $ex) {
      
        // Something went wrong encoding the data to JSON.
        error_log(__FILE__ . ": JSON Encode failed. Data follows:");
        error_log(print_r($response, true));
        /* The only correct thing to do here is return an internal server error
         * message.
         */
        http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        
    }
    
    die($json);
}

/**
 * Function to update a role object from the submitted form data.
 * 
 * @param {Array} $form_data The JSON data submitted to the form.
 * @param \Role   $role      The Role object to update.
 * @return {boolean}
 */
function updateRecord($form_data, $role)
{
    $role->name = $form_data['role_name'];
    $role->shortname = $form_data['role_shortname'];
    $role->description = $form_data['role_description'];
    $role->importexport_field = $form_data['role_importexport_field'];

    // Attempt to save the new role.
    $succeeded = $role->save();
    return $succeeded;
}

/* If we get to here, we're assuming that we have permission to do stuff, so 
 * it's time to tell the browser it's getting JSON rather than HTML or text or
 * something else.
 */
header('Content-Type: application/json');

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    abort(HttpStatusCode::HTTP_FORBIDDEN, "You are not permitted to access this resource");
}

// TODO: Put in user checking here.
// If there's no JSON passed, whinge. HTTP Error 400 = Bad Request.
if (!isset($_POST['json'])) {
    abort(HttpStatusCode::HTTP_BAD_REQUEST, "Invalid input data");
}

/* Decode the JSON in the request, now that we know we have something to decode
 * In this case, we're decoding to an associative array.
 */
try {
    $json = filter_input(INPUT_POST, 'json', FILTER_SANITIZE_STRING);
    $form = JSON::decode($json, true);
} catch (Exception $ex) {
    error_log(__FILE__ . ": Failed to parse JSON in request from " . $_SERVER['HTTP_REFERER']);
    abort(HttpStatusCode::HTTP_BAD_REQUEST, "Failed to parse JSON in input request");
}

// If an action hasn't been specified, whinge and exit...
if (!isset($form['action'])) {
    abort(HttpStatusCode::HTTP_BAD_REQUEST, "Invalid action");
}

/* Read the action parameter then remove it from the passed-in data; the rest 
 * is the form data to be saved.
 */
$action = trim(strtolower($form['action']));
unset($form['action']);
//error_log(__FILE__ . ": Requested action is '$action'");

$response = [];

// Decide what to do based on what type of operation it is.
switch ($action) {
    case 'add':
        // Here we need to define a new role based on the information that has been passed.
        //error_log(__FILE__ . ": Instantiating new role object");
        $role = new Role();
        
        $succeeded = updateRecord($form, $role);
        
        $response[AJAX_RECORDID_FIELD_NAME] = $role->role_id;
        $response[AJAX_MESSAGE_FIELD_NAME] = "Role data update " . ($succeeded ? "succeeded" : "failed");
        $response[AJAX_STATUS_FIELD_NAME] = $succeeded;
        break;
    case 'update':
        // Load the pre-existing role and set its attributes.
        //error_log(__FILE__ . ": Loading role with ID " . $form['role_id']);
        $role = OMIS\Auth\Role::loadID($form['role_id']);
        
        $succeeded = updateRecord($form, $role);
        
        $response[AJAX_RECORDID_FIELD_NAME] = $role->role_id;
        $response[AJAX_MESSAGE_FIELD_NAME] = "Role data update " . ($succeeded ? "succeeded" : "failed");
        $response[AJAX_STATUS_FIELD_NAME] = $succeeded;
        break;
    case 'delete':
        $roles = $form['role_id'];
        $deleted_roles = Role::delete($roles);
        
        /* PHP (stupidly) doesn't have an in-built mechanism to compare two
         * arrays to see if they contain the same values (or, for that matter,
         * the same key/value pairs. Stupid.
         */
        $succeeded = ($roles == $deleted_roles);
        if (!$succeeded) {
            error_log(
                __FILE__ . ": Attempted to delete the following ids: ["
                . implode(", ", $roles) . "]. Succeeded in deleting these: ["
                . implode(", ", $deleted_roles) ."]"
            );
            $response[AJAX_MESSAGE_FIELD_NAME] = "Partial success: deleted "
                . count($deleted_roles) . " of " . count($roles) . " roles.";
        } else {
            $response[AJAX_MESSAGE_FIELD_NAME] = "Successfully deleted "
                . count($deleted_roles) . " roles.";
        }
        
        // Return a true/false status, and the list of roles deleted.
        $response[AJAX_STATUS_FIELD_NAME] = $succeeded;
        $response[AJAX_RECORDID_FIELD_NAME] = $deleted_roles;
        
        break;
    default:
        abort(HttpStatusCode::HTTP_BAD_REQUEST, "Invalid action specified");
}

// Format a response.
try {
    $json = JSON::encode($response);
} catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($response, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
}

echo $json;
