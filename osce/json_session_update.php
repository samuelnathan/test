<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 define('_OMIS', 1);
 
 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
 use \OMIS\Utilities\JSON as JSON;
 
 include_once __DIR__ . '/../vendor/autoload.php';
    
 if (filter_has_var(INPUT_POST, 'mode')) {
   
    include __DIR__ . '/../extra/essentials.php';

    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
        exit();
        
    }
    
 } else {
   
    http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
    exit();
    
 }

 // json_encode takes care of Html Entities
 $db->set_Convert_Html_Entities(false); 
 
 $mode = trim(filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRIPPED));
 $calendarEnabled = $config->eventQueue['enabled'];
 
 $jsonData = [
   'error' => false  
 ];
 
 switch ($mode) {
 
    case 'update':
     
        $sessionID = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

        if ($db->exams->doesSessionExist($sessionID)) {

            $db->sessions->update(
                $sessionID,
                $_POST['date'],
                $_POST['start_time'],
                $_POST['end_time'],
                $_POST['access'],
                $_POST['description'],
                $_POST['colour']
            );

            // Update Qpercom Google calendar using the Event Queue [If
            // Feature Enabled] 
            if ($calendarEnabled) {
              
                // Get Exam Information
                $examRecord = $db->exams->getExam($db->exams->getSessionExamID($sessionID));
                // Calculate the duration of the exam
                $examTimeRange = $db->exams->getExamTimeRange($examRecord['exam_id']);

                // Notify the event
                $eventNotifier = new OMIS\Events\Calendar\CalendarEventNotifier();
                $eventNotifier->notifyExamUpdate($examRecord, $examTimeRange['time_start_exam'], $examTimeRange['time_end_exam']);
            }

        } else {

            $jsonData['error'] = true;
            http_response_code(HttpStatusCode::HTTP_NOT_FOUND);

        }
        break;
    
    
    case 'submit':
   
        // Post Variables
        $date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING);
        $startTime = filter_input(INPUT_POST, 'start_time', FILTER_SANITIZE_STRING);
        $endTime = filter_input(INPUT_POST, 'end_time', FILTER_SANITIZE_STRING);
        $examID = filter_input(INPUT_POST, 'exam_id', FILTER_VALIDATE_INT);

        if ($db->exams->doesExamExist($examID)) {

            $examRecord = $db->exams->getExam($examID);

            /**
             * (DC) This needs a rethink, what if client is using a different translation
             */
            if (trim($_POST['description']) == "e.g. Morning, Afternoon") {
                $addDescription = "Session";
            } else {
                $addDescription = $_POST['description'];
            }

            $newSessionID = $db->sessions->insert(
                $_POST['date'],
                $startTime,
                $endTime,
                $examID,
                $_POST['access'],
                $addDescription,
                $_POST['colour']
            );

            // Do we have a new session record?
            if ($newSessionID !== false) {

                // Update Qpercom Google calendar using the Event Queue [If 
                // Feature Enabled] 
                if ($calendarEnabled) {

                    $examTimeRange = $db->exams->getExamTimeRange($examID);

                    $eventNotifier = new OMIS\Events\Calendar\CalendarEventNotifier();
                    $eventNotifier->notifyExamUpdate(
                        $examRecord,
                        $examTimeRange['time_start_exam'],
                        $examTimeRange['time_end_exam']);
                        
                }

                if (isset($_POST['clone_stations']) && strlen($_POST['clone_stations']) > 0) {
                    $db->stations->cloneStations(
                        $newSessionID,
                        trim($_POST['clone_stations']),
                        $examRecord['dept_id'],
                        $examRecord['term_id']
                    );
                }

                if (isset($_POST['clone_groups']) && strlen($_POST['clone_groups']) > 0) {
                    $db->exams->cloneGroupNames($newSessionID, trim($_POST['clone_groups']));
                }

                if (isset($_POST['clone_assistants']) && strlen($_POST['clone_assistants']) > 0) {
                    $db->exams->cloneAssistants($newSessionID, trim($_POST['clone_assistants']));
                }

            }
        } else {

          $jsonData['error'] = true;
          http_response_code(HttpStatusCode::HTTP_NOT_FOUND);

        }
        
        break;
    
    default: 
      
        $jsonData['error'] = true;
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);     
        
        break;
    
 }
 
 header("Content-type: application/json");
 try {
   
    $json = JSON::encode($jsonData);
    
 } catch (Exception $ex) {
   
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($jsonData, true));
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
    
 }

 // Data to be sent
 echo $json;
 
 