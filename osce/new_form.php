<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 use \OMIS\Template as Template;

 if (!defined('_OMIS')) {

   define('_OMIS', 1);

 }

 // Ajax mode, if so then check for existence of session_id
 $ajaxMode = filter_input(INPUT_POST, 'isajax', FILTER_SANITIZE_STRING);
 $sessionIDset = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_STRING);

 if (!is_null($ajaxMode) && !is_null($sessionIDset)) {

     include __DIR__ . '/../extra/essentials.php';

     // Page Access Check / Can User Access this Section?
     if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

         return false;

     }

     $sessionID = trim($sessionIDset);

  } else {

     // Critical Session Check
     $session = new OMIS\Session;
     $session->check();

  }

  // Station Time Limit
  if (isset($_SESSION['station_time_limit'])) {

     $defaultStationTime = explode(':', $_SESSION['station_time_limit']);

  } elseif (isset($config->station_defaults['timelimit'])) {

     $defaultStationTime = explode(':', $config->station_defaults['timelimit']);

  } else {

     $defaultStationTime = [00, 05, 00];

  }

  // Alarm 1
  if (isset($_SESSION['station_alarm_one'])) {

     $defaultStationAlarm1 = explode(':', $_SESSION['station_alarm_one']);

  } elseif (isset($config->station_defaults['visualalarm1'])) {

     $defaultStationAlarm1 = explode(':', $config->station_defaults['visualalarm1']);

  } else {

     $defaultStationAlarm2 = [00, 00, 00];

  }

  // Alarm 2
  if (isset($_SESSION['station_alarm_two'])) {

     $defaultStationAlarm2 = explode(':', $_SESSION['station_alarm_two']);

  } elseif (isset($config->station_defaults['visualalarm2'])) {

     $defaultStationAlarm2 = explode(':', $config->station_defaults['visualalarm2']);

  } else {

     $defaultStationAlarm2 = [00, 00, 00];

  }

  // Next station number, pass value and popup option
  $nextStationNumber = $db->exams->getHighestStationNumber($sessionID) + 1;
  $passConfig = (isset($config->station_defaults["passvalue"])) ? $config->station_defaults["passvalue"] : 50;
  $passValue = (isset($_SESSION['station_pass_value'])) ? $_SESSION['station_pass_value'] : $passConfig;
  $weighting = (isset($_SESSION['station_weighting'])) ? $_SESSION['station_weighting'] : 0;
  $popupSession = (isset($_SESSION['default_popup_value']) && $_SESSION['default_popup_value'] == 1);
  $popupConfig =  isset($config->station_defaults['popupnotes']) && $config->station_defaults['popupnotes'];

  $data = [
     "type" => 'new_station',
     "sessionExists" => $db->exams->doesSessionExist($sessionID),
     "popupFeatureEnabled" => $db->features->enabled('station-notes', $_SESSION['user_role']),
     "exclude_grade_enabled" => ($db->exams->getExam(
              $db->exams->getSessionExamID($sessionID)
     )['grade_rule'] > 1),
     "popupSelected" => ($popupSession || $popupConfig),
     "nextNumber" => $nextStationNumber,
     "passValue" => $passValue,
     "weighting" => $weighting,
     "defaultTime" => $defaultStationTime,
     "defaultAlarm1" => $defaultStationAlarm1,
     "defaultAlarm2" => $defaultStationAlarm2
  ];

  // Render HTML template
  $template = new Template('newStationSettings.html.twig');
  $template->render($data);
