<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
return false;
}

// Get Exam (Includes settings)
$examID = filter_input(INPUT_GET, 'exam', FILTER_SANITIZE_NUMBER_INT);
$exam = $db->exams->getExam($examID, true);
$settingsRecord = $db->examSettings->get($examID);

$sectionTitle = gettext('OSCE Settings');
$sectionDescription = "Choose results columns to be displayed during " . gettext('assessment') ."<br/>"
                    . "Choose which " . gettext('student') . "  identifier type to display during " . gettext('assessment');

// Load and render the section description template
$sectionDescriptionData = [
      "title" => $sectionTitle,
      "description" => $sectionDescription,
      "return_button" => true,
      "return_text" => "Return to " . gettext('OSCE List'),
      "section_text" => [
           gettext('OSCE') => $exam['exam_name'], 
           gettext('Department') => $exam['dept_name']
      ],
      "additional_buttons" => [[
           "type" => "button", "id" => "update-settings",
           "value" => "Update Settings", "disabled" => true
      ]]
];

$template = new OMIS\Template("section_top_description.html.twig");
$template->render($sectionDescriptionData);

// Check exam to see if it exists
if (empty($examID) || empty($exam) || is_null($exam)) {
  echo "<br/><br/>" . gettext('Exam') . " does not exist, please return to previous section";
} else if (empty($settingsRecord) || is_null($settingsRecord)) {
  echo "<br/><br/>" . gettext('Exam') . " settings not found, please return to previous section";
} else {

  $settings = [];
  $settingsConfig = [];

   // Does config column settings exist?
  if (isset($config->exam_defaults['result_columns'])) {
      $settingsConfig = $config->exam_defaults['result_columns'];
  }

  // Setting types
  $types = ['identifier', 'surname', 'forename', 'session', 'date',
            'group', 'form', 'number', 'pass', 'score', 'percentage', 'grs'];

 // Gather settings values
 foreach ($types as $type) {

   // Custom title from config file ?
   $title = (isset($settingsConfig[$type]['title'])) ? $settingsConfig[$type]['title'] : $type;
   $settings['results'][$type] = [
        'title' => $title,
        'enabled' => $settingsRecord['results_column_' . $type]
    ];

  }

// Exam number feature enabled/disabled
$examNumberFeature = $db->features->enabled("exam-number", $_SESSION["user_role"]);

// Candidate number feature enabled/disabled
$candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);

/**
 * Default type of identifier to display during assessment:
 * NOTE: codes are case sensitive
 *  S => Student Identifier
 *  E => Exam Number
 *  C => Candidate Number
 */
 $settings['exam_student_id_type'] = $settingsRecord['exam_student_id_type'];

 /**
  *  Preserve group order (if set to yes, does not sort student order per group
  *  at each station in assessment tool)
  */
 $settings['preserve_group_order'] = $settingsRecord['preserve_group_order'];

  /**
   *  Auto config station. If set to yes, then examiners assigned to stations don't need to configure their station.
   *  They are presented with their preconfigured station(s)
   */
 $settings['auto_config_station'] = $settingsRecord['auto_config_station'];

 /**
  *  Should feedback fields appear to right of each scoresheet section, if not then under each scoresheet section
  */
 $settings['feedback_to_right'] = $settingsRecord['feedback_to_right'];


 // Settings template
 $template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__));
 $data = [
    'exam_id' => $examID,
    'settings' => $settings,
    'exam_number_feature' => $examNumberFeature,
    'candidate_number_feature' => $candidateNumberFeature
 ];
 $template->render($data);

}

?>
