<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Template as Template;
define('_OMIS', 1);

$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);
if (!is_null($isfor)) {
    require_once __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// Update Exam Settings
if ($isfor == 'update') {  
   
    $examID = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
    $settings = filter_input(INPUT_POST, 'settings', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);        
     
    // Update exam settings if exam exists
    if ($db->exams->doesExamExist($examID)) {
      
      // Update settings
      if ($db->examSettings->update($examID, $settings)) {
        echo 'SETTINGS_UPDATED';    
      } else {
        echo 'SETTINGS_UPDATE_FAIL';   
      }
    } else {
        echo 'EXAM_NON_EXIST';
    }
    
}
