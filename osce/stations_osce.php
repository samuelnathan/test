<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
 use \OMIS\Template as Template;

 // Definitions
 define('RETURN_MYSQLI_RESULT', true);
 define('INCLUDE_REST_STATIONS', true);

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }

 // Whats required?
 require_once __DIR__ . '/../extra/helper.php';
 require_once __DIR__ . '/osce_functions.php';

 // Initial variables
 $stationDeletionProhibited = [];
 $returnUrl = "manage.php?page=sessions_osce&amp;exam_id=";
 $rowCount = 0;
 $exists = false;

 // Session stations that had results attached / deletion prohibited
 if (isset($_SESSION['stations_with_results_warning'])) {
    $stationDeletionProhibited = $_SESSION['stations_with_results_warning'];
    unset($_SESSION['stations_with_results_warning']);
 }

 // Feature Enabled|Disabled
 list($multiExaminersAllowed, $editContentAllowed) = 
 $db->features->enabled([
      'multi-examiners', 'assess-content'
      ], $_SESSION['user_role']
 );        

 $hideExaminerColumn = ($multiExaminersAllowed) ? "" : "hide";
 $sessionID = filter_input(INPUT_GET, 'session_id', FILTER_SANITIZE_NUMBER_INT);
 $sessionDB = $db->exams->getSession($sessionID);

 // Session record exists
 if (!is_null($sessionDB)) {
    $exists = true;
    $examID = $sessionDB['exam_id'];
    $stationsDB = $db->exams->getStations(
            [$sessionID],
            !RETURN_MYSQLI_RESULT,
            'station_number', 
            'form_name', 
            INCLUDE_REST_STATIONS
    );
    $deptID = $sessionDB['dept_id'];
     // print_r($stationsDB);
    $totalStationCount = mysqli_num_rows($stationsDB);

    // Get first and last exam sessions
    $sessionsDB = array_column(
       $db->exams->getExamSessions($examID, true),
       'session_id'
    );

    $arrayHelp = new \OMIS\Utilities\ArrayHelp($sessionsDB);

    // Previous and next session urls
    list($previousSession, $nextSession) = $arrayHelp->previousAndNext($sessionID);
    $previousUrl = empty($previousSession) ? "" : "manage.php?" 
    . http_build_query([
        'page' => "stations_osce",
        'session_id' => $previousSession,
        'exam_id' => $examID
    ]);
    
    $nextUrl = empty($nextSession) ? "" : "manage.php?" 
    . http_build_query([
        'page' => "stations_osce",
        'session_id' => $nextSession,
        'exam_id' => $examID
    ]);

    $templateData = [
        'stationCount' => $totalStationCount,
        'session' => [
            'id' => $sessionID,
            'date' => strtotime($sessionDB['session_date']),
            'description' => $sessionDB['session_description']
        ],
        'exam' => [
            'id' => (int) $sessionDB['exam_id'],
            'name' => $sessionDB['exam_name']
        ],
        'department' => [
            'id' => $sessionDB['dept_id'],
            'name' => $db->departments->getDepartmentName($sessionDB['dept_id'])
        ],

       /**
        * Jump to group records previous and next
        */
       "jumpRecords" => [
          "customEvents" => true,
          "boxWidth" => 'auto',
          "previousRecordLabel" => "Day/Circuit",
          "nextRecordLabel" => "Day/Circuit",
          "returnLabel" => "Return to Day/Circuit List",
          "previousUrl" => $previousUrl,
          "nextUrl" => $nextUrl
       ]
    ];
 } else {
    $exists = \FALSE;
    $templateData = [];
 }
    
 $templateData['exists'] = $exists;

 // Render the template.
 (new Template("stations_osce.header.html.twig"))->render($templateData);

 ?>
 <div class="contentdiv"><?php
 
 (new Template("station_grouping_explained.html.twig"))->render();

 // Session exists?
 if ($exists) {
 ?>
 <form name="stations_list_form" id="stations_list_form" method="post" action="osce/exams_redirect.php" onsubmit="return(ValidateForm(this))">
 <table id="stations_table" class="table-border">
   <tr id="title-row" class="title-row">
   <td class="all-checkbox-td">
     <input type="checkbox" id="check-all" <?php if(!$totalStationCount) { ?>disabled="disabled"<?php } ?>/>
   </td>
   <th><label>Number</label></th>
   <th>
     <label id="tagging-info" title=""><?=ucwords(gettext('scenario-group'))?>
      <img class="tags-info" src="assets/images/information.png" alt="information" width="16" height="16"/>
     </label>
   </th>
   <th><label>Pass</label></th>
   <th><label>Weighting</label></th>
   <th><label>Time</label></th>
   <th><label>Alarms</label></th>
   <th><label><?=ucwords(gettext('content-form'))?></label></th>
   <th class="<?=$hideExaminerColumn?>"><label><?=ucwords(gettext('examiners'))?></label></th>
   <th><label>Settings</label></th>
   <th id="options-th">&nbsp;</th>
   <td class="title-edit-top">&nbsp;</td>
   </tr>
   
   <?php   

   // We have stations
   if($stationsDB && $totalStationCount > 0) {
     
    // Loop through stations
    while($eachStation = $db->fetch_row($stationsDB)) {
      //to get station type
  $station_type = $db->sessions->getStationType($eachStation['station_id']);

      $rowCount++;
      $stationID = $eachStation['station_id'];
      $stationNumber = $eachStation['station_number'];
      $restStation = $eachStation['rest_station'];
       
      // Rest Station
      if ($restStation) {
        ?>
        <tr id="stations_row_<?=$rowCount?>" class="datarow rest-station-row">
        <td class="checkb">
          <input type="checkbox" name="stations_check[]" id="stations-check-<?=$rowCount?>" value="<?=$stationID?>"/>
        </td>
        <td class="rest-station-number"><?=$stationNumber?></td>
        <td colspan="<?=($hideExaminerColumn ? 7 : 8)?>" class="rest-station">Rest Station</td>
        <td></td>
        <td class="nbr"></td>
        </tr>
       <?php
       
      // Station rows
      } else {
        $hasItems = $db->forms->doesFormHaveItems($eachStation['form_id']);
        $stationLinkType = ($hasItems) ? "edit" : "add";
        $stationTitle = "Click to " . $stationLinkType . " content";
        
        // Station content link
        $stationContentLink = "manage.php?page=examform" .
                              "&amp;form_id=" . $eachStation['form_id'] .
                              "&amp;station_id=" . $stationID .
                              "&amp;session_id=" . $sessionID . 
                              "&amp;show_session=" . iS($_GET, 'show_session');
                            
        // Station examiner link
        $stationExaminerlink = "manage.php?page=examiner_osce" . 
                               "&amp;station_id=" . $stationID . 
                               "&amp;session_id=" . $sessionID . 
                               "&amp;show_session=" . iS($_GET, 'show_session');

        // Station form preview link
        $formPreviewlink = "manage.php?page=previewform_osce" . 
                           "&amp;frm=" . $sessionID . 
                           "&amp;form_id=" . $eachStation['form_id'] . 
                           "&amp;station_id=" . $stationID;                               

    ?><tr id="stations_row_<?=$rowCount?>" class="datarow">
        <td class="checkb">
         <input type="checkbox" name="stations_check[]" id="stations-check-<?=$rowCount?>" value="<?=$stationID?>"/>
        </td>
        <td class="c"><?=$stationNumber; 
         
            // Delete results warning
            if(in_array($stationID, $stationDeletionProhibited)) {
               ?><div class="cannot-delete-station word-wrap">Cannot delete station results attached</div><?php
            }
        
       ?></td>
        <td class="c italic"><?=$eachStation['station_code']?></td>
        <td class="pass-time-cell"><?=(float)$eachStation['pass_value']. "%"?></td>
        <td class="pass-time-cell"><?=(float)$eachStation['weighting']. "%"?></td>
        <td class="pass-time-cell"><?=$eachStation['station_time']?></td>
        <td class="pass-time-cell">
           <?=$eachStation['visual_alarm1']?><br/><?=$eachStation['visual_alarm2']?>
        </td>
        <td><div class="formnamediv"><?php
        
            // Assessment form name
            if ($editContentAllowed) {
              $linkTitle = "Click to $stationLinkType content";
              ?><a href="<?=$stationContentLink?>" class="linkin" title="<?=$linkTitle?>"><?php
                echo $eachStation['form_name'];
              ?></a><?php

            } else {
              ?><span><?=$eachStation['form_name']?></span><?php  
            }
          
           // No items
           if (!$hasItems) {
             ?><span>&nbsp;&nbsp;[<span class="red">no content</span>]</span><?php 
           }
           
           ?>
            &nbsp;
            </div>
          </td>
         
        <td class="managetd<?=" ".$hideExaminerColumn?>">
 <?php
        examinersDisplay($stationID);
    ?>
            <br/>
            <a href="<?=$stationExaminerlink?>" class="linkin">assign <?=gettext('examiners')?></a><br/><br/>
        </td>
        <td class="c">
            <ul class="settings settings-margin">
        <?php

        // Station notes popup modal
        if ($db->features->enabled("station-notes", $_SESSION['user_role'])) {

            ?>
                <li><label title="Station/case notes display on load of assessment form? yes/no">NOTES AUTO POPUP</label> 
            <?php 
            if ($eachStation['desc_auto_popup']) { 
                ?>
                <span class="complete">YES</span>
            <?php 
            } else {
                ?>
                <span class="notcomplete">NO</span>
            <?php 
            }
                ?>
                </li>
        <?php

        }
        //show station type

         if ($station_type['station_type'] != "") {

            ?>
                <li><label title="Station Type">STATION TYPE:</label> 
          
                <span class="complete"><?php echo strtoupper($station_type['station_type']); ?></span>
           
              
                </li>
        <?php

        }

        // Only grading/Appointability has be configured
        if ($sessionDB['grade_rule'] > 1) {

        ?>
            <li>
            <label title="Switch on to exclude station from <?=gettext('grade')?> rules">
            EXCLUDE <?=gettext("GRADING")?>
            </label> 
            <?php 

            if ($eachStation['exclude_from_grading']) { 

                ?><span class="complete">YES</span><?php 

            } else {

                ?><span class="notcomplete">NO</span><?php 

            }
                ?>
            </li>
        
       <?php } ?>

        </ul>
        <br/>
        </td>
        <td class="options-td">
          <a class="form-preview-link" href="<?=$formPreviewlink?>" target="_station<?=$stationID?>">
            <img class="previewbu" src="assets/images/preview.svg" alt="Preview Station"/>
          </a>
        </td>
        <td class="titleedit height30">
            <?php editButton("edit_".$rowCount, "Edit", "Edit", "editb")?><br/><br/>
        </td>
     </tr>
         <?php
        }
      }
    }
    
    // Render bottom table rows (options/button controls)
        $data = [
            'colspan' => 10,
            'session_id' => $sessionID,
            'exam_id' => $examID,
            'row_count' => $rowCount,
            'show_session' => iS($_GET, 'show_session')
        ];
        (new Template("stations_osce.bottom.row.html.twig"))->render($data);
        
    ?></table>
 </form>
 <?php
 } else {
   ?>
  <div class="errordiv">The day/circuit selected or specified was not found in the system<br/><br/>
   <a href="<?=$returnUrl.trim(iS($_GET, 'exam_id'))?>">click here to return to day/circuit list</a>
  </div>
 <?php
 }
 ?>
  <div style="display:none" id="name-taken-confirm" title="Use existing assessment <?=gettext('form')?>?">
     <br/>
     Assessment <?=gettext('form')?> with name
      <span class="assess-form-name"></span> already exists
        in <span id="exists-dept-term"><?=gettext('form')?> bank for current <?=gettext('department')?>
          <span class="italic red"><?=$sessionDB['dept_id']?></span>
      and <?=gettext('academic term')?> <span class="italic red"><?=$sessionDB['term_id']?></span></span>
      <span style="display:none" id="exists-circuit">this Day/Circuit</span>

      <ul id="form-name-change">
         <li id="form-existing-li">
             <input type="radio" id="form-existing" name="specify_form" checked="checked">
             <label for="form-existing">Use existing <?=gettext('form')?> <span class="assess-form-name"><?=gettext('form')?></span></label>
         </li>
          <li id="form-new-li">
              <input type="radio" id="form-new" name="specify_form">
              <label for="form-new">Create new <?=gettext('form')?> with a different name</label>
              <input type="text" id="new-form-name" value="" autocomplete="off"
                     placeholder="new <?=gettext('form')?> name.." size="40"
                     disabled="disabled"
              />
              <label id="form-name-warm" style="display:none">Please enter a new name</label>
          </li>
      </ul>

  </div>
 </div>