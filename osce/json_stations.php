<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 17/11/2015
 * © 2015 Qpercom Limited.  All rights reserved
 * @For Adding New Stations To Session Section
 * @Added station exist already check is_for = 'get_stations' 23/10/2012
 */
define('_OMIS', 1);
ob_start();
$loader = require __DIR__ . '/../vendor/autoload.php';

// Need to tell the server end that we want JSON back...
header('Content-type: application/json');

// Instantiate the configuration object if it's not already set.
global $config;
if (!isset($config)) {
    $config = new OMIS\Config;
}

// Do a session check to see if the user's logged in or not.
global $session;
if (!isset($session)) {
    $session = new OMIS\Session();
    $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
}

// JSON Helper functions.
use OMIS\Utilities\JSON as JSON;

$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

if (!empty($isfor)) {
    include __DIR__ . '/../extra/essentials.php';
    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// Instantiate the database access "class"...
include_once __DIR__ . '/../classes/Database/CoreDB.php';
global $db;
if (!isset($db)) {
    $db = CoreDB::getInstance(
        $config->db,
        $config->localization['modes']['production']
    );
}

$db->set_Convert_Html_Entities(false); #json_encode takes care of Html Entities
$returnResult = array();

// Get Sessions
if ($isfor == 'get_sessions') { 
    $exam_id = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
    if ($db->exams->doesExamExist($exam_id)) {
        $sessions = $db->exams->getExamSessions($exam_id);
        while ($session = $db->fetch_row($sessions)) {
            $stationCount = $db->exams->doesSessionHaveStations(
                    $session['session_id'], true
            );
            if ($stationCount > 0) {
                $returnResult[] = array(
                    'sessionid' => $session['session_id'],
                    'sessiondesc' => $session['session_description'],
                    'sessiondate' => formatDate($session['session_date']),
                    'stationcount' => $stationCount
                );
            }
        }
    }
    
// Get Stations
} elseif ($isfor == 'get_stations') { 
    $selectedSessionID = iS($_POST, 'selected_session_id');
    $currentSessionID = iS($_POST, 'current_session_id');
    
    $selectedSessionExists = $db->exams->doesSessionExist($selectedSessionID);
    $currentSessionExists = $db->exams->doesSessionExist($currentSessionID);
    
    if ($selectedSessionExists && $currentSessionExists) {
        
        // Get existing form IDs by session ID
        $existingFormIDs = $db->forms->getFormIDsBySession($currentSessionID);
        
        // Get stations for selected session
        $stationsDB = $db->exams->getStations([$selectedSessionID]);
        
        // Loop through all stations for selected session and add to return list
        while ($station = $db->fetch_row($stationsDB)) {
           
            // If not in current list of forms ID's then don't return
            if (!in_array($station['form_id'], $existingFormIDs)) {
                $returnResult[] = [
                    'formid' => $station['form_id'],
                    'formname' => $station['form_name'],
                    'stationnum' => $station['station_number']
                ];
            }
            
        }
    }
}

try {
    $json = JSON::encode($returnResult);
} catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnResult, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HTTP_INTERNAL_SERVER_ERROR);
    exit();
}

echo $json;
