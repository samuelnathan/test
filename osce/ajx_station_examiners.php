<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Database\CoreDB as CoreDB;

define('_OMIS', 1);

include_once __DIR__ . '/../vendor/autoload.php';

$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

if (!is_null($isfor)) {
  
    include dirname(__FILE__) . '/../extra/essentials.php';
    
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        reportErrorandExit(HttpStatusCode::HTTP_FORBIDDEN);
        
    }
    
    require_once dirname(__FILE__) . '/osce_functions.php';
    
} else {
  
    reportErrorandExit(HttpStatusCode::HTTP_FORBIDDEN);
    
}

/**
 * Get list of examiners in the exam department
 */
if ($isfor == "get_examiners") {
   
  // Get passed in station ID
  $stationID = filter_input(INPUT_POST, 'station', FILTER_SANITIZE_NUMBER_INT);
    
  // Station ID doesn't exist then get the hell out of here
  if (is_null($stationID) || !$db->exams->doesStationExist($stationID)) {
    
      reportErrorandExit(HttpStatusCode::HTTP_BAD_REQUEST);
      
  }
  
  // Get session ID
  $sessionID = $db->exams->getSessionIDByStationID($stationID);
  
  // Get department ID
  $deptID = $db->departments->getDepartIDBySessionID($sessionID);
      
  /*
   *  Examiners filtering criteria to be passed to
   *  the database
   */
  $examinersFilters = [
      $deptID,             // Assessment department
      $_SESSION['cterm'],  // Current academic term
      null,                // All examiner roles
      "",                  // All account types only 
      "",                  // Don't filter by forenames 
      "",                  // Don't filter by surname
      ""                   // Don't filter by search term
  ];

  // Required fields
  $requiredFields = [
       'users.user_id',
       'surname',
       'forename'
   ];
  
  // Get examiners records
  $examinersRecords = $db->users->getExaminers(
       $examinersFilters,
       'users.surname',
       0,
       PHP_INT_MAX,
       $requiredFields
  );
  
  // Get all school admin records
  $administratorRecords =
         CoreDB::into_array(
       $db->users->getAllAdmins($requiredFields)
  );
  
  // Get system admin records
  $systemAdminRecords = CoreDB::into_array(
          $db->users->getSystemAdmins()
  );
  
  // Merge both examiner and admin records
  $allUserRecords = array_merge(
       $examinersRecords,
       $administratorRecords,
       $systemAdminRecords
  );
 
  // Sort merged array of examiners and administrators by surname
  usort($allUserRecords, function ($value1, $value2) {
      return strcasecmp($value1['surname'], $value2['surname']);
  });
  
  // Return list of students
  echo json_encode($allUserRecords);

 // Request unknown
} else {
  
  reportErrorandExit(HttpStatusCode::HTTP_BAD_REQUEST); 
  
}

/*
 *  Set the http status code and die()
 */
function reportErrorandExit($httpStatusCode = HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR) {
  
    http_response_code($httpStatusCode);
    
    error_log(
      "PHP error: failed to get request at station examiners section (http status code: " .
      $httpStatusCode . ")."
    );
    
    exit();
    
}
