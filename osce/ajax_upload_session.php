<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

define('_OMIS', 1);

 $assessment = filter_input(INPUT_POST, 'assessment', FILTER_SANITIZE_STRING);
 $mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
 $session_id = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_STRING);
 $group_id = filter_input(INPUT_POST, 'group_id', FILTER_SANITIZE_STRING);
 $session_id_ss = filter_input(INPUT_POST, 'session_id_ss', FILTER_SANITIZE_STRING);

 include __DIR__ . '/../extra/essentials.php';

if($mode == "assessment"){
 //session

$session_data = $db->exams->getExamData($assessment);
?>
<option value="">Select Session...</option>
<?php
while($session = $db->fetch_row($session_data)){
	?>
			<option value='<?php echo $session['session_id']; ?>'><?php echo $session['session_description'] ?></option>
	<?php
}

}

if($mode == "group"){
	//group 
	$group_data = $db->exams->getGroupData($session_id);
	// print_r($db->fetch_row($group_data)); die();
	?>
	<option value="">Select Group...</option>
	<?php
while($group = $db->fetch_row($group_data)){
	?>
			<option value='<?php echo $group['group_id']; ?>'><?php echo $group['group_name']; ?></option>
	<?php
}

}

if($mode == "student"){
	//group 
	$student_data = $db->exams->getStudentData($group_id);
	// print_r($db->fetch_row($group_data)); die();
	
while($student_row = $db->fetch_row($student_data)){

		$student_detail = $db->exams->getStudentDetails($student_row['student_id']);

		$student = $db->fetch_row($student_detail);

	?>
	<tr>
		<td><?php echo $student['student_id']; ?></td>
		<td><?php echo $student['forename']." ".$student['surname']; ?></td>
		<td><input type="radio" name="student_id" value="<?php echo $student['student_id']; ?>"></td>
	</tr>
	<?php

}

}

if($mode == "scoresheet"){
	//get form id from

	$form = $db->exams->getFormID($session_id_ss);
?>	<option value="">Select Scoresheet...</option> <?php
	while($formid = $db->fetch_row($form)){
		
		$form_data = $db->exams->getForm($formid['form_id']);

		$form_detail = $db->fetch_row($form_data);
?>
		<option value='<?php echo $form_detail['form_id']; ?>'><?php echo $form_detail['form_name']; ?></option>
<?php	
}


}
?>

