<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 use \OMIS\Template as Template;
 use \OMIS\Auth\Role as Role;
 use \OMIS\Database\CoreDB as CoreDB;
 
 if (!defined('_OMIS')) {
    define('_OMIS', 1);
 }

 // Ajax mode, if so then check for existance of session_id
 $ajaxMode = filter_input(INPUT_POST, 'isajax', FILTER_SANITIZE_STRING);
 $sessionIDset = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_STRING);

 if (!is_null($ajaxMode) && !is_null($sessionIDset)) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
 
    $sessionID = trim($sessionIDset);
 } else {
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
 }
 
 // Station Time Limit
 if (isset($_SESSION['station_time_limit'])) {
   $defaultStationTime = explode(':', $_SESSION['station_time_limit']);
 } elseif (isset($config->station_defaults['timelimit'])) {
   $defaultStationTime = explode(':', $config->station_defaults['timelimit']);
 } else {
   $defaultStationTime = [00, 05, 00]; 
 }
 
 // Alarm 1
 if (isset($_SESSION['station_alarm_one'])) {
    $defaultStationAlarm1 = explode(':', $_SESSION['station_alarm_one']);
 } elseif (isset($config->station_defaults['visualalarm1'])) {
    $defaultStationAlarm1 = explode(':', $config->station_defaults['visualalarm1']);
 } else {
    $defaultStationAlarm2 = [00, 00, 00];
 }

 // Alarm 2
 if (isset($_SESSION['station_alarm_two'])) {
    $defaultStationAlarm2 = explode(':', $_SESSION['station_alarm_two']);
 } elseif (isset($config->station_defaults['visualalarm2'])) {
    $defaultStationAlarm2 = explode(':', $config->station_defaults['visualalarm2']);
 } else {
    $defaultStationAlarm2 = [00, 00, 00];
 }

 // Previous Exams for Admins & Super Admins
 if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
    
    $exams = CoreDB::group(
               CoreDB::into_array(
                 $db->exams->getList()
    ), ['term_id'], true);
  
 
 // Previous Exams for School Admins
 } else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {
    
    $adminSchools = $db->schools->getAdminSchools(
       $_SESSION['user_identifier']
    );
   
    $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
    
    $exams = CoreDB::group(
        CoreDB::into_array(
      $db->exams->getListFiltered(null, $schoolIDs)
    ), ['term_id'], true);
    
 // Previous Exams for Examiners
 } else {
  
    // Get Examiner Departments
    $examinerDepts = [];
    $examinerDeptsDB = $db->departments->getExaminerDepartments($_SESSION['user_identifier']);
 
    while ($dept = $db->fetch_row($examinerDeptsDB)) {
        $examinerDepts[] = $dept['dept_id'];
    }
 
    $exams = CoreDB::group(
               CoreDB::into_array(
                 $db->exams->getList($examinerDepts)
    ), ['term_id'], true);
    
 }
 
 // Next station number, pass value and popup option
 $nextStationNumber = $db->exams->getHighestStationNumber($sessionID) + 1;
 $passConfig = (isset($config->station_defaults["passvalue"])) ? $config->station_defaults["passvalue"] : 50;
 $passValue = (isset($_SESSION['station_pass_value'])) ? $_SESSION['station_pass_value'] : $passConfig;
 $weighting = (isset($_SESSION['station_weighting'])) ? $_SESSION['station_weighting'] : 0;   
 $popupSession = (isset($_SESSION['default_popup_value']) && $_SESSION['default_popup_value'] == 1);
 $popupConfig =  isset($config->station_defaults['popupnotes']) && $config->station_defaults['popupnotes'];
 
 $data = [
    "type" => 'previous_exam_station',
    "sessionExists" => $db->exams->doesSessionExist($sessionID),
    "popupFeatureEnabled" => $db->features->enabled('station-notes', $_SESSION['user_role']),
    "exclude_grade_enabled" => ($db->exams->getExam(
             $db->exams->getSessionExamID($sessionID)
    )['grade_rule'] > 1),
    "popupSelected" => ($popupSession || $popupConfig),
    "nextNumber" => $nextStationNumber,
    "passValue" => $passValue,
    "weighting" => $weighting,
    "defaultTime" => $defaultStationTime,
    "defaultAlarm1" => $defaultStationAlarm1,
    "defaultAlarm2" => $defaultStationAlarm2,
    "exams" => $exams
 ];
 
 // Render HTML template
 $template = new Template('newStationSettings.html.twig');
 $template->render($data);
 