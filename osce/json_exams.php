<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @JSON related requests for exams
 */

 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;  
 use \OMIS\Utilities\JSON as JSON;

 define('_OMIS', 1);
 ob_start();

 // What we are requesting
 $request = filter_input(INPUT_POST, 'request', FILTER_SANITIZE_STRIPPED);

 include_once __DIR__ . '/../vendor/autoload.php';
 
 if (!is_null($request)) {
  
    include __DIR__ . '/../extra/essentials.php';
    
    // Do a session check to see if the user's logged in or not.
    if (!isset($session)) {
      
        $session = new OMIS\Session();
        $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
        
    }

    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        return false;
        
    }
    
 } else {
   
    http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
    exit();
    
 }

 // json_encode takes care of Html Entities
 $db->set_Convert_Html_Entities(false); 

 // Return data
 $jsonData = [];

 // Process request
 switch ($request) {
   case 'exam_results_status':
       $examsParamArray = $_POST['exams'];

       foreach ($examsParamArray as $examID) {
         
            $jsonData[$examID] = $db->results->doesExamHaveResults($examID) ||
                    $db->selfAssessments->existsByExam($examID);
            
       }

       break;
     default:
       http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
       exit();       
 }

 header("Content-type: application/json");
 try {
   
    $json = JSON::encode($jsonData);
    
 } catch (Exception $ex) {
   
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($jsonData, true));
    
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
    
 }

 // Data to be sent
 echo $json;
