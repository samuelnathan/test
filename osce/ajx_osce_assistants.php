<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('_OMIS', 1);

// What mode
$mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

if (!is_null($mode)) {
    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    require_once __DIR__ . '/osce_functions.php';
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// Add Existing Assistant
if ($mode == 'add_existing_assistant') {
    $session_id = trim($_POST['session_id']);
    $email = '';
    $mobile = '';
    
    //Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {

?>
    <td class="checkb">&nbsp;</td>
    <td colspan="3">
        <label for="change-existing-assistant" class="boldgreen mb-0">Previous Assistants</label><br/>
        <select id="change-existing-assistant" class="custom-select custom-select-sm">
<?php
        //Get Assistants not in Session
        $assistantsDB = $db->exams->getAssistantsNotInSession($session_id);
        $count = mysqli_num_rows($assistantsDB);
        if ($count > 0) {
            $rwcnt = 0;
            while ($assistant = $db->fetch_row($assistantsDB)) {
                $rwcnt++;
                if ($rwcnt == 1) {
                    $email = $assistant['email'];
                    $mobile = $assistant['contact_number'];
                }
                
            //Option Value [Forenames and Surname]    
            $optionValue = $assistant['forename'] .' '. $assistant['surname'];
            ?><option value="<?=$assistant['user_id']; ?>" title="Identifier: <?=$assistant['user_id'] ?>"><?=$optionValue; ?></option><?php
            
            }
        } else {
           ?><option value="*--*">No Assistants to select from</option><?php
        }
?>
    </select><br/><br/>
   </td>
   
   <td id="add-email-td">
      <br/><input class="form-control form-control-sm" type="text" id="add-email-existing" value="<?=$email; ?>" size="26"/>
      <span class="boldgreen f10">leave blank if unknown</span><br/><br/>
   </td>
   <td id="add-mobile-td">
      <br/><input class="form-control form-control-sm" type="text" id="add-mobile-existing" value="<?=$mobile; ?>"/>
      <span class="boldgreen f10">leave blank if unknown<br/>Must include country code</span><br/><br/>
   </td>
   <td class="titleedit">&nbsp;</td>
   
<?php
    } else {
        echo "SESSION_NON_EXIST";
    }
  
// Assistant Selection
} elseif ($mode == 'assistant_selection') {
    $userID = $_POST['user_id'];
    if ($db->users->doesUserExist($userID)) {
        $userDB = $db->users->getUser($userID, 'user_id', true);
        echo $userDB['email']."[*&&*]".$userDB['contact_number'];
    } else {
        echo "ASSISTANT_NON_EXIST";
    }
    
// Submit Existing Assistant
} elseif ($mode == 'submit_existing_assistant') {
    $session_id = trim($_POST['session_id']);
    
    // Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {
        $userID = trim($_POST['user_id']);
        
        // Does User Exist
        if ($db->users->doesUserExist($userID)) {
            
            // Update User
            $db->users->updateUser(
                $userID, 
                null, 
                null, 
                null, 
                $_POST['add_email'], 
                $_POST['add_mobile']
            );
                        
            // Attach Assistants            
            $db->exams->attachAssistantsToSession($userID, $session_id);
            
        } else {
            echo "ASSISTANT_NON_EXIST";
        }
    } else {
        echo "SESSION_NON_EXIST";
    }
    
// Add New Assistant
} elseif ($mode == 'add_new_assistant') {
    $session_id = trim($_POST['session_id']);
    
    // Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {
?>  <td class="checkb">&nbsp;</td>
    <td><input type="text" id="add-id" class="tf4 form-control form-control-sm"/></td>
    <td><input type="text" id="add-fn" class="tf3 form-control form-control-sm" /></td>
    <td><input type="text" id="add-sn" class="form-control form-control-sm"/></td>
    <td><input type="text" id="add-email" size="26" class="form-control form-control-sm"><span class="boldgreen f10">leave blank if unknown</span></td>
    <td><input type="text" id="add-mobile" class="form-control form-control-sm" /><span class="boldgreen f10">leave blank if unknown<br/>Must include country code</span></td>
    <td class="titleedit">&nbsp;</td>
<?php
    } else {
        echo "SESSION_NON_EXIST";
    }

// Submit New Assistant
} elseif ($mode == 'submit_new_assistant') {
    $session_id = trim($_POST['session_id']);
    
    // Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {
        $userID = trim($_POST['add_id']);
        
        // Invalid Identifier, then abort
        if (!identifierValid($userID)) {
             error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
             exit;
        }      
       
        // Does User Exist
        if (!$db->users->doesUserExist($userID)) {
        
            // Insert Assistant(User)
            $inserted = $db->users->insertAssistant(
                                   $userID, 
                                   $_POST['add_fn'],
                                   $_POST['add_sn'],
                                   $_POST['add_email'],
                                   $_POST['add_mobile']
                                );

            // Attach assistant to session 
            if ($inserted) {
                $db->exams->attachAssistantsToSession($userID, $session_id);
            }
            
        } else {
            echo "ASSISTANT_ID_EXISTS";
        }
    } else {
        echo "SESSION_NON_EXIST";
    }
  
// Edit Assistant
} elseif ($mode == 'edit_assistant') {
    $session_id = trim($_POST['session_id']);
 
    // Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {
        $originalUserID = trim($_POST['org_user_id']);
        
        // Does User Exist
        if ($db->users->doesUserExist($originalUserID)) {
            $userDB = $db->users->getUser($originalUserID, 'user_id', true);
?>
    <td class="checkb">&nbsp;<input type="hidden" id="org_user_id" value="<?=$originalUserID; ?>"/></td>
    <td><input type="text" id="edit-id" class="tf4 form-control form-control-sm" value="<?=$originalUserID; ?>"/></td>
    <td><input type="text" id="edit-fn" class="tf3 form-control form-control-sm" value="<?=$userDB['forename']; ?>" /></td>
    <td><input type="text" id="edit-sn" class="form-control form-control-sm" value = "<?=$userDB['surname']; ?>" /></td>
    <td><input type="text" id="edit-email" class="form-control form-control-sm" size="26" value="<?=$userDB['email']; ?>"><br/><span class="boldgreen f10">leave blank if unknown</span></td>
    <td><input type="text" id="edit-mobile" class="form-control form-control-sm" value="<?=$userDB['contact_number']; ?>" /><br/><span class="boldgreen f10">leave blank if unknown<br/>Must include country code</span></td>
    <td class="titleedit">&nbsp;</td>
<?php
        } else {
            echo "ASSISTANT_NON_EXIST";
        }
    } else {
        echo "SESSION_NON_EXIST";
    }
    
// Update Assistant
} elseif ($mode == 'update_assistant') {
    $session_id = trim($_POST['session_id']);
    
    // Does Session Exist
    if ($db->exams->doesSessionExist($session_id)) {
        $originalUserID = trim($_POST['org_user_id']);
        
        // Does User Exist
        if ($db->users->doesUserExist($originalUserID)) {
            $userID = trim($_POST['edit_id']);
            
            // Invalid Identifier, then abort
            if (!identifierValid($userID)) {
                 error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
                 exit;
            }          
          
            if ($userID != $originalUserID && $db->users->doesUserExist($userID)) {
                echo "ASSISTANTID_DOES_EXIST";
            } else {

                $db->users->updateUser(
                    $originalUserID,
                    $userID,
                    $_POST['edit_sn'],
                    $_POST['edit_fn'],
                    $_POST['edit_email'],
                    $_POST['edit_mobile']
                );
            }
        }
    } else {
        echo "SESSION_NON_EXIST";
    }
}
