<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 // Use template library
 use \OMIS\Template as Template;
 use \OMIS\Database\CoreDB as CoreDB;
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }

 // POST variables
 $stationID = filter_input(INPUT_GET, 'station_id', FILTER_SANITIZE_NUMBER_INT);
 $sessionID = filter_input(INPUT_GET, 'session_id', FILTER_SANITIZE_NUMBER_INT);
 $showSession = filter_input(INPUT_GET, 'show_session', FILTER_SANITIZE_NUMBER_INT);

 // Defaults
 $stationExists = false;
 $examName = $sessionInfo = $formName = $stationNum = $deptName = "";
 $weightingList = $examiners = [];
 $noOfSlots = 1;

 // Mult examiner feature enabled for the current user role
 $multiExaminersAllowed = $db->features->enabled(
      'multi-examiners',
      $_SESSION['user_role']
 );

 $stationRecord = $db->stations->getStation($stationID);

 // Station record exists
 if (!empty($stationRecord)) {

    $sessionRecord = $db->sessions->getSession($sessionID);

    $examName = $sessionRecord['exam_name'];
    $sessionInfo = formatDate($sessionRecord['session_date']) . " " 
                   . $sessionRecord['session_description'];

    $deptName = $db->departments->getDepartmentName($sessionRecord['dept_id']);
    $formName = $stationRecord['form_name'];
    $stationNum = $stationRecord['station_number'];
    $stationExists = true;

    // Get num of slots
    $noOfSlots = $db->stations->getExaminersRequired($stationID);

    // Get current set of examiners assigned to the station
    $examinersResultset = $db->stationExaminers->getExaminersForStation($stationID, false);
    $examiners = CoreDB::into_array($examinersResultset);

    $assignedExaminers = [];
    $assignedObservers = [];

    // Split data into examiners and observers
    foreach($examiners as $record) {

      if ($record['scoring_weight'] == 0) {
         $assignedObservers[] = $record;
      } else {
         $assignedExaminers[] = $record; 
      }

    }

    // Get weighting list
    $weightingList = $db->weighting->getWeightingList();

    // Remove observer weighting
    if (($key = array_search(0, $weightingList)) !== false) {
      unset($weightingList[$key]);
    }

 }
   
 // Multiple examiner feature enabled and Station exists ?
 if (!$multiExaminersAllowed || !$stationExists) {
   
   $returnUrl = "manage.php?page=stations_osce&amp;session_id=" . 
                $sessionID . "&amp;show_session=";
   
   // Multi examiner feature not enabled
   if (!$multiExaminersAllowed) {
     $message = "You do not have access to the Multiple " .gettext('Examiner'). " feature. <br/>". 
                "Please contact the administrator for further assistance";  
   } else {
     $message = "The station selected or specified was not found in the system";
   }
    
    $templateData = [
        'message' => $message,
        'return_url' => $returnUrl,
        'content_wrapper' => true
    ];
    
    $errorTemplate = new OMIS\Template('error.html.twig');
    $errorTemplate->render($templateData);
    
    // Abort, only error message should be displayed
    exit;
 }
 
 // Get previous and next stations
 $uniqueStationIDs = [];
 $stationsDB = $db->stations->getStations([$sessionID]);
 while ($station = $db->fetch_row($stationsDB)) {
     $uniqueStationIDs[] = $station['station_id']; 
 }

 $arrayHelp = new \OMIS\Utilities\ArrayHelp(
      array_unique($uniqueStationIDs)
 );

 // Previous and next session urls
 list($previousStation, $nextStation) = $arrayHelp->previousAndNext($stationID);
 $previousUrl = empty($previousStation) ? "" : "manage.php?" 
 . http_build_query([
    'page' => "examiner_osce",
    'station_id' => $previousStation,
    'session_id' => $sessionID
 ]);

 $nextUrl = empty($nextStation) ? "" : "manage.php?" 
 . http_build_query([
    'page' => "examiner_osce",
    'station_id' => $nextStation,
    'session_id' => $sessionID
 ]);
  
 // Render template 
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render([
     
    'station_exists' => $stationExists,
    'station_number' => $stationNum, 
    'station_id' => $stationID,
    'session_id' => $sessionID,
    'session_info' => $sessionInfo,
    'show_session' => $showSession,
    'exam_name' => $examName,
    'dept_name' => $deptName,
    'form_name' => $formName,
    'num_of_slots' => $noOfSlots,
    'assigned_examiners' => $assignedExaminers,
    'assigned_observers' => $assignedObservers,
    'weighting_list' => $weightingList,
    //'grade_rule_applied' => ($sessionRecord['grade_rule'] > 1),
     
    /**
     * Jump to group records previous and next
     */
    'jumpRecords' => [
       'customEvents' => true,
       'boxWidth' => 'auto',
       'previousRecordLabel' => "Station",
       'nextRecordLabel' => "Station",
       'returnLabel' => "Return to Station List",
       'previousUrl' => $previousUrl,
       'nextUrl' => $nextUrl
    ]     
     
 ]);
 