<?php 
echo "data";
die();
//dpk
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Critical session check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
  
    return false;

}

if (!isset($config)) {
  
    $config = \OMIS\Config;
    
}

require_once __DIR__ .'/../extra/helper.php';


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__ . 'osce');
$twig = new Environment($loader);

 $template = new OMIS\Template("exam.upload.html.twig");
           $data = [
           	'data' => 'data'
           ]
            $template->render($data);

    //        echo $twig->render('exam.upload.html.twig', ['data' => 'John Doe', 
    // 'occupation' => 'gardener']);
 ?>