<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

  // This file is only included, not invoked directly. Ensure it only runs then...
  defined("_OMIS") or die("Restricted Access");
  define("STUDENT_TYPE", 0);
  define("BLANK_STUDENT_TYPE", 1);
  define("STUDENT_EXAM_NUMBER", "E");
  define("STUDENT_CANDIDATE_NUMBER", "C");
  
  // Critical Session Check
  $session = new OMIS\Session;
  $session->check();
 
  // Page Access Check / Can User Access this Section?
  if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
  }

  require_once __DIR__ ."/osce_functions.php";
  require_once __DIR__ ."/../extra/helper.php";

  // Default variables and 'get' params
  $examIDParam = filter_input(INPUT_GET, "exam_id", FILTER_SANITIZE_NUMBER_INT);
  $sessionIDParam = filter_input(INPUT_GET, "session_id", FILTER_SANITIZE_NUMBER_INT);
  $showIndividualSessionParam = filter_input(INPUT_GET, "show_session", FILTER_SANITIZE_NUMBER_INT);
  $savedParam = filter_input(INPUT_GET, "saved", FILTER_SANITIZE_NUMBER_INT);
  
  // Exam number feature enabled/disabled
  $examNumberFeature = $db->features->enabled("exam-number", $_SESSION["user_role"]);
  
  // Candidate number feature enabled/disabled
  $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]); 
  
  $sessionRecord = $db->exams->getSession($sessionIDParam);
  $sessionExists = (!is_null($sessionRecord));
  $currentDate = date("F j, Y, g:i a");
  $deptRecord = [];
  $groupRecords = [];
  $courseRecords = [];
  $studentsNotGrouped = [];
  $sessionHasResults = false;
  
  // If session exists
  if ($sessionExists) {
    $sessionDate = formatDate($sessionRecord["session_date"]);
    $examID = $sessionRecord["exam_id"];
    $deptID = $sessionRecord["dept_id"];
    $deptName = $db->departments->getDepartmentName($sessionRecord["dept_id"]); 
    $deptRecord = [
        "id" => $deptID, 
        "name" => $deptName
    ];
    
    // Get exam record with term
    $examRecord = $db->exams->getExam($examID);
    $examTermID = $examRecord["term_id"];
    
    // Groups linked to sessions from database
    $groups = $db->exams->getSessionGroups($sessionIDParam);
  
    // Gather groups data
    $blankStudentCount = 0;
    
    // Render groups
    while ($group = $db->fetch_row($groups)) {
       $students = [];
       $groupID = $group["group_id"];
        
       // Students
       $assessableStudents = $db->exams->getStudents([$groupID], null, true, $examTermID);
       
       // List of students (non-blank records)
       foreach ($assessableStudents as $student) {
           
            $hasResult = $db->results->doesStudentHaveResult($student['student_id'], $sessionIDParam);
            $sessionHasResults = ($sessionHasResults || $hasResult);
            
            $students[] = [
              "order" => $student['student_order'],
              "type" => STUDENT_TYPE,
              "id" => $student['student_id'],
              "student_exam_number" => $student['student_exam_number'],
              "student_candidate_number" => $student['candidate_number'],  
              "forenames" => $student['forename'],
              "surname" => $student['surname'],
              "result" => $hasResult
            ];
       }

        // Blank records
        $blankRecords = $db->exams->getBlankStudents($groupID);
        foreach ($blankRecords as $blankRecord) {
            $blankStudentCount++;
            $students[] = [
                 "order" =>  $blankRecord["student_order"],
                 "type" => BLANK_STUDENT_TYPE
            ];
        }

        orderArrayColumn($students, "order", "standard", "ASC");
        $groupRecords[$groupID] = [
          "group_name" => $group["group_name"],
          "students" => $students
        ];
    }
    
    // Retrieve list of courses for this academic term
    $courseRecords = $db->courses->getCoursesYearsModules($examRecord['term_id']);
    
    // Pre selected year module combination (from user session)    
    $currentYearModuleKey = implode("::", [
        $_SESSION['selected_year'], 
        $_SESSION['selected_module']
    ]);
           
    // Set selected module
    $firstModule = NULL;
    foreach ($courseRecords as &$modules) {
       foreach ($modules as &$module) {
          $eachModule = $module["year_id"] . "::" . $module["module_id"];
          if ($firstModule === NULL || $currentYearModuleKey == $eachModule) {
              $firstModule = $eachModule;
              $module["selected"] = true;
          } else {
              $module["selected"] = false;
          }
       }
    }
        
    // Get selected year and module
    if (!is_null($firstModule)) {
      
      list ($selectedYearID, $selectedModuleID) = explode("::", $firstModule);
    
      // Get students not in groups
      $studentsNotGroupedDB = $db->exams->getStudentsNotInGroups(
           $sessionIDParam,
           $selectedModuleID,
           $selectedYearID
      );
      
      // Students to add list
      while ($each = $db->fetch_row($studentsNotGroupedDB)) {
           $studentsNotGrouped[] = $each;
      }
    
   }
   
   // Filter indices for groups of students
   $filterIndex = buildFilterIndex($studentsNotGrouped);
   
   // Exam settings
   $examSettings = $db->examSettings->get($examID);
   
   // Get taken exam numbers not in session
   $studentExamNumbers = $db->exams->getExamNumbersNotSession(
           $examID, 
           $sessionIDParam
   );
   
  }
 
  // Get first and last exam sessions
  $sessionsDB = array_column(
     $db->exams->getExamSessions($examID, true),
     'session_id'
  );
 
  $arrayHelp = new \OMIS\Utilities\ArrayHelp($sessionsDB);
    
  // Previous and next session urls
  list($previousSession, $nextSession) = $arrayHelp->previousAndNext($sessionIDParam);
  $previousUrl = empty($previousSession) ? "" : "manage.php?" 
  . http_build_query([
      'page' => "groups_osce",
      'session_id' => $previousSession,
      'exam_id' => $examID
  ]);
 
  $nextUrl = empty($nextSession) ? "" : "manage.php?" 
  . http_build_query([
      'page' => "groups_osce",
      'session_id' => $nextSession,
      'exam_id' => $examID
  ]);
 
  // Groups OSCE template  
  $template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__)); 
  $data = [
     "examIDParam" => $examIDParam,
     "sessionID" => $sessionIDParam,
     "sessionExists" => $sessionExists,
     "sessionRecord" => $sessionRecord,
     "sessionDate" => $sessionDate,
     "showIndividualSession" => $showIndividualSessionParam,
     "currentDate" => $currentDate,
     "departmentRecord" => $deptRecord,
     "groupsSaved" => (!is_null($savedParam) && $savedParam == 1),
     "groupRecords" => $groupRecords,
     "courseRecords" => $courseRecords,
     "studentsNotGrouped" => $studentsNotGrouped,
     "studentExamNumbers" => $studentExamNumbers,
     "filterIndices" => $filterIndex,
     "blankStudentCount" => $blankStudentCount,
     "enableExamNumber" => ($examSettings["exam_student_id_type"] == STUDENT_EXAM_NUMBER && $examNumberFeature),
     "enableCandidateNumber" => ($examSettings["exam_student_id_type"] == STUDENT_CANDIDATE_NUMBER && $candidateNumberFeature),
     "sessionHasResults" => $sessionHasResults,
     
     /**
      * Jump to group records previous and next
      */
     "jumpRecords" => [
        "customEvents" => false,
        "previousRecordLabel" => "Day/Circuit",
        "nextRecordLabel" => "Day/Circuit",
        "returnLabel" => "Return to Day/Circuit List",
        "previousUrl" => $previousUrl,
        "nextUrl" => $nextUrl
     ]
     
  ];
 
  $template->render($data);
 