<?php
/**
 * Script intended to be called by Ajax HTTP requests. Receives an Exam ID as a parameter in the requests and,
 * if that ID is not in the list of exam warnings to ignore by the user in session, adds it to the list
 *
 * @author Sergio Franco <sergio.franco@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

require_once __DIR__ . '/../extra/essentials.php';

// Page Access Check / Can User Access this Section?
//if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
//    return false;
//}

require_once __DIR__ . '/osce_functions.php';


$examId = filter_input(INPUT_POST, 'examId', FILTER_SANITIZE_NUMBER_INT);

if (array_key_exists('warnings_ignored', $_SESSION) && !in_array($examId, $_SESSION['warnings_ignored']))
    $_SESSION['warnings_ignored'][] = $examId;
else
    $_SESSION['warnings_ignored'] = [$examId];

