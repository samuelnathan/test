<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Critical session check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
  
    return false;

}

if (!isset($config)) {
  
    $config = \OMIS\Config;
    
}

require_once __DIR__ .'/../extra/helper.php';

// Initialise variables
$count = 0;
$rowCount = 0;

list($department, $term, $filterReady, $accessible)
        = validateFilterOptions();

// Ger exams
if ($filterReady) {
  
  $exams = $db->exams->getList([$department], $term, 'exam_id', $accessible);
  $count = mysqli_num_rows($exams);
  
}

// Exams that had results attached / deletion prohibited
$examDeletionProhibited = [];
if(isset($_SESSION['exams_with_results_warning'])) {
  
  $examDeletionProhibited = $_SESSION['exams_with_results_warning'];
  unset($_SESSION['exams_with_results_warning']);
  
}

// Advanced settings feature enabled/disabled
$settingsFeature = $db->features->enabled('advanced-settings', $_SESSION['user_role']);

// Exam|Student matrix feature Enabled|Disabled
$matrixFeature = $db->features->enabled('exam-matrix', $_SESSION['user_role']);

// Multiple examiners feature enabled/disabled
$multiExaminersFeature = $db->features->enabled('multi-examiners', $_SESSION['user_role']);

// Icon titles
$chartTitle = "Icon to view results";
$editTitle = "Icon to edit record";
?>
<div class="topmain">

    <div class="row">
        <div class="col-lg-5 col-sm-6 col-12">
            <div class="card d-inline-block align-top w-100 h-100">
                <div class="card-header">
                <div class="card-title h6 mb-0">
                    <div class="row no-gutters align-items-center">
                        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
                        <span class="card-title-text">
                        Manage <?=gettext('OSCE')?>
                        </span>
                    </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-text">
                    <div class="card-subtitle text-muted">
                    Manage <?=gettext('exams')?> by <?=gettext('department')?> &amp; <?=gettext('term')?>
                    </div>

                    <div>
                    To view analysis for <?=gettext('an OSCE')?> click the bar chart 
                    <img class="chart-icon" title="<?=$chartTitle?>" alt="<?=lcfirst($chartTitle)?>" src="assets/images/res.svg"/> icon
                    </div>
                    <div>
                    To edit <?=gettext('osce')?> record data click the pencil
                    <img class="editb" title="<?=$editTitle?>" alt="<?=lcfirst($editTitle)?>" src="assets/images/edit.svg"/> icon
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-auto">
            <?php

            // Render filtering options
            renderFilterOptions($department, $term, true, $accessible);

            ?>
        </div>
    </div>


<div class="topmain-clear"></div>
</div>
<div class="contentdiv">
<form name="exam_list_form" id="exam_list_form" method="post" action="osce/exams_redirect.php" onsubmit="return(ValidateForm(this));">
<table id="exam_table" class="table-border">
    <tr id="title-row" class="title-row">
        <td class="all-checkbox-td">
        <input type="checkbox" id="check-all" <?php 
        if($count == 0){ 
            ?>disabled="disabled"<?php 
        } ?>/>
        </td>
        <th>Name</th>
        <th>Description</th>
        <th>Days/Circuits</th>
        <th>Rules / Settings</th>
        <th id="options-th">&nbsp;</th>
        <td class="title-edit-top">&nbsp;</td>
    </tr>
<?php
if ($filterReady) {
  
    if ($count > 0) {
      
        // Render exam records
        while ($exam = $db->fetch_row($exams)) {
          
            $rowCount++;
            $hasCircuitColour = false;
            $examID = $exam['exam_id'];
            
            $hasResult = $db->results->doesExamHaveResults($examID) || 
                         $db->selfAssessments->existsByExam($examID);
            
            $deleteResultsWarning = in_array($examID, $examDeletionProhibited);

            // Gather session data for this exam
            $sessionsDB = $db->exams->getExamSessions($examID);
            $sessions = [];

            // Gather session data
            while ($eachSession = $db->fetch_row($sessionsDB)) {
              
                $sessionID = $eachSession['session_id'];
                $eachSession['date'] = formatDate($eachSession['session_date']);
                $eachSession['colour_hex'] = $eachSession['circuit_colour'];
                $eachSession['colour_name'] = $db->exams->getColourName($eachSession['circuit_colour']);
                
                if (strlen($eachSession['colour_hex']) > 0) {
                  
                   $hasCircuitColour = true; 
                   
                }
                
                $sessions[$sessionID] = $eachSession;
                
            }

            // Is exam complete
            $examComplete = $db->exams->examComplete($examID);

            $template = new OMIS\Template("exam.record.html.twig");
            $data = [
                'mode' => 'view',
                'row_id' => $rowCount,
                'exam_data' => $exam,
                'session_data' => $sessions,
                'exam_has_result' => $hasResult,
                'exam_has_circuit_colour' => $hasCircuitColour,
                'delete_results_warning' => $deleteResultsWarning,
                'show_advanced_settings' => $settingsFeature,
                'show_matrix_option' => ($matrixFeature && $examComplete),
                'show_multiexaminers_settings' => $multiExaminersFeature,
                'show_monitor_option' => $config->monitorNotification['enabled'],
                'show_submit_and_next' => !$config->exam_defaults['multiple_candidate'],
                'grade' => $db->gradeRules->getById($exam['grade_rule'])
            ];
            $template->render($data);
            
        }

// Output base row
if ($rowCount > 0) {
  
?>
<tr id="base-tr">
<td colspan="7" class="base-button-options" id="base-button-td">
<div class="fl">
    
    <select name="todo" id="todo" disabled="disabled" class="custom-select custom-select-sm">
        <option value="delete">delete from system</option>
    </select>
    <input type="submit" value="go" class="btn btn-outline-default btn-sm" name="go_exam" id="go-exam" disabled="disabled"/>
</div>
<div class="fr" id="add-div">
    <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
    <input type="hidden" value="<?=$rowCount?>" name="count" id="count"/>
</div>
</td>
</tr>
<?php

 }
 
} else {
  
?>
<tr id="first_exam_row">
<td colspan="7" class="mess">
    <div id="exam_row_div" class="tablestatus2 tal">
        No <?=gettext('examination')?> records were found based on the above filter criteria.
        To add a new<br/>record click the 'add' button below
    </div>
</td>
</tr>
<tr id="base-tr">
<td colspan="7" class="base-button-options" id="base-button-td">
<div class="fr" id="add-div">
    <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
</div>
</td>
</tr>
<?php

}
} else {
  
    ?> 
    <tr id="first_exam_row">
    <td colspan="7" class="mess2">
      <div id="exam_row_div" class="tablestatus1">
          Please select a <?=gettext('department')?> and a <?=gettext('term')?> from the filter options above
      </div>
    </td>
    </tr>
    <?php
    
}
?> 
</table>
</form>
</div>
