<?php

define('_OMIS', 1);

defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

require_once __DIR__ . '/../extra/essentials.php';
require_once __DIR__ . '/osce_functions.php';

if (!isset($config)) {
    $config = \OMIS\Config;
}

if (!$config->monitorNotification['enabled']) {
    http_response_code(404);
    return;
}

$examID = $_GET['examID'];

$exam = $db->exams->getExam($examID);
$examSettings = $db->examSettings->get($examID);
$sessions = $db->exams->getExamSessions($examID, true);

$studentIdKey = 'student_id';

if ($examSettings['exam_student_id_type'] === EXAM_NUMBER_TYPE)
    $studentIdKey = 'student_exam_number';
else if ($examSettings['exam_student_id_type'] === CANDIDATE_NUMBER_TYPE)
    $studentIdKey = 'candidate_number';

$template = new OMIS\Template('sessions_monitor.html.twig');
$template->render([
    'wsEndpoint' => $config->monitorNotification['wsEndpoint'],
    'examSessions' => json_encode($sessions),
    'exam' => $exam,
    'studentIdKey' => $studentIdKey
]);