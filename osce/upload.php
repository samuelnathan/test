<?php 

//dpk

/**

 * @author David Cunningham <david.cunningham@qpercom.ie>

 * For Qpercom Ltd

 * @copyright Copyright (c) 2018, Qpercom Limited

 */

// Critical session check

$session = new OMIS\Session;

$session->check();



// Page Access Check / Can User Access this Section?

if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

  

    return false;



}



if (!isset($config)) {

  

    $config = \OMIS\Config;

    

}



require_once __DIR__ .'/../extra/helper.php';



//term_id and department_id




 $term_id = filter_input(INPUT_GET, 'term_id');

 $dept_id = filter_input(INPUT_GET, 'dept_id');

 $exam_id = filter_input(INPUT_GET, 'exam_id');



 $exam_data = $db->exams->getAssessment($term_id,$dept_id);



 if(isset($_POST['submit'])){

 $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);



 	if($ext == "mp4" OR $ext == "txt" OR $ext == "pdf" OR $ext == "docx" OR $ext == "doc"){



 		 // $file_name = $_FILES['upload_file']['name'];

		 $Assessment = filter_input(INPUT_POST, 'Assessment');

		 $session = filter_input(INPUT_POST, 'session');

		 $group = filter_input(INPUT_POST, 'group');

		 $student_id = filter_input(INPUT_POST, 'student_id');

		 $scoresheet = filter_input(INPUT_POST, 'scoresheet');

		 $retention = filter_input(INPUT_POST, 'retention');

		 $file_name = $scoresheet."_".$student_id.".".$ext;

		 $multimedia_link = 'media/upload_media/'.$file_name;

		 $updated_by = $_SESSION['userinputid'];

		 // print_r($file_name); die(); 

		 // if($um){

		 	

// print_r($_FILES); die();  

if(!move_uploaded_file($_FILES['upload_file']['tmp_name'], 'media/upload_media/'.$file_name)){

	echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>

  <strong> Media not Uploaded!</stong>

  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>

    <span aria-hidden='true'>&times;</span>

  </button>

</div>";



}else{
	$multimedia = $db->exams->upload_multimedia($multimedia_link,$retention,$updated_by,$scoresheet,$student_id);

	$um = $db->exams->uploadMedia($Assessment,$session,$group,$student_id,$scoresheet,$file_name,$retention);
	// $db->exams->upload_multimedia($multimedia_link,$retention);

	echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>

  <strong>Media Uploaded Successfully!</strong>

  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>

    <span aria-hidden='true'>&times;</span>

  </button>

</div>";

}

// 		 }else{

// 		 	echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>

//   <strong>File not Submitted Successfully!</strong>

//   <button type='button' class='close' data-dismiss='alert' aria-label='Close'>

//     <span aria-hidden='true'>&times;</span>

//   </button>

// </div>";

// 		 }





 // echo $student_id = filter_input(INPUT_POST, 'student_id');



 	}else{

 		echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>

  <strong>Media Not Uploaded!</strong> Please Upload a mp4/txt/pdf/doc/docx file

  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>

    <span aria-hidden='true'>&times;</span>

  </button>

</div>";

 	}

 }



?>

<form method="POST" id="myForm" name="myForm" onsubmit='return upload_image()' enctype="multipart/form-data">

	<div class="card d-inline-block align-top mr-2 mb-1 options">

		<div class="card-header">

		    <div class="card-title h6 mb-0">

		      <div class="row no-gutters align-items-center">

		        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">

		        <span class="card-title-text">Upload Media</span>

		      </div>

		    </div>

        </div>

        <div class="card-body pb-0">

            <div class="card-text">

                <div class="f12">Filter list using the follow criteria: </div>

                <div class="row acesss">

                 <div class="Assessment-assess"> 

		            <label><b>Select Assessment</b></label>

		              <select name="Assessment" id="Assessment" required>

			           <option value="">Select Assessment...</option>

			           <?php

		                 while($exam_name = $db->fetch_row($exam_data)){

			            ?>

					    <option value='<?php echo $exam_name['exam_id']; ?>' <?php if($exam_name['exam_id'] == $exam_id){ echo "selected"; } ?>><?php echo $exam_name['exam_name'] ?></option>

			           <?php

		            }

		              ?>

		              </select>

                 </div>

                 <div class="Assessment-assess">

                    <label><b>Select Session</b></label>

				    <select name="session" id="session" required>

					<?php if($exam_id != ""){ ?>



						<option value="">Select Session...</option>

				    <?php



					$session_data = $db->exams->getExamData($exam_id);

					while($session = $db->fetch_row($session_data)){

						?>

								<option value='<?php echo $session['session_id']; ?>'><?php echo $session['session_description'] ?></option>

						<?php

						}

					} ?>

				   </select>

                 </div>  </div>

                 <div class="Assessment-assess">

                   <label><b>Select group</b></label>

                   <select name="group" id="group" required>

                   	<option value="">Select Group...</option>

                   </select>

                 </div>

            </div>

        </div>

	</div>

<br>

<br>
<div class="contentdiv">
<table border="1px" width="54.5%" class="table-border">
    <div id="radioerror"></div>
    		<tr class="title-row candidate">

    	     <th>Candidate ID</th>

    	     <th>Candidate Name</th>

    	     <th>Select</th>

    	    </tr>
    	<tbody id="student"></tbody>
</table>
</div>
<br><br>

<div class="card d-inline-block align-top mr-2 mb-1 options">

<div class="card-body pb-0">

<div class="card-text">

<div class="row" style="margin: 0">

<div class="Assessment-assess">

<label>Choose source file:(supported format: mp4,pdf,txt,doc,docx)</label><input type="file" name="upload_file" id="upload_file" 
style="font-size: 12px;width: 187px;" required>

</div>
<br>
<div id="pleaseWaitDialog" style="margin-left: -10px;
    margin-right: 10px;"></div>
<br>
<div class="Assessment-assess period-rentention">

<label>Retention period: </label><input type="number" name="retention" id="retention" required ><p>Days</p>

</div>


</div>
<div class="Assessment-assess">

<label>Select destination scoresheet:</label> 

<select name="scoresheet" id="scoresheet" required>

</select>

</div>

<br>

<div class="source-file">

<button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>

</div>

<br>

</div>

</div>

</div>

</form>

