<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

define('_OMIS', 1);

 $session_id = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_STRING);
 include __DIR__ . '/../extra/essentials.php';
?>