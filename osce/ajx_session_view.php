<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 define('_OMIS', 1);
 include_once __DIR__ . '/../vendor/autoload.php';

 if (filter_has_var(INPUT_POST, 'isfor')) {
   
    require_once __DIR__ . '/../extra/essentials.php';
    
    // Can user access this page
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }
    
    require_once __DIR__ . '/osce_functions.php';
    
 } else {
   
    require_once __DIR__ . '/../extra/noaccess.php';
    
 }

 $mode = trim(filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED));
 
 // Add new session
 if ($mode == 'add' && filter_has_var(INPUT_POST, 'exam')) {

    $examID = filter_input(INPUT_POST, 'exam', FILTER_VALIDATE_INT);
 
    if ((!empty($examID)) && ($db->exams->doesExamExist($examID))) {

        $last_src = $db->sessions->getLastSessionInsertedVariables($examID);
        list($exams, $sessions) = buildSessionsDropdown($examID);
 
        // Use the supplied date if there is one; otherwise use the current date.
        $dateString = (is_null($last_src)) ? date("d/m/Y") : formatDate($last_src['session_date']);
 ?>
    <td class="checkb">&nbsp;</td>
    <td id="session-description">
        <div class="cb fl">
            <input type="text" id="thedate" title="Day" maxlength="10" value="<?=$dateString?>"/>
        </div>
        <div class="cb fl">
            <input type="text" id="add_desc" title="Circuit Description" value="e.g. Morning, Afternoon" class="color-grey"/>
        </div>
 <?php

        // Render colour section (HTML) template 
        $template = new OMIS\Template("ajx_sessions.colour.html.twig");
        $template->render(['color' => '']);
       
 ?>
    </td>
    <td class="align-right">
        <div id="start-div">
            <label for="start-time">Start Time:</label>
            <input id="start-time" type="text" size="5" value="08:00" class="form-control-sm w-auto"/>
        </div>
        <div id="end-div">
            <label for="end-time">End Time:</label>
            <input id="end-time" type="text" size="5" value="09:00" class="form-control-sm w-auto"/>
        </div>
    </td>
    <td>
        <div class="bold tal">
            <?php outputSessionsDropdown($exams, $sessions, 'stations'); ?>
        </div>
    </td>
    <td>
        <div class="bold tal">
            <?php outputSessionsDropdown($exams, $sessions, 'groups'); ?>
        </div>
    </td>
    <td>
        <div class="bold tal">
            <?php outputSessionsDropdown($exams, $sessions, 'assistants'); ?>
        </div>
    </td>
    <td class="setheight">
        <?php sessionSettingsTemplate('add', $last_src); ?>
    </td>
    <td class="options-td">&nbsp;</td>
    <td class="titleedit">&nbsp;</td>
        <?php
    } else {
        echo 'EXAM_NON_EXIST';
    }
    
  } elseif ($mode == 'edit') {
    
    $sessionID = filter_input(INPUT_POST, 'session_id', FILTER_VALIDATE_INT);
    $sessionDB = $db->exams->getSession($sessionID);
    $sessionColour = $sessionDB['circuit_colour'];
 
    if (!is_null($sessionDB)) {

        $datestr = explode('-', $sessionDB['session_date']);
        $formatted_date = implode('/', array_reverse($datestr));
        ?>
        <td class="checkb">&nbsp;<input type="hidden" id="edit_id" value="<?=$sessionID?>"/></td>
        <td>
        <div class="fl cr">
           <input id="thedate" title="Day" name="thedate" type="text" maxlength="10" value="<?=$formatted_date?>"/>
        </div>
        <div class="tddateva2">
           <input type="text" title="Circuit Description" id="edit_desc" value="<?=$sessionDB['session_description']?>"/>
        </div>
       <?php
         
        // Render colour section (HTML) template
        $template = new OMIS\Template("ajx_sessions.colour.html.twig");
        $template->render([
            'color' => '#' . $sessionColour
        ]);
   
     ?></td>
        
  <?php 
       
    $startTime = $sessionDB['start_time'];
    $endTime = $sessionDB['end_time'];
    
    // If either of the Start or End time fields are null or invalid then apply defaults
    if (is_null($startTime) || is_null($endTime) || !strtotime($startTime) || !strtotime($endTime)) {

        $startTime = "08:00";
        $endTime = "09:00";

    } else {

        $startTime = date("H:i", strtotime($sessionDB['start_time']));
        $endTime = date("H:i", strtotime($sessionDB['end_time']));   

    }
       
    ?>
    <td>
     <div id="start-div">
     <label for="start-time">Start Time</label>
     <input id="start-time" type="text" size="5" value="<?=$startTime; ?>"/>
     </div>
     <div id="end-div">
      <label for="end-time">End Time</label>
      <input id="end-time" type="text" size="5" value="<?=$endTime; ?>"/>
     </div>
    </td>
    <td class="cell-spacing tal opacedit">
         <div><?php stationsDisplay($sessionID); ?>
            <br/>
            <a href="manage.php?page=stations_osce&amp;session_id=<?=$sessionDB['session_id']?>" class="bold">Manage Stations</a><br/><br/>
         </div>
      </td>
    <td class="cell-spacing tal opacedit">
        <div><?php groupsDisplay($sessionID); ?>
        <br/>
        <a href="manage.php?page=groups_osce&amp;session_id=<?=$sessionDB['session_id']?>" class="bold">Manage <?=gettext('Groups')?></a><br/><br/>
        </div>
    </td>
    <td class="cell-spacing tal opacedit">
        <div><?php assistantsDisplay($sessionID); ?>
        <br/>
        <a href="manage.php?page=assistants_osce&amp;session_id=<?=$sessionDB['session_id']?>" class="bold">Manage Assistants</a><br/><br/></div>
    </td>
    <td class="setheight tal"><?php sessionSettingsTemplate('edit', $sessionDB); ?></td>
    <td class="options-td">&nbsp;</td>
    <td class="titleedit">&nbsp;</td>
 <?php
    } else {

        echo 'SESSION_NON_EXIST';

    }
 }
 