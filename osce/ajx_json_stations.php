<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 define("_OMIS", 1);
 ob_start();

 // JSON Helper functions.
 use \OMIS\Utilities\JSON as JSON;
 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;

 include_once __DIR__ . '/../vendor/autoload.php';

 $mode = filter_input(INPUT_POST, "mode", FILTER_SANITIZE_STRIPPED);

 if (!is_null($mode)) {

     require_once __DIR__ . "/../extra/essentials.php";

     // Do a session check to see if the user's logged in or not.
     if (!isset($session)) {

         $session = new OMIS\Session();
         $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

     }

     if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

         http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
         exit();

     }

     require_once __DIR__ . '/../extra/helper.php';
     $sterm = selectedTerm();

 } else {

     http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
     exit();

 }

 $jsonData = [];

 switch ($mode) {

     // Add station record
     case "submit":

         $sessionID = iS($_POST, 'session_id');
         $sessionRecord = $db->sessions->getSession($sessionID);

         // Stick option selected
         $_SESSION['stick_station_option'] = $_POST['selectiontype'];

         // Does session exist
         if ($db->sessions->existsById($sessionID)) {

             // Rest Station entry only
             if($_POST['selectiontype'] == 'new_station' && $_POST['add_rest'] == 1) {

                 $db->stations->addOne(
                     $sessionID, NULL,
                     [
                         "pass" => "",
                         "number" => $_POST['add_nr'],
                         "tag" => "",
                         "time" => "",
                         "alarm1" => "",
                         "alarm2" => "",
                         "popup" => 0,
                         "rest" => 1,
                         "examiners" => 0,
                         "weighting" => "",
                         "exclude_from_grading" => 0
                     ]
                 );

                 $db->stations->reOrderStations($sessionID);
                 $db->blankStudents->deleteBlanksSubmittedBySession($sessionID);

             }

             // Clone Sessions Stations Type
             else if ($_POST['selectiontype'] == 'clone_stations_exam' && isset($_POST['clone_session_id'])) {

                 // Check if session to be cloned exists
                 if ($db->sessions->existsById($_POST['clone_session_id'])) {

                     $db->stations->cloneStations(
                         $sessionID,
                         $_POST['clone_session_id'],
                         $sessionRecord['dept_id'],
                         $sterm
                     );

                 }

             } else {

                 // New Station
                 if ($_POST['selectiontype'] == 'new_station') {

                     // Form name param
                     $formName = filter_input(INPUT_POST, 'add_stat');

                     // Form name already in use in session
                     if ($db->forms->nameTakenSession($formName, $sessionID)) {

                         http_response_code(HttpStatusCode::HTTP_GONE);
                         exit();

                     }

                     // Form name already in use
                     list($formUsed, $usedID) = array_values(
                         $db->forms->nameTaken($formName, $sessionRecord['dept_id'], $sterm)
                     );

                     if ($formUsed) {

                         http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
                         echo JSON::encode(["form_id" => $usedID]);
                         exit();

                     }

                     // Insert station to database
                     list($success, $formID) = $db->forms->insertForm(
                         $formName,
                         $_SESSION['user_identifier'],
                         $sessionRecord['dept_id'],
                         $sterm
                     );

                     // Using Old Station
                 } else {

                     // Station ID
                     $formID = trim($_POST['add_stat']);

                     // From Previous Session
                     if ($_POST['selectiontype'] == 'previous_exam_station') {
                         // Add Station to current term
                         $db->forms->attachFormsToDeptTerm([$formID], $sessionRecord['dept_id'], $sterm);
                     }

                     $success = true;
                 }

                 // Stick selected options for next station
                 $_SESSION['station_time_limit'] = filter_input(INPUT_POST, 'add_time', FILTER_SANITIZE_STRING);
                 $_SESSION['station_alarm_one'] = filter_input(INPUT_POST, 'add_alarm1', FILTER_SANITIZE_STRING);
                 $_SESSION['station_alarm_two'] = filter_input(INPUT_POST, 'add_alarm2', FILTER_SANITIZE_STRING);
                 $_SESSION['default_popup_value'] = filter_input(INPUT_POST, 'add_pop', FILTER_SANITIZE_STRING);
                 $_SESSION['station_type'] = filter_input(INPUT_POST, 'station_type', FILTER_SANITIZE_STRING);
                 $_SESSION['station_pass_value'] = filter_input(INPUT_POST, 'add_pass', FILTER_SANITIZE_STRING);
                 $_SESSION['station_weighting'] = filter_input(INPUT_POST, 'weighting', FILTER_SANITIZE_STRING);

                 $stationExists = $db->forms->doesFormExist($formID);
                 $sessionStationCount = $db->sessions->sessionFormCount($formID, $sessionID);

                 // Create the Station Item
                 if ($success && $stationExists && ($sessionStationCount == 0)) {

                     /**
                      * Get default required number of examiners for the exam
                      * and set as default for this new station
                      */
                     $examRecord = $db->exams->getExam($sessionRecord['exam_id']);
                     $requiredNumExaminers = $examRecord['default_examiners_station'];

                     $tagFiltered = preg_replace(
                         '/[^a-zA-Z0-9\/#-]/',
                         '',
                         $_POST['add_tag']
                     );
// print_r($_POST['station_type']);
                     $db->stations->addOne(
                         $sessionID,
                         $formID,
                         [
                             "pass" => $_POST['add_pass'],
                             "number" => $_POST['add_nr'],
                             "tag" => $tagFiltered,
                             "time" => $_POST['add_time'],
                             "alarm1" => $_POST['add_alarm1'],
                             "alarm2" => $_POST['add_alarm2'],
                             "popup" => $_POST['add_pop'],
                             "station_type" => $_POST['station_type'],
                             "rest" => $_POST['add_rest'],
                             "examiners" => $requiredNumExaminers,
                             "weighting" => $_POST['weighting'],
                             "exclude_from_grading" => $_POST['exclude_grade']
                         ]
                     );

                     $db->stations->reOrderStations($sessionID);
                     $db->blankStudents->deleteBlanksSubmittedBySession($sessionID);

                     // Update related outcomes
                     (new \OMIS\Outcomes\OutcomesService($db))->setUpToDateBySession($sessionID,false);

                 } else {

                     http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);

                 }

             }
         } else {

             http_response_code(HttpStatusCode::HTTP_NOT_FOUND);

         }

     break;

 // Bad Request
 default:

     http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
     break;

 }

 header("Content-type: application/json");
 try {

     $json = JSON::encode($jsonData);

 } catch (Exception $ex) {

     error_log(__FILE__ . ": JSON Encode failed. Data follows:");
     print_r_log($jsonData);
             http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
     exit();

 }

 // Data to be sent
 echo $json;
