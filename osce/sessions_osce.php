<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess($page)) {
 
     return false;
 
 }
 
 require_once __DIR__ . '/../extra/helper.php';
 require_once __DIR__ . '/osce_functions.php';
 
 // Exam ID
 $examID = filter_input(INPUT_GET, 'exam_id', FILTER_SANITIZE_NUMBER_INT);
 
 $exists = false;
 $rowCount = 0;
 $count = 0;
 
 // Sessions that had results attached / deletion prohibited
 $sessionDeletionProhibited = [];
 if(isset($_SESSION['sessions_with_results_warning'])) {

    $sessionDeletionProhibited = $_SESSION['sessions_with_results_warning'];
    unset($_SESSION['sessions_with_results_warning']);

 }
 
 ?>
 <div class="topmain">

 <div class="row">
 <div class="col">
<div class="card d-inline-block align-top w-100 h-100">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Manage Days/Circuits
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      <?php
      ?>Manage day/circuits, stations &amp; <?=gettext('students')?> for <?=gettext('OSCE')?>:<?php
 
   // Display Exam Details
   if (!is_null($examID)) {
     $examDB = $db->exams->getExam($examID);
     if (!is_null($examDB)) {
         $exists = true;
         $sessionsDB = $db->exams->getExamSessions($examID);
         $count = mysqli_num_rows($sessionsDB);
         ?>
         <br/><br/><span class="bold">Name: </span><?=$examDB['exam_name']?><br/>
         <span class="bold"><?=gettext('Department')?>: </span>
         <?=$db->departments->getDepartmentName($examDB['dept_id']) ?><br/>
        <?php
     }
   }
   
   // Icon titles
   $chartTitle = "Icon to view results";
   $editTitle = "Icon to edit record";
   
   ?><br/><div>To view analysis for a day/circuit click the bar chart <img class="chart-icon2" title="<?=$chartTitle?>" 
     alt="<?=lcfirst($chartTitle)?>" src="assets/images/res.svg"/> icon</div>
     <div>To edit a day/circuit click the pencil <img class="editb" title="<?=$editTitle?>" 
     alt="<?=lcfirst($editTitle)?>" src="assets/images/edit.svg"/> icon</div>
   <?php
   
   // What's chosen from the filter list
   $showSessionParam = filter_input(INPUT_GET, 'show_session');
   $filterParam = filter_input(INPUT_GET, 'filter');
   $showSession = (!is_null($showSessionParam) && !empty($showSessionParam));
   
   $all = (!$showSession && !in_array($filterParam, ['0','1','2','3']));
   $accessible = (!$showSession && $filterParam == '1');
   $inAccessible = (!$showSession && $filterParam == '0');
   $sessionsWithResults = (!$showSession && $filterParam == '2');
   $sessionsWithoutResults = (!$showSession && $filterParam == '3');
   
   $allAttr = ($all) ? ' selected="selected"' : '';
   $accessibleAttr = ($accessible) ? ' selected="selected"' : '';
   $inAccessibleAttr = ($inAccessible) ? ' selected="selected"' : '';
   $resultsAttr = ($sessionsWithResults) ? ' selected="selected"' : '';
   $noResultsAttr = ($sessionsWithoutResults) ? ' selected="selected"' : '';
   
 ?>
    </div>   
  </div> 
</div>
</div> 
<div class="col">
<div id="return-exam-list" class="divoptions"><input class="btn btn-sm btn-dark" type="button" id="return" value="Return to <?=gettext('OSCE List')?>"/></div>
     <div id="filter-sessions-options" class="divoptions">            
      <label for="filter-sessions" class="filter-label">Filter List</label>
      <select id="filter-sessions" class="custom-select custom-select-sm w-auto">
          <option value="">--</option>
          <option value="all"<?=$allAttr?>>All</option>
          <option value="1"<?=$accessibleAttr?>>Accessible</option>
          <option value="0"<?=$inAccessibleAttr?>>Inaccessible</option>
          <option value="2"<?=$resultsAttr?>>With results</option>
          <option value="3"<?=$noResultsAttr?>>Without results</option>
      </select>
     </div>
</div>
 </div>

 </div>
 <div class="contentdiv">
 <?php
 
 //Render table of sessions
 if ($exists) {

     $disabledAttribute = ($count == 0) ? ' disabled="disabled"' : '';
     
     ?><form name="sessions_list_form" id = "sessions_list_form" method="post" 
             action="osce/exams_redirect.php" onsubmit="return(ValidateForm(this));">
        <table id="sessions_table" class="table-border">
          <tr id="title-row" class="title-row">
            <td class="all-checkbox-td">
              <input type="checkbox"  id="check-all"<?=$disabledAttribute?>/>
            </td>
            <th>Day/Circuit</th>
            <th>Time</th>
            <th>Stations</th>
            <th><?=gettext('Groups')?></th>
            <th>Assistants</th>
            <th>Settings</th>
            <th id="options-th">&nbsp;</th>
            <td class="title-edit-top">&nbsp;</td>
          </tr>
     <?php
     
   // Loop through sessions
   if ($sessionsDB && $count > 0) {

     while ($eachSession = $db->fetch_row($sessionsDB)) {
          
        $sessionID = $eachSession['session_id'];     
        $hasResult = $db->results->doSessionsHaveResult([$sessionID], 'num');
        
        // Show Session yes/no
        $showAccessible = ($accessible && $eachSession['session_published'] == 1);
        $showInAccessible = ($inAccessible && $eachSession['session_published'] == 0);
        $individualSession = ($showSessionParam == $eachSession['session_id']);
        $recordWithResults = ($sessionsWithResults && $hasResult);
        $recordWithoutResults = ($sessionsWithoutResults && !$hasResult);
        $showSessionRecord = ($all || $individualSession || $showAccessible || 
                        $showInAccessible || $recordWithResults || $recordWithoutResults);
 			
        // Show this session check 
        if($showSessionRecord) {
                 
           $rowCount++;
                       
           // Url Parameters
           $urlParameters = "&amp;session_id=" . $sessionID .
                            "&amp;exam_id=" . $examID .
                            "&amp;show_session=" . iS($_GET, 'show_session');
           
           // Get currently set Session/Circuit colour              
           $currentSessionColour = $eachSession['circuit_colour'];          
           $colourLabel = $db->exams->getColourName($currentSessionColour);
               
     ?>
        <tr id="sessions_row_<?=$rowCount?>" class="datarow">
          <td class="checkb"><?php hashIdentifier($sessionID)?>
            <input type="checkbox" class="session-records" name="sessions_check[]" id="sessions-check-<?=$rowCount?>" value="<?=$sessionID?>"/>
          </td>
          <td class="f12">
            <div class="tddateva"><?=formatDate($eachSession['session_date'])?></div>
            <div class="fl cb wrap"><?=ucfirst($eachSession['session_description'])?></div>
           <?php
 					
           // Delete results warning
           if (in_array($sessionID, $sessionDeletionProhibited)) {

                ?><div class="cannot-delete-session word-wrap">Cannot delete day/circuit results attached</div><?php

           }
           
           // If circuit colours feature enabled and colour selected then display / Also Validate hexidecimal value
           if (ctype_xdigit($currentSessionColour)) {
          
              ?><div class="color-container">
                 <div class="color-selected" style="background-color: #<?=$currentSessionColour?>"></div>
                 <div class="color-label"><?=$colourLabel?></div>
                 <div class="clearv2"></div>
 	      </div><?php
              
           }
 				
 	  ?></td>
                 
     <td><?php
           
        echo date("H:i", strtotime($eachSession['start_time']));
        echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;:<br/>";
        echo date("H:i", strtotime($eachSession['end_time']));
           
       ?></td>
             
       <td class="cell-spacing"><?php
         $areStations = stationsDisplay($sessionID);
         ?><br/><a href="manage.php?page=stations_osce<?=$urlParameters?>" class="linkin">Manage Stations</a><br/><br/>
       </td>
       
       <td class="cell-spacing"><?php
         $areGroups = groupsDisplay($sessionID);
         ?><br/><a href="manage.php?page=groups_osce<?=$urlParameters?>" class="linkin">Manage <?=gettext('Groups')?></a><br/><br/>
       </td>
       
       <td class="cell-spacing"><?php
         assistantsDisplay($sessionID);
         ?><br/><a href="manage.php?page=assistants_osce<?=$urlParameters?>" class="linkin">Manage Assistants</a><br/><br/>
       </td>  
        <td class="setheight"><?php 
          sessionSettingsTemplate('view', $eachSession);
        ?>
        </td>
        <td class="options-td"><?php
         
         /**
          * Print Forms Option Enabled|Disabled
          */
         if ($db->features->enabled('pdf-forms', $_SESSION['user_role']) && $areStations && $areGroups) {
             ?><a class="session-print-link" href="manage.php?page=pdfstations&amp;session=<?=$sessionID?>" target="_blank">
                <img class="printbu" src="assets/images/print.svg" border="0" alt="Back Up forms" title="Print backup <?=gettext('forms')?> for day/circuit"/>
             </a><?php
         }
 
         /* 
          * Results Icon Option 
          */
         $hideResultsIcon = ($hasResult) ? "" : " invisible";
         ?><a id="session-results-link-<?=$sessionID?>" class="<?=$hideResultsIcon?>" href="manage.php?page=out_analysis&amp;s=<?=$sessionID?>" target="_sessres<?=$sessionID?>">
             <img class="sessr-icon" src="assets/images/res.svg" border="0" alt="Results" title="View day/circuit results"/>
         </a><?php
 
       ?></td>
         <td class="titleedit"><?php editButton('edit_'.$rowCount, 'Edit', 'Edit')?></td>
     </tr>
     <?php
       }
      }
    }
   
   // Render bottom table rows (options/button controls)
   $template = new OMIS\Template("sessions_osce.bottom.row.html.twig"); 
   $data = [
      'colspan' => 8,
      'exam_id' => $examID,
      'row_count' => $rowCount,
      'show_session' => iS($_GET, 'show_session')
   ];
   
   $template->render($data);
 
  ?></table>
 </form>
 <?php
  } else {
   ?><div class="errordiv">The <?=gettext('exam')?> selected or specified was not found in the system<br/>
     <a href="manage.php?page=manage_osce">click here to return to <?=gettext('OSCE')?> list</a></div>
    <?php 
  }
 ?>
 </div>