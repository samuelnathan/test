<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 use \OMIS\Auth\Role as Role;
 use \OMIS\Template as Template;
 use \OMIS\Database\CoreDB as CoreDB;

 if (!defined('_OMIS')) {
    define('_OMIS', 1);
 }
 
 if (isset($_POST['isajax']) && isset($_POST['session_id'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    require_once __DIR__ . '/osce_functions.php';
    $sessionID = trim($_POST['session_id']);
 } else {

    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
 }

 $sessionExists = $db->exams->doesSessionExist($sessionID);
 
 if ($sessionExists) {
   
    // Previous Exams for Admins & Super Admins
    if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
        
        $exams = CoreDB::group(
                   CoreDB::into_array(
                     $db->exams->getList()
        ), ['term_id'], true);
  
    // Previous exams for School Admins      
    } else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {     
    
        $adminSchools = $db->schools->getAdminSchools(
           $_SESSION['user_identifier']
        );

        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));

        $exams = CoreDB::group(
            CoreDB::into_array(
          $db->exams->getListFiltered(null, $schoolIDs)
        ), ['term_id'], true);
     
    // Previous Exams for Examiners
    } else {
      
        // Get Examiner Departments
        $examinerDepts = [];
        $examinerDeptsDB = $db->departments->getExaminerDepartments($_SESSION['user_identifier']);

        while ($dept = $db->fetch_row($examinerDeptsDB)) {
            $examinerDepts[] = $dept['dept_id'];
        }

        $exams = CoreDB::group(
                   CoreDB::into_array(
                     $db->exams->getList($examinerDepts)
        ), ['term_id'], true);
        
    }
          
  }
  
  // Render this scripts template
  $template = new Template(Template::findMatchingTemplate(__FILE__));
  $template->render([
     'sessionExists' => $sessionExists,
     'exams' => $exams
  ]);
  