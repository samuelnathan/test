<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 /**
  * Render Station List for session
  * 
  * @param int $sessionID ID number of session.
  * @return boolean TRUE if stations were rendered, FALSE otherwise.
 */
 function stationsDisplay($sessionID)
 {
    // Get DB Instance
    $db = \OMIS\Database\CoreDB::getInstance(); 
    $stations = $db->exams->getStations($sessionID, false, 'station_number', 'form_name', true);

    if (mysqli_num_rows($stations) > 0) {
      
    ?><ul class="itemdet"><?php
    
       while ($station = $db->fetch_row($stations)) {

         ?><li><?=$station['station_number'];
         
          // If Rest Station
          if ($station['rest_station']) {
            
             echo " - REST STATION";
            
          } else {
            
             $group = $station['station_code'];
             echo (strlen($group) > 0 ? " - <i>$group</i>" : "") . " - " . $station['form_name'];
            
          }

          // Multiple examiner? then display icon
          if ($station['examiners_required'] > 1) {
            
             ?>&nbsp;<img class="multiexaminer-icon" src="assets/images/group.svg" 
             alt="multiple" title="Multiple <?=ucwords(gettext('examiners'))?>"/><?php
             
          }
       
         ?></li><?php
      
       }
     
       ?></ul><?php
       return true;
       
    } else {
      
       ?><div class="red">No stations attached</div><?php
       return false;
       
    }
    
 }

/**
 * Render student groups for session
 * 
 * @param int $sessionID ID number of station.
 * @return boolean TRUE if students were rendered, FALSE otherwise.
 */
 function groupsDisplay($sessionID)
 {
     $db = \OMIS\Database\CoreDB::getInstance(); //Get DB Instance
     $arestuds = false;
     $groups = $db->exams->getSessionGroups($sessionID);

     if (mysqli_num_rows($groups) > 0) {
       ?><ul class="itemdet"><?php
        while ($group = $db->fetch_row($groups)) {
            $studentCount = $db->exams->getGroupSize($group['group_id'], true);
            if ($studentCount > 0) {
                $arestuds = true;
            }
      ?><li><?=$group['group_name']?>&nbsp;&nbsp;[<span class="red"><?=$studentCount?></span> <?=gettext("students")?>]</li><?php
        }
      ?></ul><?php
     } else {
      ?><div class="red">No <?=gettext('groups')?> attached</div><?php
     }
     return $arestuds;
 }

 /**
  * 
  * @param type $stationID
  * @return boolean
  */
 function examinersDisplay($stationID) {
    
    $db = \OMIS\Database\CoreDB::getInstance();
    $examinersDB = $db->exams->getExaminersForStation($stationID, true);
    $station = $db->exams->getStation($stationID, true);
    $requiredExaminers = $station['examiners_required'];
    
    // Count all examiners (excluding observers)
    $examiners = array_filter($examinersDB, function($examiner) { 
        return ($examiner['scoring_weight'] != 0);
    });
    $numOfExaminersAssigned = count($examiners);
    
    // Get the num of examiners
    ?>
    <ul class="itemdet">
    <?php

     // Render list of examiners
     foreach ($examinersDB as $examiner) {
       $examinerStr = $examiner['forename'] . " " . $examiner['surname'] . " - " . $examiner['user_id'];
       ?>
       <li><?=$examinerStr?></li>
       <?php
     }

     // Indicate remaining slots
     $remainingSlots = ($requiredExaminers - $numOfExaminersAssigned);
     for ($slot=1; $slot<=$remainingSlots; $slot++) {
       $title = "Any " . gettext('examiners') . " within current " . gettext('department') . " can " . gettext('examine') . "&#013;"
              . "and they are given a default regular weighting (2)";
       ?>
       <li class="red help" title="<?=$title?>">open slot (any <?=gettext('examiner')?>)</li>
       <?php
     }
    ?>
    </ul>
    <?php
    
 }

 /**
  * Build up filter index for groups of students
  */
 function buildFilterIndex($students)
 {
     $filterIndex = ["sn" => [], "fn" => []];
     foreach ($students as $student) {
         $surnameChar = mb_strtoupper(mb_substr($student['surname'], 0, 1, 'UTF-8'));
         $forenameChar = mb_strtoupper(mb_substr($student['forename'], 0, 1, 'UTF-8'));

         if (!in_array($surnameChar, $filterIndex['sn'], true)) {
             $filterIndex['sn'][] = trim($surnameChar);
         }
         if (!in_array($forenameChar, $filterIndex['fn'], true)) {
             $filterIndex['fn'][] = trim($forenameChar);
         }
     }
     sort($filterIndex['sn']);
     sort($filterIndex['fn']);

     return $filterIndex;
 }

 /**
  * Render administrator list for session
  * 
  * @param int $sessionID ID number of session
  */
 function assistantsDisplay($sessionID)
 {
     $db = \OMIS\Database\CoreDB::getInstance(); //Get DB Instance
     $admins = $db->exams->getSessionAssistants($sessionID);

     if (mysqli_num_rows($admins) > 0) {
     ?><ul class="itemdet"><?php
        while ($admin = $db->fetch_row($admins)) {
     ?><li><?= $admin['forename'] . " " . $admin['surname']; ?></li><?php
        }
     ?></ul><?php
     } else {
     ?><div class="red">No assistants attached</div><?php
     }
 }

//Build Up Drop Down Data
function buildSessionsDropdown($exam)
{
    //Get DB Instance
    $db = \OMIS\Database\CoreDB::getInstance(); 
    $sessionsArray = [];
    $examsArray = ['exam_stations' => false, 
                   'exam_groupnames' => false,
                   'exam_assistants' => false];
    
    //Get Sessions
    $sessionsDB = $db->exams->getExamSessions($exam); 
    while ($eachSession = $db->fetch_row($sessionsDB)) {
        setA($eachSession, 'session_stations', $db->exams->doesSessionHaveStations($eachSession['session_id']));
        setA($eachSession, 'session_groups', $db->exams->doesSessionHaveGroups($eachSession['session_id']));
        setA($eachSession, 'session_assistants', $db->exams->doesSessionHaveAssistants($eachSession['session_id']));
        $sessionsArray[] = $eachSession;

        // Exam has stations
        if ($eachSession['session_stations']) {
            $examsArray['exam_stations'] = $eachSession['session_stations'];
        } 
 
        // Exam has Groups
        if ($eachSession['session_groups']) {
            $examsArray['exam_groups'] = $eachSession['session_groups'];
        }
        
        // Exam has Assistants
        if ($eachSession['session_assistants']) {
            $examsArray['exam_assistants'] = $eachSession['session_assistants'];
        }
        
    }
    return array($examsArray, $sessionsArray);
}

// Output sessions dropdown
function outputSessionsDropdown($exams, $sessions, $type) {
    
   // There are sessions available then render
   if (sizeof($sessions) > 0 && isset($exams['exam_'.$type])) {
       
     // If type is groups then add additional text (Names Only)
     $defaultOption = "Reuse " . ucwords($type) . " From";   
     if ($type == "groups") {
       $defaultOption .= " (Names Only)";  
     }
     ?>      
     <select class="clone-<?=$type?>" id="clone-<?=$type?>">
       <option class="bold" value=""><?=$defaultOption?></option>
      <?php 
      
       // Render all sessions
       foreach ($sessions as $eachSession) {
           
         // Stick select option of set as a session variable
	 if ($eachSession['session_'.$type]) {
            $dateFormatted = formatDate($eachSession['session_date'], NULL, 'SHORT_YEAR');
	    $sessionDesc = $dateFormatted.' '.$eachSession['session_description']; 
            ?>
            <option value="<?=$eachSession['session_id']?>" title="<?=$sessionDesc?>">
                    <?=$sessionDesc?>
            </option>
            <?php 
	 }
         
       }
      ?>
      </select>
       
      <?php
      
   } else {
     ?>
     &nbsp;<input type="hidden" id="clone-<?=$type?>" value=""/>
     <?php
   }
 }
 
/**
 * Render settings template
 * 
 * @param  $mode               view|add|edit mode
 * @param type $sessionRecord  the session object
 */
 function sessionSettingsTemplate($mode, &$sessionRecord = NULL) {
   ?>
   <ul class="settings">
    <li>
    <label for="<?=$mode?>_acc" title="Is Day/Circuit published?">ACCESSIBLE</label>
    <?php 
   
    $sessionPublished = (!is_null($sessionRecord) && $sessionRecord['session_published'] == 1);
    
    // View session record
    if ($mode == "view") {

      if ($sessionPublished) {
        ?>
        <span class="green">YES</span>
        <?php 
      } else { 
        ?>
         <span class="red">NO</span>
        <?php
      }
      
    // Add/Edit session record
    } else {
        
      $checked = ($sessionPublished) ? ' checked="checked"': '';
      ?><input type="checkbox" id="<?=$mode?>_acc" class="setting-checkbox"<?=$checked?>/><?php
    
    }
     
    ?>
    </li>
   </ul><?php

 }
