<?php

define('_OMIS', 1);

defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");

require_once __DIR__ . '/../extra/essentials.php';
require_once __DIR__ . '/osce_functions.php';

function examinerDTO($examiner) {
    return [
        "scoring_weight" => $examiner["scoring_weight"],
        "user_id" => $examiner["user_id"],
        "surname" => $examiner["surname"],
        "forename" => $examiner["forename"],
        "email" => $examiner["email"],
        "contact_number" => $examiner["contact_number"]
    ];
}

function groupStations($stations, $key) {
    $groups = [];

    foreach ($stations as $station) {
        $value = $station[$key];

        if (array_key_exists($value, $groups))
            $groups[$value][] = $station;
        else
            $groups[$value] = [$station];
    }

    return $groups;
}

function isBlankStudent($student) {
    return array_key_exists('blank_student_id', $student);
}

//$sessionID = filter_input(INPUT_GET, "session_id", FILTER_SANITIZE_NUMBER_INT);
$sessionID = $_GET['session_id'];

$groups = $db->exams->getSessionGroups($sessionID, true);
$stations = $db->stations->bySession($sessionID);

$examID = $db->sessions->getSessionExamID($sessionID);
$examSettings = $db->examSettings->get($examID);

// Group the stations by station num
$stations = groupStations($stations, 'station_number');

// Get the ongoing rotation data
$ongoing = $db->exams->getLastUpdatedRotation($sessionID, true);

// Variable that stores the number of students in the groups
$numberOfStudents = null;

// Map each group into the rotations for that group
$result = array_map(function($group) use ($stations, $db, $examSettings, $sessionID, &$numberOfStudents) {
    // Get the students in the group
    $students = $examSettings['exam_student_id_type'] === CANDIDATE_NUMBER_TYPE ?
        $db->groups->getStudentsByGroupWithCandidateNumber($group['group_id']) :
        $db->groups->getStudentsByGroup($group["group_id"]);
    // Get the blank students in the group
    $blanks = $db->exams->getBlankStudents($group["group_id"]);

    // Merge the students and the blank students and sort the result by the 'student_order' field
    $allStudents = array_merge($students, $blanks);
    usort($allStudents, function ($student1, $student2) {
        $ord1 = (int)$student1['student_order'];
        $ord2 = (int)$student2['student_order'];

        if ($ord1 == $ord2)
            return 0;

        return ($ord1 < $ord2) ? -1 : 1;
    });

    // If the number of students is less than the number of stations, fill the missing students with blanks
    for ($bsi = count($allStudents); $bsi < count($stations); $bsi++) {
        $allStudents[] = [
            'blank_student_id' => "__$bsi",
            'group_id' => $group['group_id'],
            'student_order' => (string)($bsi + 1),
            'discarded' => false
        ];
    }

    // Update the number of students. (this value should be uniform across the session though)
    $numberOfStudents = $numberOfStudents < count($allStudents) ? count($allStudents) : $numberOfStudents;

    // Rotations array for this group
    $rotations = [
        "group" => $group,
        "rotations" => []
    ];
    $initialIndex = 0;

    for ($i = 0; $i < $numberOfStudents; $i++) {
        $rotation = [];

        if ($initialIndex < 0)
            $initialIndex = $numberOfStudents - 1;
        $j = $initialIndex;

        for ($station_number = 1; $station_number <= $numberOfStudents; $station_number++) {
            if ($j >= $numberOfStudents)
                $j = 0;

            $student = $allStudents[$j];

            $j++;

            if (array_key_exists('blank_student_id', $student))
                $student['discarded'] = $db->exams->isBlankStudentDiscarded($student['blank_student_id'], $station_number);

            // If it's not an implicit rest station
            if (array_key_exists("$station_number", $stations)) {
                $station = $stations["$station_number"];
                $rotation[] = [
                    "station" => $station,
                    "student" => $student,
                    "progress" => isBlankStudent($student) ? [] :
                        $db->exams->getScoringProgressByStationNum($student['student_id'], $sessionID, $station_number),
                    "complete" => isBlankStudent($student) ? [] :
                        $db->exams->getScoringCompleteness($student['student_id'], $sessionID, $station_number),
                    "absent" => isBlankStudent($student) ? false :
                        array_reduce(
                            array_map(function ($st) use ($db, $student) {
                                return $db->results->isAbsent($student['student_id'], $st['station_id']);
                            }, $station),
                            function ($acc, $e) {
                                return $acc || $e;
                            }, false)
                ];
            // Otherwise, we assume it's a rest station
            } else {
                $rotation[] = [
                    "station" => [[
                        "examiners_required" => "0",
                        "form_id" => "0",
                        "form_name" => "",
                        "rest_station" => "1",
                        "session_id" => $sessionID,
                        "station_number" => "$station_number",
                        "station_order" => "$station_number"
                    ]],
                    "student" => $student,
                    "progress" => [],
                    "complete" => [],
                    "absent" => false
                ];
            }
        }

        $initialIndex--;
        $rotations['rotations'][] = $rotation;
    }
    return $rotations;
}, $groups);

$stationsWithExaminers = [];
for ($station_number = 1; $station_number <= $numberOfStudents; $station_number++) {
    $stationsWithExaminers[] = [
            'station_number' => "$station_number",
            'examiners' => array_key_exists("$station_number", $stations) ? array_reduce($stations["$station_number"], function($acc, $station) use ($db) {
                return $acc + array_map('examinerDTO',
                        array_values($db->exams->getExaminersForStation($station['station_id'], true)));
            }, []) : []
    ];
}

header("Content-type: application/json");
echo json_encode([
    "groups" => $result,
    "stations" => $stationsWithExaminers,
    "ongoing" => $ongoing
]);