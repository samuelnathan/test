<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}
 
$data = [
  'exam_id' => $_REQUEST['exam_id'],
  'show_session' => iS($_REQUEST, 'show_session')
];

if (isset($_REQUEST['session_id'])) {
    $sessionID = trim($_REQUEST['session_id']);
    $data['session_id'] = $sessionID;

    // Get Session
    $sessionDB = $db->exams->getSession($sessionID);
    if (!is_null($sessionDB)) {
        $sessionDB['session_date'] = formatDate($sessionDB['session_date']);
        $examID = $sessionDB['exam_id'];
        
        // Get Department Name
        $sessionDB['dept_name'] = $db->departments->getDepartmentName($sessionDB['dept_id']);
        $data['session'] = $sessionDB;

        // Get Session Assistants
        $assistantsDB = $db->exams->getSessionAssistants($sessionID);
        $data['assistants'] = [];
        if (mysqli_num_rows($assistantsDB) > 0) {
            while ($assistant = $db->fetch_row($assistantsDB)) {
                $data['assistants'][] = $assistant;
            }
        }

        // Get first and last exam sessions
        $sessionsDB = array_column(
            $db->exams->getExamSessions($examID, true),
            'session_id'
        );
        
        $arrayHelp = new \OMIS\Utilities\ArrayHelp($sessionsDB);

        // Previous and next session urls
        list($previousSession, $nextSession) = $arrayHelp->previousAndNext($sessionID);
        $previousUrl = empty($previousSession) ? "" : "manage.php?"
        . http_build_query([
            'page' => "assistants_osce",
            'session_id' => $previousSession,
            'exam_id' => $examID
        ]);

        $nextUrl = empty($nextSession) ? "" : "manage.php?"
        . http_build_query([
            'page' => "assistants_osce",
            'session_id' => $nextSession,
            'exam_id' => $examID
        ]);
     
       /**
        * Jump to group records previous and next
        */
       $data['jumpRecords'] = [
            'customEvents' => true,
            'boxWidth' => 'auto',
            'previousRecordLabel' => "Day/Circuit",
            'nextRecordLabel' => "Day/Circuit",
            'returnLabel' => "Return to Day/Circuit List",
            'previousUrl' => $previousUrl,
            'nextUrl' => $nextUrl
       ];
        
    }
}

$template = new OMIS\Template(\OMIS\Template::findMatchingTemplate(__FILE__));
$template->render($data);
