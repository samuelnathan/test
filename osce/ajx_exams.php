<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 use \OMIS\Template as Template;
 define('_OMIS', 1);

 $mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);

 if (!is_null($mode)) {
     require_once __DIR__ . '/../extra/essentials.php';
     // Page Access Check / Can User Access this Section?
     if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
         return false;
     }
     require_once __DIR__ . '/osce_functions.php';
 } else {
     include __DIR__ . '/../extra/noaccess.php';
 }

 // Multiple examiners feature enabled/disabled
 $multiExaminersFeature = $db->features->enabled('multi-examiners', $_SESSION['user_role']);
 $calendarEnabled = $config->eventQueue['enabled'];

 // Add Exam Action
 if ($mode == 'add') {

     $countdownDefault = (isset($config->exam_defaults['show_countdown'])) ? $config->exam_defaults['show_countdown'] : true;

     $template = new Template('exam.record.html.twig');
     $data = [
         'mode' => 'add',
         'row_id' => 0,
         'exam_data' => [
             'default_examiners_station' => 1,
             'show_countdown' => $countdownDefault
         ],
         'session_data' => [],
         'exam_has_result' => false,
         'delete_results_warning' => false,
         'show_advanced_settings' => false,
         'show_matrix_option' => false,
         'show_multiexaminers_settings' => $multiExaminersFeature,
         'show_submit_and_next' => !$config->exam_defaults['multiple_candidate'],
         'grade' => [
             'list' => $db->gradeRules->all()
         ]
     ];
     $template->render($data);

 // Edit Exam Action
 } elseif ($mode == 'edit') {

     $examID = filter_input(INPUT_POST, 'exam_id', FILTER_SANITIZE_NUMBER_INT);
     $examData = $db->exams->getExam($examID);

     // Exam Exists
     if (!is_null($examData)) {

       // Gather session data for this exam
       $sessionsDB = $db->exams->getExamSessions($examID);
       $hasCircuitColour = false;
       $sessions = [];

       // Advanced settings feature enabled/disabled
       $advancedSettings = $db->features->enabled('advanced-settings', $_SESSION['user_role']);

       // Gather session data
       while ($eachSession = $db->fetch_row($sessionsDB)) {
         $sessionID = $eachSession['session_id'];
         $eachSession['date'] = formatDate($eachSession['session_date']);
         $eachSession['colour_hex'] = $eachSession['circuit_colour'];
         $eachSession['colour_name'] = $db->exams->getColourName($eachSession['circuit_colour']);
         if (strlen($eachSession['colour_hex']) > 0) {
            $hasCircuitColour = true;
         }
         $sessions[$sessionID] = $eachSession;
       }


       $template = new OMIS\Template('exam.record.html.twig');
       $data = [
           'mode' => 'edit',
           'row_id' => 0,
           'exam_data' => $examData,
           'session_data' => $sessions,
           'exam_has_result' => false,
           'exam_has_circuit_colour' => $hasCircuitColour,
           'delete_results_warning' => false,
           'show_advanced_settings' => $advancedSettings,
           'show_matrix_option' => false,
           'show_multiexaminers_settings' => $multiExaminersFeature,
           'show_submit_and_next' => !$config->exam_defaults['multiple_candidate'],
           'exam_has_result' => $db->results->doesExamHaveResults($examID),
           'grade' => [
             'list' => $db->gradeRules->all(),
             'selected' => $examData['grade_rule']
           ]
       ];
       $template->render($data);

     } else {
       echo "EXAM_NON_EXIST";
     }

 // Submit Exam Action
 } elseif ($mode == 'submit') {

     $dept = trim($_POST['dept']);
         
     if ($db->departments->existsById($dept) &&
         $db->academicterms->existsById($_SESSION['cterm'])) {

         $db->exams->insertExam(
             $dept,
             $_SESSION['cterm'],
             $_POST['add_name'],
             $_POST['add_desc'],
             [
                 'published' => $_POST['add_published'],
                 'results' => $_POST['add_viewresults'],
                 'marks' => $_POST['add_viewmarks'],
                 'totals' => $_POST['add_viewtotals'],
                 'next' => $_POST['add_submitnext'],
                 'countdown' => $_POST['add_countdown'],
                 'examiners' => $_POST['add_examinersdefault'],
                 'grade' => $_POST['grade']
             ]
         );

         // DEFAULT settings for exam
         $settings = [
          [
            'field' => 'exam_student_id_type',
            'value' => $config->exam_defaults['exam_student_id_type']
          ],
          [
            'field' => 'preserve_group_order',
            'value' => $config->exam_defaults['preserve_group_order']
          ],
          [
             'field' => 'feedback_to_right',
             'value' => $config->exam_defaults['feedback_to_right']
           ],
          [
             'field' => 'multiple_candidate',
             'value' => $config->exam_defaults['multiple_candidate']
           ],

         ];

         // Apply default result column settings if they exist in the config file
         if (isset($config->exam_defaults['result_columns']) &&
             sizeof($config->exam_defaults['result_columns']) > 0) {
           $examID = $db->inserted_id();
           $settingsConfig = $config->exam_defaults['result_columns'];

           // Loop through config settings and build array to pass to database
           foreach ($settingsConfig as $field => $eachSetting) {
               $settings[] = ['field' => 'results_column_' . $field,
                              'value' => (int)$eachSetting['enabled']];
           }
         }

         // Update exam settings in database
         $db->examSettings->update($examID, $settings);

         // DEFAULT rules per exam
         $rules = [
           [
             'field' => 'multi_examiner_results_averaged',
             'value' => (int) $config->exam_rules['multi_examiner_results_averaged']
           ]
         ];

         // Update exam rules in database
         $db->examRules->updateRules($examID, $rules, false);
echo 0;
     } else {

         echo 'DEPT_NON_EXIST';

     }

 // Update Exam Action
 } elseif ($mode == 'update') {

     $examID = trim($_POST['edit_id']);

     if ($db->exams->doesExamExist($examID)) {

         $db->exams->updateExam(
             $examID,
             $_POST['edit_name'],
             $_POST['edit_desc'],
             [
                 "published" => $_POST['edit_published'],
                 "results" => $_POST['edit_viewresults'],
                 "marks" => $_POST['edit_viewmarks'],
                 "totals" => $_POST['edit_viewtotals'],
                 "next" => $_POST['edit_submitnext'],
                 "countdown" => $_POST['edit_countdown'],
                 "examiners" => $_POST['edit_examinersdefault'],
                 "grade" => $_POST['grade']
             ]
         );

         // Set default number of examiners at all present stations
         $db->examSettings->setDefaultNumExaminersStation(
                 $examID,
                 $_POST['edit_examinersdefault']
         );

         // Notify an update in the Event queue
         if ($calendarEnabled) {

             $examTimeRange = $db->exams->getExamTimeRange($examID);
             $examRecord = $db->exams->getExam($examID);

             $eventNotifier = new \OMIS\Events\Calendar\CalendarEventNotifier();
             $eventNotifier->notifyExamUpdate(
                 $examRecord,
                 $examTimeRange['time_start_exam'],
                 $examTimeRange['time_end_exam']);


         }

     } else {
         echo 'EXAM_NON_EXIST';
     }
 }
