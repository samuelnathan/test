<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019 Qpercom Limited
 */

 use \OMIS\Session\FlashList as FlashList;
 use \OMIS\Session\FlashMessage as FlashMessage;

 /* Defined to tell any included files that they have been included rather than
  * being invoked directly from the browser. Need to declare it anywhere that a
  * file is directly invoked by the user (via AJAX).
  */
 define('_OMIS', 1);
 
 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = [
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    ];
    $template->render($data);
    return;
 }

 // Redirect to the exam matrix export feature
 if (!is_null(filter_input(INPUT_POST, 'exam_matrix'))) {
    
   include "../export/export_matrix.php";
   
 /**
  *  Save session groups: redirect request coming from groups_osce.php page
  */
 } elseif (isset($_POST['save_groups'])) {
    
    // Session ID passed in
    $sessionID = trim($_POST['session_id']); 
    $groups = [];
 
    if (!$db->sessions->existsById($sessionID)) {
        
        header('Location: ../manage.php?page=groups_osce');
        exit();

    }
    
    if (isset($_POST['groupval'])) {
        
        $session = $db->sessions->getById($sessionID);
        $examID = $session['exam_id'];
        $exam = $db->exams->getExam($examID);
        $eachGroup = $_POST['groupval'];
        $groupOrder = 0;

        for ($i = 0; $i < sizeof($eachGroup); $i++) {

            $groupOrder++;
            $groupID = trim($eachGroup[$i]);
            $originalGroupID = $groupID;
            $groupName = trim($_POST['name_' . $groupID]);
 
            // New group found
            if (stristr($groupID, 'new')) {

                $groupID = $db->groups->addSessionGroup(
                        $sessionID,
                        $groupName,
                        $groupOrder
                );
                $groups[] = $groupID;
                
             // Existing group
            } elseif ($db->groups->doGroupsExist([$groupID])) {

                $db->groups->updateGroup($groupID, $groupName, $groupOrder);
                $groups[] = $groupID;
                $db->groups->deleteStudentsInGroup($groupID);
                $db->blankStudents->deleteBlankStudentsInGroup($groupID);

            }
                        
            // Add students to groups
            if (isset($_POST['groups_students'][$originalGroupID])) {
                
                $examNumbers = isset($_POST['student_exam_number']) ? $_POST['student_exam_number'] : [];
                $students = $_POST['groups_students'][$originalGroupID];

                $db->groups->addStudentsToGroup(
                    $students, 
                    $groupID,
                    $examNumbers
                );
                
            }
            
        }
        
        // Logging
        \Analog::info($_SESSION['user_identifier'] 
             . " updated groups in session " . $session['session_description'] . " (ID $sessionID) " 
             . " in exam: " . $exam['exam_name'] . " (ID " . $exam['exam_id'] . ")"
        );
       
    }
    
    /*
     *  Delete student results approved by user
     */
    if (isset($_POST['remove_results'])) {

       foreach ($_POST['remove_results'] as $studentID) {

          $db->selfAssessments->removeOwnershipByExamStudent($examID, $studentID);
          $db->results->deleteStudentSessionResults($sessionID, $studentID);
          $db->examNotifications->dropBySessionStudent($sessionID, $studentID);

          if ($db->gradeRules->hasGrade($examID, $studentID))
              $db->gradeRules->resetGrade($examID, $studentID);

       }

    }
    
    // Delete old groups
    $db->groups->deleteOldGroups($sessionID, $groups);

    $url = "../manage.php"
         . "?page=groups_osce" 
         . "&session_id=" . $sessionID 
         . "&saved=1"
         . "&show_session=" . $_POST['show_session'];
    
    header('Location: ' . $url);
    
 } elseif (isset($_POST['go_stations'])) {
   
    // Redirects for Session Stations   
    if ($_POST['todo'] == "remove") {
      
        $sessionID = trim($_POST['session_id']);
        $db->stations->deleteStations($_POST['stations_check'], $sessionID);
        $db->blankStudents->deleteBlanksSubmittedBySession($sessionID);
        (new \OMIS\Outcomes\OutcomesService($db))->setUpToDateBySession($sessionID,false);
        
    } elseif ($_POST['todo'] == 'popup') {
      
        $db->examSettings->setStationsNotesAutoPopup($_POST['stations_check'], true);
        
    } elseif ($_POST['todo'] == 'nopopup') {
      
        $db->examSettings->setStationsNotesAutoPopup($_POST['stations_check'], false);
        
    }

    $url = "../manage.php"
         . "?page=stations_osce"
         . "&session_id=" . $_POST['session_id'] 
         . "&show_session=" . $_POST['show_session'];
    
    header('Location: ' . $url);
    
 /**
  *  Save station examiners
  */
 } elseif (isset($_POST['save_examiners'])) {
    
    // Gather passed in POST params
    $sessionID = filter_input(INPUT_POST, 'session', FILTER_SANITIZE_NUMBER_INT);
    $showSession = filter_input(INPUT_POST, 'show_session', FILTER_SANITIZE_NUMBER_INT);
    $stationID = filter_input(INPUT_POST, 'station_id', FILTER_SANITIZE_NUMBER_INT);
    
    // Number of slots requested
    $numSlots = filter_input(
            INPUT_POST,
            'num_slots',
            FILTER_SANITIZE_NUMBER_INT,
            ['options' => ['default' => 1]]
    );
    
    // If our station exists then continue, if not then output appropriate error
    if (!is_null($stationID) && $db->stations->doesStationExist($stationID)) {
        
       $examinersParam = filter_input(
                 INPUT_POST,
                 'examiners',
                 FILTER_SANITIZE_STRING,
                 FILTER_REQUIRE_ARRAY
       );
       
       $observersParam = filter_input(
                 INPUT_POST,
                 'observers',
                 FILTER_SANITIZE_STRING,
                 FILTER_REQUIRE_ARRAY
       );
        
       $weightsParam = filter_input(
                 INPUT_POST,
                 'weightings',
                 FILTER_SANITIZE_NUMBER_INT,
                 FILTER_REQUIRE_ARRAY
       );
      
       // Set empty array as default for both the examiners and weights POST parameters
       $examiners = (is_null($examinersParam)) ? [] : $examinersParam;
       $observers = (is_null($observersParam)) ? [] : $observersParam;
       $weights = (is_null($weightsParam)) ? [] : $weightsParam;
       
       // Remove duplicate examiners
       $examinersUnique = array_unique($examiners);
       
       // Remove duplicate observers
       $observersUnique = array_unique($observers);       
       
       // Number of examiners we're left with
       $examinerCount = count($examinersUnique);
       
       // Start transaction
       $db->startTransaction();
       
       // Remove old list of examiners
       $removed = $db->stationExaminers->removeStationExaminers($stationID);
       
       // Set number of examiners required
       $setRequiredNum = $db->stations->setExaminersRequired($stationID, $numSlots);
       
       // Old examiner list removed and required number set
       if ($removed && $setRequiredNum) {

         // Assign examiners
         for ($index=0; $index<$examinerCount; $index++) {
             
           // Each examiner record
           $examiner = $examiners[$index];
           
           // Skip unselected examiners and users that don't exist
           if (strlen($examiner) == 0 || !$db->users->existsById($examiner)) {
               continue;
           }
          
           // Note: default weight = Regular Weighting (2)
           $defaultWeight = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;
           $weight = (isset($weights[$index]) && strlen($weights[$index]) > 0) ? $weights[$index] : $defaultWeight;
           $db->stationExaminers->assignStationExaminer(
                   $examiner,
                   $stationID,
                   $weight
           );

         }
         
         // Assign observers
         foreach($observersUnique as $observer) {
            
           // Skip unselected observer and users that don't exist
           if (strlen($observer) == 0 || !$db->users->existsById($observer)) {
               continue;
           }
           
           // Assign observer
           $observerWeight = 0;
           $db->stationExaminers->assignStationExaminer(
                   $observer,
                   $stationID,
                   $observerWeight
           );
             
         }
         
         
         $db->commit();

         // Update related outcomes
         (new \OMIS\Outcomes\OutcomesService($db))->setUpToDateBySession($sessionID,false);
         
       // ERROR: examiners could not be assigned
       } else {
           
         // Flash an error message back at the user
         (new FlashList())->add(
           "Examiners not assigned to station. " .
           "Old examiner list could not be removed and/or required slots not set",                 
           FlashMessage::SESSION_FLASH_ERROR
         );
           
         // Abort, something went wrong
         $db->rollback();
         
       }
    
    // ERROR: examiners could not be assigned
    } else {
         // Flash an error message back at the user
         (new FlashList())->add(
           "Examiners not assigned to station. " .
           "Station ID not specified or station no longer exists in system",
           FlashMessage::SESSION_FLASH_ERROR
         );  
    }
  
    $url = "../manage.php"
         . "?page=examiner_osce"
         . "&station_id=" . $stationID 
         . "&session_id=" . $sessionID 
         . "&show_session=" . $showSession;
    
    header('Location: ' . $url);
    
  } elseif (isset($_POST['go_sessassist'])) {
    
    // Remove assistants from session
    if ($_POST['todo'] == "remove") { 
        $examID = trim($_POST['exam_id']);
        $session_id = trim($_POST['session_id']);
        $db->sessionAssistants->removeSessionAssistants($_POST['sessassist_check'], $session_id);

        $url = "../manage.php"
             . "?page=assistants_osce" 
             . "&session_id=" . $session_id 
             . "&exam_id=" . $examID 
             . "&show_session=" . $_POST['show_session'];
        header("Location: " . $url);
    }
    
  /**
   * Exams redirect  
   */
  } elseif (isset($_POST['go_exam'])) {
    //Delete Exams
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->exams->deleteExams($_POST['exam_check']);
    }

    header('Location: ../manage.php?page=manage_osce');
 
  /**
   *  Exam Sessions redirect  
   */
  } elseif (isset($_POST['go_sessions'])) {
  
    // Redirects for exam sessions section
    if ($_POST['todo'] == "delete") { 
      
        $db->sessions->deleteSessions($_POST['sessions_check']);
        
    // Published no    
    } elseif ($_POST['todo'] == "access_no") { 
        $db->examSettings->setSessionsPublished($_POST['sessions_check'], 0);
        
    // Published yes    
    } elseif ($_POST['todo'] == "access_yes") {
      
        $db->examSettings->setSessionsPublished($_POST['sessions_check'], 1);
        
    }
    
    $examParam = filter_input(INPUT_POST, 'exam_id');
    $showSessionParam = filter_input(INPUT_POST, 'show_session');
    $url = "../manage.php"
         . "?page=sessions_osce"
         . "&exam_id=" . $examParam;
    
    if (!is_null($showSessionParam) && $db->sessions->existsById($showSessionParam)) {
      $url .= "&show_session=" . $showSessionParam;
    }
    
    header('Location: ' . $url);    
    
 } else {
   
    // If none of the above return to index page    
    header('Location: ../');
    
 }

