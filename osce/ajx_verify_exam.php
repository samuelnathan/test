<?php
/**
 * Script intended to be called by Ajax HTTP requests. Receives an Exam ID as a parameter in the requests and responds
 * with a JSON array of the warnings found for that exam
 *
 * @author Sergio Franco <sergio.franco@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */


define('_OMIS', 1);

require_once __DIR__ . '/../extra/essentials.php';

// Page Access Check / Can User Access this Section?
//if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
//    return false;
//}

require_once __DIR__ . '/osce_functions.php';


$examId = filter_input(INPUT_GET, 'examId', FILTER_SANITIZE_NUMBER_INT);
$earlyWarningSystem = $config->getEarlyWarning();

if (!$db->exams->doesExamExist($examId)) {
    http_response_code(404);
    echo 'Exam not found';
    return;
}

header('Content-Type: application/json');
echo json_encode($earlyWarningSystem->checkExam($examId));
