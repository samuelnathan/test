<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */
 use \OMIS\Template as Template;
 define('_OMIS', 1);

 $isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

 if (!empty($isfor)) {

    require_once __DIR__ . '/../extra/essentials.php';
    //Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

    require_once __DIR__ . '/../extra/helper.php';
    require_once __DIR__ . '/osce_functions.php';
    $sterm = selectedTerm();

 } else {

    require_once __DIR__ . '/../extra/noaccess.php';

 }

 // Feature Enabled|Disabled
 list($multiExaminersAllowed, $editContentAllowed) = 
 $db->features->enabled([
      'multi-examiners', 'assess-content'
      ], $_SESSION['user_role']
 );

 // Sticky station option
 $sticky = iS($_SESSION, 'stick_station_option');

 // Add New Station section
 if ($isfor == 'add') {

    $sessionID = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_NUMBER_INT);

    if ($db->sessions->existsById($sessionID)) {
    
        // Get Exam Tags
        $examTags = implode(',', 
          $db->stations->getStationGroups(
            $db->sessions->getSessionExamID($sessionID)
          )
        );

      ?>
        <td id="options-panel" colspan="9" class="nbr" style="border-bottom: 1px solid #ff0000 !important">
         <input type="hidden" id="exam_tags" value="<?=$examTags?>"/>
         <div class="divcrstat">
         <label for="crold"><?=ucwords(gettext('form'))?> from <?=gettext('Bank')?></label>
         <?php

         $oldStationChecked = (hasArrayValue($_SESSION, 'stick_station_option', 'bank_station')
            || strlen($sticky) == 0) ? ' checked="checked"' : '';

         ?>
         <input type="radio" name="createact" id="crold" <?=$oldStationChecked?>/>
      <?php

        if ($editContentAllowed) {

            ?><label for="crnew">New <?=gettext('rest-form')?></label><?php

          if (hasArrayValue($_SESSION, 'stick_station_option', 'new_station')) {

            $newStationChecked = ' checked="checked"';

          } else {

            $newStationChecked = '';

          }

      ?>
        <input type="radio" name="createact" id="crnew" <?=$newStationChecked?>/>
      <?php

        }

      ?>
        <label for="crprev">Reuse <?=ucwords(gettext('form'))?>, previous <?=gettext('OSCE')?></label>
      <?php
         $previousExamChecked = (hasArrayValue($_SESSION, 'stick_station_option', 'previous_exam_station')) 
         ? ' checked="checked"' : '';
      ?>
        <input type="radio" name="createact" id="crprev" <?=$previousExamChecked?>/>
        <label for="crclone">Reuse Stations</label>
      <?php
         $cloneExamChecked = (hasArrayValue($_SESSION, 'stick_station_option', 'clone_stations_exam')) 
         ? ' checked="checked"' : '';
      ?>
        <input type="radio" name="createact" id="crclone" <?=$cloneExamChecked?>/>
        </div>
        <div id="station-add-panel">
      <?php

        $isfor = 'add';

        if (hasArrayValue($_SESSION, 'stick_station_option', 'bank_station') || strlen($sticky) == 0) {

            // New assessment form
            include 'form_bank.php';

        } elseif ($editContentAllowed && hasArrayValue($_SESSION, 'stick_station_option', 'new_station')) {

            // Use form from assessment form bank
            include 'new_form.php';

        } elseif (hasArrayValue($_SESSION, 'stick_station_option', 'previous_exam_station')) {

            // Use single form from previous session
            include 'single_form_previous_session.php';

        } elseif (hasArrayValue($_SESSION, 'stick_station_option', 'clone_stations_exam')) {

            // Copy stations from previous session
            include 'copy_stations_previous_session.php';

        } else {

           echo "Please select an option above";

        }
        
       ?>
        </div>
        <div class="clearv2"></div>
        </td>
        <?php

    } else {

        echo 'SESSION_NON_EXIST';

    }

 // Edit Station Section    
 } elseif ($isfor == 'edit') {
    
  $stationID = trim($_POST['station_id']);
  $station = $db->stations->getStation($stationID);

  if (empty($station)) {

        echo 'STATION_NON_EXIST';
        exit;

  }

  // Station Has Result
  $stationHasResult = $db->results->doesStationHaveResults($stationID, 'bool');
    
  // Get Session ID
  $sessionID = $db->sessions->getSessionIDByStationID($stationID);

  // Get Session Exam ID
  $examID = $db->sessions->getSessionExamID($sessionID);
    
  // Get Exam Tags
  $examTags = implode(',', $db->stations->getStationGroups($examID));

  // Station Time Limit
  if (isset($station['station_time'])) {

     $defaultStationTime = explode(':', $station['station_time']);

  } elseif (isset($_SESSION['station_time_limit'])) {

     $defaultStationTime = explode(':', $_SESSION['station_time_limit']);

  } elseif (isset($config->station_defaults['timelimit'])) {

     $defaultStationTime = explode(':', $config->station_defaults['timelimit']);

  } else {

     $defaultStationTime = [00, 05, 00];

  }
  
  // Station Alarm 1
  if (isset($station['visual_alarm1'])) {

     $defaultAlarm1 = explode(':', $station['visual_alarm1']);

  } elseif (isset($_SESSION['station_alarm_one'])) {

     $defaultAlarm1 = explode(':', $_SESSION['station_alarm_one']);

  } elseif (isset($config->station_defaults['visualalarm1'])) {

     $defaultAlarm1 = explode(':', $config->station_defaults['visualalarm1']);

  } else {

     $defaultAlarm1 = [00, 00, 00];

  }
  
  // Station Alarm 2
  if (isset($station['visual_alarm2'])) {

     $defaultAlarm2 = explode(':', $station['visual_alarm2']);

  } elseif (isset($_SESSION['station_alarm_two'])) {

     $defaultAlarm2 = explode(':', $_SESSION['station_alarm_two']);

  } elseif (isset($config->station_defaults['visualalarm2'])) {

     $defaultAlarm2 = explode(':', $config->station_defaults['visualalarm2']);

  } else {

     $defaultAlarm2 = [00, 00, 00];

  }
  
  ?><td class="checkb">&nbsp;
     <input type="hidden" id="edit_id" value="<?=$stationID?>"/>
     <input type="hidden" id="exam_tags" value="<?=$examTags?>"/>
    </td>
    <td class="c">
      <input type="text" id="edit_nr" size="2" maxlength="3" value="<?=$station['station_number']?>"/>
    </td>
    <td class="c">
      <input type="text" id="edit_tag" value="<?=$station['station_code']?>" size="9" maxlength="20"/>
    </td>
    <td class="c">
      <input type="text" id="edit_pass" size="2" maxlength="3" value="<?=(float)$station['pass_value']?>"/>
      <span class="bold"> %</span>
    </td>

    <td class="c">
      <input type="text" id="weighting" size="2" maxlength="6" value="<?=(float)$station['weighting']?>"/>
      <span class="bold"> %</span>
    </td> 
    
    <td><?php

        $template = new Template('timepicker.html.twig');
        $template->render([
           "pickerID" => "edit",
           "time" => $defaultStationTime 
        ]);
    
    ?></td>
    
    <td><?php
    
        $template->render([
           "pickerID" => "edit-alarm1",
           "time" => $defaultAlarm1,
           "buffer" => true
        ]);
        
        $template->render([
           "pickerID" => "edit-alarm2",
           "time" => $defaultAlarm2 
        ]);        
        
    ?></td>
    
    <td><?php
    
    //If Station Does Not Have Result
    if (!$stationHasResult):
         
     // Get Station IDS By Session ID
     $existingForms = $db->forms->getFormIDsBySession($station['session_id']);
     
     // Get Department ID
     $departmentID = $db->departments->getDepartIDBySessionID($station['session_id']);
     
     // Get Forms     
     $formRecords = $db->forms->getForms(
         $sterm, $departmentID, 
         'form_name', 'ASC', false, true
     );
     
     $formGroups = [];
     while ($each = $db->fetch_row($formRecords)) {

        // Not in current list 
        if ($station['form_id'] == $each['form_id'] || !in_array($each['form_id'], $existingForms))
           $formGroups[$each['name']][] = $each;

     }

     ?><select id="edit_stat"><?php
     
      // We Have Forms ?
      if (count($formGroups) > 0):
          
         // Loop Through Forms
         foreach ($formGroups as $group => $forms):
        
            ?><optgroup label="<?=(empty($group) ? "Untagged / Uncategorised" : ucwords($group)) ?>"><?php
            
            foreach ($forms as $form):
              $selectedClause = ($station['form_id'] == $form['form_id']) ? 
                    ' selected="selected"' : '';

              // Not in current list
              ?><option value="<?=$form['form_id']?>"<?=$selectedClause?>><?php
                 echo $form['form_name'];
              ?></option><?php
              
            endforeach;
            
            ?></optgroup><?php
            
         endforeach;
         
       else:
         ?><option value="*--*">No assessment <?=gettext('forms')?> to select from</option><?php
       endif;
       
     ?></select><?php
       
    else:

      ?><div class="statmsg">
          Cannot change <br/>station type,<br/> results attached
        </div>
        <input type="hidden" id="edit_stat" value="<?=$station['form_id']?>"/><?php
    endif;
    
 ?></td>
    <?php
     // Multi examiners feature allowed
     if ($multiExaminersAllowed) {

    ?>
    <td>&nbsp;</td>
    <?php
     }
    ?>
    <td>
      <ul class="settings">
      <?php
      
   // Station notes
   if ($db->features->enabled('station-notes', $_SESSION['user_role'])):

        $notesCheckedClause = ($station['desc_auto_popup'] == 1) ? ' checked="checked"' : '';

     ?>
      <li>
        <label for="edit_autopop" title="Station/case notes display on load of assessment form?">NOTES AUTO POPUP</label>
        <input type="checkbox" id="edit_autopop" class="setting-checkbox"<?=$notesCheckedClause?>/>
      </li>
   <?php
   endif;
   $station_type = $db->sessions->getStationType($station['station_id']);
?>
    <label for="edit_station_type" title="Change station type">STATION TYPE</label>
    <select id="edit_station_type">
      <option value="Object" <?php if($station_type['station_type'] == "Object"){ echo "selected"; } ?>>OBJECT</option>
      <option value="Unmanned" <?php if($station_type['station_type'] == "Unmanned"){ echo "selected"; } ?>>UNMANNED</option>
      <option value="Assessor" <?php if($station_type['station_type'] == "Assessor"){ echo "selected"; } ?>>ASSESSOR</option>
    </select>
  <?php
   // Exclude from grading/appointability
   if ($db->exams->getExam($db->sessions->getSessionExamID($sessionID))['grade_rule'] > 1):

    ?>
      <li>
        <label for="exclude-grading" 
               title="Tick to exclude station from <?=gettext('grade')?> rules">
               EXCLUDE <?=gettext("GRADING")?>
        </label>
        <input type="checkbox" 
               id="exclude-grading" 
               class="setting-checkbox" 
               <?=$station['exclude_from_grading'] ? ' checked="checked"' : ""; ?>
        />
      </li>
    <?php

   endif;

  ?><br/>
    </ul>
  </td>
  <td>&nbsp;</td>
  <td class="titleedit">&nbsp;</td><?php
    
 } elseif ($isfor == 'update') {

    $id = trim(filter_input(INPUT_POST, 'edit_id', FILTER_SANITIZE_STRING));
    $stationDB = $db->stations->getStation($id, true);
    
    if (empty($stationDB)) {
        echo 'STATION_NON_EXIST';
        exit;
    }
    
    // Selected station ID
    $selectedFormID = filter_input(INPUT_POST, 'edit_stat', FILTER_SANITIZE_NUMBER_INT);
    $currentFormID = $stationDB['form_id'];
    $sessionID = $stationDB['session_id'];

    $stationExists = $db->forms->doesFormExist($selectedFormID);
    $sessionStationCount = $db->sessions->sessionFormCount($selectedFormID, $sessionID);

    if ($stationExists && !($currentFormID != $selectedFormID && $sessionStationCount == 1)) {

        $tagFiltered = preg_replace(
            '/[^a-zA-Z0-9\/#-]/',
            '',
            $_POST['edit_tag']
        );

        $db->stations->updateOne(
            $id,
            $selectedFormID,
            [
                "number" => $_POST['edit_nr'],
                "tag" => $tagFiltered,
                "pass" => $_POST['edit_pass'],
                "edit_station_type" => $_POST['edit_station_type'],
                "time" => $_POST['edit_time'],
                "alarm1" => $_POST['edit_alarm1'],
                "alarm2" => $_POST['edit_alarm2'],    
                "popup" => $_POST['autopopup'],
                "weighting" => $_POST['weighting'],
                "exclude_grade" => $_POST['exclude_grade']
            ]
        );
        ?> <script type="text/javascript">console.log("data");</script> <?php

        // Update related outcomes
        (new \OMIS\Outcomes\OutcomesService($db))->setUpToDateBySession($sessionID,false);

        $db->blankStudents->deleteBlanksSubmittedBySession($sessionID);

    } else {
        echo 'STATB_NON_EXIST';
    }
 }