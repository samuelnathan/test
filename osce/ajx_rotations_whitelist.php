<?php
/**
 * Requests a token to the monitor service and responds with it to the client. WebSocket clients must use this token
 * to subscribe to a session
 */

use GuzzleHttp\Client;

define('_OMIS', 1);

require_once __DIR__ . '/../extra/essentials.php';
require_once __DIR__ . '/osce_functions.php';

if (!isset($config)) {
    $config = \OMIS\Config;
}

$stack = new \GuzzleHttp\HandlerStack();
$stack->setHandler(new \GuzzleHttp\Handler\CurlHandler());
$stack->push($config->getMonitorAuthMiddleware()->authRequest());

$client = new Client([
    'base_uri' => $config->monitorNotification['httpEndpoint'],
    'handler' => $stack
]);

try {
    // Send the request to get a token with the logged user ID
    $response = $client->post('/client', [
        'json' => [
            'user' => $_SESSION["user_identifier"],
            'namespace' => $config->db['schema']
        ]
    ]);

    // Respond with the token
    echo $response->getBody()->getContents();
} catch (\GuzzleHttp\Exception\RequestException $e) {
    $errorMsg = 'Error contacting rotations monitor server';
    Analog::error($errorMsg);
    echo $errorMsg;
}