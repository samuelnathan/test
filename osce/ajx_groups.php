<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define("_OMIS", 1);
ob_start();

// JSON Helper functions.
use \OMIS\Utilities\JSON as JSON;
use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;

include_once __DIR__ . '/../vendor/autoload.php';

$isfor = filter_input(INPUT_POST, "isfor", FILTER_SANITIZE_STRIPPED);

if (!is_null($isfor)) {
  
    require_once __DIR__ . "/../extra/essentials.php";
    
    // Do a session check to see if the user's logged in or not.
    if (!isset($session)) {
        $session = new OMIS\Session();
        $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
    }
    
    if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
        return false;
    }
    
    require_once __DIR__ . "/osce_functions.php";
    
} else {
  
    http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
    exit();
    
}

$jsonData = [];

//  Add Students JSON
switch ($isfor) {
    
  
    /**
     * Authenticate for results deletion (DC: This should not be here, 
     * but in a dedicated authentication module)
     */
    case "authenticate":
         
         $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
         if (!$db->users->checkPassword($_SESSION['user_identifier'], $password))  {
            http_response_code(HttpStatusCode::HTTP_UNAUTHORIZED);
            exit();
         }
         break;
    /**
     * Add students to groups
     */
    case "add":
    $db->set_Convert_Html_Entities(false);
    $yearModuleKey = filter_input(INPUT_POST, "module", FILTER_SANITIZE_STRING);
    list($courseYearID, $module) = explode("::", $yearModuleKey);
    $sessionID = filter_input(INPUT_POST, "session_id", FILTER_SANITIZE_NUMBER_INT);
    $groupFilter = filter_input(INPUT_POST, "group_filter", FILTER_SANITIZE_STRING);
    $jsonData = ["data" => [], "filter" => []];
    $studentData = [];

    if ($db->exams->doesSessionExist($sessionID) && $db->courses->doesModuleExist($module, $courseYearID)) {
        
        // Update module user preset
        $currentYearModuleKey = implode("::", [
            $_SESSION['selected_year'], 
            $_SESSION['selected_module']
        ]);
        
        if ($currentYearModuleKey != $yearModuleKey) {
                       
            $courseRecord = $db->courses->getCourseYear($courseYearID);         
            $_SESSION['selected_course'] = $courseRecord['course_id'];
            $_SESSION['selected_year'] = $courseYearID;
            $_SESSION['selected_module'] = $module;
            
            $db->userPresets->update([
                'filter_course' => $courseRecord['course_id'],
                'filter_year' => $courseYearID,
                'filter_module' => $module
            ]);
            
        }

        // Get Students DB
        $students = $db->exams->getStudentsNotInGroups($sessionID, $module, $courseYearID);

        // Setup array of students to work with
        while ($student = $db->fetch_row($students)) {
            $studentData[] = [
                "student_id" => $student["student_id"],
                "candidate_number" => $student["candidate_number"],
                "forename" => $student["forename"],
                "surname" => $student["surname"]
            ];
        }

        // Get Filters
        $jsonData["filter"] = buildFilterIndex($studentData);

        // Do some filtering if requested
        if (strlen($groupFilter) > 0 && strpos($groupFilter, "::")) {
            list($filterType, $filterValue) = explode("::", $groupFilter);
            $ref = ($filterType == "fn") ? "forename" : "surname";

            foreach ($studentData as $each) {
                if (mb_strtoupper(mb_substr($each[$ref], 0, 1, "UTF-8")) == $filterValue) {
                    $jsonData["data"][] = $each;
                }
            }
        } else { // No need to Filter
            $jsonData["data"] = $studentData;
        }
    }
   
    break;
    
    
    /**
     * Get student exam results
     */
    case "exam_results":
    
    $studentID = filter_input(INPUT_POST, "student_id", FILTER_SANITIZE_STRING);
    $sessionID = filter_input(INPUT_POST, "session_id", FILTER_SANITIZE_NUMBER_INT);

    $db->set_Convert_Html_Entities(false);
    $resultRecords = $db->results->getStudentResultsData(
         $studentID, 
         $sessionID
    );
        
    foreach ($resultRecords as $formID => $formResults) {
       foreach ($formResults as $record) {
        $form = $db->forms->getForm($formID);
  
        if (empty($form)) {
           continue;
        }
            
        $examinerRecord = $db->users->getUser($record['examiner_id'], 'user_id', true);
        $examinerName = empty($examinerRecord) ? "" : 
        $examinerRecord['forename'] . " " . $examinerRecord['surname'];
                        
        $jsonData[] = [
            'examiner_id' => $record['examiner_id'],
            'examiner_name' => $examinerName,
            'absent' => $record['absent'],
            'complete' => $record['is_complete'],
            'score' => roundAndFormat($record['score']),
            'number' => $record['station_number'],
            'name' => $form['form_name']
        ];
      }
    }
    
    orderArrayColumn($jsonData, 'number', 'standard', 'ASC');
        
    break;

// Bad Request
default:
            http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
    exit();
    break;
}

header("Content-type: application/json");
try {
    $json = JSON::encode($jsonData);
} catch (Exception $ex) {
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    print_r_log($jsonData);
            http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
}

// Data to be sent
echo $json;
