<?php

 /**

 * @author David Cunningham <david.cunningham@qpercom.ie>

 * For Qpercom Ltd

 * @copyright Copyright (c) 2018, Qpercom Limited

 */

 define('_OMIS', 1);
 

 $redirectOnSessionTimeOut = true;

 include __DIR__ . '/../extra/essentials.php';


 /**

  * Page Access Check / Can User Access this Section?

  */

 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {



     $template = new \OMIS\Template("noaccess.html.twig");



     $template->render([

         'logged_in' => true,

         'root_url' => $config->base_path_url,

         'username' => $_SESSION['user_identifier'],

         'page' => \OMIS\Utilities\Request::getPageID()

     ]);

     

     return;



 }

 

 /**

  *  Redirects for Assessment Forms section

  */

if(isset($_POST['poptype'])){
    if($_POST['poptype'] == "video"){


 // move_uploaded_file($_FILES['insvideo']['tmp_name'], '../media/'.$_FILES["insvideo"]["name"]);

 $ext = pathinfo($_FILES['insvideo']['name'], PATHINFO_EXTENSION);

 $form_id = $_POST['form_id'];

 $file = $form_id.".".$ext;

            $form_id = $_POST['form_id'];

            $station_id = $_POST['station_id'];

            $session_id = $_POST['session_id'];

            $show_session = $_POST['show_session'];

            $url_data = $_POST['url_data'];

            $dept_id = $_POST['dept_id'];

            $term_data = $_POST['term_data'];



 // echo "<pre>";

 // print_r($ext); die();

 if($ext == "mp4"){

    // print_r($_POST); die();

if (!move_uploaded_file($_FILES['insvideo']['tmp_name'], '../media/'.$file)) {

          

            // echo "not uploaded";

            //  print_r($_FILES);

            //  die(); 



            $_SESSION['upload_file'] = "fail";

          
           if($url_data == "bankform"){

                header('Location: ../manage.php?page=bankform&form_id='.$form_id.'&dept='.$dept_id.'&term='.$term_data);
                die();
            }else{

            header('Location: ../manage.php?page=examform&form_id='.$form_id.'&station_id='.$station_id.'&session_id='.$session_id.'&show_session='.$show_session);

            die();
            }

            


         }else{

           

            // echo "uploaded";

            // print_r($_FILES);

            // die();

            $_SESSION['upload_file'] = "successful";



            if($url_data == "bankform"){

                header('Location: ../manage.php?page=bankform&form_id='.$form_id.'&dept='.$dept_id.'&term='.$term_data);
                die();
            }else{

            header('Location: ../manage.php?page=examform&form_id='.$form_id.'&station_id='.$station_id.'&session_id='.$session_id.'&show_session='.$show_session);

            die();
            }



         }

     }else{



            $_SESSION['file_error'] = "notmp4";

            if($url_data == "bankform"){

                header('Location: ../manage.php?page=bankform&form_id='.$form_id.'&dept='.$dept_id.'&term='.$term_data);
                die();
            }else{

            header('Location: ../manage.php?page=examform&form_id='.$form_id.'&station_id='.$station_id.'&session_id='.$session_id.'&show_session='.$show_session);

            die();
            }

     }


    }
}

 if (isset($_POST['go_forms'])) {



     // If Deleting forms

     if ($_POST['todo'] == "delete") {



         $db->forms->deleteForms($_POST['forms_check']);

         $UL = '../manage.php?page=forms_osce';



     // Design Complete

     } elseif ($_POST['todo'] == 'complete') {



         $db->forms->setFormsDesignComplete($_POST['forms_check'], true);

         $UL = '../manage.php?page=forms_osce';



     // Design Not Complete

     } elseif ($_POST['todo'] == 'notcomplete') {



         $db->forms->setFormsDesignComplete($_POST['forms_check'], false);

         $UL = '../manage.php?page=forms_osce';



     /**

      * Remove from selected term, prefix = removeterm: (11 characters)

      */

     } elseif (substr($_POST['todo'], 0, 11) == "removeterm:") {



         $selectedTerm = substr(

             trim(filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING)), 11

         );



         /*

         *  Remove forms from selected term and department 

         *  if they are not linked to results in that term & department

         */

        $doesTermExist = $db->academicterms->existsById($selectedTerm);

        $doesDeptExist = $db->departments->existsById($_SESSION['selected_dept']);



        if ($doesTermExist && $doesDeptExist) {



            $db->forms->removeFormsFromDeptTerm(

                 $_POST['forms_check'],

                 $_SESSION['selected_dept'],

                 $selectedTerm

            );



        }



        $UL = '../manage.php?page=forms_osce&mode=remove';

         

     /**

      * If attach to a term, prefix = attachterm: (11 characters)

      */

     } elseif (substr($_POST['todo'], 0, 11) == "attachterm:") {



         $term = substr(

             trim(filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING)), 11

         );



         $dept = filter_input(INPUT_POST, 'dept', FILTER_SANITIZE_STRING);



         if ($db->academicterms->existsById($term)) {



             $db->forms->attachFormsToDeptTerm($_POST['forms_check'], $dept, $term);



         }



         $UL = '../manage.php?page=forms_osce&term=' . $term;

         

     }

 

     header('Location: ' . $UL);



 // Redirects for Assessment Form Sections

 } elseif (isset($_POST['submit_sections'])) {

     

     if (!$db->results->doesFormHaveResult($_POST['form_id'])) {

 

         $sectionFeedback = (int)(isset($_POST['section_feedback']) && $_POST['section_feedback']);

         $itemFeedback = (int)(isset($_POST['item_feedback']) && $_POST['item_feedback']);

         $feedbackRequired = (int)(isset($_POST['feedback_required']) && $_POST['feedback_required']);

         $feedbackInternal = isset($_POST['internal_feedback_only']) ? $_POST['internal_feedback_only'] : 0;

         $selfAssessment = isset($_POST['self_assessment']) ? $_POST['self_assessment'] : 0;

 

         $sectionID = $db->forms->insertFormSection(

             $_POST['form_id'], 

             [

               "order" => $_POST['section_order'],

               "reSortOrder" => true,

               "title" => $_POST['section_text'],

               "competence" => $_POST['competence_type'],

               "sectionFeedback" => $sectionFeedback,

               "itemFeedback" => $itemFeedback,

               "feedbackRequired" => $feedbackRequired,

               "feedbackInternal" => $feedbackInternal,

               "selfAssessment" => $selfAssessment

             ]

         );

 

     }

 

     $ref = $_SERVER['HTTP_REFERER'];

     $pos = strrpos($ref, '#');



     if ($pos !== false) {



         $ref = substr_replace($ref, '', $pos);



     }



     header('Location: ' . $ref . '#c' . $sectionID);



 // Add sections and items from previously used forms to this form

 } elseif (isset($_POST['add_previous_items'])) {

    



     $toDo = trim($_POST['to_do']);

     $selectedSections = $_POST['prevsection'];

     $currentFormID = trim($_POST['current_form_id']);

     $previousFormID = trim($_POST['previous_form_id']);

 

     // Does previous and current form exist

     if ($db->forms->doesFormExist($previousFormID) && $db->forms->doesFormExist($currentFormID)) {





         // Does Station have a scale item

         $doesStationHaveScale = $db->forms->doesFormHaveScaleSection($currentFormID);



         // Does form have results

         if (!$db->results->doesFormHaveResult($currentFormID)) {



             // If adding new section or replacing all sections in current form

             if ($toDo == 'addnew' || $toDo == 'repall') {



                 //Delete all current form sections & items

                 if ($db->forms->deleteAllFormSections($currentFormID)) {



                     $prevSections = $db->forms->getFormSections([$previousFormID]);

                     $sectionCount = 0;



                     while ($prevSection = mysqli_fetch_assoc($prevSections)) {



                         if (in_array($prevSection['section_id'], $selectedSections, true)) {

                           

                             $insertSection = ($db->forms->doesCompetenceExist($prevSection['competence_id']) 

                                     && $db->forms->doesFormSectionExist($prevSection['section_id']));

                             

                             if ($insertSection) {

                                 

                                 // Insert form section & items

                                 $sectionID = $db->forms->insertFormSection(

                                     $currentFormID, 

                                     [

                                       "order" => ++$sectionCount,

                                       "reSortOrder" => false,

                                       "title" => $prevSection['section_text'],

                                       "competence" => $prevSection['competence_id'],

                                       "sectionFeedback" => $prevSection['section_feedback'],

                                       "itemFeedback" => $prevSection['item_feedback'],

                                       "feedbackRequired" => $prevSection['feedback_required'],

                                       "feedbackInternal" => $prevSection['internal_feedback_only'],

                                        "selfAssessment" => $prevSection['self_assessment']

                                     ]

                                 );

                                                

                                 include 'insert_items.php';



                             }



                         }



                     }



                 }



             // Add section & items to end of form

             } elseif ($toDo == 'addend') {



                 $prevSections = $db->forms->getFormSections([$previousFormID]);

                 $sectionIDArray = [];



                 while ($prevSection = mysqli_fetch_assoc($prevSections)) {



                     if (in_array($prevSection['section_id'], $selectedSections, true)) {



                         $insertSection = ($db->forms->doesCompetenceExist($prevSection['competence_id']) 

                                 && $db->forms->doesFormSectionExist($prevSection['section_id']) 

                                 && ($prevSection['competence_type'] == 'standard' 

                                 || ($prevSection['competence_type'] == 'scale' && !$doesStationHaveScale)));

                         

                         if ($insertSection) {

 

                             // Insert form section

                             $sectionID = $db->forms->insertFormSection(

                                 $currentFormID, 

                                 [

                                   "order" => null,

                                   "reSortOrder" => false,

                                   "title" => $prevSection['section_text'],

                                   "competence" => $prevSection['competence_id'],

                                   "sectionFeedback" => $prevSection['section_feedback'],

                                   "itemFeedback" => $prevSection['item_feedback'],

                                   "feedbackRequired" => $prevSection['feedback_required'],

                                   "feedbackInternal" => $prevSection['internal_feedback_only'],

                                   "selfAssessment" => $prevSection['self_assessment']

                                 ]

                             );

                             

                             $sectionIDArray[] = $sectionID;

                             include 'insert_items.php';

 

                         }



                     }



                 }



             // Add section & items to start of form

             } elseif ($toDo == 'addstart') {



                 $oldSections = $db->forms->getFormSections([$currentFormID]);

                 $prevSections = $db->forms->getFormSections([$previousFormID]);

                 $sectionCount = 0;



                 while ($prevSection = mysqli_fetch_assoc($prevSections)) {



                     if (in_array($prevSection['section_id'], $selectedSections, true)) {



                         $insertSection = ($db->forms->doesCompetenceExist($prevSection['competence_id']) 

                                 && $db->forms->doesFormSectionExist($prevSection['section_id']) 

                                 && ($prevSection['competence_type'] == 'standard' 

                                 || ($prevSection['competence_type'] == 'scale' && !$doesStationHaveScale)));

                         

                         if ($insertSection) {

 

                             // Insert form section

                             $sectionID = $db->forms->insertFormSection(

                                 $currentFormID, 

                                 [

                                   "order" => ++$sectionCount,

                                   "reSortOrder" => true,

                                   "title" => $prevSection['section_text'],

                                   "competence" => $prevSection['competence_id'],

                                   "sectionFeedback" => $prevSection['section_feedback'],

                                   "itemFeedback" => $prevSection['item_feedback'],

                                   "feedbackRequired" => $prevSection['feedback_required'],

                                   "feedbackInternal" => $prevSection['internal_feedback_only'],

                                   "selfAssessment" => $prevSection['self_assessment']

                                 ]

                             );                    

 

                             include 'insert_items.php';



                         }



                     }



                 }

 

                 // Update current form order

                 while ($oldSection = mysqli_fetch_assoc($oldSections)) {



                     $sectionCount++;

                     $db->forms->updateFormSectionOrder($oldSection['section_id'], $sectionCount);



                 }



             }



             $db->forms->reCalcFormTotal($currentFormID);



         }



     }

 

     $ref = $_SERVER['HTTP_REFERER'];

     $pos = strrpos($ref, '#');



     if ($pos !== false) {



         $ref = substr_replace($ref, '', $pos);



     }



     if ($toDo == 'addend') {



         $ref = $ref . '#c' . $sectionIDArray[0];



     }



     header('Location: ' . $ref);



 // Form options section     

 } else if (isset($_POST['form_options'])) {

     

     // Return to stations list or form (station) bank section 

     if (isset($_POST['form_options'][0]) && $_POST['form_options'][0] == "return") {



         // If session_id and station_id is set return to stations page 

         if (isset($_POST['session_id']) && isset($_POST['station_id'])) {



             $UL = '../manage.php?page=stations_osce&session_id='

                 . $_POST['session_id'] . '&show_session=' . $_POST['show_session'];



         } else {



             // Otherwise return to form (station) bank section

             $UL = '../manage.php?page=forms_osce&dept='

                 . $_POST['dept_id'] . '#X' . $_POST['form_id'];



         }

 

         header('Location: ' . $UL);



     // Remove formatting from scoresheet items

     } elseif (isset($_POST['form_options'][1]) && in_array($_POST['form_options'][1], ['unformat', 'Updating'])) {

         

         $formID = trim($_POST['form_id']);

         $db->forms->clearItemsFormat($formID);

         $ref = $_SERVER['HTTP_REFERER'];

         $pos = strrpos($ref, '#');



         if ($pos !== false) {



             $ref = substr_replace($ref, '', $pos);



         }



         header('Location: ' . $ref);



     } else {



         //If none of the above return to index page 

         header('Location: ../');



     }

 

 } else {

   

     //If none of the above return to index page    

     header('Location: ../');

     

 }

 