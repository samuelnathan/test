<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

use \OMIS\Template as Template;

define("_OMIS", 1);

// ID of item to edit
$itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
$sessionID = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_NUMBER_INT);

// Item ID is set then include database connection 
if(!is_null($itemID)) {
    include __DIR__ .'/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ .'/../extra/noaccess.php';
}

// Competency framework feature enabled
$frameworkEnabled = $db->features->enabled('competency-framework', $_SESSION['user_role']);

// Get item record
$itemRecord = $db->forms->getItem($itemID);

?>
<div class="redborderv1" id="itemeditdiv-<?=$itemID?>">

<?php

// Item exists then grab what's required
if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {
   
    $itemData = $db->fetch_row($itemRecord);
    $order = $itemData["item_order"];
    $itemID = $itemData["item_id"];
    $description = $itemData["text"];
    $type = $itemData["type"];
	
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formID = $db->forms->getSectionFormID($sectionID);
    $formHasResult = $db->results->doesFormHaveResult($formID);
    $highestValue = $db->forms->getHighestItemNumber($sectionID);
	
    if ($highestValue == "") {
       $highestValue = 1;
    }

    ?>
    <div class="fl">
      <select name="item_order-<?=$itemID?>" id="item_order-<?=$itemID?>" class="sorder custom-select custom-select-sm" title="Item order">
        <?php 
            
             // Render order dropdown
            for ($i=1; $i<=$highestValue; $i++) {
                $selectedAttr = ($i == $order) ? ' selected="selected"': '';
               ?>
                <option value="<?=$i?>"<?=$selectedAttr?>><?=$i?></option>
               <?php
            }
            
        ?>
      </select>
    </div>   
    <?php
    
   /**
    *  Render competencies selector template
    */
    $competencyFrameworkID = $config->competency_frameworks['competence_framework_id'];
    $frameworkLevelID = $db->competencies->getFrameworkLevelID($competencyFrameworkID);
    $competencies = $db->competencies->getList($competencyFrameworkID, $frameworkLevelID, 1);
    $selectedCompetencies = $db->competencies->getItemCompetencies($itemID);
    
    foreach ($competencies as &$competence) {
        $competenceID = $competence['id'];
        
        $descendants = $db->competencies->getList($competencyFrameworkID, $competenceID, 2);
        foreach ($descendants as $descendant) {
          $competence['descendants'][] = [
            'id' => $descendant['id'],
            'name' => $descendant['name']
          ];
        
        }
    }
    
    // Display competency sub levels yes|no
    $displaySubLevels = $config->competency_frameworks['display_sublevels'];
    $disabledInfo = $config->feature_disabled_message;
    
    // Template data
    $templateData = [
       'competencies' => $competencies,
       'display_sublevels' => $displaySubLevels,
       'selected' => $selectedCompetencies,
       'framework_enabled' => $frameworkEnabled,
       'framework_disabled_info' => $disabledInfo
    ];
    
    $template = new Template('item_competencies_selector.html.twig');
    $template->render($templateData);

  ?><div class="qitem"><?php

    // Self Assessments Found
    $selfAssessment = $db->selfAssessments->isSectionSelfAssessment($sectionID);
    $sessionExists = $db->exams->doesSessionExist($sessionID); 

    // Self assessment item
    if ($selfAssessment && $sessionExists) {

        ?><div id="no-selfassess" class="itemtxtdiv"><?php

        $question = $db->selfAssessments->getQuestion(
            $db->exams->getSessionExamID($sessionID),
            $order
        );       

        if (empty($question)) {

            echo "No self assessment question found for this item (matched by order number). Please import into this " 
            . gettext('exam') . " the questions, responses and scores for all " . gettext('students');             

        } else {

            ?><label class="selfassess">Self Assessment Question: </label>
             <span id="self-assess-<?=$itemID?>" 
                   class="item-assessment-question" 
                   title="Self Assessment Question">
                   <?=$question['question']
             ?></span>
           <?php

        }

    } else {    

        ?><div class="itemtxtdiv">
        <div id="load-editor" class="red">Loading Editor...</div>
        <textarea id="item_text-<?=$itemID?>" cols="50" rows="4" class="itemtxt"
                    style="visibility: hidden"><?=$description?></textarea><?php

    }

    ?></div>
    <br class="clearv2"/>
   </div>
   <div class="fc"><?php

   // Item type preview image links
   $radioRel = "&lt;img src=assets/images/radio.png /&gt;";
   $textboxRel = "&lt;img src=assets/images/textbox.png /&gt";
   $sliderRel = "&lt;img src=assets/images/slider.png /&gt";
   $checkboxRel = "";
   $disabledAttr = ($formHasResult) ? ' disabled="disabled"': '';
   $radioChecked = (in_array($type, ["radio", "radiotext"])) ? ' checked="checked"' : '';
   $textboxChecked = ($type == "text") ? ' checked="checked"' : '';
   $sliderChecked = ($type == "slider") ? ' checked="checked"' : '';
   $checkboxChecked = ($type == "checkbox") ? ' checked="checked"' : '';

   ?>       
   <label for="item_type_radio-<?=$itemID?>" class="labeltext3 hoverimage" title="Preview" rel="<?=$radioRel?>">Likert Scale</label><?php
   ?><input type="radio" class="item-type" name="item_type" value="radio" id="item_type_radio-<?=$itemID?>"<?=$radioChecked?> onclick="changeItemTypeEdit(this)"<?=$disabledAttr?>/>
   <label for="item_type_text-<?=$itemID?>" class="labeltext3 hoverimage" title="Preview" rel="<?=$textboxRel?>">Enter Number</label><?php
   ?><input type="radio" class="item-type" name="item_type" value="text" id="item_type_text-<?=$itemID?>"<?=$textboxChecked?> onclick="changeItemTypeEdit(this)"<?=$disabledAttr?>/>
   <label for="item_type_slider-<?=$itemID?>" class="labeltext3 hoverimage" title="Preview" rel="<?=$sliderRel?>">Range Slider</label><?php
   ?><input type="radio" class="item-type" name="item_type" value="slider" id="item_type_slider-<?=$itemID?>"<?=$sliderChecked?> onclick="changeItemTypeEdit(this)"<?=$disabledAttr?>/>
   <label for="item_type_checkbox-<?=$itemID?>" class="labeltext3 hoverimage" title="Preview" rel="<?=$checkboxRel?>">Checkbox (Indicator Only)</label><?php
   ?><input type="radio" class="item-type" name="item_type" value="checkbox" id="item_type_checkbox-<?=$itemID?>"<?=$checkboxChecked?> onclick="changeItemTypeEdit(this)"<?=$disabledAttr?>/>
   <div id="qansoptions-<?=$itemID?>" class="ansoptioncreatediv">
	 
   <?php 

    switch ($type) {
       case "text":
           include "edit_textbox_option.php";
           break;
       case "slider":
           include "edit_slider_option.php";
           break;
       case "checkbox":
           include "checkbox_option_edit.php";
           break;
       default:
           include "edit_radio_options.php";
           break;
    }

    ?>
    </div>
    <div class="bpan" id="buttonpanel-<?=$itemID?>">
        <input type="button" value="cancel" id="cancel_edit_item" class="btn btn-outline-danger btn-sm"/>&nbsp;
        <input type="button"  value="update Item" id="update_item" class="btn btn-success btn-sm"/>
    </div>
  </div>
<?php
} else {
   ?>
    <span class="boldred">
        This item does not exist in this section, please refresh page [F5 Key].
    </span>
   <?php
}
?></div>
