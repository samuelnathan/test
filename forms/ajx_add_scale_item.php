<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
define('_OMIS', 1);

if (isset($_POST['section_id'])) {
    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$sectionID = filter_input(INPUT_POST, 'section_id', FILTER_VALIDATE_INT);
$language = $config->localization['language'];
$ratingScaleDefaultsDB = $db->forms->getRatingScaleDefault($language);

?><div class="redborderv2" id="itemadddiv"><?php

if (($sectionID > 0) && ($db->forms->doesFormSectionExist($sectionID))) {
    
 ?><div class="qitem"><?php
   ?><div class="itemtxtdiv2"><div id="load-editor" class="red">Loading Editor...</div><?php 
     ?><textarea id="item_text" cols="50" rows="4" class="itemtxt" style="visibility: hidden"><?php 
        echo $ratingScaleDefaultsDB['rcd_text']; 
     ?></textarea><?php 
   ?></div>
  <br class="clearv2"/><?php 
  ?></div>
   <div id="scale-option-types" class="fc">
 <?php
    
    // Get scale types from the database
    $ratingScaleTypes = $db->forms->getRatingScaleTypes();
   
    // What will be the default GRS type selected? Config file or first element of the scale types list  
    if (strlen($config->station_defaults['grs_scale']) > 0) {
        $setScaleType = $config->station_defaults['grs_scale'];
    } else {
        $setScaleType = $ratingScaleTypes[0]['scale_type_id'];
    }

    // Loop through all of the Global Rating Scale Types
    foreach ($ratingScaleTypes as $ratingScaleType) {
    
      $scaleTypeID = $ratingScaleType['scale_type_id'];
      $scaleTypeName = $ratingScaleType['scale_type_name'];
      $scaleTypeColour = $ratingScaleType['scale_type_colour'];
             
      // Scale Selected
      $scaleSelected = ($scaleTypeID == $setScaleType) ? " checked='checked'" : "";
      
     ?><label for="type<?=$scaleTypeID?>" class="labeltext <?=$scaleTypeColour?>"><?=$scaleTypeName?></label><?php
     ?><input type="radio" class="scale-types-radio" value="<?=$scaleTypeID?>" name="item_type" id="type<?=$scaleTypeID?>"<?=$scaleSelected?>/><?php
     
   }
   ?><div id="qansoptions" class="ansoptioncreatediv"><?php
    include "scale_type.php";
   ?></div>
    <div class="bpan" id="buttonpanel">
        <input type="button" class="btn btn-outline-danger btn-sm" value="cancel" id="cancel_new_scale"/>
        <input type="button" class="btn btn-success btn-sm" value="add Item" id="submit_new_scale"/>
    </div>
  </div>
<?php
} else {
   ?>
    <div class="option-panel-error">
       This section does not exist for this form, please refresh page [F5 Key]
    </div>
   <?php
}
?></div>