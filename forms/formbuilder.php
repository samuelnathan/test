<?php

 /**

  * @authors David Cunningham <david.cunningham@qpercom.ie>

  *          Kelvin Nunn <kelvin.nunn@qpercom.ie>

  * For Qpercom Ltd

  * @copyright Copyright (c) 2019, Qpercom Limited

  */



 // Include Configuration & Templating

 use \OMIS\Auth\Role as Role;

 use \OMIS\Database\CoreDB as CoreDB;

 use \OMIS\Config as Config;

 use \OMIS\Template as Template;



 define('INCLUDE_REST_STATIONS', true);



 // Critical Session Check

 $session = new OMIS\Session;

 $session->check();



 // Page Access Check / Can User Access this Section?

 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())

     return false;



 require_once __DIR__ . "/../extra/helper.php";



 // Get Form ID, Session ID, Station ID and Show Session URL Params

 $formID = filter_input(INPUT_GET, 'form_id', FILTER_SANITIZE_NUMBER_INT);

 $sessionID = filter_input(INPUT_GET, 'session_id', FILTER_SANITIZE_NUMBER_INT);

 $stationID = filter_input(INPUT_GET, 'station_id', FILTER_SANITIZE_NUMBER_INT);

 $showSession = filter_input(INPUT_GET, 'show_session', FILTER_SANITIZE_NUMBER_INT);

 $dept = filter_input(INPUT_GET, 'dept', FILTER_SANITIZE_STRING);

 $term = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);

 $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);



 $forms = [];

 if ($page === 'examform') {

     $stationsGrouped = array_values($db->stations->getStations([$sessionID], true, 'station_number', 'form_name', false));

     $stations = [];

     foreach ($stationsGrouped as $stationGroup)

         foreach ($stationGroup as $station)

             $stations[] = $station;

     $forms = array_column($stations, 'form_id');

 } else if ($page === 'bankform') {

     $formRecords = $db->forms->getForms($term, $dept, 'form_data.created_at', 'ASC', true);

     $forms = array_column($formRecords, 'form_id');

 }



 $arrayHelp = new \OMIS\Utilities\ArrayHelp($forms);



 // Previous and next session urls

 list($previousForm, $nextForm) = $arrayHelp->previousAndNext($formID);

 $previousUrl = empty($previousForm) ? "" : "manage.php?"

     . http_build_query([

         'page' => $page,

         'form_id' => $previousForm,

         'station_id' => $page === 'examform' ? $stationID : null,

         'session_id' => $page === 'examform' ? $sessionID : null,

         'show_session' => $page === 'examform' ? $showSession : null,

         'dept' => $page === 'bankform' ? $dept : null,

         'term' => $page === 'bankform' ? $term : null

     ]);



 $nextUrl = empty($nextForm) ? "" : "manage.php?"

     . http_build_query([

         'page' => $page,

         'form_id' => $nextForm,

         'station_id' => $page === 'examform' ? $stationID : null,

         'session_id' => $page === 'examform' ? $sessionID : null,

         'show_session' => $page === 'examform' ? $showSession : null,

         'dept' => $page === 'bankform' ? $dept : null,

         'term' => $page === 'bankform' ? $term : null

     ]);





 // get exam_id this form is assigned to from sessionID if present

 $examID = $db->exams->getSessionExamID($sessionID);



 // Does form exist ?

 if ($db->forms->doesFormExist($formID)) {



    // Define arrays and variables

    $sectionsItems = $itemsOptions = $allForms = [];

    $totalItemCount = $itemWeightedPossible = $formPossible = 0;

    $formHasItems = false;



    // Get Term

    $sterm = selectedTerm();



    // Get Terms

    $terms = $db->academicterms->getAllTerms();



    // Does form have results ?

    $formHasResult = $db->results->doesFormHaveResult($formID);



    // Does form have scale section ?

    $formHasScaleSection = $db->forms->doesFormHaveScaleSection($formID);



    // Get form information

    $formDB = $db->forms->getForm($formID);



    // Department Info (for current selected department)

    $currentDeptID = $_SESSION['selected_dept'];



    // Get form sections

    $formSectionsDB = $db->forms->getFormSections([$formID], true);



    // Get total section count

    $totalSectionCount = count($formSectionsDB);



    // Is feedback feature enabled ?

    $feedbackOptions = [

        Config::FEEDBACK_NONE => "No",

        Config::FEEDBACK_ALL => "Yes",

        Config::FEEDBACK_GRS_FAIL => "GRS Fail",

        Config::FEEDBACK_GRS_BORDERLINE => "GRS Borderline",

        Config::FEEDBACK_GRS_BORDERLINE_AND_FAIL => "GRS Fail &amp; Borderline",

        Config::FEEDBACK_FLAG_OPTION => "Flagged Option Chosen"

    ];



    $feedbackGRS = [

        Config::FEEDBACK_GRS_FAIL,

        Config::FEEDBACK_GRS_BORDERLINE,

        Config::FEEDBACK_GRS_BORDERLINE_AND_FAIL

    ];



    $feedbackOther = [

        Config::FEEDBACK_FLAG_OPTION

    ];



    /* Prepare list of sections to items and items to options into arrays */

    foreach ($formSectionsDB as &$section) {



        $sectionID = $section['section_id'];





        $sectionItemsDB = $db->forms->getSectionItems([$sectionID], true);

        $totalItemCount += count($sectionItemsDB);



        // Items to Options relationship

        $flaggedSection = false;

        $mixedValueDirection = false;

        $itemValueDirection = NULL;

        foreach ($sectionItemsDB as &$item) {



            $itemID = $item['item_id'];



            // Add options to item

            $options = $db->forms->getItemOptions($itemID, true);



            $itemsOptions[$itemID]['options'] = $options;



            // Item is flagged

            $flaggedItem = max(array_column($options, 'flag'));

            $item['flagged'] = $flaggedItem;



            if ($flaggedItem) {



                $flaggedSection = true;



            }



            // Item self assessment question

            if ($section['self_assessment'] && !empty($examID)) {



                $questionRecord = $db->selfAssessments->getQuestion($examID, $item['item_order']);

                $item['question'] = $questionRecord['question'];



            }



            // Form possible

            $formPossible += max($item['lowest_value'], $item['highest_value']);



            // DETECT items with options of opposing directions in this section

            if (in_array($item['type'], ['radio', 'slider']) && count($options) > 1)  {



                $radioLTR = ($item['type'] == 'radio' && $options[0]['option_value'] < end($options)['option_value']);

                $sliderLTR = ($item['type'] == 'slider' && $item['lowest_value'] < $item['highest_value']);

                $newValueDirection = ($radioLTR || $sliderLTR) ? 'ltr' : 'rtl';

                if ($itemValueDirection != NULL && $itemValueDirection != $newValueDirection) {

                    $mixedValueDirection = true;

                }



                $itemValueDirection = $newValueDirection;



            }



            // Form has items (YES)

            $formHasItems = true;

        }



        $section['mixedValueDirection'] = $mixedValueDirection;

        $section['flagged'] = $flaggedSection;

        $section['items'] = $sectionItemsDB;

        $sectionsItems[$sectionID] = $section;



    }



    // Array of section IDs

    $sectionIDs = array_keys($sectionsItems);



    /* Top Section Div With Form Options */

    ?><div class="topmain nmb"><div class="row"><div class="col-auto"><?php

 

     // Station Description/case notes Field enabled/disabled

     $sdFeatureEnabled = $db->features->enabled('station-notes', $_SESSION['user_role']);



     // System wide tags

     $tagRecords = $db->forms->getTags();

     $systemWideTags = array_map(function($tag) {

         return $tag['name'];

     }, $tagRecords);





     // Clone status (If Any)

     if (isset($_SESSION['form_cloned'])) {



       $cloneStatus = ($_SESSION['form_cloned'] == "yes");



       unset($_SESSION['form_cloned']);



     } else {



       $cloneStatus = null;



     }

$file_path = "media/".$formDB['form_id'].".mp4";



if(file_exists($file_path)){

    $file = "file found";

}else{

    $file = "file not found";

}

// print_r($_SESSION); die();

if(isset($_SESSION['upload_file']) AND $_SESSION['upload_file'] == "successful"){

    $file_upload = "success";

}



if(isset($_SESSION['file_error']) AND $_SESSION['file_error'] == "notmp4"){

    $error = "notmp4";

}

unset($_SESSION["upload_file"]);

unset($_SESSION["file_error"]);

     // Include Form Options Panel 1 (TOP LEFT)

     // Panel to edit current form details

     // Template data
$url_data = $_GET['page'];
if($_GET['page'] == "bankform"){
    $return_url = "manage.php?page=forms_osce";
}elseif($_GET['page'] == "examform"){
    $return_url = "manage.php?page=stations_osce&session_id=".$_GET['session_id']."&exam_id=$examID&show_session=".$_GET['show_session'];
}
 $rand = rand();

     $data = [

        'location' => 'top',

        'case_notes_feature' => $sdFeatureEnabled,

        'feature_disabled_message' => $config->feature_disabled_message,

        'total_item_count' => $totalItemCount,

        'form_has_items' => $formHasItems,

        'form_data' => $formDB,

        'author' => $db->users->getUser($formDB['author'], 'user_id', true),

        'tags' => $db->forms->getFormTags($formDB['form_id']),

        'hasContent' => $db->forms->hasContent($formDB['form_id']),

        'systemWideTags' => $systemWideTags,

        'current_dept_id' => $currentDeptID,

        'department_name' =>  $db->departments->getDepartmentName($currentDeptID),

        'academic_terms' => $terms,

        'current_term' => $sterm,

        'session_id' => $sessionID,

        'station_id' => $stationID,

        'show_session' => $showSession,

        'cloned' => $cloneStatus,

        'file' => $file,

        'file_upload' => $file_upload,

        'error' => $error,

        'url_data' => $url_data,

        'rand' => $rand,

         /**

          * Jump to group records previous and next

          */

         "jumpRecords" => [

             "customEvents" => true,

             "boxWidth" => 'auto',

             "returnLabel" => "return",

             "previousUrl" => $previousUrl,

             "nextUrl" => $nextUrl,

             "return_url" => $return_url,

             'noRecordLabel' => true

         ]

     ];



     // Render template

     $template = new Template("form_options_panel_1.html.twig");

     $template->render($data);



     /* Prepare data for Option Panel 2 Template */

     $competenciesDB = $db->forms->getCompetencies(null, true);



     // Get all departments with forms attached in current academic term

     $departmentList = $db->forms->getDepartmentsWithForms($sterm, true);



     // What departments user has access to based on his/her role

     if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {



        $filterDepts = [];



     } else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {



        $adminSchools = $db->schools->getAdminSchools(

           $_SESSION['user_identifier']

        );



        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));

        $filterDepts = array_unique(array_column(

            $db->departments->getDepartments(null, true, $schoolIDs),

            'dept_id'

        ));



     } else {



        $filterDepts = array_column(

           $db->departments->getExaminerDepartments(

             $_SESSION['user_identifier'], null, true

           ),

           'dept_id'

        );



     }



     foreach ($departmentList as $deptId => $department) {



         // Exclude examiner or station manager from certain departments

         if (count($filterDepts) > 0 && !in_array($deptId, $filterDepts)) {

            continue;

         }



         // Pull list of forms attached to this department from the database

         setA($allForms, $deptId, [

            'department' => $department['dept_name'],

            'forms' => []

         ]);



         $formRecords = $db->forms->getForms(

             $sterm, $deptId,

             'form_name', 'ASC', false, true

         );



         // Group forms by tags, exclude current assessment

         while ($each = $db->fetch_row($formRecords)) {

             if ($formID != $each['form_id']) {

                $tag = $each['name'];

                $allForms[$deptId]['forms'][$tag][] = $each;

             }

         }



     }



     // Get current exam if any

     $examID = !is_null($sessionID) ?

             $db->exams->getSessionExamID($sessionID) : null;





     // Get linked exams

     $linkedExams = $formHasResult ?

        CoreDB::into_array($db->forms->getLinkedExams($formID, $examID)) : [];



     ?></div><div id="options2-nav" class="col-lg-5 col-sm-6 col-12"><?php

     // Render template

     // Include Form Options Panel 2 (TOP RIGHT)

     // Panel to add form section "competency"

     // Template data

     $optionsPanel2Template = new Template("form_options_panel_2.html.twig");

     $optionsPanel2Template->render([

        'competencies' => $competenciesDB,

        'form_id' => $formID,

        'form_has_results' => $formHasResult,

        'current_exam' => $examID,

        'linked_exams' => $linkedExams,

        'all_forms' => $allForms,

        'form_has_scale_section' => $formHasScaleSection,

        'total_section_count' => $totalSectionCount,

        'feedback_compulsory_default' => $config->station_defaults['compulsory_feedback'],

        'feedback_options' => $feedbackOptions,

        'feedback_grs' => $feedbackGRS,

        'feedback_other' => $feedbackOther,

        'self_assess' => $db->selfAssessments->existsByExam($examID),

        'self_assess_exists' => $db->selfAssessments->scoresheetHasSelfAssessment($formID),

        'academic_terms' => $terms,

        'current_term' => $sterm,

     ]);



    ?></div>

    <div class="topmain-clear"></div>

 </div>

 </div><?php





 /* Main Content Div */

 ?><div class="contentdiv"><?php



 /* Render Form Including Sections and Items */

 if ($totalSectionCount > 0) {

  

  // Form Sections & Items     

  ?><form id="assessform_sections_form" method="post" action="forms/forms_redirect.php">

   <table id="section-items" class="clear">

   <caption><a name="b"></a></caption><?php



   $sectionIndex = 0;



   // Loop through form sectons

   foreach ($sectionsItems as $sectionID => $section) {



       $sectionIndex++;

       $sectionTitleData = [];

       $competenceDescription = $section['competence_desc'];

       $additionalTitleInfo = $section['section_text'];

       $competenceType = $section['competence_type'];

       $items= $section['items'];



       foreach ($items as $itemDetail) {



           $itemWeightedPossible = $itemWeightedPossible + ($itemDetail['weighted_value'] * $itemDetail['highest_value']);



       }



       // Scale item definitions

       $scaleItem = null;

       $sectionHasScaleItem = false;



     // Add competence description to section title

     if (strlen($competenceDescription) > 0) {



         $sectionTitleData[] = $competenceDescription;



     }



     // If we have a scale section then add scale type to section title

     if ($competenceType == "scale" && $db->forms->doesSectionHaveScaleItem($sectionID)) {



         $scaleDB = $db->forms->getSectionScaleItem($sectionID);

         $scaleItem = $db->fetch_row($scaleDB);

         $ratingScaleTemplate = $db->forms->getRatingScaleTypes($scaleItem['grs']);

         $sectionTitleData[] = $ratingScaleTemplate['scale_type_name'];

         $sectionHasScaleItem = true;



     }



     // Add additional title info if it exists

     if (strlen($additionalTitleInfo) > 0) {



         $sectionTitleData[] = $additionalTitleInfo;



     }

     // Render Section Title Template

     $sectionTitleTemplate = new Template("section_title.html.twig");

     $sectionTitleTemplate->render([

         'section' => $section,

         'section_title' => implode(" - ", $sectionTitleData),

         'form_has_result' => $formHasResult

     ]);



     // Standard Section

     if ($competenceType == "standard") {



         // Get all items competencies

         $itemsCompetencies = $db->competencies->getSectionItemCompetencies($sectionID);





         // Competency framework feature enabled

         $frameworkEnabled = $db->features->enabled('competency-framework', $_SESSION['user_role']);



         // Render template

         $sectionItemsTemplate = new Template("section_items.html.twig");



         $sectionWeighted = ($config->exam_defaults['item_weighting'] && $db->forms->hasWeightedItems($formID, $sectionID));



         $sectionItemsTemplate->render([

             'sections_items' => $sectionsItems,

             'section_has_flag' => $section['flagged'],

             'section_has_weighting' => $sectionWeighted,

             'items_competencies' => $itemsCompetencies,

             'items_options' => $itemsOptions,

             'current_section_id' => $sectionID,

             'form_has_results' => $formHasResult,

             'form_has_items' => $formHasItems,

             'framework_enabled' => $frameworkEnabled,

         ]);



     } else {

         // Scale Section

         // Render template

         $sectionScaleItemTemplate = new Template("section_scale_item.html.twig");

         $sectionScaleItemTemplate->render([

             'scale_item' => $scaleItem,

             'items_options' => $itemsOptions,

             'current_section_id' => $sectionID,

             'form_has_results' => $formHasResult,

             'form_has_items' => $formHasItems,

             'section_has_scale_item' => $sectionHasScaleItem,

         ]);

     }



     // Render feedback bar option

     $sectionFeedbackBar = new Template("section_feedback.html.twig");

     $sectionFeedbackBar->render([

         'section_id' => $sectionID,

         'form_has_result' => $formHasResult,

         'section_feedback' => $section['section_feedback'],

         'item_feedback' => $section['item_feedback'],

         'feedback_required' => $section['feedback_required'],

         'internal_feedback_only' => $section['internal_feedback_only'],

         'current_section_index' => $sectionIndex,

         'all_section_ids' => $sectionIDs,

         'total_section_count' => $totalSectionCount

     ]);



   }

     ?></table>

  </form><?php



 } else {



   // No form sections table

   ?>

    <table class="no-sections-table table-border"><caption>&nbsp;</caption>

     <tr>

      <td><div class="errordivnofloat">No <?=gettext('sections')?> exist for this <?=gettext('form')?>,

              please add <?=gettext('sections')?> from the above options</div></td>

     </tr>

   </table>

   <?php



 }





 // Render bottom option panel and display total section score

 ?><div class="optsba"><?php 



     // If form has items then display the marks

     if ($formHasItems) {



         ?><br><div class="divtotal">Total <span><?=$formPossible;

         ?></span> <?=ngettext('Mark', 'Marks', $formPossible) ?></div>

         <div></div><?php



         if ($config->exam_defaults['item_weighting'] && $db->forms->hasWeightedItems($formID)) {



             ?><div class="divtotal">Total Weighted <span><?=$itemWeightedPossible;

             ?></span> <?=ngettext('Mark', 'Marks', $itemWeightedPossible) ?></div><?php



         }



     }



     ?></div>

    <br class="clearv2"/>

    <div id="form-creator-buffer">&nbsp;</div>

  </div><?php



 } else {



     ?><div class="errordiv">This <?=gettext('form')?> is not known or does not exist<br/>

       <a class="normal" href="manage.php?page=forms_osce">select assessment <?=gettext('form')?></a>

      </div>

     <?php



 }

