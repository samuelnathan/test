<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/06/2011
 * © 2012 Qpercom Limited.  All rights reserved.
 */
define('_OMIS', 1);

if (isset($_POST['section_id']) && isset($_POST['to_del'])) {
    include __DIR__ . '/../extra/essentials.php';
    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$to_del = $_POST['to_del'];
$section_id = trim($_POST['section_id']); //get all request values    

if (!$db->results->doesFormHaveResult($db->forms->getSectionFormID($section_id))) {
    $db->forms->deleteItems($to_del, $section_id);
}
