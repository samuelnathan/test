<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

if (!defined('_OMIS')) {
    define('_OMIS', 1);
}

$displayoptions = false;
if (filter_has_var(INPUT_POST, 'isajaxcall')) {
    include __DIR__ . '/../extra/essentials.php';

    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formHasResult = $db->results->doesFormHaveResult($db->forms->getSectionFormID($sectionID));
    $itemRecord = $db->forms->getItem($itemID);
} else {
    #Critical Session Check
    $session = new OMIS\Session;
    $session->check();
}

mysqli_data_seek($itemRecord, 0);

if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {
    $itemData = $db->fetch_row($itemRecord);
    $type = $itemData['type'];
    $weightedValue = $itemData['weighted_value'];
    if ($type == "radio" || $type == "radiotext") {
        $ress = $db->forms->getItemOptions($itemID);
        $displayoptions = true;
        $numofrows = mysqli_num_rows($ress);
    } else {
        $numofrows = '';
    }
    ?>
<div class="optsdiv">
<label for="numradiobuttons-<?=$itemID?>" class="labeltexttitle">Number of Values&nbsp;</label>
    <input class="form-control w-auto form-control-sm d-inline-block" type="text" id="numradiobuttons-<?=$itemID?>" maxlength="2" size="2" onkeyup="renderRadioOptionsEdit(this)" value="<?=$numofrows?>" <?php if ($formHasResult) { ?>disabled="disabled"<?php } ?> />

    <label for="isnumval-<?=$itemID?>" title="If left unchecked scores will have a weighting value = 0 applied." class="lbnumval">Grade this item using values below</label>
    <input type="checkbox" id="isnumval-<?=$itemID?>" class="cbscore" <?php if ($type == "radio" || $type == "slider" || $type == "text"){ ?>checked="checked"<?php } ?>  <?php if($formHasResult){ ?>disabled="disabled"<?php } ?>/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="flip btn btn-sm btn-warning" id="flip-<?=$itemID?>" value="flip options" <?php if($formHasResult){ ?>disabled="disabled"<?php } ?> />


 <?php if($config->exam_defaults['item_weighting'] == true){
        include 'item_weighting.php';

    }
?>
</div>
<div id="radiobuttonsdiv-<?=$itemID?>" class="radcont">
<?php
  if ($displayoptions) {
     $i = 0;
     while ($rw = $db->fetch_row($ress)) {
        $i++;

        // Render option fields
        $template = new OMIS\Template("option_fields.html.twig");
        $template->render([
            "index" => $i,
            "id" => $itemID,
            "value" => $rw['option_value'],
            "descriptor" => $rw['descriptor'],
            "new_line" => ($i % 3 == 0),
            "value_readonly" => $formHasResult,
            "flag_enabled" => $rw['flag']
        ]);

     }

  // Additional Div to push everything down
  ?>
   <br class="clearv2"/>
  <?php
 }
?>
</div>
<?php
} else {
?>
<br/>
<span class="boldred">&nbsp;&nbsp; This item does not exist in this section, please refresh page [F5]. </span>
<br/>
    <?php
}
