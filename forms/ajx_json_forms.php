<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define("_OMIS", 1);
 ob_start();

 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;

 include_once __DIR__ . '/../vendor/autoload.php';

 $mode = filter_input(INPUT_POST, "mode", FILTER_SANITIZE_STRIPPED);

 if (!is_null($mode)) {

     require_once __DIR__ . "/../extra/essentials.php";

     // Do a session check to see if the user's logged in or not.
     if (!isset($session)) {

         $session = new OMIS\Session();
         $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

     }

     if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

         http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
         exit();

     }

 } else {

     http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
     exit();

 }

 switch ($mode) {

    // Add form record
    case "submit":

    $deptID = filter_input(INPUT_POST, 'sd', FILTER_SANITIZE_STRING);
    $formName = filter_input(INPUT_POST, 'sn', FILTER_SANITIZE_STRING);
    $tagString = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);
    $term = $_SESSION['cterm'];

    // Department or term does not exist
    if (!$db->departments->existsById($deptID) || !$db->academicterms->existsById($term)) {

        http_response_code(HttpStatusCode::HTTP_NOT_FOUND);
        exit();

    }

    // Form name already in use
    $data = $db->forms->nameTaken($formName, $deptID, $term);

    if ($data['exist']) {

        http_response_code(HttpStatusCode::HTTP_GONE);
        exit();

    }

    // Insert form
    $insertForm = $db->forms->insertForm(
        $formName,
        $_SESSION['user_identifier'],
        $deptID,
        $term
    );

    // Insert tags
    list($inserted, $formID) = $insertForm;

    if ($inserted) {

        $tags = (!is_null($tagString) && strlen($tagString) > 0)
            ? explode(',', $tagString) : [];

        $db->forms->linkFormTags(
            $formID,
            $tags
        );

    }

     break;

 // Bad Request
 default:

     http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
     break;

 }
