<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 12/02/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */

/*
 *  To be templated [26/04/2015]
 */

if (!defined('_OMIS')) {
    define('_OMIS', 1);
}

if (isset($_POST['isajaxcall'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $itemID = iS($_POST, 'item_id');
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    // result exists?
    $formHasResult = $db->results->doesFormHaveResult($db->forms->getSectionFormID($sectionID));
    $itemRecord = $db->forms->getItem($itemID);
} else {
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
}

mysqli_data_seek($itemRecord, 0);

if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {
    $itemData = $db->fetch_row($itemRecord);
    $highval = (float)$itemData['highest_value'];
    $lowval = (float)$itemData['lowest_value'];
    $weightedValue = $itemData['weighted_value'];
    $itemOptions = $db->fetch_row($db->forms->getItemOptions($itemID));


    if($config->exam_defaults['item_weighting'] == true){
        include 'item_weighting.php';

    }
?>

<div class="indivopts">
  <input type="hidden" id="descriptor-<?=$itemID?>" value=""/>
  <label for="text_rangeone-<?=$itemID?>" class="tranlb">Range&nbsp;&nbsp;</label>
  <input type="text" id="text_rangeone-<?=$itemID?>" size="2" maxlength="3" onkeyup="ValidateNumericV2(this)" value="<?=$lowval?>" <?php if($formHasResult){ ?>disabled="disabled"<?php } ?>/>
  <label for="text_rangetwo-<?=$itemID?>" class="ltxt"> to </label>
  <input type="text" id="text_rangetwo-<?=$itemID?>" size="2" maxlength="3" onkeyup="ValidateNumericV2(this)" value="<?=$highval?>" <?php if($formHasResult){ ?>disabled="disabled"<?php } ?>/>
</div>
<?php
} else {
 ?>
<br/>
<span class="boldred">&nbsp;&nbsp; This item does not exist in this section, please refresh page [F5 Key]. </span>
<br/><?php
} ?>
