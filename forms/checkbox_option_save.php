<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define('_OMIS', 1);

 $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);

 if (!is_null($id)) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
 } else {
    include __DIR__ . '/../extra/noaccess.php';
 }
 
 $mode = filter_input(INPUT_POST, 'mode');
 $order = filter_input(INPUT_POST, 'order');
 $text = filter_input(INPUT_POST, 'text');
 $unCheckedValue = filter_input(INPUT_POST, 'unchecked');
 $checkedValue = filter_input(INPUT_POST, 'checked');
 $descriptor = filter_input(INPUT_POST, 'descriptor');

 $competencies = filter_input(INPUT_POST, 'competencies', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

 /**
  * Add new checkbox item mode
  */
 if ($mode == "add") { 

   // Get current form ID
   $formID = $db->forms->getSectionFormID($id);    
    
   // No form results then insert new item
   if (!$db->results->doesFormHaveResult($formID)) {
        
      $insertedItemID = $db->forms->insertItem(
         $id, $text, "checkbox", 0, 0, 
         $order, 0, 0, true
      );
    
      $db->forms->insertOption(
          $insertedItemID, '1', 
          $descriptor, $checkedValue
      );

      $db->forms->reCalcFormTotal($formID);
      
      // Link competencies (if any)
      $db->competencies->linkToItem(
         $insertedItemID, 
         $competencies
      );
      
   }

 /**
  * Update checkbox item mode
  */
 } else if ($mode == "update") { 

    // Get item record to update
    $itemRecord = $db->forms->getItem($id);
    if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {
        
        // Item record
        $itemRecord = $db->fetch_row($itemRecord);
        $previousItemType = $itemRecord['type'];

        // Get section id
        $sectionID = $itemRecord['section_id'];
        $formID = $db->forms->getSectionFormID($sectionID);

        // Can only change item values if the form is not linked to results
        if (!$db->results->doesFormHaveResult($formID)) {
                        
            // Update item in database
            $db->forms->updateItem(
                $id, $order, 'checkbox', 
                0, 0, $text, 0, true
            );

            // Update options for checkbox
            if ($previousItemType == "checkbox") {

                $db->forms->updateOption(
                    $id, 
                    $descriptor, 
                    $checkedValue
                );
            
            // Anything else    
            } else {

                $db->forms->deleteItemOptions($id);
                $db->forms->insertOption(
                   $id, '1', $descriptor, 
                   $checkedValue
                );

            }

        } else {

            $db->forms->updateItemBasic($id, $text, $order);
            
        }
    
        // Recalculate form score total
        $db->forms->reCalcFormTotal($formID);
        
        // Link competencies (if any)
        $db->competencies->linkToItem($id, $competencies);    
    
    }

}
