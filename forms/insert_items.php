<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 * Copy items from previous form section to current form section
 */

// Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 $itemsDB = $db->forms->getSectionItems([$prevSection['section_id']]);

 //Loop through items of section that we are copying from
 while ($item = mysqli_fetch_assoc($itemsDB)) {

    $previousItemID = $item['item_id'];
    $itemDescription = $item['text'];
    $itemOrder = $item['item_order'];
    $itemAnsType = $item['type'];
    $highestItemValue = $item['highest_value'];
    $lowestItemValue = $item['lowest_value'];
    $weightedValue = $item['weighted_value'];
    $grs = $item['grs'];

    //Insert item into new section
    $itemID = $db->forms->insertItem(
        $sectionID, $itemDescription,
        $itemAnsType, $highestItemValue,
        $lowestItemValue, $itemOrder, $grs,$weightedValue
    );

    // Item map
    if (isset($itemIDMap)) {

       $itemIDMap[$previousItemID] = $itemID;

    }

    //Get item options
    $answerOptions = $db->forms->getItemOptions($previousItemID);

    // Insert items options
    switch ($itemAnsType) {
        case 'text':
        case 'slider':

            while ($answerOption = mysqli_fetch_assoc($answerOptions)) {

                list($inserted, $newOptionID) = $db->forms->insertOption(
                    $itemID,
                    $answerOption['option_order'],
                    $answerOption['descriptor']
                );

                if (isset($optionIDMap) && $inserted) {

                  $optionIDMap[$answerOption['option_id']] = $newOptionID;

                }

            }

            break;
        case 'checkbox':

            while ($answerOption = mysqli_fetch_assoc($answerOptions)) {

                list($inserted, $newOptionID) = $db->forms->insertOption(
                    $itemID,
                    $answerOption['option_order'],
                    $answerOption['descriptor'],
                    $answerOption['option_value']
                );

                if (isset($optionIDMap) && $inserted) {

                    $optionIDMap[$answerOption['option_id']] = $newOptionID;

                }

            }

            break;
        case 'radio':
        case 'radiotext':

            while ($answerOption = mysqli_fetch_assoc($answerOptions)) {

                $ansOptionValue = $answerOption['option_value'];
                $ansOptionText = $answerOption['descriptor'];
                $ansOptionOrder = $answerOption['option_order'];
                $ansOptionFlag = $answerOption['flag'];

                list($inserted, $newOptionID) = $db->forms->insertOption(
                    $itemID,
                    $ansOptionOrder,
                    $ansOptionText,
                    $ansOptionValue,
                    $ansOptionFlag
                );

                if (isset($optionIDMap) && $inserted) {

                    $optionIDMap[$answerOption['option_id']] = $newOptionID;

                }

            }

            break;
        default:
        // Do nothing (for now...)
    }
 }
