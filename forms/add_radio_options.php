<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 if (!defined('_OMIS')) {
    define('_OMIS', 1);
 }

 if (isset($_POST['isajaxcall'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page access check / can user access this section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $section_id = iS($_POST, 'section_id');
    $clone_item_yesno = '';
    $lowval = '';
    $highval = '';
 } else {
    $session = new OMIS\Session;
    $session->check();
 }

 $iscloneable = false;
 $scheck = false;

 if ($db->forms->doesFormSectionExist($section_id)) {
    if ($clone_item_yesno == "yes") {
        $ress = $db->forms->getItemOptions($clone_item);
        if ($ress && mysqli_num_rows($ress) > 0) {
            $iscloneable = true;
            $itemDB = $db->fetch_row($db->forms->getItem($clone_item));
            $numofrows = mysqli_num_rows($ress);
        }

        if ($itemDB['type'] == 'radio') {
            $scheck = true;
        }
    } else {
        $scheck = true;
    }
    ?><div class="optsdiv"><label for="numradiobuttons-<?=$section_id?>" class="labeltexttitle align-middle">Number of Values&nbsp;</label>
 <input class="form-control w-auto form-control-sm d-inline-block align-middle" type="text" id="numradiobuttons-<?=$section_id?>" maxlength="2" size="2" onkeyup="renderRadioOptions(this)" value="<?php if($iscloneable){ echo $numofrows; } ?>"/>

 <label for="isnumval-<?=$section_id?>" class="lbnumval align-middle" title="If left unchecked scores will have a weighting value = 0 applied.">Grade this item using values below</label>
 <input type="checkbox" id="isnumval-<?=$section_id?>" class="cbscore align-middle" <?php if($scheck){ ?>checked="checked"<?php }  ?>/>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="flip btn btn-sm btn-warning align-middle" id="flip-<?=$section_id?>" value="flip options"/>

 <?php if($config->exam_defaults['item_weighting'] == true){
        include 'item_weighting.php';
    }
?>
</div>

 <div id="radiobuttonsdiv-<?=$section_id?>" class="radcont"><?php
 if ($clone_item_yesno == "yes") {
      $i = 0;
      while ($rw = $db->fetch_row($ress)) {
        $i++;

         // Render option fields
        $template = new OMIS\Template("option_fields.html.twig");
        $template->render([
            "index" => $i,
            "id" => $section_id,
            "value" => $rw['option_value'],
            "descriptor" => $rw['descriptor'],
            "new_line" => ($i % 3 == 0),
            "value_readonly" => false,
            "flag_enabled" => (bool) $rw['flag']
        ]);

     }
     // Additional Div to push everything down
     ?><br class="clearv2"/><?php
  }
 ?></div>
 <?php

 } else {
   ?>
   <br/>
  <span class="boldred">&nbsp;&nbsp;&nbsp;&nbsp;The section does not exist in the system, please refresh form [F5 key].</span>
  <br/><br/>
  <?php
 }
 ?>
