<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 define('_OMIS', 1);

 // Add, submit, edit, update record,
 $mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

 // Mode post variable is set?
 if (!empty($mode)) {
     include __DIR__ . '/../extra/essentials.php';
     //Page Access Check / Can User Access this Section?

     // Get user role from session
     $userRole = $_SESSION['user_role'];

     // Current user ID
     $currentUserID = $_SESSION['user_identifier'];

     if (!\OMIS\Auth\Role::loadID($userRole)->canAccess()) {
         return false;
     }
 } else {
     include __DIR__ . '/../extra/noaccess.php';
 }

 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Auth\Role as Role;
 $userRolesAdmin = new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS);

 global $db;


 /**
  * Add record mode
  */
 if ($mode == 'add') {
     // Get Departments
     if ($userRolesAdmin->containsRole($userRole)) {
         $departments = $db->departments->getAllDepartments();
     } else {
         $departments = $db->departments->getExaminerDepartments($currentUserID);
     }

     // Get current selected department
     $selectedDept = filter_input(INPUT_POST, 'sdept', FILTER_SANITIZE_STRING);
     ?>
     <td class="checkb">&nbsp;</td>
     <td>
       <input type="text" class="name-field form-control form-control-sm" id="add_form_name" autocomplete="off" placeholder="name.."/>
     </td>
     <td>
      <div id="tagging-div">
       <select id="new-tag" class="ignore">
          <option value="">Select one...</option>
       <?php

       // Get system wide tags for auto-suggest
       $tagRecords = $db->forms->getTags();
       $systemWideTags = array_map(function($tag) {
         return $tag['name'];
       }, $tagRecords);

       foreach ($systemWideTags as $tag) {
         ?><option value="<?=$tag?>"><?=$tag?></option><?php
       }

       ?>
       </select>
       <div id="forms-tags">
          <span id="tags-existing">No existing tags</span>
       </div>
      </div>
     </td>
     <td>
      <select id="add_dept" class="custom-select custom-select-sm">
     <?php

     // Loop through all departments and render drop-down options
     while ($eachDept = $db->fetch_row($departments)) {

         $deptID = $eachDept['dept_id'];
         $deptName = $eachDept['dept_name'];
         $selected = ($selectedDept == $deptID) ? ' selected="selected"' : '';
         $schoolDB = $db->schools->getSchoolByDepartment($deptID);
         $schoolName = $schoolDB['school_description'];
         ?>
           <option value="<?=$deptID?>" title="<?=$deptName?>
           [<?=$schoolName?>]"<?=$selected?>><?=$deptID?></option>
         <?php
     }
     ?>
       </select>
     </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td class="titleedit">&nbsp;</td>
     <?php

 /**
  * Edit record mode
  */
 } elseif ($mode == 'edit') {
     // The ID number of the form to edit
     $formID = filter_input(INPUT_POST, 'station', FILTER_SANITIZE_NUMBER_INT);

     // Check to seee if the form exists ?
     if ($db->forms->doesFormExist($formID)) {

         // Get form record
         $formRecord = $db->forms->getForm($formID);

         // ID for the department that the form is attached to
         $formDeptID = $formRecord['dept_id'];

         // Formatted date
         $formattedDate = format_DateTime($formRecord['updated_at']);

         // Design complete
         $designComplete = $formRecord['design_complete'];

         // Get author record
         $authorID = $formRecord['author'];
         $authorRecord = $db->users->getUser($authorID, 'user_id', true);
         $forenames = $authorRecord['forename'];
         $surname = $authorRecord['surname'];

         // Get Departments
         if ($userRolesAdmin->containsRole($userRole)) {
             $departments = $db->departments->getAllDepartments();
         } else {
             $departments = $db->departments->getExaminerDepartments($currentUserID);
         }
         ?>
         <td class="checkb">&nbsp;
          <input type="hidden" id="edit_form_id" autocomplete="off" value="<?=$formID?>"/></td>
         <td>
          <input type="text" class="name-field form-control form-control-sm" id="edit_form_name" placeholder="name.." value="<?=$formRecord['form_name']?>"/></td>
         <td>
         <div id="tagging-div">
          <select id="new-tag" class="ignore">
             <option value="">Select one...</option>
          <?php

          // Get system wide tags for auto-suggest
          $tagRecords = $db->forms->getTags();
          $systemWideTags = array_map(function($tag) {
            return $tag['name'];
          }, $tagRecords);

          foreach ($systemWideTags as $tag) {
            ?><option value="<?=$tag?>"><?=$tag?></option><?php
          }

          ?>
          </select>
         </div>
         <div id="forms-tags"><?php

          $tags = $db->forms->getFormTags($formID);

          foreach ($tags as $tag) {
             ?><div class="form-tag-linked"><?php
               echo $tag['name'];
             ?></div><?php
          }

          if (count($tags) == 0) {
            ?><span id="tags-existing">No existing tags</span><?php
          }

         ?>
         </div>
         </td>
         <?php

         // Is the form already attached to an exam ?
         if (!$db->forms->isFormUsed($formID)) {
             ?>
             <td class="c">
             <select id="edit_dept">
             <?php
             while ($eachDept = $db->fetch_row($departments)) {
                 $deptID = $eachDept['dept_id'];
                 $deptName = $eachDept['dept_name'];
                 $selected = ($formDeptID == $deptID) ? ' selected="selected"' : '';
                 ?>
                 <option value="<?=$deptID?>" title="<?=$deptName?>" <?=$selected?>>
                   <?=$deptID?>
                 </option>
                 <?php
             }
             ?>
             </select>
         <?php
         } else {
           // Get department name
           $deptName = $db->departments->getDepartmentName($formDeptID);
             ?>
             <td class="c" title="<?=$deptName?>">
                 <?=$formDeptID?>
                 <input type="hidden" id="edit_dept" value="<?=$formDeptID?>"/>
             <?php
         }
         ?>
         </td>
         <td class="tal"><?=$forenames?> <?=$surname?> <br/>
         [<span class="boldgreen"><?=$authorID?></span>]
         </td>
         <td><?=$formattedDate?></td>
         <td>
         <ul class="settings set-adj">
          <li>
          <label title="Is form design complete?">DESIGN COMPLETE</label>
          <?php
            $checked = ($designComplete == 1) ? 'checked="checked"' : '';
          ?>
          <input type="checkbox" id="edit_complete" class="setting-checkbox"<?=$checked?>/>
          </li>
         </ul>
         </td>
         <td>&nbsp;</td>
         <td class="titleedit">&nbsp;</td>
         <?php
     } else {
         echo "FORM_NON_EXIST";
     }

 /**
  * Update record mode
  */
 } elseif ($mode == 'update') {

     $formID = filter_input(INPUT_POST, 'stat_id', FILTER_SANITIZE_NUMBER_INT);
     $formName = filter_input(INPUT_POST, 'stat_name', FILTER_SANITIZE_STRING);
     $formComplete = filter_input(INPUT_POST, 'stat_complete', FILTER_SANITIZE_NUMBER_INT);
     $deptID = filter_input(INPUT_POST, 'stat_dept', FILTER_SANITIZE_STRING);
     $tagString = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);

     if (!$db->forms->doesFormExist($formID)) {
        die("FORM_NON_EXIST");
     }

     if (!$db->departments->existsById($deptID)) {
        die("DEPT_NON_EXIST");
     }

     // Update form
     echo $db->forms->updateForm(
         $formID,
         $formName,
         $formComplete,
         $currentUserID,
         null,
         $deptID
     );

     // Update tags
     $tags = (!is_null($tagString) && strlen($tagString) > 0)
        ? explode(',', $tagString) : [];

     $db->forms->linkFormTags(
         $formID,
         $tags
     );

 /**
  * Location: form document (add, edit sections and items)
  * Function: list other stations from which the user can copy sections and items from
  */
 } elseif ($mode == 'dstations') {

     $selectedTerm = filter_input(INPUT_POST, 'sterm', FILTER_SANITIZE_STRING);
     $selectedFormID = filter_input(INPUT_POST, 'formid', FILTER_SANITIZE_NUMBER_INT);
     $selectedDept = filter_input(INPUT_POST, 'fdept', FILTER_SANITIZE_STRING);

     ?>
      <option value="--">--</option>
     <?php

     // What departments user has access to based on his/her role
     if (in_array($userRole, [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {

         $depts = $db->forms->getDepartmentsWithForms($selectedTerm);

     } else if ($userRole == Role::USER_ROLE_SCHOOL_ADMIN) {

         $adminSchools = $db->schools->getAdminSchools(
            $currentUserID
         );

         $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
         $depts = $db->departments->getDepartments(null, false, $schoolIDs);

     } else {

         $depts = $db->departments->getExaminerDepartments($currentUserID);

     }

     while ($dept = $db->fetch_row($depts)) {

         $deptID = $dept['dept_id'];
         $formGroups = [];
         ?><optgroup class="departments" label="<?=$dept['dept_name']?>: "><?php

         // Get forms for selected term and department
         $formRecords = $db->forms->getForms(
             $selectedTerm, $deptID,
             'form_name', 'ASC', false, true
         );

         // Group forms by tags, exclude current assessment
         while ($each = $db->fetch_row($formRecords)) {
             if ($selectedFormID != $each['form_id']) {
                $formGroups[$each['name']][] = $each;
             }
         }

         // Loop Through Forms
         foreach ($formGroups as $group => $forms) {

             ?><optgroup class="tags" label="~ <?=(empty($group) ? "untagged" : $group) ?>"><?php

             foreach ($forms as $form) {

               ?><option class="tag-forms" value="<?=$form['form_id']?>"><?php
                  echo $form['form_name'];
               ?></option><?php

             }

             ?></optgroup><?php

         }
      ?></optgroup><?php

    }

 /**
  * Location: form document (add, edit sections and items)
  * Function: list selected form sections from which the user can copy items from
  */
 } elseif ($mode == 'comps') {
     // Get station sections
     $previousStationID = filter_input(INPUT_POST, 'previous_form_id', FILTER_SANITIZE_NUMBER_INT);
     $currentStationID = filter_input(INPUT_POST, 'current_form_id', FILTER_SANITIZE_NUMBER_INT);
     $stationHasScale = $db->forms->doesFormHaveScaleSection($currentStationID);
     $sectionCount = 0;

     if ($db->forms->doesFormExist($previousStationID)){
        $sections = $db->forms->getFormSections([$previousStationID]);
        ?>
        <div id="prev-stations">
        <?php

        // Loop through form sections
        while ($section = $db->fetch_row($sections)) {
          $competenceType = $section['competence_type'];
          $sectionID = $section['section_id'];
             if ($competenceType == 'standard' || ($competenceType == 'scale' && !$stationHasScale)) {
                 $itemCount = $db->forms->getSectionItemCount($sectionID);
                 $sectionCount++;
                 $sectionTitle = "";
                 if (strlen($section['competence_desc']) > 0) {
                     $sectionTitle = $section['competence_desc'];
                     if (strlen($section['section_text']) > 0) {
                         $sectionTitle .= ' : ';
                     }
                 }

                 if (strlen($section['section_text'])) {
                     $sectionTitle .= $section['section_text'];
                 }

                 /* 20140115 (DW) Patching this file so that it shows "(no title)"
                  * should a competency have no title...
                  */
                 if (trim($sectionTitle) == '') {
                     $sectionTitle = "(no title)";
                 }

             // Section title output
             $sectionTitleOutput = mb_substr($sectionTitle, 0, 51, 'UTF-8');
             $sectionTitleOverflow = (strlen($sectionTitle) > 51) ? '..' : '';
             $itemCountTitle = ($itemCount == 1) ? 'item' : 'items';
             ?>
             <input type="checkbox" id="prevsection-<?=$sectionCount?>" name="prevsection[]" value="<?=$sectionID?>" checked="checked"/>
             &nbsp;&nbsp;
             <label class="sectionlabel" for="prevsection-<?=$sectionCount?>" title="<?=$sectionTitle?>">
               &nbsp;<?=$sectionTitleOutput.$sectionTitleOverflow?>
             </label><br/>
             <div class="item-info-div">with <?=$itemCount?> <?=$itemCountTitle?></div>
             <?php
         }
       }
         ?></div>
         <?php
 	}

     // If form has no sections display appropriate error message
     if ($sectionCount == 0) {
         ?>
         <div class="red" style="margin-top: 4px">&nbsp;Selected <?=gettext('form')?> has no sections</div>
         <?php
     }

 /**
  * Save search criteria
  */
 } else if ($mode == "save_search_criteria") {

   $dept = filter_input(INPUT_POST, 'department', FILTER_SANITIZE_STRING);
   $search = filter_input(INPUT_POST, 'search_term', FILTER_SANITIZE_STRING);
   $tags = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);
   if (!$db->departments->existsById($dept)) {
     return false;
   }

   // Stick criteria as session
   $_SESSION['stick_options'] = [
      'form_bank' => [
         $dept => [
            'search' =>  $search,
            'tags' => $tags
         ]
      ]
   ];

 }
