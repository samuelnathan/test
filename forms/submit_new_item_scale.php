<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
 define('_OMIS', 1);
 
 if (isset($_POST['section_id'])) {
   
     include __DIR__ . '/../extra/essentials.php';
     if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
       
         return false;
         
     }
     
 } else {
   
     include __DIR__ . '/../extra/noaccess.php';
     
 }
 
 $section_id = $_POST['section_id'];
 $item_text = $_POST['item_text'];
 $radfvals = $_POST['radiofar'];
 $radfdesc = $_POST['radiofdescar'];
 $scale = $_POST['grs'];
 
 $arrayofradiofields = [];
 
 for ($i = 0; $i < sizeof($radfvals); $i++) {
   
     $eachrow = array();
     $eachrow[] = $radfvals[$i];
     $eachrow[] = $radfdesc[$i];
     $eachrow[] = 0;
 
     $arrayofradiofields[] = $eachrow;
     
 }
 
 $form_id = $db->forms->getSectionFormID($section_id);
 
 if (!$db->results->doesFormHaveResult($form_id) && !$db->forms->doesSectionHaveScaleItem($section_id)) {
   
     $item_id = $db->forms->insertItem($section_id, $item_text, "radiotext", 0, 0, 1, $scale);
     $db->forms->insertRadioOptions($item_id, $arrayofradiofields);
     $db->forms->reCalcFormTotal($form_id);
     
 }
 