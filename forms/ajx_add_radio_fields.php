<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

define("_OMIS", 1);

if (isset($_POST["section_id"]) && isset($_POST["quantity"])) {
    include __DIR__ . "/../extra/essentials.php";
    if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . "/../extra/noaccess.php";
}

$quantity = trim($_POST["quantity"]);
$sectionID = trim($_POST["section_id"]);

if ($db->forms->doesFormSectionExist($sectionID)) {
    for ($i=1; $i<=$quantity; $i++) {
        
        // Render option fields
        $template = new OMIS\Template("option_fields.html.twig");
        $template->render([
           "index" => $i, 
           "id" => $sectionID,
           "value" => "",
           "descriptor" => "",
           "new_line" => ($i % 3 == 0),
           "value_readonly" => false, 
           "flag_enabled" => false
        ]);
             
    }
?><br class="clearv2"/><?php
} else {
   ?>
   <span class="boldred">The section does not exist in the system, please refresh form [F5 key].</span>
   <br/><br/>
  <?php
}
