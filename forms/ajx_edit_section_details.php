<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 use \OMIS\Template as Template;
 use OMIS\Database\SelfAssessments;
 define('_OMIS', 1);
 
 $mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);
 $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
 $sessionId = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_NUMBER_INT);
 
 
 if (!is_null($mode) && !is_null($id)) {
 
    include __DIR__ . "/../extra/essentials.php";
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }
 
 } else {

    include __DIR__ . "/../extra/noaccess.php";

 }
 
 
 /**
  * Edit Section
  */ 
 
 if ($mode == "edit") {
 
    $formSectionDB = $db->forms->getFormSection($id);
    $competenciesDB = $db->forms->getCompetencies(NULL, true);
    
    if (!$formSectionDB || mysqli_num_rows($formSectionDB) == 0) {

        echo "Section not found or section has been recently deleted, please refresh page (F5 Key)";
        exit;

    } else if (count($competenciesDB) == 0) {

        echo "Competence types not found, please refresh page (F5 Key)";
        exit;

    }

    // Fetch Section Details
    $formSection = $db->fetch_row($formSectionDB);
    
    // Get station competence count
    $formSectionCount = $db->forms->getFormSectionCount(
        $formSection['form_id']
    ); 

    // Prepare HTML template data
    $data = [
        'current_section_id' => $id,
        'current_competence_id' => $formSection['competence_id'],
        'current_competence_type' => $formSection['competence_type'],
        'current_section_order' => $formSection['section_order'],
        'section_count' => $formSectionCount,
        'additional_title_info' => $formSection['section_text'],
        'competencies' => $competenciesDB,
        'section_feedback' => $formSection['section_feedback'],
        'item_feedback' => $formSection['item_feedback'],
        'feedback_required' => $formSection['feedback_required'],
        'feedback_internal' => $formSection['internal_feedback_only'],

        'exam_self_assess' => $db->selfAssessments->existsByExam(
            $db->sessions->getSessionExamID($sessionId)
         ),

        'form_self_assess' => $db->selfAssessments->scoresheetHasSelfAssessment(
            $formSection['form_id']
        ),

        'section_self_assess' => $formSection['self_assessment']
    ];
   
    // Render HTML template
    $template = new Template(Template::findMatchingTemplate(__FILE__));
    $template->render($data);
 
 /**
  * Update Section
  */  
 } elseif ($mode == "update") {
 
    $competenceSelected = filter_input(INPUT_POST, "competence_selected");
 
    // A few checks
    if ($db->forms->doesFormSectionExist($id)) {
 
        if ($db->forms->doesCompetenceExist($competenceSelected)) {
            
            // Then update
            $db->forms->updateFormSection(
                $id,
                [
                    "order" => $_POST['order'],
                    "title" => $_POST['title_text'],
                    "competence" => $competenceSelected,
                    "sectionFeedback" => $_POST['section_feedback'],
                    "itemFeedback" => $_POST['item_feedback'],
                    "feedbackRequired" => $_POST['feedback_required'],
                    "feedbackInternal" => $_POST['feedback_internal'],
                    "selfAssessment" => (isset($_POST['self_assessment']) ? $_POST['self_assessment'] : null)
                ]
            );
            
        }
 
    }
 
 /**
  * Delete Section
  */     
 } elseif ($mode == "delete") {
 
    if ($db->forms->doesFormSectionExist($id)) {

        $formID = $db->forms->getSectionFormID($id);

        if (!$db->results->doesFormHaveResult($formID)) {

            $db->forms->deleteFormSection($id, $formID);
            $db->forms->reCalcFormTotal($formID);

        }

    }
 
 }
