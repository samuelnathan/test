<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

define('_OMIS', 1);
use \OMIS\Template as Template;

$itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
$quantity = filter_input(INPUT_POST, 'quantity', FILTER_SANITIZE_NUMBER_INT);
if (!in_array(NULL, [$itemID, $quantity])) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$formData = [
    'form_exists' => $db->forms->doesItemExist($itemID)
];

// If the form exists, add some more details to the template data.
if ($formData['form_exists']) {
    $formData['quantity'] = $quantity;
    $formData['id'] = $itemID;
}

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($formData);
