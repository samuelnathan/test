<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 04/10/2014
 * © 2014 Qpercom Limited.  All rights reserved.
 */
use \OMIS\Template as Template;

define('_OMIS', 1);

$section_id = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);

if (!is_null($section_id)) {
    include __DIR__ . '/../extra/essentials.php';
    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$form_id = $db->forms->getSectionFormID($section_id);

$data = array(
    'current_section_id' => $section_id,
    'section_has_scale_item' => $db->forms->doesSectionHaveScaleItem($section_id),
    'form_has_results' => $db->results->doesFormHaveResult($form_id)
);

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($data);