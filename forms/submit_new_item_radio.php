<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

$sectionID = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
if (!is_null($sectionID)) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$order = filter_input(INPUT_POST, 'item_order');
$text = filter_input(INPUT_POST, 'item_text');
$values = filter_input(INPUT_POST, 'radiofar', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$descriptions = filter_input(INPUT_POST, 'radiofdescar', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$flags = filter_input(INPUT_POST, 'radio_flags', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$isGraded = filter_input(INPUT_POST, 'isnumval');
$competencies = filter_input(INPUT_POST, 'competencies', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
$weightedVal = !$config->exam_defaults['item_weighting'] ? 1 : filter_input(INPUT_POST, 'weighted_value');

$highestValue = 0;
$lowestValue = 0;


for ($i=0; $i<sizeof($values); $i++) {
    $eachRow = [];

    $eachRow[] = $values[$i];
    $eachRow[] = $descriptions[$i];
    $eachRow[] = $flags[$i];

    $valueFields[] = $eachRow;
}

// Is graded
if ($isGraded == 1) {
    $highestValue = max($values);
    $lowestValue = min($values);
    $type = "radio";
} else {
    $highestValue = 0;
    $lowestValue = 0;
    $type = "radiotext";
}

// Get form ID
$formID = $db->forms->getSectionFormID($sectionID);

if (!$db->results->doesFormHaveResult($formID)) {

    // Insert item
    $itemID = $db->forms->insertItem(
            $sectionID, $text, $type, $highestValue, $lowestValue, $order, 0, 0, true, $weightedVal
     );

    // Insert options
    $db->forms->insertRadioOptions($itemID, $valueFields);

    // Recalculate form total
    $db->forms->reCalcFormTotal($formID);

    // Link competencies (if any)
    $db->competencies->linkToItem($itemID, $competencies);

}
