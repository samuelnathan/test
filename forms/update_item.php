<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

$itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);

if (!is_null($itemID)) {

    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

} else {

    include __DIR__ . '/../extra/noaccess.php';

}

$selectedItemType = filter_input(INPUT_POST, 'item_type');
$itemOrder = filter_input(INPUT_POST, 'item_order');
$itemText = filter_input(INPUT_POST, 'item_text');
$textRangeOne = filter_input(INPUT_POST, 'text_rangeone');
$textRangeTwo = filter_input(INPUT_POST, 'text_rangetwo');
$ansOptionText = filter_input(INPUT_POST, 'descriptor');
$weightedVal = filter_input(INPUT_POST, 'weighted_value');
$competencies = filter_input(INPUT_POST, 'competencies', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

// Get item record to update
$itemDB = $db->forms->getItem($itemID);
if ($itemDB && mysqli_num_rows($itemDB) > 0) {

    // Item record
    $itemRecord = $db->fetch_row($itemDB);
    $previousItemType = $itemRecord['type'];

    // Get section id
    $sectionID = $itemRecord['section_id'];
    $formID = $db->forms->getSectionFormID($sectionID);

    // Can only change item values if the form is not linked to results
    if (!$db->results->doesFormHaveResult($formID)) {

        $itemType = ($selectedItemType == 'slider_range') ? 'slider' : 'text';

        // Update item in database
        $db->forms->updateItem($itemID, $itemOrder, $itemType, $textRangeTwo, $textRangeOne, $itemText, 0, true, $weightedVal);

        // Update options for text|slider type
        if (in_array($previousItemType, ['text', 'slider'])) {

            $db->forms->updateOption($itemID, $ansOptionText);

        // Anything else
        } else {

            $db->forms->deleteItemOptions($itemID);
            $db->forms->insertOption($itemID, "1", $ansOptionText);

        }

    } else {

        $db->forms->updateItemBasic($itemID, $itemText, $itemOrder);
        $db->forms->updateOption($itemID, $ansOptionText);

    }

    // Recalculate form score total
    $db->forms->reCalcFormTotal($formID);

    // Link competencies (if any)
    $db->competencies->linkToItem($itemID, $competencies);

}
