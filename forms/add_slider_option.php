<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 12/02/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */
if (!defined('_OMIS')) {
    define('_OMIS', 1);
}

$isAjaxCall = filter_input(INPUT_POST, 'isajaxcall', FILTER_SANITIZE_STRING);
if (!is_null($isAjaxCall)) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $section_id = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
    $lowValue = "";
    $highValue = "";

} else {

    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();

    if ($clone_item_yesno == "yes") {
        $highValue = (float)$qarray['highest_value'];
        $lowValue = (float)$qarray['lowest_value'];

        $itemOptionsDB = $db->forms->getItemOptions($clone_item);
        if ($itemOptionsDB && mysqli_num_rows($itemOptionsDB) > 0) {
            $itemOptions = $db->fetch_row($itemOptionsDB);
        }
    }
}

// Does form section exist
if ($db->forms->doesFormSectionExist($section_id)) {
?>
<?php if($config->exam_defaults['item_weighting'] == true){
        include 'item_weighting.php';

    }
?>


<div class="indivopts">
 <input type="hidden" id="descriptor-<?=$section_id; ?>" value=""/>
 <label for="text_rangeone-<?=$section_id?>" class="tranlb">Range&nbsp;&nbsp;</label>
 <input type="text" id="text_rangeone-<?=$section_id?>" size="2" maxlength="3" onkeyup="ValidateNumericV2(this)" value="<?=$lowValue?>"/>
 <label for="text_rangetwo-<?=$section_id?>" class="ltxt"> to </label>
 <input type="text" id="text_rangetwo-<?=$section_id?>" size="2" maxlength="3" onkeyup="ValidateNumericV2(this)" value="<?=$highValue?>"/>
</div>
</div>
<?php

} else {

  $message = "The section does not exist in the system, please refresh form [F5 key]";
 ?>
  <br/>
  <span class="boldred">&nbsp;&nbsp;&nbsp;&nbsp;<?=$message?></span>
  <br/><br/>
<?php
}
?>
