<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  */

 if (!defined('_OMIS')) {
    define('_OMIS', 1);
 }

 $isAjaxCall = filter_input(INPUT_POST, 'isajaxcall', FILTER_SANITIZE_STRING);

 if (!is_null($isAjaxCall)) {

    include __DIR__ . '/../extra/essentials.php';

    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $section_id = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
    $lowValue = "0";
    $highValue = "1";

 } else {
    
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();

    // Does form section exist
    if ($clone_item_yesno == "yes") {
        
        $lowValue = (float)$qarray['lowest_value'];

        $itemOptionsDB = $db->forms->getItemOptions($clone_item);
        if ($itemOptionsDB && mysqli_num_rows($itemOptionsDB) > 0) {

            $itemOptions = $db->fetch_row($itemOptionsDB);
            $highValue = $itemOptions['option_value'];

        }
    
    }   
    
 }

 // Form section doesn't exist
 if (!$db->forms->doesFormSectionExist($section_id)) {

     ?><br/>
     <span class="boldred">&nbsp;&nbsp;&nbsp;&nbsp;
        The section does not exist in the system, please refresh form [F5 key]
     </span>
     <br/><br/>
     <?php    
    
    return;

 }

 ?>
 <div class="indivopts">
 <input type="hidden" id="checkbox-descriptor-<?=$section_id?>" value=""/>
 <input type="hidden" id="checkbox-unchecked-<?=$section_id?>" value="0"/>
 <label for="checkbox-checked-<?=$section_id?>" class="ltxt">Checked Value (Non-Grade)</label>
 <input type="text" id="checkbox-checked-<?=$section_id?>" size="2" maxlength="3" value="<?=$highValue?>"/>
 </div>
