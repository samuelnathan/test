<?php
/* Script to update with radio type
 * Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/06/2014
 * Upgraded GRS Database Structure [06/10/2014]
 * © 2014 Qpercom Limited.  All rights reserved.
 */
define('_OMIS', 1);

$item_id = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
if (!is_null($item_id)) {
    include __DIR__ . '/../extra/essentials.php';
    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$item_id = trim($_POST['item_id']);
$item_text = trim($_POST['item_text']);
$scale = trim($_POST['grs']);
$radfvals = $_POST['radfvals'];
$radfdesc = $_POST['radfdesc'];
$new_rad_cnt = sizeof($radfvals);
$arrayofradiofields = array();

for ($i = 0; $i < sizeof($radfvals); $i++) {

    $eachrow = array();

    $eachrow[] = $radfvals[$i];
    $eachrow[] = $radfdesc[$i];
    $eachrow[] = 0;

    $arrayofradiofields[] = $eachrow;
}

$res = $db->forms->getItem($item_id);
if ($res && mysqli_num_rows($res) > 0) {
    $ar = $db->fetch_row($res);
    $form_id = $db->forms->getSectionFormID($ar['section_id']);
    $has_result = $db->results->doesFormHaveResult($form_id);
    if ($has_result) {
        $db->forms->updateItem($item_id, 1, "radiotext", 0, 0, $item_text);
    } else {
        $db->forms->updateItem($item_id, 1, 'radiotext', 0, 0, $item_text, $scale);
    }

    if ($ar['type'] == "radiotext" && $db->forms->getItemOptionCount($item_id) == $new_rad_cnt) {
        #If there are results attached to the assessment form then don't allow for radio values to be updated hence the updateValues parameter (!$has_result) 
        $db->forms->updateRadioOptions($item_id, $arrayofradiofields, !$has_result);
    } else {
        if (!$has_result) {
            $db->forms->deleteItemOptions($item_id);
            $db->forms->insertRadioOptions($item_id, $arrayofradiofields); # New Values
        }
    }
    $db->forms->reCalcFormTotal($form_id);
}