<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 17/08/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */
use \OMIS\Template as Template;

if (!defined('_OMIS')) {
    define('_OMIS', 1);
}

$ansOptionText = "";
$lowValue = "";
$highValue = "";

if (!is_null(filter_input(INPUT_POST, 'isajaxcall'))) {
    include __DIR__ . '/../extra/essentials.php';

    // Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $section_id = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
} else {
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();

    if ($clone_item_yesno == 'yes') {
        $highValue = (float)$qarray['highest_value'];
        $lowValue = (float)$qarray['lowest_value'];

        $itemOptionsDB = $db->forms->getItemOptions($clone_item);
        if ($itemOptionsDB && mysqli_num_rows($itemOptionsDB) > 0) {
            $itemOptions = $db->fetch_row($itemOptionsDB);
            $ansOptionText = $itemOptions['descriptor'];
        }
    }
}

$data['identifier'] = $section_id;
$data['highval'] = $highValue;
$data['lowval'] = $lowValue;
$data['descriptor'] = $ansOptionText;
$data['exists'] = isset($section_id) && $db->forms->doesFormSectionExist($section_id);
$data['error_info'] = "The section does not exist in the system, please refresh form [F5 key]";
$data['disable_fields'] = false;
$data['item_weighting'] = $config->exam_defaults['item_weighting'];

$template = new Template('textbox_option.html.twig');
$template->render($data);
