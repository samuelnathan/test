<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 12/02/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */
use \OMIS\Template as Template;

if (!defined('_OMIS')) {
    define('_OMIS', 1);
}

// Is ajax call
$ajaxCall =  filter_input(INPUT_POST, 'isajaxcall', FILTER_SANITIZE_STRING);
if (strlen($ajaxCall) > 0) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formID = $db->forms->getSectionFormID($sectionID);
    $formHasResult = $db->results->doesFormHaveResult($formID);
    $itemRecord = $db->forms->getItem($itemID);
} else {
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
}

mysqli_data_seek($itemRecord, 0);

if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {
  $item = $db->fetch_row($itemRecord);

  $highValue = (float)$item['highest_value'];
  $lowValue = (float)$item['lowest_value'];
  $weightedValue = $item['weighted_value'];

  $optionsDB = $db->forms->getItemOptions($itemID);
  $options = $db->fetch_row($optionsDB);
  $answerOptionText = $options['descriptor'];
  $itemWeighting = $config->exam_defaults['item_weighting'];

  $data['identifier'] = $itemID;
  $data['highval'] = $highValue;
  $data['lowval'] = $lowValue;
  $data['descriptor'] = $answerOptionText;
  $data['exists'] = isset($sectionID) && $db->forms->doesFormSectionExist($sectionID);
  $data['disable_fields'] = $formHasResult;
  $data['item_weighting'] = $itemWeighting;
  $data['weighted_value'] = $weightedValue;
} else {
  $data['exists'] = false;
  $data['error_info'] = "This item does not exist in this section, please refresh page [F5 key]";
}

  $template = new Template('textbox_option.html.twig');
  $template->render($data);
