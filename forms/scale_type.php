<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
 
 if(!defined('_OMIS')) {
   define('_OMIS', 1);
 }
 
if (isset($_POST['isajaxcall'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    
    $setScaleType = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_NUMBER_INT);
    $sectionID = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
    
// Critical Session Check    
} else {
    $session = new OMIS\Session;
    $session->check();
}

// Get Scale values from database
$scaleValues = $db->forms->getRatingScaleValues($setScaleType);

if ($db->forms->doesFormSectionExist($sectionID)) {
 ?><div class="optsdiv2"><input type="button" class="flip" id="flip" value="reverse"/></div>
   <div id="radiobuttonsdiv" class="radcont"><?php 
   
   // Loop through scale values
   for($i=1; $i<=sizeof($scaleValues); $i++) {
     
     $arrayIndex = $i-1;
     $scaleColour = $scaleValues[$arrayIndex]['scale_type_colour'];
     $scaleValue = $scaleValues[$arrayIndex]['scale_value'];
     $scaleDescription = $scaleValues[$arrayIndex]['scale_value_description'];
     $readOnly = 'readonly="readonly" title="Fields are not Editable"'; 
      
  ?><div class="flp">
     <div class="fl">
       <input type="text" class="radtype2 <?=$scaleColour?>" id="<?="radio-".$i?>" value="<?=$scaleValue?>" <?=$readOnly?> placeholder="value <?=$i?>.."/>
     </div>
     <div class="fl textarea-div">
      <textarea id="<?="radiotext-".$i?>" cols="28" rows="6" class="<?=$scaleColour?>" style="overflow:hidden" <?=$readOnly?> placeholder="descriptor <?=$i?>.."><?php 
        echo $scaleDescription; 
      ?></textarea>
     </div>
     <br class="clearv2"/>
   </div><?php
			  
     if ($i%3 == 0) {
       ?><br class="clearv2"/><?php 
     }
       
    }
   ?><br class="clearv2"/>
  </div>
<?php 
 } else {
   ?><div class="option-panel-error">
     The Competence does not exist in the System anymore, please refresh page [F5 key]
    </div><?php
 }