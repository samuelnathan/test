<?php
/**
 * Author Kelvin Nunn
 * Edited Jan 2019
 * for Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
?>


<div class="itemweighdiv" >

    <?php
    $itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formHasResult = $db->results->doesFormHaveResult($db->forms->getSectionFormID($sectionID));
    if (!isset($weightedValue)){
        $weightedValue = 1;
    }
    if($weightedValue == 1){?>
    <label for="isnumval-<?=$itemID?>" class="lbnumval">Apply Item Weighting</label>
	<input type="checkbox" class="cbscore" id="weightcb" onclick="weightVal()"<?php  if($formHasResult){ ?>disabled="disabled" title="This scoresheet has results attached. Cannot change this value"<?php }?>/>
    <label  class="lbnumval" id="weighValLbl" hidden="true" >Value </label>
    <input id="itemWeightVal" class="cbscore" placeholder="default is 1" onkeypress="return hasNumber(event)" type="number" maxlength="4" max="2" min="0.01" step="0.01" hidden="true" value="<?php echo $weightedValue;?>"/><?php }else {?>
    <label for="isnumval-<?=$itemID?>" class="lbnumval">Apply Item Weighting</label>
	<input type="checkbox" class="cbscore" id="weightcb" onclick="weightVal()" checked="checked"<?php  if($formHasResult){ ?>disabled="disabled" title="This scoresheet has results attached. Cannot change this value"<?php }?>/>
     <label  class="lbnumval" id="weighValLbl" >Value </label>
    <input id="itemWeightVal" class="cbscore" onkeypress="return hasNumber(event)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type="number" maxlength="4" max="2" min="0.01" step="0.01" value="<?php echo $weightedValue;?>"<?php  if($formHasResult){ ?>disabled="disabled" title="This scoresheet has results attached. Cannot change this value"<?php }?>>

    <?php }?>
</div>
