<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
 
 if(!defined('_OMIS')) {
  define('_OMIS', 1);
 }
 
if (isset($_POST['isajaxcall'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    
    $scaleTypeDB = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_NUMBER_INT);
    $itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
    $itemDB = $db->forms->getItem($itemID);
    
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formID = $db->forms->getSectionFormID($sectionID);
    $formHasResult = $db->results->doesFormHaveResult($formID);
    
} else {
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
}

// If Item Exists
if ($itemDB && mysqli_num_rows($itemDB) > 0) {
   
 // Flip disabled or Enabled 
 $flipDisabled = ($formHasResult) ? 'disabled="disabled"' : '';  
 
 // Get current item options
 $itemOptions = $db->forms->getItemOptions($itemID);
 
 // Get Scale values from database
 $scaleValues = $db->forms->getRatingScaleValues($scaleTypeDB);

 // Select first option
 $firstOption = $db->fetch_row($itemOptions); 
 
 // Reverse the order of the array of scale types
 if ($firstOption['descriptor'] != $scaleValues[0]['scale_value_description']) {
    $scaleValues = array_reverse($scaleValues, false);
 }
 
 ?><div class='optsdiv2'><?php 
    ?><input type="button" class="flip btn btn-sm btn-warning" id="flip" value="flip options" <?=$flipDisabled?>/><?php 
 ?></div>
   <div id="radiobuttonsdiv" class="radcont"><?php 
  
     // Loop through scale values
   for($i=1; $i<=sizeof($scaleValues); $i++) {
     
     $arrayIndex = $i-1;
     $scaleColour = $scaleValues[$arrayIndex]['scale_type_colour'];
     $scaleValue = $scaleValues[$arrayIndex]['scale_value'];
     $scaleDescription = $scaleValues[$arrayIndex]['scale_value_description'];
     $readOnly = 'readonly="readonly" title="Fields are not Editable"'; 
      
  ?><div class="flp">
     <div class="fl">
       <input type="text" class="radtype2 <?=$scaleColour?>" id="<?="radio-".$i?>" value="<?=$scaleValue?>" <?=$readOnly?> placeholder="value <?=$i?>.."/>
     </div>
     <div class="fl textarea-div">
      <textarea id="<?="radiotext-".$i?>" cols="28" rows="6" class="<?=$scaleColour?>" style="overflow:hidden" <?=$readOnly?> placeholder="descriptor <?=$i?>.."><?php 
        echo $scaleDescription; 
      ?></textarea>
     </div>
     <br class="clearv2"/>
   </div><?php
			  
     if ($i%3 == 0) {
       ?><br class="clearv2"/><?php 
     }
       
    }
   ?><br class="clearv2"/>
  </div>
<?php

} else {
 ?><div class="option-panel-error">
    This item does not exist in this section, Please refresh page [F5]
  </div><?php
}