<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

$itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);
if (!is_null($itemID)) {

    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

} else {

    include __DIR__ . '/../extra/noaccess.php';

}

// Get all relevant post params
$itemOrder = filter_input(INPUT_POST, 'item_order');
$itemText = filter_input(INPUT_POST, 'item_text');
$isGraded = filter_input(INPUT_POST, 'isnumval');
$values = filter_input(INPUT_POST, 'radfvals', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$labels = filter_input(INPUT_POST, 'radfdesc', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$flags = filter_input(INPUT_POST, 'radio_flags', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
$weightedVal = filter_input(INPUT_POST, 'weighted_value');
$valueCount = sizeof($values);
$competencies = filter_input(INPUT_POST, 'competencies', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

$radioOptions = [];

// Put values and labels together for database entry
for ($i=0; $i<sizeof($values); $i++) {

    $radioOptions[] = [$values[$i], $labels[$i], $flags[$i]];

}

// Item is graded
if ($isGraded == 1) {

    $highestValue = max($values);
    $lowestValue = min($values);
    $type = "radio";

} else {

    $highestValue = 0;
    $lowestValue = 0;
    $type = "radiotext";

}

// Get current item record
$itemDB = $db->forms->getItem($itemID);
if ($itemDB && mysqli_num_rows($itemDB) > 0) {

    $itemRecord = $db->fetch_row($itemDB);
    $previousType = $itemRecord['type'];

    // Get station set id
    $sectionID = $itemRecord['section_id'];
    $formID = $db->forms->getSectionFormID($sectionID);

    // Can only change item values if the form is not linked to results
    if (!$db->results->doesFormHaveResult($formID)) {

         // Update item in database
        $db->forms->updateItem(
                $itemID, $itemOrder, $type, $highestValue, $lowestValue, $itemText, 0, true, $weightedVal
        );

        if (in_array($previousType, ['radio', 'radiotext']) && $db->forms->getItemOptionCount($itemID) == $valueCount) {

            $db->forms->updateRadioOptions($itemID, $radioOptions);

        } else {

            $db->forms->deleteItemOptions($itemID);
            $db->forms->insertRadioOptions($itemID, $radioOptions);

        }

    // then, just update labels only
    } else {

        $db->forms->updateItemBasic($itemID, $itemText, $itemOrder);
        if (in_array($previousType, ['radio', 'radiotext']) && $db->forms->getItemOptionCount($itemID) == $valueCount) {

            $db->forms->updateRadioOptions($itemID, $radioOptions, false);

        }

    }

    // Recalculate form score total
    $db->forms->reCalcFormTotal($formID);

    // Link competencies (if any)
    $db->competencies->linkToItem($itemID, $competencies);

}
