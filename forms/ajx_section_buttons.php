<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */


use \OMIS\Template as Template;

define('_OMIS', 1);

$sectionID = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);

if (strlen($sectionID) > 0) {
    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

 $sectionsItems = [];

 // Does section exist? If not print out appropriate message 
 $sectionExists = $db->forms->doesFormSectionExist($sectionID);
 
 if ($sectionExists) { 
  $formID = $db->forms->getSectionFormID($sectionID);
  $hasItems = $db->forms->doesSectionHaveItems($sectionID);
  $formHasResult = $db->results->doesFormHaveResult($formID);
  $formItemCount = $db->forms->getFormItemCount($formID);
  $sectionPossibleMarks = $db->forms->getSectionPossibleMarks($sectionID);
  
  // Get all current form sections
  $formSectionsDB = $db->forms->getFormSections([$formID], true);
  
   /* Prepare list of sections to items array */
   foreach ($formSectionsDB as &$section) {
      $formSectionID = $section['section_id'];
      $sectionItemsDB = $db->forms->getSectionItems([$formSectionID], true);
      $section['items'] = $sectionItemsDB;
      $sectionsItems[$formSectionID] = $section; 
   }
   
  $data = [
   'sections_items' => $sectionsItems,
   'current_section_id' => $sectionID,
   'has_items' => $hasItems,
   'form_has_result' => $formHasResult,
   'form_item_count' => $formItemCount,
   'section_possible_marks' => $sectionPossibleMarks
  ];

  $template = new Template(Template::findMatchingTemplate(__FILE__));
  $template->render($data);
  
} else {
  echo "Section does not exist or has been deleted, please refresh web page [F5]";   
}
