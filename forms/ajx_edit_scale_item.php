<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/02/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */
define('_OMIS', 1);

$itemID = filter_input(INPUT_POST, 'item_id', FILTER_SANITIZE_NUMBER_INT);

if (!is_null($itemID)) {
    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

?><div class="redborderv1" id="itemeditdiv"><?php

 // Get current scale item from the database
 $itemDB = $db->forms->getItem($itemID);
 $itemCount = mysqli_num_rows($itemDB);

// If Item Exists
if ($itemDB && $itemCount > 0) {
    $item = $db->fetch_row($itemDB);
    $itemID = $item['item_id'];
    $itemDescription = $item['text'];
    $scaleTypeDB = $item['grs'];
  
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formID = $db->forms->getSectionFormID($sectionID);
    $formHasResult = $db->results->doesFormHaveResult($formID);
        
 ?><div class="qitem">
    <input type="hidden" id="scale-item-id" value="<?=$itemID?>"/>
    <input type="hidden" id="item_order" value="1"/>
    <div class="itemtxtdiv"><?php 
     ?><div id="load-editor" class="red">Loading Editor...</div><?php 
     ?><textarea id="item_text" cols="50" rows="4" class="itemtxt" style="visibility: hidden"><?php 
       echo $itemDescription; 
     ?></textarea>
    </div>
    <br class="clearv2"/>
   </div>   
   <div id="scale-option-types" class="fc">
   <?php

    // Get Rating Scale Types
    $ratingScaleTypes = $db->forms->getRatingScaleTypes();
   
    // Loop through all of the Global Rating Scale Types
    foreach ($ratingScaleTypes as $ratingScaleType) {
    
      $scaleTypeID = $ratingScaleType['scale_type_id'];
      $scaleTypeName = $ratingScaleType['scale_type_name'];
      $scaleTypeColour = $ratingScaleType['scale_type_colour'];
      $scaleSelected = ($scaleTypeID == $scaleTypeDB) ? ' checked="checked"' : '';
      $scaleDisabled = ($formHasResult) ? ' disabled="disabled"' : '';
      
     ?><label for="type<?=$scaleTypeID?>" class="labeltext <?=$scaleTypeColour?>"><?=$scaleTypeName?></label><?php
     ?><input type="radio" class="scale-types-radio" value="<?=$scaleTypeID?>" name="item_type" id="type<?=$scaleTypeID?>"<?php echo $scaleSelected; echo $scaleDisabled?>/><?php
     
   }
   ?><div id="qansoptions" class="ansoptioncreatediv">
        <?php include "edit_scale_type.php"?>
    </div>
    <div class="bpan" id="buttonpanel">
        <input type="button" value="cancel" id="cancel_update_scale_item"/>
        <input type="button" value="update Item" id="update_scale_item"/>
    </div>
</div>
<?php
} else {
   ?><div class="option-panel-error">
       This item does not exist in this section, please refresh page [F5 Key]
     </div><?php
}
?></div>