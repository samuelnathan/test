<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

use \OMIS\Template as Template;

define('_OMIS', 1);

// Current section ID and session ID
$section_id = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);
$session_id = filter_input(INPUT_POST, 'session_id', FILTER_SANITIZE_NUMBER_INT);

if (!is_null($section_id)) {

    include __DIR__ . "/../extra/essentials.php";
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

} else {

    include __DIR__ . "/../extra/noaccess.php";

}

// Competency framework feature enabled
$frameworkEnabled = $db->features->enabled('competency-framework', $_SESSION['user_role']);

// Render new item options and controls
?>
<div class="redborderv2" id="itemadddiv-<?=$section_id?>">
<?php

// Does the actual form section exist
if ($db->forms->doesFormSectionExist($section_id)) {

    // Check for clone items
    $clone_item = filter_input(INPUT_POST, 'clone_item', FILTER_SANITIZE_NUMBER_INT);
    $clone_item_yesno = filter_input(INPUT_POST, 'clone_item_yesno', FILTER_SANITIZE_STRING);

    // Get item details for cloning
    if ($clone_item_yesno == "yes") {

        $qarray = $db->fetch_row($db->forms->getItem($clone_item));
        $itemtype = $qarray['type'];

    }

    // Get next item number and pre-select it
    $maxItemNumber = $db->forms->getHighestItemNumber($section_id);

    if ($maxItemNumber == "") {

        $maxItemNumber = 1;

    } else {

        $maxItemNumber++;

    }

    // Check for clone items
    $clone_item = filter_input(INPUT_POST, 'clone_item', FILTER_SANITIZE_NUMBER_INT);
    $clone_item_yesno = filter_input(INPUT_POST, 'clone_item_yesno', FILTER_SANITIZE_STRING);

    // Get item details for cloning
    if ($clone_item_yesno == "yes") {

        $qarray = $db->fetch_row($db->forms->getItem($clone_item));
        $itemtype = $qarray['type'];

    }

   ?>
   <div class="fl">
    <select id="item_order-<?=$section_id?>" class="sorder" title="Item Order">
    <?php

        // Render item order options
        for ($i=1; $i<=$maxItemNumber; $i++) {

            ?>
            <option value="<?=$i?>"
                <?=$i == $maxItemNumber ? ' selected="selected"' : ''?>>
                <?=$i?>
            </option>
            <?php

        }

    ?>
    </select><?php

   ?></div>
   <?php

   /**
    *  Render competencies selector template
    */
    $competencyFrameworkID = $config->competency_frameworks['competence_framework_id'];
    $frameworkLevelID = $db->competencies->getFrameworkLevelID($competencyFrameworkID);
    $competencies = $db->competencies->getList($competencyFrameworkID, $frameworkLevelID, 1);

    foreach ($competencies as &$competence) {
        $competenceID = $competence['id'];

        $descendants = $db->competencies->getList($competencyFrameworkID, $competenceID, 2);
        foreach ($descendants as $descendant) {
          $competence['descendants'][] = [
            'id' => $descendant['id'],
            'name' => $descendant['name']
          ];

        }
    }

    // Display competency sub levels yes|no
    $displaySubLevels = $config->competency_frameworks['display_sublevels'];
    $disabledInfo = $config->feature_disabled_message;

    // Template data
    $templateData = [
       'competencies' => $competencies,
       'display_sublevels' => $displaySubLevels,
       'selected' => [],
       'framework_enabled' => $frameworkEnabled,
       'framework_disabled_info' => $disabledInfo
    ];
    $template = new Template('item_competencies_selector.html.twig');
    $template->render($templateData);

?>
  <div class="qitem"><?php

    // Self Assessments Found
    $selfAssessment = $db->selfAssessments->isSectionSelfAssessment($section_id);
    $sessionExists = $db->exams->doesSessionExist($session_id);

    // Self assessment item
    if ($selfAssessment && $sessionExists) {

        ?><div id="no-selfassess" class="itemtxtdiv"><?php

        $question = $db->selfAssessments->getQuestion(
            $db->exams->getSessionExamID($session_id),
            $maxItemNumber
        );

        if (empty($question)) {

           echo "No self assessment question found for this item (matched by order number). Please import into this "
                . gettext('exam') . " the questions, responses and scores for all " . gettext('students');

        } else {

           ?><label class="selfassess">Self Assessment Question: </label>
             <span id="self-assess-<?=$section_id?>"
                   class="item-assessment-question"
                   title="Self Assessment Question">
                   <?=$question['question']
             ?></span>
           <?php

        }

      } else {

         ?><div class="itemtxtdiv"><div id="load-editor" class="red">Loading Editor...</div>
         <textarea id="item_text-<?=$section_id?>" cols="50" rows="16"
              class="itemtxt" style="visibility: hidden"></textarea><?php

      }

   ?></div>
   <br class="clearv2"/>
   </div>

   <div class="fc">
   <?php
    // Set variables required for the item type selectors
    $labelClass = "labeltext3 hoverimage";

    $radioID = "item_type_radio-" . $section_id;
    $radioRel = "&lt;img src=assets/images/radio.png /&gt;";
    $noCloneItem = ($clone_item_yesno == "no" || $clone_item_yesno == "");
    $radioClone = ($clone_item_yesno == "yes" && in_array($itemtype, ["radio", "radiotext"]));
    $checkRadio = ($noCloneItem || $radioClone) ? 'checked="checked"' : '';

    $textboxID = "item_type_text-" . $section_id;
    $textboxRel = "&lt;img src=assets/images/textbox.png /&gt";
    $checkTextbox = ($clone_item_yesno == "yes" && $itemtype == "text") ? 'checked="checked"' : '';

    $sliderID = "item_type_slider-" . $section_id;
    $sliderRel = "&lt;img src=assets/images/slider.png /&gt";
    $checkSlider = ($clone_item_yesno == "yes" && $itemtype == "slider") ? 'checked="checked"' : '';

    $checkboxID = "item_type_checkbox-" . $section_id;
    $checkboxRel = "";
    $checkCheckbox = ($clone_item_yesno == "yes" && $itemtype == "checkbox") ? 'checked="checked"' : '';

   ?>
   <label for="<?=$radioID?>" class="<?=$labelClass?>" title="Preview" rel="<?=$radioRel?>"<?=$checkRadio?>>Likert Scale</label><?php
   ?><input type="radio" name="item_type" value="radio" id="<?=$radioID?>" class="item-type" <?=$checkRadio?> onclick="changeItemType(this)"/>

   <label for="<?=$textboxID?>" class="<?=$labelClass?>" title="Preview" rel="<?=$textboxRel?>">Enter Number</label><?php
   ?><input type="radio" name="item_type" value="text" id="<?=$textboxID?>" class="item-type" <?=$checkTextbox?> onclick="changeItemType(this)"/>

   <label for="<?=$sliderID?>" class="<?=$labelClass?>" title="Preview" rel="<?=$sliderRel?>">Range Slider</label><?php
   ?><input type="radio" name="item_type" value="slider" id="<?=$sliderID?>" class="item-type" <?=$checkSlider?> onclick="changeItemType(this)"/>

   <label for="<?=$checkboxID?>" class="<?=$labelClass?>" title="Preview" rel="<?=$checkboxRel?>">Checkbox (Indicator Only)</label><?php
   ?><input type="radio" name="item_type" value="choice" id="<?=$checkboxID?>" class="item-type" <?=$checkCheckbox?> onclick="changeItemType(this)"/>

   <div id="qansoptions-<?=$section_id?>" class="ansoptioncreatediv">
   <?php

          // Clone item of textbox type
	  if ($clone_item_yesno == "yes" && $itemtype == "text") {

         include "add_textbox_option.php";

	  }

          // Clone item of slider type
	  else if ($clone_item_yesno == "yes" && $itemtype == "slider") {

             include "add_slider_option.php";

      }

           // Clone item of checkbox type
      else if ($clone_item_yesno == "yes" && $itemtype == "checkbox") {

            include "checkbox_option_add.php";

      }

          // Clone item of radio type
	  else {
	     include "add_radio_options.php";
      }

   ?>
</div>
    </div>
    <div class="bpan" id="buttonpanel-<?=$section_id?>">
     <input type="button" class="btn btn-sm btn-outline-danger" value="cancel" id="cancel_add_statitems-<?=$section_id?>" 
            onclick="canceladdItemToSection(this)"/>&nbsp;
     <?php

       // Submit button
       if (!$selfAssessment || !empty($question)) {

           ?><input type="button" class="btn btn-sm btn-success" value="add Item" id="submit_new_item-<?=$section_id?>" 
                    onclick="submitNewItem(this)"/><?php 

       }

     ?>
    </div>
   </div>
<?php

} else {

   $errorMessage = "This section does not exist for this "
    . gettext('form') . ", Please refresh page [F5 Key]";

   ?>
   <span class="boldred"><?=$errorMessage?></span>
   <?php

}
?>

