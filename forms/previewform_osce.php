<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

use \OMIS\Database\CoreDB as CoreDB;

// Critical Session Check
$session = new OMIS\Session();
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once './assess/assess_func.php';

// 'gettext' translation variables
$formLabel = ucfirst(gettext('form'));
$sectionsLabel = ucfirst(gettext('sections'));

// filter_input options to validate that the value passed in is an integer and
// greater than zero.
$filter_options_uint = ["options" => ["min_range" => 1]];

// filter_input options for where the only valid result is a zero or a one.
$filter_options_binary_int = ["options" => ["min_range" => 0, "max_range" => 1]];

// Form ID
$formID = filter_input(INPUT_GET, 'form_id', FILTER_VALIDATE_INT, $filter_options_uint);

if ($formID === \FALSE) {
    error_log(__FILE__ . "Invalid Form ID supplied as input.");
    die("<div class=\"errordiv\">$formLabel not found</div>");
} else {
    $formDB = $db->forms->getForm($formID);
}

$stationExists = false;
$stationTag = "";

/**
 *  Comments Required Classes
 *  0=>No,
 *  1=>Yes,
 *  2=>Fail,
 *  3=>Borderline,
 *  4=>Fail or Borderline
 *  5=>flag option
 */
$commentsReqClasses = [
    0 => "",
    1 => "crequired",
    2 => "crequired-fail",
    3 => "crequired-bl",
    4 => "crequired-fb",
    5 => "crequired-flag"
];

// Form Exists?
if (!is_array($formDB) || (count($formDB) == 0)) {
    error_log(__FILE__ . ": Form ID $formID is not a valid form or is an empty form");
    die("<div class=\"errordiv\">$formLabel not found</div>");
}

$deptName = $db->departments->getDepartmentName($formDB['dept_id']);
$hasNotes = (strlen($formDB['form_description']) > 0);

// From session stations section frm = station_id
$sessionID = filter_input(INPUT_GET, 'frm', FILTER_VALIDATE_INT, $filter_options_uint);

if (!empty($sessionID) && $db->exams->doesSessionExist($sessionID)) {
    $sessionDB = $db->exams->getSession($sessionID);

    // Get Exam Information
    $examInfo = $db->exams->getExam($sessionDB['exam_id']);
    $viewMarks = $examInfo['view_marks'];
    $viewTotals = $examInfo['view_totals'];

    $station_id = filter_input(INPUT_GET, 'station_id', FILTER_VALIDATE_INT, $filter_options_uint);

    if (!empty($station_id)) {
        $stationDB = $db->exams->getStation($station_id, true);
    }

    $stationExists = (!empty($stationDB));

    if ($stationExists) {
        $stationTag = $stationDB['station_code'];
    }

    // Popup notes demo
    $popupNotes = ($stationExists && $hasNotes) ? $stationDB['desc_auto_popup'] : 0;

// From station bank section
} else {
    $popupNotes = (int) filter_input(INPUT_GET, 'notes', FILTER_VALIDATE_INT, $filter_options_binary_int);
    $viewMarks = (int) filter_input(INPUT_GET, 'marks', FILTER_VALIDATE_INT, $filter_options_binary_int);
    $viewTotals = (int) filter_input(INPUT_GET, 'totals', FILTER_VALIDATE_INT, $filter_options_binary_int);

}
 
?><form method="post" action="">
    <div id="exam-assessment-top">
    <table id="top-table" class="table-border">
     <tr>
      <th class="title">Assessment <?=$formLabel?> Name</th>
      <td class="stationdettd"><?=wordwrap($formDB['form_name'], 30, "<br/>")?>
        <input type="hidden" id="form_name" value="<?=$formDB['form_name']?>"/>
        <input type="hidden" id="form_id" value="<?=$formID?>"/>
      </td>
     </tr><?php

     if (strlen($stationTag) > 0) {
      ?><tr>
    <th class="title">Station Tag</th>
    <td class="stationdettd"><?=wordwrap($stationTag, 30, "<br/>")?></td>
        </tr><?php
     }
    ?>

    <tr>
     <th class="title"><?=gettext('Department')?></th>
     <td class="stationdettd"><?php
       echo $deptName?>
       <input type="hidden" id="dept" value="<?=$deptName?>"/>
     </td>
    </tr>

    <tr>
     <th class="title vatop">Possible Station Settings</th>
     <td class="stationdettd" id="settings-td">
     <ul class="settings alt"><?php
       if ($hasNotes) {
         ?><li>NOTES AUTO POPUP = &nbsp;&nbsp;
           <select id="notes-pop" class="preview-opts custom-select custom-select-sm w-auto">
        <option value="1" class="boldgreen" <?php if ($popupNotes) { ?>selected="selected"<?php } ?> >YES</option>
	        <option value="0" class="boldred">NO</option>
       </select>
	 </li><?php
       } ?>
	 <li>VIEW ITEM MARKS = &nbsp;&nbsp;
         <select id="mar" class="preview-opts custom-select custom-select-sm w-auto">
           <option value="0" class="boldred">NO</option>
	   <option value="1" class="boldgreen" <?php if ($viewMarks) { ?>selected="selected"<?php } ?> >YES</option>
	 </select>
	</li>
	<li>VIEW <?=strtoupper($formLabel);?> TOTAL = &nbsp;&nbsp;
        <select id="tot" class="preview-opts custom-select custom-select-sm w-auto">
           <option value="0" class="boldred">NO</option>
	   <option value="1" class="boldgreen" <?php if ($viewTotals) { ?>selected="selected"<?php } ?> >YES</option>
	</select>
        </li><?php
         if ($hasNotes) {
           ?><li id="show-notes-li"><a id="show-notes" href="javascript:void(0)">preview notes</a></li><?php
         }
       ?></ul>
    </td>
   </tr>
  <tr id="status">
    <td colspan="2" class="titlestatus">
     <div>Loading <?=strtolower($formLabel);?> please wait...</div>
    </td>
  </tr>
 </table>
   <div id="clock" title="Count down to zero from when <?=strtolower($formLabel);?> was opened" unselectable="on"></div>
   <div class="fc"></div>
</div><?php

// Display station notes section if enabled
if ($hasNotes) {
  ?><div id="station-notes-outer">
    <div id="notes-title" unselectable="on">Assessment Notes</div>
    <div id="station-notes-main" unselectable="on">
         <?php echo html_entity_decode($formDB['form_description']); ?>
        <br>
         <video width="400" controls>
  <source src="media/<?php echo $stationDB['form_id']; ?>.mp4?v=<?php echo rand(); ?>" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
<?php 
      // $file_data = $db->exams->getFile($stationDB['session_id'],$stationDB['form_id']);
      //       $file = $db->fetch_row($file_data);
      //       // echo $file['file_name'];
      //       $ext = pathinfo($file['file_name'], PATHINFO_EXTENSION);
      //       if($ext == "mp4" AND $file !=""){ 
                ?>
            <!-- <video width="400" controls>
  <source src="media/upload_media/<?php // echo $file['file_name']; ?>" type="video/mp4">
  Your browser does not support HTML5 video.
</video> -->
<?php 
// }elseif($ext == "txt"  AND $file != ""){
// $file_text = fopen("media/upload_media/".$file['file_name'],"r");
// while(! feof($file_text))
//   {
//   echo fgets($file_text). "<br />";
//   }
// fclose($file_text);
// }elseif($ext == "pdf"  AND $file != ""){ 
    ?>

<!--       <iframe src="media/upload_media/<?php //echo $file['file_name']; ?>" width="100%" style="height:100%"></iframe> -->

<?php
// }else{ ?>
  <?php// echo $formDB['form_description']; 
// }
?>
   </div>
    <div id="close-div"><input type="button" id="close-notes" value="continue" class="btn btn-sm btn-success"/></div>
   </div><?php
}

// Form Sections
$totalPossible = 0;
$formSections = $db->forms->getFormSections([$formID]);
$radioCount = $db->forms->getHighestAnswerOptionCount($formID);

if (!($radioCount > 0)) {
    $radioCount = 1;
}

$sectionCount = mysqli_num_rows($formSections);

?><table border="0" id="exam_assessment_table" class="table-border"><?php

  if ($sectionCount > 0) {

    // See if at least 1 section requires feedback entry
    $tempFormSections = CoreDB::into_array($formSections);
    mysqli_data_seek($formSections, 0);

    $noFeedbackEntry = empty(array_filter(array_column($tempFormSections, 'section_feedback')));

    $sectionNumber = 0;
    $itemCount = 0;
    $optionCount = 1;

    /******************** Go Through Sections **************************/
    while ($section = $db->fetch_row($formSections)) {

     $sectionNumber++;
     $isScoreCount = 0;
     $sectionItems = [];

     $sectionID = $section['section_id'];
     $competenceType = $section['competence_type'];
     $competenceDescription = ucfirst($section['competence_desc']);
     $additionalInfo = ucfirst($section['section_text']);

     /************** Get Competency Items and ANS Options **************/
     $items = $db->forms->getSectionItems([$section['section_id']]);

     // Get num of items
     $numOfItems = mysqli_num_rows($items);
     $maxOptionCount = 0;

     if ($numOfItems > 0) {

      /* Build up Item and Options */
      while ($eachItem = $db->fetch_row($items)) {

        $sectionItems[$eachItem['item_id']]['type'] = $eachItem['type'];
        $sectionItems[$eachItem['item_id']]['highest_value'] = (float)$eachItem['highest_value'];
        $sectionItems[$eachItem['item_id']]['lowest_value'] = (float)$eachItem['lowest_value'];
        $sectionItems[$eachItem['item_id']]['item_order'] = $eachItem['item_order'];
        $sectionItems[$eachItem['item_id']]['text'] = $eachItem['text'];
        $sectionItems[$eachItem['item_id']]['grs'] = $eachItem['grs'];

        $itemOptionsDB = $db->forms->getItemOptions($eachItem['item_id']);

        if (mysqli_num_rows($itemOptionsDB) > $maxOptionCount) {
           $maxOptionCount = mysqli_num_rows($itemOptionsDB);
        }

        // Add Item Options
        $itemOptions = [];
        while ($itemOptionDB = $db->fetch_row($itemOptionsDB)) {
            $itemOptions[$itemOptionDB['option_id']] = [
                'option_value' => $itemOptionDB['option_value'],
                'descriptor' => $itemOptionDB['descriptor'],
                'option_order' => $itemOptionDB['option_order'],
                'flag' => $itemOptionDB['flag'],
                'covered' => 0
            ];
        }

        $sectionItems[$eachItem['item_id']]['options'] = $itemOptions;

      }
        // Get item with max options
        $itemWithMaxOptions = getItemWithMaxOptions($sectionItems, $maxOptionCount);
    }

    /*********************** Title Section ***************************************/

    $titleClass = $competenceType == "scale" ? "scalebar" : "";
    $titleClass .= $section['self_assessment'] ? " selfassessment-bar" : "";
    $titleCellClass = $numOfItems > 0 || $section['section_feedback'] ? "title-section" : "title-sectionv2";
    $titleColspan = $numOfItems == 0 ? $radioCount + 2  : 2;

    ?><tr class="<?=$titleClass?>"><?php

         ?><th class="<?=$titleCellClass?>" colspan="<?=$titleColspan?>"><?php
          ?><div class="word-wrap"><?php

            echo implode(" : ", array_filter([$competenceDescription, $additionalInfo]));

          ?></div><?php
         ?></th><?php

         // Cells with descriptors
         if ($numOfItems > 0 && $maxOptionCount > 0) {

            foreach ($itemWithMaxOptions['options'] as $maxItemOption) {

              ?><th class="descriptors"><?php
                 ?><div class="descdiv"><?php

                  if (!$section['self_assessment'] && in_array($itemWithMaxOptions['type'], ["radio", "radiotext"])) {

                    echo wordwrap($maxItemOption['descriptor'], 20, "<br/>");

                  }

                 ?></div><?php
               ?></th><?php

            }

        }

        // Remaining (empty) option cells
        if ($numOfItems > 0) {

          for ($v=1; $v<=$radioCount-$maxOptionCount; $v++) {

             ?><th class="descriptors"><div class="descdiv">&nbsp;</div></th><?php

          }

        }

   ?></tr><?php

   /******************************* Items Section ********************************/

    //Section item count
    $sectionItemCount = 1;
    //Total possible for each section
    $sectionPossible = 0;

    // Cycle through the section items
    foreach ($sectionItems as $itemID => $sectionItem) {

        $itemCount++;
        $itemAnsType = $sectionItem['type'];

        // Database Item High & Low Values
        $highestItemValue = (float)$sectionItem['highest_value'];
        $lowestItemValue = (float)$sectionItem['lowest_value'];

        // True Item High & Low Values
        $trueHigh = max($highestItemValue, $lowestItemValue);
        $trueLow = min($highestItemValue, $lowestItemValue);

        $itemDescription = $sectionItem['text'];
        $sectionPossible += $trueHigh;
        $isScaleItem = ($sectionItem['grs'] == 1);
        $rowID = "section-" . $sectionNumber . "-qrow-" . $itemCount;

    ?><tr <?php if ($sectionItemCount%2 == 0) { ?>class="lgrey"<?php } ?> id="<?=$rowID?>">
	  <th class="titleqcount">&nbsp;<?php if ($noFeedbackEntry) { ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php } ?></th>
      <td class="items"><?php

     /**
      * Self assessment question
      */
     if ($section['self_assessment']) {

        ?><div class="self-assessment-question"><?php

        // Exam view mode
        if ($stationExists) {

            $questionRecord = $db->selfAssessments->getQuestion(
                $sessionDB['exam_id'], $sectionItem['item_order']
            );
            echo $questionRecord['question'];

        } else {

            echo "&lt;Self Assessment Question - " . ucwords(gettext('form')) . " Bank Placeholder&gt;";

        }

        ?>
        </div>
        <div class="self-assessment-response"><span>&lt;<?=gettext('student')?> response&gt;</span></div>
        <div class="self-assessment-score"><span>&lt;<?=gettext('student')?> score&gt;</span></div><?php

     } else {

        echo adjustHTML($itemDescription);

     }

     ?></td><?php

/****************************GET ANSWER OPTIONS********************************/

    /************************** IF TEXT TYPE **********************************/
    if ($itemAnsType == "text") {
        $totalOptionCount = 1;
        $ansOptionID = key($sectionItems[$itemID]['options']);
        $ansOptionText = $sectionItems[$itemID]['options'][$ansOptionID]['descriptor'];
        $optionTextWrapped = wordwrap($ansOptionText,40,"<br>\n");
        $isScoreCount++;

      ?><td colspan="<?=$radioCount?>" class="options anslider"><?php
         ?><div class="textbox-descriptor"><?=$optionTextWrapped?></div><?php
         ?><input type="text"
                  onblur="CalculateTotal(this, 0)"
                  class="textinputtest"
                  name="qid[<?=$itemID?>]"
                  id="ansoption-<?=$optionCount?>"
                  size="4"
          /><?php
         ?><label for="ansoption-<?=$optionCount?>"> (<span class="red"><?=$trueLow." to ".$trueHigh?></span>)</label><?php
         ?><input type="hidden" id="lowhigh<?=$itemCount?>" value="<?=$trueLow.",".$trueHigh?>"/><?php
         ?><input type="hidden" name="ansoptid[<?=$itemID?>]" value="<?=$ansOptionID?>"/>
	</td><?php

        $optionCount++;
    }

    /********************************* IF SLIDER TYPE *************************/
    elseif ($itemAnsType == "slider") {
        $totalOptionCount = 1;
        $ansOptionID = key($sectionItems[$itemID]['options']);
        $isScoreCount++;

        ?><td colspan="<?=$radioCount?>" class="options anslider"><?php

        if ($lowestItemValue < 0 && $highestItemValue > 0) {
            $offset = 0;
        } else {
            $offset = $lowestItemValue;
        }

        // Generate Slider
        $sliderType = ($viewMarks) ? "numeric" : "percent";
        generateSlider(
                $lowestItemValue,
                $highestItemValue,
                $itemID,
                $optionCount,
                $offset,
                $sliderType,
                "jquery"
        );

        ?></td><?php

        $optionCount++;

    }

    /********************************* IF Checkbox Type *************************/
    elseif ($itemAnsType == "checkbox") {

        $totalOptionCount = 1;
        $optionCount++;

        ?><td colspan="<?=$radioCount?>" class="c nbr">
          <input type="checkbox" class="checkbox-control" value="1"/>
        </td><?php

    }

    /************************* IF RADIO, RADIO TEXT TYPE **********************/
    elseif (in_array($itemAnsType, ['radio', 'radiotext'])) {
        $totalOptionCount = count($sectionItems[$itemID]['options']);
        $mainCount = 0;

        /**
         * If this is a GRS item then we need to get the type
         * and the values with their borderline and fail information used
         * in the compulsory feedback feature
         */
        if ($isScaleItem) {
           $GRSValues = $db->forms->getRatingScaleValues(
                   $sectionItem['grs'],
                   NULL,
                   false,
                   'scale_value'
           );
        }


        if ($section['self_assessment']) {

              $selfAssessmentOptionCount = 1;

              // Loop through item options
              foreach ($sectionItems[$itemID]['options'] as $ansOptionID => $optionData) {

                  $ansOptionValue = $optionData['option_value'];
                  $ansOptionText = $optionData['descriptor'];

                  ?><td class="options <?php
                      if ($isScaleItem) {
                          echo " anscale";
                      }
                      if ($selfAssessmentOptionCount == 1) {
                          echo " nbl";
                      }
                      if ($numOfItems == 1) {
                          echo " vatop";
                      }

                    ?>"><?php
              if ($itemAnsType == "radio") {
                  $isScoreCount++;
              }

              /**
               * What follows is the item option
               */
              if (strlen($ansOptionText) == 0) {
                  $ansOptionText = "No Details Available";
              }

              $toolTipText = $ansOptionText;
              $radioTitle = ($isScaleItem || !$viewMarks) ? ' title="'.$toolTipText.'"' : '';
              /**
               *  If a GRS option is a fail and/or borderline
               *  then apply the appropriate classes to it's input element.
               *  These classes are checked for when the compulsory feedback is required
               *  for ticked borderline and fail GRS options
               */
              $failBlString = " ";
              if ($isScaleItem && isset($GRSValues[$ansOptionValue])) {
                 $failBorderlines = [];
                 $GRSData = $GRSValues[$ansOptionValue];
                 if ($GRSData['borderline'] == 1) {
                   $failBorderlines[] = "borderline";
                 }

                 if ($GRSData['fail'] == 1) {
                   $failBorderlines[] = "fail";
                 }

                 $failBlString .= implode(" ", $failBorderlines);
              }

              /**
               * This is a FLAG OPTION
               */
              $flagOption = ($optionData['flag']) ? " flag" : "";

              ?><input type="radio"<?php
                   echo $radioTitle?> name="qid[<?php
                   echo $itemID?>]" id="ansoption-<?php
                   echo $optionCount?>" class="radoption<?=$failBlString?><?=$flagOption?>" value="<?php
                   echo $ansOptionID?>" onclick="CalculateTotal(this, 0)"/><?php

              ?><input type="hidden" id="ansoptval:<?=$ansOptionID?>" value="<?=$ansOptionValue?>"/><?php
                $scaleTitleText = $isScaleItem ? $ansOptionText : $ansOptionValue;
                $toolTipWrapped = wordwrap($toolTipText, 40, "<br/>");
              ?><label for="ansoption-<?php
                  echo $optionCount?>" class="hmv" title="Value Details" rel="<?php
                  echo $toolTipWrapped?>"><?php
                ?>(<span class="red"><?php
                  echo $scaleTitleText?></span>)<?php
              ?></label><?php



              ?>
             </td>

             <?php
              $optionCount++;
              $selfAssessmentOptionCount++;

          }


        } else {

        // Loop through max options
        foreach ($itemWithMaxOptions['options'] as $maxItemOption) {
            $mainCount++;

            // Loop through item options
            foreach ($sectionItems[$itemID]['options'] as $ansOptionID => $optionData) {
                // If option is not covered already
                if (!$optionData['covered']) {
                    // If option descriptions match then place option in this
                    // column
                    if (compareAnsOptions($maxItemOption['descriptor'], $optionData['descriptor']) ||
                        !inMainArray($itemWithMaxOptions['options'], $optionData, $mainCount) ||
                        repeatedAns($sectionItems[$itemID]['options'], $optionData['descriptor'])) {

                        $sectionItems[$itemID]['options'][$ansOptionID]['covered'] = 1;
                        $ansOptionValue = $optionData['option_value'];
                        $ansOptionText = $optionData['descriptor'];
            ?><td class="options <?php
                if ($isScaleItem) {
                    echo " anscale";
                }
                if ($mainCount == 1) {
                    echo " nbl";
                }
                if ($numOfItems == 1) {
                    echo " vatop";
                }

              ?>"><?php
        if ($itemAnsType == "radio") {
            $isScoreCount++;
        }

        /**
         * What follows is the item option
         */
        if (strlen($ansOptionText) == 0) {
            $ansOptionText = "No Details Available";
        }

        $toolTipText = $ansOptionText;
        $radioTitle = ($isScaleItem || !$viewMarks) ? ' title="'.$toolTipText.'"' : '';
        /**
         *  If a GRS option is a fail and/or borderline
         *  then apply the appropriate classes to it's input element.
         *  These classes are checked for when the compulsory feedback is required
         *  for ticked borderline and fail GRS options
         */
        $failBlString = " ";
        if ($isScaleItem && isset($GRSValues[$ansOptionValue])) {
           $failBorderlines = [];
           $GRSData = $GRSValues[$ansOptionValue];
           if ($GRSData['borderline'] == 1) {
             $failBorderlines[] = "borderline";
           }

           if ($GRSData['fail'] == 1) {
             $failBorderlines[] = "fail";
           }

           $failBlString .= implode(" ", $failBorderlines);
        }

        /**
         * This is a FLAG OPTION
         */
        $flagOption = ($optionData['flag']) ? " flag" : "";

        ?><input type="radio"<?php
             echo $radioTitle?> name="qid[<?php
             echo $itemID?>]" id="ansoption-<?php
             echo $optionCount?>" class="radoption<?=$failBlString?><?=$flagOption?>" value="<?php
             echo $ansOptionID?>" onclick="CalculateTotal(this, 0)"/><?php

        ?><input type="hidden" id="ansoptval:<?=$ansOptionID?>" value="<?=$ansOptionValue?>"/><?php

        /**
         *  View item option marks
         */
        if ($viewMarks || $section['self_assessment']) {
             $scaleTitleText = $isScaleItem ? $ansOptionText : $ansOptionValue;
             $toolTipWrapped = wordwrap($toolTipText, 40, "<br/>");
           ?><label for="ansoption-<?php
               echo $optionCount?>" class="hmv" title="Value Details" rel="<?php
               echo $toolTipWrapped?>"><?php
             ?>(<span class="red"><?php
               echo $scaleTitleText?></span>)<?php
           ?></label><?php
        }


        ?>
       </td>

       <?php
        $optionCount++;
        break;

        } else {
            ?><td class="filler" >&ndash;</td><?php
            $totalOptionCount++;
            break;
        }
      }
    }
 }
 }

      // Any remaining TDs
      fillerTD($radioCount, $totalOptionCount, "filler");

    } else {
      ?><td>ITEM TYPE ERROR</td><?php
    }

    // Total possible score and result
    if ($sectionItemCount == $numOfItems && $isScoreCount > 0) {
        $totalPossible += $sectionPossible;
        ?><input type="hidden" id="totalsection-<?=$sectionNumber?>" value=""/><?php
    }

    $sectionItemCount++;

 ?></tr><?php

      /*
       * Item feedback field
       */
      if ($section['item_feedback']) {

         ?><tr>
            <td class="item-comment-icon-cell">
              <div>&nbsp;</div>
            </td>
            <td class="comment" colspan="<?=$radioCount+2?>">
             <textarea placeholder="item feedback.." id="item-comment-<?php
               echo $sectionItemCount?>" class="<?php
               echo $classComment?>"></textarea>
            </td>
         </tr><?php

      }

   }

  // If SCALE SECTION include in class variable
  $classComment = ($competenceType == "scale") ? " scale-comment" : "";

  // If COMMENTS are required include in css class variable
  /**
   *  0=>No,
   *  1=>Yes,
   *  2=>Fail,
   *  3=>Borderline,
   *  4=>Fail & Borderline
   *  5=>flag option
   */
   $classComment .= " ".$commentsReqClasses[$section['feedback_required']];

   ?><tr <?=($section['section_feedback']) ? '' : 'class="hide"' ?>>
    <td class="comment-icon-cell">
        <div>&nbsp;</div>
    </td>
    <td class="comment" colspan="<?=$radioCount+2?>">
    <textarea placeholder="section feedback.." name="comment-<?php
    echo $sectionID?>" id="comment-<?php
    echo $sectionNumber?>" class="<?php
    echo $classComment?>"></textarea>
    </td>
    </tr><?php

 }
 ?>
 <tr>
 <td colspan="<?=$radioCount+3?>" class="tdscorebuttons"><?php

/**************************** FINAL SCORE SECTION ******************************/
  if ($itemCount > 0) {
     $hideTotalScore = (!$viewTotals) ? ' hide': '';
     ?><div class="totalscore<?=$hideTotalScore?>">
        <span class="boldred">Total </span>
	<div id="scorediv" class="scorevalue">
        <div id="scoreval" class="innervaldiv"></div>/<?php
          echo $totalPossible;
         ?></div>
       </div>
	 <input type="hidden" id="sectioncount" value="<?=$sectionNumber?>"/>
	 <input type="hidden" id="view_marks" value="<?=$viewMarks?>"/>
      <?php
         /**
          * Get time limit set in db, if not use
          * the default set in config file
          */
         if ($stationExists) {
           $timeLimit = $stationDB['station_time'];
         } else {
           $timeLimit = $config->station_defaults['timelimit'];
         }
      ?>
         <input type="hidden" id="time_limit" value="<?=$timeLimit?>"/>
      <?php
   }

   ?></td>
  </tr>
 <?php

 } else {
  ?><tr><td class="noitemsfnd2">No <?=strtolower($sectionsLabel)?> or items found</td></tr><?php
 }

 ?></table>
</form>
<br/>
<br/>
