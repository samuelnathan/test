<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
 
 define('_OMIS', 1);
 define('CLONE_INTO_BANK', 1);
 define('MAKE_UNIQUE_TO_EXAM', 2);
 
 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';
 
 /**
  * Can user access this section?
  */
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
 
     $template = new \OMIS\Template("noaccess.html.twig");
     $template->render([
         'logged_in' => true,
         'root_url' => $config->base_path_url,
         'username' => $_SESSION['user_identifier'],
         'page' => \OMIS\Utilities\Request::getPageID()
     ]);
     
     return;
 
 }
 
 // If none of the above return to index page 
 if (!isset($_POST['clone_form'])) {
 
    header('Location: ../');
 
 }
  
 /**
  * Clone assessment form (scoresheet)
  */    
 $cloneAction = filter_input(INPUT_POST, 'clone_action', FILTER_SANITIZE_NUMBER_INT);
 $orgFormID = filter_input(INPUT_POST, 'clone_form', FILTER_SANITIZE_NUMBER_INT);
 $orgDept = filter_input(INPUT_POST, 'clone_dept', FILTER_SANITIZE_STRING);
 $orgName = filter_input(INPUT_POST, 'org_name', FILTER_SANITIZE_STRING);
 $newTerm = filter_input(INPUT_POST, 'clone_term', FILTER_SANITIZE_STRING);
 $newName = filter_input(INPUT_POST, 'new_name', FILTER_SANITIZE_STRING);
 $orgFormDesc = filter_input(INPUT_POST, 'form_desc', FILTER_SANITIZE_STRING);
 $sectionIDMap = $itemIDMap = $optionIDMap = [];
 
 $db->startTransaction();
 
 // Add form shell
 list($inserted, $newFormID) = $db->forms->insertForm(
     $newName, 
     $_SESSION['user_identifier'], 
     $orgDept, 
     ($cloneAction == CLONE_INTO_BANK ? $newTerm : $_SESSION['cterm']),
     ($cloneAction == MAKE_UNIQUE_TO_EXAM ? $orgFormDesc : "")
 );
 
 if (!$inserted) {
 
    $db->rollback();
    $_SESSION['form_cloned'] = "fail";
    header('Location: ../manage.php?page=bankform&form_id='.$orgFormID);
    exit();
    
 }
 
 // Clone form sections and items
 $prevSections = $db->forms->getFormSections([$orgFormID]);
 
 while ($prevSection = mysqli_fetch_assoc($prevSections)) {
    

        // Insert form section
        $sectionID = $db->forms->insertFormSection(
            $newFormID,
            [
                "order" => null,
                "reSortOrder" => false,
                "title" => $prevSection['section_text'],
                "competence" => $prevSection['competence_id'],
                "sectionFeedback" => $prevSection['section_feedback'],
                "itemFeedback" => $prevSection['item_feedback'],
                "feedbackRequired" => $prevSection['feedback_required'],
                "feedbackInternal" => $prevSection['internal_feedback_only'],
                "selfAssessment" => 0
            ]
        );

        // Gather section mapping
        $sectionIDMap[$prevSection['section_id']] = $sectionID;

        include 'insert_items.php';

        // Recalc form total
        $db->forms->reCalcFormTotal($newFormID);
    
 }    
    
 switch ($cloneAction) {
 
    /**
     * Clone into bank (clone action)
     */        
    case CLONE_INTO_BANK: 
 
        // Switch term
        $_SESSION['cterm'] = $newTerm;
        
        \Analog::info(
            $_SESSION['user_identifier'] . " cloned into bank ($orgDept - $newTerm) " .
            "form(id:$orgFormID) $orgName => form(id:$newFormID) $newName "
        );
 
        $db->commit();
 
        $_SESSION['form_cloned'] = "yes";
        header('Location: ../manage.php?page=bankform&form_id='.$newFormID);
        exit();
 
        break;
                
    /**
     * Make unique to exam (clone action)
    */      
    case MAKE_UNIQUE_TO_EXAM:
 
        $orgStationID = filter_input(INPUT_POST, 'station_id', FILTER_SANITIZE_NUMBER_INT);
        
        $stationRecord = $db->stations->getStation($orgStationID);
 
        $examRecord = $db->exams->getExam(
                $db->sessions->getSessionExamID($stationRecord['session_id'])
        );
 
        $switchedFeedback = $db->feedback->switchFormFeedback(
            $orgFormID,
            $examRecord['exam_id'],
            $sectionIDMap
        );

        $switchedScores = $db->results->switchFormScores(
            $orgFormID,
            $examRecord['exam_id'],
            $itemIDMap,
            $optionIDMap
        );
        
        $switchedForm = $db->exams->switchExamForm(
            $examRecord['exam_id'],
            $orgFormID,
            $newFormID
        );
        
        if (!$switchedFeedback || !$switchedScores || !$switchedForm) {
            
                error_log("Failed to switch forms, feedback and results");
                $_SESSION['form_cloned'] = "fail";
                $db->rollback();
                
                header(
                "Location: ../manage.php?page=examform" 
                . "&form_id=$orgFormID"
                . "&station_id=$orgStationID"
                . "&session_id=" . $stationRecord['session_id']
                );
                exit();
                
        }
        
        \Analog::info(
            $_SESSION['user_identifier'] . " made form(id:$orgFormID) $orgName => form(id:$newFormID) " .
            "$newName unique to exam(id:" . $examRecord['exam_id'] . ") " . $examRecord['exam_name']
        );
 
        $db->commit();
 
        $_SESSION['form_cloned'] = "yes";
        header(
            "Location: ../manage.php?page=examform" 
            . "&form_id=$newFormID"
            . "&station_id=$orgStationID"
            . "&session_id=" . $stationRecord['session_id']
        );
        exit();
        break;
    
 }
