<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

define('_OMIS', 1);

$sectionID = filter_input(INPUT_POST, 'section_id', FILTER_SANITIZE_NUMBER_INT);

if (!is_null($sectionID)) {

    include __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }

} else {

    include __DIR__ . '/../extra/noaccess.php';

}

// Get all request values
$type = filter_input(INPUT_POST, 'item_type');
$order = filter_input(INPUT_POST, 'item_order');
$text = filter_input(INPUT_POST, 'item_text');
$rangeMinimum = filter_input(INPUT_POST, 'text_rangeone');
$rangeMaximum = filter_input(INPUT_POST, 'text_rangetwo');
$optionLabel = filter_input(INPUT_POST, 'descriptor');
$weightedVal = !$config->exam_defaults['item_weighting'] ? 1 : filter_input(INPUT_POST, 'weighted_value');
$competencies = filter_input(INPUT_POST, 'competencies', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

// Get current form ID
$formID = $db->forms->getSectionFormID($sectionID);

// No form results then insert new slider item
if (!$db->results->doesFormHaveResult($formID)) {

    $itemType = $type == 'score_entry' ? 'text' : 'slider';
    $itemID = $db->forms->insertItem(
            $sectionID, $text, $itemType, $rangeMaximum, $rangeMinimum, $order, 0, 0, true, $weightedVal
    );

    $db->forms->insertOption($itemID, '1', $optionLabel);
    $db->forms->reCalcFormTotal($formID);

    // Link competencies (if any)
    $db->competencies->linkToItem($itemID, $competencies);

}
