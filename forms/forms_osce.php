  <?php
  /**
   * @author David Cunningham <david.cunningham@qpercom.ie>
   * For Qpercom Ltd
   * @copyright Copyright (c) 2018, Qpercom Limited
   */
  use \OMIS\Auth\Role as Role;
  use \OMIS\Template as Template;
  
  // Critical Session Check
  $session = new OMIS\Session;
  $session->check();

  // Page Access Check / Can User Access this Section?
  if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      return false;
  }
  
  require_once __DIR__ . '/../extra/helper.php';

  $forms = [];
  
  // Drop-down filtering options
  list($selectedDept, $selectedTerm, $filterReady,) = validateFilterOptions();

  // Prepare ordering
  $url = 'manage.php?page=forms_osce'
       . '&amp;dept=' . $selectedDept 
       . '&amp;term=' . $selectedTerm;

  $orderFields = [
      'form_name',
      'form_name',
      'dept_id',
      'author',
      'updated_at',
      'has_result',
      'design_complete'
  ];

  list($orderType, $orderIndex, $orderMap) = prepareListOrdering('asc', 0, $orderFields);

  // Forms that have results attached / deletion prohibited
  $formWarnings = [];
  if (isset($_SESSION['form_warnings'])) {

     $formWarnings = $_SESSION['form_warnings'];
     unset($_SESSION['form_warnings']);

  }
 
  // Gather station data
  if ($filterReady) {
    
     $formRecords = $db->forms->getForms(
         $selectedTerm,
         $selectedDept,
         'form_data.created_at',
         'ASC',
         true
     );

     // Forms field sort, sort index > 0
     if ($orderIndex > 0) {
        orderArrayColumn($formRecords, $orderMap[$orderIndex], 'standard', strtoupper($orderType));
     }

     // Add to form records
     foreach ($formRecords as $form) {

        $authorRecord = $db->users->getUser($form['author'], 'user_id', true);
        $formName = wordwrap($form['form_name'], 30, '<br/>');
        $mode = ($db->forms->doesFormHaveItems($form['form_id']) ? "edit" : "add");
        
        $forms[] = [
           'id' => $form['form_id'],
           'name' => $formName,
           'tags' => $db->forms->getFormTags($form['form_id']),
           'link' => "manage.php?page=bankform&amp;form_id=".$form['form_id']
               .'&amp;dept='.$selectedDept.'&amp;term='.$selectedTerm,
           'authorID' => $form['author'],
           'authorName' => $authorRecord['forename']." ".$authorRecord['surname'],
           'departmentID' => $form['dept_id'],
           'departmentName' => $db->departments->getDepartmentName($form['dept_id']),
           'updated' => format_DateTime($form['updated_at']),
           'complete' => $form['design_complete'],
           'mode' => $mode,
           'formWarnings' => (isset($formWarnings[$form['form_id']]) ? $formWarnings[$form['form_id']] : [])
        ];

     }

  }

   // Get all terms
   $terms = $db->academicterms->getAllTerms();
    
   // Get departments
   if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {

      $deptRecords = $db->departments->getAllDepartments(true);

   } else if ($_SESSION["user_role"] == Role::USER_ROLE_SCHOOL_ADMIN) {
        
      $adminSchools = $db->schools->getAdminSchools($_SESSION["user_identifier"]);
      $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
      $deptRecords = $db->departments->getDepartments(null, true, $schoolIDs);
        
   } else {
      $deptRecords = $db->departments->getExaminerDepartments(
          trim($_SESSION['user_identifier']),
          null,
          true
      );
   }
   
   // Group by school
   $schools = OMIS\Database\CoreDB::group(
        $deptRecords,
        ['school_description'],
        true
   );

   // Render twig template
   $template = new Template(Template::findMatchingTemplate(__FILE__)); 
   $template->render([
      'filterReady' => $filterReady,
      'sortType' => $orderType,
      'sortIndex' => $orderIndex,
      'schools' => $schools,
      'forms' => $forms,
      'filterTags' => $db->forms->getTags($selectedTerm, $selectedDept),
      'stickies' => isset($_SESSION['stick_options']) ? $_SESSION['stick_options'] : [],
      'selected_department' => $selectedDept,  
      'selected_term' => $selectedTerm,
      'terms' => $terms,
      'url' => $url
   ]);
