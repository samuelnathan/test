<?php
/**
 * Collection of assorted useful functions
 *
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
*/

define('_OMIS', 1);

$mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);
$formID = filter_input(INPUT_POST, 'formid', FILTER_SANITIZE_NUMBER_INT);
$formName = filter_input(INPUT_POST, 'formname');
$formDescription = filter_input(INPUT_POST, 'formdesc');
$formComplete = filter_input(INPUT_POST, 'complete', FILTER_SANITIZE_NUMBER_INT);
$poptype = filter_input(INPUT_POST, 'poptype');
$tagString = filter_input(INPUT_POST, 'tags', FILTER_SANITIZE_STRING);
// $files = filter_input(INPUT_POST, 'files', FILTER_SANITIZE_STRING);
// $files = $_FILES['files'];
// print_r($_POST['files']);
// die();
  
// Lets check the passed in parameters
if (!empty($mode) && $mode == 'update_form_details' && 
    !empty($formID) && !is_null($formComplete) &&
    !empty($formName) && !is_null($formDescription)) {

    // Include database connection
    require_once __DIR__ . '/../extra/essentials.php';
   
     // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    // If user does not exist return error status message
    if (!$db->users->doesUserExist($_SESSION['user_identifier'])) {
        die("USER_NON_EXIST");
    }

    // If form does not exist return error status message
    if (!$db->forms->doesFormExist($_POST['formid'])) {
        die("FORM_NON_EXIST");
    }
// echo $poptype;
    // Update Form
    $db->forms->updateForm(
        $formID,
        $formName,
        $formComplete,
        $poptype,
        $_SESSION['user_identifier'],
        $formDescription
    );
    // $filename = $this->getRequest()->getFileName('img');
    // $this->getRequest()->moveFile('img','/media/'.$filename);

// if($_FILES["img"]["name"] !== ''){
//                     // $data = 1;
//                       $data = move_uploaded_file($_FILES["img"]["tmp_name"],$_FILES["img"]["name"]);
//                       echo $data;
//                       echo "file found";
//                     }else{
//                      $data = 0;
//                      echo $data;
//                      echo "file not found";
//                     }
    // print_r($_POST['files']); 
    // print_r($_FILES); 

    // Update tags
    $tags = (!is_null($tagString) && strlen($tagString) > 0) 
       ? explode(',', $tagString) : [];
    
    $db->forms->linkFormTags(
        $formID, 
        $tags
    );
    
} else {
    include __DIR__ . '/../extra/noaccess.php';
}
