<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  */
 
 if (!defined('_OMIS')) {
    define('_OMIS', 1);
 }
 
 if (isset($_POST['isajaxcall'])) {
    include __DIR__ . '/../extra/essentials.php';

    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }

    $itemID = iS($_POST, 'item_id');
    $sectionID = $db->forms->getSectionIDByItem($itemID);
    $formHasResult = $db->results->doesFormHaveResult(
        $db->forms->getSectionFormID($sectionID)
    );
    $itemRecord = $db->forms->getItem($itemID);
 
 } else {
     
    // Critical Session Check
    $session = new OMIS\Session;
    $session->check();
 }
 
 mysqli_data_seek($itemRecord, 0);
 
 if ($itemRecord && mysqli_num_rows($itemRecord) > 0) {

    $itemOptions = $db->fetch_row(
        $db->forms->getItemOptions($itemID)
    ); 

    ?>
    <div class="indivopts">
      <input type="hidden" id="checkbox-descriptor-<?=$itemID?>" value=""/>
      <input type="hidden" id="checkbox-unchecked-<?=$itemID?>" value="0"/>
      <label for="checkbox-checked-<?=$itemID?>" class="ltxt">Checked Value (Non-Grade)</label>
      <input type="text" id="checkbox-checked-<?=$itemID?>" size="2" maxlength="3" 
       value="<?=$itemOptions['option_value']?>"<?php 
       if ($formHasResult) { 
           ?> disabled="disabled"<?php 
       } 
      ?>/>
    </div>
    <?php

} else {
    ?><br/>
    <span class="boldred">&nbsp;&nbsp; 
        This item does not exist in this section, please refresh page [F5 Key].
    </span>
    <br/>
    <?php
} ?>
