<?php
/**
 * Page to manage roles
 * 
 * This page should only be accessible to super admins, and is used to create 
 * and destroy roles in the system
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
/* TODO: Figure out the consequences of deleting a role from the system, what it 
 * affects, and whether we should be looking at creating some sort of archiving
 * system like has been done (or is being done) for stations, competences, etc.
 */

global $page;
global $db;

#Critical Session Check
$session = new OMIS\Session;
$session->check();

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once __DIR__ . '/../extra/helper.php';
require_once __DIR__ . '/../osce/osce_functions.php';

// This gets the list of roles in the relevant sort order.
define('SORT_FIELD_DEFAULT', 'role_id');
define('SORT_ORDER_DEFAULT', 'asc');
$sort_field = isset($_GET['order_by']) ? $_GET['order_by'] : SORT_FIELD_DEFAULT;
$sort_direction_alpha = isset($_GET['direction']) ? $_GET['direction'] : SORT_ORDER_DEFAULT;

$sort_direction = ((strtolower(trim($sort_direction_alpha)) === 'desc') ? SORT_DESC : SORT_ASC);

// Make sure the field exists...
$sort_field_regex = '/^' . $sort_field . '$/i';
$matching_fields = $db->getTableColumns('roles', $sort_field_regex);

// If it doesn't exist, default to the norm.
if (empty($matching_fields)) {
    // TODO: Figure out nicer error message for this.
    echo "No such field $sort_field. Falling back on " . SORT_FIELD_DEFAULT;
    $sort_field = SORT_FIELD_DEFAULT;
}

$roles = $db->users->getRoles(null, $sort_field, $sort_direction);
?>
<div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Role Management Tool (DEFUNCT)
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      This tool allows a super administrator to add, edit and delete role types.
        <div class="warnfo">[<span>Currently only available to Super Administrators</span>]</div>
    </div>   
  </div> 
</div> 
    <div class="topmain-clear"></div>
</div>
<div class="contentdiv">
    <form name="role_list_form" id="role_list_form">
        <input type="hidden" name="count" id="count" value="<?=count($roles);?>"/>
        <table class="table-border" id="role_list_table">
            <tr class="title-row">
                <td class="all-checkbox-td"><input type="checkbox" id="check-all"/></td>
<?php
    foreach (range(1, 4) as $field_id) :
?>
                <th class="fw-normal"><a class="btn-link" href="#">Field <?= $field_id; ?></a></th>
<?php
    endforeach;
?>
                <td>&nbsp;</td>
            </tr>
<?php foreach ($roles as $role): ?>
            <tr class="datarow" id="role_row_<?=$role->role_id;?>">
                <td class="checkb">
                    <input type="checkbox" name="role_id[]" id="role_id[<?=$role->role_id;?>]" value="<?=$role->role_id;?>"/>
                </td>
                <td class="w0"><?=$role->name;?></td>
                <td class="w0"><?=$role->shortname;?></td>
                <td class="w0"><?=$role->description;?></td>
                <td class="w0"><?=$role->importexport_field;?></td>
                <td class="titleedit2"><?php editButton('edit_' . $role->role_id, 'Edit', 'Edit'); ?></td>
            </tr>
<?php endforeach; ?>
            <tr id="base-tr">
                <td colspan="6" class="base-button-options" id="base-button-td">
                    <div class="fl">
                        <select name="todo" id="todo" disabled="disabled" class="custom-select custom-select-sm">
                            <optgroup label="delete roles from system">
                                <option value="delete" title="Delete accounts selected from system" class="orange">delete</option>
                            </optgroup>
                        </select>
                        <input type="button" value="go" name="role_task" id="role_task" disabled="disabled" class="btn btn-outline-default btn-sm" />
                    </div>
                    <div class="fr" id="add-div">
                        <!-- Other Variables -->
                        <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm" />
                        <input type="hidden" value="<?= count($roles); ?>" name="count" id="count" />
                        <input type="hidden" value="<?= $sort_field; ?>" name="order_by" />
                        <input type="hidden" value="<?= $sort_direction_alpha; ?>" name="direction" />
                    </div>				
                </td>
            </tr>
        </table>
    </form>
</div>
