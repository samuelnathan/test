<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Download Feedback for Students
 */

use OMIS\Utilities\Path as Path;
use OMIS\Utilities\ZipFile as ZipFile;
define('_OMIS', 1);

try {

    $examIDParam = filter_input(INPUT_POST, 'download_feedback_exam', FILTER_SANITIZE_NUMBER_INT);
    $sessionIDParam = filter_input(INPUT_POST, 'download_feedback_session', FILTER_SANITIZE_NUMBER_INT);

    // Exam ID is not empty then include necessary classes
    if (!empty($examIDParam)) {
        require_once __DIR__ . '/../extra/essentials.php';  
        $featureAccess = $db->features->enabled('feedback-download', $_SESSION['user_role']);  
        if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess() || !$featureAccess) {
            throw new Exception("You are unauthorised to use the feedback download feature, " . 
            "please contact your administrator for assistance");
        }
    } else {
       throw new Exception("Bad request data, cannot continue");
    }

    // Does session exist
    if (!$db->exams->doesExamExist($examIDParam)) {
       throw new Exception("Bad request data, Exam identifier not specified");
    }

    // Get exam record
    $examRecord = $db->exams->getExam($examIDParam);

    // Session ID specified and exists? Then filter by Session ID also
    $sessionID = (!empty($sessionIDParam) && $db->exams->doesSessionExist($sessionIDParam)) ? $sessionIDParam : null;

    // Get Feedback Records
    $feedbackRecords = $db->feedback->getStudentFeedbackRecords(
        $examIDParam,
        $sessionID
    );

    // We don't have any feedback records to download, abort
    if (mysqli_num_rows($feedbackRecords) == 0) {
        throw new Exception("No feedback records to export");
    }

    // Define the path, the location of the feedback files
    $feedbackFolder = Path::join($config->tmp_path, 'feedback');

    // Check for existance of Feedback Folder
    if (!is_dir($feedbackFolder)) {
       error_log(__FILE__ . " : Could not find feedback folder: $feedbackFolder. Exiting.");
       throw new Exception("Could not find feedback resource on server, please contact system technical support");
    }

    // Create zip archive ready to export
    $zipFileName = "feedback ". sanitizeFileName(
        replaceCharacter(
            strtolower($examRecord['exam_name'])
    ));
    
    $zipFilePath = Path::join($feedbackFolder, $zipFileName . ".zip");
    $feedbackZipFile = new ZipFile($zipFilePath, ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Add all found feedback files
    $addCount = 0;
    while ($each = $db->fetch_row($feedbackRecords)) {

        // Default status per cycle
        $status = false;

        // Feedback record has been created
        if ($each['time_created'] != null) {
        
            // Retrieve PDF containing the student feedbck
            $pdfName = $each['student_id'] . '_' . $examIDParam . '.pdf';
            $feedbackFilePath = Path::join($feedbackFolder,  $pdfName);

            if (file_exists($feedbackFilePath)) {

                // Add the PDF to the archive
                $status = $feedbackZipFile->addFile(
                    $feedbackFilePath, 
                    $zipFileName . "/" . sanitizeFileName(
                       replaceCharacter(
                          $each['surname'] . '_' . $each['forename']
                          . '_' . $each['student_id'] . '_' . $examIDParam
                       )
                    ) . '.pdf'
                );

            // ERROR: could not find feedback file (PDF), tell user by placeholder txt file! 
            } else {

                $message = "Failed to find feedback file: $pdfName for " . gettext('student') . ": " .
                $each['student_id']. ". Please attempt to generate feedback again";
                error_log(__FILE__ . " : " . $message);
                
                // Add error file to archive
                $status = $feedbackZipFile->createFile(
                    $zipFileName . "/" . sanitizeFileName(
                        replaceCharacter($pdfName)
                    ) . "_FILE NOT FOUND.txt",
                    $message
                );

            }

            // Increment file add count
            if ($status) {
               $addCount++;
            }

        }

    }

    // Oh ooh, no files added, tell user by error file download
    if (count($addCount) == 0) {
        throw new Exception("Failed to add feedback files to zip archive, please contact system technical support");
    }

    // Destroy, in turn close archive object
    unset($feedbackZipFile);

    // File name for export
    $fileNameExport = sprintf(
        "%s.zip",
        $zipFileName
    );

    // ZIP archive ready for the browser
    header("Content-disposition: attachment;filename=\"$fileNameExport\"");
    header("Content-type: application/zip");
    header("Pragma: no-cache"); 
    header("Expires: 0");
    readfile($zipFilePath);

    // Log who downloaded the feedback ZIP file
    \Analog::info(
        $_SESSION['user_identifier'] . " downloaded feedback for " 
    . $examRecord['exam_name'] . "(ID:$examIDParam)"
    );

    // Tidy up please
    if (file_exists($zipFilePath)) {
        unlink($zipFilePath);
    }

/**
 * Abort download if exception thrown and inform user by spitting 
 * out error logs (in place of ZIP file)
 */
} catch (Exception $e) {
    header("Content-disposition: attachment;filename=\"feedback_download_failed.txt\"");
    header("Content-type: text/plain");
    header("Pragma: no-cache"); 
    header("Expires: 0");
    error_log(__FILE__ . $e->getMessage());
    echo $e->getMessage();
    exit();
}

?>
