# Import file template directory

This directory contains the templates for the importing tool. These templates are picked up automatically
by the system.
The system creates an associative array in the form `name => path` where `name` is the name of the template
and `path` the relative path from the web root.

The `name` is obtained by the directory where the template is stored. And `path` is the path of that template file.

Therefore, the following directory structure:
```
import
├── courses_modules
│   └── template.csv
├── exam_team
│   └── plantilla.csv
├── readme.md
└── students
    └── students.csv
```

Will generate the following array:
```php
[
    'courses_modules' => 'path/of/observe/storage/custom/import/courses_modules/template.csv',
    'exam_team' => 'path/of/observe/storage/custom/import/exam_team/plantilla.csv',
    'students' => 'path/of/observe/storage/custom/import/courses_modules/students.csv'
]
```