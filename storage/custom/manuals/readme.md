# Manuals directory

This directory contains the manuals that are displayed in the homepage.
The system picks up the files to create the `$config->manuals` array automatically.

The array generated has the following format:
```php
[
    [
        'name' => '<Name of the pdf file (without the extension)>',
        'link' => '<Path of the pdf file relative to the web root>',
        'timestamp' => '<Unix timestamp of the file (for sorting purposes)>'        
    ],
    [
        'name' => ...,
        'link' => ...,
        'timestamp' => ...
    }
]
```

The elements of the array are sorted by timestamp, name ascending