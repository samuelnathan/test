# Custom templates
This folder contains the twig templates custom for each client. Currently, only
the template for Single Sign-On is required in case the SSO is enabled in
the configuration file.


## Single Sign-On
The file is referenced in the `login.html.twig` template. It must be named `sso_form.html.twig` 