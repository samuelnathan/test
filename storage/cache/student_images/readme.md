About This Folder
=================

This folder contains the image cache for student images for OMIS. This file
exists here as much to ensure that the folder structure gets stored by git as 
anything else, but it also serves as a reminder that (in production) this folder 
contains valuable student data and should not be deleted lightly.
