<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * @Manage Exam Student Feedback Tool
 */
 use \OMIS\Database\CoreDB as CoreDB;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }
 
 require_once __DIR__ . '/../extra/helper.php';
 
 $sterm = selectedTerm();
 $schoolIDs = $deptIDs = null;

 $templateData = [
     'terms' => $db->academicterms->getAllTerms(),
     'selectedTerm' => $sterm
 ];
  
 if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SCHOOL_ADMIN) {
   
    $adminSchools = $db->schools->getAdminSchools(
       $_SESSION['user_identifier']
    );
   
    $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
      
 } else if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_EXAM_ADMIN) {
   
    $deptIDs = $db->departments->getExaminerDepartmentIDs(
           $_SESSION['user_identifier'], 
           $sterm
    );
       
 }
  
 $examRecords = CoreDB::group(
        CoreDB::into_array(
      $db->exams->getListFiltered($sterm, $schoolIDs, $deptIDs)
 ), ['school_description', 'dept_id', 'exam_id'], true, true);
   
 $templateData['schools'] = $examRecords;
 
 // Load Feedback Templates
 $templatesDB = $db->feedback->getEmailTemplates();
 $templates = [];
 
 while ($template = $db->fetch_row($templatesDB)) {
    // Ignore log/training templates.
    if (!in_array($template['template_id'], ['tp11', 'tp12'])) {
        $templates[] = $template;
    }
 }
 
 $templateData['templates'] = $templates;
 
 $templateData['competency_framework_enabled'] = 
 $db->features->enabled('competency-framework', $_SESSION['user_role']);
 
 $templateData['feedback_download_enabled'] = 
 $db->features->enabled('feedback-download', $_SESSION['user_role']);
 
 $templateData['feedback_send_enabled'] = 
 $db->features->enabled('feedback-system', $_SESSION['user_role']);
 
 $selectedTemplate = filter_input(INPUT_GET, 'template', FILTER_SANITIZE_NUMBER_INT);
 
 if (!is_null($selectedTemplate)) {
    $templateData['selected_template'] = $selectedTemplate;
 }
 
 $presets = $db->userPresets->get();
 $templateData['presets'] = $presets;

 $templateData['validate_emails'] = isset($config->feedback_tool['validate_recipient_emails']) ?
        $config->feedback_tool['validate_recipient_emails'] : true;
 
 // Load and render the template.
 $template = new OMIS\Template("feedback_tool.html.twig");
 $template->render($templateData);
 