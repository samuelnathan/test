<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

?>
<div class="topmain">
<div class="text-center">
<div class="card mt-4 d-inline-block align-top mr-2 is-request">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center text-left">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Forgotten Password
        </span>
      </div>
    </div>
  </div>
  <div class="card-body text-left">
    <div class="card-text">
      Complete the <span class="boldred">user identifier</span> &amp; <span class="boldred">email address</span> fields below for a<br/>password reset.<br/><br/>
              An email with a new password will be sent to you on request provided that<br/>your account is activated.
    </div>   
  </div> 
</div> 
</div>
    <div class="topmain-clear"></div>
</div>
<div class="contentdiv">
   <?php

  $request = filter_input(INPUT_POST, 'request', FILTER_SANITIZE_STRING);
  $identifier = filter_input(INPUT_POST, 'identifier', FILTER_SANITIZE_STRING);
  $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

   if (!is_null($request) && !is_null($identifier) && !is_null($email)) {

    if (!$db->users->doesUserAccountExist($identifier, $email)) {

      ?>
      <div class="password-request-fail">User Identifier 
       <span class="bold"><?=htmlspecialchars($identifier)?></span> and/or Email Address <span class="bold"><?=$email?></span>
        are either incorrect, inactive or not found in the system,<br />
        Please click <a href="manage.php?page=request">here</a> to try again
      </div>
      <?php

    } else {

        //gen password
        $userpass = trim(genPassword(10));
        $user_details = $db->users->getUser($identifier, "user_id", \TRUE);

        $subject = sprintf("New Password %s", $config->getName());
        $email_template = new \OMIS\Template('forgot_password.html.twig');
        /* Render the template to make the message and word wrap it so that
         * it'll not exceed 70 characters in width so that it will fit in all
         * mail clients.
         */
        $message = wordwrap(
            $email_template->render(
                [
                    'firstname' => $user_details['forename'],
                    'new_password' => $userpass,
                    'app_name' => $config->getName()
                ],
                \TRUE
            ),
            70
        );
        
        // Instantiate the mailer object and tell it we're using SMTP.
        $mailer = new PHPMailer(TRUE);
        $mailer->isSMTP();
        
        try {
            $mailer->Host = $config->mailserver['host'];
                       
            // All debugging information outputted to apache error log file
            $mailer->Debugoutput = 'error_log';            
            
            $mailer->SMTPAuth = \TRUE;
            $mailer->Port = $config->mailserver['port'];
            $mailer->Username = $config->mailserver['username'];
            $mailer->Password = $config->mailserver['password'];
            $mailer->setFrom($config->email['from_address']);
            $mailer->isHTML(\TRUE);
            
            $mailer->Subject = $subject;
            $mailer->msgHTML($message);
            $mailer->addAddress($email);
            
            $logFail = "system failed to send password reset email to user "
                     . $identifier . " (password reset section)";
            
            if ($db->users->updateUserAccount($user_details['email'], $identifier, $userpass)) {
                echo "<p>New Password Generated. Sending email...</p>\n";
                ob_flush();
                
                // Mail sent, inform user and add log entry
                if ($mailer->Send()) {
                  echo "<p>Password email sent to $email. Please check your inbox (and your junk folder, just in case!)</p>\n";
                  $logSuccess = "system password reset email sent to user "
                              . $identifier . " (password reset section)";
                  \Analog::info($logSuccess);
                  
                // Mail failed to send, inform user and add log entry
                } else {
                  echo "<p>Failed to send password reset email to $email. Please contact your administrator for further assistance</p>\n";
                  \Analog::error($logFail);
                }
                
            }
        } catch (phpmailerException $ex) {
            echo $ex->errorMessage();
            error_log(__FILE__ . ": Failed to send password recovery email to $email. Reason: " . $ex->errorMessage());
            \Analog::error($logFail);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            error_log(__FILE__ . ": Failed to send password recovery email to $email. Reason: " . $ex->getMessage());
            \Analog::error($logFail);
        }
    }

} else {

?>
    <div class="requestform" id="logintable">
        <form name="requestform" id="requestform" method="post" action="manage.php?page=request" class="card">
        <div class="card-body">
          <div class="form-group">
              <label for="id">
              User Identifier
              </label>
              <input class="form-control" autofocus type="text" name="identifier" id="identifier" size="32" value="<?=iS($_SESSION, 'userinputid'); ?>"/>
              <div id="identifier-error" class="infotd"></div>
          </div>
          <div class="form-group">
              <label for="id">
              Email Address
              </label>
              <input class="form-control" type="text" name="email" id="email" size="40"/>
              <div class="infotd" id="email-error"></div>
          </div>

          <div class="form-group">
            <input type="hidden" name="request" value="request"/>
            <input type="button" name="requestbutton" id="request-button" class="btn btn-success w-100" value="request"/>
          </div>
          
          </div>

        </form>
    </div>        
<?php

    }

    ?>
</div>
<!-- <style type="text/css">
  .right-background{
    background-image: none;
  }
</style> -->