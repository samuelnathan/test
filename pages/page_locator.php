<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * 
 * This page contains all references to all other pages.
 * This is used for the menu structure.
 */

use \OMIS\Session\FlashList as FlashList;
require_once __DIR__ . '/../classes/class.Breadcrumbs.inc.php';

/* A list of $pageFilter values (see the huge master "switch" statement in 
 * extra/header_html.php) for whom we don't want to hide how they look as
 * they populate.
 * For example, examinees_processimages outputs live, unbuffered status messages
 * about how it's getting on importing students.
 */
$suppress_loading_screen_spages = ['processimages'];

/* Decide if we want the "content loading please wait" div or not based on if we
 * happen to be on a page where it's not wanted.
 */
$show_loading_div = (!in_array($pageFilter, $suppress_loading_screen_spages));
?>
<div id="wrapper">
<div id="top" class=" border-bottom">
<div class="container-fluid">
<?php 
  require 'layout/header.php';
?>
</div>
</div>
<?php
    if ($show_loading_div) { 
?>
    <div id="loading">content loading please wait.....</div>
    <div id="main" class="container-fluid">
<?php 
    } else {
        /* Need to override the visibility of the "main" div here as it's
         * default state is display: none...
         */
?>
    <div id="main" style="display: block" class="container-fluid">
<?php
    }
/* @TODO It's generally considered poor practice to use a constructor to
 * generate output as a side-effect. This should get changed when there's time.
 */
new Breadcrumbs;

(new FlashList())->display();

if (!isset($page)) {
    global $page;
}

switch ($page) {
    // Main Page
    case 'main': 
        include 'pages/main.php';
        break;

    // Manage Features
    case 'manage_features':
        include 'features/manage_features.php';
        break;

    // Admin Tools
    case 'admin_tool':
        include 'tools/admin_tool.php';
        break;

    // Swap Tool
    case 'swap_tool':
        include 'tools/swap_tool.php';
        break;

    // Station Tool
    case 'station_tool':
        include 'tools/station_tool.php';
        break;

    // Security Log
    case 'loginman_tool': 
        include 'tools/loginman_tool.php';
        break;
    // Log Tool
    case 'userlog_tool':
        include 'tools/log.php';
        break;
    
    // Feedback Tool
    case 'feedback_tool': 
        include 'tools/feedback_tool.php';
        break;
    
    // Role management tool
    case 'role_tool': 
        include 'tools/role_tool.php';
        break;
    
    // Global Rating Scale Pages in Admin Tools Section
    case 'grs_types_tool':
        include 'tools/grs_types_tool.php';
        break;
    case 'grs_values_tool':
        include 'tools/grs_values_tool.php';
        break;
    
    // Manage Terms
    case 'manage_terms':
        include 'terms/manage_terms.php';
        break;
    
    // Account Page
    case 'account':
        include 'pages/account.php';
        break;

    // Request Password Page
    case 'request':
        include 'pages/request.php';
        break;

    // Manage Examiner
    case 'manage_examiners':
        include 'examiners/manage_examiners.php';
        break;

    // Edit Examiner
    case 'edit_examiners':
        include 'examiners/edit_examiners.php';
        break;

    // Manage Admins
    case 'man_admin':
        include 'admins/man_admin.php';
        break;

    // Add Student Modules
    case 'update_examinees':
        include 'examinees/update_examinees.php';
        break;

    // Manage Examinees
    case 'manage_examinees':
        include 'examinees/manage_examinees.php';
        break;

    // Osces (Exams)
    case 'manage_osce':
        include 'osce/manage_osce.php';
        break;

    // upload Osces (Exams)
    case 'upload':
        include 'osce/upload.php';
        break;
    
    
    // Manage advanced exam settings
    case 'settings_osce': 
        include 'osce/settings_osce.php';
        break;

    // Sessions
    case 'sessions_osce':
        include 'osce/sessions_osce.php';
        break;

    // Stations Bank
    case 'forms_osce':
        include 'forms/forms_osce.php';
        break;

    // Groups
    case 'groups_osce':
        include 'osce/groups_osce.php';
        break;

    // Station Examiners
    case 'examiner_osce':
        include 'osce/examiner_osce.php';
        break;

    // Stations
    case 'stations_osce':
        include 'osce/stations_osce.php';
        break;

    // Assistants
    case 'assistants_osce':
        include 'osce/assistants_osce.php';
        break;
    
    // Examiners
    case 'examiner_osce':
        include 'osce/examiner_osce.php';
        break;

    // Session Stations
    case 'tdstations_osce':
        include 'osce/tdstations_osce.php';
        break;

    // Station Items Page
    case 'examform':
    case 'bankform':
        include 'forms/formbuilder.php';
        break;

    // Manage Schools
    case 'manage_schools':
        include 'schools/manage_schools.php';
        break;

    // Manage Depts
    case 'manage_departments':
        include 'schools/manage_departments.php';
        break;

    // Manage Courses
    case 'manage_courses':
        include 'schools/manage_courses.php';
        break;

    // Manage Course Years
    case 'manage_years':
        include 'schools/manage_years.php';
        break;

    // Manage Course Modules
    case 'manage_modules':
        include 'schools/manage_modules.php';
        break;

    // First Step in Importing Process
    case 'importingStep1':
        include 'import/data_import_step1.php';
        break;

    // Second Step in Importing Process
    case 'importingStep2':
        include 'import/data_import_step2.php';
        break;

    // Third Step in Importing Process
    case 'importingStep3':
        include 'import/data_import_step3.php';
        break;

    // Fourth and Last Step of Importing Process
    case 'importingStep4':
        include 'import/data_import_step4.php';
        break;
    
    // Assistants
    case 'assist':
        include 'assistants/assist.php';
        break;
    
    case 'examinees_uploadimages':
        // Upload examinee (student) images
        include 'examinees/examinees_uploadimages.php';
        break;
    
    case 'examinees_processimages':
        // Process examinee (student) images
        include 'examinees/examinees_processimages.php';
        break;

    // Error Handling:
    default:
        include 'pages/error.php';
        break;
}

  /**
   * Enable pubble (setting in configuration file)
   * Only when we have a session
   */
   if (isset($_SESSION['user_identifier'])) {
     
      $pubbleTemplate = new \OMIS\Template("pubble.html.twig");

      $data = [
        'pubble' => $config->support['pubble']
      ];

      // Add user full name
      if (isset($_SESSION['user_forename'], $_SESSION['user_surname'])) {
         $data['user_fullname'] = $_SESSION['user_forename'] . " " . $_SESSION['user_surname'];
      }

      // Add user email address
      if (isset($_SESSION['user_email'])) {
         $data['user_email'] = $_SESSION['user_email'];
      }

      // Render pubble bubble
      $pubbleTemplate->render($data);
      
   }
  
?>
  </div>
 </div><?php
 
 (new OMIS\Template('footer.html.twig'))->render([
    'skin' => $config->localization['skin'],
 ]);
 