<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

    return false;

 }

 $user = $db->users->getUser(
     $_SESSION['user_identifier'],
     'user_id',
     true
 );

 $data = [
    'user' => $user,
    'redirect_url' => 'pages/pages_redirect.php',
    'username' => $_SESSION['user_identifier'],
    'pwdStrength' => [
       'verdictList' => ['weak', 'normal', 'medium', 'strong', 'very strong'],
       'settings' => (isset($config->passwords) ? $config->passwords : [])
    ]
 ];

 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($data);
