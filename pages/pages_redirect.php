<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Redirect file for pages section e.g. account (might be a good idea to move 
 * the account code files into a separate directory)
 */

define('_OMIS', 1);
$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

/* As this page doesn't return a rendered HTML page, then there's no sense in it
 * returning a HTML error message. Instead, we return the next best thing - 
 * HTTP status message saying "your credentials are no good... right now". This
 * is why this is a 401 rather than a 403 (Forbidden) in case the browser opts
 * to not check again...
 */
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    http_response_code(401); // Unauthorized
    return;
}

// Okay, we're going to return JSON.
header("Content-Type: application/json");

$account_submit = filter_input(INPUT_POST, 'account_submit', FILTER_SANITIZE_STRING);

if (is_null($account_submit)) {
    //If none of the above return to index page    
    header('Location: ../');
}

// Update account information
if ($account_submit == 'change') {

    //forename_ip surname_ip email_ip password_ip
    $userID = $_POST['user_id'];
    $firstname = $_POST['forename_ip'];
    $surname = $_POST['surname_ip'];
    $email = $_POST['email_ip'];
    $password = $_POST['password_ip'];

    //Check Password
    if ($db->users->checkPassword($userID, $password)) {
        //Update user
        $updated = $db->users->updateUser($userID, null, $surname, $firstname, $email);

        //Update user session variables
        if ($updated) {
            $_SESSION['user_forename'] = $firstname;
            $_SESSION['user_surname'] = $surname;
            $_SESSION['user_email'] = $email;
            // Echo the status as JSON.
            echo '{"success": true}';
        }
    } else {
        echo '{"success": false}';
    }
    
 // Update account password
} elseif ($account_submit == 'password_change') {
    
   
    $userID = $_POST['user_id'];
    $password = $_POST['password_ip'];
    $newPassword = $_POST['new_pass'];

    if ($db->users->checkPassword($userID, $password)) {
        if (mysqli_num_rows($db->users->getUser($userID)) > 0) {
            $success = $db->users->changeUserPassword($userID, $newPassword);

            // The user requested to email the password reminder
            if (filter_input(INPUT_POST, 'email_pass', FILTER_SANITIZE_NUMBER_INT) == 1) {
               $userRecord = $db->users->getUser($userID, 'user_id', true);
                $userEmail = $userRecord['email'];
                $subject = $config->getName() . " New Password Reminder";
                $message = "Hello " .$userRecord['forename'] . ", \n\n" .
                        "Your new login password is: \n\n" . $newPassword . " \n\n" .
                        "Kind regards " . $config->getName() . " team ";
                
                $emailSent = sendemail(
                    $userEmail,
                    $subject,
                    $message, 
                    $config->email['from_address']
                );
                
                if ($emailSent) {
                   $userMessage = "Your new password details have been emailed to you.";
                   $logMessage = $_SESSION['user_identifier'] . " sent password reset email (manage account section)";
                   \Analog::info($logMessage);
                } else {
                   $userMessage = "Failed to send password via email. SMTP server is down at this time.";
                   $logMessage = $_SESSION['user_identifier'] . " failed to send password reset email (manage account section)";
                   \Analog::error($logMessage);
                }
             
                $return_value['message'] = $userMessage;
            }
           
            $return_value['success'] = $success;
        } else {
            $return_value = [
                'success' => false,
                'message' => 'No such user!'
            ];
        }
    } else {
        $return_value = [
            'success' => false,
            'message' => 'Current password incorrect'
        ];
    }
}
// @TODO (DW) There's no "else" here, is that a problem?
if (isset($return_value)) {
    echo \OMIS\Utilities\JSON::encode($return_value);
}
