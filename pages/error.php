<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/06/2011
 * © 2012 Qpercom Limited.  All rights reserved.
 */
?><div class = 'topmain'>
<div class="card d-inline-block align-top mr-2 mt-4">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Error
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
We're sorry but the page you were looking for cannot be found.<br />You can click <a href='manage.php?page=main'>here</a> to go back to the main page.
    </div>   
  </div> 
</div> 
<br class = 'topmain-clear' />
</div>
<div class = 'contentdiv'></div>