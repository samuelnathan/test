<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Template as Template;
use \Carbon\Carbon;

$instanceID = (isset($_SESSION['instance_identifier'])) ? $_SESSION['instance_identifier'] : "";
// Is admin role
if (isset($_SESSION['instance_identifier']) && isset($_SESSION['user_role'])) {
    $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS);
    $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);
} else {
    $userIsAdmin = false;
}

// Get academic term
$academicTermRecord = $db->academicterms->getDefaultTerm();

/* Get the end date for the term and force the time part to midnight (will 
 * default to current time otherwise)
 */
$endDate = Carbon::createFromFormat("Y-m-d", $academicTermRecord['end_date'])->setTime(0, 0, 0);

/* Today's date is past the end date of the current set term, then warn admin.
 * Remember we're dealing with DateTimes not pure dates, so we have to allow for
 * the fact that an hour ago is in the past but is NOT a previous day...
 */ 
$dateExpired = ($endDate->isPast() && !$endDate->isToday());

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render([
   'instance_identifier' => $instanceID,
   'user_is_admin' => $userIsAdmin,
   'instance_long_name' => $config->getName(true),
   'manuals' => $config->manuals,
   'academic_term' => $_SESSION['cterm'],
   'academic_term_default' => $academicTermRecord['term_id'],
   'academic_term_expired' => $dateExpired,
   'twitter' => $config->twitter,
   'pubbleLink' => $config->support['pubble']['enabled'],
   'domain' => $config->localization['domain'], 
   'clientLogoExists' => file_exists("storage/custom/images/client-logo-home.png") 
]);

