<?php
/* Original Author: Cormac Mc Swiney
 * For Qpercom Ltd
 * Date: 20/06/2016
 * © 2016 Qpercom Limited. All rights reserved.
 * @Manage Stations Import & Export
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once __DIR__ . '/../extra/helper.php';
require_once __DIR__ . '/../osce/osce_functions.php';

?>
<div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Clear All Login Timeouts
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      In this section you can clear all login timeouts.
    </div>   
  </div> 
</div> 
</div>
<div class="contentdiv">
<?php
 
// Clear Last Logins
if (filter_has_var(INPUT_POST, 'clear_login_attempts')) {
    $db->users->clearLoginAttempts();
}

$loginAttempts = $db->users->getLoginAttempts();
$totalNumRows = mysqli_num_rows($loginAttempts);

if ($totalNumRows == 0) {
?>
    <br/>
        No failed login attempts on file.
<?php 
} else {
    $rowCount = 1;
    ?>
    <div class="information-fs mt-2">
        <form id="login_form" method="post" action="manage.php?page=loginman_tool">
            <input class="btn btn-sm btn-info" type="submit" id="clear-login-button" name="clear_login_attempts" value="Clear login attempts"/>
        </form>
    </div>
    <table class="table-border" id="attempts-table">
    <tr class="title-row">
        <th>IP Address</th>
        <th>User Id</th>
        <th>Failed Attempts</th>
        <th class="nbr">Last Login Attempt</th>
    </tr>
    <?php
    while ($loginAttempt = $db->fetch_row($loginAttempts)) {
    
      // No border bottom
      $noBorderBottom = ($rowCount == $totalNumRows) ? 'class="nbbtr"' : '';
        
     ?>
      <tr <?=$noBorderBottom?>>
        <td><?=$loginAttempt['ip']?></td>
        <td><?=$loginAttempt['username']?></td>
        <td class="c"><?=$loginAttempt['attempts']?></td>
        <td class="nbr"><?=$loginAttempt['last_login']?></td>
      </tr>
     <?php
      $rowCount++;
      
    }
?>
</table>
<?php
}
?>
</div>
