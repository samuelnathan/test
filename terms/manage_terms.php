<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Use templating in this section
use \OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

// Get all academic terms
$terms = $db->academicterms->getAllTerms();

// Template data
$data = [
    'one_term_warn' => isset($_SESSION['one_term_warn']),
    'terms' => $terms
];

// Set default term in session
$defaultTermRecord = $db->academicterms->getDefaultTerm();
$_SESSION['cterm'] = $defaultTermRecord['term_id'];

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($data);

