<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 
if (!defined('_OMIS')) {
    define('_OMIS', 1);
}


$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = array(
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    );
    $template->render($data);
    return;
}

//Term Section Redirect [Added 28/12/11]
if (isset($_POST['go_term'])) {
    if ($_POST['todo'] == "delete") { #If Delete
        if ($db->academicterms->deleteTerms($_POST['term_check'])) {
            $db->academicterms->rearrangeTerms(null, 'delete');
        }
    }
    header('Location: ../manage.php?page=manage_terms');

//If none of the above return to index page    
} else {
    header('Location: ../');
}
