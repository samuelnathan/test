<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use Carbon\Carbon;
define('_OMIS', 1);

$mode = filter_input(INPUT_POST, 'mode', FILTER_SANITIZE_STRING);

if (!is_null($mode)) {
    include __DIR__ . '/../extra/essentials.php';
   // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// Add academic term
switch ($mode) {
    
  case "add":
      
    // List of terms from the database
    $terms = $db->academicterms->getAllTerms();
      
    // The defaultt term
    $defaultTerm = $db->academicterms->getDefaultTerm();
    
    // Default start date
    $startDateObj = Carbon::now();
    $defaultStartDate = $startDateObj->format('d/m/Y');
   
    // Default end date
    $defaultEndDate = $startDateObj->addYears(1)->subDay(1)->format('d/m/Y');
    
    ?>
     <td class="checkb">&nbsp;</td>
     <td><input type="text" id="id" class="tf4 form-control form-control-sm" maxlength="12" value="<?=date("Y-").(date("y")+1)?>"/></td>
     <td><textarea id="description" class="ta1 form-control form-control-sm" rows="6">Term <?=date("Y-").(date("Y")+1)?></textarea></td>
     <td><input id="start-date" class="date-field form-control form-control-sm" type="text" maxlength="10" value="<?=$defaultStartDate?>"/></td>
     <td><input id="end-date" class="date-field form-control form-control-sm" type="text" maxlength="10" value="<?=$defaultEndDate?>"/></td>
     <td>
     <ul class="settings">
       <li class="form-row align-items-center">
        <label class="vatop" title="Is <?=gettext('term')?> default <?=gettext('term')?>? yes/no">
        SET DEFAULT <?=strtoupper(gettext('term'))?> =
        </label>
        <select id="default-term" class="custom-select custom-select-sm w-auto">
          <option value="0" class="red">NO</option>
          <option value="1" class="green" selected="selected">YES</option>
        </select>
       </li>
       <li class="form-row align-items-center">
        <label class="vatop" title="Clone <?=gettext('Courses & Modules')?> from previous <?=gettext('term')?>?">
        CLONE  <?=strtoupper(gettext('Courses & Modules'))?> FROM&nbsp;
        </label>
        <select id="clone-courses" class="custom-select custom-select-sm w-auto"><option value="">--</option>
        <?php 
        
        // Loop through all academic terms
        foreach ($terms as $term) {

            $termID = $term['term_id'];
            $defaultTermID = $defaultTerm['term_id'];
            $termDesc = $term['term_desc'];
            $termSelected = ($defaultTermID == $termID) ? ' selected="selected"' : '';
            
           ?>
           <option value="<?=$termID?>" <?=$termSelected?> title="<?=$termDesc?>"><?=$termID?></option>
           <?php

         }
         
        ?>
        </select>
       </li>
      </ul>
   </td>
   <td class="titleedit2">&nbsp;</td>
   <?php
  break;
  
 // Edit term mode
 case "edit":
     
    // Get term record by term id param
    $termID = filter_input(INPUT_POST, 'id');
    $termRecord = $db->academicterms->getById($termID);
    $termLabel = gettext('term');
    $defaultTermHelp = "Is this $termLabel the default $termLabel? yes/no. If you require to set another $termLabel as the "
                     . "'default $termLabel' then you need to set 'yes' to it's 'default $termLabel' option";

    // If the term exists then 
    if ($termRecord && count($termRecord) > 0) {
    
     $termDesc = $termRecord['term_desc'];
     $startDate = formatDate($termRecord['start_date']);
     $endDate = formatDate($termRecord['end_date']);
     $isDefaultTerm = (bool) $termRecord['default_term'];
     $disabledAttributes = ($isDefaultTerm) ? ' disabled="disabled"' : '';
     $selectedAttribute = ($isDefaultTerm) ? ' selected="selected"' : '';
       
     ?>
     <td class="checkb">&nbsp;</td>
     <td>
      <input type="hidden" id="org-id" value="<?=$termID?>"/>
      <input type="text" id="edit-id" class="tf4 form-control form-control-sm" value="<?=$termID?>" maxlength="12"/>
     </td>
     
     <td>
       <textarea id="description" class="ta1 form-control form-control-sm" rows="6"><?=$termDesc?></textarea>
     </td>
     
     <td><input id="start-date" class="date-field form-control form-control-sm" type="text" maxlength="10" value="<?=$startDate?>"/></td>
     <td><input id="end-date" class="date-field form-control form-control-sm" type="text" maxlength="10" value="<?=$endDate?>"/></td>
     <td>
     <ul class="settings">
      <li class="form-row align-items-center">
       <label class="vatop" title="<?=$defaultTermHelp?>">DEFAULT <?=strtoupper($termLabel)?> =</label>
       <select id="default-term"<?=$disabledAttributes?> class="custom-select custom-select-sm w-auto">
         <option value="0" class="red">NO</option>
         <option value="1" class="green"<?=$selectedAttribute?>>YES</option>
       </select>
      </li>
     </ul>
    </td>
    <td class="titleedit">&nbsp;</td>
     <?php
    } else {
       echo "TERM_NON_EXIST";
    }
 
  break;
  
 // Submit term mode
 case "submit":
    
    // New ID param
    $newTermID = filter_input(INPUT_POST, 'id');
     
    // If term already exists then abort
    $termAlreadyExists = $db->academicterms->existsById($newTermID);
    if ($termAlreadyExists) {
       die("TERM_EXISTS");  
    }
    
    // Santize passed in params
    $termIDCloneCourseInfo = filter_input(INPUT_POST, 'clone_courses');
    $termDesc = filter_input(INPUT_POST, 'description');
    $startDate = filter_input(INPUT_POST, 'start_date');
    $endDate = filter_input(INPUT_POST, 'end_date');
    $isDefaultTerm = filter_input(INPUT_POST, 'default_term');

    /**
     * Validate dates for term (startDate and endDate)
     * Invalid date values, then report to user
     */
    if (!dateValid($startDate) || !dateValid($endDate)) {
        die("INVALID_DATES");
    } else if (strtotime($startDate) >= strtotime($endDate)) {
        die("INVALID_ORDER_DATES");
    }

    // Add new academic term
    $newTermSubmitted = $db->academicterms->insertTerm(
            $newTermID,
            $termDesc,
            $startDate,
            $endDate,
            $isDefaultTerm
    );

    // New term submitted and previous term selected exists then clone course info
    $doesPreviousTermExist = $db->academicterms->existsById($termIDCloneCourseInfo);
    if ($newTermSubmitted && $doesPreviousTermExist) {
        $db->courses->cloneTermCourseYearsModules(
                $newTermID, 
                $termIDCloneCourseInfo
        );
    }
  break;
  
 // Update term mode
 case "update":
     
    // Original term ID
    $originalTermID = filter_input(INPUT_POST, 'original_id');
    $termExists = $db->academicterms->existsById($originalTermID);
    
    // If the term doesn't exist then abort
    if (!$termExists) {
        die("TERM_NON_EXIST");
    }
     
    // Sanitize the params
    $newTermID = filter_input(INPUT_POST, 'id');
    $startDate = filter_input(INPUT_POST, 'start_date');
    $endDate = filter_input(INPUT_POST, 'end_date');
    $termDesc = filter_input(INPUT_POST, 'description');
    $isDefaultTerm = filter_input(INPUT_POST, 'default_term');
    
    // Does new term exist ?
    $newTermIDExists = $db->academicterms->existsById($newTermID);

    // If the term is already used then return appropriate message to client side
    if ($newTermID != $originalTermID && $newTermIDExists) {
        die("TERM_ALREADY_USED"); 
    }
    
    /**
     * Validate dates for term (startDate and endDate)
     * Invalid date values, then report to user
     */
    if (!dateValid($startDate) || !dateValid($endDate)) {
        die("INVALID_DATES");
    } else if (strtotime($startDate) >= strtotime($endDate)) {
        die("INVALID_ORDER_DATES");
    }          

    // Update term
    $db->academicterms->updateTerm(
            $newTermID,
            $originalTermID,
            $startDate,
            $endDate,
            $termDesc,
            $isDefaultTerm
    );
    
}

