<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 01/04/2015
 * Stations Means Graph
 * @Changed number to scenario_group
 *
 * © 2015 Qpercom Limited. All rights reserved.
 */
define('_OMIS', 1);
use \JpGraph\JpGraph as JpGraph;

include __DIR__ . '/../extra/essentials.php';
//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

//Require and Include
require_once 'Math/Stats.php';
JpGraph::load();
JpGraph::module('bar');
JpGraph::module('error');
JpGraph::module('line');
JpGraph::module('plotline');


/* * ** Definitions *** */
include_once '../analysis/analysis_definitions.php';

//Exit if no data from summary section [reference build_summary.php file]
if (!isset($_SESSION['stations_means']['stations']) || empty($_SESSION['stations_means']['stations'])) {
    echo 'Graph has not received data';
    exit;
}

//Data from summary section
$summary_array = $_SESSION['stations_means'];

//Remove the unwanted session variable
unset($_SESSION['stations_means']);

//Overall Average
$average = $summary_array['overall_avg'];

//Combination/Filter Value
$cnm = $summary_array['cnm'];

$xAxisData = array(); //x axis values
$yAxisData = array(); //the bar values
$sdarray = array();

//[Loop Through Stations]
foreach ($summary_array['stations'] as $station) {
    //Add name and average values to x and y arrays
    $xAxisData[] = (($cnm != STATION_COMBINE_TAG) ? 'Station ' : '') . $station['scenario_group'];
    // This keeps the value between 0 and 100.
    $yAxisData[] = max(min($station['average'], 100), 0);
}

//[Create Graph]
$graph = new Graph(520, 470, 'auto');
$graph->img->SetMargin(50, 20, 20, 110);
$graph->img->SetImgFormat('png');

//Setup y axis scale
$graph->SetScale('textlin', 0, 109);

//Set major and minor tick to 10
$graph->yaxis->scale->ticks->Set(10);

//Set margin Color
$graph->SetMarginColor('#F5DEB3');

//Set up the x axis values
$graph->xaxis->SetTickLabels($xAxisData);

//Create bar plots
$bars = new BarPlot($yAxisData);

//Add the bars to the graph
$graph->Add($bars);

$bars->SetAbsWidth(14);
$bars->SetFillGradient('lightblue', 'lightblue', GRAD_MIDVER);
$bars->SetColor('black');
$bars->SetLegend('result mean');
$bars->value->SetFont(FF_TIMES, FS_BOLD, 8);
$bars->value->SetAngle(90);
$bars->value->SetColor('black', 'darkred');
$bars->value->HideZero();
$bars->value->Show();
$bars->SetValuePos('center');

/* 
 * Standard Deviation Band (green color bar chart):
 * Do we have more than one station (average score) which is required to calculate a standard deviation?
 * If so then calculate it and add a standard deviation band to the bar chart
 */
if (sizeof($yAxisData) > 1) {
    $data = new Math_Stats();
    $data->setData($yAxisData);
    $std = $data->stDevWithMean($average);

    $band = new PlotBand(HORIZONTAL, BAND_RDIAG, $average - $std, $average + $std, 'green');
    $graph->AddBand($band);

    $band->ShowFrame(true);
    $band->SetOrder(DEPTH_FRONT);
    $band->SetDensity(97);
}

//Create average plot line
$plotline = new PlotLine(HORIZONTAL, $average, 'red', 2);

//Add average plot line
$graph->AddLine($plotline);

//Pltline Legend
$plotline->SetLegend('overall mean');

//Graph Title
$graph->title->Set('Stations Mean %');

//Add legend Data
$graph->legend->SetMarkAbsSize(6);
$graph->legend->SetFont(FF_ARIAL, FS_BOLD, 10);
$graph->legend->SetFrameWeight(1);
$graph->legend->SetColumns(10);
$graph->legend->SetVColMargin(10);
$graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
$graph->legend->SetShadow(false, 0);

//Add titles to x & y axis
$graph->xaxis->title->Set(' Stations');
$graph->yaxis->title->Set(' Mean Mark (%) ');

//Set font for x & y axis titles
$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);
$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);

//Set margins for x & y axis
$graph->xaxis->SetTitleMargin(-10);
$graph->yaxis->SetTitleMargin(30);

//Set font for x & y axis
$graph->xaxis->SetFont(FF_ARIAL, FS_BOLD, 9);
$graph->yaxis->SetFont(FF_ARIAL, FS_BOLD, 9);

//Set label margins and angle
$graph->xaxis->SetLabelMargin(12);
$graph->xaxis->SetLabelAngle(90);

//Set colors of x & y axis
$graph->xaxis->SetColor('red');
$graph->yaxis->SetColor('red');

//Set weights of x & y axis
$graph->xaxis->SetWeight(2);
$graph->yaxis->SetWeight(3);

//Set y axis grid weight
$graph->ygrid->SetWeight(2);
$graph->ygrid->Show(true, true);

$graph->SetFrame(true, '#BDBDBD', 1);

//Add shadow to graph
$graph->SetShadow();

//Output Graph
$graph->Stroke();
