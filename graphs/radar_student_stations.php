<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 use \JpGraph\JpGraph as JpGraph;

 define('_OMIS', 1);
 define('EXCLUDE_ABSENTS', true);

 include __DIR__ . '/../extra/essentials.php';

 // page access check / can user access this section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 include_once '../analysis/analysis_definitions.php';
 require_once 'GraphHelper.php';

 $formNames = [];
 $accExamineePercentages = [];
 $accExamPercentages = [];

 // Some checks
 if (isset($_GET['student'], $_GET['exam'], $_GET['cnm'], $_GET['session']) &&
     $db->students->existsById($_GET['student']) && $db->exams->existsById($_GET['exam']) && $db->sessions->existsById($_GET['session'])) {

     $student = trim($_GET['student']);
     $exam = trim($_GET['exam']);
     $session = trim($_GET['session']);

     // Request combine by number/tag
     $cnm = !isset($_GET['cnm']) ? STATION_COMBINE_SCENARIO : trim($_GET['cnm']);

 } else {

     die("One or more record identifiers no longer found in the system");

 }

 // Get unique station identifiers
 if ($cnm == STATION_COMBINE_DEFAULT) { # Station numbers

     $uniqueIDs = $db->stations->getUniqueStationNumbers($exam);
     $dbField = "station_number";

 } elseif ($cnm == STATION_COMBINE_TAG) { # Tags

     $uniqueIDs = $db->stations->getStationGroups($exam);
     $dbField = "station_code";

 } else { # Form IDs

     $uniqueIDs = $db->exams->getUniqueFormIdentifiers($exam);
     $dbField = "form_id";

 }

 // Get Student Results
 $resultsDB = $db->results->bySession(
     $student, $session, true,
     $dbField, EXCLUDE_ABSENTS
 );

 // Loop through Unique Station Identifiers
 foreach ($uniqueIDs as $uniqueID) {

    $hasResult = false;
    $scoresWeightings = $selfAssessWeightings = [];

    // We don't have results then skip
    if (!isset($resultsDB[$uniqueID])) {

        continue;

    }

    // Multi Results array
    $multiResults = $resultsDB[$uniqueID];

    // Get form ID no matter the case
    $formID = ($cnm == STATION_COMBINE_SCENARIO) ? $uniqueID : $resultsDB[$uniqueID][0]['form_id'];

    // Self assessment
    $formSelfAssess = $db->selfAssessments->scoresheetHasSelfAssessment($formID);
    $ownerID = $formSelfAssess ? $db->selfAssessments->getOwnerByStudentExam($student, $exam) : "";

     // Loop through Multi Student Results
    foreach ($multiResults as $resultRow) {

        // Exclude observer weighting
        $isObserver = ($resultRow['scoring_weight'] == \OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER);

        if ($resultRow['scoring_weight'] != null && $isObserver) {

            continue;

        }

        // No weighting specified
        if (!isset($resultRow['scoring_weight']) || $resultRow['scoring_weight'] == null) {

            /* If we don't know what weighting to assign to this examiner for any
             * reason, then go with a default weighting.
             */
            $examinerScoringWeight = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;

            // Examiner Scoring Weight
        } else {

            /* The examiner is "known" to the system, so we should be able to
             * get that examiner's pre-defined weighting.
             */
            $examinerScoringWeight = $resultRow['scoring_weight'];

        }

        // Self assessment
        if ($formSelfAssess && $ownerID != $resultRow['examiner_id']) {

            $selfAssessWeightings[] = [
                "examiner_score" => (float) $resultRow['score'],
                "examiner_weighting" => (int) $examinerScoringWeight
            ];

        } else {

            $scoresWeightings[] = [
                "examiner_score" => (float) $resultRow['score'],
                "examiner_weighting" => (int) $examinerScoringWeight,
            ];

        }

        // Has Result
        $hasResult = true;

    }

    // If we have a result for this station then grab form/scoresheet name for radar graph
    if ($hasResult) {

         // Decode any html entities found in the form name
         $formName = html_entity_decode($multiResults[0]['form_name'], ENT_QUOTES, 'UTF-8');

        // Station Description
        if ($cnm == STATION_COMBINE_SCENARIO) {

            $formNames[] = wordwrap($formName, 12);

        } else {

            $stationItem = ($cnm == STATION_COMBINE_DEFAULT) ? "#" . $uniqueID : $uniqueID . " -";
            $formNames[] = $stationItem . " " . wordwrap($formName, 12);

        }

        // Weightings
        $weightingTotal = array_sum(
            array_column($scoresWeightings, "examiner_weighting")
        );

        if ($weightingTotal > 0) {

            $sum = array_sum(
                array_map("array_product", $scoresWeightings)
            );

            $adjustedScore = $sum / $weightingTotal;

        } else {

            $adjustedScore = 0;

        }

        $possibleScore = $multiResults[0]['total_value'];

        // Self Assessment non owners (add to percent calc)
        if ($formSelfAssess) {

           // Weightings
           $selfWeightingTotal = array_sum(
                array_column($selfAssessWeightings, "examiner_weighting")
           );

           $selfSum = array_sum(
               array_map("array_product", $selfAssessWeightings)
           );

           $adjustedScore += ($selfSum / $selfWeightingTotal);
           $possibleScore += $db->selfAssessments->getNonOwnerPossible($formID);

        }

        // Examinee Percentages
        $accExamineePercentages[] = percent($adjustedScore, $possibleScore);

      // If student has no result for this station
    } else {

        /**
         * Decode any html entities found in the form name
         */
        // Station Description
        if ($cnm == STATION_COMBINE_SCENARIO) {

            $formDB = $db->forms->getForm($uniqueID);
            $formName = html_entity_decode($formDB['form_name'], ENT_QUOTES, 'UTF-8');
            $formNames[] = wordwrap($formName, 12);

        } else {

            $formNames[] = (($cnm == STATION_COMBINE_DEFAULT) ? "#" : "") . $uniqueID;

        }

        // Set to 0 as we have no result
        $accExamineePercentages[] = 0;

    }

    // Exam Percentages
    $examOverallDB = $db->results->getOverallScore($exam, $uniqueID, $dbField);
    $accExamPercentages[] = percent($examOverallDB['score'], $examOverallDB['possible']);

 }

 /***************************[ Build & Output Graph ]***************************/

 // Create new Radar Graph object
 JpGraph::load();
 JpGraph::module('radar');
 JpGraph::module('bar');

 // If Number of Results < 3 Display in a Bar Chart
 if (count($accExamineePercentages) < 3) {

     $graph = new Graph(700, 600);
     $graph->SetScale('textlin', 0, 100);

     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {

         $graph->img->SetAntiAliasing();

     }

     // Left, Right, Top, Bottom
     $graph->img->SetMargin(50, 20, 20, 80);
     $graph->SetFrame(true, '#989898', 1);

     // Create Bar Plots
     $plot1 = new BarPlot($accExamineePercentages);
     $plot2 = new BarPlot($accExamPercentages);

     // Add to Plot Group
     $plotGroup = new GroupBarPlot([$plot1, $plot2]);
     $graph->Add($plotGroup);

     $plotGroup->SetWidth(0.1);

     // Students (blue plot)
     $plot1->SetLegend(ucwords(gettext('student')) . ' %');
     $plot1->SetFillColor('blue');
     $plot1->SetAbsWidth(30);

     // OSCE (red plot)
     $plot2->SetLegend(gettext('OSCE'). ' Group %');
     $plot2->SetFillColor('red');
     $plot2->SetAbsWidth(30);

     // Configure x and y axis
     $graph->xaxis->title->Set('Stations');
     $graph->xaxis->SetTickLabels($formNames);
     $graph->yaxis->title->Set('Score (%)');

     $graph->xaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->xaxis->SetTitleMargin(8);

     $graph->yaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->yaxis->scale->ticks->Set(10);
     $graph->yaxis->SetTitleMargin(30);

     // Configure Legend
     $graph->legend->SetMarkAbsSize(6);
     $graph->legend->SetFont(FF_ARIAL, FS_BOLD, 8);
     $graph->legend->SetFrameWeight(2);
     $graph->legend->SetColumns(10);
     $graph->legend->SetVColMargin(10);
     $graph->legend->SetFillColor('#FFFFFF');
     $graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');

 } else {

     $graph = new RadarGraph(680, 560);
     /**
      * The following 3 lines of code resolves bitbucket issue #339:
      * Long Station Names (Titles) can get chopped off by edge of image canvas
      */

     // Text Template Object (JpGraph)
     $textTemplate = new Text("Testing Text");

     // Support graph function class
     $GraphHelper = new GraphHelper($graph, $textTemplate);

     // Compute new plot size while trying to fit all titles onto canvas
     $plotSize = $GraphHelper->computePlotSize($formNames);

     // Set the plot size
     $graph->SetPlotSize($plotSize);

     // Set the axes titles
     $graph->SetTitles($formNames);

     // Apply further settings and plot the graph
     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {

         $graph->img->SetAntiAliasing();

     }

     $graph->SetScale('lin', 0, 100);
     $graph->SetFrame(true, '#989898', 1);
     $graph->yscale->ticks->Set(10, 10);
     $graph->SetCenter(0.5, 0.5);
     $graph->axis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->axis->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->legend->SetPos(0.01, 0.01, 'right', 'top');
     $graph->grid->SetLineStyle('solid');
     $graph->grid->SetColor('navy');

     $p1 = new RadarPlot($accExamineePercentages);
     $p2 = new RadarPlot($accExamPercentages);

     $graph->Add($p1);
     $graph->Add($p2);

     $p1->SetLegend(ucwords(gettext('student')) . ' %');
     $p1->SetLineWeight(2);
     $p1->SetColor('blue');
     $p1->SetFill(false);

     $p2->SetLegend(gettext('OSCE') . ' Group %');
     $p2->SetLineWeight(2);
     $p2->SetColor('red');
     $p2->SetFill(false);

 }

 $graph->Stroke();
