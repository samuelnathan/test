<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 07/04/2016
 * © 2016 Qpercom Limited. All rights reserved.
 * Graph section helper class
 */

class GraphHelper {
    
  // JpGraph object 
  private $graph; 
  
  // Template text object
  private $text;
  
  // Constructor, pass RadarGraph and JpGraph text template object
  public function __construct($graph, $textTemplate)
  {
     $this->graph = $graph;
     $this->text = $textTemplate;
     $textTemplate->SetFont(FF_ARIAL, FS_NORMAL, 8);

  }

  /**
   * The following function fixes bitbucket issue #339: 
   * Long Station Names (Titles) can get chopped off by edge of image canvas
   * 
   * @param array $titles - list of graph titles
   * @param double $maxPlotSize - maximum plot size
   * @param double $minPlotSize - minimum plot size
   * @param int $thresholdTextRows - the threshold number of text rows 
   *  to be reached before reducing the plot size 
   * @return $plotSize
   */
  public function computePlotSize($titles, $maxPlotSize = 0.8, $minPlotSize = 0.55, $thresholdTextRows = 3) {

    // Set default the plot size to the maximum plot size
    $plotSize = $maxPlotSize;
     
    // Calculate the text height, font size plus line height of a single line of text appearing on the graph canvas
    $textLineHeight = $this->text->GetTextHeight($this->graph->img);
      
    // Find the maximum title text height from the array of titles
    $MaxTitleHeight = $textLineHeight;
    foreach ($titles as $title) {

      // Get the title text height using JpGraph's own built-in function
      $this->text->set($title);
      $currentTitleHeight = $this->text->GetTextHeight($this->graph->img);
      if ($currentTitleHeight > $MaxTitleHeight) {
          $MaxTitleHeight = $currentTitleHeight;
      }

    }
    
    // To calculate the number of text rows, divide the max title height by the single text line height.
    $numTextRows = ($MaxTitleHeight/$textLineHeight);
    
    /**
     * If the longest title has a text row count greater than the threshold number of text
     * rows then reduce the plot size ever so slightly to fit the title onto the canvas.
     */
     // Calculate the reduction required
     $reduction = ($numTextRows > $thresholdTextRows) ? (($numTextRows-$thresholdTextRows)*0.07) : 0;

     // Apply the reduction to the plot size if any
     $plotSize -= $reduction;

    /**
     * Prevent the plot size going below a minimum plot size as the 
     * plots and axes may render unreadable to the user.
     */
     if ($plotSize < $minPlotSize) {
         $plotSize = $minPlotSize;
     }    

    return $plotSize;
  }

}
