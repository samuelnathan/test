<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use \JpGraph\JpGraph as JpGraph;

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Include graph helper class
 JpGraph::load();
 JpGraph::module('radar');
 JpGraph::module('bar');
 require_once 'GraphHelper.php';

 /**************************[ Build & Output Graph ]*****************************/

 // Adjust Overall Percentages Array for Stations Done
 $stationMeanScoresArray = [];

 foreach ($studentStationScores as $formID => $stationScore) {

     $stationMeanScoresArray[] = $radarMeanScoresDB[$formID];

 }

 // If Number of Results < 3 Display in a Bar Chart
 if (count($studentStationScores) < 3) {

     $graph = new Graph(700, 600);
     $graph->SetScale('textlin', 0, 100);

     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {
         $graph->img->SetAntiAliasing();
     }

     // Left, Right, Top, Bottom
     $graph->img->SetMargin(50, 20, 20, 80);
     $graph->SetFrame(true, '#989898', 1);

     // Create Bar Plots
     $plot1 = new BarPlot(array_values($studentStationScores));
     $plot2 = new BarPlot($stationMeanScoresArray);

     // Add to Plot Group
     $plotGroup = new GroupBarPlot([$plot1, $plot2]);
     $graph->Add($plotGroup);

     $plotGroup->SetWidth(0.1);

     // Students (blue plot)
     $plot1->SetLegend(ucwords(gettext('student')) . ' %');
     $plot1->SetFillColor('blue');
     $plot1->SetAbsWidth(30);

     // OSCE Group (red plot)
     $plot2->SetLegend('Group %');
     $plot2->SetFillColor('red');
     $plot2->SetAbsWidth(30);

     // Configure x and y axis
     $graph->xaxis->title->Set('Stations');
     $graph->xaxis->SetTickLabels($stationGraphLabels);
     $graph->yaxis->title->Set('Score (%)');

     $graph->xaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->xaxis->SetTitleMargin(8);

     $graph->yaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->yaxis->scale->ticks->Set(10);
     $graph->yaxis->SetTitleMargin(30);

     // Configure Legend
     $graph->legend->SetMarkAbsSize(6);
     $graph->legend->SetFont(FF_ARIAL, FS_BOLD, 8);
     $graph->legend->SetFrameWeight(2);
     $graph->legend->SetColumns(10);
     $graph->legend->SetVColMargin(10);
     $graph->legend->SetFillColor('#FFFFFF');
     $graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
 } else {

     // Create new Radar Graph object
     $graph = new RadarGraph(680, 560);

    /**
     * The following 3 lines of code resolves bitbucket issue #339:
     * Long Station Names (Titles) can get chopped off by edge of image canvas
     */

     // Text Template Object (JpGraph)
     $textTemplate = new Text("Testing Text");

     // Support graph function class
     $GraphHelper = new GraphHelper($graph, $textTemplate);

     // Compute new plot size while trying to fit all titles onto canvas
     $plotSize = $GraphHelper->computePlotSize($stationGraphLabels);

     // Set the plot size
     $graph->SetPlotSize($plotSize);

     // Set the axes titles
     $graph->SetTitles($stationGraphLabels);

     // Apply further settings and plot the graph
     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {
         $graph->img->SetAntiAliasing();
     }

     $graph->SetScale('lin', 0, 100);
     $graph->SetFrame(true, '#989898', 1);
     $graph->yscale->ticks->Set(10, 10);
     $graph->SetCenter(0.5, 0.52);
     $graph->axis->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->axis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->legend->SetPos(0.01, 0.01, 'right', 'top');
     $graph->grid->SetLineStyle('solid');
     $graph->grid->SetColor('navy');
     $graph->img->SetImgFormat('png');

     $plot1 = new RadarPlot(array_values($studentStationScores));
     $plot2 = new RadarPlot($stationMeanScoresArray);

     $graph->Add($plot1);
     $graph->Add($plot2);

     $plot1->SetLegend(ucwords(gettext('student')) . ' %');
     $plot1->SetLineWeight(2);
     $plot1->SetColor('blue');
     $plot1->SetFill(false);

     $plot2->SetLegend('Group %');
     $plot2->SetLineWeight(2);
     $plot2->SetColor('red');
     $plot2->SetFill(false);
 }

 // Output Graph to file
 $graph->Stroke($fileNameRadar);

 // Do some cleaning up
 unset($graph);
