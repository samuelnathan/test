<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

define('_OMIS', 1);

use \JpGraph\JpGraph as JpGraph;

include __DIR__ . '/../extra/essentials.php';
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Include more
JpGraph::load();
JpGraph::module('bar');
JpGraph::module('error');

// Exit if no data from summary section [reference build_summary.php file]
if (!isset($_SESSION['stations_students_failed'])) {
    echo 'Graph has not received data';
    exit;
}

// Data from build rawdata section
$raw_array = $_SESSION['stations_students_failed'];

// Remove the unwanted session variable
unset($_SESSION['stations_students_failed']);

// Total # Stations
$num_of_stations = $raw_array['station_count'];

// Stations Failed Count
$failed_count_arr = $raw_array['failed_count'];

// Data Arrays 
$fail_data = array(); // Y axis values
$x_array = array(); // x axis values
// Maximum fail count
$max_fail_cnt = 0;

// [Loop Through Stations]
for ($i = 1; $i <= $num_of_stations; $i++) {

    // Fail Count
    if (isset($failed_count_arr[$i])) {
        $fail_data[] = $failed_count_arr[$i];

        // Maximum fail count
        if ($failed_count_arr[$i] > $max_fail_cnt) {
            $max_fail_cnt = $failed_count_arr[$i];
        }
    } else {
        $fail_data[] = 0;
    }

    // x-axis titles
    $station_title = ' Station';
    if ($i > 1) {
        $station_title .= 's';
    }

    // Add station name
    $x_array[] = $i . $station_title;
}

// Increase fail count to give more space at top of graph
$max_fail_cnt += 5;

// [Create Graph]
$graph = new Graph(520, 470, 'auto');
$graph->img->SetMargin(50, 20, 20, 120);
$graph->img->SetImgFormat('png');

// Setup y axis scale 
$graph->SetScale('textlin', 0, $max_fail_cnt);

// Set major and minor tick to 10 
$graph->yaxis->scale->ticks->Set(10);

// Set margin Color 	
$graph->SetMarginColor('#F5DEB3');

// Set up the x axis values
$graph->xaxis->SetTickLabels($x_array);

// Create the 1 bar plot
$barplot = new BarPlot($fail_data);

// Add Plot
$graph->Add($barplot);

// bar plot settings
$barplot->SetFillColor('#FF8C00');
$barplot->SetAbsWidth(14);
$barplot->SetLegend('failed count');
$barplot->value->SetFont(FF_TIMES, FS_BOLD, 8);
$barplot->value->SetAngle(90);
$barplot->value->SetColor('black', 'darkred');
$barplot->value->HideZero();
$barplot->value->Show();
$barplot->SetValuePos('center');
$barplot->value->SetFormat('%s');

// Graph Title
$graph->title->Set('Station Fail Count for Students Passed');

// Add legend Data  
$graph->legend->SetMarkAbsSize(6);
$graph->legend->SetFont(FF_ARIAL, FS_BOLD, 10);
$graph->legend->SetFrameWeight(1);
$graph->legend->SetColumns(10);
$graph->legend->SetVColMargin(10);
$graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
$graph->legend->SetShadow(false, 0);

// Add titles to x & y axis
$graph->xaxis->title->Set(' Number of Stations');
$graph->yaxis->title->Set(' Number of Students ');

// Set font for x & y axis titles
$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);
$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);

// Set margins for x & y axis
$graph->xaxis->SetTitleMargin(-10);
$graph->yaxis->SetTitleMargin(30);

// Set font for x & y axis
$graph->xaxis->SetFont(FF_ARIAL, FS_BOLD, 9);
$graph->yaxis->SetFont(FF_ARIAL, FS_BOLD, 9);

// Set label margins and angle
$graph->xaxis->SetLabelMargin(12);
$graph->xaxis->SetLabelAngle(90);

// Set colors of x & y axis
$graph->xaxis->SetColor('red');
$graph->yaxis->SetColor('red');

// Set weights of x & y axis
$graph->xaxis->SetWeight(2);
$graph->yaxis->SetWeight(3);

// Set y axis grid weight
$graph->ygrid->SetWeight(2);
$graph->ygrid->Show(true, true);

$graph->SetFrame(true, '#BDBDBD', 1);

// Add shadow to graph
$graph->SetShadow();

// Output Graph
$graph->Stroke();
