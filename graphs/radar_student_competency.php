<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use \JpGraph\JpGraph as JpGraph;

 // If request comes from a url call, student results section
 $urlRequest = filter_input(INPUT_GET, 'url_request', FILTER_SANITIZE_STRING);
 $studentResultsSection = !is_null($urlRequest);

 // If request comes from a url call, include database functions
 if (!is_null($urlRequest)) {
     define('_OMIS', 1);
     include __DIR__ . '/../extra/essentials.php';
 }

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Include graph helper class
 JpGraph::load();
 JpGraph::module('radar');
 JpGraph::module('bar');
 require_once 'GraphHelper.php';

 // If request comes from a url call
 if ($studentResultsSection) {

     // Get filters params
     $studentID = filter_input(INPUT_GET, 'student', FILTER_SANITIZE_STRING);
     $sessionID = filter_input(INPUT_GET, 'session', FILTER_SANITIZE_STRING);
     $stationRecords = $db->exams->getStations([$sessionID]);
     $stationIDs = [];

     // Gather station IDs
     while ($station = $db->fetch_row($stationRecords)) {
        $stationID = $station['station_id'];
         if(!in_array($stationID, $stationIDs, true)){
           array_push($stationIDs, $stationID);
        }
     }

     // Competency results per student and station IDs
     $competencyResults = $db->competencies->getExamCompetencyResults(
             $stationIDs,
             $studentID
     );

     // Calculate percentage score
     foreach ($competencyResults as &$competencyResult) {
         $competencyResult['percent'] = percent($competencyResult['score'], $competencyResult['possible']);
         $competencyResult['name'] .= "\n(" . $competencyResult['percent'] ."%)";
     }

 }

 /**************************[ Build & Output Graph ]*****************************/

 // Get Scores
 $scores = array_column($competencyResults, 'percent');

 // Get labels
 $labels = array_column($competencyResults, 'name');

 // Squeeze labels
 array_walk($labels, function(&$label) {
     $label = wordwrap($label,15,"\n");
 });

 // If Number of Results < 3 Display in a Bar Chart
 if (count($competencyResults) < 3) {

     $graph = new Graph(700, 600);
     $graph->SetScale('textlin', 0, 100);

     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {
         $radarGraph->img->SetAntiAliasing();
     }

     // Left, Right, Top, Bottom
     $graph->img->SetMargin(50, 20, 20, 80);
     $graph->SetFrame(true, '#989898', 1);

     // Create Bar Plots
     $plot = new BarPlot($scores);

     // Students (blue plot)
     $plot->SetLegend(gettext('Competency') . ' %');
     $plot->SetFillColor('#0000ff');
     $plot->SetAbsWidth(30);

     // Add plot to graph
     $graph->Add($plot);

     // Configure x and y axis
     $graph->xaxis->title->Set(gettext('Competencies'));
     $graph->xaxis->SetTickLabels($labels);
     $graph->yaxis->title->Set('Score (%)');

     $graph->xaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->xaxis->SetTitleMargin(8);

     $graph->yaxis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->yaxis->scale->ticks->Set(10);
     $graph->yaxis->SetTitleMargin(30);

     // Configure Legend
     $graph->legend->SetMarkAbsSize(6);
     $graph->legend->SetFont(FF_ARIAL, FS_BOLD, 8);
     $graph->legend->SetFrameWeight(2);
     $graph->legend->SetColumns(10);
     $graph->legend->SetVColMargin(10);
     $graph->legend->SetFillColor('#ffffff');
     $graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
 } else {

     // Create new Radar Graph object
     $graph = new RadarGraph(680, 560);

    /**
     * The following 3 lines of code resolves bitbucket issue #339:
     * Long Station Names (Titles) can get chopped off by edge of image canvas
     */

     // Text Template Object (JpGraph)
     $textTemplate = new Text("Testing Text");

     // Support graph function class
     $GraphHelper = new GraphHelper($graph, $textTemplate);

     // Compute new plot size while trying to fit all titles onto canvas
     $plotSize = $GraphHelper->computePlotSize($labels);

     // Set the plot size
     $graph->SetPlotSize($plotSize);

     // Set the axes titles
     $graph->SetTitles($labels);

     // Apply further settings and plot the graph
     /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
      * graph) only if it's possible on the current system.
      */
     if (function_exists('imageantialias')) {
         $graph->img->SetAntiAliasing();
     }

     $graph->SetScale('lin', 0, 100);
     $graph->SetFrame(true, '#989898', 1);
     $graph->yscale->ticks->Set(10, 10);
     $graph->SetCenter(0.5, 0.52);
     $graph->axis->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->axis->title->SetFont(FF_ARIAL, FS_NORMAL, 8);
     $graph->legend->SetPos(0.01, 0.01, 'right', 'top');
     $graph->grid->SetLineStyle('solid');
     $graph->grid->SetColor('navy');
     $graph->img->SetImgFormat('png');

     $plot = new RadarPlot($scores);

     $graph->Add($plot);

     $plot->SetLegend(gettext('Competencies') . ' %');
     $plot->SetLineWeight(2);
     $plot->SetColor('#0000ff');
     $plot->SetFill(true);
     $plot->SetFillColor('#539dc2');
 }

 // Output Graph to file
 if($studentResultsSection) {
   $graph->Stroke();
 } else {
   $graph->Stroke($fileNameCompetenciesRadar);
 }

 // Do some cleaning up
 unset($graph);
