<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */
 define('_OMIS', 1);

 use \JpGraph\JpGraph as JpGraph;

 include __DIR__ . '/../extra/essentials.php';
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Include more
 JpGraph::load();
 JpGraph::module('radar');

 // Adjust Overall Percentages Array for Stations Done
 $values = $labels = [];
 $competencies = $_SESSION['station_competencies'];
 foreach ($competencies as $competence) {

     $axisValue = percent($competence['score'], $competence['possible']);
     $values[] = $axisValue;
     $labels[] = $competence['name'] . "\n($axisValue %)";

 }

 // Squeeze labels
 array_walk($labels, function(&$label) {
    $label = wordwrap($label,15,"\n");
 });

 // Create new Radar Graph object
 $graph = new RadarGraph(680, 560);

 // Set the axes titles
 $graph->SetTitles($labels);

 // Apply further settings and plot the graph
 /* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking
  * graph) only if it's possible on the current system.
  */
 if (function_exists('imageantialias'))
     $graph->img->SetAntiAliasing();

 $graph->SetScale('lin', 0, 100);
 $graph->SetFrame(true, '#989898', 1);
 $graph->yscale->ticks->Set(10, 10);
 $graph->SetCenter(0.5, 0.52);
 $graph->axis->SetFont(FF_ARIAL, FS_NORMAL, 9);
 $graph->axis->title->SetFont(FF_ARIAL, FS_NORMAL, 9);
 $graph->legend->SetPos(0.01, 0.01, 'right', 'top');
 $graph->grid->SetLineStyle('solid');
 $graph->grid->SetColor('black');
 $graph->img->SetImgFormat('png');

 $plot = new RadarPlot($values);

 $graph->Add($plot);

 $plot->SetLegend(gettext('Competency') . ' %');
 $plot->SetLineWeight(2);
 $plot->SetColor('#0000FF');
 $plot->SetFill(true);
 $plot->SetFillColor('#539DC2');

 // Output Graph to file
 $graph->Stroke();

 // Do some cleaning up
 unset($_SESSION['station_competencies']);
