<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}
define('_OMIS', 1);

// Include JpGraph and modules
use \JpGraph\JpGraph as JpGraph;

JpGraph::load();
JpGraph::module('bar');
JpGraph::module('plotmark.inc');
JpGraph::module('plotline');
JpGraph::module('plotband');

include __DIR__ . '/../extra/essentials.php';
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// We need the math stats functions
require_once 'Math/Stats.php';
$s = new Math_Stats();

// Abort if we don't have the groups and stations params
if (!isset($_REQUEST['g']) || !isset($_REQUEST['sts'])) {
  return false;
}

// Get groups and stations params
$groups = explode(',', $_REQUEST['g']);
$stations = explode(',', $_REQUEST['sts']);

// Get all answers
$itemScores = $db->results->getItemScoresByStationsGroups($stations, $groups);

// X axis values
$xAxisData = [];

// y axis values
$yAxisData = [];

$min = 0;
$max = 100;
$overallSum = 0;
$overallPossible = 0;
$itemNum = 0;

// Gather mean scores for all the items
foreach ($itemScores as $itemID => $scores) {
    
    // Accumulate the item number
    $itemNum++;
    
    // If we don't have a numeric array of scores then skip
    if (!is_Array_Numeric($scores)) {
       continue;
    }
   
    // Set the score data for stats
    $s->setData($scores);
    $sum = $s->sum();
    $overallSum += $sum;
    $scoreCount = $s->count();

    // Get item record from database by item ID number
    $itemRecord = $db->fetch_row($db->forms->getItem($itemID));
    $lowestPossible = $itemRecord['lowest_value'];
    $highestPossible = $itemRecord['highest_value'];

    // What is the item mean?
    $itemPossible = max($lowestPossible, $highestPossible);
    $itemsPossible = ($itemPossible * $scoreCount);
    $overallPossible += $itemsPossible;
    if ($itemsPossible != 0) {
        $mean = ($sum / $itemsPossible) * 100;
    } else {
        $mean = 0;
    }
    $yAxisData[] = $mean;
    $xAxisData[] = "Item $itemNum";
    
}

$averageMark = ($overallSum / $overallPossible) * 100;
$graph = new Graph(650, 600, 'auto');
$graph->img->SetMargin(50, 20, 20, 110);
$graph->img->SetImgFormat('png');

// Setup y axis scale
$graph->SetScale('textlin', $min, $max);

// Set major and minor tick to 10
$graph->yaxis->scale->ticks->Set(10);

$graph->SetMarginColor('#F5DEB3');

// Set up the x axis values
$graph->xaxis->SetTickLabels($xAxisData);

//Create bar plots
$bars = new BarPlot($yAxisData);

// Add bars to the graph
$graph->Add($bars);
$bars->SetLegend('Result Mean');
$bars->SetAbsWidth(12);

// Setup color for gradient fill style 
$bars->SetFillGradient('lightblue', 'lightblue', GRAD_MIDVER);
$bars->SetColor('black');

$plotline = new PlotLine(HORIZONTAL, $averageMark, 'red', 2);
$graph->AddLine($plotline);
$plotline->SetLegend('Overall Mean');

/*
 * Standard Deviation Band (green color bar chart):
 * Do we have more than one item (average score) which is required to calculate a standard deviation?
 * If so then calculate it and add a standard deviation band to the bar chart
 */
if (sizeof($yAxisData) > 1) {
    $s->setData($yAxisData);
    $std = $s->stDevWithMean($averageMark);
    $band = new PlotBand(HORIZONTAL, BAND_RDIAG, $averageMark - $std, $averageMark + $std, 'green');
    $graph->AddBand($band);
    $band->ShowFrame(true);
    $band->SetOrder(DEPTH_FRONT);
    $band->SetDensity(97);
}

// legend  
$graph->legend->SetMarkAbsSize(6);
$graph->legend->SetFont(FF_ARIAL, FS_BOLD, 10);
$graph->legend->SetFrameWeight(2);
$graph->legend->SetColumns(10);
$graph->legend->SetVColMargin(30);
$graph->legend->SetLineSpacing(70);

$graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');

$graph->yaxis->title->Set(' Item Mean Mark (%) ');

$graph->xaxis->title->Set(' Items');

$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);
$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);

$graph->xaxis->SetTitleMargin(-7);
$graph->yaxis->SetTitleMargin(24);

$graph->xaxis->SetFont(FF_ARIAL, FS_BOLD, 9);
$graph->yaxis->SetFont(FF_ARIAL, FS_BOLD, 9);

$graph->xaxis->SetLabelMargin(20);

$graph->xaxis->SetLabelAngle(90);

$graph->yaxis->SetColor('red');
$graph->yaxis->SetWeight(3);

$graph->xaxis->SetColor('red');
$graph->xaxis->SetWeight(2);

$graph->ygrid->SetWeight(2);
$graph->ygrid->Show(true, true);

$graph->SetFrame(true, '#BDBDBD', 1);

$graph->SetShadow();

$graph->Stroke();
