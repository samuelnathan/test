<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 12/06/2014
 * Stations Cronbachs Graph
 * @Changed number to scenario_group
 * © 2014 Qpercom Limited. All rights reserved.
 */
use \JpGraph\JpGraph as JpGraph;
define('_OMIS', 1);

include __DIR__ . '/../extra/essentials.php';
//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}
//Require and Include
require_once 'Math/Stats.php';
JpGraph::load();
JpGraph::module('bar');
JpGraph::module('error');
JpGraph::module('line');
JpGraph::module('plotline');

/* * ** Definitions *** */
include_once '../analysis/analysis_definitions.php';

//Exit if no data from summary section [reference build_summary.php file]
if (!isset($_SESSION['stations_crons']) || count($_SESSION['stations_crons']) == 0) {
    echo 'Graph has not received data';
    exit;
}

//Data from summary section
$cronbachs_array = $_SESSION['stations_crons'];

//Remove the unwanted session variable
unset($_SESSION['stations_crons']);

//Combination/Filter Value
$cnm = $cronbachs_array['cnm'];

$xarray = array(); //x axis values
$yarray = array(); //the bar values

//[Loop Through Stations]
foreach ($cronbachs_array['values'] as $station) {
    //Add name and cronbach values to x and y arrays
    $xarray[] = (($cnm != STATION_COMBINE_TAG) ? 'Station ' : '') . $station['scenario_group'];
    $yarray[] = $station['cronbach'];
}

//[Create Graph]
$graph = new Graph(520, 470, 'auto');
$graph->img->SetMargin(50, 20, 20, 110);
$graph->img->SetImgFormat('png');

//Setup y axis scale
$graph->SetScale('textlin', 0, 1.0);

//Set major and minor tick to 10
$graph->yaxis->scale->ticks->Set(0.1);

//Set margin Color
$graph->SetMarginColor('#F5DEB3');

//Set up the x axis values
$graph->xaxis->SetTickLabels($xarray);

//Create bar plots
$bars = new BarPlot($yarray);

//Add the bars to the graph
$graph->Add($bars);

//Bars Settings
$bars->SetAbsWidth(14);
$bars->SetFillGradient('#DCDCDC', '#DCDCDC', GRAD_MIDVER);
$bars->SetColor('black');
$bars->SetLegend('cronbach alpha');
$bars->value->SetFont(FF_TIMES, FS_BOLD, 8);
$bars->value->SetAngle(90);
$bars->value->SetColor('black', 'darkred');
$bars->value->HideZero();
$bars->value->Show();
$bars->value->SetFormat('%s');
$bars->SetValuePos('center');

//Create 0.8 plot line
$plotline = new PlotLine(HORIZONTAL, 0.8, 'red', 2);

//Add average plot line
$graph->AddLine($plotline);

//Plotline Legend
$plotline->SetLegend('acceptable internal consistency');

//Graph Title
$graph->title->Set('Stations Cronbach Alpha');

//Add legend Data
$graph->legend->SetMarkAbsSize(6);
$graph->legend->SetFont(FF_ARIAL, FS_BOLD, 10);
$graph->legend->SetFrameWeight(1);
$graph->legend->SetColumns(10);
$graph->legend->SetVColMargin(10);
$graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
$graph->legend->SetShadow(false, 0);

//Add titles to x & y axis
$graph->xaxis->title->Set(' stations');
$graph->yaxis->title->Set(' cronbach alpha ');

//Set font for x & y axis titles
$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);
$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);

//Set margins for x & y axis
$graph->xaxis->SetTitleMargin(-10);
$graph->yaxis->SetTitleMargin(30);

//Set font for x & y axis
$graph->xaxis->SetFont(FF_ARIAL, FS_BOLD, 9);
$graph->yaxis->SetFont(FF_ARIAL, FS_BOLD, 9);

//Set label margins and angle
$graph->xaxis->SetLabelMargin(12);
$graph->xaxis->SetLabelAngle(90);

//Set colors of x & y axis
$graph->xaxis->SetColor('red');
$graph->yaxis->SetColor('red');

//Set weights of x & y axis
$graph->xaxis->SetWeight(2);
$graph->yaxis->SetWeight(3);

//Set y axis grid weight
$graph->ygrid->SetWeight(2);
$graph->ygrid->Show(true, true);

$graph->SetFrame(true, '#BDBDBD', 1);

//Add shadow to graph
$graph->SetShadow();

//Output Graph
$graph->Stroke();
