<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 16/06/2014
 * Stations Pass/Fail
 * @Changed number to scenario_group
 *
 * © 2014 Qpercom Limited. All rights reserved.
 */
define('_OMIS', 1);
use \JpGraph\JpGraph as JpGraph;

include __DIR__ . '/../extra/essentials.php';
//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

//Include more
JpGraph::module('bar');
JpGraph::module('error');
JpGraph::load();

/* * ** Definitions *** */
include_once '../analysis/analysis_definitions.php';

//Exit if no data from summary section [reference build_summary.php file]
if (!isset($_SESSION['stations_pf']['stations']) || count($_SESSION['stations_pf']['stations']) == 0) {
    echo 'Graph has not received data';
    exit;
}

//Data from summary section
$summary_array = $_SESSION['stations_pf'];

//Remove the unwanted session variable
unset($_SESSION['stations_pf']);

//Total # Students
$num_of_students = $summary_array['total_students'];

//Combination/Filter Value
$cnm = $summary_array['cnm'];

//Data Arrays
$fail_data = array();
$pass_data = array();
$x_array = array(); //x axis values
//Station Count
$station_cnt = count($summary_array['stations']);

//[Loop Through Stations]
foreach ($summary_array['stations'] as $station) {
    //Fail Count
    $fail_data[] = $station['fail_count'];

    //Pass Count
    $pass_data[] = $station['pass_count'];

    //Add station name
    $x_array[] = (($cnm != STATION_COMBINE_TAG) ? 'Station ' : '') . $station['scenario_group'];
}

//[Width between each of stations 2 bars]
$between_with = 0.9;

if ($station_cnt <= 5) {
    $between_with = 0.4;
}

//[Create Graph]
$graph = new Graph(520, 470, 'auto');
$graph->img->SetMargin(50, 20, 20, 110);
$graph->img->SetImgFormat('png');

/* (DW) Activate Image Anti-Aliasing (which will produce a cleaner-looking 
 * graph) only if it's possible on the current system.
 */
if (function_exists('imageantialias')) {
    //$radarGraph->img->SetAntiAliasing();
}

//Setup y axis scale
$graph->SetScale('textlin', 0, $num_of_students + 10);

//Set major and minor tick to 10
$graph->yaxis->scale->ticks->Set(10);

//Set margin Color
$graph->SetMarginColor('#F5DEB3');

//Set up the x axis values
$graph->xaxis->SetTickLabels($x_array);

//Create the 2 bar plots
$barplot1 = new BarPlot($pass_data);
$barplot2 = new BarPlot($fail_data);

//Create the grouped bar plot
$gbplot = new GroupBarPlot(array($barplot1, $barplot2));

$graph->Add($gbplot);

//Bar plots Settings
$barplot1->SetFillColor('#60AFFE');
$barplot1->SetAbsWidth(14);
$barplot1->SetLegend('pass');
$barplot1->value->SetFont(FF_TIMES, FS_BOLD, 8);
$barplot1->value->SetAngle(90);
$barplot1->value->SetColor('black', 'darkred');
$barplot1->value->HideZero();
$barplot1->value->Show();
$barplot1->SetValuePos('center');
$barplot1->value->SetFormat('%s');

$barplot2->SetFillColor('#FF0000');
$barplot2->SetAbsWidth(14);
$barplot2->SetLegend('fail');
$barplot2->value->SetFont(FF_TIMES, FS_BOLD, 8);
$barplot2->value->SetAngle(90);
$barplot2->value->SetColor('black', 'darkred');
$barplot2->value->HideZero();
$barplot2->value->Show();
$barplot2->SetValuePos('center');
$barplot2->value->SetFormat('%s');

$gbplot->SetWidth($between_with);

//Graph Title
$graph->title->Set('Stations Pass / Fail');

//Add legend Data
$graph->legend->SetMarkAbsSize(6);
$graph->legend->SetFont(FF_ARIAL, FS_BOLD, 10);
$graph->legend->SetFrameWeight(1);
$graph->legend->SetColumns(10);
$graph->legend->SetVColMargin(10);
$graph->legend->SetPos(0.5, 0.97, 'center', 'bottom');
$graph->legend->SetShadow(false, 0);

//Add titles to x & y axis
$graph->xaxis->title->Set(' Stations');
$graph->yaxis->title->Set(' Number of Students ');

//Set font for x & y axis titles
$graph->xaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);
$graph->yaxis->title->SetFont(FF_ARIAL, FS_BOLD, 8);

//Set margins for x & y axis
$graph->xaxis->SetTitleMargin(-10);
$graph->yaxis->SetTitleMargin(30);

//Set font for x & y axis
$graph->xaxis->SetFont(FF_ARIAL, FS_BOLD, 9);
$graph->yaxis->SetFont(FF_ARIAL, FS_BOLD, 9);

//Set label margins and angle
$graph->xaxis->SetLabelMargin(12);
$graph->xaxis->SetLabelAngle(90);

//Set colors of x & y axis
$graph->xaxis->SetColor('red');
$graph->yaxis->SetColor('red');

//Set weights of x & y axis
$graph->xaxis->SetWeight(2);
$graph->yaxis->SetWeight(3);

//Set y axis grid weight
$graph->ygrid->SetWeight(2);
$graph->ygrid->Show(true, true);

$graph->SetFrame(true, '#BDBDBD', 1);

//Add shadow to graph
$graph->SetShadow();

//Output Graph
$graph->Stroke();
