 <?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @Edit template for feedback email
  */
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;
 
 // Initial Variables & POST Variables
 $templateID = $subject = $hello = $bodyContent = "";
 $templateExists = false;
 $templateIDParam = filter_input(INPUT_GET, 'template', FILTER_SANITIZE_STRING);
 $updateFailed = (filter_input(INPUT_GET, 'updated', FILTER_SANITIZE_NUMBER_INT) === 0);
 $editMode = ((!is_null(filter_input(INPUT_GET, 'mode'))) || $updateFailed);
 
 // Load Feedback Templates
 $template = $db->feedback->getEmailTemplates($templateIDParam, true);
 
 if (!is_null($template)) {
 
     // We are cloning a template
     if ((filter_input(INPUT_GET, 'clone') == 'yes')) {

        $templateID = $db->feedback->cloneEmailTemplate($templateIDParam);
        $template = $db->feedback->getEmailTemplates($templateID, true);

     } else {

        $templateID = $templateIDParam;

     }
 
     // Template Exists
     $templateExists = true;
 
     // Filter Email Subject
     $subject = preg_replace("/\[student_id\]/", '', $template['email_subject']);
 
     // Filter Email Body
     $dom = new DOMDocument();
     $dom->loadHTML('<?xml encoding="UTF-8">' . adjustHTML($template['email_body']));
     $tagName = $dom->getElementsByTagName('*');
 
     $firstP = true;
     foreach ($tagName as $node) {
       
         if (in_array($node->nodeName, ['p', 'ol', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'table'])) {
           
             if ($firstP && $node->nodeName == 'p') {
               
                $firstP = false;
                $hello = strip_tags(preg_replace("/ \[forename\] \[surname\],/", '', $dom->saveXML($node)));
               
             } else {
               
                $bodyContent .= $dom->saveXML($node);
               
             }
             
         }
         
     }
     
 }

 // Default pdf instructions (if empty or not HTML)
 if (empty($template['pdf_instructions']))
    $template['pdf_instructions'] = (new OMIS\Template("pdf_instructions_template.html.twig"))->render([], true);
  
 // Render the template
 (new OMIS\Template('template_tool.html.twig'))->render([
    'body_content' => $bodyContent,
    'instructions_content' => adjustHTML($template['pdf_instructions']),
    'instructions_enabled' => $template['pdf_instructions_enabled'],
    'edit_mode' => $editMode,
    'hello' => $hello,
    'orgname' => $template['template_name'],
    'email_cc' => $template['email_cc'],
    'subject' => $subject,
    'template_exists' => $templateExists,
    'template_id' => $templateID,
    'update_failed' => $updateFailed
]);
 