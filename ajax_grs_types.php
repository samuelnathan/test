<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @JSON Global Rating Scale Types Admin Tool Section
 */

define('_OMIS', 1);

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Utilities\JSON as JSON;

if (isset($_POST['isfor'])) {
  
    include __DIR__ . '/../extra/essentials.php';
     //Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        return false;
        
    }
    
} else {
  
    include __DIR__ . '/../extra/noaccess.php';
    
}

// Clean 'isfor' variable
$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

// Encode data to JSON
$encodeJSON = false;

// The return string
$returnVariable = "";

// Add Global Rating Scale type
if ($isfor == "add_grs_type") {
  
  // Clean POST Variables
  $scaleTypeID = filter_input(INPUT_POST, 'scale_type_id');
  $scaleTypeName = filter_input(INPUT_POST, 'scale_type_name');
  $scaleTypeColour = filter_input(INPUT_POST, 'scale_type_colour');
    
  $success = $db->forms->insertRatingScaleType($scaleTypeID, $scaleTypeName, $scaleTypeColour);
  $returnVariable = (($success)? 'RECORD_SAVED' : 'RECORD_NOT_SAVED');
}

// Update Global Rating Scale type
else if ($isfor == "update_grs_type") {
  
  // Clean POST Variables
  $recordKey = filter_input(INPUT_POST, 'record_key');
  $scaleTypeID = filter_input(INPUT_POST, 'scale_type_id');
  $scaleTypeName = filter_input(INPUT_POST, 'scale_type_name');
  $scaleTypeColour = filter_input(INPUT_POST, 'scale_type_colour');
    
  $success = $db->forms->updateRatingScaleType($recordKey, $scaleTypeID, $scaleTypeName, $scaleTypeColour);
  $returnVariable = (($success)? 'RECORD_UPDATED' : 'RECORD_NOT_UPDATED');
}

// Delete Global Rating Scale Types 
else if ($isfor == "delete_grs_types") {
  
  // Clean POST Variables
  $scaleTypes = filter_input(INPUT_POST, 'records_to_delete', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
 
  // Delete Rating Scale Types
  $success = $db->forms->deleteRatingScaleTypes($scaleTypes);
  $returnVariable = (($success)? 'RECORDS_DELETED' : 'RECORDS_NOT_DELETED');
  
}
// Get All Global Rating Scale Types 
else if ($isfor == "get_all_grs_types") {
  
  // Encoding to JSON
  $encodeJSON = true;
  
  // json_encode takes care of Html Entities
  $db->set_Convert_Html_Entities(false);
  
  // Get Rating Scale Types
  $scaleTypes = $db->forms->getRatingScaleTypes();
  
  $returnVariable = $scaleTypes;
  
}
// Get Global Rating Scale Type 
else if ($isfor == "get_grs_type") {
  
  // Encoding to JSON
  $encodeJSON = true;
 
  // Record key
  $recordKey = filter_input(INPUT_POST, 'record_key', FILTER_SANITIZE_STRIPPED);
  
  // json_encode takes care of Html Entities
  $db->set_Convert_Html_Entities(false);
  
  // Get Rating Scale Type
  $scaleType = $db->forms->getRatingScaleTypes($recordKey);

  $returnVariable = $scaleType;
   
}

else {
  $returnVariable = "REQUEST_NOT_PROCESSED";
}

// Return data, raw or encoded in JSON
if ($encodeJSON) {
  try {
    echo JSON::encode($returnVariable);
  } catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnArray, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
  }
} else {
   echo $returnVariable;
}