<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 22/05/2015
 * © 2015 Qpercom Limited. All rights reserved.
 * @Send SMS Examiners
 */
/* Defined to tell any included files that they have been included rather than
 * being invoked directly from the browser. Need to declare it anywhere that a
 * file is directly invoked by the user (via AJAX).
 */
define('_OMIS', 1);

#ob_start();

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

error_reporting(E_ALL & ~E_DEPRECATED);
include __DIR__ . '/../extra/essentials.php';

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

#Initial Variables
$recipients_records = array();
$sms_id = NULL;
$credits_left = 0;
$sent = 0;

#A few checks
if (!isset($_POST['sms_message']) || !isset($_POST['sms_examiners']) || count($_POST['sms_examiners']) == 0) {
    header('Location: ../manage.php?page=sms_examiners&smode=submitted&sent=0');
}

#Start Transaction
$db->query("START TRANSACTION");

#SMS Message
$sms_message = trim($_POST['sms_message']);

#Record Message
$rsm_return = $db->sms->recordSMSMessage($_SESSION['user_identifier'], $sms_message, count($_POST['sms_examiners']));

#SMS Message Recorded
if ($rsm_return[0]) {
    #Sms ID
    $sms_id = $rsm_return[1];

    #Send SMS to Recipients
    foreach ($_POST['sms_examiners'] as $examiner_id) {
        #Get Examiner Details
        $examiner_details = $db->users->getExaminer($examiner_id);

        #Gather Recipients Records
        $recipients_records[] = array($sms_id, $examiner_id, $examiner_details['contact_number'], 0);
    }

    #Send SMS
    include 'sms_examiners_protocol.php';

    #Record SMS Message Recipients
    $db->sms->recordSMSMessageRecipients($recipients_records);

    #Commit Transaction
    $db->query("COMMIT");
} else {
    #Rollback Transaction
    $db->query("ROLLBACK");
}
header('Location: ../manage.php?page=sms_examiners&smode=submitted&sent=' . $sent . '&sms_id=' . $sms_id . '&credits=' . $credits_left);
