<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
 define('_OMIS', 1);

 // Make sure we have a working autoloader without declaring it twice...
 global $loader;

 if (!isset($loader)) {

    $loader = require __DIR__ . '/../vendor/autoload.php';
 }

 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Template as Template;
 use \OMIS\Auth\Role as Role;

 // What action are we taking or mode are we in (add, edit, submit, update)
 $mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

 if (!is_null($mode)) {

    require_once __DIR__ . '/../extra/essentials.php';
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

        return false;

    }
    require_once 'examiners_functions.php';

 } else {

    require_once __DIR__ . '/../extra/noaccess.php';

 }

 /* 
  * What academic term ID to use. If no academic term
  * is selected then use the system term
  */

 if (!isset($_SESSION['temp_term']) || strlen($_SESSION['temp_term']) == 0) {

     if ($db->academicterms->existsById($_SESSION['cterm'])) {

         $inTerm = $_SESSION['cterm'];

     } else {

         $inTerm = $db->academicterms->getDefaultTerm()['term_id'];

     }

  // Or else use the selected term
  } else {

    $inTerm = $_SESSION['temp_term'];

  }

  $examinerRoleCategory = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, false);
  $rolesAvailable = $examinerRoleCategory->getRoles();

  $forDeptParam = filter_input(INPUT_POST, 'fordept', FILTER_SANITIZE_STRING);
  $roleFiltered = filter_input(INPUT_POST, 'role_filtered', FILTER_SANITIZE_NUMBER_INT);

  // Add training status field if feature enabled
  $trainingStatus = $db->features->enabled('training-status', $_SESSION['user_role']);

  if (!is_null($forDeptParam)) {

     $forDepartment = trim($forDeptParam);

     if ($forDepartment == "") {

         $forDepartment = null;

     }

  }

  // Add Examiner Record
  if ($mode == 'add_examiner') {

      $terms = $db->academicterms->getAllTerms();

      ?>
      <td class="checkb">&nbsp;</td>
      <td><input type="text" id="add_exmnr_id" class="tf4 form-control form-control-sm"/></td>
      <td><input type="text" id="add_exmnr_fn" class="tf3 form-control form-control-sm"/></td>
      <td><input type="text" id="add_exmnr_sn" class="tf2 form-control form-control-sm"/></td>
      <td><input type="text" id="add_exmnr_em" class="tf2 form-control form-control-sm"/></td>
      <td><input type="text" id="add_exmnr_no" class="tf8 form-control form-control-sm" title="Contact number" autocomplete="false"/></td>

      <?php
      // Include template for password fields
      $template = new Template("password_fields.html.twig");
      $template->render([
          'show_change_checkbox' => false,
          'enable_password_fields' => true
      ]);
      ?>
      <td>
          <select id="add_exmnr_lvl" class="custom-select custom-select-sm w-auto">
              <?php
              /* We're adding a new user here, so they can't be an Exam Admin if the user
               * that's currently logged in is also an Exam Admin. If they're an Admin or
               * Super Admin, though, it's fine. No other user roles should be allowed to
               * access this page.
               */
              if ($_SESSION['user_role'] == Role::USER_ROLE_EXAM_ADMIN) {
                  $rolesAvailable = filterAvailableRoles(
                      $rolesAvailable,
                      []
                  );
              }

              // Loop through available roles and render options
              foreach ($rolesAvailable as $role) {
                  $optionSelected = ($roleFiltered == $role->role_id) ? ' selected="selected"' : "";
                  ?>
                  <option value="<?=$role->role_id?>" title="<?=gettext($role->name)?>
                  Role: <?=gettext($role->description)?>"<?=$optionSelected?>>
                      <?=gettext($role->shortname)?>
                  </option>
              <?php }
              ?>
          </select>
      </td>
      <td id="add-dpt" class="edepts">
          <select id="choose-term" class="custom-select custom-select-sm w-auto">
          <?php

              foreach ($terms as $termID => $term) {

                  ?><option value="<?=$termID?>"
                  <?=($inTerm == $termID ? 'selected="selected"' : "") ?>><?=$termID?>
                  </option><?php

              }

           ?>
          </select>
          <?php
          renderDepartments('add', $forDepartment, null);
          ?></td>
      <?php if ($trainingStatus) { ?>
          <td>
              <select id="add_exmnr_train" class="custom-select custom-select-sm w-auto">
                  <option value="0" class="red" title="User not trained">Not Trained</option>
                  <option value="1" class="green" title="User trained">Trained</option>
              </select>
          </td>
      <?php } ?>
      <td>
          <select id="add_exmnr_ac" class="custom-select custom-select-sm w-auto">
              <option value="1" class="green" title="Account enabled">Enabled</option>
              <option value="0" class="red" title="Account disabled">Disabled</option>
          </select>
      </td>
      <td class="titleedit2">&nbsp;</td>
      <?php

  // Edit Examiner Record
 } elseif ($mode == 'edit_examiner') {
     // Editing examiner
     $examinerID = filter_input(INPUT_POST, 'examiner');
     $examiner = $db->users->getExaminer($examinerID);
     if (!is_null($examiner)) {
         ?>
        <td class="checkb">&nbsp;</td>
         <td><input type="text" id="edit_exmnr_id" class="tf4 form-control form-control-sm" value="<?=$examiner['user_id']?>"/></td>
        <td><input type="text" id="edit_exmnr_fn" class="tf3 form-control form-control-sm" value="<?=$examiner['forename']?>"/></td>
         <td><input type="text" id="edit_exmnr_sn" class="tf2 form-control form-control-sm" value="<?=$examiner['surname']?>"/></td>
        <td><input type="text" id="edit_exmnr_em" class="tf2 form-control form-control-sm" value="<?=$examiner['email']?>"/></td>
         <td><input type="text" id="edit_exmnr_no" class="tf8 form-control form-control-sm" value="<?=$examiner['contact_number']?>" title="Contact number"/></td>
        
         <?php
        // Include template for password fields
          $template = new Template("password_fields.html.twig");
         $data = ['show_change_checkbox' => true,
                   'enable_password_fields' => false];
          $template->render($data);
       
         ?>
        
         <td>
            <select id="edit_exmnr_lvl" class="custom-select custom-select-sm w-auto">
         <?php
         /* If the currently logged in user is an Exam Admin, then we need to
         * prevent them from elevating other users to be Exam Admins. However,
          * if that user is already an Exam Admin, then that option needs to
          * exist in the list to select from.
         */
         /* @TODO: Implement a role hierarchy explaining which users can create
          * which client types.
         */
         /* This page should only be accessible by Super Admins, Admins and Exam
          * Admins. Of these Super admins and "regular" admins can create any type
          * of user listed on this page, but exam admins shouldn't be able to
          * create other exam admins. So, we need to filter them out of this list.
          */
         if ((int)$examiner['user_role'] !== Role::USER_ROLE_EXAM_ADMIN) {
            //error_log(__FILE__ . ": Filtering list.");
             if ((int) $_SESSION['user_role'] == Role::USER_ROLE_EXAM_ADMIN) {
                 $rolesAvailable = filterAvailableRoles(
                     $rolesAvailable,
                    []
                 );
             }
         }

         foreach ($rolesAvailable as $role):
             $selected = ($examiner['user_role'] == $role->role_id);
             $selectedAttribute = ($selected ? ' selected="selected"' : '');
             ?>
             <option value="<?=$role->role_id?>" title="<?=gettext($role->name)?> Role: <?=gettext($role->description)?>"<?=$selectedAttribute?>>
                 <?=gettext($role->shortname)?>
             </option>
             <?php
         endforeach;

         $examinerTrained = ($examiner['trained'] == 1);
         $examinerTrainedSelect = ($examinerTrained) ? ' selected="selected"' : '';

         $examinerEnabled = ($examiner['activated'] == 0);
         $examinerEnabledSelect = ($examinerEnabled) ? ' selected="selected"' : '';
         ?>
         </select>
         </td>
         <td id="edit-dpt" class="edepts"><?php
           renderDepartments('edit', null, $examinerID);
         ?></td>
         <?php if ($trainingStatus) { ?>
         <td>
             <select id="edit_exmnr_train" class="custom-select custom-select-sm w-auto">
                 <option value="0" class="red" title="User not trained">Not Trained</option>
                 <option value="1" class="green" <?=$examinerTrainedSelect?> title="User trained">Trained</option>
             </select>
         </td>
         <?php
         } else {
            ?><input type="hidden" id="edit_exmnr_train" value="<?=$examiner['trained']?>"/><?php
         } ?>
         <td>
             <select id="edit_exmnr_ac" class="custom-select custom-select-sm w-auto">
                 <option value="1" class="green" title="Account enabled">Enabled</option>
                 <option value="0" class="red" <?=$examinerEnabledSelect?> title="Account disabled">Disabled</option>
             </select>
         </td>
         <td class="titleedit">
             <input type="hidden" id="examiner_nr" value="<?=$examinerID?>"/>
         </td>
         <?php
     } else {
         echo 'USER_NON_EXIST';
     }

 // Submit Examiner Record
 } elseif ($mode == 'submit_examiner') {

     $examinerID = filter_input(INPUT_POST, 'exmnr_id');

     // Invalid Identifier, then abort
     if (!identifierValid($examinerID)) {

        error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
        exit;

     }

     // Make sure the examiner ID isn't already in use.
     if ($db->users->doesUserExist($examinerID)) {

         echo 'USER_ALREADY_USED';

     } else {

         if ($db->academicterms->existsById($_POST['exmnr_term'])) {

             $result = $db->users->insertExaminer(
                 $examinerID,
                 $_POST['exmnr_fn'],
                 $_POST['exmnr_sn'],
                 $_POST['exmnr_em'],
                 $_POST['exmnr_no'],
                 $_POST['exmnr_pass'],
                 $_POST['exmnr_lvl'],
                 $_POST['exmnr_ac'],
                 $_POST['exmnr_train'],
                 $_POST['depts'],
                 $_POST['exmnr_term']
             );

             if (!is_bool($result)) {

                 // If this result isn't a boolean, then it's an error message...
                 echo $result;

             } else {

                 echo ($result) ? 'true' : 'false';

             }
         } else {

             echo 'TERM_NON_EXIST';

         }
     }

 // Update Examiner Record
 } elseif ($mode == 'update_examiner') {

     // Update
     $originalID = trim($_POST['org_exmnr_id']);
     $examinerID = trim($_POST['exmnr_id']);
     if ($db->academicterms->existsById($inTerm)) {
         if ($db->users->doesUserExist($originalID)) {

             // Invalid Identifier, then abort
             if (!identifierValid($examinerID)) {
                error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
                exit;
             }

             if ($examinerID != $originalID && $db->users->doesUserExist($examinerID)) {
                 echo 'USER_ALREADY_USED';
             } else {
                 $success = $db->users->updateExaminer(
                     $examinerID,
                     $originalID,
                     $_POST['exmnr_fn'],
                     $_POST['exmnr_sn'],
                     $_POST['exmnr_em'],
                     $_POST['exmnr_no'],
                     $_POST['exmnr_pass'],
                     $_POST['exmnr_lvl'],
                     $_POST['exmnr_ac'],
                     $_POST['exmnr_train'],
                     $_POST['exmnr_passc'],
                     $_POST['depts'],
                     $inTerm
                 );

                 if ($success) {
                     // If user is examiner set edited warning flag and update user details
                     if ($_SESSION['user_identifier'] == $originalID) {
                         $_SESSION['editing_warn'] = true;
                         $_SESSION['user_forename'] = $_POST['exmnr_fn'];
                         $_SESSION['user_surname'] = $_POST['exmnr_sn'];
                         $_SESSION['user_email'] = $_POST['exmnr_em'];
                         $currentUser = true;
                     } else {
                         $currentUser = false;
                     }

                     // If new ID is not the same as old ID
                     if ($originalID != $examinerID) {
                         if ($currentUser) {
                             $_SESSION['user_identifier'] = $examinerID;
                         }
                         $db->users->updateExaminerID($examinerID, $originalID);
                         $db->forms->changeAuthorID($examinerID, $originalID);
                     }
                 }
             }
         } else {
             echo 'USER_NON_EXIST';
         }
     } else {
         echo 'TERM_NON_EXIST';
     }
 }
