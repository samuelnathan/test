<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */
 global $page;


 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 use \OMIS\Auth\Role as Role;
 use \OMIS\Auth\RoleCategory as RoleCategory;

 // Page Access Check / Can User Access this Section?
 if (!Role::loadID($_SESSION['user_role'])->canAccess($page)) {

     return false;

 }

 require_once 'examiners_functions.php';

 // Initial Variables
 $revalidateParam = filter_input(INPUT_GET, 'revalidate', FILTER_SANITIZE_NUMBER_INT);
 $revalidate = ($revalidateParam == 1);
 $sourceParam = filter_input(INPUT_GET, 'source', FILTER_SANITIZE_STRING);
 $rowCount = 0;
 $total = 0;

 // Count of list passed in
 if ($revalidate && isset($_SESSION['examiners_need_validation'])) {
     $total = count($_SESSION['examiners_need_validation']);
 } elseif (!$revalidate && isset($_SESSION['examiners_edit']) && is_array($_SESSION['examiners_edit'])) {
     $total = count($_SESSION['examiners_edit']);
 }

 // List of examiners
 $rolesExamAdmin = (new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, false))->getRoles();

 // Add training status field if feature enabled
 $trainingStatus = $db->features->enabled('training-status', $_SESSION['user_role']);

 ?>
 <div class="topmain">
 <div class="row">
    <div class="col-lg-5 col-sm-6 col-12">
    <div class="card d-inline-block align-top w-100 h-100">
      <div class="card-header">
        <div class="card-title h6 mb-0">
          <div class="row no-gutters align-items-center">
            <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
            <span class="card-title-text">
              Edit <?=gettext('Exam-Team-Placeholder') . gettext(' Team') ?>
            </span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="card-text">
          You can edit and update users on this section.<br/>
          <?php if ($sourceParam != "import") { ?><br/>You can also set a common password for all team members.<? } ?>
        </div>   
      </div> 
    </div> 
    </div>

    <div class="col-auto<?=($sourceParam == "import" ? ' hide' : '') ?>">
      <div class="card d-inline-block align-top w-100 h-100">
        <div class="card-header">
          <div class="card-title h6 mb-0">
            <div class="row no-gutters align-items-center">
              <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
              <span class="card-title-text">
                Common Password (Optional)
              </span>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="card-text">
            <input class="form-control form-control-sm w-auto d-inline-block" type="password" id="common-password-field"/><input type="button" id="common-password-button" class="btn btn-sm btn-success" value="apply"/>
          </div>   
        </div> 
      </div> 
    </div>
  </div>
  <div class="topmain-clear"></div>
 </div>
 <div class="contentdiv">
 <?php

 if ($revalidate) :
     ?>
     <div class="edit-errordiv">
         User Identifiers specified are already in use, please try to use different Identifiers
     </div>
     <?php
 endif;
 ?>

 <form name="examredit_list_form" id="examredit_list_form" method="post"
     action="examiners/examiners_redirect.php" onsubmit="return(ValidateForm(this));">
 <table id="examredit_table" class="table-border">
  <tr id="title-row" class="title-row">
  <td class="all-checkbox-td">&nbsp;</td>
  <th>Identifier</th>
  <th>Forenames</th>
  <th>Surname</th>
  <th>Email Address</th>
  <th>Contact Number</th>
  <th>Password</th>
  <th>User Role</th>
  <th><?=gettext('Departments')?></th>
  <?php if ($trainingStatus) { ?>
  <th>Training</th>
  <?php } ?>
  <th>Account</th>
  <td class="title-edit-top">&nbsp;</td>
  </tr>
 <?php

 // Output examiner records
 if ($total > 0) {
     for ($i=0; $i<$total; $i++) {
         // Row Count
         $rowCount = $i + 1;

         // Are we revalidating?
         if ($revalidate) {
             $record = $_SESSION['examiners_need_validation'][$i];
             $originalID = trim($record['org_id']);
             $newID = trim($record['new_id']);
             $selectedDepts = $record['depts'];
             $changePassword = $record['change_pass'];
         } else {
             $originalID = trim($_SESSION['examiners_edit'][$i]);
             $newID = $originalID;
             $record = $db->users->getExaminer($originalID);
             $selectedDepts = null;
             $changePassword = 0;
         }
   // Examiner record exists ?
   if (!is_null($record)) {

   // Revalidate CSS clas
   $revalidateClass = ($revalidate) ? " revalidate" : "";
   $trainingYes = ($record['trained'] == 1) ? ' selected="selected"' : '';
   $enabledDisabled = ($record['activated'] == 0) ? ' selected="selected"' : '';

 ?>
   <tr class="edit-exmr-rows">
   <td class="checkb">&nbsp;<input type="hidden" class="orgID form-control form-control-sm" value="<?=$originalID?>"/></td>
   <td><input type="text" name="records[<?=$originalID?>][id]" class="tf4 newID form-control form-control-sm <?=$revalidateClass?>" value="<?=$newID?>"/></td>
   <td><input type="text" name="records[<?=$originalID?>][forenames]" class="tf3 form-control form-control-sm" value="<?=$record['forename']?>"/></td>
   <td><input type="text" name="records[<?=$originalID?>][surname]" class="tf2 form-control form-control-sm" value="<?=$record['surname']?>"/></td>
   <td><input type="text" name="records[<?=$originalID?>][email]" class="tf5 email form-control form-control-sm" value="<?=$record['email']?>"/></td>
   <td><input type="text" id="exmnr_no<?=$rowCount?>" name="records[<?=$originalID?>][telephone]" class="tf8 form-control form-control-sm" value="<?=$record['contact_number']?>"/></td>
   <?php

     $passwordFieldName = "records[". $originalID ."][password]";
     $changeFieldName = "records[". $originalID ."][change]";

     // Include template for password fields
     $template = new OMIS\Template("password_fields.html.twig");
     $data = [
       'show_change_checkbox' => true,
       'enable_password_fields' => false,
       'password_field_name' => $passwordFieldName,
       'change_field_name' => $changeFieldName
     ];
     $template->render($data);

   ?>
   <td>
    <select name="records[<?=$originalID?>][role]" class="custom-select custom-select-sm">
 <?php
     foreach ($rolesExamAdmin as $role):
       $selected = ($record['user_role'] == $role->role_id);
       $selectedAttribute = ($selected ? ' selected="selected"' : '');
 ?>
       <option value="<?=$role->role_id?>" title="<?=gettext($role->name)?> Role: <?=gettext($role->description)?>"<?=$selectedAttribute?>>
       <?=gettext($role->shortname)?>
       </option>
 <?php
     endforeach;
 ?>
    </select>
   </td>
   <td class="edepts">
   <?php
      renderDepartments('xedit', null, $originalID, $rowCount, $selectedDepts);
   ?>
   </td>
   <?php if ($trainingStatus) { ?>
     <td>
      <select name="records[<?=$originalID?>][trained]" class="custom-select custom-select-sm">
        <option value="0" class="red" title="User not trained">Not Trained</option>
        <option value="1" class="green"<?=$trainingYes?> title="User trained">Trained</option>
      </select>
     </td>
   <?php } else { ?>
     <input type="hidden" name="records[<?=$originalID?>][trained]" value="<?=$record['trained']?>"/>
   <?php } ?>
   <td>
    <select name="records[<?=$originalID?>][enabled]" class="custom-select custom-select-sm">
      <option value="1" class="green" title="Account enabled">Enabled</option>
      <option value="0" class="red"<?=$enabledDisabled?> title="Account disabled">Disabled</option>
    </select>
   </td>
   <td class="titleedit">&nbsp;</td>
   </tr><?php
     }
   }
 }

  // Bottom rows
  if ($rowCount == 0) {
   $message = "The users selected to edit do not exist, click the 'back' button to return to list";
   ?>
   <tr id="first_examredit_row">
   <td colspan="<?=($trainingStatus) ? 13 : 12 ?>" class="mess">
    <div id="examredit_row_div" class="tablestatus2"><?=$message?></div>
   </td>
   </tr>
   <?php
  }
   ?>
   <tr id="base-tr">
   <td colspan="<?=($trainingStatus) ? 11 : 10 ?>" class="base-button-options" id="base-button-td">
   <div class="fr" id="add-div">
   <?php
     $cancelButtonCss = ($rowCount == 0) ? 'back' : 'cancel';
   ?>
   <input type="button" id="cancel-edit-exmrs" value="<?=$cancelButtonCss?>" class="btn btn-sm btn-danger"/>
   <?php

    // Records to update? then display the update button
    if ($rowCount > 0) {
    ?>
    <input type="submit" value="update" name="update_multiple_examiners" class="btn btn-sm btn-success"/>
    <?php
    }

    /* Password strength settings for the password fields
     * Plugin: pwstrength bootstrap Jquery
     */
    if (isset($config->passwords)) {
    ?>
     <script>
      // pass PHP variable declared above to JavaScript variable
      var verdictList = <?=json_encode(['weak', 'normal', 'medium', 'strong', 'very strong'])?>;
      var passwordSettings = <?=json_encode($config->passwords)?>;
     </script>
    <?php
    }

    ?>
   </div>
   </td>
   <td class="nbb nbr"></td>
   </tr>
  </table>
 </form>
 </div>
 <?php

 // Clean up some variables
 if (isset($_SESSION['examiners_need_validation'])) {
     unset($_SESSION['examiners_need_validation']);
 }