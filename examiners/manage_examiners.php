<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
use \OMIS\Auth\Role as Role;
use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Load configuration file
global $config;
if (!isset($config)) {
    $config = new OMIS\Config;
}

// Load what's required
require_once "examiners_functions.php";
require_once __DIR__ . "/../extra/helper.php";

// Define variables required later on
$exportData = [];
$users = [];
$userCount = 0;
$pageNr = 1;

// Get user roles
$userRolesExaminer = (new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, false))->getRoles();

// Filtered values
list($deptFilter,
     $termFilter,
     $roleFilter,
     $accountFilter,
     $forenameFilter,
     $surnameFilter,
     $search,     
     $filterReady) = examinerFiltering();

// Collection of data filters
$dataFilters = [
    $deptFilter, 
    $termFilter,
    $roleFilter,
    $accountFilter,
    $forenameFilter,
    $surnameFilter,
    $search
];

// Get list of examiners
if ($filterReady) {
    
    /**
     * If a new user has been added then order the list
     * by created so that they appear at the top.
     * If not just order by user surname.
     */
    $newExaminerCreated = filter_input(INPUT_GET, 'created', FILTER_SANITIZE_STRING);
    
    if (!is_null($newExaminerCreated)) {

        $defaultDatabaseOrder = 'users.created_at DESC';

    } else {

        $defaultDatabaseOrder = 'users.surname';

    }
    
    $users = $db->users->getExaminers(
        $dataFilters,
        $defaultDatabaseOrder
    );

    $userCount = count($users);

 }

 // Table fields for list ordering
 $fields = [
    'user_id',
    'forename',
    'surname',
    'email',
    'contact_number',
    'password',
    'user_role',
    'contact_number',
    'last_login',
    'activated'
 ];

 // Add training status field if feature enabled
 $trainingStatus = $db->features->enabled('training-status', $_SESSION['user_role']);
 if ($trainingStatus) array_splice($fields, -1, 0, 'trained');

// Prepare for list ordering
$defaultOrderType = 'desc';
$defaultOrderIndex = 0;
list($orderType, $orderIndex, $orderMap) 
     = prepareListOrdering($defaultOrderType, $defaultOrderIndex, $fields);

// If Count = 0 Disable Ordering
if ($userCount == 0) {
    $orderIndex = 'DISABLED';
}
// Which field (column) to order
$toOrder = $orderMap[$orderIndex];

// Get current page number parameter
$pageNrParam = filter_input(INPUT_GET, 'num', FILTER_SANITIZE_NUMBER_INT);
if (strlen($pageNrParam) > 0 && is_numeric($pageNrParam)) {
    $pageNr = $pageNrParam;
}

// Get the number of records per page from the config file
$itemsPerPage = $config->localization['pagination']['items_per_page'];

// Get total number of records for display
$pageCount = ceil($userCount / $itemsPerPage);

// Reset page number if it doesn't make sense
if ($pageNr < 0 || $pageNr > $pageCount) {
    $pageNr = 1;
}

// Principal url arguments template
$urlArgsTemplate = "page=manage_examiners"
                 . "&amp;dept=%s"
                 . "&amp;term=%s"
                 . "&amp;role=%s"
                 . "&amp;account=%s"
                 . "&amp;forenames=%s"
                 . "&amp;surnames=%s"
                 . "&amp;search=%s";

// Apply params to principal url
$mainUrl = "manage.php?" . vsprintf(
    $urlArgsTemplate,
    $dataFilters
);

// Pagination url arguments template
$pageArgsTemplate = $mainUrl
                  . "&amp;order_index=%s"
                  . "&amp;order_type=%s"
                  . "&amp;num=";

// Apply params to principal url
$paginationUrl = vsprintf(
    $pageArgsTemplate,
    [$orderIndex, $orderType]
);

// Lists sorting url arguments template
$sortArgsTemplate = $mainUrl
                  . "&amp;num=%s";


// Apply params to principal url
$sortUrl = vsprintf(
    $sortArgsTemplate,
    [$pageNr]
);

?>
<div class="topmain">
<?php 

   /*
    *  Render filtering fields, academic term,
    *  department, account and so on...
    */
   renderFilterFields($dataFilters);
?>
</div>
<div id="content-div" class="contentdiv">

<?php
  
  /* 
   * What academic term ID to use. if it's filtered in the dropdown as 'Any'
   * then use the system term
   */
  // If no academic term is selected then use the system term     
  if (!isset($_SESSION['temp_term']) || strlen($_SESSION['temp_term']) == 0) {

      if ($db->academicterms->existsById($_SESSION['cterm'])) {

          $lastKnownTerm = $_SESSION['cterm'];

      } else {

          $lastKnownTerm = $db->academicterms->getDefaultTerm()['term_id'];

      }

    // Or else use the selected term
  } else {

      $lastKnownTerm = $_SESSION['temp_term'];

  }

  /* Render the pagination controls if there's more
   * than a page worth of users.
   */
  if ($userCount > $itemsPerPage) {
    $template = new Template("pagination.html.twig");
    $data = [
      'pageCount' => $pageCount,
      'currentPage' => $pageNr,
      'pageURL' => $paginationUrl
    ];
    $template->render($data);
  }
  
?>
<form name="examiner_list_form" id="examiner_list_form" method="post" 
         action="examiners/examiners_redirect.php" onsubmit="return(ValidateForm(this));" autocomplete="false">
 <input style="display:none;" type="text" name="disable_chrome_autfill"/>
 <input style="display:none;" type="password" name="disable_chrome_autfill"/>
 <table id="examiner_table" class="table-border">
<?php
caption_Session_Warn(
    "editing_warn",
    "The logged on user <span class='ul'>". $_SESSION['user_identifier'] ."</span> is not permitted "
    . "to delete, disable or change the access level of their own account"
);
    if ($filterReady) {
?>
     <tr>
      <td id="filtered-info-cell" colspan="<?=(count($fields) + 2)?>">
         <b><?=count($users)?></b> users match selected filter criteria
      </td>
     </tr>
<?php
    }
    
    // Disable all checkbox ?
    $disableAllCheck = ($userCount == 0) ? ' disabled="disabled"' : "";
    $passwordTitleClass = ($userCount == 0) ? "password-title" : "not-allowed fw-normal";

?>
     <tr id="title-row" class="title-row">
       <td class="all-checkbox-td">
         <input type="checkbox" class="check" id="check-all"<?=$disableAllCheck?>/>
       </td>
       <th><?=makeColumnOrderable('Identifier', $toOrder, 'user_id', 1, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><?=makeColumnOrderable('Forenames', $toOrder, 'forename', 2, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><?=makeColumnOrderable('Surname', $toOrder, 'surname', 3, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><?=makeColumnOrderable('Email Address', $toOrder, 'email', 4, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><?=makeColumnOrderable('Contact', $toOrder, 'contact_number', 5, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><a href="javascript:void(0)" class="<?=$passwordTitleClass?>" title="You cannot sort the password field">Password</a></th>
       <th><?=makeColumnOrderable('User Role', $toOrder, 'user_role', 7, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <th><a href="javascript:void(0)" class="<?=$passwordTitleClass?>" title="You cannot sort the <?php
          echo gettext('department')?> field as it can have multiple values per record"><?=gettext('Department')?></a></th>
       <th class="last-login"><?=makeColumnOrderable('Last Login', $toOrder, 'last_login', 9, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <?php if ($trainingStatus) { ?>
       <th><?=makeColumnOrderable('Training', $toOrder, 'trained', 10, $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <?php } ?>
       <th><?=makeColumnOrderable('Account', $toOrder, 'activated', count($fields), $orderType, $users, $sortUrl, 'standard', null, null)?></th>
       <td class="title-edit-top"> 
        <input type="hidden" id ="dept_sel" name="dept_sel" value="<?=$deptFilter?>"/>
        <input type="hidden" value="<?=$lastKnownTerm?>" id="last-known-term"/>
       </td>
      </tr>
 <?php
            
      if (!$filterReady) {
        // Message to inform user how to display list of examiners
        $message =  "To view lists of " . gettext('examiners') . " please select a " . gettext('department') . ", "
                  . gettext('year') . " &amp; user role from the filter list above";
        
        ?><tr id="first_examiner_row">
          <td colspan="<?=(count($fields) + 2)?>" class="mess2">
           <div id="examiner_row_div" class="tablestatus1"><?=$message?></div>
          </td>
         </tr><?php
      } elseif ($userCount > 0) {
        $rowIndex = 0;
        $overallIndex = 0;
        $pageEnd = $pageNr * $itemsPerPage;
        $pageStart = $pageEnd - $itemsPerPage;
  
        // Render user records
        foreach ($users as $user) {
         if (($overallIndex >= $pageStart && $overallIndex < $pageEnd) || $pageNr == 0) {
            
           // User ID
           $userID = $user['user_id'];
             
           // last login formatted
           $lastLogin = format_DateTime($user['last_login']);
                        
           // Build Export Row
           $rowIndex++;
           $exportData[$userID] = [
                "forename" => $user['forename'],
                "surname" => $user['surname'],
                "email" => $user['email'],
                "contact_number" => $user['contact_number'],
                "user_role" => $user['user_role'],
                "last_login" => $user['last_login'],
                "trained" => (($user['trained']) ? "trained" : "not trained"),
                "account" => (($user['activated']) ? "enabled" : "disabled")];
           
            ?>
            <tr id="examiner_row_<?=$rowIndex?>" class="datarow">
              <td class="checkb">
                <input type="checkbox" name="examiner_check[]" id="examiner-check-<?=$rowIndex?>" class="check" value="<?=$userID?>"/>
              </td>
              <td class="w0 searchable"><?=$userID?></td>
              <td class="w0 searchable"><?=$user['forename']?></td>
              <td class="w0 searchable"><?=$user['surname']?></td>
              <td class="w0 searchable"><?=$user['email']?></td>
              <td class="searchable"><?=$user['contact_number']?></td>
              <td class="c">***************</td>
<?php
    $role = \OMIS\Auth\Role::loadID($user['user_role']);
?>
                <td title="<?=$role?>"><?=gettext($role->name)?></td>
<?php
                $depts = $db->departments->getExaminerDepartments($userID, $termFilter);
                if (mysqli_num_rows($depts) > 0) :
?>
                 <td class="c">
                 <ul class="depts">
  <?php          while ($dept = $db->fetch_row($depts)): 
                   $deptID = $dept['dept_id'];
                   $deptName = $dept['dept_name'];              
   ?>
                   <li title="<?=$deptName?>"><?=$deptID?></li>
                   
<?php             endwhile;
?>
                 </ul>
<?php            
                 else : 
?>
                  <td class="boldcentrred">NONE
<?php            
                 endif;
?>
                  </td>
                   <td class="last-login"><?=$lastLogin?></td><?php
                       
                    // Training Cell
                    if ($trainingStatus) {
                        if ($user['trained'] == 1) {
                           ?><td class="c green">Trained</td><?php 
                        } else {
                           ?><td class="c red">Not Trained</td><?php
                        }
                    }

                    // Account Access Cell
                    if ($user['activated'] == 1) {
                       ?><td class="c green">Enabled</td><?php 
                    } else {
                       ?><td class="c red">Disabled</td><?php 
                    }
            ?>
                    <td class="titleedit2">
                    <?php editButton('edit_' . $rowIndex, 'Edit', 'Edit')?>
                    </td>
             </tr>
             <?php
             }
         $overallIndex++;
     }
     
     // Base row
     if ($rowIndex > 0) {
     ?>
     <tr id="base-tr">
     <td colspan="<?=(count($fields) + 2)?>" class="base-button-options" id="base-button-td">
      <div class="fl">
      <select name="todo" id="todo" class="examiners-action custom-select custom-select-sm" disabled="disabled">
        
        <optgroup label="update users">
          <option value="edit" title="Edit accounts selected &amp; set common password">edit / update</option>
        </optgroup>
        <optgroup label="attach users to <?=gettext('term')?>">
        <?php

        /**
         * Allow user to attach accounts to a term/year
         */
          $terms = array_slice(array_reverse(
                  $db->academicterms->getAllTerms()
          ), 0, 3);

          foreach ($terms as $termID => $term) {

            // Current term exist
            if ($termFilter == $termID) {

              continue;

            }

            ?><option value="attachterm:<?=$termID?>"
                title="Attach to <?=gettext('term')?> '<?=$termID?>'"><?=gettext('term')?> <?=$termID?>
              </option><?php

          }

        ?>
        </optgroup>
        <optgroup label="set user access">
          <option value="ena" title="Enable accounts selected">enable account</option>
          <option value="dis" title="Disable accounts selected">disable account</option>
        </optgroup>
        <optgroup label="set user training">
          <option value="trained" title="Mark selected as trained">trained</option>
          <option value="notrained" title="Mark selected as not trained">not trained</option>
        </optgroup>
        <optgroup label="set role">
<?php
    foreach ($userRolesExaminer as $role):
        $roleTitle = "Change to ". gettext($role->name) . " Role: " . gettext($role->description);
?>
        <option value="role:<?=$role->role_id?>" title="<?=$roleTitle?>">change to '<?=strtolower(gettext($role->name))?>'</option>
<?php
    endforeach;
?>
        </optgroup>
        <optgroup label="export users">
          <option value="exportcsv" title="Export users to csv">csv for MS Excel</option>
          <option value="exportxls" title="Export users to xlsx">xlsx for MS Excel</option>
          <option value="exportpdf" title="Export users to PDF">pdf Document</option>
        </optgroup>
        <optgroup label="send message to users">
          <option value="sms" title="Send SMS">send SMS</option>
        </optgroup>
        <optgroup label="delete users from system">
           <option value="delete" title="Delete accounts selected from system" class="orange">delete</option>
        </optgroup>
      </select>
      <input type="submit" value="go" class="btn btn-outline-default btn-sm" name="go_examiner" id="go-examiner" disabled="disabled"/>
    </div>
    <div class="fr" id="add-div">
       
        <!-- Hidden Variables for 'SMS Examiners' option -->
        <input type="hidden" id="sms-mode" name="smode" value="send" disabled="disabled"/>
        <input type="hidden" id="sms-page" name="page" value="sms_examiners" disabled="disabled"/>

        <!-- More hidden variables -->
        <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
        <input type="hidden" value="<?=$rowIndex?>" id="count"/>
        <input type="hidden" value="<?=$orderIndex?>" id="order-index" name="order_index"/>
        <input type="hidden" value="<?=$orderType?>" id="order-type" name="order_type"/>
        <input type="hidden" value="<?=$pageNr?>" id="page-number"/>
        <input type="hidden" id="examiners-data" name="examiners_data" value="<?=base64_encode(gzcompress(serialize($exportData)))?>"/>
        
    </div>
    </td>
   </tr><?php
   }
  } else {
      
   // Message on how to add a new examiner
   $message = "To add a new user, click the 'add' button below";
  ?><tr id="first_examiner_row">
        <td colspan="<?=(count($fields) + 2)?>" class="mess">
          <div id="examiner_row_div" class="tablestatus2"><?=$message?></div>
        </td>
    </tr>
    <tr id="base-tr">
        <td colspan="<?=(count($fields) + 2)?>" class="base-button-options" id="base-button-td">
          <div class="fr" id="add-div">
            <input type="button" value="add +" name="add" id="add" class="btn btn-success btn-sm"/>
          </div>
        </td>
    </tr>
  <?php
   }
   ?>
  </table>     
 </form>
  <?php
  
  /* Render the pagination controls if there's more
   * than a page worth of users.
   */
  if ($userCount > $itemsPerPage) {
     $template->render($data);     
  }
  
  ?>
</div>
<?php
 /* Password strength settings for the password fields
  * Plugin: pwstrength bootstrap Jquery
  */
 if (isset($config->passwords)) {
?>
 <script>
  // pass PHP variable declared above to JavaScript variable
  var verdictList = <?=json_encode(['weak', 'normal', 'medium', 'strong', 'very strong'])?>;
  var passwordSettings = <?=json_encode($config->passwords)?>;
 </script>
<?php
 }
?>

