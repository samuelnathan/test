<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

#Critical Session Check
$session = new OMIS\Session;
$session->check();

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

#Initial Variables & POST Variables
$examiners = isset($_POST['examiner_check']) ? $_POST['examiner_check'] : array();
$send_mode = (isset($_REQUEST['smode']) && $_REQUEST['smode'] == 'send');
$sent = (isset($_GET['sent']) && $_GET['sent'] == 1);
$sms_message = '';

#Get Recipients if any
if (!$send_mode && $sent && isset($_GET['sms_id'])) {
    $message_details = $db->sms->getSMSMessage($_GET['sms_id']);
    $sms_message = $message_details['sms_message'];
    $examiners = $db->sms->getSMSMessageRecipients($_GET['sms_id']);
}
?><div class='contentdiv'><?php
 if($send_mode && count($examiners) == 0){
   ?><div class='errordiv'>No <?=ucwords(gettext('examiners'))?> were selected, please close this popup window and select <?=ucwords(gettext('examiners'))?> from the list</div><?php
 } else {
  ?><form id='sms-form' method='post' action='examiners/send_sms.php' onsubmit="return(ValidateForm())">
	<div id='sms-container'>
	<div class='sms-title'>Recipients</div>
	<div id='sms-examiners'><?php
	 	 
	 #Loop through examiners
	 foreach($examiners as $data){
	    
		#Examiner ID
		$examiner_id = ($send_mode) ? $data : $data['recipient_id'];
		
		#Send Mode
		if($send_mode){
		  $examiner_id = $data;
		} else {
		  $examiner_id = $data['recipient_id'];
		  $rec_sent = $data['sent'];
		}
		
		#Get Examiner Details
		$examiner_details = $db->users->getExaminer($examiner_id);
			
		?><div class='examiner-bubble <?php if(!$send_mode && $rec_sent){ ?>green-bg<?php } else if(!$send_mode && !$rec_sent){ ?>amber-bg<?php } ?>'>
		   <input type='hidden' class='examiner-id' name='sms_examiners[]' value="<?=$examiner_id; ?>">
		   <img src='assets/images/deletetiny.svg' alt='delete' /><?=$examiner_details['surname']; ?>, <?=$examiner_details['forename']; ?>
		  </div><?php
	 }
	
	?></div>
	<div class='sms-title'>SMS Message</div>
	<div id='sms-content'><textarea id='sms-message' name='sms_message' rows='4' cols='76' maxlength='160' <?php if(!$send_mode){ ?>class='read-only' readonly='readonly'<?php } ?>><?=$sms_message; ?></textarea></div>
	<div id='sms-base'><?php

		#Send failed message
		if(!$send_mode && !$sent){
           ?><div class='errordiv'>SMS messages failed to send, please try again</div><?php
        }
		  
		#Send mode, display required buttons
		if($send_mode){
		   ?><input type='button' id='sms-cancel' value='cancel' /><input type='submit' id='sms-send' name='sms_send' value='send' /><?php 
		} else {
		   if(isset($_GET['credits'])){ ?><label for='credits-left' id='credits-left-label'>Credits Left</label><input id='credits-left' type='text' value='<?=$_GET['credits']; ?>' class='read-only' readonly='readonly' /><?php } ?>
		     <input type='button' id='sms-cancel' value='close' /><?php
		}
		
	 ?></div>
    </div>
  </form><?php 
  }
 ?></div>
