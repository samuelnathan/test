<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 23/11/2015
 * © 2015 Qpercom Limited. All rights reserved.
 * @Send SMS Examiners
 */

// Library to parce and validate mobile numbers
use \libphonenumber\PhoneNumberUtil as PhoneNumberUtil;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Mobile Country Code
$mobile_country_code = (isset($config->sms['country'])) ? $config->sms['country'] : 'IE';

// SMS Library
$sms_library = $config->sms['library'];

// Mobile Username & Password
$username = $config->sms['username'];
$password = $config->sms['password'];

// Sent Yes/No
$sent = 0;

// Credits Left
$credits_left = 0;

// Library Switch
switch (strtolower($sms_library)) {
    case 'textmagic': // Case Text Magic
        // Initial Variables
        $is_unicode = true;
        $phones = [];

        // Protocol Connect / Authenticate
        $api = new TextMagicSMS\TextMagicAPI(['username' => $username, 'password' => $password]);

        // Credits Left
        $credits_left = $api->getBalance();

        // Enough Credits?
        if ($credits_left > 0) {
            // Loop through recipient records
            foreach ($recipients_records as &$recipient_row) {
                  
              // Remove any non numeric characters
              $mobileNum = preg_replace('/\D/', '', $recipient_row[2]);
              
              /**
               *  Parce mobile number while including the country ISO (config file)
               */
              $phoneUtil = PhoneNumberUtil::getInstance();
              try {
                $mobileNumObj = $phoneUtil->parse($mobileNum, $mobile_country_code);
              } catch (\libphonenumber\NumberParseException $e) {
                error_log($e);
              }

              // Valid mobile number?
              $mobileNumValid = $phoneUtil->isValidNumber($mobileNumObj);              
             
              // If it's a valid mobile then it add it to the send queue
              if ($mobileNumValid) {
                 $phones[] = $mobileNum;
                 $recipient_row[3] = 1;
              }
              
            }

            // Send message to recipients
            if (count($phones) > 0) {
                $sent = 1;
                $api->send($sms_message, $phones, $is_unicode);
            }
        }

        // Credits Left
        $credits_left = $api->getBalance();

        break;
        
    // Case Bulk Text
    case 'bulktext': 
    case 'mymobileapi':
      
        $senderID = isset($config->localization['skin']['app_name']) ? 
                $config->localization['skin']['app_name'] : "Qpercom";
      
        $api = new \OMIS\APIs\MyMobileAPI($username, $password, $senderID);
        
        $credits_left = $api->checkCredits();
        
        // Count how many valid-looking phone numbers we have...
        foreach ($recipients_records as &$recipient) {

            // Remove any non numeric characters
            $mobileNum = preg_replace('/\D/', '', $recipient[2]);

            /**
             *  Parce mobile number while including the country ISO (config file)
             */
            $phoneUtil = PhoneNumberUtil::getInstance();
            try {
                $mobileNumObj = $phoneUtil->parse($mobileNum, $mobile_country_code);
            } catch (\libphonenumber\NumberParseException $e) {
                error_log($e);
            }

            // Valid mobile number?
            $mobileNumValid = $phoneUtil->isValidNumber($mobileNumObj);

            // If it's a valid mobile then it add it to the send queue
            if ($mobileNumValid) {
                $phones[] = $mobileNum;
                $recipient[3] = 1;
            }
            
        }

        // Send SMS messages
        $api->sendSms(implode(',', $phones), $sms_message);
        
        $sent = 1;
        
        $credits_left = $api->checkCredits();
        break;
    default:
        throw new \InvalidArgumentException("Unknown SMS API/Service '$sms_library'");
}
