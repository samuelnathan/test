<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Auth\Role as Role;

/*
 *  Output Department list
 */
function renderDepartments($type, $forDept, $examinerID, $rowCount = 1, $preSelectDepts  = null)
{
    // Get DB Instance
    $db = \OMIS\Database\CoreDB::getInstance();
    
    // Get a complete list of available departments.
    if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {
        
        $adminSchools = $db->schools->getAdminSchools($_SESSION['user_identifier']);
        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
        $depts = $db->departments->getDepartments(null, true, $schoolIDs);

    } else {
        $depts = $db->departments->getAllDepartments(true);
    }

    // If the user is a super admin or an admin, let them choose from all departments. If not,
    $filterPermittedDepartments = (!in_array(
        $_SESSION['user_role'],
        [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN, Role::USER_ROLE_SCHOOL_ADMIN]
    )); 
    
   /* 
    * What academic term ID to use. if it's filtered in the dropdown as 'Any'
    * then use the system term
    */
    $inAllTerms = false;
    
    // If no academic term is selected then use the system term     
    if (!isset($_SESSION['temp_term']) || strlen($_SESSION['temp_term']) == 0) {
       $inAllTerms = true;
       $inTerm = $_SESSION['cterm'];
    // Or else use the selected term
    } else {
       $inTerm = $_SESSION['temp_term'];        
    }
   
    /* If we need to filter by departments, we need to enable only those departments
     * that the current user can assign. Any other assignments should be greyed
     * out. Should also put a tool tip or something on those disabled ones to
     * say why the user can't select those departments.
     */
    $permittedDeptIDs = [];
    if ($filterPermittedDepartments) {
        $examinerDepts = $db->departments->getExaminerDepartmentIDs(
            $_SESSION['user_identifier'],
            $inTerm
        );
        $permittedDepts = $db->departments->getDepartments($examinerDepts);
        while ($department = $db->fetch_row($permittedDepts)) {
            $permittedDeptIDs[] = $department['dept_id'];
        }
        
        // Shouldn't need this any more...
        unset($permittedDepts);
    } else {
        $permittedDeptIDs = array_column($depts, 'dept_id');
    }
    
    /* Irrespective of the user types, we now have a list of departments to
     * enable to allow the user to specify which departments a user is assigned
     * to. Let's iterate through these and get the ids, as really that's all we need...
     */
    $currentDepts = [];
    $attachDepts = [];
    
    // If new examiner mode
    if ($type == 'add') {
        // Current Department
        if (!is_null($forDept) && (trim($forDept) !== '')) {
            $currentDepts[$forDept] = $db->departments->getDepartment($forDept, true);
        } else {
            $forDept = null;
        }
        
        // Departments to attach
        foreach ($depts as $eachDept) {
            if (is_null($forDept) || ($forDept != $eachDept['dept_id'])) {
                $attachDepts[$eachDept['dept_id']] = $eachDept;
            }
        }
    
    // If edit or multi row edit examiner section
    } elseif (in_array($type, ['edit', 'xedit'])) {
      
        // Department pre selection / edit validation section
        if (is_array($preSelectDepts)) {
            $examinerDepts = $preSelectDepts;
        } else {
            // Other
            $examinerDepts = $db->departments->getExaminerDepartmentIDs(
                    $examinerID,
                    ($inAllTerms) ? NULL : $inTerm
            );            
        }

        // Loop through departments
        foreach ($depts as $eachDept) {
            if (in_array($eachDept['dept_id'], $examinerDepts, true)) {
                // Current Departments
                $currentDepts[$eachDept['dept_id']] = $eachDept;
            } else { // Departments To Attach
                $attachDepts[$eachDept['dept_id']] = $eachDept;
            }
        }
    }
    ?>
    <ul class="editdepts">
    <?php
    
    $deptCount = 0;
    foreach ([$currentDepts, $attachDepts] as $depts) {
        $inputChecked = ($depts == $currentDepts) ? ' checked="checked"' : '';
        
        foreach ($depts as $deptID => $deptRecord) {
            $elementID = $rowCount.$deptCount;
            $elementNameFormat = 'name="records[%s][depts][]"';
            if ($type == 'xedit') {
                $elementName = sprintf($elementNameFormat, $examinerID);
            } else {
                $elementName = sprintf($elementNameFormat, '');
            }
            $elementTitle = sprintf('%s [%s]', $deptRecord['dept_name'], $deptRecord['school_description']);
            
            /* Check if the currently logged in user is allowed assign users to
             * the department being rendered so that we can enable or disable
             * the checkbox as appropriate
             */
            if (in_array($deptID, $permittedDeptIDs)) {
                $disabledProperty = "";
            } else {
                $disabledProperty = ' disabled="disabled" title="You can\'t assign users"';
            }
            ?>
            <li>
              <input type="checkbox" value="<?=$deptID?>" <?=$elementName?> id="depts-<?=$elementID?>" 
                     class="depts-cbs"<?=$inputChecked?><?=$disabledProperty?>/>
              <label for="depts-<?=$elementID?>" title="<?=$elementTitle?>"><?=$deptID?></label>
            </li>
            <?php
            $deptCount++;
        }
    }
    ?>
    </ul>
        <?php
}

/**
 * Render filter fields
 * 
 * @param array $urlArgs   url arguments
 * @return array           selected department name
 */
function renderFilterFields($urlArgs)
{
    
   // Url Arguments
   list($selectedDept,
        $selectedTerm,
        $selectedRole,
        $selectedAccount,
        $selectedForenames,
        $selectedSurnames,
        $search) = $urlArgs;
   
    $userRolesExaminer = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, false);

    // Get DB Instance
    $db = \OMIS\Database\CoreDB::getInstance();

    // Get department data
    if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
        
        $deptRecords = $db->departments->getAllDepartments(true);

    }  else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {

        $adminSchools = $db->schools->getAdminSchools($_SESSION['user_identifier']);
        $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
        $deptRecords = $db->departments->getDepartments(null, true, $schoolIDs);    

    } else {

        $deptRecords = $db->departments->getExaminerDepartments(
            $_SESSION['user_identifier'], null, true
        );
    }

    // Get all academic terms
    $terms = $db->academicterms->getAllTerms();
    $selectedDeptName = "";

    // Group by school
    $schools = OMIS\Database\CoreDB::group(
        $deptRecords,
        ['school_description'],
        true
    );

    ?>
<script>
    jQuery(function() {
        jQuery('#dept').chosen();
    });
</script>
<div class="card d-inline-block align-top mr-2 mb-1 options">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Manage <?=gettext('Exam-Team-Placeholder') . gettext(' Team') ?>
        </span>
      </div>
    </div>
  </div>
  <div class="card-body pb-0">
    <div class="card-text">
      <div class="f12">Filter list using the follow criteria: </div>
 <br/>
 
 <div class="row">
     <div class="col-12 col-sm-6">
 <ul class="filter filter-col-item">
 <li>
 <label for="dept"><?=gettext('Department')?></label>
 <select id="dept" class="w-auto custom-select custom-select-sm">
     
 <?php 
   
  /**
   * Only display 'Any' option if user is super admin or system admin
   */
  if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) { 
     ?><option value="" class="black">Any</option><?php
  }
  
  // Render schools department options 
  foreach ($schools as $schoolName => $school) :
    ?><optgroup label="<?=ucwords($schoolName)?>"><?php
    foreach ($school as $dept) :
       $deptID = $dept['dept_id'];
       $deptName = $dept['dept_name'];
        
       if ($selectedDept == $deptID) {
          $selectedDeptName = $deptName;
          $selected = ' selected="selected"';
        } else {
          $selected = '';
        }
        ?>
        <option value="<?=$deptID?>"<?=$selected?>>
          <?=$deptName?> [<?=$deptID?>]
        </option>
        <?php
      endforeach;
      ?></optgroup><?php
    endforeach;
    ?>
  </select>
  </li>
  <li>
      
  <label for="term"><?=gettext('Term')?> </label>
  <select id="term" class="w-auto custom-select custom-select-sm">
   <option value="" class="black">Any</option>
    <?php
    
    // Render term options 
    foreach ($terms as $term) :
      $termID = $term['term_id'];
      $termDesc = $term['term_desc'];
      $selected = ($selectedTerm == $termID) ? ' selected="selected"' : '';
      ?>
       <option value="<?=$termID?>" title="<?=$termDesc?>"<?=$selected?>><?=$termID?></option>
      <?php
    endforeach;
    ?>
   </select>
  </li>
  
  <li>
  <label for="role">User Role </label>
  <select id="role" class="w-auto custom-select custom-select-sm">
  <option value="" class="black">Any</option>
  <?php
    
  // Render user roles
  foreach ($userRolesExaminer->getRoles() as $role) :
    $selected = ($selectedRole == $role->role_id) ? ' selected="selected"' : '';
    ?>
    <option value="<?=$role->role_id?>"<?=$selected?> title="<?=gettext($role->name)?> Role: <?=gettext($role->description)?>">
        <?=gettext($role->name)?>
    </option>
  <?php
  endforeach;
  ?>
  </select>
  </li>
  <li>
    <?php
     $title = "Identifier, Surname, Forenames, email";
     $placeholder = "Records containing...";
    ?>
    <input id="search" class="form-control form-control-sm" title="<?=$title?>" type="text" size="28" placeholder="<?=$placeholder?>" value="<?=$search?>"/>
  </li>
 </ul>
 </div>
 <?php 
 
 /**
  * Render account filtering (all, enabled, disabled)
  */
 
 $enabledSelected = ($selectedAccount == "1") ? ' selected="selected"' : '';
 $disabledSelected = ($selectedAccount == "0") ? ' selected="selected"' : '';
 
 ?>
 <div class="col-12 col-sm-6">
 <ul class="filter filter-col-item">
 <li>
 <label for="account">Account </label>
 <select id="account" class="w-auto custom-select custom-select-sm">
  <option value="" class="black">Any</option>
  <option value="1"<?=$enabledSelected?>>Enabled</option>
  <option value="0"<?=$disabledSelected?>>Disabled</option>
 </select>
 </li>
 
 <?php 
 
 
 /**
  * Render Forenames (A-Z)
  */
 
 // Form alphabet array
 $filterAlphabet = range('A', 'Z');
 
 ?>
 
 <li>
 <label for="forenames">Forenames (A-Z) </label>
 <select id="forenames" class="w-auto custom-select custom-select-sm">
  <option value="" class="black">Any</option>
 
  <?php 
  
  foreach ($filterAlphabet as $letter) {
   
   $selected = ($letter == strtoupper($selectedForenames)) ? ' selected="selected"' : '';  
   ?>
   <option value="<?=$letter?>"<?=$selected?>><?=$letter?></option>
   <?php
  }
  
  ?>
  
 </select>
 </li>
 <?php
 

 /**
  * Render Surnames (A-Z)
  */

 ?>
 <li>
 <label for="surnames">Surnames (A-Z) </label>
 <select id="surnames" class="w-auto custom-select custom-select-sm">
  <option value="" class="black">Any</option>
  <?php 
  
  foreach ($filterAlphabet as $letter) {
   $selected = ($letter == strtoupper($selectedSurnames)) ? ' selected="selected"' : '';
   ?>
   <option value="<?=$letter?>"<?=$selected?>><?=$letter?></option>
   <?php
  }
  
  ?>
 </select>
 </li>
 <li class="tar">
   <input type="button" id="filter-list" value="filter list" class="btn btn-info btn-sm"/>  
 </li>
 </ul>
 </div>
 </div>
    </div>   
  </div> 
</div> 
    
  <?php
  
  // Return department name
  return ['dept_name' => $selectedDeptName];
}

/**
 * Examiner list filtering
 * @return array filtered list
 */
function examinerFiltering()
{

    // Defaults
    $db = \OMIS\Database\CoreDB::getInstance();
    $filterReady = false;
    $selectedDept = "";
    $selectedTerm = "";
    $selectedRole = "";
    $selectedAccount = "";
    $selectedForenames = "";
    $selectedSurnames = "";
    $search = "";
    
    /**
     * Department Checks
     */
    $deptParam = filter_input(INPUT_GET, 'dept', FILTER_SANITIZE_STRING);
    
    if (!is_null($deptParam)) {
      $selectedDept = $deptParam;
      if (strlen($selectedDept) > 0) {
        $_SESSION['selected_dept'] = $deptParam;
      }
    } elseif (isset($_SESSION['selected_dept'])) {
      $selectedDept = $_SESSION['selected_dept'];
    }

    /**
     * Academic Term Checks
     */
    $termParam = filter_input(INPUT_GET, 'term', FILTER_SANITIZE_STRING);
    
    if (!is_null($termParam)) {
        $selectedTerm = $termParam;
        $_SESSION['temp_term'] = $selectedTerm;
    // Current academic term selected
    } elseif (isset($_SESSION['temp_term'])) {
        $selectedTerm = $_SESSION['temp_term'];
    // System academic term
    } elseif (isset($_SESSION['cterm'])) {
        $selectedTerm = $_SESSION['cterm'];
        $_SESSION['temp_term'] = $selectedTerm;
    }

    /**
     * Examiner role checks
     */
    $roleParam = filter_input(INPUT_GET, 'role', FILTER_SANITIZE_NUMBER_INT);
    
    if (!is_null($roleParam)) {
        $selectedRole = $roleParam;
        $_SESSION['selected_role'] = $roleParam;
    } elseif (isset($_SESSION['selected_role'])) {
        $selectedRole = $_SESSION['selected_role'];
    }
    
    
    /**
     * Regular expression for account parameter (enabled = 1, disabled = 0)
     * Also accepts empty strings
     */
    $regExpAccount = "/^[01]?$/";
    
    
    /**
     * Examiner account check (all = empty, enabled = 1, disabled = 0)
     */
    $accountParam = filter_input(INPUT_GET, 'account', FILTER_SANITIZE_NUMBER_INT); 
    
    if (!is_null($accountParam) && preg_match($regExpAccount, $accountParam)) {
        $selectedAccount = $accountParam;
        $_SESSION['selected_account'] = $accountParam;
    } elseif (isset($_SESSION['selected_account'])) {
        $selectedAccount = $_SESSION['selected_account'];
    }
    
    /**
     * Regular expression for forenames (A-Z) and surnames (A-Z) parameters
     * Also accepts empty strings
     */
    $regExpNames = "/^[A-Z]?$/";
    
    /**
     * Forenames (A-Z)
     */
    $forenamesParam = filter_input(INPUT_GET, 'forenames', FILTER_SANITIZE_STRING);
      
    if (!is_null($forenamesParam) && preg_match($regExpNames, $forenamesParam)) {
        $selectedForenames = $forenamesParam;
        $_SESSION['selected_forenames'] = $forenamesParam;
    } elseif (isset($_SESSION['selected_forenames'])) {
        $selectedForenames = $_SESSION['selected_forenames'];
    }
    
    /**
     * Surnames (A-Z)
     */
    $surnamesParam = filter_input(INPUT_GET, 'surnames', FILTER_SANITIZE_STRING);
      
    if (!is_null($surnamesParam) && preg_match($regExpNames, $surnamesParam)) {
        $selectedSurnames = $surnamesParam;
        $_SESSION['selected_surnames'] = $surnamesParam;
    } elseif (isset($_SESSION['selected_surnames'])) {
        $selectedSurnames = $_SESSION['selected_surnames'];
    }
    
    /**
     *  Search data
     */    
    $searchParam = filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING);
    if (!is_null($searchParam)) {
      $search = $searchParam; 
    } else {
      $search = "";
    }
    
    /**
     *  Final Checks & User Preference
     */
   
    // Valid department and academic term?
    $validDept = ($selectedDept == '' || $db->departments->existsById($selectedDept));
    $validTerm = ($selectedTerm == '' || $db->academicterms->existsById($selectedTerm));
    if ($validDept && $validTerm) {

      $db->userPresets->update(['filter_department' => $selectedDept]);
      $filterReady = true;

    }
       
    // Return filtered results
    $filterResults = [];
    $filterResults[] = $selectedDept;
    $filterResults[] = $selectedTerm;
    $filterResults[] = $selectedRole;
    $filterResults[] = $selectedAccount;
    $filterResults[] = $selectedForenames;
    $filterResults[] = $selectedSurnames;
    $filterResults[] = $search;
    $filterResults[] = $filterReady;
    
    return $filterResults;

}

/**
 * Removes roles from a list of roles based on their role_ids.
 * 
 * @param \OMIS\Auth\Role[] $roleList       List of role objects.
 * @param int[]             $rolesToRemove List of unwanted role IDs.
 * @return \OMIS\Auth\Role[] List of roles minus the unwanted ones.
 * @throws \InvalidArgumentException
 */
function filterAvailableRoles($roleList, $rolesToRemove) {
    // If the list of roles to remove
    if (empty($rolesToRemove)) {
        return $roleList;
    }
    
    // Promote $roleList to an array if needs be.
    if (!is_array($roleList)) {
        $roleList = [$roleList];
    }
    
    // Ditto for $rolesToRemove.
    if (!is_array($rolesToRemove)) {
        $rolesToRemove = [$rolesToRemove];
    }
    
    // Check that the provided list of roles is actually a list of roles...
    $validRoles = array_filter($roleList, "\OMIS\Auth\Role::isRole");
    if (count($validRoles) < count($roleList)) {
        throw new \InvalidArgumentException(__FILE__. ":" . __METHOD__
            . ": Provided role list has one or more bad items");
    }
    
    $validRoleIDs = array_filter($rolesToRemove, "is_int");
    if (count($validRoleIDs) < count($rolesToRemove)) {
        throw new \InvalidArgumentException(__FILE__. ":" . __METHOD__
            . ": Role id list has one or more bad items");
    }
    
    // Okay, do the necessary.
    /* Count backwards to avoid disrupting the item order when iterating. If we
     * count forwards, we're collapsing the array gradually every time we delete
     * an entry so we need to test each index repeatedly until it fails the test
     * - this way, each index only needs to get tested once.
     */
    for ($i = count($roleList) - 1; $i >= 0; $i--) {
        // If this role is on the list, remove it.
        if (in_array($roleList[$i]->role_id, $rolesToRemove)) {
            unset($roleList[$i]);
        }
    }
    
    return $roleList;
}
