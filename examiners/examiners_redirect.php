<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 define('_OMIS', 1);

 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
     $template = new \OMIS\Template("noaccess.html.twig");
     $data = [
         'logged_in' => true,
         'root_url' => $config->base_path_url,
         'username' => $_SESSION['user_identifier'],
         'page' => \OMIS\Utilities\Request::getPageID()
     ];
     $template->render($data);
     return;
 }

 // Examiner Management Redirect
 if (isset($_POST['go_examiner']) && isset($_POST['examiner_check'])) {

     $orderIndex = iS($_POST, "order_index");
     $orderType = iS($_POST, "order_type");

     $todo = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING);
     $source = filter_input(INPUT_POST, 'source', FILTER_SANITIZE_STRING);
     $examiners = filter_input(INPUT_POST, 'examiner_check', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

     if ($todo == 'edit') {

         //Redirect to Edit/Update Section
         $_SESSION['examiners_edit'] = $examiners;
         header('Location: ../manage.php?page=edit_examiners'
             . '&order_index=' . $orderIndex
             . '&order_type=' . $orderType
             . '&source=' . $source
         );

     } elseif ($todo == 'exportcsv' || $todo == 'exportxls') {

         //Export Examiners csv or xls [Added 08/04/2013]
         include '../export/export_examiner_list.php';

     } elseif ($todo == 'exportpdf') {

         //Export Examiners to PDF
         include '../pdfgen/pdf_examiners.php';

     /**
      * Add selected term, prefix = attachterm: (11 characters)
      */
     } elseif (substr($_POST['todo'], 0, 11) == "attachterm:") {

         $selectedTerm = substr(
             trim(filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING)), 11
         );

         // Attach to current term
         if ($db->academicterms->existsById($selectedTerm)) {

             $db->users->addExaminersToTerm($examiners, $selectedTerm);

         }

         header('Location: ../manage.php?page=manage_examiners&term=' . $selectedTerm);

     } else {

         //Delete Examiners
         if ($todo == "delete") {
             $db->users->deleteExaminers($examiners);
             //Set Examiner Enabled / Disabled
         } elseif ($todo == 'ena' || $todo == 'dis') {
             $db->users->enableDisableExaminers($examiners, $todo);
             //Set Examiner / Examiner Admin Level
         } elseif (preg_match('/^role:[0-9]+$/', $todo) === 1) {
             $roleID = array_pop(explode(":", $todo));
             $db->users->changeUserRoleExaminers($examiners, $roleID);
             //Set Examiner Trained / Not Trained
         } elseif ($todo == 'trained' || $todo == 'notrained') {
             $db->users->trainedExaminers($examiners, $todo);
         }

         /* If we're doing something like enabling, disabling or deleting to a
          * list of users that includes the user doing the changing, make sure
          * they get warned first!
          */
         $me = $_SESSION['user_identifier'];
         if (in_array($todo, array('exa', 'delete', 'dis'), true) && in_array($me, $examiners, true)) {
             $_SESSION['editing_warn'] = true;
         }

         //Redirect to manage examiners section
         header('Location: ../manage.php?page=manage_examiners&order_index='
                 . $orderIndex . '&order_type=' . $orderType);
     }

 /*
  *  Examiner list edit/update section
  */
 } elseif (isset($_POST['update_multiple_examiners'])) {

     // Submit update
     if ($_POST['update_multiple_examiners'] == 'update' && isset($_POST['records'])) {

        // Need revalidation default
        $needRevalidation = false;

        // Does term exist ?
        if ($db->academicterms->existsById($_SESSION['cterm'])) {

           foreach ($_POST['records'] as $originalID => $record) {
              $examinerID = trim($record['id']); // New Examiner ID
              $originalID = trim($originalID);   // Original Examiner ID

              // Does user (examiner) exist
              if ($db->users->doesUserExist($originalID)) {

                 // Check for new ID is already in use
                 if (identifierValid($examinerID) && !($examinerID != $originalID && $db->users->doesUserExist($examinerID))) {

                     // Request to change password for this record
                     $passwordChange = (isset($record['change']) ? 1 : 0);

                     // Update examiner in database
                     $examinerUpdated = $db->users->updateExaminer(
                         $examinerID,                   // New Examiner ID
                         $originalID,                   // Original Examiner ID
                         $record['forenames'],          // Forenames
                         $record['surname'],            // Surname
                         $record['email'],              // Email
                         $record['telephone'],          // Contact number
                         iS($record, 'password'),       // Password
                         $record['role'],               // Level (role)
                         $record['enabled'],            // Account enabled/disabled
                         $record['trained'],            // Trained yes/no
                         $passwordChange,               // Password change
                         $record['depts'],              // Departments
                         $_SESSION['cterm']             // Academic term
                     );

                     // Examiner updated
                     if ($examinerUpdated == true) {

                       // If user is examiner set edited warning flag and update user details
                       if ($_SESSION['user_identifier'] == $originalID) {
                          $_SESSION['editing_warn'] = true;
                          $_SESSION['user_forename'] = $record['forenames'];
                          $_SESSION['user_surname'] = $record['surname'];
                          $_SESSION['user_email'] = $record['email'];
                          $currentUser = true;
                       } else {
                          $currentUser = false;
                       }

                       // If new ID is not the same as old ID
                       if ($originalID != $examinerID) {
                          if ($currentUser) {
                            $_SESSION['user_identifier'] = $examinerID;
                          }
                            $db->users->updateExaminerID($examinerID, $originalID);
                            $db->forms->changeAuthorID($examinerID, $originalID);
                       }
                    }
                 } else {

                     // Need revalidation default
                     $needRevalidation = true;

                     // Set if not set already
                     if (!isset($_SESSION['examiners_need_validation'])) {
                        $_SESSION['examiners_need_validation'] = [];
                     }

                     // Password change?
                     $passwordChange = (isset($record['change']) ? true : false);

                     // Pass in examiner array
                     $_SESSION['examiners_need_validation'][] = [
                           'new_id' => $examinerID,               // New Examiner ID
                           'org_id' => $originalID,               // Original Examiner ID
                           'forename' => $record['forenames'],       // Forenames
                           'surname' => $record['surname'],         // Surname
                           'email' => $record['email'],           // Email
                           'contact_number' => $record['telephone'], // Contact number
                           'change_pass' => $passwordChange,      // Password
                           'user_role' => $record['role'],        // Level (role)
                           'activated' => $record['enabled'],    // Account enabled/disabled
                           'trained' => $record['trained'],       // Trained yes/no
                           'depts' => $record['depts']            // Departments
                     ];
                 }
              }
           }
        }

        // Need revalidation?
        if ($needRevalidation) {
           header('Location: ../manage.php?page=edit_examiners&revalidate=1');
        } else {
           header('Location: ../manage.php?page=manage_examiners');
        }
     }

 // If none of the above return to index page
 } else {
     header('Location: ../');
 }
