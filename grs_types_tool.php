<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 31/10/2014
 * © 2014 Qpercom Limited. All rights reserved.
   Manage Global Rating Scale Types
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

//Use Role Auth Class
use \OMIS\Auth\Role as Role;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
$user_role = Role::loadID($_SESSION['user_role']);
if (!$user_role->canAccess()) {
    return false;
}

$sectionTitle = "Manage global rating scale types";        
$sectionDescription = "Add, edit and remove Global Rating Scale types for assessment forms<br/>" .
                      "Click the 'Scale Type ID' links to manage the GRS values"; 
$recordListTitles = ['Scale Type ID', 'Scale Type Name', 'Scale Type Colour'];


/* 
 * Two types of fields [text, link]
 * Text: field is displayed as plain text
 * Link: field value is displayed as a URL link to another (sub) section
 */
$field1 = ['type' => 'link']; 
$field2 = ['type' => 'text'];
$field3 = ['type' => 'text'];
$fieldTypes = [$field1, $field2, $field3];

// Pass sub page to template (only one in this case)
$subPages = ['grs_values_tool'];

// Get GRS Types from database
$grsTypesDB = $db->forms->getRatingScaleTypes();
$grsTypes = array();

// Alter array for template. Make 'scale_type_id' field index
foreach ($grsTypesDB as $eachTypeRow) {
    $grsTypes[$eachTypeRow['scale_type_id']] = $eachTypeRow;
}        

$editMessage = "Edit Global Rating Scale Type";
$listEmptyMessage = "No Global Rating Scale Types found, " .
                    "please click the 'add' button to<br/>add " .
                    "a new Global Rating Scale Type.";
        
// Load and render the section description template
$sectionDescriptionData = ["title" => $sectionTitle,
                           "description" => $sectionDescription,
                           "section_text" => [],
                           "return_button" => false];        
        
$sectionDescriptionTemplate = new OMIS\Template('section_top_description.html.twig');
$sectionDescriptionTemplate->render($sectionDescriptionData);

// GRS Management feature enabled? Then allow user access to feature controls
$grsManagementFeature = $db->features->enabled('grs-management', $_SESSION['user_role']);
if ($grsManagementFeature) {
   // Load and render the records list template
   $recordListData = ["titles" => $recordListTitles,
                      "records" => $grsTypes,
                      "field_types" => $fieldTypes,
                      "sub_pages" => $subPages,
                      "additional_hidden_fields" => [],
                      "edit_message" => $editMessage,
                      "list_empty_message" => $listEmptyMessage];

  $recordListTemplate = new OMIS\Template('records_list.html.twig');
  $recordListTemplate->render($recordListData);

// Appropriate error message
} else {
   $message = "You do not have access to this feature.<br/>" .
              "Please contact your administrator for further assistance";
   
   $returnUrl = "manage.php?page=admin_tool";
   
   $templateData = ['message' => $message,
                    'return_url' => $returnUrl,
                    'content_wrapper' => true]; 
   $errorTemplate = new OMIS\Template('error.html.twig');
   $errorTemplate->render($templateData);
}
 