<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * @Poll feedback progress
 */

define('_OMIS', 1);

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Utilities\JSON as JSON;

include_once __DIR__ . '/../vendor/autoload.php';

$examID = filter_input(INPUT_GET, 'exam', FILTER_SANITIZE_NUMBER_INT);
$return = [];

// Exam ID is not empty then include necessary classes
if (!empty($examID)) {

    require_once __DIR__ . '/../extra/essentials.php';
    
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
       http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
       exit();
       
    }
    
} else {
  
       http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
       die ("Not authorized to view this section");
       
}

$feedbackStudents = $db->feedback->getStudentFeedbackRecords($examID);

while ($each = $db->fetch_row($feedbackStudents)) {
    
    if (!$each['is_ready'] || is_null($each['time_created']) || $each['is_sent'] == 0) {
       continue;
    }
        
    $return[] = [
       'id' => $each['student_id'],
       'time_sent' => $each['time_sent'],
       'is_sent' => $each['is_sent']
    ];
    
}

try {
    $json = JSON::encode($return);
} catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($return, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit;
}

echo $json;
