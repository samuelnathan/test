<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * @JSON Tools Section
 */
define('_OMIS', 1);

if (isset($_POST['isfor'])) {
  
    include __DIR__ . '/../extra/essentials.php';
    
    // Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
    
} else {
  
    include __DIR__ . '/../extra/noaccess.php';
    
}

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Utilities\JSON as JSON;

// Is for (mode)
$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

// json_encode takes care of Html Entities
$db->set_Convert_Html_Entities(false); 


/*
 * Delete individual feedback record generated (not sent)
 */
if ($isfor == 'delete_record' && isset($_POST['exam']) && isset($_POST['student'])) {
    if ($db->exams->doesExamExist($_POST['exam']) && $db->students->doesStudentExist($_POST['student'])) {
        // Delete feedback file if it already exists
        if (isset($_SESSION['base'])) {
            $fileName = $_POST['student'] . '_' . $_POST['exam'] . '.pdf';
            $filePath = \OMIS\Utilities\Path::join($config->tmp_path, 'feedback', $fileName);
            if (file_exists($filePath) === true) {
                unlink($filePath);
            }
        }
        echo $db->feedback->deleteFeedbackRecords($_POST['exam'], $_POST['student']);
    }
} else {
    $returnData = [];

    // Exam SESSIONS
    if ($isfor == 'swap_sessions') {
        $examID = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
        $sessions = [];
        if ($db->exams->doesExamExist($examID)) {
            $sessions = $db->exams->getExamSessions($examID);
            while ($session = $db->fetch_row($sessions)) {
                $returnData[] = [
                    'session_id' => $session['session_id'],
                    'session_description' => $session['session_description'],
                    'session_date' => formatDate($session['session_date'])
                ];
            }
        }
        
    // Get students and stations feedback section
    } elseif ($isfor == 'stations_students') {
        
        // Exam ID (required)
        $examIDParam = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
        
        // Session ID (not required)
        $sessionIDParam = filter_input(INPUT_POST, 'session', FILTER_SANITIZE_NUMBER_INT);
        
        // Return data template
        $returnData = [
            0 => [],
            1 => []
        ];

        // Does exam exist
        if ($db->exams->doesExamExist($examIDParam)) {
            
            // Session ID check
            $sessionID = $db->exams->doesSessionExist($sessionIDParam) ? $sessionIDParam : null;
            
            // Gather list of station numbers
            $stationNumbers = $db->exams->getUniqueStationNumbers(
                    $examIDParam,
                    $sessionID
            );
            
            foreach ($stationNumbers as $number) {
                $returnData[0][] = [
                    'station_number' => $number
                ];
            }

            // Get student feedback records
            $students = $db->feedback->getStudentFeedbackRecords(
                    $examIDParam,
                    $sessionID
            );
            
            // Gather list of students
            while ($student = $db->fetch_row($students)) {
                $returnData[1][] = [
                   'session_id' => $student['session_id'],
                   'session_date' => formatDate($student['session_date']),
                   'session_name' => ucfirst($student['session_description']),   
                   'group_id' => $student['group_id'],
                   'group_name' => $student['group_name'],                  
                   'student_id' => $student['student_id'],
                   'surname' => $student['surname'],
                   'forename' => $student['forename'],
                   'email' => $student['email']
                ];
            }
            
        }
    
    // Get sessions linked to an exam 
    } if ($isfor == 'get_exam_sessions') {
      
       $examID = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
       $returnData = [];
       
       if ($db->exams->doesExamExist($examID)) {
         
            $sessions = $db->exams->getExamSessions($examID, true);
           
            foreach ($sessions as $session) {
              
                $returnData[] = [
                    'id' => $session['session_id'],
                    'name' => $session['session_description'] .' - '. formatDate($session['session_date'])
                ];
                
            }
            
       }
       
    // Get feedback status (number of pdf files generated)
    } elseif ($isfor == 'feedback_count') {
      
        $examID = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
        $count = 0;

        if ($db->exams->doesExamExist($examID)) {
            // Get feedback created count
            $count = $db->feedback->getFeedbackCreatedCount($examID);
        }

        $returnData[] = ['complete' => $count];
        
    } elseif ($isfor == 'swap_groups_stations') {
        // Session groups
        $session_id = filter_input(INPUT_POST, 'session', FILTER_SANITIZE_NUMBER_INT);

        if ($db->exams->doesSessionExist($session_id)) {
            
            // Get groups
            $groups = $db->exams->getSessionGroups($session_id);
            while ($group = $db->fetch_row($groups)) {
                $returnData['groups'][] = [
                    'id' => $group['group_id'],
                    'name' => $group['group_name']
                ];
            }
     
            // Get stations
            $stations = $db->exams->getStations([$session_id]);
            while ($station = $db->fetch_row($stations)) {
                $returnData['stations'][] = [
                    'id' => $station['station_id'],
                    'number' => $station['station_number'],
                    'name' => $station['form_name']
                ];
            }
            
        }
       
    } elseif ($isfor == 'swap_students') {
        #GROUP STUDENTS
        $groupID = filter_input(INPUT_POST, 'group', FILTER_SANITIZE_NUMBER_INT);
        $students = [];

        if ($db->groups->doGroupsExist([$groupID])) {
            $students = $db->exams->getStudents([$groupID], null, false);

            while ($student = $db->fetch_row($students)) {
                $returnData[] = [
                    'student_id' => $student['student_id'],
                    'surname' => $student['surname'],
                    'forename' => $student['forename']
                ];
            }
        }
    } elseif ($isfor == 'swap_years') {
        #COURSE YEARS
        $courseID = filter_input(INPUT_POST, 'course_id', FILTER_SANITIZE_STRIPPED);
        $courseYears = [];

        if ($db->courses->doesCourseExist($courseID)) {
            $courseYears = $db->courses->getCourseYears($courseID, $_SESSION['cterm'], false);
            while ($year = $db->fetch_row($courseYears)) {
                $returnData[] = [
                    'year_id' => $year['year_id'],
                    'year_name' => $year['year_name']
                ];
            }
        }
    } elseif ($isfor == 'swap_course_students') {
        #YEAR STUDENTS
        $courseYearID = filter_input(INPUT_POST, 'course_year_id', FILTER_SANITIZE_NUMBER_INT);
        $students = [];

        if ($db->courses->doesCourseYearExist($courseYearID, null, null, $_SESSION['cterm'])) {
            $students = $db->students->getStudentsInYear($courseYearID);

            while ($student = $db->fetch_row($students)) {
                $returnData[] = [
                    'student_id' => $student['student_id'],
                    'surname' => $student['surname'],
                    'forename' => $student['forename']
                ];
            }
        }
        
    /**
     * Get forms by department (export forms tool)
     */
    } elseif ($isfor == 'forms') {

        $term = filter_input(INPUT_POST, 'term', FILTER_SANITIZE_STRING);
        $department = filter_input(INPUT_POST, 'dept', FILTER_SANITIZE_STRING);

        $forms = $db->forms->getForms($term, $department);

        while ($form = $db->fetch_row($forms)) {
           
            $returnData[] = [
                'form_id' => $form['form_id'],
                'form_name' => $form['form_name']
            ];

        }

    /**
     * Get list of email template names
     */
    } elseif ($isfor == 'templates') {
        $templates = $db->feedback->getEmailTemplates(null, true, 'template_id');

        foreach ($templates as $id => $template) {

            // Skip log file and examiner training templates
            if (in_array($id, ['tp11', 'tp12'])) {
                continue;
            }

            $returnData[] = [
                'id' => $id,
                'name' => $template[0]['template_name']
            ];

        }

    /**
     * Get list of exams
     */
    } elseif ($isfor == 'exams') {

        $term = filter_input(INPUT_POST, 'term', FILTER_SANITIZE_STRING);
        if (is_null($term) || !$db->academicterms->existsById($term)) {

            $term = $_SESSION['cterm'];

        } else {

            $_SESSION['cterm'] = $term;

        }

        // Save term prefix for next time
        $db->userPresets->update(['filter_term' => $_SESSION['cterm']]);

        if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SCHOOL_ADMIN) {

             $adminSchools = $db->schools->getAdminSchools(
                 $_SESSION['user_identifier']
             );

             $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));

        } else if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_EXAM_ADMIN) {

             $deptIDs = $db->departments->getExaminerDepartmentIDs(
                 $_SESSION['user_identifier'],
                 $term
             );

        }

        $examRecords = \OMIS\Database\CoreDB::group(
            \OMIS\Database\CoreDB::into_array(
                 $db->exams->getListFiltered($term, $schoolIDs, $deptIDs)
             ), ['school_description', 'dept_id', 'exam_id'], true, true);

        $returnData['schools'] = $examRecords;

    }

    try {
        $json = JSON::encode($returnData);
    } catch (Exception $ex) {
        // Something went wrong encoding the data to JSON.
        error_log(__FILE__ . ": JSON Encode failed. Data follows:");
        error_log(print_r($returnData, true));
        /* The only correct thing to do here is return an internal server error
         * message.
         */
        http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        exit();
    }

    echo $json;
}
