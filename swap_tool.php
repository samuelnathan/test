<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('SWAP_STUDENTS_EXAM', 1);
define('SWAP_STUDENTS_COURSE', 2);
define('SWAP_STATION_RESULTS', 3);

// Include
use \OMIS\Database\CoreDB as CoreDB;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Exams
$examsDB = $db->exams->getList(NULL, $_SESSION['cterm']);
$exams = CoreDB::into_array($examsDB);

// Courses
$swapInCourses = $db->courses->getAllCourses($_SESSION['cterm']);

$swapOutSessions = [];
$swapOutGroups = [];
$swapOutStations = [];
$swapOutStudents = [];

$swapInSessions = [];
$swapInGroups = [];
$swapInStations = [];
$swapInStudents = [];
$swapInYears = [];

$swapInSelectedCourse = "";
$swapInSelectedYear = "";
$swapOutSelectedExam = "";
$swapInSelectedExam = "";
$swapOutSelectedSession = "";
$swapInSelectedSession = "";
$swapOutSelectedGroup = "";
$swapInSelectedGroup = ""; 
$swapOutSelectedStation = "";
$swapInSelectedExamStudent = "";
$swapInSelectedCourseStudent = "";
$swapOutSelectedStudent = "";
$swapAction = 1;

// Get swap status
$swapStatus = (isset($_SESSION['swap_student'])) ? $_SESSION['swap_student'] : "";
unset($_SESSION['swap_student']);


// Swap feature enabled
$swapFeatureEnabled = $db->features->enabled('swap-tool', $_SESSION['user_role']);
if ($swapFeatureEnabled) {

    /**
     *  Ooops, password entered by user was incorrect so we need to
     *  stick the variables selected (students, groups, course etc)
     *  and ask the user to enter their password again.
     *  If we need to stick the selections
     */
     if (isset($_SESSION['swap_criteria'])) {
        $stickVariables = $_SESSION['swap_criteria'];
        unset($_SESSION['swap_criteria']);

        list ($swapOutSelectedGroup,
              $swapOutSelectedStudent,
              $swapInSelectedGroup,
              $swapInSelectedExamStudent,
              $swapInSelectedCourseStudent,
              $swapInSelectedCourse,
              $swapInSelectedYear,
              $swapAction,
              $swapOutSelectedStation) = $stickVariables;

        /**
         * - Reverse engineer Session ID and Exam ID for swap OUT group
         * - Generate Session, Group and Student list
         */
        if (!empty($swapOutSelectedGroup)) {
            $swapOutSelectedSession = $db->exams->getSessionIDByGroupID($swapOutSelectedGroup);
            $swapOutSelectedExam = $db->exams->getSessionExamID($swapOutSelectedSession);
            $swapOutSessions = $db->exams->getExamSessions($swapOutSelectedExam, true);
            $swapOutGroups = $db->exams->getSessionGroups($swapOutSelectedSession, true);
            $swapOutStudents = $db->exams->getStudents($swapOutSelectedGroup, null, true);
            $swapOutStations = $db->exams->getStations([$swapOutSelectedSession], true);
        }

        /**
         * - Reverse engineer Session ID and Exam ID for swap IN group
         * - Generate Session, Group and Student list
         */
        if (in_array($swapAction, [SWAP_STUDENTS_EXAM, SWAP_STATION_RESULTS])) {
            
            if ($swapAction == SWAP_STUDENTS_EXAM && !empty($swapInSelectedGroup)) {
               $swapInSelectedSession = $db->exams->getSessionIDByGroupID($swapInSelectedGroup);
               $swapInSelectedExam = $db->exams->getSessionExamID($swapInSelectedSession);
               $swapInSessions = $db->exams->getExamSessions($swapInSelectedExam, true);
               $swapInGroups = $db->exams->getSessionGroups($swapInSelectedSession, true);
               $swapInStations = $db->exams->getStations([$swapInSelectedSession], true);
               $swapInStudents = $db->exams->getStudents([$swapInSelectedGroup], $swapInSelectedSession, true);
            } else {
               $swapInStudents = $swapOutStudents;
            }
        /**
         * - Generate a list of course years from course ID
         */    
        } elseif (!empty($swapInSelectedCourse)) {

            // Get list of years by course
            $swapInYears = $db->courses->getCourseYears(
                $swapInSelectedCourse,
                $_SESSION['cterm'],
                true
            );

            // Get list of students for the year selected
            $swapInStudents = $db->students->getStudentsInYear($swapInSelectedYear, true);

        }
    }
    
    // Template data
    $templateData = [
        'exams' => $exams,
        'courses' => $swapInCourses,
        'years' => $swapInYears,
        'swapin_sessions' => $swapInSessions,
        'swapout_sessions' => $swapOutSessions,
        'swapin_groups' => $swapInGroups,
        'swapout_groups' => $swapOutGroups,
        'swapin_stations' => $swapInStations,
        'swapout_stations' => $swapOutStations,    
        'swapin_students' => $swapInStudents,
        'swapout_students' => $swapOutStudents,

        'swapin_selected_course' => $swapInSelectedCourse,
        'swapin_selected_year' => $swapInSelectedYear,
        'swapout_selected_exam' => $swapOutSelectedExam,
        'swapin_selected_exam' => $swapInSelectedExam,    
        'swapout_selected_session' => $swapOutSelectedSession,
        'swapin_selected_session' => $swapInSelectedSession,
        'swapout_selected_group' => $swapOutSelectedGroup,
        'swapin_selected_group' => $swapInSelectedGroup,
        'swapout_selected_station' => $swapOutSelectedStation,  
        'swapout_selected_student' => $swapOutSelectedStudent,
        'swapin_selected_course_student' => $swapInSelectedCourseStudent,
        'swapin_selected_exam_student' => $swapInSelectedExamStudent,

        'swap_action' => $swapAction,
        'swap_status' => $swapStatus
    ];

    // Load and render the template.
    $template = new OMIS\Template("swap_tool.html.twig");
    $template->render($templateData);

// Appropriate error message
} else {
   $message = "You do not have access to this feature.<br/>" .
              "Please contact your administrator for further assistance";
   
   $returnUrl = "manage.php?page=admin_tool";
   
   $templateData = ['message' => $message,
                    'return_url' => $returnUrl,
                    'content_wrapper' => true]; 
   $errorTemplate = new OMIS\Template('error.html.twig');
   $errorTemplate->render($templateData);
}
