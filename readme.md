Configuring OMIS on a new system
=========
This document covers the basics of setting up OMIS on a new system. The important steps are:

 1. Run composer to install auto-loading libraries as required
 2. Set up the database
 3. Set up the configuration file
 4. Run `npm install` or `yarn install` to installed NPM libraries 

Required Packages (Dependencies) and Composer
--------
[Composer](http://www.getcomposer.org) is a dependency manager for php. It uses a JSON file called `composer.json` (which is in version control and should be checked out in OMIS' root directory) to define which libraries it is managing.

Installation instructions are available [here](http://getcomposer.org/doc/00-intro.md). Once it's installed, running the following command from the OMIS root should cause composer to automatically download the relevant version of the relevant libraries:

    user@server:/omis$ composer install

This will create a `vendor/` folder underneath the OMIS root, where all of its downloaded libraries will live. If any library managed by composer needs updating later, this can be achieved using:

    user@server:/omis$ composer update

It will also create a new file in the OMIS root - `composer.lock` - which is a data file for composer which tells it what's already been installed. **Do not delete composer.lock or the vendor/ folder**.

Composer Libraries (at the time of writing):

 1. [Twig](https://packagist.org/packages/twig/twig)
 1. [Analog](https://packagist.org/packages/analog/analog)
 1. [password-compat](https://packagist.org/packages/ircmaxell/password-compat)
 1. [TCPDF](https://packagist.org/packages/tecnick.com/tcpdf)
 1. [PHPExcel](https://packagist.org/packages/phpoffice/phpexcel)
 1. [array_column](https://packagist.org/packages/rhumsaa/array_column)
 1. [PHPMailer](https://packagist.org/packages/phpmailer/phpmailer)
 1. [TextMagic](https://packagist.org/packages/textmagic/smsphp)
 1. [JpGraph](https://packagist.org/packages/jpgraph/jpgraph)
 1. [SOAP](http://pear.php.net/package/SOAP/) (from PEAR)
 1. [Math_Stats](http://pear.php.net/package/Math_Stats) (from PEAR)

`array_column` and `password-compat` are only included for PHP 5.4 support. The
functionality they provide is provided natively by PHP 5.5+.

Set Up the Database
----------
I'm not going to go into details here, but basically:

1. Make sure that a suitable version of MySQL is installed
2. Create a schema with a suitable name
3. Populate that schema with a dump file defining either the demo data or whatever custom dump is required for this installation
4. Create a user with only the required privileges for only this schema - **don't use the root mysql user for this** (see [here](http://en.wikipedia.org/wiki/Principle_of_least_privilege) for why) - that can then be supplied to OMIS so that it can function. The list of privileges should be as short as possible, something like:
    - INSERT
    - UPDATE
    - DELETE
    - SELECT (and possibly:)
    - CREATE TEMPORARY TABLE
    - EXECUTE (for stored procedures)
    - FILE
    - LOCK TABLES
5. Use that newly-created MySQL user to configure OMIS' database access.

**OMIS 1.9.71:**

![omis_1.9.71.png](https://bitbucket.org/repo/Rednre/images/1303898422-omis_1.9.71.png)

Set Up The Configuration File
--------
This is covered in reasonable detail in [extra/readme.md](extra/readme.md). Go read that.

## Setting up Gulp ##

Run `npm install -g gulp-cli` or `yarn global add gulp-cli` to install the Gulp CLI on your OS. 

To run a task open a terminal and run `gulp <task>` and the rest is magic. There are three tasks you will care about:

**Task**|**What it does**|**When to use it**
:-----:|-----|-----
`build`|On demand draws all the CSS and JavaScript files from `assets/src`, minifies and uglifies them (generates source maps) |When running Observe in dev. mode
`build-production`|On demand draws all the CSS and JavaScript files from `assets/src`, minifies and uglifies them (no source maps) |When running Observe prod. mode
`watch`|Automatically (build automation) draws all the CSS and JavaScript files from `assets/src`, minifies and uglifies them (generates source maps) |When running Observe in dev. mode
`deploy`|On demand draws all the CSS and JavaScript files from `assets/src`, minifies the CSS and obscures the JavaScript files (excludes source maps) |When running Observe in prod. mode only (experimental)

You can also apply `build` and `deploy` tasks to either the CSS or JavaScript files `build` or `deploy` tasks  

Set Up The Omis .NET Email Service (Feedback System).
--------

Version: 4.0, compatible with Omis 1.9.x only

Location: 


```
#!

/opt/omis_mailer_4.0

```



Stored Procedures that must be installed on the client database:

```
#!MySql

DELIMITER ;;
CREATE DEFINER=`client_database_username`@`%` PROCEDURE `FindTemplateById`(
IN Templateid varchar ( 10 )
)
BEGIN

               SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
               SELECT email_subject, email_body   
               from email_templates 
               where template_id = Templateid;
               COMMIT;
       END ;;

CREATE DEFINER=`client_database_username`@`%` PROCEDURE `GetStudentData`()
BEGIN
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
   SELECT * FROM students s
               inner join student_feedback sf
               on s.student_id = sf.student_id
               inner join email_templates ft
               on ft.template_id = sf.template_id
               where sf.is_sent = 0 
               AND sf.is_created = 1
               AND sf.is_ready = 1;
    COMMIT;
        END ;;

CREATE DEFINER=`client_database_username`@`%` PROCEDURE `SetFeedbackStatusSent`(
        IN student_id varchar(30),
        IN osce_id bigint(10),
        IN time_sent datetime)
BEGIN
   SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;             
             Update student_feedback 
               Set 
               is_sent = 1,
               time_sent = time_sent
               where student_id = student_id
               AND exam_id = osce_id;
     COMMIT;               
 END ;;
DELIMITER ;

```


Template config file for the Omis .NET email service:


```
#!xml

<?xml version="1.0"?>
<configuration>

  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>

    <!-- CRUD SectionGroup Declaration -->
    <section name="JobConfiguration" type="Opercom.OMIS.ServicesEngine.Configuration, Opercom.OMIS.ServicesEngine"/>
    <!-- END CRUD SectionGroup -->

    <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" >
      <section name="Opercom.OMIS.ServicesEngine.Email.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    </sectionGroup>
    
  </configSections>

  <!-- JobSettings Configuration Section  if key is not found then its added to setting parameters .
  Job name must be unique -->
  <JobConfiguration>
    <JobSettings>
      <add key="StudentFeedbackJob.RuleNameSpace" Value="Opercom.OMIS.Jobs.StudentFeedbackJob.RunJob"/>
      <add key="StudentFeedbackJob.JobNumber" Value="1"/>
      <add key="StudentFeedbackJob.AttachmentFilePath" Value="/var/www/client_instance_directory/tmp/feedback/[student_id]_[exam_id].pdf"/>
      <add key="StudentFeedbackJob.DBConnection" Value="server=database_server;Allow Zero Datetime=true;user=user_name;database=database_name;port=3306;password=user_password;"/>
      <add key="StudentFeedbackJob.EmailTemplateDbColumn" Value="email_body"/>
      <add key="StudentFeedbackJob.FromEmailAddress" Value="joe.bloggs@qpercom.com"/>
      <add key="StudentFeedbackJob.FromFriendlyName" Value="Joe Bloggs"/>
	  <add key="StudentFeedbackJob.BCCStudentEmail" Value="true"/>
      <add key="StudentFeedbackJob.BCCEmailAddresses" Value="mary.bloggs@qpercom.com"/>
      <add key="StudentFeedbackJob.EmailSubjectDbColumn" Value="email_subject"/>
      <add key="StudentFeedbackJob.ProccessRecordLimit" Value="-1"/>
      <add key="StudentFeedbackJob.SendLogEmailAddresses" Value="joe.bloggs@qpercom.com"/>
      <add key="StudentFeedbackJob.SendLogEmailTemplateId" Value="tp11"/>
      <add key="StudentFeedbackJob.SendLogAppenderName" Value="StudentFeedbackJob_Logger"/>
    </JobSettings>
  </JobConfiguration>
  <!-- JobSettings Configuration Section  -->

  <appSettings>
    <add key="JobRootFolder" value="/opt/omis_mailer_4.0/"/>
    </appSettings>


   <applicationSettings>
    <Opercom.OMIS.ServicesEngine.Email.Properties.Settings>
      <setting name="host" serializeAs="String">
        <value>mail.qpercom.ie</value>
      </setting>
      <setting name="port" serializeAs="String">
        <value>25</value>
      </setting>
      <setting name="impersonate" serializeAs="String">
        <value>true</value>
      </setting>
      <setting name="username" serializeAs="String">
        <value>relay@qpercom.ie</value>
      </setting>
      <setting name="password" serializeAs="String">
        <value>7g2)%ut#!R8#98n5:$4#0R</value>
      </setting>
      <setting name="EnableCert" serializeAs="String">
        <value>True</value>
      </setting>
    </Opercom.OMIS.ServicesEngine.Email.Properties.Settings>
  </applicationSettings>
  
  
  <log4net>
    <!-- RollingFileAppender looks after rolling over files by size or date -->
    <appender name="RollingFileAppender_Logger" type="log4net.Appender.RollingFileAppender">
      <param name="File" value="/opt/omis_mailer_4.0/Logs/OMISSERVICE_Alllogs.txt"/>
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <!--This stops file locking after program stops. Remove if optimum performance is required -->
      <appendToFile value="false"/>
      <rollingStyle value="Date"/>
      <datePattern value="yyyyMMdd"/>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%newline%date %-5level - %message"/>
      </layout>
    </appender>


    <appender name="StudentFeedbackJob_Logger" type="log4net.Appender.RollingFileAppender">
      <param name="File" value="/opt/omis_mailer_4.0/Logs/StudentFeedback_Logs_Client.txt"/>
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock"/>
      <!--This stops file locking after program stops. Remove if optimum performance is required -->
      <appendToFile value="true"/>
      <rollingStyle value="Once"/>
      <datePattern value="yyyyMMdd"/>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%newline%date %-5level - %message"/>
      </layout>
    </appender>
    

    <logger name="Opercom.OMIS.Jobs.StudentFeedbackJob" >
      <level value="INFO"/>
      <appender-ref ref="StudentFeedbackJob_Logger"/>
    </logger>

    <!-- Setup the root category, add the appenders and set the default level -->
    <root>
      <level value="INFO"/>
      <appender-ref ref="RollingFileAppender_Logger"/>
    </root>

    
    
  </log4net>

  <startup>
    
  <supportedRuntime version="v2.0.50727"/></startup>
</configuration>

```  
  
   
   

It is required to install mono on the linux server (latest version 4.01 04/05/2015):

On Ubuntu (12.04):
```
#!php
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list

echo "deb http://download.mono-project.com/repo/debian wheezy-libtiff-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list

sudo apt-get update

apt-get install mono-complete
```

Reference: 
http://www.mono-project.com/docs/getting-started/install/linux/

## Tagging (Pipelines)

When a tag is pushed to the repo with the prefix 'release-', it builds a new docker image and push it to AWS ECR.

  1. Command to create a new tag
  ```$ git tag -a release-<VERSION>```

  2. Command to push a new tag to the repo
  ```$ git push origin release-<VERSION>```