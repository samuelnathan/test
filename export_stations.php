<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 define('_OMIS', 1);

 include __DIR__ . '/../extra/essentials.php';

 use \OMIS\Utilities\Path as Path;

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 // logfile setup
 $config = new OMIS\Config;

 $date = date('Y_m_j_H\hi');

 $workingDir = Path::join($config->tmp_path, 'exports');

 if (!is_dir($workingDir)) {

     error_log(__FILE__ . ": Working directory for exports doesn't exist. Creating.");

     if (!mkdir($workingDir, 0700, true)) {

         error_log(__FILE__ . ": Failed to create working directory for exports '$workingDir'. Aborting.");
         /* @TODO (DW) Replace this with some sort of proper error message when
          * we have time.
          */
         die("Failed to create specified export folder. Export failed. Please click back to continue.");

     }

 }

 $zipFilename = 'exported' . $date . '.zip';
 $combined = Path::join($workingDir, $zipFilename);

 $zip = new ZipArchive();
 $zip->open($combined, ZipArchive::CREATE);

 $pathNum = 0;
 $pathFiles[0] = 0;

 // Get the form IDs.
 $formIDs = filter_input(
         INPUT_POST,
         'stations',
         FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY
 );

 $exportPath = Path::join($workingDir, 'files');

 if (!is_dir($exportPath)) {

     error_log(__FILE__ . ": Temporary ZIP working directory for exports doesn't exist. Creating.");

     if (!mkdir($exportPath, 0700, true)) {

         error_log(__FILE__ . ": Failed to create temporary ZIP directory for exports '$exportPath'. Aborting.");
         /* @TODO (DW) Replace this with some sort of proper error message when
          * we have time.
          */
         die("Failed to create specified export folder. Export failed. Please click back to continue.");

     }

 }

 // Disable html entity conversion
 $db->set_Convert_Html_Entities(false);

 foreach ($formIDs as $formID) {

     if ($db->forms->doesFormExist($formID)) {

         $formRecord = $db->forms->getForm($formID);
         \Analog::info($_SESSION['user_identifier'] . " exported station called " . $formRecord['form_name']);

         // Preparing new XML document
         $doc = new DOMDocument("1.0", "UTF-8");
         $doc->formatOutput = true;
         $stationParent = $doc->createElement('station');
         $doc->appendChild($stationParent);

         // Meta Data
         $stationDB = $db->forms->getForm($formID);
         $formName = $stationDB['form_name'];
         $author = ($stationDB['author']);
         $formDescription = ($stationDB['form_description']);

         $metaParent = $doc->createElement('meta');

         // Station Name
         $titleElement = $doc->createElement('title');
         $titleElement->appendChild($doc->createTextNode($formName));
         $metaParent->appendChild($titleElement);

         // Author name
         $authorElement = $doc->createElement('author');
         $authorElement->appendChild($doc->createTextNode($author));
         $metaParent->appendChild($authorElement);

         // Station description
         $formDescriptionElement = $doc->createElement('form_description');
         $formDescriptionElement->appendChild($doc->createTextNode($formDescription));
         $metaParent->appendChild($formDescriptionElement);
         $stationParent->appendChild($metaParent);

         /*
          *  Add content tags if any
          */
         $tagsParent = $doc->createElement('tags');

         $contentTags = $db->forms->getFormTags($formID);
         foreach ($contentTags as $tag) {

           $eachTagElement = $doc->createElement('tag');
           $eachTagElement->appendChild($doc->createTextNode($tag['name']));
           $tagsParent->appendChild($eachTagElement);

         }

         $stationParent->appendChild($tagsParent);

         // Competencies
         $competencesParent = $doc->createElement('competences');

         // form sections
         $formSections = $db->forms->getFormSections([$formID], false);

         // Process station sections
         while ($section = $db->fetch_row($formSections)) {

             $items = $db->forms->getSectionItems([$section['section_id']]);

             $competenceParent = $doc->createElement('competence');

             $competenceTypeElement = $doc->createElement('competence_id');
             $competenceTypeElement->appendChild($doc->createTextNode($section['competence_id']));
             $competenceParent->appendChild($competenceTypeElement);

             $additionalInfoElement = $doc->createElement('section_text');
             $additionalInfoElement->appendChild($doc->createTextNode($section['section_text']));
             $competenceParent->appendChild($additionalInfoElement);

             $feedbackRequiredElement = $doc->createElement('feedback_required');
             $feedbackRequiredElement->appendChild($doc->createTextNode($section['feedback_required']));
             $competenceParent->appendChild($feedbackRequiredElement);

             $sectionFeedbackElement = $doc->createElement('section_feedback');
             $sectionFeedbackElement->appendChild($doc->createTextNode($section['section_feedback']));
             $competenceParent->appendChild($sectionFeedbackElement);

             $itemFeedbackElement = $doc->createElement('item_feedback');
             $itemFeedbackElement->appendChild($doc->createTextNode($section['item_feedback']));
             $competenceParent->appendChild($itemFeedbackElement);

             $internalFeedbackElement = $doc->createElement('internal_feedback_only');
             $internalFeedbackElement->appendChild($doc->createTextNode($section['internal_feedback_only']));
             $competenceParent->appendChild($internalFeedbackElement);

             $itemsParent = $doc->createElement('items');

             // Process section items
             while ($item = $db->fetch_row($items)) {

                 $itemParent = $doc->createElement('item');
                 $itemDescElement = $doc->createElement('text');

                 // Look at HTML
                 $itemHtml = adjustHTML($item['text']);

                 // Finding images in HTML
                 $piecesA = explode("src=\"", $itemHtml);
                 $numPieces = count($piecesA);
                 $pieceCount = 1;

                 while ($pieceCount < $numPieces) {

                     $piecesB = explode("\"", $piecesA[$pieceCount]);
                     $pieceCount++;
                     $pathFiles[$pathNum] = $piecesB[0];
                     $pathNum++;

                 }

                 // Creating CDATA Node
                 $itemDescElement->appendChild($doc->createCDATASection($itemHtml));
                 $itemParent->appendChild($itemDescElement);

                 $answerTypeElement = $doc->createElement('type');
                 $answerTypeElement->appendChild($doc->createTextNode($item['type']));
                 $itemParent->appendChild($answerTypeElement);

                 $scaleElement = $doc->createElement('grs');
                 $scaleElement->appendChild($doc->createTextNode($item['grs']));
                 $itemParent->appendChild($scaleElement);

                 $criticalElement = $doc->createElement('critical');
                 $criticalElement->appendChild($doc->createTextNode($item['critical']));
                 $itemParent->appendChild($criticalElement);

                 $highestItemValueElement = $doc->createElement('highest_value');
                 $highestItemValueElement->appendChild($doc->createTextNode($item['highest_value']));
                 $itemParent->appendChild($highestItemValueElement);

                 $lowestItemValueElement = $doc->createElement('lowest_value');
                 $lowestItemValueElement->appendChild($doc->createTextNode($item['lowest_value']));
                 $itemParent->appendChild($lowestItemValueElement);

                 $itemsParent->appendChild($itemParent);
                 $competenceParent->appendChild($itemsParent);

                 $options = $db->forms->getItemOptions($item['item_id']);

                 $optionsParent = $doc->createElement('options');

                 // Process item options
                 while ($option = $db->fetch_row($options)) {

                     $optionParent = $doc->createElement('option');

                     $valueElement = $doc->createElement('value');
                     $valueElement->appendChild($doc->createTextNode($option['option_value']));
                     $optionParent->appendChild($valueElement);

                     $textElement = $doc->createElement('text');
                     $textElement->appendChild($doc->createTextNode($option['descriptor']));
                     $optionParent->appendChild($textElement);

                     $flagElement = $doc->createElement('flag');
                     $flagElement->appendChild($doc->createTextNode($option['flag']));
                     $optionParent->appendChild($flagElement);

                     $optionsParent->appendChild($optionParent);
                     $itemParent->appendChild($optionsParent);

                 }

             }

             $competencesParent->appendChild($competenceParent);
             $stationParent->appendChild($competencesParent);

         }

         // Creating & saving XML file
         $fileName = remove_alt_char($formName) . '.xml';
         $filePath = Path::join($exportPath, $fileName);

         $doc->save($filePath);
         $zip->addFile($filePath, $fileName);

         // Adding images
         if ($pathFiles[0] !== 0) {

             foreach ($pathFiles as $pathfile) {

                 $zip->addFile('../' . $pathfile, $pathfile);

             }

         }

     }

 }

 // Closing Zipfile
 $zip->close();

 header('Content-type: application/zip');
 header("Content-length: " . filesize($combined));
 header('Content-Disposition: attachment; filename="' . $zipFilename . '"');
 readfile($combined);

 // Clean files
 unlink($combined);
 foreach (glob(Path::join($exportPath, '*.xml')) as $file) {

     unlink($file);

 }
