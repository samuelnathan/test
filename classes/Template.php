<?php
namespace OMIS;

use OMIS\Utilities\Path as Path;
use Twig_SimpleFilter;

/**
 * Wrapper class for rendering templates with the Twig templating engine
 *
 * @link http://twig.sensiolabs.com/documentation/ Twig Documentation
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class Template
{
    private $config;
    private $template_path;
    // Template data...
    public $data = [];
    // Twig Object...
    private $twig;

    /**
     * Constructor.
     *
     * @param  string $template Path to desired Twig template
     * @param array $extraPaths Extra paths to load templates from
     * @throws \Twig_Error_Loader
     * @global OMIS\Config $config Configuration settings store
     */
    public function __construct($template, $extraPaths = [])
    {
        // This is for the configuration file...
        global $config;

        // Make sure the template file exists...
        if (!is_string($template) || empty($template)) {
            $error = "Supplied template path is not a string or is empty, cannot be a valid path";
            throw new \RuntimeException($error);
        }

        $template = Utilities\Path::fixSeparators($template);

        // Set up Twig...
        if (dirname($template) != ".") {
            // The thing exists.
            $template_folder = dirname($template);
        } else {
            /* We don't have a path, so we have to assume that it's the path
             * from which this class was called. The only way I can find to do
             * this is to dismantle a backtrace, so here goes...
             */
            $trace = debug_backtrace();
            $caller = $trace[0];
            $template_folder = dirname($caller['file']);
        }

        // If the configuration file is not loaded, then instantiate it...
        $this->config = (isset($config)) ? $config : new OMIS\Config;

        $loader = new \Twig_Loader_Filesystem($template_folder);
        $this->template_path = $template;
        $loader->addPath($config->template_path);

        // Include resources path for changelog files
        $loader->addPath($config->base_path . '/resources');
        
        // Include layout path for templates
        $loader->addPath($config->base_path . '/layout');

        foreach ($extraPaths as $extraPath) {
            $loader->addPath($extraPath);
        }

        $this->twig = new \Twig_Environment(
            $loader,
            array('cache' => $config->twig_cache, 'auto_reload' => true)
        );
       
        // Add I18n extension
        $twigI18nExtension = new \Twig_Extensions_Extension_I18n();
        $this->twig->addExtension($twigI18nExtension);
   
        /*
         * Custom filter that can be used to decode html entities in twig templates
         * Author: David Cunningham: 
         * Date: 13/12/2014 
         */
        
        $decodeHtmlFilter = new Twig_SimpleFilter('decode_html_entities', function ($html) {
            /* (DW) Modified this quite a bit. It should now also remove any
             * numerically encoded entities, both in decimal and hex..
             */
            return preg_replace_callback(
                "/(&#[0-9]+;|&#x[0-9a-fA-F]+;)/",
                function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); },
                html_entity_decode($html, ENT_QUOTES, 'UTF-8')
            ); 
        });
        $this->twig->addFilter($decodeHtmlFilter);        
        
        
        /*
         * Custom filter 'ucwords' that uppercases the first character of each word in a string
         * and respects the remaining characters of each of the words, whether they are lowercase or uppercase. 
         * On the otherhand, the built-in title filter uppercases the first letter of each
         * word and lowercase's the rest.  
         * Author: David Cunningham: 
         * Date: 07/12/2015
         */
        
        $ucwords = new Twig_SimpleFilter('ucwords', function ($string) {
            return ucwords($string);
        });
        $this->twig->addFilter($ucwords);        
        
        
        /**
         * Word wrap extension (Twig's word wrap function doesn't behave the same way)
         */
        $wordWrap = new Twig_SimpleFilter('custom_wordwrap', function ($string, $limit) {
            return wordwrap($string, $limit, "<br>\n");
        });
        
        $this->twig->addFilter($wordWrap);         
       
        // Just in case, we may need to instantiate this to something.
        $this->data = array();
    }

    /**
     * Render the template with the provided data. If no data is provided as a
     * parameter for the function, the existing data is used.
     *
     * @param mixed[] $data
     * @param bool    $returnValue If TRUE, return the rendered template rather
     * than outputting it.
     * @return mixed Nothing if $returnValue is FALSE, otherwise the rendered
     * template output.
     */
    public function render($data = null, $returnValue = \FALSE)
    {
        // Load the relevant template
        $template = $this->twig->loadTemplate($this->template_path);

        /* If $data isn't empty, merge it into whatever's already there before
         * rendering the template.
         */
        if (!empty($data)) {
            $this->data = array_merge($this->data, $data);
        }
        
        /* Render the template. This calls the Twig Template object's own render
         * method, it's not being recursive :)
         */
        if ($returnValue) {
            return $template->render($this->data);
        } else {
            $template->display($this->data);
        }
    }
    
    /**
     * Given the name of a PHP script, this checks if there's a matching template
     * to go with it in the same folder. If so, it returns the full path to that
     * file.
     * 
     * @param string $file Path to file
     * @return string|null Path to template file if found, otherwise NULL.
     */
    static public function findMatchingTemplate($file) {
        // Quick sanity check...
        if (empty($file) || (trim($file) === '')) {
            return NULL;
        }
        
        $parts = pathinfo($file);
        
        $path_base = Path::setSeparator(Path::join($parts['dirname'], $parts['filename']));
        
        // These are the file suffixes we're expecting.
        $file_suffixes = ['.html.twig', '.twig'];
        
        foreach ($file_suffixes as $suffix) {
            $expected_file = Path::fixSeparators($path_base . $suffix);
            if (file_exists($expected_file)) {
                /* Twig will find the file in the current path, this seems to
                 * work better than an absolute path.
                 */
                return basename($expected_file);
            }
        }
        
        return NULL;
    }
}
