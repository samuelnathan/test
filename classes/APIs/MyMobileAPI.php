<?php
namespace OMIS\APIs;

class MyMobileAPI {
    
    public function __construct($username, $password, $senderID) {
        $this->url = 'http://www.mymobileapi.com/api5/http5.aspx';
        $this->username = $username; // your login username
        $this->password = $password; // your login password
        $this->senderID = $senderID; // your sender identifier
    }
    
    /**
     * Get number of credits remaining.
     * 
     * @return string
     */
    public function checkCredits() {
        $data = array(
            'Type' => 'credits',
            'Username' => $this->username,
            'Password' => $this->password
        );
        $response = $this->querySmsServer($data);
        
        // NULL response only if connection to sms server failed or timed out
        if ($response == NULL) {
            return '???';
        } elseif ($response->call_result->result) {
            return $response->data->credits;
        }
    }

    /**
     * Send SMS Message
     * 
     * @param string $mobile_number Mobile number to send message to
     * @param string $msg Message text/body
     * @return \SimpleXMLElement response from server.
     */
    public function sendSms($mobile_number, $msg) {
        $data = array(
            'customerid' => 'Qpercom',
            'senderid' => $this->senderID,
            'Type' => 'sendparam',
            'Username' => $this->username,
            'Password' => $this->password,
            'numto' => $mobile_number, //phone numbers (can be comma seperated)
            'data1' => $msg, //your sms message
        );
        $response = $this->querySmsServer($data);
        return $this->returnResult($response);
    }

    /**
     * Query API server and return response in object format
     * 
     * @param mixed $messageParameters
     * @param mixed $optional_headers
     * @return \SimpleXMLElement
     */
    private function querySmsServer($messageParameters, $optional_headers = null) {
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $messageParameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        /* prevent large delays in PHP execution by setting timeouts while 
         * connecting and querying the 3rd party server
         */
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 2000); // response wait time
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 2000); // output response time
        $response = curl_exec($ch);
        if (!$response)
            return NULL;
        else
            return new \SimpleXMLElement($response);
    }

    /**
     * Handle SMS server response
     * 
     * @param \SimpleXMLElement $response Response from server
     * @return \OMIS\APIs\StdClass Formatted response description
     */
    private function returnResult($response) {
        $return = new \StdClass();
        $return->pass = NULL;
        $return->msg = '';
        if ($response == NULL) {
            $return->pass = FALSE;
            $return->msg = 'SMS connection error.';
        } elseif ($response->call_result->result) {
            $return->pass = 'CallResult: ' . TRUE . '</br>';
            $return->msg = 'EventId: ' . $response->send_info->eventid . '</br>Error: ' . $response->call_result->error;
        } else {
            $return->pass = 'CallResult: ' . FALSE . '</br>';
            $return->msg = 'Error: ' . $response->call_result->error;
        }
        return $return;
    }
}
