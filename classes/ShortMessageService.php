<?php
/**
 * Utility class to simply process and send SMS messages
 * @author David Cunningham <david.cunningham@qpercom.ie>
 */
namespace OMIS; 
use \libphonenumber\PhoneNumberUtil as PhoneNumberUtil;

 class ShortMessageService
 {
   
    const MESSAGE_UNICODE = true;
   
    private $config;
    private $library;
    private $countryCode;
    private $username;
    private $password;
    private $senderid;
    private $creditsRemaining;
   
    public function __construct()
    {
      
        // If the configuration file is not loaded, then instantiate it...
        global $config;
        $this->config = (isset($config)) ? $config : new OMIS\Config; 
      
        // Library
        $this->library = $config->sms['library'];
      
        // Country code
        $this->countryCode = (isset($config->sms['country'])) ? $config->sms['country'] : 'IE';
      
        // Mobile username & password
        $this->username = $config->sms['username'];
        $this->password = $config->sms['password'];
        $this->senderid = isset($config->localization['skin']['app_name']) ? 
                $config->localization['skin']['app_name'] : "Qpercom";
      
        // Credits remaining (default)
        $this->creditsRemaining = 0;
      
    }
    
    /**
     * Send SMS Message
     * 
     * @param int[] $MobileNumbers  List of phone numbers
     * @param string $message       Message to send
     * @return bool  true|false     Status
     */
    public function send($MobileNumbers, $message)
    {
                 
      // Validate numbers
      $numbers = $this->validateNumbers($MobileNumbers);

      if (count($numbers) == 0) {
         error_log("No valid numbers found, aborting SMS send");
         return false;
      }
      
      // Library Switch
      switch (strtolower($this->library)) {
  
         // Case Text Magic
         case 'textmagic':
        
            try {
           
              // Protocol connect / authenticate
              $api = new \TextMagicSMS\TextMagicAPI([
                 'username' => $this->username,
                 'password' => $this->password
              ]);

              // Credits Left
              $this->creditsRemaining = $api->getBalance();

              // Insufficient Credits? If not then abort
              if ($this->creditsRemaining == 0) {
                 error_log("No credits remaining, check account for library: '" . $this->library . "'");
                 return false;
                 break;
              }

              // Send message to recipients
              $api->send($message, $numbers, self::MESSAGE_UNICODE);
            
            } catch (\Exception $e) {
              
              error_log(
               "Failed to send SMS using library: " . 
               $this->library . " Error: " . $e->getMessage()
              );
              
            }
            
            break;
        
        // Case Bulk Text
        case 'bulktext': 
        case 'mymobileapi':
                   
            try {
          
              $api = new \OMIS\APIs\MyMobileAPI(
                  $this->username, 
                  $this->password,
                  $this->senderid
              );

              $this->creditsRemaining = $api->checkCredits();

              // Insufficient Credits? If not then abort
              if ($this->creditsRemaining == 0) {
                 error_log("No credits remaining, check account for library: '" . $this->library . "'");
                 return false;
                 break;
              }

              // Send SMS messages
              $api->sendSms(implode(',', $numbers), $message);

            } catch (\Exception $e) {
              
              error_log(
               "Failed to send SMS using library: " . 
               $this->library . " Error: " . $e->getMessage()
              );
              
            }
            
           break;
         default:
            error_log(
              "Unknown SMS API/Service '". $this->library . "' in ShortMessageService.php / Assessment"
            );     
            return false;
      }
   
      return true;
      
    }
    
   /**
    * Validate all numbers
    * 
    * @param int[] $numbers       List of mobile numbers to be validated
    * @return int[]  true|false   Valid numbers
    */
    private function validateNumbers($numbers)
    {
        $validNumbers = []; 
      
        // Loop through numbers
        foreach ($numbers as $number) {

            // Remove any non numeric characters
            $numberSan = preg_replace('/\D/', '', $number);

           /**
            *  Parse mobile number while including the country ISO (config file)
            */
            $phoneUtil = PhoneNumberUtil::getInstance();
            try {
              
               $mobileNumObj = $phoneUtil->parse($numberSan, $this->countryCode);
               
            } catch (\libphonenumber\NumberParseException $e) {
                
               error_log($e);
               return [];
               
            }

            // Valid mobile number?
            $mobileNumValid = $phoneUtil->isValidNumber($mobileNumObj);              

            // If it's a valid mobile then add it to the send queue
            if ($mobileNumValid) {
              
               $validNumbers[] = $numberSan;
               
            }

        }
      
        return $validNumbers;
      
    }

 }
 