<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 * Outcomes base class
 */

 namespace OMIS\Outcomes;

 abstract class Outcome
 {

     public function __construct()
     {

     }

     /*
      * Method to determine outcome
      */
     abstract public function determine(array $data, Rules $rules);

     /**
      * Determine final score and possible either weighted or aggregated
      *
      * @param bool  $aggregated     Aggregated scores
      * @param bool  $weighted       Weighted rule (yes/no)
      * @param mixed[] $data         List of score values, possibles and their weightings
      *
      * @return int[] returns final station score and possible calculated
      * @throws InvalidArgumentException
      */
     protected function determineScoreAndPossible($aggregated, $weighted, $data)
     {

         $finalScore = $finalPossible = 0;

         // Abort if no data
         if (empty($data))
             return [0, 0];

         /**
          * AGGREGATE taken from the multiple scores
          */
         if ($aggregated) {

             $finalPossible = array_sum(array_column($data, 'possible'));
             $finalScore = array_sum(array_column($data, 'value'));


         /**
          * WEIGHTED AVERAGE taken from the multiple scores
          */
         } elseif ($weighted) {

             $totalWeight = array_sum(array_column($data, 'weighting'));
             $finalPossible = max(array_column($data, 'possible'));

             $finalScore = (array_sum(array_map(function ($each) {
                 return ($each['value'] * $each['weighting']);
             }, $data)) / $totalWeight);


         /**
          * STRAIGHT AVERAGE taken from the multiple scores (DEFAULT)
          */
         } else {

             $finalPossible = max(array_column($data, 'possible'));
             $finalScore = (array_sum(array_column($data, 'value')) / count($data));

         }

         return [$finalScore, $finalPossible];

     }

     /**
      * Determine if the student passed or failed the item within the limits set by
      * the "grace interval" set in the configuration.
      *
      * @param float     $awarded        Marks awarded for the item.
      * @param float     $pass_pc        The percentage specified for this item
      *                                  as being a "pass"
      * @param float     $maximum        The maximum marks possible for the item.
      *                                  Default is 100 to allow percentages to
      *                                  be easily processed.
      * @param int       $decimal_places How many places to round when doing pass
      *                                  /fail comparison
      * @param float     $grace_pc       How far below the actual pass grade a
      *                                  mark can be before it's deemed a "fail".
      * @return boolean TRUE if the student passed, FALSE if they fail.
      * @throws InvalidArgumentException
      */
     protected function determinePassOrFail($awarded, $pass_pc, $maximum = 100, $decimal_places = 1, $grace_pc = null)
     {
         global $config;

         /* If $grace_pc is not set, it pulls the default value from the
          * configuration and uses that.
          */
         if (empty($grace_pc)) {
             $grace_pc = (float) $config->station_defaults['grace_percent'];
         }

         // If the constant 'FUDGE_FACTOR' is not already defined then define it
         if (!defined('FUDGE_FACTOR')) {
             define('FUDGE_FACTOR', 0.00001);
         }

         // Some sanity checking.
         // Checking that the awarded marks make sense versus the possible max.
         // This approach means that there's a "fudge factor" to allow for the
         // possibility that there have been issues expressing the figure as an
         // IEEE-754 floating point number. Rather than an "x > y" test we're
         // using "x - y > e" test which should be more forgivng as the precision
         // of e is far fewer decimal places.
         if (($awarded - $maximum) > FUDGE_FACTOR) {
             $error_message = sprintf("Awarded marks %.2f is greater than "
                 . "specified maximum of %f", $awarded, $maximum);
             //throw new \InvalidArgumentException($error_message);
             error_log($error_message);
             return true;
         }

         // Make sure that the number of decimal places is sane.
         if (!is_int($decimal_places) || $decimal_places < 0) {
             $error_message = "Decimal places value of $decimal_places is invalid"
                 . " / impossible";
             //throw new \InvalidArgumentException($error_message);
             error_log($error_message);
         }

         // Make sure that the grace percentage is positive or zero.
         if (!is_numeric($grace_pc) || ($grace_pc < 0)) {
             $error_message = "Grace percentage value of $grace_pc is invalid "
                 . "/ impossible";
             //throw new \InvalidArgumentException($error_message);
             error_log($error_message);
         }

         /* Subtract the grace percentage from the pass percentage to get the
          * actual grace percentage.
          */
         $pass_pc -= $grace_pc;

         /* Calculate the score, rounded to the appropriate number of decimal
          * places.
          */
         $awarded_pc = @((float) $awarded * 100 / $maximum);

         /* Round number up to the appropriate number of decimal places. Can't
          * use builtin round() function here as we want to round up no matter
          * what, e.g. rounding 3.21 to one decimal place to result in 3.3, not
          * 3.2.
          */
         $scaling = pow(10, $decimal_places);
         $awarded_pc_rounded = round($awarded_pc * $scaling) / $scaling;

         // Make the "big decision"...
         return ($awarded_pc_rounded - $pass_pc) > (0 - FUDGE_FACTOR);

     }

 }