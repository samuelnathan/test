<?php

 namespace OMIS\Outcomes;
 use OMIS\Database\CoreDB as CoreDB;


 class OutcomesService
 {

     /**
      * @var CoreDB
      */
     private $db;


     public function __construct(CoreDB $db)
     {
         $this->db = $db;
     }

     /**
      * Get the outcomes for a given student in a given exam. If the outcomes are
      * set to be recalculated, performs the calculation before obtaining the outcomes
      *
      * @param $studentId
      * @param $examId
      * @return array|null
      */
     public function getOutcomesForStudentAndExam($studentId, $examId)
     {

         $this->db->startTransaction();

         // Check if there's need to recalculate the outcomes before querying them
         $recalculate = !$this->db->studentStationOutcomes->isUpToDate($studentId, $examId);

        
         // If it is, perform the calculation
         if ($recalculate) {
     
             $this->calculateByStudentAndExam($studentId, $examId);

             $this->db->exams->resultsUpdatedAt(
                $this->db->exams->getSessionExamID($sessionId)
             );

         }    

         // Obtain the outcomes
         $result = $this->db->studentStationOutcomes->byStudentAndExam($studentId, $examId);

         $this->db->commit();

         return $result;

     }

     /**
      * Get the outcomes for a given student in a given station. If the outcomes
      * for the exam associated with the station are set to be recalculated,
      * performs the calculation before obtaining the outcomes
      *
      * @param $studentId
      * @param $sessionId
      * @param $number
      * @return mixed[]
      */
     public function getOutcomesForStudentAndStation($studentId, $sessionId, $number)
     {
   
         $this->db->startTransaction();

         // Check if there's need to recalculate the outcomes before querying them
         $recalculate = !$this->db->studentStationOutcomes->isUpToDateByStation($studentId, $sessionId, $number);

         // If it is, perform the calculation
         if ($recalculate) {

             $this->calculateByStudentAndExam($studentId, $sessionId, $number);

             $this->db->exams->resultsUpdatedAt(
                 $this->db->exams->getSessionExamID($sessionId)
             );   

         }    

         // Obtain the outcomes
         $result = $this->db->studentStationOutcomes->byStudentAndStation($studentId, $sessionId, $number);

         $this->db->commit();

         return $result;
     }

     /**
      * Set the up to date flag for a given student in a given exam
      *
      * @param $studentId
      * @param $examId
      * @param $isUpToDate
      * @return bool Success of the operation
      */
     public function setUpToDate($studentId, $examId, $isUpToDate)
     {
         return $this->db->examdata->setUpToDate($studentId, $examId, $isUpToDate);
     }

     /**
      * Set the up to date flag for all students in a given exam
      *
      * @param int $examId
      * @param bool $isUpToDate
      * @return bool Success of the operation
      */
     public function setUpToDateByExam($examId, $isUpToDate)
     {
         return $this->db->examdata->setUpToDateByExam($examId, $isUpToDate);
     }

     /**
      * Set the up to date flag for all students in a given session
      *
      * @param int $sessionId
      * @param bool $isUpToDate
      * @return bool Success of the operation
      */
     public function setUpToDateBySession($sessionId, $isUpToDate)
     {
         return $this->db->examdata->setUpToDateBySession($sessionId, $isUpToDate);
     }

     /**
      * Set the up to date flag for a given student in the exam associated with a given
      * station
      *
      * @param $studentId
      * @param $sessionId
      * @param $stationNum
      * @param $isUpToDate
      * @return int Number of rows affected
      */
     public function setUpToDateByStation($studentId, $sessionId, $stationNum, $isUpToDate)
     {
         return $this->db->studentStationOutcomes->setUpToDateByStation($studentId, $sessionId, $stationNum, $isUpToDate);
     }

     /**
      * Set the up to date flag for the students that have results
      * in the exams associated with a given form
      *
      * @param $formId
      * @param $isUpToDate
      * @return int Number of rows affected
      */
     public function setUpToDateByForm($formId, $isUpToDate)
     {
         return $this->db->studentStationOutcomes->setUpToDateByForm($formId, $isUpToDate);
     }


     /**
      * Calculate totals and grades per student and exam
      *
      * @param string $studentId   Identifier number for student
      * @param int $examId         Identifier number for exam
      * @return bool status        Calculation status
      */
     private function calculateByStudentAndExam($studentId, $examId)
     {

         // Get exam stations results
         $stations = CoreDB::group(
             CoreDB::into_array(
                 $this->db->results->getResultsByExam($studentId, $examId, false)
             ), ['station_number'], true
         );

         // If there are no results for this student in the exam, set it as up to date and
         // return true
         if (count($stations) === 0) {
             $this->setUpToDate($studentId, $examId, true);
             return true;
         }

         // Get exam rules
         $rules = new Rules(
             $this->db->examRules->getRules($examId)
         );

         $stationOutcomes = $this->calculateByStudentAndStations($studentId, $stations, $rules);
         $examOutcome = (new ExamOutcome())->determine($stationOutcomes, $rules);

         $this->db->examdata->updateOutcome(
             $studentId, $examId, $examOutcome
         );

         $this->setUpToDate($studentId, $examId, true);

         // Temp return (DC: come back to)
         return true;

     }

     /**
      * Calculate totals and grades by student and list of stations
      *
      * @param string $studentId            Identifier number for student
      * @param mixed[] $stations            Stations data
      * @param mixed[] $rules               Exam rules data
      * @return mixed[] $stationOutcomes    Station outcomes
      */
     private function calculateByStudentAndStations($studentId, $stations, $rules)
     {

         $stationOutcomes = [];

         // Get student exam possible (required for station weightings calculations)
         $examPossible = array_reduce($stations, function(&$possible, $forms) {
             return $possible + array_sum(array_column($forms, 'adjusted_possible'));
         });

         // Determine on a station per station basis
         foreach ($stations as $number => $forms) {

             $sessionId = (int)$forms[0]['session_id'];
             $formsIds = array_column($forms, 'form_id');

             // Get examiner weightings
             $examiners = $this->db->stationExaminers->getExaminerWeightings(
                 array_unique(array_column($forms, 'station_id'))
             );

             // Calculate items outcomes
             $this->calculateByStudentItems($studentId, $sessionId, $formsIds, $rules);

             // Calculate station outcomes
             $stationOutcome = (new StationOutcome())->determine([
                 'forms' => $forms,
                 'examiners' => $examiners,
                 'examPossible' => $examPossible,
             ], $rules);

             $stationOutcomes[] = $stationOutcome;

             $this->db->studentStationOutcomes->update(
                 $studentId, $sessionId, $number, $stationOutcome
             );

         }

         return $stationOutcomes;

     }

     /**
      * Calculate totals and grades by student and list of items
      *
      * @param string $studentId      Identifier number for student
      * @param int   $sessionId       Identifier number for session
      * @param int[] $formIds         Identifier numbers for the forms
      * @param mixed[] $rules         Exam rules data
      * @return bool status           Calculation status
      */
     private function calculateByStudentItems($studentId, $sessionId, $formIds, $rules)
     {

         $items = $this->db->results->getItemScoresByStudentSession($studentId, $sessionId, $formIds);

         // Determine on a station per station basis
         foreach ($items as $id => $scores) {

             $itemOutcome = (new ItemOutcome())->determine($scores, $rules);

             $this->db->studentItemOutcomes->update(
                 $studentId, $sessionId, $id, $itemOutcome
             );

         }

         // Temp return (DC: come back to)
         return true;

     }
    public function selfAssessmentAdjustment($resultID) {
        $selfAssessment = $this->db->results->selfAssessmentCheck($resultID);

        if ($selfAssessment !== null) {
            $examinerId = $selfAssessment['examiner_id'];
            $studentId = $selfAssessment['student_id'];

            $isSelfAssessed = $this->db->selfAssessments->getOwnershipByStudentExaminer($studentId, $examinerId);

            if ($isSelfAssessed == 0 ) {
                $selfAssessedScore = $this->db->selfAssessments->selfAssessedScore($resultID);

                $possScore = $selfAssessedScore['poss'];
                $possWeighted = $selfAssessedScore['weightedPoss'];

                $this->db->results->setPossScoreSelfAssess($possScore, $possWeighted, $resultID);

            }
        }
    }
    public function UpdateCompleteResults($latestScore, $passValue, $stationPossible, $examId) {

        $scores = array($latestScore, $stationPossible, $passValue);

        $rules = new Rules($this->db->examRules->getRules($examId));

        $formOutcome = (new FormOutcome())->determine($scores, $rules);

        return $formOutcome;
    }
 }
