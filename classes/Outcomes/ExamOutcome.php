<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * Outcomes exam level class
  */

 namespace OMIS\Outcomes;


 class ExamOutcome extends Outcome
 {

     public $score = 0.000;
     public $adjustedScore = 0.000;
     public $possibleScore = 0.000;
     public $adjustedPossible = 0.000;
     public $passPossible = 0.000;
     public $passed = 0;
     public $adjustedPassed = 0;

     // Method to determine outcome
     public function determine(array $data, Rules $rules)
     {

         if (empty($data)) return;

         // Raw Strand
         $this->score = array_sum(array_column($data, 'score'));
         $this->possibleScore = array_sum(array_column($data, 'possibleScore'));

         // Weighted Strand
         $this->adjustedScore = array_sum(array_column($data, 'adjustedScore'));
         $this->adjustedPossible = array_sum(array_column($data, 'adjustedPossible'));

         // Avg Pass Outcome
         $this->passPossible = array_sum(array_column($data, 'passPossible'))/count($data);

         // Score passed
         $this->passed = (int)$this->determinePassOrFail(
             $this->score,
             $this->passPossible,
             $this->possibleScore
         );

         // Weighted passed
         $this->adjustedPassed = (int)$this->determinePassOrFail(
             $this->adjustedScore,
             $this->passPossible,
             $this->adjustedPossible
         );

         return get_object_vars($this);

     }

 }