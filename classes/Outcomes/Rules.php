<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 * Outcomes rules class
 */

 namespace OMIS\Outcomes;


 class Rules
 {

     public $aggregated;
     public $overallGradeWeighted;
     public $maxGrsFail;
     public $minNumberPass;
     public $gradeRule;

     public function __construct(array $rules)
     {

         $this->aggregated = (bool) !$rules['multi_examiner_results_averaged'];
         $this->overallGradeWeighted = (bool) $rules['grade_weightings'];
         $this->maxGrsFail = $rules['max_grs_fail'];
         $this->minNumberPass = $rules['min_stations_to_pass'];
         $this->gradeRule = $rules['grade_rule'];

     }

 }