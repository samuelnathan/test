<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * Outcomes exam level class
  */

 namespace OMIS\Outcomes;


 class ItemOutcome extends Outcome
 {

     public $score = 0.000;
     public $weightedScore = 0.000;

     // Method to determine outcome
     public function determine(array $data, Rules $rules)
     {

         if (empty($data)) return;

         // Raw Strand
         $this->score = (float)array_sum(array_column($data, 'option_value'));

         // Weighted Strand
         $this->weightedScore =(float)array_sum(array_column($data, 'option_weighted_value'));


         return get_object_vars($this);

     }

 }