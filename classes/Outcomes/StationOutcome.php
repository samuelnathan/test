<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * Outcomes station level class
  */

 namespace OMIS\Outcomes;

 class StationOutcome extends Outcome
 {

     public $score = 0.000;
     public $adjustedScore = 0.000;
     public $possibleScore = 0.000;
     public $adjustedPossible = 0.000;
     public $passPossible = 0.000;
     public $passed = 0;
     public $adjustedPassed = 0;

     // Method to determine outcome
     public function determine(array $data, Rules $rules)
     {

         if (empty($data['forms'])) return;

         $forms = $data['forms'];
         $examPossible = $data['examPossible'];

         // Determine examiner scores + finalise weightings
         $scoreData = $this->examinerScores($forms, $data['examiners']);

         // Raw Strand
         list($this->score, $this->possibleScore) = $this->determineScoreAndPossible(
                $rules->aggregated, false, $scoreData['scores']
         );

         // Weighted Strand
         list($this->adjustedScore, $this->adjustedPossible) = $this->determineScoreAndPossible(
               $rules->aggregated, true, $scoreData['scoresWeighted']
         );

         // Station weighted value
         $weightingValue = (array_sum(array_column($forms, 'weighting')));

         // If we have station weighting value then apply weightings
         if ($weightingValue > 0)
             $this->adjustedScore = ((($this->adjustedScore/$this->adjustedPossible) * $weightingValue)/100) * $examPossible;

         // Avg Pass Outcome
         $this->passPossible = array_sum(array_column($forms, 'pass_value'))/count($forms);

         // Score strand passed
         $this->passed = (int) $this->determinePassOrFail(
             $this->score,
             $this->passPossible,
             $this->possibleScore
         );

         // Weighted strand passed
         $this->adjustedPassed = (int) $this->determinePassOrFail(
             $this->adjustedScore,
             $this->passPossible,
             $this->adjustedPossible
         );

         return get_object_vars($this);

     }

     // Method to gather examiner [weighted] scores
     private function examinerScores(array $forms, array $examiners)
     {

        $scores = $weighted = [];

        foreach ($forms as $form) {

            $stationID = $form['station_id'];
            $examinerID = $form['examiner_id'];

            if (isset($examiners[$stationID][$examinerID])) {

                $weighting = (int)$examiners[$stationID][$examinerID];

                // Exclude observers
                if ($weighting == \OMIS\Database\Exams::EXAMINER_WEIGHTING_OBSERVER)
                    continue;

            } else {

                $weighting = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;

            }

            // Observer (0) weighting
            $scores[$examinerID] = [
                'value' => (float)$form['score'],
                'possible' => (float)$form['possible'],
                'weighting' => $weighting
            ];

            // With existing weightings
            $weighted[$examinerID] = [
                'value' => (float)$form['weighted_score'],
                'possible' => (float)$form['weighted_possible'],
                'weighting' => $weighting
            ];

        }

        return [
            'scores' => $scores,
            'scoresWeighted' => $weighted
        ];

     }

 }
