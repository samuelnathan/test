<?php
namespace OMIS\Outcomes;


class FormOutcome extends Outcome
{
    public $score = 0.000;
    public $possibleScore = 0.000;
    public $passPossible = 0.000;
    public $passed = 0;


/**
     * {@inheritDoc}
     * @see \OMIS\Outcomes\Outcome::determine()
     */
    public function determine(array $data, Rules $rules)
    {

        $this->score = (float) $data[0];

        $this->possibleScore = $data[1];

        $this->passPossible = $data[2];

        // Score passed
        $this->passed = (int)$this->determinePassOrFail(
            $this->score,
            $this->passPossible,
            $this->possibleScore
            );
        return $this->passed;
    }




}