<?php
 namespace OMIS;
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2016, Qpercom Limited
  */
 
 
 class Release 
 {
   
    private static $_instance = null;
    public static $version;
    public static $date;
    public static $build;
    
  
    /**
     * Set version details of the app from <root>/version.json
     */
    private function __construct()
    {
        
        if (file_exists('version.json')) {
          
            list(self::$version, self::$date, self::$build) = array_values(
               json_decode(
                 file_get_contents('version.json'),
                 true
               )
            );
            
        } else {
          
           error_log(__METHOD__ . ": Unable to open version.json to get app version");
           self::$version = "?";
           self::$date = "?";
           self::$build = "?";
            
        }
        
    }
    
    /**
     * Get class instance, instantiate if required
     * @return object      class instance
     */
    public static function getInstance()
    {
        
       if (self::$_instance === null) {
         
          self::$_instance = new Release();
          
       }
       
       return self::$_instance;
      
    }
    
 }
 