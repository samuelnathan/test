<?php
namespace OMIS\Miscellaneous;
use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Auth\Role as Role;

/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

class Assessment
{
    private $db;

    const INCLUDE_OBSERVER_RECORDS = true;
   
    /**
     * Constructor
     */
    public function __construct()
    {   
       $this->db = \OMIS\Database\CoreDB::getInstance();
    }

    /**
     * Get assessment (live) departments by term ID
     *
     * @param string $term   identifier number for term
     * @return mixed[]|empty[]
     */
    public function departments($term)
    {

        // Get all departments with live exams
        $deptsLive = $this->db->exams->getDepartmentsWithLiveExams($term);

        // Get departments by admin or examiner roles
        if (in_array($_SESSION['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {

            $deptsSource = \OMIS\Database\CoreDB::into_array(
                $this->db->exams->getList(
                    null, $term, 'exams.dept_id'
                )
            );

        // Get departments for school admin
        } else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {

            $adminSchools = $this->db->schools->getAdminSchools(
                $_SESSION['user_identifier']
            );

            $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
            $deptsSource = \OMIS\Database\CoreDB::into_array(
                $this->db->exams->getListFiltered(
                    $term, $schoolIDs,
                    null, 'exams.dept_id'
                )
            );

        // All departments for all other roles
        } else {

            $deptsSource = $this->db->departments->getExaminerDepartments(
                $_SESSION['user_identifier'],
                null, RETURN_AS_ARRAY
            );

        }

        // Only get departments that have live exams
        return array_filter($deptsSource, function ($each) use ($deptsLive) {
            return in_array($each['dept_id'], $deptsLive);
        });

    }
       
    /**
     * Get assessment groups by session ID
     * 
     * @param string $sessionID   identifier number for session
     * @return mixed[]|empty[]     
     */
    public function groups($sessionID)
    {
        $groupRecords = $this->db->exams->getSessionGroups($sessionID);

        // Assemble a list of the groups to display...
        $groups = [];
        if (mysqli_num_rows($groupRecords) > 0) {
            while ($group = $this->db->fetch_row($groupRecords)) {
                $groupID = $group['group_id'];
                $studentCounts = $this->db->exams->getGroupSize($groupID, true, true);
                $newGroup = [
                    'id' => $groupID,
                    'name' => $group['group_name'],
                    'studentCount' => $this->db->exams->getGroupSize($groupID)
                ];
                
                // This formats the student count based on if there are blanks or not.
                if ($studentCounts['blanks'] == 0) {
                    $newGroup['studentCount'] = $studentCounts['total'];
                } else {
                    $newGroup['studentCount'] = sprintf(
                        "%d = %d + %d blank",
                        $studentCounts['total'],
                        $studentCounts['actual'],
                        $studentCounts['blanks']
                    );
                }
                
                $groups[] = $newGroup;
            }
        }
     return $groups;
   }

  /**
   * Get assessment stations by session ID
   * 
   * @param string $sessionID   identifier number for session
   * @return mixed[]|empty[]     
   */
   public function stations($sessionID)
   {
     
        // We have an empty session id, cannot continue any further
        if (empty($sessionID)) {
           return [];
        }
     
        /* This will contain the list of stations the currently logged in user is 
        * allowed to see
        */
        $stationGroupsPermitted = [];

        /* This is the list of stations in the database logged against the current 
        * session ID.
        */
        $stationGroups = $this->db->exams->getStations([$sessionID], true);

        // Current examiner ID (exam as feature, exam admin level)
        if (isset($_SESSION['station_examiner'])) {
            $examinerID = $_SESSION['station_examiner'];
            $userResult = $this->db->users->getUser($examinerID);
            $user = $this->db->fetch_row($userResult);
            $examinerRole = (int) $user['user_role'];
            
        // Logged in examiner
        } else {
            $examinerID = $_SESSION['user_identifier'];
            $examinerRole = (int) $_SESSION['user_role'];
        }

        /* Filter the stations to see which this examiner is allowed select. If the
        * Session Station has no assigned examiners, then it's considered "open" and
        * anyone in that department can examine it; if it has assigned examiners, only
        * those assigned examiners can select that station.
        */
        $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
        $userAssignedStation = $this->db->exams->isUserAssignedSession(
                $examinerID, 
                $sessionID
        );

        if (!$adminRoles->containsRole($_SESSION['user_role']) || $userAssignedStation) {
            // This is the norm, where the user is NOT a super admin.
            // Iterate through the station groups...
            foreach ($stationGroups as $stationGroupID => $stations) {

                // Iterate through the stations in the group
                foreach ($stations as $station) {

                    $stationID = $station['station_id'];

                    $examinersRequired = $this->db->exams->getExaminersRequired(
                        $stationID,
                        self::INCLUDE_OBSERVER_RECORDS
                    );

                    /* Since we need some specific quantity, then we need to check
                    * if the current user is one of the permitted few...
                    */
                    $assignedExaminers = $this->db->exams->getStationExaminers($stationID);
                    $examinerCount = count($assignedExaminers);

                    if ($examinerCount > 0 && $examinersRequired == $examinerCount && !in_array($examinerID, $assignedExaminers)) {

                        /* Currently logged in user is not on the examiner list.
                        * Don't let them choose this station...
                        */
                        continue;

                    }

                    /* If we get to here, then the station is available for selection.
                    * Add it to the appropriate group for rendering in the template.
                    */
                    if (isset($stationGroupsPermitted[$stationGroupID])) {

                        $stationGroupsPermitted[$stationGroupID][] = $station;

                    } else {

                        $stationGroupsPermitted[$stationGroupID] = [$station];

                    }

                }
            }
            
        } else {
            // User is an exam admin, they should be able to see all stations.
            $stationGroupsPermitted = $stationGroups;
        }
        return $stationGroupsPermitted;
  }

}
