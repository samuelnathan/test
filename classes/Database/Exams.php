<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  * Database function calls relating to Exams, Exam Sessions, Groups, Stations
  * Examiners and Assistants
  */

 namespace OMIS\Database;

 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Auth\Role as Role;
 use OMIS\Database\DAO\AbstractEntityDAO;

 include_once __DIR__ . '/../../extra/usefulphpfunc.php';

 class Exams extends AbstractEntityDAO
 {
     /* Constants defining the weighting to use if we don't have one already
      * defined, and for observers...
      */
     const EXAMINER_WEIGHTING_OBSERVER = 0;
     const EXAMINER_WEIGHTING_LOW = 1;
     const EXAMINER_WEIGHTING_REGULAR = 2;
     const EXAMINER_WEIGHTING_HIGH = 3;

     /* Set the default weighting to assign when we don't know what the weighting
      * should be.
      */
     const EXAMINER_WEIGHTING_DEFAULT = self::EXAMINER_WEIGHTING_REGULAR;

     /* Constants to determine how the getExaminerWeightings() method should
      * organize the data it returns to the user.
      */
     const EXAMINER_WEIGHTING_GROUP_EXAMINER = 1;
     const EXAMINER_WEIGHTING_GROUP_STATION = 2;

     /**
      * Minimum and maximum number of examiners per station
      */
     const MINIMUM_EXAMINERS_STATION = 1;
     const MAXIMUM_EXAMINERS_STATION = 30;

     /**
      * Include observer records
      */
     const INCLUDE_OBSERVER_RECORDS = true;

     protected function getTableName() { return 'exams'; }

     protected function getIds() { return ['exam_id']; }

     //Is there an Exam today [Updated 03/02/2014]
     public function examOnDate($date)
     {
         $sql = "SELECT COUNT(*) FROM exam_sessions WHERE session_date = '"
             . $this->db->clean($date) . "'";

         return (CoreDB::single_result($this->db->query($sql)) > 0);
     }

     /**
      * Get the details for a specific exam (optionally including department info)
      * as an associative array.
      *
      * @param int  $exam_id          ID number of exam
      * @param bool $show_departments If TRUE, include relevant department data
      * @return mixed[]|null An associative array of data if the query succeeds.
      * If not, NULL.
      */
     public function getExam($exam_id, $show_departments = false)
     {
         $exam_id = $this->db->clean($exam_id);

         if ($show_departments) {
             $department_join = "INNER JOIN departments USING(dept_id) ";
         } else {
             $department_join = "";
         }

         $query = "SELECT * FROM exams " . $department_join . "WHERE exam_id = '"
             . $exam_id . "'";

         return $this->db->fetch_row($this->db->query($query));
     }

     /**
      * Get examination sessions as an associative array.
      *
      * @param int  $examID          ID number of exam
      * @param bool $toArray         If TRUE, convert to array
      * @param string $active        Only active sessions (optional)
      * @return mixed[]|null An associative array of data if the query succeeds. If not, NULL.
      */
     public function getExamSessions($examID, $toArray = false, $active = null)
     {

         $activeSql = ($active != null) ?
         " AND session_published = '" . $this->db->clean($active) . "'" : "";

         $examIDSan = $this->db->clean($examID);
         $sql = "SELECT * FROM exam_sessions WHERE exam_id = '$examIDSan' "
              . "$activeSql ORDER BY session_date, session_id";

         $resultSet = $this->db->query($sql);
         if ($toArray) {
             return $this->db->into_array($resultSet);
         } else {
             return $resultSet;
         }

     }

     /**
      * Update exam
      *
      * @param int  $exam_id - ID of exam
      * @param string  $exam_name - Name of exam
      * @param string  $exam_description - Description of exam
      * @param int $exam_published - Can examiners access exam (1=true,0=false)
      * @param int $view_results - Allow examiners to view results during exam (1=true,0=false)
      * @param int $view_marks - Allow examiners to view marks on forms during exam (1=true,0=false)
      * @param int $view_totals - Allow examiners to view overall totals on forms during exam (1=true,0=false)
      * @param int $submit_next - Allow examiners to use the submit & next feature on each form (1=true,0=false)
      * @param int $show_countdown - Show countdown timer in the assessment form (1=true,0=false)
      * @param int $default_examiners - Default number of examiners for all stations in this exam (1-4)
      * @param int $grade  default = 1
      * @return true|false If query fails then bool false is returned.
      */
     public function updateExam($id, $name, $description, $settings)
     {

         $id = $this->db->clean($id);
         $name = $this->db->clean($name);
         $description = $this->db->clean($description);

         $san = array_map(
             "\OMIS\Database\CoreDB::clean",
             array_values($settings)
         );

         list($published, $results, $marks, $totals,
              $next, $countdown, $examiners, $grade) = $san;

         // Set the default number of examiners per station in this exam
         if (self::MINIMUM_EXAMINERS_STATION <= $examiners && $examiners <= self::MAXIMUM_EXAMINERS_STATION) {

             $number = $examiners;

         } else {

             $number = self::MINIMUM_EXAMINERS_STATION;

         }

         $sql = "UPDATE exams SET exam_name = '$name', exam_description = '$description', 
                 exam_published = '$published', view_results = '$results', view_marks = '$marks', 
                 view_totals = '$totals', submit_next = '$next', show_countdown = '$countdown', 
                 default_examiners_station = '$number', grade_rule = '$grade' 
                 WHERE exam_id = '$id'";

         return $this->db->query($sql);

     }

     /**
      * Get student exam numbers not in current session
      * @param int $examID      identifier number for exam
      * @param int $sessionID   identifier number for session
      * @return array           list of exam numbers taken
      */
     public function getExamNumbersNotSession($examID, $sessionID) {
        $sql = "SELECT DISTINCT student_exam_number "
             . "FROM (group_students "
             . "INNER JOIN session_groups USING(group_id)) "
             . "INNER JOIN exam_sessions USING(session_id) "
             . "WHERE exam_id = " . $this->db->clean($examID) . " "
             . "AND session_id <> " . $this->db->clean($sessionID);

        return CoreDB::into_values($this->db->query($sql));
     }

     /**
      * Insert exam
      *
      * @param string $dept_id - Department that the exam should be attached to
      * @param string $term_id - Term that the exam should be attached to
      * @param string  $exam_name - Name of exam
      * @param string  $exam_description - Description of exam
      * @param int $exam_published - Can examiners access exam (1=true,0=false)
      * @param int $view_results - Allow examiners to view results during exam (1=true,0=false)
      * @param int $view_marks - Allow examiners to view marks on forms during exam (1=true,0=false)
      * @param int $view_totals - Allow examiners to view overall totals on forms during exam (1=true,0=false)
      * @param int $submit_next - Allow examiners to use the submit & next feature on each form (1=true,0=false)
      * @param int $show_countdown - Show countdown timer on the assessment form (1=true,0=false)
      * @param int $default_examiners - Default number of examiners for all stations in this exam (1-4)
      * @param int $grade  default = 1
      *
      * @return resultset|false If query fails then bool false is returned.
      */
     public function insertExam($dept, $term, $name, $description, $settings)
     { 
      
         $name = $this->db->clean($name);
         $description = $this->db->clean($description);

         $san = array_map(
             "\OMIS\Database\CoreDB::clean",
             array_values($settings)
         );

         list($published, $results, $marks, $totals,
             $next, $countdown, $examiners, $grade) = $san;

         // Set the default number of examiners per station in this exam
         if (self::MINIMUM_EXAMINERS_STATION <= $examiners && $examiners <= self::MAXIMUM_EXAMINERS_STATION) {

            $number = $examiners;

         } else {

            $number = self::MINIMUM_EXAMINERS_STATION;

         }

         $dept = $this->db->clean($dept);
         $term = $this->db->clean($term);

         $sql =
         "INSERT INTO exams (exam_name, exam_description, exam_published, 
          view_results, view_marks, view_totals, submit_next, show_countdown, 
          default_examiners_station, dept_id, term_id, grade_rule) VALUES 
          ('$name', '$description', '$published', '$results', '$marks', '$totals', 
          '$next', '$countdown', '$number', '$dept', '$term', '$grade')";

          // return $sql;
         return $this->db->query($sql);

     }

     // Delete Exams [Updated 26/05/2014]
     public function deleteExams($toDelete)
     {
         global $config;
         $count = count($toDelete);
         $deleteCount = 0;
         $index = 0;

         while ($index < $count) {
             $exam_id = $this->db->clean($toDelete[$index]);
             $index++;

             if ($this->db->results->doesExamHaveResults($exam_id)) {
                 setA($_SESSION, 'exams_with_results_warning', array());
                 $_SESSION['exams_with_results_warning'][] = $exam_id;
                 continue;
             }

             // Delete Exam Sessions
             $examSessionsDB = $this->getExamSessions($exam_id, true);
             $sessionsToDelete = array_column($examSessionsDB, 'session_id');
             $this->db->sessions->deleteSessions($sessionsToDelete);

             $sql = "DELETE FROM exams WHERE exam_id = '" . $this->db->clean($exam_id) . "'";

             if ($this->db->query($sql)) {
                 \Analog::info(
                     $_SESSION['user_identifier'] . " deleted exams with identifier "
                     . "$exam_id from " . $config->getName()
                 );
                 $deleteCount++;
             }
         }
         return $deleteCount;
     }

     /**
      * Checks if an exam with the given ID exists.
      *
      * @deprecated Prefer to use existsById standard function
      * @param int $exam_id ID number of exam
      * @return bool TRUE if exam exists, FALSE if not.
      */
     public function doesExamExist($exam_id) { return $this->existsById($exam_id); }

     /**
      * Does exam contain student
      * @param int $examID ID number of exam
      * @param string $studentID ID of student
      * @return boolean true|false
      */
     public function doesExamContainStudent($examID, $studentID)
     {
         $studentIDSanitized = $this->db->clean($studentID);
         $examIDSanitized = $this->db->clean($examID);
         $sql = "SELECT COUNT(*) "
              . "FROM (group_students "
              . "INNER JOIN session_groups USING(group_id)) "
              . "INNER JOIN exam_sessions USING(session_id) "
              . "WHERE student_id = '$studentIDSanitized' "
              . "AND exam_id = '$examIDSanitized'";
         $count = CoreDB::single_result($this->db->query($sql));
         return ($count > 0);
     }


     /**
      * Get all of exams, filterable by departments and/or term,
      * and sortable.
      *
      * @param mixed[] $depts        Array of departments to filter by
      * @param string $term          ID number of term of the field used to index the result array
      * @param string $groupBy       The database field used to group related results
      * @param string $active        Only active exams
      * @return bool|mysqli_result
      */
     public function getList($depts = null, $term = null, $groupBy = 'exam_id', $active = null)
     {

         // Basic checks
         if ($depts !== null && !is_array($depts)) {
            return [];
         }

         if ($term !== null && !$this->db->academicterms->existsById($term)) {
            return [];
         }

         // Define variables
         $deptSql = $termSql = $activeSql = '';
         $sanDepartmentIDs = [];

         // Dept ID's Filtering, clean array elements and create sql string
         if (is_array($depts) && count($depts) > 0) {
             for ($i = 0; $i < count($depts); $i++) {
                 if ($this->db->departments->existsById($depts[$i])) {
                   $sanDepartmentIDs[] = $this->db->clean($depts[$i]);
                 }
             }
         }

         if (!empty($sanDepartmentIDs)) {
             $deptSql = " AND dept_id IN ('" . implode("', '", $sanDepartmentIDs) . "')";
         }

         // Term query
         if ($term !== null) {
             $termSql = " AND term_id = '" . $this->db->clean($term) . "'";
         }

         // Active query
         if ($active != null) {
             $activeSql = " AND exam_published = '" . $this->db->clean($active) . "'";
         }

         // Main query
         $sql = "SELECT exams.*, departments.* "
              . "FROM exams INNER JOIN departments USING(dept_id) "
              . "WHERE 1 $deptSql$termSql$activeSql "
              . "GROUP BY $groupBy "
              . "ORDER BY departments.dept_name";

         return $this->db->query($sql);

     }

      /**
      * Get all of exams, filterable term by schools and/or departments
      * @param string $term        Filter by term (optional)
      * @param int[] $schools      Array of schools to filter by (optional)
      * @param string[] $depts     Array of departments to filter by (optional)
      * @param string $groupBy     The database field used to group related results (optional)
      * @return bool|mysqli_result
      */
     public function getListFiltered($term = null, $schools = null, $depts = null, $groupBy = 'exam_id')
     {

         $sanSchools = $sanDepts = [];
         $schoolsSql = $deptsSql = "";

         /*
          * Schools Filtering
          */
         if (is_array($schools) && count($schools) > 0) {

             foreach ($schools as $id) {

                 if ($this->db->schools->doesSchoolExist($id)) {
                     $sanSchools[] = $this->db->clean($id);
                 }

             }

             $schoolsSql = " AND school_id IN (" . implode(",", $sanSchools) . ")";

         }

         /*
          * Departments Filtering
          */
         if (is_array($depts) && count($depts) > 0) {

             foreach ($depts as $id) {

                 if ($this->db->departments->existsById($id)) {
                     $sanDepts[] = $this->db->clean($id);
                 }

             }

             $deptsSql = " AND dept_id IN ('" . implode("','", $sanDepts) . "')";

         }

         /**
          * Term filtering
          */
         $termSql = ($term !== null) ?
         " AND term_id = '" . $this->db->clean($term) . "'" : '';

         $sql = "SELECT * "
              . "FROM exams INNER JOIN departments USING (dept_id) "
              . "INNER JOIN schools USING(school_id) "
              . "WHERE 1$termSql$schoolsSql$deptsSql "
              . "GROUP BY $groupBy "
              . "ORDER BY schools.school_description, "
              . "departments.dept_name";

         return $this->db->query($sql);

     }

     /* Get list of departments with live exams
      * @param string $term_id Academic Term ID
      *
      * @return array returns array of live departments
      */
     public function getDepartmentsWithLiveExams($term_id)
     {
         $term_id = $this->db->clean($term_id);

         $sql = "SELECT DISTINCT(dept_id) as live_department "
               ."FROM exams "
               ."WHERE exam_published = 1 AND term_id = '". $term_id ."'";

         return CoreDB::into_values($this->db->query($sql));
     }

     /**
      * Get Session Record
      * @deprecated Prefer to use directly $db->sessions->getSession
      */
     public function getSession($sessionID) { return $this->db->sessions->getSession($sessionID); }

     /**
      * @deprecated Prefer to use directly $db->sessions->getSessionIDByStationID
      * @param $station_id
      * @return mixed
      */
     public function getSessionIDByStationID($station_id) { return $this->db->sessions->getSessionIDByStationID($station_id); }

     /**
      * @deprecated Prefer to use directly $db->sessions->doesSessionExist
      * @param $session_id
      * @return bool
      */
     public function doesSessionExist($session_id) { return $this->db->sessions->doesSessionExist($session_id); }

     /**
      * @deprecated Prefer to use directly $db->sessions->getStudentSession
      * @param $student_id
      * @param $exam_id
      * @return bool|\mysqli_result
      */
     public function getStudentSession($student_id, $exam_id) { return $this->db->sessions->getStudentSession($student_id, $exam_id); }


     /**
      * Get the Exam ID that is associated with a specific Session
      *
      * @deprecated Prefer to use directly $db->sessions->getSessionExamID
      * @param int $session_id Session ID to search for
      * @return type
      */
     public function getSessionExamID($session_id) { return $this->db->sessions->getSessionExamID($session_id); }

     /**
      * Get the database record for a specific station.
      *
      * @deprecated Prefer to use directly $db->stations->getStation
      * @param int  $id     ID number of station
      * @param bool $array If TRUE, casts the mysql_result to an array.
      * @return bool|mixed|\mysqli_result FALSE if the station doesn't exist.
      *          If there is a station and $toArray is TRUE, an associative
      *          array with the station's attributes or otherwise a mysqli_result
      *          object.
      */
     public function getStation($id, $array = true) { return $this->db->stations->getStation($id, $array); }

     /**
      * Get Exam Stations By Sessions
      *
      * @deprecated Prefer to use directly $db->stations->getStations
      * @param int[] $sessions Array of session IDs to search for
      * @param bool $array If TRUE, return results as an array, otherwise a mysqli_result.
      * @param string $filter (Optional) Extra field name that can be used to further group results
      * @param string $order (Optional) Name of field to order results by
      * @param bool $rests If TRUE, include rest station data.
      * @return mixed[]|\mysqli_result Depends on value of $array.
      */
     public function getStations($sessions, $array = false, $filter = 'station_number', $order = 'form_name', $rests = false) {
         return $this->db->stations->getStations($sessions, $array, $filter, $order, $rests);
     }

     /**
      * Switch exam form
      *
      * @param int $id      Number identifier of the exam
      * @param int $orgFormID   Number identifier of original form ID
      * @param int $newFormID   Number identifier of new form ID
      * @return bool|status   Switched (true|false)
      */
     public function switchExamForm($id, $orgFormID, $newFormID)
     {

         /*
          * Need to get sessions first not to upset triggers
          * when updating same table
          */
         $sessions = CoreDB::into_values(
           $this->db->query(
               "SELECT session_id FROM exam_sessions 
                WHERE exam_id = '" . $this->db->clean($id) . "'"
           )
         );

         if (empty($sessions)) {

             return false;

         }

         return $this->db->query(
           "UPDATE session_stations 
            SET form_id = '" . $this->db->clean($newFormID) . "' 
            WHERE session_id IN (" . implode(",", $sessions) . ") 
            AND form_id = '" . $this->db->clean($orgFormID) . "'"
         );

     }

     /**
      * Delete session stations
      * @deprecated Prefer to use directly $db->stations->deleteStations
      * @param type $toDelete       stations to delete
      * @param type $sessionID     ID number for exam session
      * @return int count          number of stations deleted
      */
     public function deleteStations($toDelete, $sessionID) { return $this->db->stations->deleteStations($toDelete, $sessionID); }

     /**
      * @deprecated Prefer to use directly $db->stations->getStationsBySessionID
      * @param $station_key
      * @param $session_id
      * @return mixed
      */
     public function getStationsBySessionID($station_key, $session_id) {
         return $this->db->stations->getStationsBySessionID($station_key, $session_id);
     }

     //get Student Count In Session [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->sessions->getStudentCountInSession
      * @param $session_id
      * @return mixed
      */
     public function getStudentCountInSession($session_id) { return $this->db->sessions->getStudentCountInSession($session_id); }

     /**
      * Get stations by exam
      *
      * @deprecated Prefer to use directly $db->stations->getStationsByExam
      * @param string $examID             id number for the exam
      * @param array $excludeStationNums  station numbers to exclude (optional)
      * @return array                     list of unique station records
      */
     public function getStationsByExam($examID, $excludeStationNums = []) { return $this->db->stations->getStationsByExam($examID, $excludeStationNums); }


     //Does Session Station Exist [Updated 03/02/2014]
     /**
      * @deprecated Prefer to use directly $db->stations->doesStationExist
      * @param $station_id
      * @param null $session_id
      * @return bool
      */
     public function doesStationExist($station_id, $session_id = null) { return $this->db->stations->doesStationExist($station_id, $session_id); }

     /**
      * Is station multi-scenarion
      * Note: only 1 scenario result can be submitted per station
      *
      * @deprecated Prefer to use directly $db->stations->multiScenarioStation
      * @param int $stationID      ID number of session_stations
      * @return bool true|false
      */
     public function multiScenarioStation($stationID) { return $this->db->stations->multiScenarioStation($stationID); }

     /**
      * Get station scenarios
      *
      * @deprecated Prefer to use directly $db->stations->getStationScenarios
      * @param int $stationID  id number for the station
      * @param int $sessionID  id number for the session
      * @return array          array of station scenarios
      */
     public function getStationScenarios($stationID, $sessionID) { return $this->db->stations->getStationScenarios($stationID, $sessionID); }

     //Does Session have Stations [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->sessions->doesSessionHaveStations
      * @param $session_id
      * @param bool $return_count
      * @return bool|mixed
      */
     public function doesSessionHaveStations($session_id, $return_count = false)
     {
         return $this->db->sessions->doesSessionHaveStations($session_id, $return_count);
     }

     //Get Highest Station Number [Updated 26/01/2014]
     /**
      * @deprecated Prefer to use directly $db->stations->getHighestStationNumber
      * @param $session_id
      * @return mixed
      */
     public function getHighestStationNumber($session_id) { return $this->db->stations->getHighestStationNumber($session_id); }

     //Get Lowest Station Number [Updated 04/02/2014]
     /**
      * @deprecated Prefer to use directly $db->stations->getLowestStationNum
      * @param $session_id
      * @return mixed
      */
     public function getLowestStationNum($session_id) { return $this->db->stations->getLowestStationNum($session_id); }

     //Get Group Details [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->groups->getGroup
      * @param $group_id
      * @return mixed
      */
     public function getGroup($group_id) { return $this->db->groups->getGroup($group_id); }

     /**
      * Get a list of sessions records by session_id
      *
      * @deprecated Prefer to use directly $db->groups->getSessionGroups
      * @param mixed[]|null $sessionID ID number of the session
      * @param boolean $asArray        convert mysqliresultset into an array
      * @return mixed[]|\mysqli_result|false
      */
     public function getSessionGroups($sessionID, $asArray = false)
     {
         return $this->db->groups->getSessionGroups($sessionID, $asArray);
     }

     /**
      * Checks if a student is a member of a particular student group.
      *
      * @deprecated Prefer to use directly $db->groups->doesStudentExistInGroup
      * @param string $student_id Student ID number
      * @param int $group_id Group ID number
      * @return bool TRUE if student is a member of specified group otherwise FALSE.
      */
     public function doesStudentExistInGroup($student_id, $group_id) { return $this->db->groups->doesStudentExistInGroup($student_id, $group_id); }

     /**
      * Return the (non-blank) students in a given group/session pairing.
      *
      * @deprecated Prefer to use directly $db->groups->getStudents
      * @param mixed $groupIDs      One or more group IDs (scalar or array) or NULL
      *                             to return all groups.
      * @param mixed $sessionID     Either a session ID or null
      * @param bool  $returnArray   If TRUE, return an array, otherwise a mysqli_result
      * @param string  $termID      Term identifier (optional)
      * @return mixed               Either an array or a mysqli_result depending on $returnArray
      */
     public function getStudents($groupIDs = null, $sessionID = null, $returnArray = true, $termID = null)
     {
         return $this->db->groups->getStudents($groupIDs, $sessionID, $returnArray, $termID);
     }

     /**
      * Get student group record
      *
      * @deprecated Prefer to use directly $db->groups->getStudentGroupRecord
      * @param string $studentID    number to identifier the student
      * @param int $groupID         number to identifier the group
      * @return mixed[]
      */
     public function getStudentGroupRecord($studentID, $groupID) { return $this->db->groups->getStudentGroupRecord($studentID, $groupID); }

     /**
      * Get students not in session groups
      *
      * @deprecated Prefer to use directly $db->groups->getStudentsNotInGroups
      * @param int $sessionID
      * @param string $moduleID
      * @param int $yearID
      * @return bool true|false
      */
     public function getStudentsNotInGroups($sessionID, $moduleID, $yearID)
     {
         return $this->db->groups->getStudentsNotInGroups($sessionID, $moduleID, $yearID);
     }

     /**
      * @deprecated Prefer to use directly $db->blankStudents->deleteBlanksSubmitted
      * @param $group_id
      * @param $station_number
      * @return bool|\mysqli_result
      */
     public function deleteBlanksSubmitted($group_id, $station_number) { return $this->db->blankStudents->deleteBlanksSubmitted($group_id, $station_number); }

     /**
      * Return any blank students for a given student group and station number.
      *
      * @deprecated Prefer to use directly $db->blankStudents->getUnisedStudentBlanks
      * @param int  $station_number  Number of station (can be shared among stations to allow multiple scenarios)
      * @param int  $group_id     ID number of student group
      * @param bool $convertArray if TRUE, return results as an array; otherwise, a mysqli_result object.
      * @return mixed Depends on $convertArray and if there are matching records or not.
      */
     public function getUnusedStudentBlanks($station_number, $group_id, $convertArray = false)
     {
         return $this->db->blankStudents->getUnusedStudentBlanks($station_number, $group_id, $convertArray);
     }

     //Ignore Blank Student [Updated 04/02/2014]
     /**
      * @deprecated Prefer to use directly $db->blankStudents->ignoreBlankStudent
      * @param $blank_student_id
      * @param $station_number
      * @return bool|\mysqli_result
      */
     public function ignoreBlankStudent($blank_student_id, $station_number)
     {
         return $this->db->blankStudents->ignoreBlankStudent($blank_student_id, $station_number);
     }

     /**
      * Check if a station is a rest station
      *
      * @deprecated Prefer to use directly $db->stations->isRestStation
      * @param int $station_id Station ID to check
      * @return bool TRUE if the station is a rest station, FALSE otherwise.
      */
     public function isRestStation($station_id) { return $this->db->stations->isRestStation($station_id); }

     /**
      * Get students & blanks, one array
      * @deprecated Prefer to use directly $db->groups->getStudentsAndBlanks
      * @param int $groupID
      * @param string $termID
      * @return students[]
      */
     public function getStudentsAndBlanks($groupID, $termID = null)
     {
         return $this->db->groups->getStudentsAndBlanks($groupID, $termID);
     }

     //Get Blank Students [Updated 04/02/2014]
     /**
      * @deprecated Prefer to use directly $db->blankStudents->getBlankStudents
      * @param $group_id
      * @return array
      */
     public function getBlankStudents($group_id) { return $this->db->blankStudents->getBlankStudents($group_id); }

     /**
      * Check whether a blank student has been discarded or not
      * @deprecated Prefer to use directly $db->blankStudents->isBlankStudentDiscarded
      * @param $blankStudentId int ID of the blank student
      * @param $stationNum int Station number
      * @return string
      */
     public function isBlankStudentDiscarded($blankStudentId, $stationNum)
     {
         return $this->db->blankStudents->isBlankStudentDiscarded($blankStudentId, $stationNum);
     }

     //get Group count [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->groups->getSessionGroupCount
      * @param $session_id
      * @return mixed
      */
     public function getSessionGroupCount($session_id) { return $this->db->groups->getSessionGroupCount($session_id); }

     //Does Session have Groups [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->sessions->doesSessionHaveGroups
      * @param $session_id
      * @return bool
      */
     public function doesSessionHaveGroups($session_id) { return $this->db->sessions->doesSessionHaveGroups($session_id); }

     /**
      * Count the number of students in a particular student group.
      *
      * @deprecated Prefer to use directly $db->groups->getGroupSize
      * @param int  $group_id       ID number of student group
      * @param bool $include_blanks If TRUE, include blank students in counts (if
      *                             used with $detailed = TRUE, then actual and
      *                             blank student coutns are included separately
      *                             in the totals)
      * @param bool $detailed       If TRUE, return results as an associative array.
      * @return int|mixed[]         If $detailed is TRUE, return an array; if not, a single integer.
      */
     public function getGroupSize($group_id, $include_blanks = false, $detailed = false)
     {
         return $this->db->groups->getGroupSize($group_id, $include_blanks, $detailed);
     }

     //Re Order Groups [Updated 05/02/2014]
     /**
      * @deprecated Prefer to use directly $db->groups->reOrderGroups
      * @param $session_id
      */
     public function reOrderGroups($session_id) { $this->db->groups->reOrderGroups($session_id); }

     //Get Session ID By Group ID [Updated 05/02/2014]
     /**
      * @deprecated Prefer to use directly $db->groups->getSessionIdByGroupID
      * @param $group_id
      * @return mixed
      */
     public function getSessionIDByGroupID($group_id) { return $this->db->groups->getSessionIDByGroupID($group_id); }

     //Clone Group Names [Updated 06/02/2014]
     /**
      * @deprecated Prefer to use directly $db->groups->cloneGroupNames
      * @param $new_session_id
      * @param $org_session_id
      */
     public function cloneGroupNames($new_session_id, $org_session_id) { $this->db->groups->cloneGroupNames($new_session_id, $org_session_id); }

     /**
      * Get session assistants
      * @deprecated Prefer to use directly $db->sessionAssistants->getSessionAssistants
      * @param int $id             Identifier of session
      * @param bool $toArray       Convert to array
      * @return mixed[]
      */
     public function getSessionAssistants($id, $toArray = false) { return $this->db->sessionAssistants->getSessionAssistants($id, $toArray); }

     //Attach Assistants to Session [Updated 05/02/2014]
     /**
      * @deprecated Prefer to use directly $db->sessionAssistants->attachAssistantsToSession
      * @param $user_id
      * @param $session_id
      * @return bool|\mysqli_result
      */
     public function attachAssistantsToSession($user_id, $session_id) { return $this->db->sessionAssistants->attachAssistantsToSession($user_id, $session_id); }

     //Get assistants not attached to session [Updated 10/02/2014]
     /**
      * @deprecated Prefer to use directly $db->sessionAssistants->getAssistantsNotInSession
      * @param $session_id
      * @return bool|\mysqli_result
      */
     public function getAssistantsNotInSession($session_id) { return $this->db->sessionAssistants->getAssistantsNotInSession($session_id); }

     //Get Session Assistants Count [Updated 10/02/2014]
     /**
      * @deprecated Prefer to use directly $db->sessionAssistants->getSessionAssistantsCount
      * @param $session_id
      * @return mixed
      */
     public function getSessionAssistantsCount($session_id) { return $this->db->sessionAssistants->getSessionAssistantsCount($session_id); }

     //Does Session have Assistants [Updated 27/01/2014]
     /**
      * @deprecated Prefer to use directly $db->sessionAssistants->doesSessionHaveAssistants
      * @param $session_id
      * @return bool
      */
     public function doesSessionHaveAssistants($session_id) { return $this->db->sessionAssistants->doesSessionHaveAssistants($session_id); }

     //Clone Assistants [Updated 06/02/2014]
     /**
      * @deprecated Prefer to use directly $db->sessionAssistants->cloneAssistants
      * @param $new_session_id
      * @param $org_session_id
      */
     public function cloneAssistants($new_session_id, $org_session_id) { $this->db->sessionAssistants->cloneAssistants($new_session_id, $org_session_id); }

     /**
      * Return a list of examiners that are assigned to a given session_stations (i.e.
      * instance of a station in a specific session of a specific exam). If the
      * list is empty, then this is considered "open" and any examiner in the
      * appropriate department can elect to examine this form.
      *
      * @deprecated Prefer to use directly $db->stationExaminers->getStationExaminers
      * @param int    $station_id ID number of station set
      * @param string $order_by      Table field name to sort results by
      * @return string[] List of examiner IDs allowed to examine station (if assigned)
      */
     public function getStationExaminers($station_id, $order_by = "scoring_weight")
     {
         return $this->db->stationExaminers->getStationExaminers($station_id, $order_by);
     }

     /**
      * Is the user assigned to session
      *
      * @deprecated Prefer to use directly $db->stationExaminers->isUserAssignedSession
      * @param string $userID   Identifier for the user
      * @param int $sessionID   Identifier for the session
      * @return bool            User assigned to station in exam true|false
      */
     public function isUserAssignedSession($userID, $sessionID) { return $this->db->stationExaminers->isUserAssignedSession($userID, $sessionID); }


     /**
      * Get the examiners associated with this station and group
      *
      * @deprecated Prefer to use directly $db->stationExaminers->getStationAssociatedExaminers
      * @param int $stationID   ID number for the station
      * @param int $groupID     ID number for the group
      * @return array[]         List of examiners
      */
     public function getStationAssociatedExaminers($stationID, $groupID)
     {
         return $this->db->stationExaminers->getStationAssociatedExaminers($stationID, $groupID);
     }


     /**
      * Get a list of all of the students for a given session and whether (and in
      * what way) they're available to a specific examiner. Should correctly
      * handle multiple scenarios too.
      *
      * @deprecated Prefer to use directly $db->stationExaminers->getStationExaminerStudents
      * @param int      $stationID       ID number of station
      * @param int      $studentGroupID  ID number of student group
      * @param string   $examinerID      ID number of examiner
      * @return mixed FALSE if no permission, otherwise an associative array.
      */
     public function getStationExaminerStudents($stationID, $studentGroupID, $examinerID)
     {
         return $this->db->stationExaminers->getStationExaminerStudents($stationID, $studentGroupID, $examinerID);
     }

     /**
      * Get the number of examiners required for a given station.
      *
      * @deprecated Prefer to use directly $db->stations->getExaminersRequired
      * @param int  $station_id  ID number of session_stations.
      * @param bool $include_observers   If TRUE, include the number of zero-weighted
      *                                  examiners (i.e. observers) are included
      *                                  in the returned count.
      * @param bool $return_zero If TRUE, returns the number of examiners deemed
      *                          required for this station. If FALSE, it sets 1
      *                          as the effective minimum value it returns.
      *
      * @return int Number of examiners required for this examiner; minimum is 1
      *             if $return_zero is FALSE.
      */
     public function getExaminersRequired($station_id, $include_observers = false, $return_zero = false)
     {
         return $this->db->stations->getExaminersRequired($station_id, $include_observers, $return_zero);
     }

     /**
      * Return the list of examiners assigned to this station.
      *
      * @deprecated Prefer to use directly $db->stationExaminers->getExaminersForStation
      * @param int  $stationID ID number of station.
      * @param bool $asArray   If TRUE, return results as an associative array
      *
      * @return \mysqli_result|mixed[] depends on $asArray
      */
     public function getExaminersForStation($stationID, $asArray = false)
     {
         return $this->db->stationExaminers->getExaminersForStation($stationID, $asArray);
     }

     /**
      * Get exam examiner weightings
      * Return the list of examiners with their weightings for an exam.
      *
      * @deprecated Prefer to use directly $db->stationExaminers->getExaminerWeightings
      * @param int|int[] $stationIds ID number(s) of stations to retrieve
      *                              examiner weightings for
      * @param int (optional) Grouping arrangement - see EXAMINER_WEIGHTING_GROUP_*
      *                       constants. Group by station or examiner.
      * @return mixed[] Associative array of examiner station weights, grouped by
      *                 examiner ID.
      */
     public function getExaminerWeightings($stationIds, $groupBy = self::EXAMINER_WEIGHTING_GROUP_STATION)
     {
         return $this->db->stationExaminers->getExaminerWeightings($stationIds, $groupBy);
     }

     /**
      * @deprecated Prefer to use directly $db->stations->getUniqueStationNumbers
      * Get Unique Station Numbers
      */
     public function getUniqueStationNumbers($exam_id, $session_id = null)
     {
         return $this->db->stations->getUniqueStationNumbers($exam_id, $session_id);
     }

     /**
      * Get unique form identifiers
      * @param int $examID    examination identifier
      * @return int[] array of form identifiers, if query fails then bool false is returned
      */
     public function getUniqueFormIdentifiers($examID)
     {
         $sql = "SELECT DISTINCT form_id "
              . "FROM exam_sessions INNER JOIN session_stations "
              . "USING(session_id) WHERE exam_id = '" . $this->db->clean($examID) . "' "
              . "AND form_id IS NOT NULL "
              . "ORDER BY station_number";

         return CoreDB::into_values($this->db->query($sql));
     }

     //Get Unique Exam Examiner Names [Updated 11/02/2014]
     public function getExamExaminerNames($exam_id)
     {
         $sql = "SELECT DISTINCT examiner_id, surname, forename FROM (((exams "
             . "INNER JOIN exam_sessions USING(exam_id)) "
             . "INNER JOIN session_stations USING(session_id)) "
             . "INNER JOIN student_results USING(station_id)) "
             . "INNER JOIN users ON student_results.examiner_id = users.user_id "
             . "WHERE exam_id = '" . $this->db->clean($exam_id) . "'";

         $mysqliResult = $this->db->query($sql);
         $resultArray = CoreDB::into_array($mysqliResult);
         return CoreDB::group($resultArray, ['examiner_id'], true, true);
     }

     /**
      * Does student result exist
      * @deprecated Prefer to use directly $db->results->existsByStationStudentAndExaminer
      * @param int $station_id
      * @param string $student_id
      * @param string $examiner_id
      * @return bool true|false
      */
     public function doesResultExist($station_id, $student_id, $examiner_id)
     {
         return $this->db->results->existsByStationStudentAndExaminer($station_id, $student_id, $examiner_id);
     }

     /**
      * @deprecated Prefer to use directly $db->weighting->examinerStationWeighting
      * Get current examiner station weighting
      */
     public function examinerStationWeighting($examinerID, $stationID) {
         return $this->db->weighting->examinerStationWeighting($examinerID, $stationID);
     }

     /**
      * Get the list of examiner weightings available to choose from.
      *
      * @deprecated Prefer to use directly $db->weighting->getWeightingList
      * @return mixed[] Mapping of numerical weights to their descriptions.
      */
     public function getWeightingList()
     {
         return $this->db->weighting->getWeightingList();
     }

     /**
      * Get Colour Name
      * @deprecated Prefer to use directly $db->colourMatching->getColourName
      */
     public function getColourName($colour_code) { return $this->db->colourMatching->getColourName($colour_code); }

     /**
      * Get all colour records
      * @deprecated Prefer to use directly $db->colourMatching->getAllCircuitColours
      * @param string $indexField   index field
      * @param array                circuit colour records
      */
     public function getAllCircuitColours($indexField = null) { return $this->db->colourMatching->getAllCircuitColours($indexField); }


    /**
     * Is the exam ready to go
     * 
     * @param int $examID
     */
    public function examComplete($examID) {
        
        $idSan = $this->db->clean($examID);
        $stationsSql = "SELECT COUNT(station_id) " 
                     . "FROM session_stations "
                     . "JOIN exam_sessions USING(session_id) "
                     . "WHERE exam_id = $idSan";
        $studentsSql = "SELECT COUNT(student_id) "
                    . "FROM group_students "
                    . "JOIN session_groups USING (group_id) "
                    . "JOIN exam_sessions USING (session_id) "
                    . "WHERE exam_id = $idSan";
        $stations = CoreDB::single_result($this->db->query($stationsSql));
        $students = CoreDB::single_result($this->db->query($studentsSql));
        
        return ($stations > 0 && $students > 0);
        
     }

    /**
     * Get all the form sections in the sessions and stations of an exam
     * @param $examId
     * @return array
     */
    public function getFormSectionsByExam($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT form_sections.*, competence_type '
               . 'FROM exam_sessions JOIN session_stations JOIN forms JOIN form_sections JOIN competencies '
               . 'ON exam_sessions.session_id = session_stations.session_id AND '
               . '   session_stations.form_id = forms.form_id AND '
               . '   forms.form_id = form_sections.form_id AND '
               . '   form_sections.competence_id = competencies.competence_id '
               . "WHERE exam_sessions.exam_id = $examId";

        return CoreDB::into_array($this->db->query($query));
    }

     /**
     * Get the forms used in an exam that have no sections
     * @param $examId
     * @return array
     */
    public function getEmptyFormsForExam($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT * '
               . 'FROM exam_sessions JOIN session_stations JOIN forms '
               . '  ON exam_sessions.session_id = session_stations.session_id AND '
               . '     session_stations.form_id = forms.form_id '
               . "WHERE exam_sessions.exam_id = $examId AND forms.form_id NOT IN (SELECT form_id FROM form_sections)";

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Get the groups associated to an exam that have no students attached
     *
     * @param $examId
     * @return array
     */
    public function getEmptyGroupsForExam($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT exam_sessions.session_id, session_description, group_id, group_name '
               . 'FROM exam_sessions JOIN session_groups '
               . 'ON exam_sessions.session_id = session_groups.session_id '
               . "WHERE exam_sessions.exam_id = $examId AND session_groups.group_id NOT IN ( "
               . '  SELECT group_students.group_id '
               . '  FROM group_students '
               . '  WHERE group_students.group_id = session_groups.group_id '
               . ')';

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Get the sessions associated to an exam that have no groups attached
     *
     * @param $examId
     * @return array
     */
    public function getSessionsWithoutGroups($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT exam_sessions.session_id, session_description '
               . 'FROM exam_sessions LEFT JOIN session_groups '
               . 'ON exam_sessions.session_id = session_groups.session_id '
               . "WHERE exam_id = $examId AND session_groups.group_id IS NULL";

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Get the sessions associated to an exam that have no stations attached
     *
     * @param $examId
     * @return array
     */
    public function getSessionsWithoutStations($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT exam_sessions.session_id, session_description '
               . 'FROM exam_sessions LEFT JOIN session_stations '
               . 'ON exam_sessions.session_id = session_stations.session_id '
               . "WHERE exam_id = $examId AND session_stations.station_id IS NULL";

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Check if a given exam has any stations with notes
     *
     * @param $examId
     * @return bool|null
     */
    public function examHasFormsWithNotes($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS any_notes '
            . 'FROM exam_sessions JOIN session_stations JOIN forms '
            . 'ON exam_sessions.session_id = session_stations.session_id AND '
            . '   session_stations.form_id = forms.form_id '
            . "WHERE exam_id = $examId AND NOT (form_description IS NULL OR form_description = '')";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        else if ($result === "0") return false;
        else return null;
    }

    /**
     * Check if a given exam has results with multiple examiners
     *
     * @param $examId
     * @return bool|null
     */
    public function hasMultipleExaminersResults($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) '
               . 'FROM ( '
               . '  SELECT student_results.station_id, student_id, COUNT(examiner_id) '
               . '  FROM exam_sessions JOIN session_stations JOIN student_results '
               . '  ON exam_sessions.session_id = session_stations.session_id AND '
               . '     session_stations.station_id = student_results.station_id '
               . "  WHERE exam_id = $examId "
               . '  GROUP BY student_results.station_id, student_id '
               . '  HAVING COUNT(examiner_id) > 1) as with_multi';

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        else if ($result === "0") return false;
        else return null;
    }

    /**
     * Check if an exam has feedback in its results
     *
     * @param $examId
     * @return bool|null
     */
    public function hasFeedback($examId) {
        $examId = $this->db->clean($examId);
        $query = "SELECT IF((SELECT COUNT(*) 
                 FROM exam_sessions JOIN session_stations JOIN student_results JOIN section_feedback 
                 ON exam_sessions.session_id = session_stations.session_id 
                 AND session_stations.station_id = student_results.station_id 
                 AND student_results.result_id = section_feedback.result_id 
                 WHERE exam_sessions.exam_id = $examId 
                 AND NOT (section_feedback.feedback_text IS NULL OR TRIM(section_feedback.feedback_text = '')) 
               ) + (
                SELECT COUNT(*)
                 FROM exam_sessions JOIN session_stations JOIN student_results JOIN item_feedback 
                 ON exam_sessions.session_id = session_stations.session_id 
                 AND session_stations.station_id = student_results.station_id 
                 AND student_results.result_id = item_feedback.result_id 
                 WHERE exam_sessions.exam_id = $examId 
                 AND NOT (item_feedback.feedback_text IS NULL OR TRIM(item_feedback.feedback_text = ''))
               ) > 0, 1, 0) AS has_feedback";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;
    }
    
    /**
     * Check if an exam has results submitted by an observer
     *
     * @param $examId
     * @return bool|null
     */
    public function hasObserverResults($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS has_observer_ratings '
               . 'FROM exam_sessions JOIN session_stations JOIN station_examiners JOIN student_results '
               . 'ON exam_sessions.session_id = session_stations.session_id AND '
               . '   session_stations.station_id = station_examiners.station_id AND '
               . '   station_examiners.examiner_id = student_results.examiner_id '
               . "WHERE exam_sessions.exam_id = $examId AND station_examiners.scoring_weight = " . self::EXAMINER_WEIGHTING_OBSERVER;

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;
    }

    /**
     * Check that the form items used in a given exam have competencies linked
     *
     * @param $examId
     * @return bool|null
     */
    public function hasLinkedCompetencies($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS has_competencies '
               . 'FROM exam_sessions JOIN session_stations JOIN form_sections JOIN section_items JOIN item_competencies '
               . 'ON exam_sessions.session_id = session_stations.session_id AND '
               . '   session_stations.form_id = form_sections.form_id AND '
               . '   form_sections.section_id = section_items.section_id AND '
               . '   section_items.item_id = item_competencies.item_id '
               . "WHERE exam_sessions.exam_id = $examId";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;
    }

    /**
     * Check if any section in the forms used in a given exam are Global Rating Scale sections
     *
     * @param $examId
     * @return bool|null
     */
    public function hasGRSSection($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS has_grs '
               . 'FROM exam_sessions JOIN session_stations JOIN form_sections JOIN competencies '
               .  'ON exam_sessions.session_id = session_stations.session_id AND '
               .  '   session_stations.form_id = form_sections.form_id AND '
               .  '   form_sections.competence_id = competencies.competence_id '
               .  "WHERE exam_sessions.exam_id = $examId AND competencies.competence_type = 'scale'";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;
    }

    /**
    * Get the start and end times for an exam. Calculates the earlies start for
    * its sessions and the latest end for its sessions
    * @param int $examID
    */
    public function getExamTimeRange($examID) {
        $idSan = $this->db->clean($examID);
        $query = "SELECT * FROM "
               . "(SELECT TIMESTAMP(session_date, MIN(start_time)) AS time_start_exam "
               . "FROM exam_sessions "
               . "WHERE exam_id = $idSan "
               . "AND session_date <= ALL(select session_date FROM exam_sessions WHERE exam_id = $idSan)) AS exam_start, "
               . "(SELECT TIMESTAMP(session_date, MAX(end_time)) AS time_end_exam "
               . "FROM exam_sessions "
               . "WHERE exam_id = $idSan "
               . "AND session_date >= ALL(select session_date FROM exam_sessions WHERE exam_id = $idSan)) AS exam_end";

        return $this->db->fetch_row($this->db->query($query));
    }

     /**
      * Get the current scoring progress of a student in a given station
      * @param $studentID
      * @param $sessionID
      * @param $stationNum
      * @return array
      */
      public function getScoringProgressByStationNum($studentID, $sessionID, $stationNum) {
        $studentID = $this->db->clean($studentID);
        $sessionID = $this->db->clean($sessionID);
        $stationNum = $this->db->clean($stationNum);

        $query = "SELECT COUNT(DISTINCT item_scores.score_id) AS completed, examiner_id, users.forename, users.surname, users.email, "
               .        "users.contact_number, is_complete, session_stations.station_id, COUNT(DISTINCT section_items.item_id) AS total "
               . "FROM session_stations JOIN student_results JOIN item_scores JOIN users JOIN form_sections JOIN section_items "
               . "ON session_stations.station_id = student_results.station_id "
               .     "AND student_results.result_id = item_scores.result_id "
               .     "AND student_results.examiner_id = users.user_id "
               .     "AND session_stations.form_id = form_sections.form_id "
               .     "AND form_sections.section_id = section_items.section_id "
               . "WHERE student_id = '$studentID' AND session_stations.session_id = $sessionID AND session_stations.station_number = $stationNum "
               . "GROUP BY examiner_id";

        return CoreDB::into_array($this->db->query($query));
    }

     /**
      * Get the completion data of the scoring of a given student in a given station
      * @param $studentID
      * @param $sessionID
      * @param $stationNum
      * @return array
      */
      public function getScoringCompleteness($studentID, $sessionID, $stationNum) {
        $studentID = $this->db->clean($studentID);
        $sessionID = $this->db->clean($sessionID);
        $stationNum = $this->db->clean($stationNum);

        $query = "SELECT student_results.station_id, examiner_id, is_complete "
               . "FROM student_results JOIN session_stations "
               . "ON student_results.station_id = session_stations.station_id "
               . "WHERE session_id = $sessionID AND station_number = $stationNum AND student_id = '$studentID'";

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Find the result (specific student in specific station) that has been most recently updated in a given session
     * @param $sessionID int ID of the session
     * @param bool $onlyIncomplete Flag to specify if the method must return only the last updated of an ongoing station
     * @return array
     */
    public function getLastUpdatedRotation($sessionID, $onlyIncomplete = false) {
        $sessionIdSan = $this->db->clean($sessionID);

        $onlyIncompleteExpression = $onlyIncomplete ?
            "AND student_results.is_complete = 0 " : "";

        $query = "SELECT student_results.*, MAX(item_scores.updated_at) AS last_updated_score "
               . "FROM item_scores "
               . "JOIN student_results JOIN session_stations "
               . "ON item_scores.result_id = student_results.result_id AND student_results.station_id = session_stations.station_id "
               . "WHERE session_stations.session_id = $sessionIdSan " . $onlyIncompleteExpression
               . "GROUP BY item_scores.result_id "
               . "ORDER BY last_updated_score DESC "
               . "LIMIT 1";

        $dbResult = $this->db->fetch_row($this->db->query($query));

        if (!$dbResult)
            return $dbResult;

        return [
            "student_id" => $dbResult["student_id"],
            "station_id" => $dbResult["station_id"],
            "updated_at" => $dbResult["last_updated_score"]
        ];
    }

    /**
    * Check if a given exam has any stations with weighting/adjusted
    *
    * @param $examId
    * @return boolean
    */
    public function examHasWeightingStations($examId) {
        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS any_weighting '
             . 'FROM exam_sessions JOIN session_stations '
             . 'ON exam_sessions.session_id = session_stations.session_id '
             . "WHERE exam_id = $examId AND NOT (weighting IS NULL OR weighting = 0.0)";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        else if ($result === "0") return false;
        else return null;
    }

   /**
    * Check if a given exam is associated with any scoresheet that has at least one item within it's sections
    * with a weight different than 1
    *
    * @param $examId
    * @return bool|null
    */
    public function hasWeightedItem($examId) {

        $examId = $this->db->clean($examId);
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) AS has_weighted_items '
                . 'FROM exam_sessions JOIN session_stations JOIN form_sections JOIN section_items '
                . 'ON exam_sessions.session_id = session_stations.session_id AND '
                . '   session_stations.form_id = form_sections.form_id AND '
                . '   form_sections.section_id = section_items.section_id '
                . "WHERE exam_sessions.exam_id = $examId AND section_items.weighted_value <> 1.00";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;

   }

    /**
     * Check if an exam as adjusted outcomes, received from Qpercom Analyse
    * @param $id
    * @return bool|null
    */
    public function hasAdjustedOutcomes($id) {

        $query = "SELECT IF(COUNT(*) > 0, 1, 0) AS adjusted "
               . "FROM student_exam_data "
               . "WHERE exam_id = {$this->db->clean($id)} "
               . "AND (grade_trigger LIKE '%scoreSource%' OR adjusted_source = 'analyse')";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        if ($result === "0") return false;
        return null;

   } 

   /**
    * Exam has weightings (station OR examiner OR item weighting)
    * @param int $id  Exam identifier
    * @return bool yes/no
    */
   public function hasAnyWeighting($id) { 

    $sql = "SELECT IF(COUNT(*) > 0, 1, 0) AS has "
            . "FROM exams INNER JOIN exam_sessions USING(exam_id) "
            . "INNER JOIN session_stations USING(session_id) "
            . "INNER JOIN forms USING(form_id) "
            . "LEFT JOIN station_examiners USING(station_id) "
            . "LEFT JOIN form_sections USING(form_id) "
            . "LEFT JOIN section_items USING(section_id) "
            . "WHERE exam_id = {$this->db->clean($id)} AND "
            . "(session_stations.weighting <> 0.000 OR scoring_weight IN (1,3)"
            . " OR weighted_value <> 1.000)";

    return (CoreDB::single_result($this->db->query($sql)) === "1");

  }

  /**
   * Update results updated at timestamp for the exam
   * @param int $id  Exam identifier
   * @return bool yes/no
   */
   public function resultsUpdatedAt($id) {

        return $this->db->query(
            "UPDATE exam SET results_update_at = '" 
            . date("Y-m-d H:i:s") . "' AND exam_id = {$this->db->clean($id)}"
        );
    
   } 

   //get assesment from exam

   public function getAssessment($term_id,$dept_id){

        return $this->db->query(
            "SELECT exam_name,exam_id FROM `exams` WHERE `term_id` = '{$this->db->clean($term_id)}' AND `dept_id` = '{$this->db->clean($dept_id)}'"
        );

   }

//get exam_session data

   public function getExamData($id){

    return $this->db->query(
      "SELECT `session_id`,`session_description` FROM `exam_sessions` WHERE `exam_id` = '{$this->db->clean($id)}'"
    );

   }


//get group data

   public function getGroupData($id){

    return $this->db->query(
      "SELECT `group_id`,`group_name` FROM `session_groups` WHERE `session_id` = '{$this->db->clean($id)}'"
    );

   }

// get students id from group
   public function getStudentData($id){

        return $this->db->query(
          "SELECT `student_id` FROM `group_students` WHERE `group_id` = '{$this->db->clean($id)}'"
        );

   }

   //get student details

   public function getStudentDetails($id){
      return $this->db->query(
        "SELECT `student_id`,`forename`,`surname` FROM `students` WHERE `student_id` = '{$this->db->clean($id)}'"
      );
   }


   public function getFormID($id){
    return $this->db->query(
      "SELECT `form_id` FROM `session_stations` WHERE `session_id` = '{$this->db->clean($id)}'"
    );
   }

   public function getForm($id){
    return $this->db->query(
      "SELECT `form_id`,`form_name` FROM `forms` WHERE `form_id` = '{$this->db->clean($id)}'"
    );
   }

    public function uploadmedia($Assessment,$session,$group,$student_id,$scoresheet,$file_name,$retention){
    return $this->db->query(
      "INSERT INTO `upload_media`(`assessment_id`, `session_id`, `group_id`, `student_id`, `scoresheet_id`, `file_name`,`retention_days`) VALUES ('{$this->db->clean($Assessment)}','{$this->db->clean($session)}','{$this->db->clean($group)}','{$this->db->clean($student_id)}','{$this->db->clean($scoresheet)}','{$this->db->clean($file_name)}','{$this->db->clean($retention)}')"
    );
   }

      public function getFile($session_id,$form_id){
    return $this->db->query(
      "SELECT `file_name` FROM `upload_media` WHERE `session_id` = '{$this->db->clean($session_id)}' AND `scoresheet_id` = '{$this->db->clean($form_id)}' ORDER BY `upload_media`.`id` DESC LIMIT 1"
    );
   }

   public function getDaysData(){
    return $this->db->query(
      "SELECT `created_at`,`file_name`,`retention_days` FROM `upload_media`"
    );
   }
}
