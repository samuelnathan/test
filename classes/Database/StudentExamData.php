<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * Database function calls relating to Student Exam Data
  */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class StudentExamData extends AbstractEntityDAO
 {

    protected function getTableName() { return 'student_exam_data'; }
    protected function getIds() { return ['student_id', 'exam_id']; }

    /**
     * Register students exam data record
     * 
     * @param string[] $students   ID numbers for students
     * @param int $exam         ID number for exam
     * @param string $notes     Notes text
     * @param string $author    Notes author
     * 
     * @return int  number of records that required updating
     */
    public function registerStudentData($students, $exam, $notes = '', $author = null) 
    {

      // Validate student lists
      if (!is_array($students) || count($students) == 0) {

        return 0;

      }

      $examSan = $this->db->clean($exam);
      $notesSan = $this->db->clean($notes);
      $authorSan = $this->db->clean($author);
      $authorSan = empty($authorSan) ? "NULL" : "'$authorSan'";
      $updateCount = 0;

      foreach ($students as $student) {

          $record = $this->byStudentExam($student, $exam);

          if (mysqli_num_rows($record) == 0) {

              $studentSan = $this->db->clean($student);
              $updated = $this->db->query(
                "INSERT INTO student_exam_data (student_id, exam_id, " .
                "notes, notes_author) VALUES('$studentSan', '$examSan', 
                '$notesSan', $authorSan)"
              );

              $updateCount += (int)$updated;

          }

      }
    
    }

    /**
     * Update student exam data
     * 
     * @param string $student   ID number for student
     * @param int $exam         ID number for exam
     * @param string $notes     Notes text
     * @param string $author    Notes author
     * 
     * @return mysqli_result|bool FALSE if query fails, otherwise mysqli_result
     */
    public function updateStudentData($student, $exam, $notes, $author) 
    {
      
      list($student, $exam, $notes, $author) = array_map(
         "\OMIS\Database\CoreDB::clean", 
         func_get_args()
      );

      $timestamp = date("Y-m-d H:i:s");

      $updated = $this->db->query(
         "UPDATE student_exam_data SET notes = '$notes', 
          notes_updated = '$timestamp', notes_author = '$author' 
          WHERE student_id = '$student' 
          AND exam_id = '$exam'"
      );
 
      if ($updated) {

          $examRecord = $this->db->exams->getExam($exam);
          \Analog::info($author
            . " edited notes/issues for student $student " 
            . " in exam " . $examRecord['exam_name']
          );

      }

      return $updated;
      
    }

    /**
     * Get student exam data
     * 
     * @param string $student   ID number for student
     * @param int $exam         ID number for exam
     * 
     * @return mysqli_result|bool FALSE if query fails, otherwise mysqli_result
     */
    public function byStudentExam($student, $exam) 
    {
      
      return $this->db->query(
          "SELECT * FROM student_exam_data 
          WHERE student_id = '" . $this->db->clean($student) . "' 
          AND exam_id = '" . $this->db->clean($exam) . "'"
      );
      
    }

    /**
     * Drop exam data by exam and student
     * 
     * @param int $exam          Session identifier
     * @param string $student    Student identifier
     * @return bool true|false   Status
     */
    public function dropByExamStudent($exam, $student) 
    {
        
        $sql = "DELETE FROM student_exam_data WHERE student_id = '"
             . $this->db->clean($student) . "' AND exam_id = '" 
             . $this->db->clean($exam) . "'";

        return $this->db->query($sql);
        
    }

     /**
      * Update the exam outcome record for a student
      * @param string $studentID     Identifier number for student
      * @param int $examID           Identifier number for exam
      * @param mixed[] $data         Outcome data to set
      * @return bool
      */
     public function updateOutcome($studentID, $examID, array $data)
     {

         $sql = 'INSERT INTO student_exam_data '
              . '(student_id, exam_id, score, adjusted_score, '
              . 'possible, adjusted_possible, pass, '
              . 'passed, adjusted_passed) '
              . 'VALUES (?,?,?,?,?,?,?,?,?) '
              . 'ON DUPLICATE KEY UPDATE '
              . 'score = ?, adjusted_score = ?, possible = ?, '
              . 'adjusted_possible = ?, pass = ?, passed = ?, adjusted_passed = ?';

         $dataValues = array_values($data);

         $stmt = $this->db->prepare($sql);
         $stmt->bind_param(
             'sidddddiidddddii', $studentID, $examID,
                    ...$dataValues, ...$dataValues
         );

         $status = $stmt->execute();

         $stmt->close();

         return $status;

     }

     /** Set uptodate by exam
      * @param int $examID
      * @param bool $isUpToDate
      * @return bool
      */
     public function setUpToDateByExam($examID, $isUpToDate)
     {

         $sql = 'UPDATE student_exam_data '
             . 'SET up_to_date = ? '
             . 'WHERE exam_id = ?';

         $isUpToDateInt = $isUpToDate ? 1 : 0;

         $stmt = $this->db->prepare($sql);
         $stmt->bind_param('ii', $isUpToDateInt, $examID);
         $status = $stmt->execute();

         $stmt->close();

         return $status;

     }

     /** Set uptodate by session
      * @param int $sessionID
      * @param bool $isUpToDate
      * @return bool
      */
     public function setUpToDateBySession($sessionID, $isUpToDate)
     {

         $sql = 'UPDATE student_exam_data '
              . 'INNER JOIN exam_sessions USING (exam_id) '
              . 'INNER JOIN session_groups USING (session_id) '
              . 'INNER JOIN group_students '
              . 'ON (group_students.student_id = student_exam_data.student_id '
              . ' AND session_groups.group_id = group_students.group_id) '
              . 'SET up_to_date = ? '
              . 'WHERE session_id = ? ';

         $isUpToDateInt = $isUpToDate ? 1 : 0;

         $stmt = $this->db->prepare($sql);
         $stmt->bind_param('ii', $isUpToDateInt, $sessionID);
         $status = $stmt->execute();

         $stmt->close();

         return $status;

     }

     /**
      * @param string $studentID
      * @param int $examID
      * @param bool $isUpToDate
      * @return bool
      */
     public function setUpToDate($studentID, $examID, $isUpToDate)
     {
         $query = 'UPDATE student_exam_data '
             . 'SET up_to_date = ? '
             . 'WHERE student_id = ? AND exam_id = ?';

         $isUpToDateInt = $isUpToDate ? 1 : 0;

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('isi', $isUpToDateInt, $studentID, $examID);
         $status = $stmt->execute();

         $stmt->close();

         return $status;
     }

 }
