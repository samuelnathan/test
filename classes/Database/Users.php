<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to Users
 */

namespace OMIS\Database;

use \OMIS\Auth\Role as Role;
use \OMIS\Auth\RoleCategory as RoleCategory;
use OMIS\Database\DAO\AbstractEntityDAO;

class Users extends AbstractEntityDAO
{

    /**
     * Generates a hashed version of the supplied plaintext $password using PHP
     * 5.5's new password hashing functions _or_ the equivalent functions from
     * the password_compat (see link) library for earlier versions of PHP (these are
     * available through Composer)
     *
     * @link https://github.com/ircmaxell/password_compat password_compat library
     * @global \OMIS\Config $config Configuration object
     * @param  string $password Plaintext password
     * @return string Hashed password
     */
    private function bCrypt($password)
    {
        global $config;
        if (!isset($config)) {
            $config = new \OMIS\Config();
        }

        return password_hash($password, PASSWORD_DEFAULT, array("cost" => $config->passwords['hash_cost']));
    }

    /**
     * Get user information by [Updated 18/06/2014]
     * @param  string  $identifier  Identifier value
     * @param  string  $field  Identifier database field e.g. 'user_id'
     * @param  boolean   $convertToArray if TRUE converts return data to an array
     * @return mixed[]|mysqli_result|bool If the user does not exist, returns FALSE.
     *          Otherwise, either a mixed array or a mysqli_result describing the
     *          user depending on whether $convertToArray is TRUE or FALSE (respectively)
     */
    public function getUser($identifier, $field = "user_id", $convertToArray = false)
    {
        $user_id = $this->db->clean($identifier);
        $sql = "SELECT * FROM users WHERE ". $field . " = '" . $user_id . "'";
        
        $result = $this->db->query($sql);
        if (($result !== \FALSE) && $convertToArray) {
            return $this->db->fetch_row($result);
        } else {
            return $result;
        }
    }
    
    /**
     * Update user in the database.
     * 
     * @param string $org_user_id Original User ID
     * @param string $new_user_id New User ID
     * @param string $surname New surname (if any)
     * @param string $forename New firstname (if any)
     * @param string $email New email address (if any)
     * @param string $contact_number New contact number (if any)
     * @return bool TRUE on success, FALSE otherwise.
     * 
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function updateUser($org_user_id, $new_user_id = null, $surname = null, $forename = null, $email = null, $contact_number = null)
    {
        $originalIDSantitized = $this->db->clean($org_user_id);
        
        /* Iterate through the bulk of the fields and update them if there's a
         * value specified...
         */
        $fields = [];
        foreach (['surname', 'forename', 'email', 'contact_number'] as $field) {
            // (DW) The double-$ syntax below is *entirely* intentional :)
            if (!is_null($$field)) {
                $fields[] = sprintf("%s='%s'", $field, $this->db->clean($$field));
            }
        }
        
        if (!empty($new_user_id) && ($org_user_id != $new_user_id)) {
            $newIDSantitized = $this->db->clean($new_user_id);
            $fields[] = "user_id='$newIDSantitized'";
        }

        // No fields specified.
        if (empty($fields)) return true;

        $sql = "UPDATE users SET " . implode(", ", $fields) . " " 
             . "WHERE users.user_id = '$originalIDSantitized'";

        return $this->db->query($sql);
        
    }



    //Update user account password
    /* @TODO 2012-12-13 (DW) This function is used once by pages/request.php,
     * which is a "forgot my password" form. This could be replaced by logic in
     * that page which calls the changeUserPassword() method instead???
     */
    public function updateUserAccount($email, $user_id, $password)
    {
        $email = $this->db->clean($email);
        $user_id = $this->db->clean($user_id);
        $password = $this->db->clean($password);
        $encrypted_password = $this->bCrypt($password);
        $sql = "UPDATE users SET password = '$encrypted_password' WHERE "
            . "email = '$email' AND user_id = '$user_id'";

        return $this->db->query($sql);
    }

    /**
     * Change a user's password.
     *
     * @param  string  $user_id      User_id of user to set password for
     * @param  string  $new_password New desired password
     * @return boolean TRUE if password changed or same as was already, FALSE if update fails.
     */
    public function changeUserPassword($user_id, $new_password)
    {
        $user_id = $this->db->clean($user_id);
        
        // Check if the user exists...
        $user = $this->getUser($user_id, 'user_id', true);
        if (empty($user)) {
            $error_message = $_SESSION['user_identifier'] . " attempted to change "
                . "password for user $user_id, but doesn't exist";
            \Analog::warning($error_message);
            //error_log(__METHOD__ . ": ". $error_message);
            return false;
        }
        
        $new_password = $this->bCrypt($this->db->clean($new_password));

        $q_checkpassword = "SELECT password FROM users WHERE user_id = '" . $user_id . "'";
        $current_password = CoreDB::single_result($this->db->query($q_checkpassword));
        if ($new_password == $current_password) {
            $error_message = $_SESSION['user_identifier'] . " attempted to change "
                . "the password for user $user_id to what it was already";
            \Analog::warning($error_message);
            return true;
        }

        /* If we get to here, the password currently stored is different to the
         * one we want to set it to, so let's change it.
         */
        $q_updatepassword = "UPDATE users SET password = '$new_password' WHERE user_id='$user_id'";

        $success = $this->db->query($q_updatepassword);
        if ($success) {
            \Analog::info(
                $_SESSION['user_identifier'] . " changed password "
                . "for user $user_id"
            );
        } else {
            \Analog::error(
                $_SESSION['user_identifier'] . " failed to change"
                . "password for user $user_id"
            );
        }
        
        return $success;
    }

    //Does user exist [Updated 05/02/2014]
    public function doesUserExist($user_id) { return $this->existsById($user_id); }

    /**
     * Check if a given user account exists and is active.
     * @param string $user_id User ID
     * @param string $email (Optional) user's e-mail address
     * @return bool TRUE if the user exists, FALSE otherwise.
     */
    public function doesUserAccountExist($user_id, $email = null)
    {
        $sql = "SELECT COUNT(*) as count FROM users WHERE activated = '1' AND "
            . "user_id = '" . $this->db->clean($user_id) . "'";
        
        if (!empty($email)) {
            $sql .= " AND email = '" . $this->db->clean($email) . "'";
        }

        return (CoreDB::single_result($this->db->query($sql)) == 1);
    }

    //Get user contact number [Updated 27/01/2014]
    public function getUserContactNumber($user_id)
    {
        $sql = "SELECT contact_number FROM users WHERE user_id = '"
            . $this->db->clean($user_id) . "'";
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Get list of examiners
     *
     * @param  array      $filterCriteria  Filter Criteria contains fields: term (ID number of term),
     *                                     department (ID number of department), user role (Role ID or IDs),
     *                                     account(enabled, disabled, all), forename (Single character from A-Z),
     *                                     surname (Single character from A-Z),
     * 
     * @param  string     $orderBy         Table column(s) to order results by
     * @param  int        $start           Start index of returned results
     * @param  int        $end             End index of returned results
     * @param  array      $fields          Selected fields 
     * @return mixed[][]                   Array containing query results.
     */
    public function getExaminers($filterCriteria, $orderBy = 'users.created_at', $start = 0, $end = PHP_INT_MAX, $fields = null)
    {
        // No required fields passed in then use the default list 
        if ($fields == null) {
          $selectFields = [
            'users.user_id',
            'surname',
            'forename',
            'email',
            'password',
            'user_role',
            'contact_number',
            'last_login',
            'trained',
            'activated'
          ];
        } else {
          $selectFields = $fields;
        }
        
        $sql = $this->buildExaminerQuery($selectFields, $filterCriteria, $orderBy);

        // If we need to limit the list, do that now.
        if (($start > 0) || ($end < PHP_INT_MAX)) {
            $count = abs($end - $start);
            $sql .= " LIMIT $start, $count";
        }

        return CoreDB::into_array($this->db->query($sql));
    }
    
/**
     * Build query to get a list of examiners from the database.
     *
     * @param  mixed   selectFields      Either a string (single field) or array (many)
     * @param  array   $filterCriteria   Filter Criteria contains fields: term (ID number of term),
     *                                   department (ID number of department), user role (Role ID or IDs),
     *                                   account(enabled, disabled, all), forename (Single character from A-Z),
     *                                   surname (Single character from A-Z),
     * 
     * @param  string  $orderBy          Table column(s) to order results by
     * @return string                    generated SQL Query string.
     */
    public function buildExaminerQuery($selectFields, $filterCriteria, $orderBy = null)
    {
        
        // Unpack array of filter criteria into readable variables
        list($deptID,
             $termID,
             $userRole,
             $account,
             $forenameFirstChar,
             $snameFirstChar,
             $search) = $filterCriteria;
        
        
        // Sanitize anything that can make it to the database...
        $termID = $this->db->clean($termID);
        if (is_scalar($userRole)) {
            // Stop this mangling arrays.
            $userRole = $this->db->clean($userRole);
        }
        
        // Put all of the WHERE clauses in an array to simplify construction of
        // the overall query...
        $whereClauses = [];

        /*
         *  If the user only wants enabled or disabled accounts, add the appropriate condition
         *  to the query...
         */
        if (strlen($account) > 0) {
           if ($account) {
            $whereClauses[] = "activated = '1'";
           } elseif (!$account) {
            $whereClauses[] = "activated = '0'";
           }
        }

        /*
         * If there's been a non-empty $userRole (user role) passed in, then add a
         * clause to the WHERE part of the query to suit...
         */
        if (!empty($userRole)) {
           if (is_array($userRole)) {
             $whereClauses[] = "user_role IN (" . implode(", ", $userRole) . ")";
           } else {
             $userRole = trim($userRole);
             if (!empty($userRole)) {
              $whereClauses[] = "user_role = $userRole";
             }
           }
        }

        // Restrict the query by username...
        $whereClauseFormat = "user_id IN (SELECT user_id FROM examiner_department_term "
                           . "WHERE 1";
        
        // Department filtering
        if (!is_null($deptID) && trim($deptID) != '') {
            $deptID = $this->db->clean($deptID);
            $whereClauseFormat .= sprintf(" AND dept_id = '%s'", $deptID);
        }
        
        // Academic term filtering
        if (!is_null($termID) && trim($termID) != '') {
            $whereClauseFormat .= sprintf(" AND term_id = '%s'", $termID);
        }
        
        // Forenames filtering (First Character A-Z)
        $forenameFirstChar = $this->db->clean($forenameFirstChar);
        if (ctype_alpha($forenameFirstChar)) {
            $whereClauseFormat .= sprintf(" AND forename LIKE '%s'", $forenameFirstChar . "%");
        }
        
        // Surname filtering (First Character A-Z)
        $snameFirstChar = $this->db->clean($snameFirstChar);
        if (ctype_alpha($snameFirstChar)) {
            $whereClauseFormat .= sprintf(" AND surname LIKE '%s'", $snameFirstChar . "%");
        }
        
        $whereClauseFormat .= ')';
        $whereClauses[] = $whereClauseFormat;
        
        /* If the $selectFields parameter is an array, then implode() it to make a
         * list. If not, assume it's a string and use it "raw":
         */
        if (is_array($selectFields)) {
            $fieldList = implode(", ", $selectFields);
        } else {
            $fieldList = $selectFields;
        }
        
        /**
         *  Search ID, Forenames, Surname, email
         */
         $search = $this->db->clean($search);
         
         if (strlen($search) > 0) {
            
            // Extract search terms from search string 
            $searchTerms = explode(' ', $search);
           
            $whereSearchParts = [];
            foreach ($searchTerms as $searchTerm) {
              
              // Remove additional white spaces  
              $searchTerm = preg_replace('/\s+/', '', $searchTerm);
              
              // Add search terms
              $whereSearchParts[] = "user_id LIKE '".$searchTerm."%'";
              $whereSearchParts[] = "forename LIKE '".$searchTerm."%'";
              $whereSearchParts[] = "surname LIKE '".$searchTerm."%'";
              $whereSearchParts[] = "email LIKE '".$searchTerm."%'";

            }
            
            // Combine search clause parts
            $whereClauses[] = "(" . implode(" OR ", $whereSearchParts) . ")";
            
         }
         
         // Combine all clauses
         $whereClause = "WHERE " . implode(" AND ", $whereClauses);
               
        /* If we're just looking for a record count, we don't want to group the
         * records...
         */
        if (!is_array($selectFields) && (stripos(trim($selectFields), "count(") === 0)) {
            $groupClause = "";
        } else {
            $groupClause = "GROUP BY users.user_id";
        }

        $sql = "SELECT $fieldList FROM users INNER JOIN examiner_training "
             . "USING (user_id) $whereClause $groupClause ";

        if (!is_null($orderBy)) {
            $sql .= "ORDER BY $orderBy";
        }

        return $sql;
    }
    
    /**
     * Get Examiner User Accounts
     *
     * @return mixed[][] Array containing query results.
     */
    public function getExaminerUserAccounts()
    {
        $userRolesExaminer = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS);
        $roleList = implode(', ', $userRolesExaminer->role_ids);

        $sql = "SELECT * " .
               "FROM users " .
               "WHERE (user_role IN (" . $roleList . "))";

        return CoreDB::into_array($this->db->query($sql));

    }

    //Get Examiners Departments [Updated 27/01/2014]
    public function getExaminersDepartments($term_id, $to_array = false)
    {
        $user_roles_examiner = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS);
        $role_list = implode(', ', $user_roles_examiner->role_ids);

        $sql = "SELECT users.user_id, examiner_department_term.dept_id " .
            "FROM users " .
            "INNER JOIN examiner_department_term " .
            "USING(user_id) " .
            "WHERE (user_role IN (" . $role_list . "))" .
            "AND term_id = '" . $this->db->clean($term_id) . "'" .
            "GROUP BY users.user_id, examiner_department_term.dept_id " .
            "ORDER BY users.created_at";
        
        if ($to_array == true) {
            return CoreDB::into_array($this->db->query($sql));
        } else {
            return $this->db->query($sql);
        }
    }

    /*
     * A link for a exam team member with at least one
     * department exists
     * @param string $id    String identifier for user
     * @param int $role     Integer role id for user
     */
    public function departmentLinkExists($id, $role)
    {

        // Not concerned by non assessment team members
        if (!in_array($role, [
            Role::USER_ROLE_EXAMINER,
            Role::USER_ROLE_EXAM_ADMIN,
            Role::USER_ROLE_STATION_MANAGER
        ])) return true;

        return (CoreDB::single_result(
            $this->db->query(
        "SELECT IF(COUNT(*) > 0, 1, 0) 
               FROM examiner_department_term 
               WHERE user_id = '{$this->db->clean($id)}'"
            )) == "1");

    }
   
    /**
     * Get Examiner Table Headers
     */
    public function getExaminerColumns()
    {
        
        $dbName = CoreDB::single_result($this->db->query("select database()"));

        $sql = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE "
            . "(TABLE_NAME = 'users' OR TABLE_NAME = 'examiner_department_term') "
            . "AND TABLE_SCHEMA = '" . $dbName . "' AND COLUMN_NAME NOT IN "
            . "('term_id', 'last_login', 'updated_at', 'created_at', 'last_active')";

        return CoreDB::into_values($this->db->query($sql));

    }

    //Get Examiner Record [Updated 27/01/2014]
    public function getExaminer($user_id)
    {
        $user_id = $this->db->clean($user_id);
        $sql = "SELECT user_id, email, password, user_role, last_login, "
            . "activated, forename, surname, trained, contact_number "
            . "FROM users INNER JOIN examiner_training USING(user_id) "
            . "WHERE user_id = '" . $user_id . "'";

        return $this->db->fetch_row($this->db->query($sql));
    }

    //Delete Examiners [Updated 04/02/2014]
    public function deleteExaminers($examiners)
    {
        // Callback to process the examiners list.
        $callback_examiners = function ($value) {
            /* Clean the string, but return FALSE if it's the current user so
             * that it can be removed by array_filter.
             */
            $value = $this->db->clean($value);
            return ($value === $_SESSION['user_identifier']) ? \FALSE : "'$value'";
        };
        
        /* Clean up the examiners. As the callback function returns FALSE for
         * items where the user is the current user, array_filter removes these.
         */
        $examiners = array_filter(array_map($callback_examiners, $examiners));
        $success = \TRUE;
        $delete_counts = [];
        foreach (['users', 'examiner_department_term'] as $table) {
            $sql = "DELETE FROM $table WHERE user_id IN (" . implode(", ", $examiners) . ")";
            $result = $this->db->query($sql);
            
            // Find out how many rows this query affected
            $delete_count = $this->db->getAffectedRowCount();
            
            /* Record this as being a success if the number of deleted records
             * is what we expect.
             */
            $success &= (($result) && ($delete_count === count($examiners)));
            $delete_counts[] = $delete_count;
        }
        
        if ($success) {
            \Analog::info(
                $_SESSION['user_identifier'] . " deleted the following "
                . "examiners: " . implode(", ", $examiners)
            );
        } else {
            \Analog::info(
                $_SESSION['user_identifier'] . " encountered errors "
                . "deleting these examiners: " . implode(", ", $examiners)
            );
        }
        
        /* The smallest number of deletions should indicate how many were
         * deleted successfully.
         */
        return min($delete_counts);
    }

    //Enable / Disable Examiners [Updated 04/02/2014]
    public function enableDisableExaminers($examiners, $enabled)
    {
        $enabled = ($enabled == 'ena') ? 1 : 0;
        $updateCount = 0;

        foreach ($examiners as $user_id) {
            $q = "UPDATE users SET activated = '" . $enabled . "' WHERE user_id = '"
                . $this->db->clean($user_id) . "'";

            if ($this->db->query($q)) {
                $updateCount++;
            }
        }

        return $updateCount;
    }

    //Set Examiners Trained / Not Trained [Updated 04/02/2014]
    public function trainedExaminers($examiners, $trained)
    {
        $trained = ($trained == 'trained') ? 1 : 0;
        $updateCount = 0;

        foreach ($examiners as $user_id) {
            $q = "UPDATE examiner_training SET trained = '" . $trained
                . "' WHERE user_id = '" . $this->db->clean($user_id) . "'";

            if ($this->db->query($q)) {
                $updateCount++;
            }
        }

        return $updateCount;
    }

    //Change User Role for Examiners passed in [Updated 04/02/2014]
    public function changeUserRoleExaminers($examiners, $user_role)
    {
        $updateCount = 0;
        foreach ($examiners as $user_id) {
            $user_id = $this->db->clean($user_id);
            if ($_SESSION['user_identifier'] == $user_id) {
                // If Current User
                continue;
            }
            
            $q = "UPDATE users SET user_role = " . $user_role . " WHERE user_id = '"
                . $user_id . "'";
            
            if ($this->db->query($q)) {
                $updateCount++;
            }
        }

        return $updateCount;
    }

    //Insert Examiner [Updated 05/02/2014]
    public function insertExaminer($examiner_id, $firstname, $surname, $email_address, $contact_number, $password, $role, $account, $trained, $depts, $term_id)
    {
        //1 or more departments have being selected
        if (count($depts) == 0) {
            return false;
        }
        $examiner_id = $this->db->clean($examiner_id);
        $firstname = $this->db->clean($firstname);
        $surname = $this->db->clean($surname);
        $email_address = $this->db->clean($email_address);
        $role = $this->db->clean($role);
        $contact_number = $this->db->clean($contact_number);
        $password = $this->bCrypt($this->db->clean($password));
        $trained = $this->db->clean($trained);
        $account = $this->db->clean($account);
        $term_id = $this->db->clean($term_id);

        //Insert query
        $sql = "INSERT INTO users (user_id, surname, forename, email, contact_number, "
            . "password, user_role, activated) VALUES ('" . $examiner_id
            . "', '" . $surname . "', '" . $firstname . "', '" . $email_address
            . "', '" . $contact_number . "', '" . $password . "', '" . $role
            . "', '" . $account . "')";

        //If user has being inserted....
        if (!($this->db->query($sql))) {
            /* @TODO (DW) This should be a handled exception rather than
             * returning a "raw" database error (or part of one) to the user
             */
            $error = explode("'", mysqli_error($this->db->getConnection()));

            return $error[1];
        }
        
        \Analog::info($_SESSION['user_identifier'] . " created user with identifier " . $examiner_id);

        //Insert departments
        $qd = "INSERT INTO examiner_department_term (user_id, dept_id, term_id) VALUES ";

        //Loop through departments
        $c = 0;
        foreach ($depts as $dept) {
            $dept = $this->db->clean($dept);
            if ($this->db->departments->existsById($dept)) {
                $c++;
                $qd .= " ('" . $examiner_id . "', '" . $dept . "', '" . $term_id . "'),";
            }
        }

        if ($c > 0) {
            $this->db->query(substr_replace($qd, '', -1));
        }

        //Insert trained
        $sql = "INSERT INTO examiner_training (user_id, trained) VALUES ('"
            . $examiner_id . "', '" . $trained . "')";
        return $this->db->query($sql);
    }

    //Update examiner in system [Updated 05/02/2014]
    public function updateExaminer($userid_new, $userid_previous, $firstname, $surname, $email_address, $contact_number, $password, $role, $account_status, $trained, $change_password, $depts, $term_id)
    {
        if (count($depts) == 0) {
            /* If there are no departments for the examiner, abort the update
             * process.
             * @TODO (DW) 07/MAY/2014 Is there a good reason why this is so? 
             * Surely one can change a user's name or other details even if
             * they're not assigned to a department?
             */
            return false;
        }
        
        //We have 1 or more departments to insert
        $userid_new = $this->db->clean($userid_new);
        $userid_previous = $this->db->clean($userid_previous);
        $firstname = $this->db->clean($firstname);
        $surname = $this->db->clean($surname);
        $email_address = $this->db->clean($email_address);
        $contact_number = $this->db->clean($contact_number);
        $password = $this->db->clean($password);
        $role = $this->db->clean($role);
        $account_status = $this->db->clean($account_status);
        $trained = $this->db->clean($trained);
        $change_password = $this->db->clean($change_password);
        $term_id = $this->db->clean($term_id);

        if ($change_password == 1) {
            //If we need to change the password...
            $passwordSql = "password = '" . $this->bCrypt($password) . "',";
        } else {
            $passwordSql = "";
        }
        
        //If Current Logged On User
        if ($_SESSION['user_identifier'] == $userid_previous) {
            $examiner_update_sql = "UPDATE users SET user_id = '" . $userid_new
                . "', surname = '" . $surname . "', forename = '" . $firstname
                . "', " . $passwordSql . " email = '" . $email_address
                . "', contact_number = '" . $contact_number . "' WHERE users.user_id = '"
                . $userid_previous . "' ";
        } else {
            $examiner_update_sql = "UPDATE users SET user_id = '" . $userid_new
                . "', surname = '" . $surname . "', forename = '" . $firstname
                . "', email = '" . $email_address . "', contact_number = '"
                . $contact_number . "', " . $passwordSql . " user_role = '"
                . $role . "', activated = '" . $account_status
                . "' WHERE users.user_id = '" . $userid_previous . "' ";
        }

        //If examiner is updated
        if ($this->db->query($examiner_update_sql)) {
            //Delete previous departments and add new ones
            $examiner_term_sql = "DELETE FROM examiner_department_term WHERE user_id = '"
                . $userid_new . "' AND term_id = '" . $term_id . "'";
            $this->db->query($examiner_term_sql);
            $qd = "INSERT INTO examiner_department_term (user_id, dept_id, term_id) VALUES ";

            //Loop through departments
            $c = 0;
            foreach ($depts as $dept) {
                $dept = $this->db->clean($dept);
                if ($this->db->departments->existsById($dept)) {
                    $c++;
                    $qd .= " ('" . $userid_new . "', '" . $dept . "', '" . $term_id . "'),";
                }
            }

            if ($c > 0) {
                $this->db->query(substr_replace($qd, '', -1));
            }

            //Update training field.
            $update_trained_sql = "UPDATE examiner_training SET trained = '"
                . $trained . "' WHERE user_id = '" . $userid_new . "'";
            return $this->db->query($update_trained_sql);
        }

        // If we get to here, we failed to update the examiner.
        return false;
    }

    /**
     * Update examiner identifier in un cascaded rows (no foreign key relationship)
     * Record needs to be retained
     * @param string $new           New identifier
     * @param string $original      Old identifier
     * @return bool|mysqli_result
     */
    public function updateExaminerID($new, $original)
    {

        $newSan = $this->db->clean($new);
        $orgSan = $this->db->clean($original);

        $results = $this->db->query(
            "UPDATE student_results SET examiner_id = '$newSan' 
              WHERE examiner_id = '$orgSan'"
        );

        $ownerships = $this->db->query(
            "UPDATE self_assessment_ownerships SET examiner_id = '$newSan' 
              WHERE examiner_id = '$orgSan'"
        );

        return ($results && $ownerships);

    }

    //Add Examiners To Term [Updated 05/02/2014]
    public function addExaminersToTerm($examiners, $term_id)
    {
        $sql = "INSERT INTO examiner_department_term (user_id, dept_id, term_id) VALUES";

        //Cycle through Examiners and Departments
        foreach ($examiners as $examiner) {
            /* If the user doesn't exist, then there's no point in doing any
             * more about them...
             */
            if (!$this->doesUserExist($examiner)) {
                continue;
            }

            $depts = $this->db->departments->getExaminerDepartmentIDs($examiner);
            foreach ($depts as $dept) {
                /* Add the examiner to the list if they're not already in the
                 * department.
                 */
                $examiner_dept_query = "SELECT COUNT(*) FROM examiner_department_term WHERE "
                    . "user_id = '$examiner' AND dept_id = '$dept' AND term_id = '$term_id'";

                if (CoreDB::single_result($this->db->query($examiner_dept_query)) == 0) {
                    $sql .= " ('" . $examiner . "', '" . $dept . "', '" . $term_id . "'),";
                }
            }
        }

        return $this->db->query(substr_replace($sql, '', -1));
    }

    /*
     * Import Examiner Data
     */
    public function importExaminerData($exaInsert, $exaUpdate, $exadeptInsert)
    {

        $importStats = [
            'complete' => false,
            'errorMsg' => '',
            'updatedExaminers' => [],
            'insertedExaminers' => []
        ];

        // Insert Examiners
        $importStats['insertedExaminers'] = $this->insertExaminers($exaInsert);

        // Dept data of the Examiners is going to be inserted
        $this->insertExaminersDepartments($exadeptInsert, $_SESSION['cterm']);

        // Updated Examiners
        $importStats['updatedExaminers'] = $this->updateExaminers($exaUpdate);
        $importStats['complete'] = true;

        return $importStats;

    }

    //Update Examiner [Updated 07/04/2013 Added 'contact_number' column]
    /* TODO: Fix the import features for examiners etc. so that they correctly
     * import all necessary roles correctly.
     */
    public function updateExaminers($exaUpdate)
    {

        $examiner_roles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, true);
        $valid_roles = $examiner_roles->getRoles();
        $updatedIds = [];

        foreach ($exaUpdate as $userID => $updateFields) {

            $this->db->clean_array($updateFields);
            $updateFieldCount = 0;

            /* Name */
            if (strlen($updateFields[1]) == 0) {

                $forenamesql = '';

            } else {

                $forenamesql = "forename = '" . $updateFields[1] . "',";
                $updateFieldCount++;

            }

            if (strlen($updateFields[2]) == 0) {

                $snamesql = '';

            } else {

                $snamesql = "surname = '" . $updateFields[2] . "',";
                $updateFieldCount++;

            }

            /* Password */
            if (strlen($updateFields[3]) == 0) {

                $passwordsql = '';

            } else {

                $passwordsql = "password = '" . $this->bCrypt($updateFields[3]) . "',";
                $updateFieldCount++;

            }

            /* Email */
            if (strlen($updateFields[4]) == 0) {

                $emailsql = "";

            } else {

                $emailsql = "email = '" . $updateFields[4] . "',";
                $updateFieldCount++;

            }

            /* Contact Num */
            if (strlen($updateFields[5]) == 0) {

                $contactsql = "";

            } else {

                $contactsql = "contact_number = '" . $updateFields[5] . "',";
                $updateFieldCount++;

            }

            /* Level/Role */
            $user_role_field = trim(mb_strtolower($updateFields[6]));

            /* By default, this falls back on the examiner role being the
             * default if it can't match the level (role) specified to one it
             * knows about.
             */
            $user_role_sql = "user_role = '" . Role::USER_ROLE_EXAMINER . "',";

            /* Loop through each role and see if the field name (included in the
             * table explicitly for this purpose) matches what's in the imported
             * data...
             */
            foreach ($valid_roles as $role) {

                if (in_array($user_role_field, ["exam_admin", "examiner_admin", "centre_admin"])) {

                    $user_role_sql = "user_role = '3',";
                    $updateFieldCount++;

                   break;

                } else if ($role->importexport_field == $user_role_field) {

                    $user_role_sql = "user_role = '$role->role_id',";
                    $updateFieldCount++;
                    break;

                }

            }

            /* Enabled */
            $enabled = mb_strtolower($updateFields[7]);

            if (strlen($enabled) == 0) {

                $enabledsql = "";

            } elseif ($enabled == 'yes') {

                $enabledsql = "activated = '1',";
                $updateFieldCount++;

            } else {

                $enabledsql = "activated = '0',";
                $updateFieldCount++;

            }

            if ($this->doesUserExist($userID) && $updateFieldCount > 0) {

                $overallpars = $forenamesql . ' ' . $snamesql . ' ' . $passwordsql
                    . ' ' . $emailsql . ' ' . $contactsql . ' ' . $user_role_sql
                    . ' ' . $enabledsql;

                $updateQuery = "UPDATE users SET " . substr_replace($overallpars, '', -1)
                    . " WHERE user_id = '" . $this->db->clean($userID) . "'";

                if ($this->db->query($updateQuery)) {

                    $updatedIds[] = $userID;

                }

            }

        }

        return $updatedIds;

    }

    //Insert Examiners [Updated 07/04/2013 Added 'contact_number' column and training info]
    public function insertExaminers($exaInsert)
    {
        
        $examiner_roles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAMINERS, true);
        $valid_roles = $examiner_roles->getRoles();
        $insertedIds = [];

        foreach ($exaInsert as $userID => $insertFields) {

            $this->db->clean_array($insertFields);

            /* Name */
            if (strlen($insertFields[1]) == 0) {

                $forename = "X";

            } else {

                $forename = $insertFields[1];

            }

            if (strlen($insertFields[2]) == 0) {

                $surname = "Y";

            } else {

                $surname = $insertFields[2];

            }

            /* Password */
            if (strlen($insertFields[3]) == 0) {

                /* TODO: Replace this hard-coded password constant with a
                 * setting/configuration file option?
                 */
                $password = $this->bCrypt("er765tttb");

            } else {

                $password = $this->bCrypt($insertFields[3]);

            }

            $passwordSql = "password = '" . $password . "',";

            /* Email */
            if (strlen($insertFields[4]) == 0) {

                $email = "unknown@unknown.com";

            } else {

                $email = $insertFields[4];

            }

            /* Contact Num */
            if (strlen($insertFields[5]) == 0) {

                $contact_number = "";

            } else {

                $contact_number = $insertFields[5];

            }

            /* Level/Role */
            $user_role_field = mb_strtolower($insertFields[6]);
            /* TODO: Does the import tool need some sort of error log or similar
             * to show how well it did importing stuff?
             */
            /* By default, this falls back on the examiner role being the
             * default if it can't match the level (role) specified to one it
             * knows about.
             */
            $user_role = Role::USER_ROLE_EXAMINER;
            /* Loop through each role and see if the field name (included in the
             * table explicitly for this purpose) matches what's in the imported
             * data...
             */
            foreach ($valid_roles as $role) {
               
               if (in_array($user_role_field, ["exam_admin", "examiner_admin", "centre_admin"])) {
                   
                   $user_role = 3;
                   break;
                                
               } else if ($role->importexport_field == $user_role_field) {
                   
                   $user_role = $role->role_id;
                   break;

               }

            }

            /* Enabled */
            $enabled_field = mb_strtolower($insertFields[7]);
            $enabled = (($enabled_field === 'yes') ? 1 : 0);
            $userIDSan = $this->db->clean($userID);

            $sql = "INSERT into users SET user_id = '" . $userIDSan . "', forename = '" . $forename . "', "
                 . "surname = '" . $surname . "', " . $passwordSql . " contact_number = '" . $contact_number . "', "
                 . "email = '" . $email . "', user_role = '" . $user_role . "', activated = '" . $enabled . "'";

            /* If the user already exists, this may throw an ErrorException.
             * Need to catch that just in case.
             */
            try {

                $result = $this->db->query($sql);

                if ($result) {

                    $insertedIds[] = $userID;

                    // Insert trained row
                    $this->db->query(
                        "INSERT INTO examiner_training (user_id) VALUES ('{$userIDSan}')"
                    );

                }

            } catch (ErrorException $e) {

                error_log(__METHOD__ . ": Failed to insert user ' " .
                    $userID . "': Already exists or other error"
                );

                continue;

            }

        }

        return $insertedIds;

    }

    //Insert Examiners Departments [Updated 11/02/2014]
    public function insertExaminersDepartments($exadeptInsert, $term_id)
    {
        $insertCount = 0;
        foreach ($exadeptInsert as $values) {

            $user_id = $this->db->clean($values[0]);
            $dept_id = $this->db->clean($values[1]);
            $term_id = $this->db->clean($term_id);

            $q = "INSERT INTO examiner_department_term (user_id, dept_id, term_id) "
                . "VALUES ('" . $user_id . "', '" . $dept_id . "', '" . $term_id . "')";

            if ($this->db->query($q)) {
                $insertCount++;
            }
        }

        return $insertCount;
    }

    /**
     * Get all administrators (super, system, school admins)
     * 
     * @param  array  $fields  fields to include
     * @return mysqli_result   list of administrators
     */
    public function getAllAdmins($fields = null)
    {
       // No required fields passed in then use the default list
       if ($fields == null) {
        
         $fields = [
            "user_id", "surname", "forename", "email",
            "password", "r.role_name as user_role",
            "contact_number", "last_login", "activated",
            "created_at", "users.user_role as user_role_id",
         ];
         
       }
        
       $userRoles = [
          Role::USER_ROLE_SCHOOL_ADMIN
       ];
       
       $schoolsJoin = $schoolsWhere = "";

       $sql = "SELECT %s FROM users INNER JOIN roles "
            . "AS r ON (users.user_role = r.role_id) %s "
            . "WHERE user_role IN (%s) %s "
            . "GROUP BY user_id "
            . "ORDER BY created_at ASC";
       
       // Super Admin Role
       if ($_SESSION['user_role'] == Role::USER_ROLE_SUPER_ADMIN) {
            
            $userRoles[] = Role::USER_ROLE_SUPER_ADMIN;
            $userRoles[] = Role::USER_ROLE_ADMIN;
            
       // System Admin Role
       } else if ($_SESSION['user_role'] == Role::USER_ROLE_ADMIN) {
            
            $userRoles[] = Role::USER_ROLE_ADMIN;
            
       // School Admin Role
       } else if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {
  
          $schoolIDs = array_unique(
              array_column(
                  $this->db->schools->getAdminSchools($_SESSION['user_identifier']), 
                 'school_id'
             )
          );
                
          $schoolsJoin = "INNER JOIN school_admins USING (user_id) ";          
          $schoolsWhere = "AND school_id IN (" . implode(",", $schoolIDs) . ") ";
          
       }
              
       $sql = sprintf(
           $sql,
           implode(',', $fields),
           $schoolsJoin,
           implode(',', $userRoles),
           $schoolsWhere
       );
       
       return $this->db->query($sql);
       
    }
    
    /**
     * Get system administrators
     * 
     * @return mysqli_result   list of system administrators
     */
    public function getSystemAdmins() {
      return $this->db->query(
         "SELECT users.user_id, surname, forename
          FROM users WHERE users.user_role = 
          '" . Role::USER_ROLE_ADMIN . "'"
       );
    }

    /**
     * Insert a new administrator
     *
     * @param  string  $user_id  Desired Username/User ID of new administrator
     * @param  string  $forename    Given/First name
     * @param  string  $surname    Family/Sur name
     * @param  string  $email    E-mail address
     * @param  string  $password Password (in plaintext, will be encrypted before getting to database)
     * @param  int     $user_role    User level (role) ID
     * @param  int     $status   1 = Enabled, 0 = Disabled
     * @return boolean TRUE if the admin was created, FALSE if not.
     */
    public function insertAdmin($user_id, $forename, $surname, $email, $password, $user_role, $status)
    {
        $user_id = $this->db->clean($user_id);
        $forename = $this->db->clean($forename);
        $surname = $this->db->clean($surname);
        $email = $this->db->clean($email);
        $password = $this->bCrypt($this->db->clean($password));
        $user_role = $this->db->clean($user_role);
        $status = $this->db->clean($status);

        $sql = "INSERT INTO users (user_id, surname, forename, email, password, user_role, activated) "
                . "VALUES ('$user_id', '$surname', '$forename', '$email', '$password', '$user_role', '$status')";

        $success = $this->db->query($sql);
        if ($success) {
            // (DW) Logging that an event happened should only take place if the
            // event actually, so moved this here.
            \Analog::info($_SESSION['user_identifier'] . " created administrator with identifier $user_id");
        }

        return $success;
    }
        
    /**
     * Delete administrators from the system
     *
     * @param  string[] $admins List of user IDs of administrators to delete
     * @return int      Number of administrators successfully deleted.
     */
    public function deleteAdmins($admins)
    {
        global $config;
        $user_roles_admin = new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS);
        $admin_role_list = implode(", ", $user_roles_admin->role_ids);
        $deleted_count = 0;
        
        foreach ($admins as $user_id) {
            $user_id = $this->db->clean($user_id);
            if ($_SESSION['user_identifier'] == $user_id) {
                // Only delete user if not the currently logged-in user!
                continue;
            }
            
            // Delete the user from the user table first...
            $delete_user_query = "DELETE FROM users WHERE user_id = '$user_id' "
                . "AND (user_role IN (" . $admin_role_list . "))";
            $success = $this->db->query($delete_user_query);
            
            if ($success) {
                // Only log something if it actually happened...
                \Analog::info(
                    $_SESSION['user_identifier'] . " deleted administrator with identifier "
                    . $user_id . " from " . $config->getName()
                );
                $deleted_count++;
            }
            
            //Delete In Case Account Contained in Examiners List
            $delete_edt_query = "DELETE FROM examiner_department_term WHERE user_id = '$user_id'";
            $this->db->query($delete_edt_query);
        }

        return $deleted_count;
    }

    //Update admin in system [Updated 05/02/2014]
    public function updateAdmin($edit_id, $org_id, $edit_fn, $edit_sn, $edit_email, $edit_password, $change_password, $edit_user_role, $edit_acc)
    {
        $edit_id = $this->db->clean($edit_id);
        $org_id = $this->db->clean($org_id);
        $edit_fn = $this->db->clean($edit_fn);
        $edit_sn = $this->db->clean($edit_sn);
        $edit_email = $this->db->clean($edit_email);
        $edit_password = $this->db->clean($edit_password);
        $edit_acc = $this->db->clean($edit_acc);
        $change_password = $this->db->clean($change_password);
        $edit_user_role = $this->db->clean($edit_user_role);

        if ($change_password == 1) {
            $edit_password = $this->bCrypt($edit_password);
            $passwordSql = "password = '" . $edit_password . "',";
        } else {
            $passwordSql = "";
        }

        if ($_SESSION['user_identifier'] == $org_id) {
            //If Current Logged On User
            $sql = "UPDATE users SET user_id = '" . $edit_id . "', surname = '"
                . $edit_sn . "', forename = '" . $edit_fn . "', " . $passwordSql
                . " email = '" . $edit_email . "' "
                . "WHERE users.user_id = '" . $org_id . "'";
        } else {
            $sql = "UPDATE users SET user_id = '" . $edit_id . "', surname = '"
                . $edit_sn . "', forename = '" . $edit_fn . "', email = '"
                . $edit_email . "', " . $passwordSql . " user_role = '"
                . $edit_user_role . "', activated = '" . $edit_acc . "' "
                . "WHERE users.user_id = '" . $org_id . "'";
        }

        return ($this->db->query($sql));
    }

    //Get All Exam Assistants [Updated 05/02/2014]
    public function getAllAssistants()
    {
        $sql = "SELECT user_id, forename, surname, email, contact_number, LOWER(r.role_name) "
            . "AS `user_role` FROM users INNER JOIN roles AS r ON (users.`user_role` = r.role_id)"
            . "WHERE `user_role` = " . Role::USER_ROLE_EXAM_ASSISTANT;

        return CoreDB::into_array($this->db->query($sql));
    }

    /**
     * Insert assistant (in User Table) [Updated 28/09/2015] 
     * 
     * @param string $user_id       Username
     * @param string $firstname     First/Given Name
     * @param string $surname       Surname/Family Name
     * @param string $email         Email Address
     * @param string $contact_number   Contact Telephone Number
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function insertAssistant($user_id, $firstname, $surname, $email, $contact_number)
    {
        //Clean Parameters
        list($user_id, $firstname, $surname, $email, $contact_number) = array_map(
            "\OMIS\Database\CoreDB::clean",
            func_get_args()
        );
        
        /* (DW) Fixes issue #376 that assistants are being created with no
         * assigned password. Since assistants are not meant to be allowed to
         * log in, then if we set their password to something massive and 
         * hopefully unguessable, then that should be good enough as a temporary
         * fix.
         * @TODO Either rethink how assistants work or figure out a less ugly
         * fix for this.
         */
        // Generate a reasonably complex, random password... 32 typeable characters
        $password = \OMIS\Utilities\Random::bytes(
            32,
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@£$%^&*(){}[]:;'|~`<,>.?/\"\\"
        );
        $hashedPassword = $this->bCrypt($password);

        $sql = "INSERT INTO users (user_id, password, surname, forename, email, contact_number, "
            . "user_role, activated) VALUES ('" . $user_id . "', '"
            . $hashedPassword . "', '" . $surname . "', '" . $firstname . "', '"
            . $email . "', '" . $contact_number . "', " . Role::USER_ROLE_EXAM_ASSISTANT
            . ", '0')";

        //Run Query
        $result = $this->db->query($sql);

        if ($result) {
            \Analog::info(
                $_SESSION['user_identifier'] . " created assistant with identifier "
                . $user_id
            );
        }
        return $result;
    }

    //Remove Assistants [Updated 10/02/2014]
    public function deleteAssistants($toDelete)
    {
        global $config;
        $n = count($toDelete);
        $deleteCount = 0;
        $i = 0;

        while ($i < $n) {
            $user_id = $this->db->clean($toDelete[$i]);
            $sql = "DELETE FROM users WHERE user_id = '" . $user_id . "' AND user_role = "
                . Role::USER_ROLE_EXAM_ASSISTANT;

            if ($this->db->query($sql)) {
                \Analog::info(
                    $_SESSION['user_identifier'] . " deleted assistant with identifier "
                    . $user_id . " from " . $config->getName()
                );
                $deleteCount++;
            }

            $i++;
        }

        return $deleteCount;
    }

    /**
     * Return a list of all roles known to the system
     *
     * Simple wrapper function that iterates through all of the defined roles
     * and returns an array of Role objects representing the specified roles.
     *
     * @param int[]  $role_list Array of Role IDs to look up. Leave as null for all roles.
     * @param string $sort_by   (Optional) Field name to sort results by.
     * @param int    $sort_dir  Order of sort (Either SORT_ASC or SORT_DEC)
     *
     * @return \Role[] array of Role objects.
     * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
     */
    public function getRoles($role_list = null, $sort_by = null, $sort_dir = SORT_ASC)
    {
        $roles = [];

        /* If $sort_by is set, then pass the current list of Role IDs to the
         * database to get sorted in the correct order.
         */
        if (is_null($sort_by)) {
            $sort_by = "role_id";
        }

        switch ($sort_dir) {
            case SORT_DESC:
                $direction = " DESC";
                break;
            case SORT_ASC:
            default:
                /* Default is ascending, and we don't need to specify this
                 * in the query.
                 */
                $direction = "";
        }

        if (empty($role_list)) {
            $limit_clause = "";
        } else {
            $role_id_list = implode(", ", $role_list);
            $limit_clause = " WHERE `role_id` IN ($role_id_list)";
        }

        $sql = "SELECT `role_id` FROM `roles` $limit_clause ORDER BY `$sort_by`$direction;";

        /* Assuming we get some results, hopefully put them into a neat little
         * array.
         */
        $mysqliResult = $this->db->query($sql);
        if ($mysqliResult) {
           $resultArray = CoreDB::into_array($mysqliResult);
           $role_list = array_keys(
               CoreDB::group($resultArray, ['role_id'], true)
           );
        }

        foreach ($role_list as $role_id) {
            $roles[] = \OMIS\Auth\Role::loadID($role_id);
        }

        return $roles;
    }

    /**
     * Check that the supplied password is correct. If it uses the old method
     * of storing passwords (an unsalted, bare MD5 hash) then it updates it to
     * the latest method.
     *
     * @global OMIS\Config $config OMIS Configuration settings
     * @param  string  $userID  User ID whose password is to be checked
     * @param  string  $password Plaintext password as received from login form
     * @return boolean TRUE if password is correct, FALSE if not.
     */
    public function checkPassword($userID, $password)
    {
        /* $config is instantiated with the class so should exist when this
         * non-static function is called.
         */
        global $config;
        $userIDSan = $this->db->clean($userID);

        // Get the actual password stored in the database.
        $sql = "SELECT password FROM users WHERE user_id = '$userIDSan'";
        $storedHash = CoreDB::single_result($this->db->query($sql));

        /* password_verify(), password_hash() etc. are built-in from PHP 5.5 up.
         * For other versions, we're autoloading a compatibility layer via
         * composer (ircmaxwell/password_compat).
         */
        if (password_verify($password, $storedHash)) {
            // Password is correct.
            return true;
        } elseif ($storedHash === md5($password)) {
            /* Password is correct but is stored as a "raw" unsalted MD5 hash.
             * Need to fix that.
             */
            $rehashedPassword = password_hash(
                $password,
                PASSWORD_DEFAULT,
                array("cost" => $config->passwords['hash_cost'])
            );
            
            $sql = "UPDATE users SET password = '$rehashedPassword' " .
                   "WHERE user_id = '$userIDSan'";
            $success = $this->db->query($sql);
            
            if (!$success) {
                error_log(__METHOD__ . ": Failed to update password to new hashed version!");
            }

            return true;
        } else {
            return false;
        }
    }

    //Confirm IP Address [Updated 11/02/2014]
    public function confirmIPAddress($ip_address, $attempts_t, $min, $username)
    {
        list($ip_address, $attempts_t, $min, $username) = array_map(
            "\OMIS\Database\CoreDB::clean",
            func_get_args()
        ); //Clean Parameters

        $sql = "SELECT attempts, (CASE WHEN last_login IS NOT NULL AND "
                . "DATE_ADD(last_login, INTERVAL '" . $min . "' MINUTE) > NOW() THEN 1 "
                . "ELSE 0 END) AS denied FROM login_tracking WHERE ip = '" . $ip_address . "' "
                . "AND username = '" . $username . "'";

        $data = $this->db->fetch_row($this->db->query($sql));
        $attempts_t++;

        //Verify that at least one login attempt is in database
        if (!$data) {
            return false;
        } elseif ($data["attempts"] >= ($attempts_t - 1)) {
            if ($data["denied"] == 1) {
                return true;
            } else {
                $this->clearLoginAttempts($ip_address, $username);
                return false;
            }
        } else {
            return false;
        }
    }

    //Add login attempts [Updated 11/02/2014]
    public function addLoginAttempt($ip_address, $username)
    {
        // Clean Parameters
        list($ip_address, $username) = array_map(
            "\OMIS\Database\CoreDB::clean",
            func_get_args()
        );

        $sql = "SELECT * FROM login_tracking WHERE (ip = '" . $ip_address . "' AND "
                . "username = '" . $username . "')";

        $data = $this->db->fetch_row($this->db->query($sql));

        if ($data) {
            $attempts = $data["attempts"] + 1;

            $sql = "UPDATE login_tracking SET attempts = " . $attempts . ", last_login = NOW() "
                    . "WHERE ip = '" . $ip_address . "' AND username = '" . $username . "'";
            $result = $this->db->query($sql);
        } else {
            $sql = "INSERT INTO login_tracking (attempts, ip, last_login, username) "
                    . "VALUES (1, '" . $ip_address . "', NOW(), '" . $username . "')";
            $result = $this->db->query($sql);
        }

        return $result;
    }

    /**
     * Get login attempts
     * 
     * @param string $ip_address
     * @param string $username
     * @return mysqli_result login attempt records
     */
    public function getLoginAttempts($ip_address = null, $username = null)
    {
        if ($ip_address == null) {
            $sql = "SELECT * FROM login_tracking";

            /* 2014-09-04 (DW) filter out users with no failed attempts if no
             * username is specified.
             */
            if (empty($username)) {
                $sql .= " WHERE (attempts > 0)";
            }
            
            $sql .= " ORDER BY last_login DESC";
        } else {
            list($ip_address, $username) = array_map(
                "\OMIS\Database\CoreDB::clean",
                func_get_args()
            ); //Clean Parameters
            $sql = "SELECT * FROM login_tracking WHERE (ip = '" . $ip_address . "') "
                . "AND (username = '" . $username . "')";
        }
        
        return $this->db->query($sql);
    }

    //Clear Login Attempts [Updated 11/02/2014]
    public function clearLoginAttempts($ip_address = null, $username = null)
    {

        $ipExists = !is_null($ip_address);
        $unExists = !is_null($username);

        $clearSingleRecord = ($ipExists && $unExists);

        $ip_address = $this->db->clean($ip_address);
        $username = $this->db->clean($username);

        $sql = "UPDATE login_tracking SET attempts = 0 ";
        $sql .= ($ipExists || $unExists) ? "WHERE " : "";
        $sql .= ($ipExists) ? "ip = '" . $ip_address . "'" : "";
        $sql .= ($ipExists && $unExists) ? " AND " : "";
        $sql .= ($unExists) ? "username = '" . $username . "'" : "";

        $success = $this->db->query($sql);
        if ($success && !$clearSingleRecord) {
            \Analog::info(
                $_SESSION['user_identifier'] . " manually cleared all login attempts"
            );
        }

        return $success;
    }

    //Set time stamp for login [Updated 27/01/2014]
    public function setTimeStampLogin($user_id)
    {
        $user_id = $this->db->clean($user_id);
        $dates = array();
        $dates[0] = date("Y-m-d H:i:s");
        $dates[1] = strtolower(date("F j, Y, g:i a"));
        $this->db->query("UPDATE users SET last_login = '" . $dates[0] . "' WHERE user_id = '" . $user_id . "'");

        return $dates;
    }
   
    protected function getTableName() { return 'users'; }

    protected function getIds() { return ['user_id']; }

}

