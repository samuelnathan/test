<?php
/**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class StudentItemOutcomes extends AbstractEntityDAO
 {

     protected function getTableName() { return 'student_item_outcomes'; }
     protected function getIds() { return ['outcome_id']; }


     /**
      * Update the item outcome record for a student
      * @param string $studentID     Identifier number for student
      * @param int $sessionID        Identifier number for session
      * @param int $itemID           Identifier number for item
      * @param mixed[] $data         Outcome data to set
      * @return bool
      */
     public function update($studentID, $sessionID, $itemID, array $data)
     {

         $sql = 'INSERT INTO student_item_outcomes '
              . '(student_id, session_id, item_id, score, weighted_score) '
              . 'VALUES (?,?,?,?,?) '
              . 'ON DUPLICATE KEY UPDATE score = ?, weighted_score = ?';

         $dataValues = array_values($data);
         $stmt = $this->db->prepare($sql);
         $stmt->bind_param(
             'siidddd', $studentID,$sessionID, $itemID,
             ...$dataValues,...$dataValues);

         $status = $stmt->execute();
         $stmt->close();

         return $status;

     }

 }
