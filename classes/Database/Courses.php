<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */


namespace OMIS\Database;

use \OMIS\Auth\Role as Role;
use OMIS\Database\DAO\AbstractEntityDAO;

class Courses extends AbstractEntityDAO
{

    protected function getIds()
    {
        return ['course_id'];
    }

    protected function getTableName()
    {
        return 'courses';
    }

    /**
     * Get course record by course identifier
     * 
     * @param int $courseID    identifier for course
     * @return array           course record data
     */
    public function getCourse($courseID) { return $this->getById($courseID); }

 
    /**
     *  Insert Course
     * 
     * @param int $id       identifier for the course
     * @param string $name  course name
     * @return boolean
     */   
    public function insertCourse($id, $name)
    {
        
        global $config;
        $sql = "INSERT INTO courses (course_id, course_name) VALUES ('"
            . $this->db->clean($id) . "', '"
            . $this->db->clean($name) . "')";
        
        $success = $this->db->query($sql);
        if ($success) {
            \Analog::info(
                $_SESSION['user_identifier'] . " created course " . $name
                . " to " . $config->getName()
            );
        }

        return $success;

    }

    /**
     *  Update Course
     * 
     * @param int $courseID        identifier for the course
     * @param int $originalID      original identifier before the update
     * @param string $courseName   course name
     * @return boolean
     */
    public function updateCourse($courseID, $originalID, $courseName)
    {

        $idQuery = " ";
        if ($courseID != $originalID) {
            $newIDSanitized = $this->db->clean($courseID);
            $idQuery = ", course_id = '$newIDSanitized' ";
        }

        $courseNameSanitized = $this->db->clean($courseName);
        $originalIDSanitized = $this->db->clean($originalID);
        $sql = "UPDATE courses "
             . "SET course_name = '$courseNameSanitized'" . $idQuery
             . "WHERE course_id = '$originalIDSanitized'";
        if (!$this->db->query($sql)) {
            return false;
        }
        
        return true;

    }

    /**
     * Get All Courses Information
     * 
     * @param type $term ID number of term (optional)
     * @return courses[]
     */
    public function getAllCourses($term = null)
    {
       
        global $config;
        $termSql = $yearJoin = 
        $schoolsJoin = $schoolsGroupBy = "";
        
        /**
         * Filter by term (optional)
         */
        if (!is_null($term)) {
          
            $deptSql = $this->deptCouplingClause($term);
            $termSql = " AND term_id = '".$this->db->clean($term)."'";
            $yearJoin = " INNER JOIN course_years USING(course_id)";
            
        } else {
          
            $deptSql = $this->deptCouplingClause($_SESSION['cterm']);
            
        }
        
        /**
         *  We need to return school and department data 
         *  if coupling enabled
         */
        if ($config->db['couple_depts_courses']) {
          
          $schoolsJoin = "INNER JOIN departments 
             ON (courses.course_id = departments.dept_id) 
             INNER JOIN schools USING (school_id)";
          $schoolsGroupBy = "school_description, ";
          
        }
        
        $sql = "SELECT * FROM courses 
                $yearJoin $schoolsJoin 
                WHERE 1 $termSql $deptSql 
                GROUP BY $schoolsGroupBy course_id";

        return CoreDB::into_array(
            $this->db->query($sql)
        );

    }

    // Delete Courses
    public function deleteCourses($courses)
    {
        global $config;
        foreach ($courses as $course_id) {
            $course_id = $this->db->clean($course_id);
            \Analog::info($_SESSION['user_identifier'] . " deleted course with identifier " . $course_id . " from " . $config->getName());
            $this->db->query("DELETE FROM courses WHERE course_id = '" . $course_id . "'");
        }
    }

    /**
     * Update an array of courses
     */
    public function updateCourses($courses)
    {
       $result = true;
       foreach ($courses as $courseID => $courseData) {
          
         $courseName = $this->db->clean($courseData[0]);
         if (!empty($courseName)) {
           $courseID = $this->db->clean($courseID);
           $query = "UPDATE courses "
                  . "SET course_name = '" . $courseName . "' "
                  . "WHERE course_id = '" . $courseID . "'";
              
           if (!$this->db->query($query)) {
             $result = false;
           }
          
         }
          
       }
       return $result;
    }

    /*
     * Insert course
     */
    public function insertCourses($courses)
    {
        $query = "INSERT INTO courses (course_id, course_name) VALUES (";
        $courseCount = 1;
        foreach ($courses as $course_id => $course_data) {

            $course_id = $this->db->clean($course_id);
            $courseName = $this->db->clean($course_data[0]);

            if ($courseCount > 1) {
                $query = $query . ", (";
            }
            $query .= "'" . $course_id . "', '" . $courseName . "')";
            $courseCount++;
        }
        if ($courseCount > 1) {
            return $this->db->query($query);
        } else {
            return true;
        }
    }

    // Does Course Exist
    public function doesCourseExist($course_id) { return $this->existsById($course_id); }

    /**
     * Get course year record by year ID
     */
    public function getCourseYear($yearID)
    {
        $yearID = $this->db->clean($yearID);
        $query = "SELECT courses.course_id, year_id, course_name, year_name, term_id "
               . "FROM course_years " 
               . "INNER JOIN courses USING(course_id) "
               . "WHERE year_id = '" . $yearID . "'";
        return $this->db->fetch_row($this->db->query($query));
    }

    //Get Course Year ID By Year and Course
    public function getCourseYearID($course_id, $year_id, $term_id)
    {
        $result = $this->db->query("SELECT year_id FROM course_years WHERE course_id = '"
                . $this->db->clean($course_id) . "' AND year_name = '"
                . $this->db->clean($year_id) . "' AND term_id = '" 
                . $this->db->clean($term_id) . "'");
        if (mysqli_num_rows($result) == 0) {
            return false;
        } else {
            return CoreDB::single_result($result);
        }
    }

    // Insert Course Year
    public function insertCourseYear($course_id, $year_name, $term_id)
    {

        $sql = "INSERT INTO course_years (course_id, year_name, term_id) " .
                "VALUES ('" . $this->db->clean($course_id) . "', '"
                . $this->db->clean($year_name) . "', '"
                . $this->db->clean($term_id) . "')";

        if (!$this->db->query($sql)) {
            return false;
        }
        \Analog::info($_SESSION['user_identifier'] . " inserted course year with identifier " 
                . $year_name . " into course "
                . $course_id . " and term "
                . $term_id);
        return true;
    }

    //Delete Course Years
    public function deleteCourseYears($course_years)
    {
        $course_year_list = implode(', ', array_map('intval', $course_years));
        $query = "DELETE FROM course_years WHERE year_id IN ($course_year_list)";
        
        $success = $this->db->query($query);
        
        if ($success) {
            \Analog::info($_SESSION['user_identifier'] . " deleted course years with these IDs: [$course_year_list]");
        } else {
            $log_message = $_SESSION['user_identifier'] . " failed to delete course years with these IDs: [$course_year_list]";
            \Analog::error($log_message);
            error_log(__METHOD__ . ": $log_message");
        }
    }

    //Update Course Year
    public function updateCourseYear($year_id, $year_name)
    {
        $query = "UPDATE course_years SET year_name = '" . $this->db->clean($year_name)
            . "' WHERE year_id = '" . $this->db->clean($year_id) . "'";
        // @TODO: (DW) Add logging here?
        return $this->db->query($query);
    }

    /**
     * Does Course Year Exist
     * 
     * @param int $yearID
     * @param string $course
     * @param string $year
     * @param string $term
     * @return boolean
     */
    public function doesCourseYearExist($yearID = null, $course = null, $year = null, $term = null)
    {
        
        if ($yearID === null && $course === null && $year === null && $term === null) {
          
           return false;
           
        }
      
        $qvalues = ($yearID === null) ? '' : "year_id = '" . $this->db->clean($yearID) . "' AND ";
        $qvalues .= ($course === null) ? '' : "course_id = '" . $this->db->clean($course) . "' AND ";
        $qvalues .= ($year === null) ? '' : "year_name = '" . $this->db->clean($year) . "' AND ";
        $qvalues .= ($term === null) ? '' : "term_id = '" . $this->db->clean($term) . "' AND ";

        $sql = "SELECT COUNT(*) FROM course_years WHERE " . substr_replace($qvalues, '', -5);
        
        if (CoreDB::single_result($this->db->query($sql)) > 0) {
          
            return true;
            
        } else {
        
           return false;
           
        }
      
    }

    // Insert Course Years
    public function insertCourseYears($year_names, $term_id)
    {
        
        $sql = "INSERT INTO course_years (course_id, year_name, term_id) VALUES (";
        $count = 1;

        // Loop through course years
        foreach ($year_names as $year_name => $year_name_data) {

            $course_id = $this->db->clean($year_name_data[0]);
            $year_name = $this->db->clean($year_name_data[1]);
            $term_id = $this->db->clean($term_id);

            // Does course year exist
            if (!$this->doesCourseYearExist(null, $course_id, $year_name)) {

                if ($count > 1) {

                    $sql .= ", (";

                }

                $sql .= "'" . $course_id . "', '" . $year_name . "', '" . $term_id . "')";

                $count++;

            }

        }
        
        // Run query and return
        if ($count > 1) {

            return $this->db->query($sql);

        } else {

            return true;

        }

    }

    /**
     * Get module count by the Course Year ID
     */
    public function getCourseYearModuleCount($courseYearID)
    {
        $sql = "SELECT COUNT(*) FROM year_modules WHERE year_id = "
             . $this->db->clean($courseYearID) . " ORDER BY module_id";
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Get courses, years and modules
     * @param string $term   term identifier 
     * @return []            array of course years and modules
     */
    public function getCoursesYearsModules($term) {
       
       $deptSql = $this->deptCouplingClause($term);

       $sql = "SELECT course_years.year_id, courses.course_id, courses.course_name, "
            . "course_years.year_name, module_id, module_name "
            . "FROM ((courses INNER JOIN course_years USING(course_id)) "
            . "INNER JOIN year_modules USING(year_id)) INNER JOIN modules USING(module_id) "
            . "WHERE course_years.term_id = '$term' "
            . $deptSql
            . "ORDER BY courses.course_name, course_years.year_name, "
            . "LENGTH(modules.module_id), modules.module_id";

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['course_id'], true, false);

    }

    /**
     * Get a list of Course Years, optionally filtered by course ID and/or term ID.
     * 
     * @param string $course_id (Optional) course ID.
     * @param string $term_id (Optional) term ID.
     * @param bool $convertToArray if TRUE, convert results to associative array.
     * @return mixed[]|mysqli_result
     */
    public function getCourseYears($course_id = null, $term_id = null, $convertToArray = true)
    {
        // Build any non-empty WHERE clause parameters to the query. (the use of double
        // dollar signs is entirely intended here!
        $params = [];
        foreach (['course_id', 'term_id'] as $param) {
            if (empty($$param)) {
                continue;
            }
            $params[] = "$param = '" . $this->db->clean($$param) . "'";
        }
        
        // Start building the query.
        $query = "SELECT * FROM course_years INNER JOIN terms USING (term_id) ";
        if (!empty($params)) {
            // If there are parameters, then assemble the WHERE clauses to filter by...
            $query .= "WHERE " . implode(" AND ", $params) . " ";
        }
        $query .= "GROUP BY year_id ORDER BY ";
        
        // If we're not specifying the term, let's sort the returns by term and then by year.
        if (!empty($term_id)) {
            $query .= "terms.created_at DESC, ";
        }
        $query .= "year_name";
        $results = $this->db->query($query);
        
        return ($convertToArray) ? CoreDB::into_array($results) : $results;
    }

    //Import Courses, Years and module data
    public function importCourseModuleData($crsInsert, $crsUpdate, $crsYearInsert, $modInsert, $modUpdate, $modyrInsert)
    {
        $importStats = array('complete' => false,
            'errorMsg' => '',
            'updatedCourses' => 0,
            'insertedCourses' => 0,
            'insertedYears' => 0,
            'insertedModules' => 0,
            'updatedModules' => 0,
            'insertedModuleYears' => 0
        );

        //First start a mysql transaction so that we can perform a rollback in case there are errors during import
        $this->db->query('START TRANSACTION');

        global $config;
        if (!isset($config)) {
          $config = \OMIS\Config::getInstance();
        }
        
        // If courses are coupled with departments, then ignore insert|update courses
        if ($config->db['couple_depts_courses'] || ($this->insertCourses($crsInsert) && $this->updateCourses($crsUpdate))) {
            
            $importStats['insertedCourses'] = $config->db['couple_depts_courses'] ? 0 : count($crsInsert);
            $importStats['updatedCourses'] = $config->db['couple_depts_courses'] ? 0 : count($crsUpdate);
            
            if ($this->insertCourseYears($crsYearInsert, $_SESSION['cterm'])) {
                $importStats['insertedYears'] = mysqli_affected_rows($this->db->getConnection());
                if ($this->insertModules($modInsert) && $this->updateModules($modUpdate)) { //Insert Modules & Update Modules
                    $importStats['insertedModules'] = count($modInsert);
                    $importStats['updatedModules'] = count($modUpdate);

                    if ($this->insertYearsModules($modyrInsert, $_SESSION['cterm'])) { //Insert Module Years
                        $this->db->query('COMMIT');
                        $importStats['insertedModuleYears'] = count($modyrInsert);
                        $importStats['complete'] = true;
                    } else {
                        $this->db->query('ROLLBACK');
                        $importStats['complete'] = false;
                        $importStats['errorMsg'] = "Couldn't insert module years, importing failed";
                        return $importStats;
                    }
                } else {
                    $this->db->query('ROLLBACK');
                    $importStats['complete'] = false;
                    $importStats['errorMsg'] = "Couldn't insert modules, importing failed";
                    return $importStats;
                }
            } else {
                $this->db->query('ROLLBACK');
                $importStats['complete'] = false;
                $importStats['errorMsg'] = "Couldn't insert course years, importing failed";
                return $importStats;
            }
        } else {
            $this->db->query('ROLLBACK');
            $importStats['complete'] = false;
            $importStats['errorMsg'] = "Couldn't insert courses, importing failed";
            return $importStats;
        }
        return $importStats;
    }

    /*
     * Get Course Year Module Table Headers
     */
    public function getYearModuleColumns()
    {
        
        global $config;
        $dbName = $config->db['schema'];
        
        $sql = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE "
             . "TABLE_NAME IN ('courses', 'course_years', 'modules') "
             . "AND TABLE_SCHEMA = '" . $dbName . "' "
             . "AND COLUMN_NAME NOT IN ('year_id', 'term_id', 'updated_at', 'created_at')";

        return CoreDB::into_values($this->db->query($sql));

    }

    //Get All Modules
    public function getAllModules()
    {
        return $this->db->query("SELECT * FROM modules");
    }

    /**
     * Get All Module Years
     */
    public function getModulesYears($termID = null)
    {
        $termsql = ($termID === null) ? '' : " WHERE term_id = '$termID'";
        $sql = "SELECT year_modules.module_id, year_modules.year_id, course_id, term_id, year_name "
             . "FROM year_modules INNER JOIN course_years USING(year_id)" . $termsql;
        return $this->db->query($sql);
    }

    /**
     * Get modules by the course year ID
     */
    public function getModulesByCourseYear($yearID, $first_order)
    {
        $yearID = $this->db->clean($yearID);
        $query = "SELECT modules.module_id, module_name, year_modules.year_id "
               . "FROM modules "
               . "INNER JOIN year_modules USING(module_id) "
               . "WHERE year_id = '" . $yearID . "' "
               . "ORDER BY " . $first_order;
        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Get modules by course ID and term ID
     */
    public function getModulesByCourse($courseID, $termID)
    {

       $courseID = $this->db->clean($courseID);
       $termID = $this->db->clean($termID);
       $query = "SELECT year_modules.year_id, year_modules.module_id, modules.module_name "
              . "FROM (course_years " 
              . "INNER JOIN year_modules USING(year_id)) "
              . "INNER JOIN modules USING(module_id) "
              . "WHERE course_years.course_id = '" . $courseID . "' "
              . "AND course_years.term_id = '" . $termID . "'";
       
       return CoreDB::into_array($this->db->query($query));
    }

    //Clone Term Course Years & Modules
    public function cloneTermCourseYearsModules($new_term_id, $previous_term_id)
    {
        $success = false;
        $yrsq_start = "INSERT INTO course_years (course_id, term_id, year_name) VALUES ";
        $modsq_start = "INSERT INTO year_modules (year_id, module_id) VALUES ";
        $yrs = $this->getCourseYears(null, $previous_term_id, false);

        //Clone Course Years
        while ($yr = mysqli_fetch_assoc($yrs)) {
            if ($this->db->query($yrsq_start . "('" . $yr['course_id'] . "', '" . $this->db->clean($new_term_id) . "', '" . $yr['year_name'] . "')")) {
                //Clone Course Year Modules
                $new_yr_id = $this->db->inserted_id();
                $modules = $this->getModulesByCourseYear($yr['year_id'], 'module_name');
                foreach ($modules as $module) {
                    if (!$this->db->query($modsq_start . "('" . $new_yr_id . "', '" . $module['module_id'] . "')")) {
                        $success = false;
                    }
                }
            } else {
                $success = false;
            }
        }
        return $success;
    }

    /**
     *  Remove modules from system
     * @param array $modules     list of modules
     * @param int $yearID        ID number for course year
     */
    public function deleteModules($modules, $yearID)
    {
        $yearID = $this->db->clean($yearID);

        foreach ($modules as $moduleID) {
            $moduleID = $this->db->clean($moduleID);

            // Remove module from course year
            $query1 = "DELETE FROM year_modules WHERE module_id = '$moduleID' AND year_id = '$yearID'";
            $removedFromYear = $this->db->query($query1);
            
            // Remove module from student modules table
            $query2 = "DELETE FROM student_courses WHERE module_id = '$moduleID' AND year_id = '$yearID'";
            $removedFromStudentModules = $this->db->query($query2);

            if ($removedFromYear && $removedFromStudentModules) {
                \Analog::info($_SESSION['user_identifier'] 
                . " removed module with ID: $moduleID from course year with ID: $yearID");
            }
            
            // Delete module from system if not attached to any other course year
            $query3 = $this->db->query("SELECT COUNT(*) FROM year_modules WHERE module_id = '$moduleID'");
            if (CoreDB::single_result($query3) == 0) {
                $this->db->query("DELETE FROM modules WHERE module_id = '$moduleID'");
            }
        }
    }

    //Insert Years Modules
    public function insertYearsModules($module_years, $term_id)
    {
        $q = "INSERT INTO year_modules (year_id, module_id) VALUES (";
        $count = 1;
        foreach ($module_years as $module_id => $mod_year) {
            list($course_id, $year_name) = explode('_', $mod_year);

            $yearID = $this->getCourseYearID($course_id, $year_name, $term_id);
            if ($yearID !== false) {
                if ($count > 1) {
                    $q .= ", (";
                }
                $q .= "'" . $yearID . "', '" . $module_id . "')";
                $count++;
            }
        }
        if ($count > 1) {
            return $this->db->query($q);
        }
        return true;
    }

    // Get all courses modules in system
    public function getCourseModules($course_id, $term_id, $year_id = null)
    {
        if (is_null($year_id)) {
            $courseYearsQ = " ";
        } else {
            $courseYearsQ = "AND course_years.year_id = '" . $year_id . "' ";
        }

        $course_id = $this->db->clean($course_id);
        $term_id = $this->db->clean($term_id);

        $sql = "SELECT course_years.year_id, course_years.course_id, course_years.year_name, "
             . "modules.module_id, modules.module_name "
             . "FROM (course_years INNER JOIN year_modules USING(year_id)) "
             . "INNER JOIN modules USING(module_id) "
             . "WHERE course_years.course_id = '" . $course_id . "' AND course_years.term_id = '" . $term_id . "' "
             . $courseYearsQ . "ORDER BY year_name, module_id";

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['year_id'], true);
    }

    /**
     * Get module
     * 
     * @param string $moduleID
     * @return bool true|false
     */
    public function getModule($moduleID)
    {
        $sql = "SELECT * FROM modules WHERE module_id = '" . $this->db->clean($moduleID) . "'";
        return $this->db->fetch_row($this->db->query($sql));
    }

    //Insert Module
    public function insertModule($module_id, $module_name, $year_id)
    {

        $module_id = $this->db->clean($module_id);
        $module_name = $this->db->clean($module_name);
        $year_id = $this->db->clean($year_id);

        $q1 = "INSERT INTO modules (module_id, module_name) VALUES ('" . $module_id . "', '" . $module_name . "')";
        $q2 = "INSERT INTO year_modules (year_id, module_id) VALUES ('" . $year_id . "', '" . $module_id . "')";

        if ($this->db->query($q1) && $this->db->query($q2)) {
            return true;
        } else {
            return false;
        }
    }
    
    //Attach Module to Year
    public function attachModuleToYear($module_id, $year_id)
    {

        $module_id = $this->db->clean($module_id);
        $year_id = $this->db->clean($year_id);

        $q = "INSERT INTO year_modules (year_id, module_id) VALUES ('" . $year_id . "', '" . $module_id . "')";
        return $this->db->query($q);
    }

    //Update Module
    public function updateModule($module_id, $org_id, $module_name)
    {
        $module_id = $this->db->clean($module_id);
        $org_id = $this->db->clean($org_id);
        $module_name = $this->db->clean($module_name);

        $q = "UPDATE modules SET module_id = '" . $module_id . "', module_name = '" . $module_name . "' WHERE module_id = '" . $org_id . "'";

        return $this->db->query($q);
    }

    /**
     * Does module exist
     * @param string $module       module identifier
     * @param int $year            course year identifier
     * @return bool exists true|false
     */
    public function doesModuleExist($module, $year = null)
    {
        $join = $clause = '';
        
        if ($year != null) {
           $join = "INNER JOIN year_modules USING (module_id)";
           $clause = " AND year_id = '" . $this->db->clean($year) . "'";
        }
        
        $sql = "SELECT COUNT(*) FROM modules $join WHERE "
             . "module_id = '" . $this->db->clean($module) . "'" . $clause;
        
        return (CoreDB::single_result($this->db->query($sql)) > 0);
    }

    /**
     * Insert Modules
     */
    public function insertModules($modules)
    {
        $query = "INSERT INTO modules (module_id, module_name) VALUES ";

        $module_values = [];
        foreach ($modules as $module_id => $mod_data) {
            $module_id = $this->db->clean($module_id);
            $module_name = $this->db->clean($mod_data[0]);

            $module_values[] = "('" . $module_id . "', '" . $module_name . "')";
        }

        if (count($module_values) > 0) {
            $query .= implode(", ", $module_values);
            return $this->db->query($query);
        } else {
            return true;
        }
    }

    /**
     * Update Modules
     */
    public function updateModules($modules)
    {
      
      // Loop through passed in modules and update each ones
      foreach ($modules as $moduleID => $moduleData) {
         if ($this->doesModuleExist($moduleID)) {
            $moduleID = $this->db->clean($moduleID);
            $moduleName = $this->db->clean($moduleData[0]);
            $nameQuery = (!empty($moduleName)) ? "module_name = '" . $moduleName . "' " : "";
            if ($nameQuery != "") { 
              $query = "UPDATE modules SET " . $nameQuery
                     . "WHERE module_id = '" . $moduleID . "'";
              if (!$this->db->query($query)) {
                return false;
              }
           }
        }
     }
    
    return true;
    }
 
    /**
     * Build department coupling sql clause
     *
     * @param  string $term  academic term/year
     * @return string  sql clause
     */
    public function deptCouplingClause($term)
    {

        global $config;
        
        // Is the coupling enabled?
        if (!$config->db['couple_depts_courses']) {
           return "";
        }
        
        // Filter for school admins
        if ($_SESSION['user_role'] == Role::USER_ROLE_SCHOOL_ADMIN) {
         
            $schools = $this->db->schools->getAdminSchools(
                $_SESSION['user_identifier']
            );
            
            $ids = array_unique(array_column($schools, 'school_id'));
            return "AND course_id IN (SELECT dept_id FROM departments WHERE 
                    school_id IN (" . implode(",", $ids) . ")) ";
         
        // Filter for exam admins
        } else if ($_SESSION['user_role'] == Role::USER_ROLE_EXAM_ADMIN) {
           
            $ids = $this->db->departments->getExaminerDepartmentIDs(
               $_SESSION['user_identifier'], 
               $term
            );
            
            return "AND course_id IN ('" . implode("','", $ids) . "') ";
            
        } else {
            return "";
        }

    }

}
