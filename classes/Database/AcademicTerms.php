<?php
/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 19/11/2015
   © 2015 Qpercom Limited.  All rights reserved.
   Database function calls relating to Academic Terms
*/

namespace OMIS\Database;

use OMIS\Database\DAO\AbstractEntityDAO;

class AcademicTerms extends AbstractEntityDAO
{

    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds() { return ['term_id']; }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName() { return 'terms'; }

    /**
     * Get default term
     * 
     * @return array default term
     */
    public function getDefaultTerm()
    {
        // Get Term Count
        $count = CoreDB::single_result($this->db->query("SELECT * FROM terms"));
        
        // No terms found in the system then add one
        if ($count == 0) {
            
            $currentYear = date('Y');
            $pastYear = $currentYear - 1;
            $shortYear = substr($currentYear, -2);
            $termID = $pastYear . '-' . $shortYear;
            $termDescription = 'Term ' . $pastYear . '-' . $currentYear;  
                    
            // Default start date
            $startDateObj = \Carbon\Carbon::now();
            $startDate = $startDateObj->format('Y-m-d');

            // Default end date
            $endDate = $startDateObj->addYears(1)->subDay(1)->format('Y-m-d');                    
            
            $termInserted = $this->insertTerm(
               $termID,
               $termDescription,
               $startDate,
               $endDate,
               1
            );
            
            if ($termInserted) {
                return ['term_id' => $termID];
            } else {
                return null;
            }
            
        // Return default term
        } else {
            
            $sql = "SELECT * FROM terms WHERE default_term = '1' LIMIT 1";
            $mysqliResult = $this->db->query($sql);
            if (mysqli_num_rows($mysqliResult) == 0) {
                $this->rearrangeTerms();
            } 
            
            // If no default term set then set it
            return $this->db->fetch_row($mysqliResult);
            
        }
    }

    /**
     * Get a list of all terms in the database.
     * 
     * @param string|string[] $order_columns String or array of strings containing 
     *                                       list of columns to order by
     * @return mixed[] Array of term data
     */
    public function getAllTerms($order_columns = ['term_id', 'created_at'])
    {

        if (is_array($order_columns)) {

            $order_columns = implode(",", $order_columns);

        }
        
        $sql = "SELECT * FROM terms ORDER BY " 
             . $this->db->clean($order_columns);
        
        $termRecords = [];
        $mysqliResults = $this->db->query($sql);
        while ($mysqliResult = $this->db->fetch_row($mysqliResults)) {

            $termID = $mysqliResult['term_id'];
            $termRecords[$termID] = $mysqliResult;

        }

        return $termRecords;

    }

    //Get Term Count [For Terms Section, Updated 27/01/2014]
    public function getTermCount()
    {
        return CoreDB::single_result($this->db->query('SELECT COUNT(*) FROM terms')); //Get term count
    }

    /**
     * Insert academic term
     * 
     * @param string $id            term identifier
     * @param string $description   description of the term 
     * @param date $startDate       start date of term 
     * @param date $endDate         end date of term 
     * @param int $defaultTerm      set as default term (yes/no)
     * @return bool true|false      success (yes/no)
     */
    public function insertTerm($id, $description, $startDate, $endDate, $defaultTerm)
    {
        $id = $this->db->clean($id);
        $description = $this->db->clean($description);
        $sql = "INSERT INTO terms (term_id, term_desc, start_date, end_date) "
             . "VALUES ('$id', '$description', '$startDate', '$endDate')";

        $success = $this->db->query($sql);
        if ($success) {
            \Analog::info($_SESSION['user_identifier'] . " added term $description");
            $this->rearrangeTerms($id, 'add', $defaultTerm);
        }

        return $success;
    }

 
    /**
     * Update academic term
     * 
     * @param string $newID          new ID
     * @param string $originalID     original ID
     * @param date $startDate        start date
     * @param date $endDate          end date
     * @param string $description    term description
     * @param int $defaultTerm       set as default term yes|no
     * @return true|false            term updated yes|no
     */
    public function updateTerm($newID, $originalID, $startDate, $endDate, $description, $defaultTerm)
    {
        
        $newIdentifier = $this->db->clean($newID);
        $originalID = $this->db->clean($originalID);
        $description = $this->db->clean($description);
        $sql = "UPDATE terms SET term_id = '$newID', term_desc = '$description', "
             . "start_date = '$startDate', end_date = '$endDate' "
             . "WHERE term_id = '$originalID'";

        // If success then rearrange the terms
        $success = $this->db->query($sql);
        if ($success) {
            $this->rearrangeTerms(
                $newIdentifier,
                'edit',
                $defaultTerm
            );
            
            // Log update
            \Analog::info(
                $_SESSION['user_identifier'] . " updated term with identifier $originalID" 
            );
        }

        return $success;
        
    }

    //Delete Term
    public function deleteTerms($terms)
    {
        global $config;
        $success = true;
        $prevent_delete = false;
        $term_delete_blocked = false;
        $cterm_set = (isset($_SESSION['cterm'])) ? true : false;
        $terms_cnt = count($terms);
        $loop_cnt = 0;
        // Do not allow for deletion of at least 1 term
        if ($this->getTermCount() == count($terms)) { 
            $prevent_delete = true;
            $_SESSION['one_term_warn'] = true;
        }

        foreach ($terms as $term_id) {
            $cleaned_term = $this->db->clean($term_id);
            $loop_cnt++;

            if ($prevent_delete && (($cterm_set && $_SESSION['cterm'] == $cleaned_term) || ($loop_cnt == $terms_cnt && !$term_delete_blocked))) {
                $term_delete_blocked = true;
                //....and do nothing
                continue;
            }

            //Delete Term
            $sql = "DELETE FROM terms WHERE term_id = '" . $cleaned_term . "';";
            //Delete Forms
            $sql .= "DELETE FROM form_department_term WHERE term_id = '" . $cleaned_term . "';";
            //Delete Exams
            $sql .= "DELETE FROM exams WHERE term_id = '" . $cleaned_term . "';";
            //Delete Course Years
            $sql .= "DELETE FROM course_years WHERE term_id = '" . $cleaned_term . "';";

            if ($this->db->multi_query($sql)) {
                \Analog::info(
                    $_SESSION['user_identifier'] . " deleted term with identifier "
                    . $cleaned_term . " from " . $config->getName()
                );
            } else {
                $success = false;
            }
        }

        return $success;
    }

    //Reshuffle default Terms [For Terms Section, Updated 27/01/2014]
    public function rearrangeTerms($set_term_id = null, $type = null, $default_term = null)
    {
        $terms = $this->getAllTerms();
        $totalCount = count($terms);
        $hasBeenSet = false;
        $currentCount = 0;
        $updateCount = 0;

        foreach ($terms as $term) {
            $currentCount++;
            if ($totalCount == 1 || ($totalCount == $currentCount && $hasBeenSet == false)) { //If only 1 term or nothing has been set
                $toSetValue = 1;
                $_SESSION['cterm'] = $term['term_id'];
            } elseif ($set_term_id == $term['term_id'] && $default_term == 1) { //If this Term is to be set
                $toSetValue = 1;
                $hasBeenSet = true;
                $_SESSION['cterm'] = $term['term_id'];
            } elseif (($type == 'add' || $type == 'edit' || $type == 'delete') && $default_term == 0) { //Do nothing and exit
                break;
            } else { //Set To 0
                $toSetValue = 0;
            }

            //Make Update
            if ($term['default_term'] != $toSetValue) {
                $q = "UPDATE terms SET default_term = '$toSetValue' WHERE term_id = '" . $term['term_id'] . "'";
                if ($this->db->query($q)) {
                    $updateCount++;
                }
            }
        }

        return $updateCount;
    }

}
