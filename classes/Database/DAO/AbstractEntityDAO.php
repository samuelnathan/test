<?php

namespace OMIS\Database\DAO;

use OMIS\Database\CoreDB;


abstract class AbstractEntityDAO implements EntityDAO
{

    /**
     * Underlying CoreDB instance
     *
     * @var CoreDB
     */
    protected $db;

    public function __construct(CoreDB $db)
    {
        $this->db = $db;
    }

    /**
     * Get a single row given its ID in the database. If the element is not found, returns null
     *
     * @param mixed ...$id
     * @return mixed
     */
    public function getById(...$id)
    {
        $query = 'SELECT * FROM ' . $this->getTableName() . ' '
               . 'WHERE ' . $this->createIdSqlCondition($id);

        return $this->db->fetch_row($this->db->query($query));
    }

    /**
     * Check if an element exists in the database given its ID
     *
     * @param mixed ...$id
     * @return boolean|null
     */
    public function existsById(...$id)
    {
        $query = 'SELECT IF(COUNT(*) > 0, 1, 0) '
               . 'FROM ' . $this->getTableName() . ' '
               . 'WHERE ' . $this->createIdSqlCondition($id);

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        else if ($result === "0") return false;
        else return null;
    }

    /**
     *
     *
     * @param mixed ...$id
     * @return bool
     */
    public function deleteById(...$id)
    {
        $query = 'DELETE FROM ' . $this->getTableName() . ' '
               . 'WHERE ' . $this->createIdSqlCondition($id);

        return $this->db->query($query);
    }

    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    abstract protected function getIds();

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    abstract protected function getTableName();

    /**
     * Creates the SQL condition to query the table by ID
     *
     * @param array $ids
     * @return string
     */
    private function createIdSqlCondition(array $ids)
    {
       $idNames = $this->getIds();
       if (count($idNames) !== count($ids))
           throw new \Exception('Number of IDs passed doesn\'t match number of IDs specified for entity');

       return implode(' AND ', array_map(function ($id, $value) {
           $value = CoreDB::clean($value);
           return "$id = '$value'";
       }, $idNames, $ids));
    }

}