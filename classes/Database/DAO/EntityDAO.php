<?php

namespace OMIS\Database\DAO;


interface EntityDAO
{

    /**
     * Get a single row given its ID in the database. If the element is not found, returns null
     *
     * @param mixed ...$id
     * @return mixed
     */
    public function getById(...$id);

    /**
     * Check if an element exists in the database given its ID
     *
     * @param mixed ...$id
     * @return boolean
     */
    public function existsById(...$id);

    /**
     * Delete an element from the database given its ID
     *
     * @param mixed ...$id
     * @return bool
     */
    public  function deleteById(...$id);

}