<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2020, Qpercom Limited
  * Database calls. No logic thought out yet (this is for outsourcing partners)
  */

 namespace OMIS\Database;
 use OMIS\Database\DAO\AbstractEntityDAO;

 class Multimedia extends AbstractEntityDAO
 {
     
     /**
      * Sample method
      */
     public function getByStation($id) {

 
        $stmt = $this->db->prepare(
            "SELECT * FROM station_multimedia 
              INNER JOIN multimedia_uploaded USING(multimedia_id) 
              WHERE station_id = ? "
        );

        $stmt->bind_param('i', $exam);
        $stmt->execute();
        $result = $stmt->get_result();

        $stmt->close();

        return $result;

     }

 }