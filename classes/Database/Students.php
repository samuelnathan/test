<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

namespace OMIS\Database;

use OMIS\Database\DAO\AbstractEntityDAO;

class Students extends AbstractEntityDAO
{

    /**
     * Gets the database record for the student specified by $student_id and

     * @param string $studentID     Identifier number for student
     * @param string $termID        Identifier number for term to locate candidate number, if any (optional)
     * @return mixed[]|null         If the student exists, its record is returned as an 
     *                              associative array; IF not, NULL is returned.
     */
    public function getStudent($studentID, $termID = null)
    {
        
        $studentID = $this->db->clean($studentID);
        $termID = $this->db->clean($termID);

        if (!is_null($termID)) {
          $join = "LEFT JOIN candidate_numbers "
                . "ON (students.student_id = candidate_numbers.student_id "
                . "AND candidate_numbers.term_id = '$termID') ";
          $field = ", candidate_numbers.candidate_number";     
        } else {
          $join = "";
          $field = "";
        }
        
        $sql = "SELECT students.*$field FROM students " . $join
             . "WHERE students.student_id = '$studentID'";
        
        return $this->db->fetch_row($this->db->query($sql));
    }

    /**
     * Gets all students in the system, optionalled ordered by the $order_by field.
     * 
     * @param mixed $order_by If a string, it's assumed to be a field name in
     * the students table.
     * @return mysqli_result|FALSE FALSE if there are no students or $order_by
     * is invalid; otherwise mysqli_result.
     */
    public function getAllStudents($order_by = \NULL)
    {
        $query = "SELECT * FROM students";
        
        if (is_string($order_by) && !empty($order_by)) {
            $query .= " ORDER BY " . $this->db->clean($order_by);
        }
        return $this->db->query($query);
    }

    /*
     * Get Students Table Headers 
     */
    public function getStudentColumns()
    {
        
        $dbName = CoreDB::single_result($this->db->query("select database()"));
        $query = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE (TABLE_NAME = 'students' " .
                "OR TABLE_NAME = 'student_courses') AND TABLE_SCHEMA = '" . $dbName . 
                "' AND COLUMN_NAME NOT IN ('year_id', 'student_image', 'updated_at', 'created_at') ";

        return CoreDB::into_values($this->db->query($query));

    }

    //Insert Student [Updated 10/02/2014]
    public function insertStudent($student_id, $forename, $surname, $dob, $gender, $nationality, $email)
    {
        $student_id = $this->db->clean($student_id);
        $forename = $this->db->clean($forename);
        $surname = $this->db->clean($surname);
        $dob = $this->db->clean($dob);
        $gender = $this->db->clean($gender);
        $nationality = $this->db->clean($nationality);
        $email = $this->db->clean($email);

        $query = "INSERT INTO students (student_id, forename, surname, dob, "
            . "gender, nationality, email) VALUES  ('" . $student_id . "', '"
            . $forename . "', '" . $surname . "', '" . $dob . "', '"
            . $gender . "', '" . $nationality . "', '" . $email . "')";

        if ($this->db->query($query)) {
            \Analog::info($_SESSION['user_identifier'] . " created student with identifier "
                . $student_id);
            return true;
        }
        return false;
    }

    /**
     * Update Student
     * 
     * @param array $studentData    student information
     * @return mysqli_result|false
     */
    public function updateStudent($studentData)
    {
       /* Double dollar sign creates a variable named after the variable supplied,
        * e.g. if $a = 'name' and $b = 'value', then $$a = $b is equivalent to:
        * $name = 'value'
        */
        foreach ($studentData as $key => $data) {
            $$key = $this->db->clean($data);
        }

        $sql = "UPDATE students SET student_id = '$id', forename = '$forenames', "
             . "surname = '$surname', dob = '$dob', gender = '$gender', "
             . "email = '$email', nationality = '$nationality' "
             . "WHERE student_id = '$originalID'";
             
        return $this->db->query($sql);
    }

    /**
     * Update students
     * @param array $toUpdate  list of students to update
     * @return boolean         success true|false
     */
    public function updateStudents($toUpdate)
    {
        foreach ($toUpdate as $studentKey => $fields) {
            $this->db->clean_array($fields);
            $sql = "UPDATE students SET";

            // Student Identifier
            if (strlen($fields[0]) > 0) {
                $sql .= " student_id = '" . $fields[0] . "',";
            }            
            
            // Student Forenames
            if (strlen($fields[1]) > 0) {
                $sql .= " forename = '" . $fields[1] . "',";
            }

            // Student surname
            if (strlen($fields[2]) > 0) {
                $sql .= " surname = '" . $fields[2] . "',";
            }

            // Date of birth
            if (strlen($fields[3]) > 0) {
                $sql .= " dob = '" . $fields[3] . "',";
            }

            // Gender
            if (strlen($fields[4]) > 0) {
                $sql .= " gender = '" . $fields[4] . "',";
            }

            // Nationality
            if (strlen($fields[5]) > 0) {
                $sql .= " nationality = '" . $fields[5] . "',";
            }

            // Email
            if (strlen($fields[6]) > 0) {
                $sql .= " email = '" . $fields[6] . "',";
            }

            $sql = substr_replace($sql, '', -1);
            $studentKeySan = $this->db->clean($studentKey);
            $sql .= " WHERE lower(student_id) = '$studentKeySan'";
            $result = $this->db->query($sql);

            if (!$result) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to delete students permanently from Omis
     * 
     * @param array $students     list of students
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function deleteStudents($students)
    {

        $groupsToOrder = [];
        $pass = true;

        // Loop through list of students and remove from system
        foreach ($students as $studentID) {
                      
         // Does student have results
         $studentHasResult = $this->db->results->doesStudentHaveResult(
             $studentID, null,
             null, null,
             null, true
         );
         
         // Student doesn't have result
         if(!$studentHasResult) {

              $groupsToOrder = array_merge(
                   $groupsToOrder,
                   $this->db->groups->getStudentGroups($studentID)
              );

              // Sanitize student ID
              $studentID = $this->db->clean($studentID);

              // # Delete results in cFROM student_resultsase of absent/partial results not caught in check prior
              $sql = "DELETE FROM student_results WHERE student_id = '$studentID';";

              // Remove from courses
              $sql .= "DELETE FROM student_courses WHERE student_id = '$studentID';";

              // Remove from exam groups
              $sql .= "DELETE FROM group_students WHERE student_id = '$studentID';";

              // Delete student
              $sql .= "DELETE FROM students WHERE student_id = '$studentID';";

              // Execute the mult-query
              $deleted = $this->db->multi_query($sql);

              // Student deleted ?
              if($deleted) {

                 \Analog::info($_SESSION['user_identifier'] .
                 ' deleted Student with Identifier ' . $studentID);

              } else {

                 $pass = false;

              }
          
          // Student cannot be deleted because he/she has results attached
          } else {

              setA($_SESSION, 'students_with_results_warning', []);
              $_SESSION['students_with_results_warning'][] = $studentID;

          }
          
        }

        $this->db->groups->sortGroupsOrder($groupsToOrder);
        return $pass;

    }

    /**
     * Function to check if a student with the specified user ID exists.
     * 
     * @param string $student_id Examinee (student) ID number
     * @return boolean TRUE if student exists, FALSE if not.
     */
    public function doesStudentExist($student_id) { return $this->existsById($student_id); }
    
    
     
    /**
     * Search/filter students and return
     * 
     * @param string $courseID - course ID
     * @param string $yearID - course year ID
     * @param string $moduleID - course module ID
     * @param string $searchTerm - search term (ID, Surname, Forenames) 
     * @param int $offset - offset for MySql limit
     * @param int $rowCount - row count for MySql limit
     * @param string $fieldToOrder - field to order by
     * @param string $orderType - order type [ASC|DESC]
     * @param string $termID - academic term ID
     * @return mysqli_result|false
     */
    public function searchStudents($courseID, $yearID, $moduleID, $searchTerm, $offset, 
                                   $rowCount, $fieldToOrder, $orderType, $termID = null)
    {

        $courseID = $this->db->clean($courseID);
        $yearID = $this->db->clean($yearID);
        $moduleID = $this->db->clean($moduleID);
        $searchTerm = $this->db->clean($searchTerm);
        $termID = $this->db->clean($termID);
        $offset = $this->db->clean($offset);
        $fieldToOrder = $this->db->clean($fieldToOrder);
        $orderType = $this->db->clean($orderType);
        $rowCount = $this->db->clean($rowCount);
       
        // Defaults
        $yearQuery = "";
        $moduleQuery = "";
        $termQuery = "";
        $searchQuery = "";
        $limitQuery = "";
        
        // Course year query
        if (strlen($yearID) > 0) {
          $yearQuery = " AND student_courses.year_id = '" . $yearID . "'";
        }
        
        // Module query
        if (strlen($moduleID) > 0) {
          $moduleQuery = " AND student_courses.module_id = '" . $moduleID . "'";
        }
       
        // Academic term query 
        if ($termID !== null) {
          $termQuery = " AND course_years.term_id = '" . $termID . "'";
        }

        // Search query
        if ($searchTerm !== '') {
          $searchTerms = explode(' ', $searchTerm);
          $searchTermCount = count($searchTerms);
            
          // Loop through search values
          foreach ($searchTerms as $term) {
            $term = trim($term);
            $searchQuery .= "students.student_id LIKE '" . $term . "%' OR " .
                            "students.forename LIKE '" . $term . "%' OR " .
                            "students.surname LIKE '" . $term . "%' OR ";
          }
            
          // Finalize search query
          if ($searchTermCount > 0) {
            $searchQuery = " AND (" . substr_replace($searchQuery, '', -3) . ")";
          }
        }
        
        // Ordering query
        if (strlen($fieldToOrder) > 0 && $fieldToOrder != 'UNORDERED') {
          $fieldToOrder = " ORDER BY " . $fieldToOrder . " ";
        } else {
          $fieldToOrder = "";
          $orderType = "";
        }

        // Limit records query        
        if ($offset !== 'NL' && $rowCount !== 'NL') {
          $limitQuery = " LIMIT " . $offset . ", " . $rowCount;
        }
      
        $sql = "SELECT students.student_id, candidate_numbers.candidate_number, students.forename, students.surname, students.dob, " .
               "students.gender, students.email, students.nationality, students.student_image, " .
               "course_years.year_name, course_years.year_id " .
               "FROM (students " .
               "INNER JOIN student_courses USING(student_id)) " .
               "INNER JOIN course_years USING(year_id) " .
               "LEFT JOIN candidate_numbers " .
               "ON (students.student_id = candidate_numbers.student_id " .
               "AND candidate_numbers.term_id = '$termID') " .            
               "WHERE course_years.course_id = '" . $courseID . "'" .
               $yearQuery . $moduleQuery . $termQuery . $searchQuery . " " .
               "GROUP BY students.student_id" .
               $fieldToOrder . $orderType . 
               $limitQuery;
        
        return $this->db->query($sql);
    }

    /**
     * Get total student for search
     * @param string $courseID - course ID
     * @param string $yearID - course year ID
     * @param string $moduleID - course module ID
     * @param string $searchTerm - search term (ID, Surname, Forenames)
     * @param string $termID - academic term ID
     * @return int|false - total student count
     */
    public function searchStudentsTotalCount($courseID, $yearID, $moduleID, $searchTerm, $termID = null)
    {

        $courseID = $this->db->clean($courseID);
        $yearID = $this->db->clean($yearID);
        $moduleID = $this->db->clean($moduleID);
        $searchTerm = $this->db->clean($searchTerm);
        $termID = $this->db->clean($termID);
       
        // Defaults
        $yearQuery = "";
        $moduleQuery = "";
        $termQuery = "";
        $searchQuery = "";
        
        // Course year query
        if (strlen($yearID) > 0) {
          $yearQuery = " AND student_courses.year_id = '" . $yearID . "'";
        }
        
        // Module query
        if (strlen($moduleID) > 0) {
          $moduleQuery = " AND student_courses.module_id = '" . $moduleID . "'";
        }
        
        // Academic term query 
        if ($termID !== null) {
          $termQuery = " AND course_years.term_id = '" . $termID . "'";
        }
       
        // Search query
        if ($searchTerm !== '') {
          $searchTerms = explode(' ', $searchTerm);
          $searchTermCount = count($searchTerms);
            
          // Loop through search values
          foreach ($searchTerms as $term) {
            $term = trim($term);
            $searchQuery .= "students.student_id LIKE '" . $term . "%' OR " .
                            "students.forename LIKE '" . $term . "%' OR " .
                            "students.surname LIKE '" . $term . "%' OR ";
          }
            
          // Finalize search query
          if ($searchTermCount > 0) {
            $searchQuery = " AND (" . substr_replace($searchQuery, '', -3) . ")";
          }
        }
              
        $query = "SELECT COUNT(DISTINCT students.student_id) " .
                 "FROM (students " .
                 "INNER JOIN student_courses USING(student_id)) " .
                 "INNER JOIN course_years USING(year_id) " .
                 "WHERE course_years.course_id = '" . $courseID . "'" .
                 $yearQuery . $moduleQuery . $termQuery . $searchQuery;
       
        return CoreDB::single_result($this->db->query($query));
    }

    /**
     * Import student data
     * 
     * @param array $toInsert           list of new students
     * @param array $toUpdate           list of students to update
     * @param array $courseData         course data (years and modules)
     * @param array $candidateNumbers   candidate numbers
     * @return boolean true|false       success       
     */
    public function importStudentData($toInsert, $toUpdate, $courseData, $candidateNumbers)
    {
        
        $courseError = "Could not insert student records, importing failed.<br/>" 
                     . "Please Check that the Course ID's &amp; Course Years in "
                     . "the file match the Course ID's &amp; Course Years in the system.<br/>" 
                     . "Please Check that the Module ID's in the file match the Module ID's in the system.";

        $report = [
            "complete" => false,
            "errorMsg" => "",
            "updatedStudents" => 0,
            "insertedStudents" => 0,
            "studentYearsInserted" => 0,
            "studentModulesInserted" => 0,
            "failedCandidateNumbers" => []
        ];

        // Create sql queries to insert new data in database
        $insertQuery = "INSERT INTO students (student_id, forename, surname,"
                     . "dob, gender, nationality, email) VALUES ";
        $row = 0;
        foreach ($toInsert as $fields) {
            $this->db->clean_array($fields);
            $insertQuery .= "('" . implode("', '", $fields) . "'),";
            $row++;
        }
        
        /*
         *  First start a mysql transaction so that we can perform a
         *  rollback in case there are errors during importing
         */
        $this->db->startTransaction();
        if (!$this->updateStudents($toUpdate)) {
            $this->db->rollback();
            $report["errorMsg"] = "Could not update students, importing failed";
            return $report;
        } else {
            $report["updatedStudents"] = count($toUpdate);
        }

        if ($row != 0 && !$this->db->query(rtrim($insertQuery, ","))) {
            $this->db->rollback();
            $report["errorMsg"] = "Could not insert new student records, importing failed";
            return $report;
        } elseif ($row > 0) {
            $report["insertedStudents"] = mysqli_affected_rows($this->db->getConnection());
        }

        /*
         * Course and Module data of the students is going to be inserted
         */
        list($insertSuccess, $nrYearsAdded, $nrModulesAdded) = $this->insertStudentsCourseData($courseData);
        
        if (!$insertSuccess) {
            $this->db->rollback();
            $report["errorMsg"] = $courseError;
            return $report;
        } else {
            $report["studentModulesInserted"] = $nrModulesAdded;
            $report["studentYearsInserted"] = $nrYearsAdded;
        }
        
        /*
         * Insert student candidates
         */
        if (count($candidateNumbers) > 0) {
           $report["failedCandidateNumbers"] = $this->insertCandidateNumbers(
                   $candidateNumbers
           );
        }
        
        /*
         * This releases the table and make the delete and insert complete.
         * No turning back after this point.
         */
        $this->db->commit();
        $report["complete"] = true;
        return $report;
    }
 
    /**
     * Insert candidate numbers
     * @param mixed[] $studentsNumbers    List of candidate numbers
     * @return boolean success true|false
     */    
    public function insertCandidateNumbers($studentsNumbers, $termID = null) 
    { 
        
        $skipped = [];
        $term = is_null($termID) ? $_SESSION["cterm"] : $this->db->clean($termID);
        
        foreach ($studentsNumbers as $studentID => $candidateNumber) {
            
            $student = $this->db->clean($studentID);
            $candidate = $this->db->clean($candidateNumber);
            $added = false;
            
            /**
             * Check to see if candidate number is taken for 
             * this student in this term, if so take note and skip
             */
             if (strlen($candidate) > 0) {
               $takenResult = $this->db->query("SELECT COUNT(*) FROM candidate_numbers "
                                          . "WHERE candidate_number = '$candidate' "
                                          . "AND term_id = '$term' AND student_id <> '$student'");
            
                if (CoreDB::single_result($takenResult) > 0) {
                   $skipped[$studentID] = $candidateNumber; 
                   continue;
                }
            }
            
            $deleted = $this->db->query("DELETE FROM candidate_numbers "
                                      . "WHERE student_id = '$student' "
                                      . "AND term_id = '$term'");
            if ($deleted) {
               $added = $this->db->query("INSERT INTO candidate_numbers (student_id, term_id, candidate_number) "
                                       . "VALUES ('$student', '$term', '$candidate')");
            }
            
            if (!$deleted || !$added) {
                $skipped[$studentID] = $candidateNumber;  
            }
            
        }
        
        return $skipped;
    }
    
    /**
     * Get candidate numbers
     * 
     * @param string $termID  identifier number for academic term
     * @return mixed[]        students identifiers with their candidate numbers
     */
    public function getCandidateNumbers($termID) {
        $termIDSan = $this->db->clean($termID);
        $resultset = $this->db->query("SELECT student_id, candidate_number "
                                    . "FROM candidate_numbers "
                                    . "WHERE term_id = '$termIDSan'");
        
        return $this->db->into_array($resultset);
    }
    
    /**
     * Swap students
     * 
     * @param string $swapOutStudent   swap out student ID
     * @param int $swapOutGroup        swap out group ID
     * @param string $swapInStudent    swap in student ID
     * @param int $swapInGroup         swap in group ID (optional)
     * @param int $swapStation         swap station ID (optional)
     * @return boolean swapped true|false
     */
    public function swapStudents($swapOutStudent, $swapOutGroup, $swapInStudent, $swapInGroup = null, $swapStation = null)
    {
        // Sanitize parameters        
        $swapOutStudentSan = $this->db->clean($swapOutStudent);
        $swapOutGroupSan = $this->db->clean($swapOutGroup);
        $swapInStudentSan = $this->db->clean($swapInStudent);
        $swapInGroupSan = $this->db->clean($swapInGroup);
        $swapStationSan = $this->db->clean($swapStation);
        
        // Return template
        $return = [
          'success' => false,
          'no_results_found' => false
        ];
        
        $this->db->startTransaction();
        
        /**
         * If the swap station option has been selected then this 
         * would mean that the user would like to swap the results between the students for the station
         * only. 
         */
        if (!empty($swapStationSan)) {
          
          // Does the station selected exist
          if ($this->db->exams->doesStationExist($swapStation)) {
              
            // Swap OUT result for student at station
            $outResultIDsSql = "SELECT result_id FROM student_results "
                             . "WHERE student_id = '$swapOutStudentSan' AND station_id = '$swapStation'";

            // Swap IN result for student at station
            $inResultIDsSql = "SELECT result_id FROM student_results "
                            . "WHERE student_id = '$swapInStudentSan' AND station_id = '$swapStation'";
          
            $outResultIDs = CoreDB::into_values($this->db->query($outResultIDsSql));
            $inResultIDs = CoreDB::into_values($this->db->query($inResultIDsSql));
            
            $numResultsOut = sizeof($outResultIDs);
            $numResultsIn = sizeof($inResultIDs);
            
            // No results found, then abort
            if ($numResultsOut === 0 && $numResultsIn === 0) {
               $return['no_results_found'] = true;
               $this->db->rollback();
               return $return;                
            }
           
            $outUpdate = "UPDATE student_results SET student_id = '$swapInStudentSan' "
                       . "WHERE result_id IN (" . implode(',', $outResultIDs). ")";
            
            $inUpdate = "UPDATE student_results SET student_id = '$swapOutStudentSan' "
                      . "WHERE result_id IN (" . implode(',', $inResultIDs). ")";
                  
            $outUpdated = ($numResultsOut == 0 || $this->db->query($outUpdate));
            $inUpdated = ($numResultsIn == 0 || $this->db->query($inUpdate));
            
            // Run the updates
            if ($outUpdated && $inUpdated) {
               $this->db->commit();
               
               // Get station records
               $stationRecord = $this->db->exams->getStation($swapStation, true);
               $formName = $stationRecord['form_name'];
               $stationNum = $stationRecord['station_number'];
               
               // Log message
               $logMessage = $_SESSION['user_identifier']  
                           . " swapped student $swapOutStudent result "
                           . " in station $stationNum - $formName (ID:$swapStation)"
                           . " with student $swapInStudent result in station $stationNum - $formName (ID:$swapStation)";
           
               \Analog::info($logMessage);
               
               $return['success'] = true;
               return $return;
            } else {
               $this->db->rollback();
               return $return; 
            }
          } else {
            $this->db->rollback();
            return $return;   
          }
          
        /**
         * If the user would like to swap the student between groups in the same exam or swap 
         * in a student from a course & module chosen
         */  
        } else {
       
          $swapInGroupSelected = ($swapInGroup != null);
        
          /**
           *  Both students in same group check
           */
          $swapWithinGroup = ($swapInGroupSelected && $swapOutGroup == $swapInGroup);
        
          $swapOutSessionID = $this->db->exams->getSessionIDByGroupID($swapOutGroup);
          $query1 = "UPDATE group_students SET student_id = '$swapInStudentSan' "
                  . "WHERE group_id = '$swapOutGroupSan' AND student_id = '$swapOutStudentSan'";
                      
          // Swap out result IDs
          $outResultIDsSql = "SELECT result_id FROM student_results "
                           . "WHERE student_id = '$swapOutStudentSan' AND station_id IN "
                           . "(SELECT station_id FROM session_stations "
                           . "WHERE session_id = '$swapOutSessionID')";
          $outResultIDs = CoreDB::into_values($this->db->query($outResultIDsSql));          

          // Capture all result records IDs before we do a swap
          if ($swapInGroupSelected) {
                    
             // Get swap in session ID
             $swapInSessionID = $this->db->exams->getSessionIDByGroupID($swapInGroup);
           
             // Swap in result IDs
             $inResultIDsSql = "SELECT result_id FROM student_results "
                             . "WHERE student_id = '$swapInStudentSan' AND station_id IN "
                             . "(SELECT station_id FROM session_stations "
                             . "WHERE session_id = '$swapInSessionID')";
             $inResultIDs = CoreDB::into_values($this->db->query($inResultIDsSql));
           
          }
                  
          // Swap within group
          $result1 = ($swapWithinGroup) ? true : $this->db->query($query1);
          
          // Swap results if any
          if (isset($outResultIDs) && sizeof($outResultIDs) > 0) {
              
            $query2 = "UPDATE student_results SET student_id = '$swapInStudentSan' "
                    . "WHERE student_id = '$swapOutStudentSan' "
                    . "AND result_id IN (" . implode(',', $outResultIDs). ")";                
              
            $result2 = $this->db->query($query2);
            
          } else {
            $result2 = true;
          }  
          
          // There is a swap in group selected 
          if ($swapInGroupSelected) {
              
           $query3 = "UPDATE group_students SET student_id = '$swapOutStudentSan' "
                   . "WHERE group_id = '$swapInGroupSan' AND student_id = '$swapInStudentSan'";
                       
           $query4 = "UPDATE student_results SET student_id = '$swapOutStudentSan' "
                   . "WHERE student_id = '$swapInStudentSan' "
                   . "AND result_id IN (" . implode(',', $inResultIDs). ")";
           
           $result4 = (sizeof($inResultIDs) > 0) ? $this->db->query($query4) : true;
   
           // Swap within same group then just simply flip the group orders
           if ($swapWithinGroup) {
               
             $outClause = " WHERE student_id='$swapOutStudentSan' AND group_id=$swapOutGroupSan";
             $inClause = " WHERE student_id='$swapInStudentSan' AND group_id=$swapInGroupSan";
             $outOrder = CoreDB::single_result(
                 $this->db->query(
                    "SELECT student_order FROM group_students" .
                    $outClause
                 )
             );
             
             $inOrder = CoreDB::single_result(
                 $this->db->query(
                    "SELECT student_order FROM group_students" .
                    $inClause
                 )
             );
             
             $query5 = "UPDATE group_students SET student_order='$inOrder'$outClause;"
                     . "UPDATE group_students SET student_order='$outOrder'$inClause;";
             $result5 = $this->db->multi_query($query5);
             $result3 = true;
           } else {
             $result3 = $this->db->query($query3);
             $result5 = true;  
           }
       
         }

         // Query failure, then bailout
         if (!$result1 || !$result2 || ($swapInGroupSelected && (!$result3 || !$result4 || !$result5))) {
            $this->db->rollback();
            return $return;
            
         // Query success, commit and log success
         } else {
            $this->db->commit();
            
            // Get swap out group record
            $swapOutGroupRecord = $this->db->exams->getGroup($swapOutGroup);           
            $swapOutGroupName = $swapOutGroupRecord['group_name'];
            
            if ($swapInGroupSelected) {
                $swapInGroupRecord = $this->db->exams->getGroup($swapInGroup);
                $swapInGroupName = $swapInGroupRecord['group_name'];
                $swapInGroupMessage = " in exam group $swapInGroupName (ID:$swapInGroup)";
            } else {
                $swapInGroupMessage = "";
            }
            
            $userRole = \OMIS\Auth\Role::loadID($_SESSION['user_role'])->name;
            
            $logMessage = $userRole . " " . $_SESSION['user_identifier']  
                        . " swapped student $swapOutStudent "
                        . " in exam group $swapOutGroupName (ID:$swapOutGroup)"
                        . " with student $swapInStudent$swapInGroupMessage";
           
            \Analog::info($logMessage);
            $return['success'] = true;
            return $return;
        }
        
       }
    }
   
    /**
     * Does exam contain student
     * @param type $exam_id
     * @return type
     */
    public function doesExamHaveResults($exam_id)
    {
        $exam_id = $this->db->clean($exam_id);
        $q = "SELECT COUNT(*) FROM student_results WHERE station_id"
            . " IN (SELECT station_id FROM session_stations"
            . " INNER JOIN exam_sessions USING(session_id) WHERE exam_id = '" . $exam_id . "')";
        $count = CoreDB::single_result($this->db->query($q));
        return (int) ($count > 0);
    }   
    
    /*
     * Remove old student records
     * @return bool true|false
     */
    public function purgeUnusedStudentRecords()
    {
        
        $sql = "SELECT student_id FROM students "
             . "WHERE student_id NOT IN (SELECT student_id FROM student_courses) "
             . "AND student_id NOT IN(SELECT student_id FROM student_results) "
             . "AND student_id NOT IN(SELECT student_id FROM group_students)";
        
        $students = CoreDB::into_values($this->db->query($sql));
       
        // No students found
        if (empty($students)) {
           return false;
        }

         /*
         *  Iterate through student records and remove them
         *  from the system
         */
        $deleteSql = "";
        foreach ($students as $studentID) {
            $deleteSql .= "DELETE FROM students "
                       .  "WHERE student_id = '$studentID';";
        }
        
        // Execute multi delete query
        return $this->db->multi_query($deleteSql);
    }

    /**
     * Get all student and courses for term
     * @param string $termID   academic term
     * @return mysqli_result   list of all students and their courses
     */
    public function getAllStudentCourseData($termID)
    {
        $termID = $this->db->clean($termID);
        $query = "SELECT * FROM student_courses "
               . "INNER JOIN course_years USING(year_id) "
               . "WHERE course_years.term_id = '$termID'"
               . "GROUP BY student_id, year_id, module_id";
        
        return $this->db->query($query);
        
    }

    /**
     * Get course information for one student in one term
     * 
     * @param string $studentID      ID number for a student
     * @param string $courseID       ID number for a course
     * @param string $termID         ID number for a term
     * @param string $groupByKey     group by key
     * @return array|false           returns array of course information 
     *                               or false if the query fails
     */
    public function getStudentCourseData($studentID, $courseID, $termID, $groupByKey = null)
    {
        // Sanatize parameters
        list($studentID, $courseID, $termID) = array_map("\OMIS\Database\CoreDB::clean", func_get_args());
        
        $sql = "SELECT course_data.course_id, course_data.year_id, course_data.year_name, student_data.module_id "
             . "FROM (SELECT * FROM course_years WHERE course_id = '"
             . $courseID . "' AND term_id = '"
             . $termID . "') AS course_data "
             . "INNER JOIN (SELECT * FROM student_courses WHERE student_id = '"
             . $studentID . "') AS student_data "
             . "USING(year_id) "
             . "ORDER BY year_name";

        $mysqliResult = $this->db->query($sql);
        
        // If the database query returns something, then convert it into an array.
        if ($mysqliResult !== false) {
           // Group by field
           if (!is_null($groupByKey)) {
              /* Just return the year and module relationship as an array 
               * minus everything else 
               */
              $resultArray = CoreDB::into_array($mysqliResult);
              return CoreDB::group($resultArray, [$groupByKey, 'module_id'], true);
              
           } else {
              return CoreDB::into_array($mysqliResult);
           }
        }
        
        // If we get to there, the query failed. Return false.
        return false;
    }

    /**
     * Insert students course data
     * 
     * @param string $studentID   identifier for student
     * @param array $courseData   array of course data (year and modules)    
     * @return bool true|false    success, true or false
     */
    public function insertStudentCourseData($studentID, $courseData)
    {
     // Check if we have course data
     if (is_array($courseData) && sizeof($courseData) > 0) {
        $studentID = $this->db->clean($studentID);
        $query = "INSERT INTO student_courses (student_id, year_id, module_id) VALUES ";
        
        // Build query from course data
        foreach ($courseData as $yearID => $modules) {
          
           // Skip if course does not exist
           if (!$this->db->courses->doesCourseYearExist($yearID)) {
             continue;  
           }
           
           $yearID = $this->db->clean($yearID);
           foreach ($modules as $moduleID) {
              
              // Skip if module does not exist
              if (!$this->db->courses->doesModuleExist($moduleID)) {
               continue;
              }
              
              $moduleID = $this->db->clean($moduleID); 
              $query .= "('$studentID', '$yearID', '$moduleID'),";  
           }
            
        }
        
        // Returns true or false depending on the query fared
        return $this->db->query(rtrim($query, ','));
        
      } else {
        return false;
      }
    }

    /**
     * Remove students from a course
     * 
     * @param array $students    list of students
     * @param string $courseID   identifier for the course
     * @param string $termID     identifier for the term
     * @return bool true|false
     */
    public function removeStudentsFromCourse($students, $courseID, $termID)
    {
        $courseID = $this->db->clean($courseID);
        $termID = $this->db->clean($termID);
        $query = "";

        // inner query
        $inner = "SELECT year_id "
               . "FROM course_years WHERE course_id = '$courseID' "
               . "AND term_id = '$termID'";
        
        /*
         *  Iterate through student records and remove them
         *  from the course
         */
        foreach ($students as $studentID) {
            $studentID = $this->db->clean($studentID);
            $query .= "DELETE FROM student_courses "
                    . "WHERE student_id = '$studentID' "
                    . "AND year_id IN ($inner);";
        }

        // Execute query
        $result = $this->db->multi_query($query);
        if ($result) {
            // Log changes
            foreach ($students as $studentID) {
              $logMessage = $_SESSION['user_identifier']." removed student: "
                          . $studentID." from course: ".$courseID." and term: "
                          . $termID;  
                
                \Analog::info($logMessage);
            }
        }

        return $result;
    }

    /**
     * Get student course years
     * @param string $studentID - student ID
     * @param string $courseID - course ID
     * @param string $termID - academic term ID
     * @return mysqli_result
     */
    public function getStudentCourseYears($studentID, $courseID, $termID)
    {

        $sql = "SELECT course_years.year_name " .
               "FROM course_years " .
               "INNER JOIN student_courses USING(year_id) " .
               "WHERE student_courses.student_id = '" . $this->db->clean($studentID) . "' " .
               "AND course_years.course_id = '" . $this->db->clean($courseID) . "' " .
               "AND course_years.term_id = '" . $this->db->clean($termID) . "' " .
               "GROUP BY course_years.year_id " .
               "ORDER BY course_years.year_name";

        return CoreDB::into_values($this->db->query($sql));
    }

    /**
     * Get students in course year
     * 
     * @param int $courseYearID   ID number for the course year
     * @param boolean array       return as array
     * @return mysqliresult|mixed[] 
     */
    public function getStudentsInYear($courseYearID, $asArray = false)
    {
        $courseYearID = $this->db->clean($courseYearID);
        $sql = "SELECT student_id, forename, surname, student_image "
             . "FROM students "
             . "INNER JOIN student_courses "
             . "USING(student_id) "
             . "WHERE year_id = '$courseYearID' "
             . "GROUP BY student_id "
             . "ORDER BY surname";
        $mysqliResult = $this->db->query($sql);
        if ($asArray) {
          return CoreDB::into_array($mysqliResult);            
        } else {
          return $mysqliResult;
        }
        
    }

    /**
     * Insert course info for students
     * @param array $courseData
     * @return array success status and report
     */
    public function insertStudentsCourseData($courseData)
    {
        $query = "INSERT INTO student_courses (student_id, year_id, module_id) VALUES ";
        $rowCount = count($courseData);
        $success = true;
        
        $yearsAdded = [];
        $modulesAdded = [];
         
        // We have records to insert
        if ($rowCount > 0) {
           
          // Clean data and form query insert values  
          foreach ($courseData as $course) {
  
            $studentID = $course[0];
            $yearID = $course[1];
            $moduleID = $course[2];
            
            $studentExists = $this->doesStudentExist($studentID);
            $yearExists = $this->db->courses->doesCourseYearExist($yearID);
            $moduleExists = $this->db->courses->doesModuleExist($moduleID);
            
            // Skip if a student, year or module do not exist in the database
            if (!$studentExists || !$yearExists || !$moduleExists) {
              continue;  
            } else {
              $yearsAdded[] = $studentID.$yearID;
              $modulesAdded[] = $studentID.$moduleID;
            }
            
            // Sanitize and add values to query
            $this->db->clean_array($course);
            $query .= "('" . implode("', '", $course) . "'),";
            
          }
          
          // Strip trailing comma and execute query
          $query = rtrim($query, ',');
          $success = $this->db->query($query);
                   
        }
        
       $nrOfYears = count(array_unique($yearsAdded));
       $nrOfModules = count(array_unique($modulesAdded));
        
       $report = [$success,       // Succes
                  $nrOfYears,     // Number of years added
                  $nrOfModules];  // Number of modules added
        
        return $report;
    }

    /**
     * Get student modules by course
     * @param string $studentID
     * @param string $courseID
     * @param string $termID
     * @return mysqli_result|false
     */
    public function getStudentModules($studentID, $courseID, $termID)
    {

        $query  = "SELECT modules.module_id, modules.module_name " .
                "FROM (course_years " .
                "INNER JOIN student_courses USING(year_id)) " .
                "INNER JOIN modules USING(module_id) " .
                "WHERE student_courses.student_id = '" . $this->db->clean($studentID) . "' " .
                "AND course_years.course_id = '" . $this->db->clean($courseID) . "' " .
                "AND course_years.term_id = '" . $this->db->clean($termID) . "' " .
                "GROUP BY modules.module_id " .
                "ORDER BY course_years.year_name, module_name";

        return $this->db->query($query);
    }

    /**
     * Remove student course data (years and modules)
     * @param string $studentID   identifier for student
     * @param string $courseID    identifier for course
     * @param string $termID      identifier for term
     * @return bool true|false
     */
    public function removeStudentCourseData($studentID, $courseID, $termID)
    {
        // Sanatize parameters
        list($studentID, $courseID, $termID) = array_map("\OMIS\Database\CoreDB::clean", func_get_args());  
       
        $query = "DELETE FROM student_courses "
               . "WHERE student_id = '$studentID' "
               . "AND year_id IN "
               . "(SELECT year_id"
               . " FROM course_years"
               . " WHERE term_id = '$termID' AND course_id = '$courseID')";
        
        return $this->db->query($query);
    }

    /**
     * Retrieves the name of the image file stored for a specific 
     * student/examinee. This image should be located in the originals/ folder
     * of the student image cache.
     * 
     * @param string $student_id ID number of student
     * @return mixed Image file name on success, FALSE if the student doesn't exist.
     */
    public function getStudentImage($student_id)
    {
        $student_id = $this->db->clean($student_id);
        $query = "SELECT student_image FROM students WHERE student_id = '" . $student_id . "'";

        $result = CoreDB::single_result($this->db->query($query));

        if (($result !== false) && is_string($result)) {
            /* If the result is a string, then it's a valid result. Strip off
             * any superfluous leading or trailing whitespace.
             */
            $result = trim($result);
        }
        return $result;
    }

    /**
     * Function to set the student_image field for a student.
     * 
     * Note: It's assumed here that the image is stored in the student 
     * ImageCache and that as a result we don't need to store the path, just the 
     * filename.
     * 
     * @param string $student_id     ID number of student
     * @param string $image_path Name or path of image.
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function updateStudentImage($student_id, $image_path)
    {
        $student_id = $this->db->clean($student_id);
        $image_name = $this->db->clean(basename($image_path));

        /* If the value we're trying to set is already set, MySQLi will report
         * that zero rows were changed by the update statement, even though the
         * end result is that the desired value is set (even if that's only
         * because it had been done so before). So, we need to check if that's
         * the case here so that we can report to the user that the value is set
         * to what they want it to be.
         */
        $existing_image = $this->getStudentImage($student_id);

        if ($existing_image == $image_name) {
            return true;
        }

        /* If we get to here, then the student image is not the same as before,
         * so it's in our interest to update the database as required..
         */
        $query = "UPDATE students SET student_image=? WHERE student_id=?";
        $statement = mysqli_prepare($this->db->getConnection(), $query);
        mysqli_stmt_bind_param($statement, 'ss', $image_name, $student_id);

        if (mysqli_stmt_execute($statement)) {
            $affected_rows = mysqli_stmt_affected_rows($statement);
        } else {
            $affected_rows = 0;
        }

        return ($affected_rows > 0);
    }

    protected function getTableName() { return 'students'; }

    protected function getIds() { return ['student_id']; }

}
