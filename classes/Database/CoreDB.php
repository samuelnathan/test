<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to exam settings
 */

/**
 * DW: This is to ensure that this script knows what the database constants are.
 * Otherwise, many of the queries will fail...
 */
namespace OMIS\Database;

use Carbon\Carbon;

class CoreDB
{
    private static $_t_inst = null;
    private static $_t_con = null;

    // Convert html entities
    private static $_convert_html_entities = true;

    // From Charset - If Importing from Excel for example
    private static $_charset_from = null;
    public $users;
    public $userPresets;
    public $exams;
    public $examRules;
    public $examNotifications;
    public $examSettings;
    public $gradeRules;
    public $gradeValues;
    public $courses;
    public $academicterms;
    public $competencies;
    public $features;
    public $forms;
    public $results;
    public $feedback;
    public $schools;
    public $departments;
    public $sessions;
    public $sessionAssistants;
    public $stations;
    public $stationExaminers;
    public $groups;
    public $students;
    public $blankStudents;
    public $examdata;
    public $sms;
    public $iso;
    public $weighting;
    public $colourMatching;
    public $selfAssessments;
    public $studentStationOutcomes;
    public $studentItemOutcomes;
    public $multimedia;

    //Constructor
    public function __construct()
    {
        //logfile setup
        global $config;
        if (!isset($config)) {
            $config = new \OMIS\Config;
        }

        // Initiate database objects
        $this->academicterms = new AcademicTerms($this);
        $this->competencies = new Competencies($this);
        $this->courses = new Courses($this);
        $this->exams = new Exams($this);
        $this->examRules = new ExamRules($this);
        $this->examSettings = new ExamSettings($this);
        $this->examNotifications = new ExamNotifications($this);
        $this->gradeRules = new GradeRules($this);
        $this->gradeValues = new GradeValues($this);
        $this->features = new Features($this);
        $this->forms = new Forms($this);
        $this->iso = new ISO($this);
        $this->results = new Results($this);
        $this->feedback = new Feedback($this);
        $this->sms = new SMS($this);
        $this->schools = new Schools($this);
        $this->departments = new Departments($this);
        $this->students = new Students($this);
        $this->examdata = new StudentExamData($this);
        $this->users = new Users($this);
        $this->userPresets = new UserPresets($this);
        $this->selfAssessments = new SelfAssessments($this);
        $this->sessions = new Sessions($this);
        $this->stations = new Stations($this);
        $this->stationExaminers = new StationExaminers($this);
        $this->groups = new Groups($this);
        $this->blankStudents = new BlankStudents($this);
        $this->sessionAssistants = new SessionAssistants($this);
        $this->weighting = new Weighting($this);
        $this->colourMatching = new ColourMatching($this);
        $this->studentStationOutcomes = new StudentStationOutcomes($this);
        $this->studentItemOutcomes = new StudentItemOutcomes($this);
        // $this->multimedia = new Multimedia($this);

    }

    //clone
    public function __clone()
    {
        trigger_error('Cannot duplicate a singleton', E_USER_ERROR);
    }

    /**
     * Create an instance of the database connection.
     *
     * @global \OMIS\Config $config
     * @param null|\OMIS\Database\CoreDB $db        (Optional) Existing database object.
     * @param mixed                      $prod_mode (Optional) Production mode
     * @return \OMIS\Database\CoreDB
     */
    public static function getInstance($db = null, $prod_mode = null)
    {

        // New config object if not created
        global $config;

        if (!isset($config)) {
           $config = new \OMIS\Config;
        }

        // If the database to connect to isn't specified, assume it's the default.
        if (is_null($db)) {
            $db = $config->db;
        }

        /* If we're not sure what the production mode is supposed to be, or if
         * it's not specified, then ask the configuration settings what it's
         * set to... assuming that setting is specified.
         */
        if (is_null($prod_mode) && isset($config->localization['modes']['production'])) {
            $prod_mode = $config->localization['modes']['production'];
        }

        if (self::$_t_inst === null) {
            self::$_t_inst = new CoreDB();
            self::$_t_con = @mysqli_connect($db['host'], $db['username'], $db['password'], $db['schema']);
            if (!self::$_t_con) {
                if (isset($prod_mode) && $prod_mode) {
                    $connect_error = '';
                } else {
                    $connect_error = ": " . mysqli_connect_error();
                }

                die("Could Not Connect$connect_error<br/>[Please Inform Administrator]");
            }

            // Character set = utf8
            mysqli_set_charset(self::$_t_con, 'utf8');

            // Set timezone of connection taken from config file
            $timezoneConfig = $config->localization['timezone'];
            mysqli_query(self::$_t_con, "SET time_zone = '"  . $timezoneConfig . "'");

        }
        return self::$_t_inst;
    }

    //Get Database Connection and Return [18/03/2014]
    public function getConnection()
    {
        return self::$_t_con;
    }

    //Renew Connection
    public function renewConnection($db) {

        self::$_t_con = @mysqli_connect($db['host'], $db['username'], $db['password'], $db['schema']);
        if (!self::$_t_con) {
            die("Could not connect<br/>[Please Inform Administrator]");
          return false;
        }
        mysqli_set_charset(self::$_t_con, 'utf8');
        return true;

    }

   /**
     * Query database
     * @param string $query value containing the query string
     * @return boolean|mysqli_result FALSE if query fails; mysqli_result object otherwise.
     */
    public function query($query)
    {
        $resultset = mysqli_query(self::$_t_con, $query);
        if (!$resultset) {
            $callers = debug_backtrace();
            $calling_function = $callers[1]['function'];
            error_log(
                __METHOD__ . ": Database query '$query' failed - error was "
                . mysqli_error(self::$_t_con) . ". Calling function was $calling_function"
            );
            return false;
        } else {
            return $resultset;
        }
    }

   /**
     * Run multiple queries in one call [16/12/2014]
     * @param string $queries string with multiple queries
     * @return boolean|mysqli_result FALSE if query fails; mysqli_result object otherwise.
     */
    public function multi_query($queries)
    {
        $result = mysqli_multi_query(self::$_t_con, $queries);

        if ($result) {
            //Run through results to clear from server
            while (mysqli_more_results(self::$_t_con)) {
                mysqli_next_result(self::$_t_con);
            }
        }
        return $result;
    }

    /**
     * Get the version number of the database from the database itself - e.g.
     * find out which version of the database the current installation's
     * been updated to.
     *
     * @return int[] Database version script(s) installed.
     */
    public function getDatabaseVersions()
    {
        // Ask for the most recent version number
        $query = "SELECT `version` FROM `database_versions` ORDER BY `applied`";
        $db_result = $this->query($query);

        if (!empty($db_result)) {
            return array_column($this->into_array($db_result), "version");
        }

        // If there's no version available, return a zero.
        return [];
    }

    //Get last Inserted ID
    public function inserted_id()
    {
        return mysqli_insert_id(self::$_t_con);
    }

   /**
     * Sanitizes value before insertion into database
     * @param $value value to sanitize
     * @return string|int|bool sanitized value.
     */
    public static function clean($value)
    {
        // If array then return empty string
        if (is_array($value)) {
            return '';
        }

        // Allow numbers, booleans and nulls to pass through unharmed
        if (in_array($value, [NULL, TRUE, FALSE], TRUE) || is_numeric($value)) {
            return $value;
        }

        // If From Charset
        if (self::$_charset_from !== null) {
            $value = iconv(self::$_charset_from, 'UTF-8', $value);
        }

        // Escape value
        $value = mysqli_real_escape_string(self::$_t_con, trim($value));

        return $value;
    }

   /**
     * Sanitizes array of values before insertion into database Updated [16/12/2014]
     * @param $array array to sanitize
     * @return array sanitized array of values.
     */
    public static function clean_array(&$array)
    {
        // If From Charset
        if (self::$_charset_from !== null) {
            // Escape values
            foreach ($array as &$value) {
                $value = mysqli_real_escape_string(
                    self::$_t_con,
                    trim(iconv(self::$_charset_from, 'UTF-8', $value))
                );
            }
        } else {
            // Escape values
            foreach ($array as &$value) {
                $value = mysqli_real_escape_string(self::$_t_con, trim($value));
            }
        }
    }

    //Set the character set if the data is coming from other sources like excel
    public function set_Charset_From($new_charset)
    {
        self::$_charset_from = $new_charset;
    }

    /**
     * Transform mysqli result into an array
     *
     * @param mysqli_result $mysqliResult
     * @return array of data
     */
    public static function into_array($mysqliResult)
    {

        /* Make sure that what we're going to try to iterate over is actually a
         * mysqli_result object before we throw errors unnecessarily.
         */
        if (!($mysqliResult instanceof \mysqli_result)) {
            /* Log the error and return an empty array. This behaviour should
             * better handle instances where
             */
            $callers = debug_backtrace();
            $calling_function = $callers[1]['function'];
            error_log(
                __METHOD__ . ": passed something other than an mysqli_result from "
                . $calling_function
            );
            return [];
        }

        $returnArray = [];

        // Loop through results and sanitize the data
        while ($result = mysqli_fetch_assoc($mysqliResult)) {

            // Sanitize the result
            if (self::$_convert_html_entities) {
                foreach ($result as &$value) {
                    $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
                }
            }
            $returnArray[] = $result;
        }

        return $returnArray;

    }

    /**
     * Recursively group a list of associative arrays by one or more fields/keys
     * they contain.
     *
     * @param mixed[] $data     Array of data to sort.
     * @param array   $keys     List of keys to sort by, in order of desired grouping
     * @param bool    $fieldsAsIndices Use the value of the grouping key as the index of each matching item
     * @param bool    $collapse If TRUE, then use key value as the index of the group and remove it from each item. If false, order numerically instead and preserve the key value in the outputted data.
     *
     * @return mixed[] Nested, grouped array where the contents of $data have been grouped according to $keys.
     * @throws \InvalidArgumentException If the parameters are bad or incompatible with each other (e.g. not enough data to sort by the required number of keys)
     */
    public static function group($data, array $keys, $fieldsAsIndices = false, $collapse = false) {

        // Make sure there are keys supplied
        if (empty($keys)) {
            // throw new \InvalidArgumentException("No grouping keys supplied");
            //error_log(__method__ . " No grouping keys supplied CoreDB::group(), returning empty array");
            return $data;
        }

        // Make sure that there's some data we can group...
        if (empty($data)) {
            // throw new \InvalidArgumentException("No data array found, parameter 1");
            //error_log(__method__ . " Data array (parameter 1) empty, returning empty array");
            return [];
        }

        // Get the first key off the front of the array.
        $key = array_shift($keys);

        // Get the list of unique values in the supplied data for that key.
        $keyValues = array_unique(array_column($data, $key));

        // Empty array for the results to go into.
        $results = [];

        // Iterate through all the possible values of array item $key.
        foreach ($keyValues as $groupKeyValue) {
            $group = [];
            foreach (array_keys($data) as $index) {
                /* If we find an element that should be in the group, add it to that
                 * group and remove it from the main array to conserve memory.
                 */
                if ($data[$index][$key] === $groupKeyValue) {
                    $group[] = $data[$index];
                    unset($data[$index]);
                }
            }

            /* If there are more keys to group by, then recursively call this
             * function to sub-sort this group by second and subsequent keys.
             */
            if (!$fieldsAsIndices) {
                $results[] = (empty($keys)) ? $group : self::group($group, $keys, $fieldsAsIndices, $collapse);
                continue;
            }


            /**
             *  Remove the key we're grouping by from each element in the group
             */
            array_walk($group, function (&$item) use ($key) {
                unset($item[$key]);
            });

            /**
             * The keys are empty, meaning we can attach the group of
             * data to the return results.
             */
            if (empty($keys)) {
                /**
                 *  If there is one record per group key and the collapse records parameter
                 *  is set then assign the record directly to the key, if that makes sense.
                 */
                $results[$groupKeyValue] = ($collapse) ? array_shift($group) : $group;
            } else {
                /**
                 *  If there are sub grouping keys then continue recursively
                 *  through the array
                 */
                $results[$groupKeyValue] = self::group($group, $keys, $fieldsAsIndices, $collapse);
            }

        }
        return $results;
    }

    /**
     * @param \mysqli_result $result
     * @param string[] $groupKeys Keys to group the results into following the given order.
     * @param string[] $columnsToSelect Columns to select into the resulting groupings
     * @param callable[] $select Functions to map the columns. The key in the given array is the name of the column,
     *                           and the value is the function that receives the column value and returns the resulting
     *                           value.
     *
     * @return array
     */
    public static function groupBySortedColumns(\mysqli_result $result, array $groupKeys, array $columnsToSelect = [], array $select = [])
    {
        if (count($groupKeys) === 0)
            return [];

        $groupedResult = [];

        // Iterate through the result set
        while ($row = $result->fetch_assoc()) {
            // Point back to the first grouping level
            $groups = &$groupedResult;

            // Iterate through the grouping keys
            foreach ($groupKeys as $key) {
                // If it's the first time this key is found, add it to the grouping
                if (!array_key_exists($key, $groups))
                    $groups[$key] = [];

                // Reference the group values
                $group = &$groups[$key];
                $groupKey = (string)$row[$key];

                // If it's the first time the value is found, add it
                if (!array_key_exists($groupKey, $group))
                    $group[$groupKey] = [];

                // Point to the grouping in order to advance to the next level in
                // the next iteration
                $groups = &$group[$groupKey];

                // Add to the result the values of the given columns to select
                foreach (array_key_exists($key, $columnsToSelect) ? $columnsToSelect[$key] : [] as $columnToSelect) {
                    $selectColumn = array_key_exists($columnToSelect, $select) ? $select[$columnToSelect] : function ($v) { return $v; };
                    $groups[$columnToSelect] = $selectColumn($row[$columnToSelect]);
                }
            }
        }

        return $groupedResult;
    }

    /**
     * fetch database row from result set
     * Takes a mysqli_result object and returns the first row as an associative
     * array. To do the same for multiple rows, use into_array();
     *
     * @param mysqli_result $resultset Result set
     * @return mixed[]
     */
    public function fetch_row($resultset)
    {
        /* (DW) mysqli_fetch_assoc() could get thrown a boolean FALSE here, so
         * we need to cater for that.
         */
        $returnArray = ($resultset === false) ? null : mysqli_fetch_assoc($resultset);

        if (!is_null($returnArray) && self::$_convert_html_entities) {
            foreach ($returnArray as &$value) {
                $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
            }
        }
        return $returnArray;
    }

    /**
     * Take a mysqli_result object (which we're assuming to be a single record
     * containing a single field) and return just that first field of that first
     * record. If the $_convert_html_entities setting is true, this value is
     * also has it's text encoded with HTML entities so that it can be rendered
     * directly.
     *
     * @param mysqli_result|false $result mysqli_result object (or false if the
     *                            query generating the result failed)
     * @return mixed Either null (if the query failed) or whatever data type
     *               best matches the record as far as PHP is concerned.
     */
    public static function single_result($result)
    {
        /* If the query that produced $result as an output failed, then $result
         * will be FALSE. If we throw a FALSE at mysql_fetch_row(), it will
         * (perhaps confusingly) return null...
         */
        if (empty($result)) {
            return null;
        }

        list($value) = mysqli_fetch_row($result);

        if (self::$_convert_html_entities && is_string($value)) {
            $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        }

        return $value;
    }

    //Turn off convert html entities
    public function set_Convert_Html_Entities($on)
    {
        self::$_convert_html_entities = $on;
    }

    /**
     * Put Result Rows Into Single Values
     */
    public static function into_values($mysqliResult)
    {
        $return = [];

        if ($mysqliResult) {
            while ($r = mysqli_fetch_array($mysqliResult)) {
                $return[] = cleanout($r[0]);
            }
        }
        return $return;
    }

    public static function toSqlList($array, $skipFalsy = true)
    {
        if ($skipFalsy)
            $array = array_filter($array, function ($value) {
                return $value !== null;
            });

        $commas = implode(', ', $array);
        return "($commas)";
    }

    /**
     * Function to get a list of tables from MySQL.
     *
     * @param string $regex (Optional) Regular expression to filter the returned table list.
     *
     * @return string[] Array containing all table names.
     */
    private function getTableList($regex = null)
    {
        // Get a list of tables.
        $q = "SHOW TABLES;";
        $results = CoreDB::into_array($this->query($q));

        $table_names = array();
        /* If there's stuff to extract, get the name of the pertinent field (it'll
         * be "Tables_in_<x>" where <x> is the database name) and then grab the
         * field's value in each case.
         */

        if (!empty($results)) {
            // Get the name of the first field in the first result record.
            $field_name = array_keys($results[0])[0];

            foreach ($results as $result) {
                // If there's a regex defined, test the table name with it.
                if (($regex != null) && (!preg_match($regex, $result[$field_name]))) {
                    continue;
                }
                $table_names[] = $result[$field_name];
            }
        }

        return $table_names;
    }

    /**
     * Function to get a list of columns for a given table
     *
     * @param type $table_name Table to get list of columns/field names for.
     *
     * @return string[] List of field names.
     */
    public function getTableColumns($table_name, $regex = null)
    {
        $columns = [];

        // Check if the table exists or not first.
        $match_regex = '/^' . $table_name . '$/i';
        $matching_tables = $this->getTableList($match_regex);
        if (empty($matching_tables)) {
            return $columns;
        }

        $sql = "SHOW COLUMNS FROM $table_name";

        $resultArray = CoreDB::into_array($this->query($sql));
        $results = array_keys(
             $this->group($resultArray, ['Field'], true)
        );

        if ($regex === null) {
            return $results;
        } else {
            foreach ($results as $column) {
                if (preg_match($regex, $column)) {
                    $columns[] = $column;
                }
            }

            return $columns;
        }
    }

    /**
     * Get the number of rows affected by the last query to be executed.
     *
     * @return int Number of rows affected, or -1 if the query failed.
     */
    public function getAffectedRowCount()
    {
        return mysqli_affected_rows(self::$_t_con);
    }

    /**
     * Start transaction
     * return bool true|false
     */
    public function startTransaction() {
        return $this->query('START TRANSACTION');
    }

    /**
     * Commit transaction
     * return bool true|false
     */
    public function commit() {
        return $this->query('COMMIT');
    }

    /**
     * Rollback transaction
     * return bool true|false
     */
    public function rollback() {
        return $this->query('ROLLBACK');
    }

    public function toSqlDate(Carbon $date) {
        return $date->toIso8601String();
    }

    /**
     * @param $sql string
     * @return \mysqli_stmt
     */
    public function prepare($sql) {
        return self::$_t_con->prepare($sql);
    }

}
