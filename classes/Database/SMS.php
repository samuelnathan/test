<?php

/* Original Author: David Cunningham
  For Qpercom Ltd
  Date: 03/03/2014
  © 2014 Qpercom Limited.  All rights reserved.
  Database function calls relating to SMS
 */

namespace OMIS\Database;

class SMS
{
//Core Database 'coreDB' reference
    private $db;

    //Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    //Record SMS Message in Database [Updated 11/02/2014]
    public function recordSMSMessage($sent_from, $sms_message, $recipient_count)
    {

        list($sent_from, $sms_message, $recipient_count) = array_map("\OMIS\Database\CoreDB::clean", func_get_args()); //Clean Parameters

        $q = "INSERT INTO sms_messages (sent_from, sms_message, recipient_count) " .
                "VALUES ('" . $sent_from . "', '" . $sms_message . "', '" . $recipient_count . "')";

        return array($this->db->query($q), $this->db->inserted_id());
    }

    //Get SMS Message [Updated 11/02/2014]
    public function getSMSMessage($sms_id)
    {
        $q = "SELECT sent_from, sms_message, recipient_count, created_at FROM sms_messages WHERE sms_id = '" . $this->db->clean($sms_id) . "'";
        return $this->db->fetch_row($this->db->query($q));
    }

    //Record SMS Message Recipients in Database [Updated 11/02/2014]
    public function recordSMSMessageRecipients($recipients_records)
    {

        //Beginning of Query
        $q = "INSERT INTO sms_message_recipients (sms_id, recipient_id, mobile_num, sent) VALUES ";

        //Loop through recipients records
        foreach ($recipients_records as $rec_record) {
            $q = $q . "('" . implode("', '", $rec_record) . "'),";
        }

        return $this->db->query(substr_replace($q, '', -1));
    }

    //Get SMS Message Recipients [Updated 11/02/2014]
    public function getSMSMessageRecipients($sms_id)
    {
        $q = "SELECT recipient_id, mobile_num, sent FROM sms_message_recipients WHERE sms_id = '" . $this->db->clean($sms_id) . "'";
        return CoreDB::into_array($this->db->query($q));
    }
}

?>
