<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * Database function calls relating to exam rules
 */

namespace OMIS\Database;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class ExamRules
{
    //Core Database 'coreDB' reference
    private $db;

    //Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Update exam rules
     *
     * @param int  $examID    ID number of exam
     * @param array $rules    Array of rules, field names and values
     * @param bool  $logging  logging switch
     * @return true|false If query fails then bool false is returned.
     */
    public function updateRules($examID, $rules, $logging = true) 
    {
        
        // If non-exist
        $this->newRules($examID);
        
        $rulesString = "";
        // Loop through rules and build query (Could use implode() or something similar here)
        foreach ($rules as $rule) {
            $field = $this->db->clean($rule['field']);
            $valueSan = $this->db->clean($rule['value']);
            
            $value = (strlen($valueSan) == 0) ? "NULL" : $valueSan;
            
            $rulesString .= "$field = $value, ";
        }

        $rulesSql = substr($rulesString, 0, -2);
        $examIDSan = $this->db->clean($examID);
        $sql = "UPDATE exam_rules SET $rulesSql " .
               "WHERE exam_id = '$examIDSan'";
        
        $status = $this->db->query($sql);
        
        if ($status && $logging) {
            $exam = $this->db->exams->getExam($examIDSan)['exam_name'];
            $log = $_SESSION['user_identifier'] 
                   . " updated individual exam rules"
                   . " for exam: $exam (ID:$examIDSan) | $rulesSql";
            \Analog::info($log);
        }
        return $status;

    }
    
    /**
     * Get exam rules
     * @param type $examID  Identifier number for exam
     * @return rules[]
     */
    public function getRules($examID) 
    {
    
       // If non-exist
       $this->newRules($examID);
     
       $sql = "SELECT exam_rules.*, exams.grade_rule FROM exam_rules "
            . "INNER JOIN exams USING(exam_id) "
            . "WHERE exam_id = " . $this->db->clean($examID);
     
       return $this->db->fetch_row(
          $this->db->query($sql)  
       );
       
    }
    
    /**
     * New rules record if non-exist
     * @param type $examID  Identifier number for exam
     * @return bool true|false status
     */
    public function newRules($examID) 
    {
 
       $examIDSan = $this->db->clean($examID);  
       $sql = "SELECT * FROM exam_rules "
            . "WHERE exam_id = " . $examIDSan;

       $resultset = $this->db->query($sql);
      
       // Create row if non-exist
       if (mysqli_num_rows($resultset) == 0) {
           $this->db->query("INSERT INTO exam_rules (exam_id) "
                               . "VALUES ($examIDSan)");
       }
      
       return true;
       
    }
   
    /**
     * Update stations values (pass and weight values)
     * 
     * @param array $data       station and their pass and weight values
     */
    public function updateStationValues($data) 
    {
        $sql = $logStringPass = $logStringWeight = "";
        
        foreach ($data as $stationID => $values) {

            $passValue = $values['pass'];
            $weightValue = $values['weight'];
            $passCheck = (is_numeric($passValue) && ($passValue >= 0 && $passValue <= 100));
            $weightCheck = (is_numeric($weightValue) && ($weightValue >= 0 && $weightValue <= 100));

            if ($passCheck && $weightCheck) {
                $passSan = $this->db->clean($passValue);
                $weightSan = $this->db->clean($weightValue);
                $stationIDSan = $this->db->clean($stationID);
                $sql .= "UPDATE session_stations "
                      . "SET pass_value = '$passSan', weighting = '$weightSan' "
                      . "WHERE station_id = '$stationIDSan'; ";
                $logStringPass .= "(ID:$stationIDSan) = $passSan, ";
                $logStringWeight .= "(ID:$stationIDSan) = $weightSan, ";
            }

        }
               
        $status = $this->db->multi_query($sql);
        
        if ($status) {
            $log = $_SESSION['user_identifier'] 
            . " updated station pass (%) exam rules " 
            . substr($logStringPass, 0, -2);
            \Analog::info($log);

            $log = $_SESSION['user_identifier'] 
            . " updated station weighting (%) exam rules " 
            . substr($logStringWeight, 0, -2);
            \Analog::info($log);      
        }
  
        return $status;       
        
        
    }
    
    /**
     * Get competency rules
     * 
     * @param int $examID      examination identifier
     * @return mixed[]         competency rules array
     */
    public function getCompetencyRules($examID) 
    {
            
       $sql = "SELECT exam_competency_rules.id, exam_competency_rules.competency_id, exam_competency_rules.pass, "
            . "competency_framework_levels.name, competency_framework_levels.level_colour_code "   
            . "FROM exam_competency_rules "
            . "INNER JOIN competency_framework_levels "
            . "ON (exam_competency_rules.competency_id = competency_framework_levels.id) "
            . "WHERE exam_id = " . $this->db->clean($examID) . " "
            . "GROUP BY exam_competency_rules.id";
       
       return CoreDB::into_array($this->db->query($sql));
    }
    
    
    /**
     * Check competency rules. Create rules if they don't exist, 
     * remove old ones no longer in use
     * 
     * @param int $examID      examination identifier
     */
    public function checkCompetencyRules($examID) 
    {
        
       // Current competency rules 
       $currentRules = $this->getCompetencyRules($examID);
       
       // Rules competency IDs
       $rulesCompetencyIDs = array_column($currentRules, 'competency_id');       
       
       // Current exam competencies
       $formIDs = $this->db->exams->getUniqueFormIdentifiers($examID);
       $currentCompetencies = $this->db->competencies->getExamCompetencies($formIDs); 
           
       foreach ($currentCompetencies as $competency) {
           $currentID = $competency['id'];
           
           // Create missing rule
           if (!in_array($currentID, $rulesCompetencyIDs)) {
               $this->createCompetencyRule($examID, $currentID);
              
           // Mark rule as checked and included
           } else if (($index = array_search($currentID, $rulesCompetencyIDs)) !== false) {
                unset($rulesCompetencyIDs[$index]);
           }
       }
       
       // Remove unused rules
       foreach ($rulesCompetencyIDs as $toDelete) {
         $this->removeCompetencyRule($examID, $toDelete);  
       }
      
    }
    
    /**
     * Update competency rules
     * 
     * @param array $data       competency data
     */
    public function updateCompetencyRules($data) 
    {
        $sql = $logString = "";
        foreach ($data as $ruleID => $value) {
            if (is_numeric($value) && ($value >= 0 && $value <= 100)) {
                $ruleIDSan = $this->db->clean($ruleID);
                $valueSan = $this->db->clean($value);
                $sql .= "UPDATE exam_competency_rules SET pass = '$valueSan' "
                      . "WHERE id = '$ruleIDSan'; ";
                $logString .= "(ID:$ruleIDSan) = $valueSan, ";
            }
        }
       
        $status = $this->db->multi_query($sql);
        
        if ($status) {
            $log = $_SESSION['user_identifier'] 
            . " updated competency exam rules " 
            . substr($logString, 0, -2);
            \Analog::info($log);
        }
  
        return $status;        
        
    }
    
    /**
     * Create competency rule
     * 
     * @param int $examID          examination identifier
     * @param int $competencyID    competency identifier
     */   
    public function createCompetencyRule($examID, $competencyID) 
    {
        $examIDSan = $this->db->clean($examID);
        $competencyIDSan = $this->db->clean($competencyID);
        $sql = "INSERT INTO exam_competency_rules (exam_id, competency_id, pass) VALUES "
             . "('$examIDSan', '$competencyIDSan', '40')";
        return $this->db->query($sql);
    }
    
    /**
     * Remove competency rule
     * 
     * @param int $examID          examination identifier
     * @param int $competencyID    competency identifier
     */   
    public function removeCompetencyRule($examID, $competencyID) 
    {
        $examIDSan = $this->db->clean($examID);
        $competencyIDSan = $this->db->clean($competencyID);
        $sql = "DELETE FROM exam_competency_rules "
             . "WHERE exam_id='$examIDSan' AND competency_id='$competencyIDSan'";
        return $this->db->query($sql);
    }
    
    /**
     * Is the divergence threshold exceeded at station
     * 
     * @param int $examID        Examination identifier
     * @param int $stationID     Station identifier
     * @param string $studentID  Student identifier
     */   
    public function divergenceThresholdExceeded($examID, $stationID, $studentID) 
    {
        
        $rules = $this->getRules($examID);
       
        if (empty($rules['divergence_threshold'])) {
           return false;
        }
        
        $scores = $this->db->into_values( 
          $this->db->query(
            "SELECT score FROM student_results 
             WHERE student_id='" . $this->db->clean($studentID) . "' 
             AND station_id=" . $this->db->clean($stationID)     
        ));
           
        if (empty($scores) || count($scores) == 1) {
           return false;   
        }
        
        $divergence = max($scores) - min($scores);
        $exceeds = ($divergence >= $rules['divergence_threshold']); 
        return [
          'exceeds' => $exceeds,
          'divergence' => $divergence,
          'threshold' =>  $rules['divergence_threshold']
        ];
       
    }


    
    
}
