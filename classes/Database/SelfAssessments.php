<?php

 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  * Database function calls relating to Schools & Departments
  */
 namespace OMIS\Database;


 class SelfAssessments
 {

     /**
      * @var CoreDB
      */
     private $db;

     public function __construct(CoreDB $db)
     {
         $this->db = $db;
     }

     /**
      * Get self assessment record
      * @param $exam
      * @return mixed[]
      */
     public function getRecord($exam)
     {

         return $this->db->fetch_row(
             $this->db->query(
                 "SELECT * FROM self_assessments
                 WHERE exam_id = '{$this->db->clean($exam)}'"
             )
         );

     }

     /**
      * Check if an exam has any Self Assessment Scoresheets
      * @param $examId
      * @return bool
      */
     public function existsByExam($examId)
     {

         $sql = "SELECT IF(COUNT(*) > 0, 1, 0) AS result "
             . "FROM self_assessments "
             . "WHERE exam_id = '{$this->db->clean($examId)}'";

         return CoreDB::single_result($this->db->query($sql)) == "1";

     }

     public function scoresheetHasSelfAssessment($formId)
     {

         $formId = $this->db->clean($formId);
         $sql = "SELECT IF(COUNT(*) > 0,1,0) AS RESULT FROM form_sections "
             ."where form_id = '$formId' AND self_assessment = '1'";

             return CoreDB::single_result($this->db->query($sql)) =='1';
     }


     public function isSectionSelfAssessment($sectionId)
     {
         $sectionId = $this->db->clean($sectionId);
         $sql = "SELECT IF(COUNT(*) > 0,1,0) AS RESULT FROM form_sections "
             ."where section_id = '$sectionId' AND self_assessment = '1'";

             return CoreDB::single_result($this->db->query($sql)) == '1';
     }

     /**
      * Students in exam with no self assessments
      * @param $exam
      * @return mixed list of students
      */
     public function studentsWithout($exam)
     {

         $sql = "SELECT student_id FROM group_students
                 INNER JOIN session_groups USING (group_id)
                 INNER JOIN exam_sessions USING (session_id)
                 WHERE exam_id = '{$this->db->clean($exam)}'
                 AND student_id NOT IN (
                    SELECT student_id FROM self_assessment_responses
                    INNER JOIN self_assessment_questions USING(question_id)
                    INNER JOIN self_assessments USING (self_assessment_id)
                    WHERE exam_id = '{$this->db->clean($exam)}'
                 )
                 GROUP BY student_id";

         return CoreDB::into_values($this->db->query($sql));

     }

     /**
      * Lists all the self assessment scoresheets for a given exam
      * @param $examId
      * @return array|null Returns the scoresheet as an array that contains the scoresheet data and the questions grouped
      * by applicant
      */
     public function listByExam($examId)
     {

         $sql = "SELECT "
                . "  self_assessment_questions.question_id, "
                . "  self_assessment_questions.question_ord, "
                . "  self_assessment_questions.question, "
                . "  self_assessment_responses.student_id, "
                . "  self_assessment_responses.response, "
                . "  self_assessment_responses.score "

                . "FROM self_assessments JOIN "
                . "  self_assessment_questions JOIN "
                . "  self_assessment_responses "
                . "ON self_assessments.self_assessment_id = self_assessment_questions.self_assessment_id AND "
                . "  self_assessment_questions.question_id = self_assessment_responses.question_id "

                . "WHERE self_assessments.exam_id = '{$this->db->clean($examId)}' "
                . "ORDER BY student_id, question_ord";

         $rows = CoreDB::into_array($this->db->query($sql));

         if (count($rows) === 0)
             return null;

         return [

             'applicants' => array_reduce($rows, function ($applicants, $row) {
                 $item = [
                     'question' => [
                         'question_ord' => $row['question_ord'],
                         'question_id' => $row['question_id'],
                         'text' => $row['question']
                     ],

                     'response' => [
                         'text' => $row['response'],
                         'score' => $row['score']
                     ]
                 ];

                 $applicantId = $row['student_id'];

                 if (array_key_exists($applicantId, $applicants))
                     $applicants[$applicantId][] = $item;
                 else
                     $applicants[$applicantId] = [$item];

                 return $applicants;
             }, [])
         ];
     }

     /**
      * Insert a new Self Assessment scoresheet
      * @param $examId
      * @param null $title
      * @return mixed|null ID of the new scoresheet or null if the insert failed
      */
     public function insertScoresheet($exam, $title = null, $maxScore = null)
     {

         $recordExists = $this->existsByExam($exam);
         $exam = $this->db->clean($exam);

         if ($title !== null) $title = $this->db->clean($title);
         if ($maxScore !== null) $maxScore = $this->db->clean($maxScore);

         // Record already exists
         if ($recordExists) {

             $sql = "UPDATE self_assessments
                     SET title = '$title', max_score = '$maxScore'
                     WHERE exam_id = '$exam'";

             $scoresheetId = $this->getRecord($exam)['self_assessment_id'];

         // New record
         } else {

             $columnsSql = CoreDB::toSqlList(
                 ['exam_id', $title !== null ? 'title' : null,
                     $maxScore !== null ? 'max_score' : null]);

             $values = CoreDB::toSqlList(
                 [$exam, $title !== null ? "'$title'" : null,
                     $maxScore !== null ? "'$maxScore'" : null]);

             $sql = "INSERT INTO self_assessments $columnsSql "
                  . "VALUES $values";

         }

         if (!$this->db->query($sql))
              return null;

         return $recordExists ? $scoresheetId :
             CoreDB::single_result($this->db->query('SELECT LAST_INSERT_ID()'));

     }

     /**
      * Insert a new row into the self_assessment_questions table
      * @param $selfAssessmentId string ID of the Self Assessment Scoresheet it belongs to
      * @param $question string Question text
      * @param $questionOrd string Order of the question
      *
      * @return mixed|null ID of the newly created question. Null if there was an error inserting
      */
     public function insertScoresheetQuestion($selfAssessmentId, $question, $questionOrd)
     {
         $selfAssessmentId = $this->db->clean($selfAssessmentId);
         $question = $this->db->clean($question);
         $questionOrd = $this->db->clean($questionOrd);

         $sql = "INSERT INTO self_assessment_questions (self_assessment_id, question, question_ord) "
                . "VALUES ($selfAssessmentId, '$question', $questionOrd)";

         if (!$this->db->query($sql))
             return null;

         return CoreDB::single_result($this->db->query('SELECT LAST_INSERT_ID()'));
     }

     /**
      * Inserts a new row into the self_assessment_response table
      * @param $questionId string ID of the question is being responded
      * @param $studentId string ID of the student/applicant that responds
      * @param $score string Score the applicant specified
      * @param null $response string Text of the response
      *
      * @return mixed|null ID of the newly created question. Null if there was an error inserting
      */
     public function insertScoresheetResponse($questionId, $studentId, $score, $response = null)
     {
         $questionId = $this->db->clean($questionId);
         $studentId  = $this->db->clean($studentId);
         $score = $this->db->clean($score);
         if ($response !== null) $response = $this->db->clean($response);

         $columnsSql = CoreDB::toSqlList(['question_id', 'student_id', 'score', $response !== null ? 'response' : null]);
         $values = CoreDB::toSqlList([$questionId, "'$studentId'", $score, $response !== null ? "'$response'" : null]);

         $sql = "INSERT INTO self_assessment_responses $columnsSql "
                . "VALUES $values";

         if (!$this->db->query($sql))
             return null;

         return CoreDB::single_result($this->db->query('SELECT LAST_INSERT_ID()'));
     }

     /**
      * Delete all the scoresheets assigned to a given exam
      * @param $exam ID of the exam
      * @return bool|mysqli_result
      */
     public function deleteScoresheetsForExam($exam)
     {

         $sql = "DELETE FROM self_assessment_questions
                 WHERE self_assessment_id = (SELECT self_assessment_id
                 FROM self_assessments WHERE exam_id = '{$this->db->clean($exam)}')";

         return $this->db->query($sql);

     }

     /**
      * Get the single self assessment score
      * sheet linked to a particular student and exam.
      *
      * @param $examId
      * @param $studentId
      * @return array|bool - Return the first item in the results array. If no results return false.
      */
     public function findByExamAndStudent($examId, $studentId)
     {
         $sql = "SELECT self_assessment_id, exam_id, title, student_id
                 FROM self_assessments
                 WHERE exam_id = '$examId'
                 AND student_id = '$studentId'
                 LIMIT 1";

         $results = CoreDB::into_array($this->db->query($sql));

         return count($results) > 0 ? $results[0] : false;
     }

     /**
      * Get all self assessment items for a self assessment.
      * @param int $selfAssessment     self assessment identifier
      * @param int $section     section identifier
      * @return mixed[]
      */
     public function getItems($selfAssessment, $section)
     {

         $sql = "SELECT * FROM self_assessment_questions
                 INNER JOIN section_items ON (self_assessment_questions.question_ord = section_items.item_order)
                 INNER JOIN form_sections USING (section_id)
                 WHERE section_id = '{$this->db->clean($section)}'
                 AND self_assessment_id = '{$this->db->clean($selfAssessment)}'";

         return CoreDB::into_array($this->db->query($sql));

     }

     /**
      * Remove self assessment items score
      *
      * @param int[] $items   array of items identifiers
      * @param int $result    number identifier for result record
      * @return bool|mysqli_result
      */
     public function deleteItemsScore($items, $result)
     {

         if (empty($items) || empty($result)) {

           return false;

         }

         // Filters (NULLs), sanitises items and implodes to string
         $itemsFiltered = array_filter($items);
         $this->db->clean_array($itemsFiltered);
         $itemString = implode("', '", $itemsFiltered);

         $sql = "DELETE FROM item_scores
                 WHERE result_id = '{$this->db->clean($result)}'
                       AND item_id IN ('$itemString')";

         return $this->db->query($sql);

     }

     /**
      * find self assessment questions by question order and student ID
      *
      * @param int $exam            Exam ID
      * @param int $questionOrder   Question order number
      * @param string $student      Student identifier
      * @return mixed[]
      */
     public function getQuestionByOrderAndStudent($exam, $questionOrder, $student)
     {

         $sql = "SELECT self_assessment_questions.question_id, question, score,
                   response, self_assessment_questions.self_assessment_id
                 FROM self_assessment_questions
                   INNER JOIN self_assessment_responses
                     ON self_assessment_questions.question_id = self_assessment_responses.question_id
                   INNER JOIN self_assessments USING(self_assessment_id)
                 WHERE question_ord = '{$this->db->clean($questionOrder)}'
                   AND exam_id = '{$this->db->clean($exam)}'
                   AND student_id = '{$this->db->clean($student)}'";

         return $this->db->fetch_row($this->db->query($sql));

     }

     /**
      * Get self assessment ownership
      * @param string $student        Identifier for student
      * @param int $selfAssessment    Identifier for self assessment
      * @return bool
      */
     public function getOwnership($student, $selfAssessment)
     {

         $sql = "SELECT *
                 FROM self_assessment_ownerships
                 WHERE student_id = '{$this->db->clean($student)}'
                   AND self_assessment_id = '{$this->db->clean($selfAssessment)}'";

         return $this->db->fetch_row($this->db->query($sql));

     }

     /**
      * Get ownership by student and examiner

      * @param string $student    Identifier for student
      * @param string $exam       Identifier for exam
      * @return mixed[]
      */
      public function getOwnerByStudentExam($student, $exam)
      {

          $sql = "SELECT examiner_id
                  FROM self_assessment_ownerships
                  INNER JOIN self_assessments USING(self_assessment_id)
                  WHERE exam_id = '{$this->db->clean($exam)}'
                    AND student_id = '{$this->db->clean($student)}'
                  GROUP BY self_assessment_id";

           return CoreDB::single_result($this->db->query($sql));

      }
       
     /**
      * Insert ownership to a self assessment exam
      * @param string $student        Identifier for student
      * @param string $examiner       Identifier for examiner
      * @param int $selfAssessment    Identifier for self assessment
      * @return bool|mysqli_result
      */
     public function takeOwnership($student, $examiner, $selfAssessment)
     {

         $student = $this->db->clean($student);
         $examiner = $this->db->clean($examiner);
         $selfAssessment = $this->db->clean($selfAssessment);

         $sql = "INSERT INTO self_assessment_ownerships (student_id, examiner_id, self_assessment_id)
                 VALUES ('$student', '$examiner', '$selfAssessment')";

         if(!$this->db->query($sql)) {

             return false;

         }

         \Analog::info($_SESSION['user_identifier'] . " has taken "
             . "ownership for self assessment (id:$selfAssessment) "
             . gettext('student') . " (id:$student) " . gettext('examiner') . " (id:$examiner)"
         );

         return true;

     }

     /**
      * Cancel ownership of a self assessment exam
      *
      * @param string $student        Identifier for student
      * @param string $examiner       Identifier for examiner
      * @param int $selfAssessment    Identifier for self assessment
      * @return bool status
      */
     public function cancelOwnership($student, $examiner, $selfAssessment)
     {

         $student = $this->db->clean($student);
         $examiner = $this->db->clean($examiner);
         $selfAssessment = $this->db->clean($selfAssessment);

         $sql = "DELETE FROM self_assessment_ownerships
                 WHERE student_id = '$student' AND
                       examiner_id = '$examiner' AND
                       self_assessment_id = '$selfAssessment'";

         $success = $this->db->query($sql);

         if ($success) {

             \Analog::info($_SESSION['user_identifier'] . " has canceled ownership for self assessment (id:$selfAssessment) "
             . gettext('student') . " (id:$student)  " . gettext('examiner') . " (id:$examiner)");

         } else {

             $message = $_SESSION['user_identifier'] . " failed to cancel ownership for self assessment (id:$selfAssessment) "
                      . gettext('student') . " (id:$student) " . gettext('examiner') . " (id:$examiner)";
             \Analog::error($message);
             error_log(__METHOD__ . ": $message");

         }

         return $success;

     }

     /**
      * Remove ownership by exam and group identifiers
      *
      * @param int $exam    Number identifier for an exam
      * @param int $group   Number identifier for a group
      * @return bool        Status
      */
     public function removeOwnershipByExamGroup($exam, $group)
     {

         $assessment = $this->getByExamID($exam);

         if (empty($assessment)) {

           return false;

         }

         $success = $this->db->query(
            "DELETE FROM self_assessment_ownerships
             WHERE self_assessment_id = '{$assessment['self_assessment_id']}'
             AND student_id IN (SELECT student_id
             FROM group_students
             WHERE group_id = '{$this->db->clean($group)}')"
         );

         if ($success) {

             \Analog::info(
                  $_SESSION['user_identifier']. " removed any self assessment ownership for "
                  . gettext('students') . " in " . gettext('exam') . " (id:$exam) and group (id:$group)"
             );

         } else {

             $message = $_SESSION['user_identifier']. " failed to remove self assessment ownership for "
                      . gettext('students') . " in " . gettext('exam') . " (id:$exam) and group (id:$group)";

             \Analog::error($message);
             error_log(__METHOD__ . ": $message");

         }

         return $success;

     }


     /**
      * Remove ownership by result record identifier
      *
      * @param int $result   Number identifier for an result record
      * @return bool         Status
      */
     public function removeOwnershipByResult($result)
     {

         $resultRecord = $this->db->results->getResultByID($result);

         $assessment = $this->getByExamID(
             $this->db->exams->getSessionExamID(
                   $this->db->exams->getSessionIDByStationID($resultRecord['station_id'])
             )
         );

         if (empty($assessment)) {

           return false;

         }

         $success = $this->db->query(
            "DELETE FROM self_assessment_ownerships
             WHERE self_assessment_id = '{$assessment['self_assessment_id']}'
             AND examiner_id = '{$resultRecord['examiner_id']}'
             AND student_id = '{$resultRecord['student_id']}'"
         );

         if ($success) {

             \Analog::info(
                  $_SESSION['user_identifier']. " removed any self assessment ownership for "
                  . gettext('student') . " (id:{$resultRecord['student_id']}) with result (id:$result)"
             );

         } else {

             $message = $_SESSION['user_identifier']. " failed to remove self assessment ownership for "
                      . gettext('student') . " (id:{$resultRecord['student_id']}) with result (id:$result)";

             \Analog::error($message);
             error_log(__METHOD__ . ": $message");

         }

         return $success;

     }


      /**
      * Remove ownership by result record identifier
      *
      * @param int $exam         Number identifier for exam
      * @param string $student   Number identifier for student
      * @return bool         Status
      */
     public function removeOwnershipByExamStudent($exam, $student)
     {

         $assessment = $this->getByExamID($exam);

         if (empty($assessment)) {

           return false;

         }

         $success = $this->db->query(
            "DELETE FROM self_assessment_ownerships
             WHERE self_assessment_id = '{$assessment['self_assessment_id']}'
             AND student_id = '{$this->db->clean($student)}'"
         );

         if ($success) {

             \Analog::info(
                  $_SESSION['user_identifier']. " removed any self assessment ownership for "
                  . gettext('student') . " (id:$student) in " . gettext('exam') . " (id:$exam)"
             );

         } else {

             $message = $_SESSION['user_identifier']. " failed to remove self assessment ownership for "
                      . gettext('student') . " (id:$student) in " . gettext('exam') . " (id:$exam)";

             \Analog::error($message);
             error_log(__METHOD__ . ": $message");

         }

         return $success;

     }


     /**
      * Get result id by student, station and examinerI
      *
      * @param string $student       Identifier for student
      * @param int $station          Identifier for station
      * @param string $examiner      Identifier for examiner

      * @return int                  Identifier of result record
      */
     public function getResultID($student, $station, $examiner)
     {

         $student = $this->db->clean($student);
         $station = $this->db->clean($station);
         $examiner = $this->db->clean($examiner);

         $sql = "SELECT result_id FROM student_results
                 WHERE student_id = '$student'
                       AND station_id = '$station'
                       AND examiner_id = '$examiner' ";

         return CoreDB::single_result($this->db->query($sql));

     }

     public function hasScoresheetBeenSubmitted($stationID, $studentID)
     {

         $studentID = $this->db->clean($studentID);
         $stationID = $this->db->clean($stationID);

         $sql = "SELECT IF(COUNT(*) > 0, 1, 0) FROM student_results
                 WHERE is_complete <> 0
                    AND student_id = " . $studentID . "
                    AND station_id = " . $stationID;

         return CoreDB::single_result($this->db->query($sql)) == "1";

     }

     /**
      * Returns a self assessment of an exam
      *
      * @param int $exam        Identifier for exam
      * @return mixed[]
      */
     public function getByExamID($exam)
     {

         return $this->db->fetch_row($this->db->query(
            "SELECT * FROM self_assessments
             WHERE exam_id = '{$this->db->clean($exam)}'"
         ));

     }

    /**
     * Get self assessment question
     * @param int $exam            Identifier for exam
     * @param int $order           Question Order Number
     * @return mixed[]
     */
    public function getQuestion($exam, $order)
    {

        $sql = "SELECT question_id, question FROM self_assessment_questions
                INNER JOIN self_assessments USING (self_assessment_id)
                WHERE exam_id = '{$this->db->clean($exam)}'
                AND question_ord = '{$this->db->clean($order)}'";

        return $this->db->fetch_row($this->db->query($sql));

     }

    /**
     * Get self assessment section by form id
     * @param int $form        Identifier for form
     * @return mixed[]
     */
    public function getSection($form)
    {

        $sql = "SELECT * FROM form_sections
                WHERE self_assessment = 1
                AND form_id = '{$this->db->clean($form)}'";

        return $this->db->fetch_row($this->db->query($sql));

     }

    /**
     * Get non self assessment possible
     * @param int $form        Identifier for form
     * @return mixed[]
     */
    public function getNonOwnerPossible($form)
    {

        $sql = "SELECT SUM(GREATEST(lowest_value, highest_value))
                 AS total FROM form_sections
                INNER JOIN section_items USING(section_id)
                WHERE form_sections.self_assessment = 0
                 AND form_id = '{$this->db->clean($form)}'
                 AND section_items.type
                  NOT IN ('radiotext', 'checkbox')";

        $sqlResult = $this->db->query($sql);
        return $sqlResult ? CoreDB::single_result($sqlResult) : 0;

    }
    /*
     * get ownership status by student_id and examiner_id
     */
    public function getOwnershipByStudentExaminer($studentId, $examinerId) {
        $sql = "Select * from self_assessment_ownerships WHERE student_id ='$studentId'
                          AND examiner_id='$examinerId';";
        return $this->db->getAffectedRowCount($this->db->query($sql));
    }
    /*
     * Get the weighted possible and the raw possible of form excluding the self assessment section
     */
    public function selfAssessedScore($resultId) {
        $sql = "SELECT SUM(highest_value) as poss,
                      SUM(section_items.highest_value*section_items.weighted_value) AS weightedPoss from section_items where section_id =
                      (SELECT form_sections.section_id FROM form_sections
                      JOIN session_stations on form_sections.form_id = session_stations.form_id
                      JOIN student_results on session_stations.station_id=student_results.station_id
                      WHERE self_assessment = 0 AND result_id ='$resultId') ;";
                      return $this->db->fetch_row($this->db->query($sql));
    }

 }
