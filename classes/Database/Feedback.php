<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to feedback tool
 */

namespace OMIS\Database;

class Feedback
{
    // Core Database 'coreDB' reference
    private $db;

    // Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get feedback template (email)
     * 
     * @param int $templateID
     * @param bool $toArray
     * @param string $index
     * @return mixed[]
     */
    public function getEmailTemplates($templateID = null, $toArray = false, $index = null)
    {
        $templateSql = "";
        if (!empty($templateID)) {
            $templateSql = " WHERE email_templates.template_id = '"
                . $this->db->clean($templateID) . "'";
        }
        $sql = "SELECT * FROM email_templates" . $templateSql . " ORDER BY "
             . "LENGTH(template_name), template_name";

        $mysqliResult = $this->db->query($sql);

        if ($toArray) {
          if ($index !== null) {
             $resultArray = CoreDB::into_array($mysqliResult);
             $returnData = CoreDB::group($resultArray, [$index], true);  
          } else {
             $returnData = $this->db->fetch_row($mysqliResult);
          }
        } else {
           $returnData = $mysqliResult;
        }

        return $returnData;
    }

    /**
     * Update Feedback Template
     * 
     * @param int $id
     * @param string $templateName
     * @param string $emailCC
     * @param string $emailSubject
     * @param html $emailBody
     * @param html $pdfInstructions
     * @param enum('no', 'yes', 'fail') $instructionsEnabled
     * @return int (0, 1)
     */
    public function updateEmailTemplate($id, $data)
    {
    
        $instructSql = $data['instructions_enabled'] == "no" ? '' 
        : "pdf_instructions = '{$this->db->clean($data['instructions'])}',";

        $sql = "UPDATE email_templates SET 
               template_name = '{$this->db->clean($data['name'])}',
               email_cc = '{$this->db->clean($data['cc'])}',  
               email_subject = '{$this->db->clean($data['subject'])}', 
               email_body = '{$this->db->clean($data['body'])}', 
               $instructSql
               pdf_instructions_enabled = '{$this->db->clean($data['instructions_enabled'])}' 
               WHERE template_id = '{$this->db->clean($id)}'";

        return (int) $this->db->query($sql);

    }
     
    /**
     * Clone Feedback Template
     * @param int $templateID
     * @return string|bool New Template ID|false
     */  
    public function cloneEmailTemplate($templateID) 
    {
       // Get all templates
       $this->db->set_Convert_Html_Entities(false);
       $allTemplates = CoreDB::into_array(
           $this->getEmailTemplates(null)
       );
       $this->db->set_Convert_Html_Entities(true);

       if (empty($allTemplates)) {
          return false;
       }

       // Sort 'template_id' field by natural order
       array_multisort(
         array_column($allTemplates, 'template_id'), 
          SORT_ASC, SORT_NATURAL, $allTemplates
       );

       //  New template ID
       $lastTemplateID = $allTemplates[count($allTemplates) -1]['template_id'];
       $newID = 'tr' . (preg_replace('/\D/', '', $lastTemplateID) + 1);
       
       // Retrieve data to clone and add new template record
       foreach ($allTemplates as $each) {

           if ($templateID == $each['template_id']) {

              $sql = "INSERT INTO email_templates "
                   . "(template_id, template_name, email_subject, email_body) "
                   . "VALUES ('".$newID."', '" . $each['template_name'] . " " 
                   . "- " . date("d-m-Y H:i") ."', '".$each['email_subject']."', " 
                   . "'".$each['email_body']."')";

              $this->db->query($sql);
              return $newID;

           }

       }

       // No template to clone, return false
       return false;

    }

    /**
     * Get student exam feedback records
     * @param int $examID      ID number of the exam
     * @param int $sessionID   ID number for a session (optional)
     * @return mysqli_result   student records
     */
    public function getStudentFeedbackRecords($examID, $sessionID = null)
    {
        $examID = $this->db->clean($examID);
        $sessionID = $this->db->clean($sessionID);
        $sessionClause = is_null($sessionID) ? "" : "AND session_id = '$sessionID' ";
        
        $sql = "SELECT list.group_id, list.group_name, list.session_description, "
             . "list.session_id, list.session_date, list.student_id, list.forename, "
             . "list.surname, list.email, list.exam_id, feedback.time_created, receives_instructions, "
             . "feedback.time_sent, feedback.is_created, feedback.is_sent, feedback.is_ready, feedback.template_id "
             . "FROM (SELECT session_groups.group_id, session_groups.group_name, exam_sessions.session_id, "
             . "exam_sessions.session_date, exam_sessions.session_description, students.student_id, students.forename, "
             . "students.surname, students.email, exam_sessions.exam_id "
                
             . "FROM ((((students INNER JOIN student_results USING (student_id)) "
             . "INNER JOIN session_stations USING (station_id)) "
             . "INNER JOIN exam_sessions USING (session_id)) "
             . "INNER JOIN session_groups USING (session_id)) "
             . "INNER JOIN group_students "
             . "ON (students.student_id = group_students.student_id AND session_groups.group_id = group_students.group_id) "
             . "WHERE exam_sessions.exam_id = '$examID' $sessionClause"
             . "AND student_results.absent = '0' AND student_results.is_complete = '1' "
             . "GROUP BY students.student_id) AS list "
                
             . "LEFT JOIN (SELECT * FROM student_feedback WHERE exam_id = '$examID') "
             . "AS feedback USING (student_id) "
             . "GROUP BY list.student_id "
             . "ORDER BY list.session_id, list.group_id";
        
        return $this->db->query($sql);
    }

    /**
     * Set Feedback Ready
     * @param int $examID         Identifier number for the exam
     * @return bool true|false    Success true|false
     */
    public function setFeedbackReady($examID)
    {
        $sql = "UPDATE student_feedback SET is_ready = '1' WHERE exam_id = '"
             . $this->db->clean($examID) . "'";

        return $this->db->query($sql);
    }

    /**
     * Set Feedback Created
     * @param string $studentID             Identifier number for the student
     * @param int $examID                   Identifier number for the exam
     * @param string $templateID            Identifier number for the template
     * @param int (0,1) $instructions       PDF instructions
     * @param date $dateTime                Timestamp
     * @param bool $isCreated               Set record as created
     * @return bool true|false      Success true|false
     */    
    public function setFeedbackCreated($studentID, $examID, $templateID, $instructions, $dateTime, $isCreated)
    {
        
        $studentID = $this->db->clean($studentID);
        $examID = $this->db->clean($examID);
        $templateID = $this->db->clean($templateID);
        $dateTime = $this->db->clean($dateTime);
        $isCreated = $this->db->clean($isCreated);
        $instructions = $this->db->clean($instructions);

        $sql = "INSERT INTO student_feedback (student_id, exam_id, template_id, "
             . "time_created, is_ready, is_created, is_sent, receives_instructions) "
             . "VALUES ('$studentID', '$examID', '$templateID', '$dateTime', '0', "
             . "'$isCreated', '0', '$instructions') ON DUPLICATE KEY UPDATE template_id = '$templateID', "
             . "time_created = '$dateTime', is_ready = '0', is_created = '$isCreated', is_sent = '0'";

        return $this->db->query($sql);

    }

    /**
     * Get feedback created count
     * @param int $examID   Identifier number for the exam
     **/
    public function getFeedbackCreatedCount($examID)
    {
        $sql = "SELECT COUNT(student_id) as count FROM student_feedback WHERE "
             . "exam_id = '" . $this->db->clean($examID)
             . "' AND is_created = '1'";

        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Delete feedback records
     * @param int $examID   Identifier number for the exam
     * @param string $studentID   Identifier number for the student 
     * @return bool true|false    Success true|false    
     **/
    public function deleteFeedbackRecords($examID, $studentID = null)
    {
        $studentClause = ($studentID === null) ? "" : " AND student_id = '" . $this->db->clean($studentID). "'";
        $sql = "DELETE FROM student_feedback WHERE exam_id = '" . $examID . "'" . $studentClause;

        return $this->db->query($sql);
    }

    /**
     * Get exam feedback data
     * 
     * @param int $examID     ID number for an exam
     * @return mixed array    Array of results ID's and all of the section 
     *                        feedback concatenated into one string for each
     */
    public function getExamFeedbackData($examID)
    {

        // Increase concat maximum length
        $this->db->query("SET SESSION group_concat_max_len = 10000");

        $sql = "SELECT feedback_data.result_id, 
                 group_concat(feedback_data.item_feedback SEPARATOR ' ') AS item_feedback,
                 group_concat(feedback_data.section_feedback SEPARATOR ' ') AS section_feedback 
                FROM (SELECT student_results.result_id, section_items.section_id, 
                group_concat(item_feedback.feedback_text SEPARATOR ' ') AS item_feedback, 
                section_feedback.feedback_text AS section_feedback 
                FROM 
                    (((((exam_sessions 
                    INNER JOIN session_stations USING (session_id)) 
                    INNER JOIN student_results USING (station_id)) 
                    INNER JOIN form_sections USING(form_id)) 
                    LEFT JOIN section_items ON (form_sections.section_id = section_items.section_id OR section_items.item_id IS NULL)) 
                    LEFT JOIN item_feedback ON (
                       student_results.result_id = item_feedback.result_id AND 
                       section_items.item_id = item_feedback.item_id)) 
                    LEFT JOIN section_feedback ON (  
                       student_results.result_id = section_feedback.result_id AND  
                       form_sections.section_id = section_feedback.section_id) 
                WHERE 
                    exam_id = '{$this->db->clean($examID)}' 
                GROUP BY student_results.result_id, form_sections.section_id) 
                AS feedback_data 
                GROUP BY result_id";
        

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);

        return CoreDB::group($resultArray, ['result_id'], true, true);        
        
    }

    /**
     * Get section feedback
     * 
     * @param int $sectionID                 ID number for section
     * @param int $resultID                  ID number for result
     * @param int $excludeInternalFeedback   exclude feedback marked as internal
     * @return string
     */
    public function getSectionFeedback($sectionID, $resultID, $excludeInternalFeedback = false)
    {
        $sectionID = $this->db->clean($sectionID);
        $resultID = $this->db->clean($resultID);

        $countSql = "SELECT COUNT(*) FROM section_feedback "
                  . "WHERE result_id = '$resultID'";
        $count = CoreDB::single_result($this->db->query($countSql));

        $excludeClause = ($excludeInternalFeedback) ? "AND internal_feedback_only = 0 " : '';
        
        if ($count > 0) {
          
            $sql = "SELECT feedback_text FROM section_feedback "
                 . "INNER JOIN form_sections USING(section_id) " 
                 . "WHERE section_id = '$sectionID' "
                 . "AND result_id = '$resultID' "
                 . "$excludeClause";

            return CoreDB::single_result($this->db->query($sql));
            
        } else {
            return "";
        }
    }

    /**
     * Get section feedback
     *
     * @param int $id                        ID number for item
     * @param int $resultID                  ID number for result
     * @param int $excludeInternalFeedback   exclude feedback marked as internal
     * @return string
     */
    public function getItemFeedback($id, $resultID, $excludeInternalFeedback = false)
    {

        $excludeClause = $excludeInternalFeedback ? "AND internal_feedback_only = 0 " : '';

        $result = CoreDB::single_result($this->db->query(
            "SELECT feedback_text FROM item_feedback 
             INNER JOIN section_items USING(item_id) 
             INNER JOIN form_sections USING(section_id) 
             WHERE item_feedback.item_id = '{$this->db->clean($id)}' 
             AND result_id = '{$this->db->clean($resultID)}' $excludeClause 
             GROUP BY item_feedback.item_id"
        ));

        return empty($result) ? "" : $result;

    }

    /**
     * Insert examiner comments per section
     *
     * @param  int     $sectionID    ID of section in form
     * @param  int     $resultID     Result identifier
     * @param  string  $feedbackText Feedback Text
     * @return boolean TRUE if a new row was created, otherwise false.
     */
    public function insertExaminerFeedback($sectionID, $resultID, $feedbackText)
    {
        $sectionID = $this->db->clean($sectionID);
        $resultID = $this->db->clean($resultID);
        $feedbackText = $this->db->clean($feedbackText);

        $sql = "INSERT INTO section_feedback (section_id, result_id, feedback_text) "
             . "VALUES ('" . $sectionID . "', '" . $resultID . "', '" . $feedbackText . "')";

        return $this->db->query($sql);
    }

    /**
     * Insert examiner item comment/feedback per section
     *
     * @param  int     $id         ID of competence at station
     * @param  int     $resultID   Result identifier
     * @param  string  $text       Feedback Text
     * @return boolean TRUE if a new row was created, otherwise false.
     */
    public function insertItemFeedback($id, $resultID, $text)
    {

        return $this->db->query(
           "INSERT INTO item_feedback (item_id, result_id, feedback_text) 
            VALUES ('{$this->db->clean($id)}', '{$this->db->clean($resultID)}', 
                    '{$this->db->clean($text)}')"
        );

    }

    /**
     * Rewire exam feedback from 1 form to another
     * 
     * @int $id               ID number of form
     * @int $examID           ID number of exam
     * @int[] $sectionIDMap   Section ID mapping (replace old IDs with new ones)
     * @param bool true|false
     */    
    public function switchFormFeedback($id, $examID, $sectionIDMap) 
    {
      
        $success = true;

        /*
         * Need to get result IDs first not to upset triggers
         * when updating same table
         */
        $resultIDs = CoreDB::into_values(
            $this->db->query(
               "SELECT DISTINCT result_id 
                FROM student_results 
                INNER JOIN session_stations USING (station_id) 
                INNER JOIN exam_sessions USING (session_id) 
                WHERE exam_id = '" . $this->db->clean($examID) . "' 
                AND form_id = '" . $this->db->clean($id) . "'"
            )
        );
        
        if (empty($sectionIDMap)) {
        
            return false;
               
        } else if (empty($resultIDs)) {
            
            return true;
            
        }
        
        foreach ($sectionIDMap as $oldSectionID => $newSectionID) {
    
            $success &= $this->db->query(
                "UPDATE section_feedback 
                 SET section_id = '" . $this->db->clean($newSectionID) . "' 
                 WHERE result_id IN (" . implode(",", $resultIDs) . ") 
                 AND section_id  = '" . $this->db->clean($oldSectionID) . "'"
            );

        }
        
        return $success;

    }
  
}
