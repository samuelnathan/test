<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * Database function calls relating to grade values
 */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class GradeValues extends AbstractEntityDAO
 {

    protected function getTableName() { return 'grade_values'; }
    protected function getIds() { return ['grade_value_id']; }

 }

