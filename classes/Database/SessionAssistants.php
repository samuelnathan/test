<?php

namespace OMIS\Database;


use OMIS\Auth\Role;
use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class SessionAssistants extends AbstractEntityDAO
{

    /**
     * Get session assistants
     * @param int $id             Identifier of session
     * @param bool $toArray       Convert to array
     * @return mixed[]
     */
    public function getSessionAssistants($id, $toArray = false)
    {

        $sql = "SELECT session_assistants.user_id, users.forename, users.surname, users.email, "
            . "users.contact_number, users.user_role FROM session_assistants "
            . "INNER JOIN users USING(user_id) WHERE session_id = "
            . "'" . $this->db->clean($id) . "'";

        if ($toArray) {
            return CoreDB::into_array($this->db->query($sql));
        } else {
            return $this->db->query($sql);
        }

    }

    //Attach Assistants to Session [Updated 05/02/2014]
    public function attachAssistantsToSession($user_id, $session_id)
    {
        $user_id = $this->db->clean($user_id);
        $session_id = $this->db->clean($session_id);

        $sql = "INSERT INTO session_assistants (user_id, session_id) "
            . "VALUES ('" . $user_id . "', " . $session_id . ")";

        return $this->db->query($sql);
    }

    //Remove Session Assistants [Updated 05/02/2014]
    public function removeSessionAssistants($toDelete, $session_id)
    {
        $count = count($toDelete);
        $removeCount = 0;
        $index = 0;

        while ($index < $count) {
            $sql = "DELETE FROM session_assistants WHERE user_id = '"
                . $this->db->clean($toDelete[$index]) . "' AND session_id = '"
                . $this->db->clean($session_id) . "'";

            if ($this->db->query($sql)) {
                \Analog::info(
                    $_SESSION['user_identifier'] . " removed assistant with ID: "
                    . $toDelete[$index] . " from session with ID: " . $session_id
                );
                $removeCount++;
            }

            $index++;
        }
        return $removeCount;
    }

    //Get assistants not attached to session [Updated 10/02/2014]
    public function getAssistantsNotInSession($session_id)
    {
        $usersQ = "SELECT user_id FROM session_assistants WHERE session_id = '"
            . $this->db->clean($session_id) . "'";

        $sql = "SELECT user_id, users.forename, users.surname, users.contact_number, "
            . "users.email FROM users WHERE user_id NOT IN (" . $usersQ
            . ") AND user_role = " . Role::USER_ROLE_EXAM_ASSISTANT;

        return $this->db->query($sql);
    }

    //Get Session Assistants Count [Updated 10/02/2014]
    public function getSessionAssistantsCount($session_id)
    {
        $sql = "SELECT COUNT(user_id) FROM session_assistants WHERE session_id = '"
            . $this->db->clean($session_id) . "'";

        return CoreDB::single_result($this->db->query($sql));
    }

    //Does Session have Assistants [Updated 27/01/2014]
    public function doesSessionHaveAssistants($session_id)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT COUNT(*) FROM session_assistants WHERE session_id = '"
            . $session_id . "'";

        $cnt = CoreDB::single_result($this->db->query($sql));
        return ($cnt > 0);
    }

    //Clone Assistants [Updated 06/02/2014]
    public function cloneAssistants($new_session_id, $org_session_id)
    {
        $new_session_id = $this->db->clean($new_session_id);

        if ($this->db->sessions->doesSessionExist($org_session_id)) {
            $assistsDB = $this->getSessionAssistants($org_session_id);
            while ($assist = mysqli_fetch_assoc($assistsDB)) {
                $this->attachAssistantsToSession($assist['user_id'], $new_session_id);
            }
        }
    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds()
    {
        return ['session_id', 'user_id'];
    }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'session_assistants';
    }
}