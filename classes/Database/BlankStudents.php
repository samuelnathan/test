<?php

namespace OMIS\Database;


use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class BlankStudents extends AbstractEntityDAO
{

    /**
     * Delete Blank Students in Group
     */
    public function deleteBlankStudentsInGroup($group_id)
    {
        $sql = "DELETE FROM blank_students WHERE group_id = '" . $this->db->clean($group_id) . "'";
        return $this->db->query($sql);
    }

    //Delete Blank Students Submitted By Group and Station Number [Updated 05/02/2014]
    public function deleteBlanksSubmitted($group_id, $station_number)
    {
        $sql = "DELETE blank_students_discarded.* FROM blank_students_discarded, "
            . "blank_students WHERE blank_students.blank_student_id = blank_students_discarded.blank_student_id "
            . "AND blank_students_discarded.station_number = '" . $this->db->clean($station_number)
            . "' AND group_id = '" . $this->db->clean($group_id) . "'";

        return $this->db->query($sql);
    }

    //Delete Blank Students Submitted By Station [Updated 05/02/2014]
    public function deleteBlanksSubmittedBySession($session_id)
    {
        $exam_id = $this->db->sessions->getSessionExamID($session_id);
        $uniqueStationNums = $this->db->stations->getUniqueStationNumbers($exam_id, $session_id);
        $notInQ = (count($uniqueStationNums) == 0) ? "" : " AND station_number NOT IN("
            . implode(',', $uniqueStationNums) . ")";

        $sql = "DELETE blank_students_discarded.* FROM (blank_students "
            . "INNER JOIN session_groups USING(group_id)) "
            . "INNER JOIN blank_students_discarded USING(blank_student_id) "
            . "WHERE session_groups.session_id = '" . $this->db->clean($session_id)
            . "'" . $notInQ;

        return $this->db->query($sql);
    }

    //Get Unused student blanks [Updated 04/02/2014]
    /**
     * Return any blank students for a given student group and station number.
     *
     * @param int  $station_number  Number of station (can be shared among stations to allow multiple scenarios)
     * @param int  $group_id     ID number of student group
     * @param bool $convertArray if TRUE, return results as an array; otherwise, a mysqli_result object.
     * @return mixed Depends on $convertArray and if there are matching records or not.
     */
    public function getUnusedStudentBlanks($station_number, $group_id, $convertArray = false)
    {
        $group_id = $this->db->clean($group_id);
        $station_number = $this->db->clean($station_number);
        $sql = "SELECT blank_students.blank_student_id, blank_students.student_order "
            . "FROM blank_students WHERE group_id = '" . $group_id
            . "' AND blank_students.blank_student_id NOT IN "
            . "(SELECT blank_student_id FROM blank_students_discarded "
            . "INNER JOIN blank_students USING(blank_student_id)"
            . " WHERE group_id = '" . $group_id . "' AND station_number = '"
            . $station_number . "')";

        $result = $this->db->query($sql);
        return ($convertArray) ? CoreDB::into_array($result) : $result;
    }

    //Ignore Blank Student [Updated 04/02/2014]
    public function ignoreBlankStudent($blank_student_id, $station_number)
    {
        $blank_student_id = $this->db->clean($blank_student_id);
        $station_number = $this->db->clean($station_number);
        $sql = "INSERT IGNORE INTO blank_students_discarded (blank_student_id, station_number) "
            . "VALUES ('" . $blank_student_id . "', '" . $station_number . "')";
        return $this->db->query($sql);
    }

    //Get Blank Students [Updated 04/02/2014]
    public function getBlankStudents($group_id)
    {
        $group_id = $this->db->clean($group_id);
        $query = "SELECT * FROM blank_students WHERE group_id = '" . $group_id
            . "' ORDER BY blank_students.student_order";
        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Check whether a blank student has been discarded or not
     * @param $blankStudentId int ID of the blank student
     * @param $stationNum int Station number
     * @return string
     */
    public function isBlankStudentDiscarded($blankStudentId, $stationNum)
    {
        $blankStudentId = $this->db->clean($blankStudentId);
        $stationNum = $this->db->clean($stationNum);

        $query = "SELECT IF(COUNT(*) > 0, 1, 0) "
            . "FROM blank_students_discarded "
            . "WHERE blank_student_id = $blankStudentId AND station_number = $stationNum";

        return CoreDB::single_result($this->db->query($query)) == "1";
    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds()
    {
        return ['blank_student_id'];
    }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'blank_students';
    }
}