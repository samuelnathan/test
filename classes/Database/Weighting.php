<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 include_once __DIR__ . '/../../extra/usefulphpfunc.php';

 class Weighting extends AbstractEntityDAO
 {
     
     /**
      * Get current examiner station weighting
      */
     public function examinerStationWeighting($examinerID, $stationID) {

         $examinerIDSan = $this->db->clean($examinerID);
         $stationIDSan = $this->db->clean($stationID);

         $sql = "SELECT scoring_weight, weight_type "
             . "FROM station_examiners "
             . "INNER JOIN weighting "
             . "USING (scoring_weight) "
             . "WHERE examiner_id = '$examinerIDSan' "
             . "AND station_id = '$stationIDSan'";

         $mysqliResult = $this->db->query($sql);

         // None found
         if (!$mysqliResult || mysqli_num_rows($mysqliResult) == 0) {
             return false;

             // Weightings found
         } else {
             return $this->db->fetch_row($mysqliResult);
         }

     }

     /**
      * Get the list of examiner weightings available to choose from.
      *
      * @return mixed[] Mapping of numerical weights to their descriptions.
      */
     public function getWeightingList()
     {
         $sql = "SELECT * FROM weighting";
         $results = CoreDB::into_array($this->db->query($sql));
         if (empty($results)) {
             return [];
         } else {
             // (DW) Slice up the array to return it as a set of key -> value pairs.
             return array_combine(
                 array_column($results, 'scoring_weight'),
                 array_column($results, 'weight_type')
             );
         }
     }


     /**
      * Get the name of the columns that form the primary key of the entity in the database
      *
      * @return array
      */
     protected function getIds()
     {
         return ['scoring_weight'];
     }

     /**
      * Get the name of the table that represents the entity in the database
      *
      * @return string
      */
     protected function getTableName()
     {
         return 'weighting';
     }
 }