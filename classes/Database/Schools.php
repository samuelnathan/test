<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to Schools & Departments
 */

namespace OMIS\Database;

use OMIS\Database\DAO\AbstractEntityDAO;

class Schools extends AbstractEntityDAO
{

    /**
     * Get a list of all schools known on the system.
     * 
     * @return mysqli_result|bool FALSE if query fails, otherwise mysqli_result.
     */
    public function getAllSchools()
    {
        return $this->db->query("SELECT * FROM schools ORDER BY school_description");
    }

    /**
     * Get a specific school returned as an associative array.
     * 
     * @param int $school_id ID number of school
     * @return mixed[]|null NULL if query fails, otherwise an associative array
     */
    public function getSchool($school_id) { return $this->getById($school_id); }

    //Get School By Department [Updated 05/02/2014]
    /**
     * Get the school associated with a specific department as an associative array.
     * 
     * @param string $dept_id Department ID
     * @return mixed[]|null NULL on failure, otherwise the school as an associative array
     */
    public function getSchoolByDepartment($dept_id)
    {
        $sql = "SELECT * FROM schools WHERE school_id = "
            . "(SELECT school_id FROM departments WHERE dept_id = '"
            . $this->db->clean($dept_id) . "')";

        return $this->db->fetch_row($this->db->query($sql));
    }

    /**
     * Add a new school to the database.
     * 
     * @param string $name Name of new school.
     * @return bool TRUE on success, FALSE on failure.
     * @throws \InvalidArgumentException If the name is invalid or already used.
     */
    public function insertSchool($name)
    {
        // Strip off any leading or trailing whitespace.
        $name = trim($name);
        
        // Don't allow an empty school.
        if ($name === '') {
            throw new \InvalidArgumentException('School Name cannot be empty');
        }
        
        /* Check that the name of the school we're about to add is not already
         * in use.
         */
        $existing_schools = CoreDB::into_array($this->getAllSchools());
        foreach ($existing_schools as $school) {
            $school_name = strtolower(trim($school['school_description']));
            if ($school_name === strtolower($name)) {
                throw new \InvalidArgumentException("School Name '$name' already in use");
            }
        }
        
        /* It's the school's name, but the field is called school_description.
         * I guess it's a historical throwback thing.
         */
        $sql = "INSERT INTO schools (school_description) VALUES ('"
            . $this->db->clean($name) . "')";

        $success = $this->db->query($sql);

        if ($success) {
            \Analog::info($_SESSION['user_identifier'] . " created school " . $name);
        }

        return $success;
    }

    /**
     * Update a school's details.
     * 
     * @param int $school_id ID number of school to update
     * @param string $school_desc Updated School Description (really Name)
     * @return bool TRUE on update success, FALSE on failure.
     * @throws \InvalidArgumentException If the name is invalid or already used.
     */
    public function updateSchool($school_id, $school_desc)
    {
        $id = (int) $school_id;
        $name = trim($this->db->clean($school_desc));

        // Don't try to update a school that doesn't exist.
        if (!$this->doesSchoolExist($id)) {
            throw new \InvalidArgumentException("School with ID '$id' not found");
        }
        
        // Don't allow an empty school.
        if ($name === '') {
            throw new \InvalidArgumentException('School Name cannot be empty');
        }
        /* Check that the name of the school we're about to add is not already
         * in use.
         */
        $existing_schools = CoreDB::into_array($this->getAllSchools());
        foreach ($existing_schools as $school) {
            $school_name = strtolower(trim($school['school_description']));
            if ($school_name === strtolower($name) && (int) $school['school_id'] !== $id) {
                throw new \InvalidArgumentException("School Name '$name' already in use");
            }
        }
        
        $sql = "UPDATE schools SET school_description = '$name' WHERE "
            . "school_id = $id";

        // UPDATE query will return TRUE or FALSE
        return ($this->db->query($sql));
    }

    /**
     * Check if the school with ID $school_id exists in the database or not.
     * 
     * @param int $school_id ID of school to check
     * @return bool TRUE if the school exists, FALSE if not.
     */
    public function doesSchoolExist($school_id) { return $this->existsById($school_id); }

    //Delete Schools [Updated 05/02/2014]
    public function deleteSchools($schools)
    {
        global $config;
        foreach ($schools as $school) {
            $school = $this->db->clean($school);
            $result = $this->db->query("DELETE FROM schools WHERE school_id = '" . $school . "'");

            if (!$result) {
                // Failed to delete the school
                continue;
            }
            
            $infoQ = "SELECT school_description FROM schools WHERE school_id = '" . $school . "'";
            $schoolName = CoreDB::single_result($this->db->query($infoQ));
            \Analog::info(
                $_SESSION['user_identifier'] . " deleted school with description "
                . $schoolName . " from " . $config->getName()
            );
        }
    }

    /**
     * Get School & Department Table Headers
     */
    public function getSchoolDepartmentColumns()
    {
        
        global $config;
        $dbName = $config->db['schema'];

        $sql = "SELECT DISTINCT COLUMN_NAME FROM INFORMATION_SCHEMA.Columns WHERE "
            . "TABLE_NAME IN('schools','departments') AND TABLE_SCHEMA = '" . $dbName 
            . " AND COLUMN_NAME NOT IN ('updated_at', 'created_at')";

        return CoreDB::into_values($this->db->query($sql));

    }

    /**
     * Get Admin Schools
     * @param string    $user identifier of user
     * @param mixed[]   $array (optional) return data as array
     * @param bool      $allSchools (optional) return all schools
     * @return mixed[]|$mysqliResult  list of schools that admin is linked to
     */
    public function getAdminSchools($user, $array = true, $allSchools = false) 
    {
        $joinClause = $allSchools ? 'LEFT' : 'INNER';
        $userSan = $this->db->clean($user);
        $mysqliResult = $this->db->query(
           "SELECT schools.*, administrator.user_id FROM schools $joinClause JOIN 
           (SELECT * FROM school_admins WHERE user_id = '$userSan') AS 
           administrator USING (school_id) 
           ORDER BY schools.school_description"       
        );
        
        return ($array ? CoreDB::into_array($mysqliResult) : $mysqliResult);

    }

     /**
     * Add admin to schools <DC TODO: Need to add proper return status>
     * @param string    $user identifier of user
     * @param int[]     $schools (optional) school identifiers to link to, 
     *                  null => all schools
     * @return bool     Insert status
     */
    public function addAdminToSchools($user, $schools = null) 
    {
        $userSan = $this->db->clean($user);

        /**
         * Sanitize School Identifiers
         */
        if (!empty($schools) && is_array($schools)) {
            $schoolsSan = array_map(
                function ($value) {
                    return "'" . trim($this->db->clean($value), "'\"") . "'";
                },
                $schools
            );
        } else {
           $schoolsSan = CoreDB::into_values($this->getAllSchools());
        }

        // Insert Schools
        foreach ($schoolsSan as $school) {
            $this->db->query(
              "INSERT INTO school_admins (school_id, user_id) 
               VALUES ($school, '$userSan') "
            );
        }
         
        return true;
    }

    /**
     * Remove admin from all schools
     * @param string    $user identifier of use
     * @param int[]     $schools (optional) specific school identifiers to remove, 
     *                  null => all schools
     * @return bool     Delete status
     */
    public function removeAdminFromSchools($user, $schools = null)
    {

        // Sanitize School Identifiers
        if (!empty($schools) && is_array($schools)) {
            $schoolsSan = array_map(
                function ($value) {
                    return "'" . trim($this->db->clean($value), "'\"") . "'";
                },
                $schools
            );

            $schoolClause = " AND school_id IN (" . implode(",", $schoolsSan) . ")";

        } else {

            $schoolClause = "";          
  
        }

        return $this->db->query(
            "DELETE FROM school_admins WHERE user_id = '" .
            $this->db->clean($user) . "'" .
            $schoolClause
        );

    }

    protected function getTableName() { return 'schools'; }

    protected function getIds() { return ['school_id']; }

}
