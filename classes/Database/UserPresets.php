<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to user presets
 */

namespace OMIS\Database;

class UserPresets
{
    // Core Database 'coreDB' reference
    private $db;

    // Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    
    /**
     * Update CURRENT user presets record
     * 
     * @param mixed[] $data   data containing all of the preset information
     * @return bool  status true|false
     */
    public function update($data)
    {
        if ($this->db->users->doesUserExist($_SESSION['user_identifier']) && count($data) == 0) {
          return false;
        }
       
        $setSql = "";
        foreach ($data as $key => $value) {
            $setSql .= $key . " = '" . $this->db->clean($value) . "',";
        }
        $sql = "UPDATE user_presets SET " . substr_replace($setSql, '', -1) . " "
             . "WHERE user_id = '" . $this->db->clean($_SESSION['user_identifier']) . "'";
        return $this->db->query($sql);
       
    }
    
    /**
     * Get CURRENT user preset record
     * @return mixed[]  preset records for current user
     */
    public function get()
    {
       // Create record, also checks if non-exist
       $this->create();
       
       $sql = "SELECT * FROM user_presets WHERE user_id = '"
            . $this->db->clean($_SESSION['user_identifier']) . "'";
       
       return $this->db->fetch_row($this->db->query($sql));
    }
    
    
    /**
     * New user preset record for CURRENT logged in user
     * @return bool true|false status
     */
    public function create() 
    {
  
       $currentUser = $this->db->clean($_SESSION['user_identifier']);
       $sql = "SELECT * FROM user_presets WHERE user_id = '$currentUser'";
       $resultset = $this->db->query($sql);
      
       // Create row if non-exist
       if (mysqli_num_rows($resultset) == 0) {
           return $this->db->query("INSERT INTO user_presets (user_id) "
                          . "VALUES ('$currentUser')");
       } else {
           return false;
       }
       
    }
    
    
}
