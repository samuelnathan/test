<?php

namespace OMIS\Database;


use OMIS\Auth\RoleCategory;
use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class StationExaminers extends AbstractEntityDAO
{

    /**
     * Return a list of examiners that are assigned to a given session_stations (i.e.
     * instance of a station in a specific session of a specific exam). If the
     * list is empty, then this is considered "open" and any examiner in the
     * appropriate department can elect to examine this form.
     *
     * @param int    $station_id ID number of station set
     * @param string $order_by      Table field name to sort results by
     * @return string[] List of examiner IDs allowed to examine station (if assigned)
     */
    public function getStationExaminers($station_id, $order_by = "scoring_weight")
    {
        $station_id = $this->db->clean($station_id);
        $sql = "SELECT DISTINCT examiner_id FROM station_examiners "
            . "WHERE station_id = '" . $station_id . "' ORDER BY " . $order_by;

        $mysqliResult = $this->db->query($sql);

        /* If there are no examiners assigned to this station, then return an
         * empty list. The calling code should explicitly know how to handle this.
         */
        if (!$mysqliResult || mysqli_num_rows($mysqliResult) == 0) {
            return [];
        }

        $resultArray = CoreDB::into_array($mysqliResult);
        $groupedArray = CoreDB::group($resultArray, ['examiner_id'], true);

        // We just require the examiner_id field from each record...
        return array_keys($groupedArray);
    }

    /**
     * Is the user assigned to session
     *
     * @param string $userID   Identifier for the user
     * @param int $sessionID   Identifier for the session
     * @return bool            User assigned to station in exam true|false
     */
    public function isUserAssignedSession($userID, $sessionID) {
        $userID = $this->db->clean($userID);
        $sessionID = $this->db->clean($sessionID);

        $sql = "SELECT COUNT(*) "
            . "FROM station_examiners "
            . "INNER JOIN session_stations USING(station_id) "
            . "WHERE session_id = $sessionID AND station_examiners.examiner_id = '$userID'";
        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    /**
     * Get assigned station for an examiner today
     *
     * @param string $userID   Identifier for the user
     * @return mixed[] station with session information
     */
    public function getActiveStationAssignment($userID)
    {

        $userIDSan = $this->db->clean($userID);

        $sql = "SELECT exams.dept_id, exams.term_id, departments.dept_name,  
        exams.exam_id, exams.exam_name, exam_sessions.session_id, 
        exam_sessions.session_description, exam_sessions.circuit_colour, 
        session_stations.station_id, session_stations.station_number, 
        forms.form_name, session_groups.group_id, session_groups.group_name 
        FROM ((((((station_examiners 
        INNER JOIN session_stations USING (station_id)) 
        INNER JOIN exam_sessions USING (session_id)) 
        INNER JOIN exams USING (exam_id)) 
        INNER JOIN session_groups USING (session_id)) 
        INNER JOIN departments USING (dept_id)) 
        INNER JOIN forms USING (form_id))
        INNER JOIN exam_settings USING (exam_id) 
        WHERE exam_published = 1 AND session_published = 1 
        AND auto_config_station = 1 AND examiner_id = '$userIDSan' 
        AND session_date = CURDATE() 
        GROUP BY station_id ORDER BY start_time ASC";

        $resultset = $this->db->query($sql);
        return (!$resultset ? [] : $this->db->into_array($resultset));

    }

    /**
     * Get the examiners associated with this station and group
     *
     * @param int $stationID   ID number for the station
     * @param int $groupID     ID number for the group
     * @return array[]         List of examiners
     */
    public function getStationAssociatedExaminers($stationID, $groupID)
    {
        $stationID = $this->db->clean($stationID);
        $groupID = $this->db->clean($groupID);

        $sql = "SELECT examiner_id, forename, surname "
            . "FROM station_examiners "
            . "INNER JOIN users ON station_examiners.examiner_id = users.user_id "
            . "WHERE station_id = '$stationID' "
            . "UNION DISTINCT "
            . "SELECT examiner_id as examiner_id, users.forename, users.surname "
            . "FROM student_results "
            . "INNER JOIN users ON student_results.examiner_id = users.user_id "
            . "INNER JOIN group_students USING (student_id) "
            . "WHERE station_id = '$stationID' AND group_id = '$groupID'";

        $mysqliResult = $this->db->query($sql);

        if (!$mysqliResult || mysqli_num_rows($mysqliResult) == 0) {
            return [];
        }

        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['examiner_id'], true, true);
    }

    /**
     * Get a list of all of the students for a given session and whether (and in
     * what way) they're available to a specific examiner. Should correctly
     * handle multiple scenarios too.
     *
     * @param int      $stationID       ID number of station
     * @param int      $studentGroupID  ID number of student group
     * @param string   $examinerID      ID number of examiner
     * @return mixed FALSE if no permission, otherwise an associative array.
     */
    public function getStationExaminerStudents($stationID, $studentGroupID, $examinerID)
    {
        /* Get details for the station. If that fails, then don't bother trying
         * to do anything else, as the station doesn't (seem to) exist.
         */
        $station = $this->db->stations->getStation($stationID);

        $examinerWeightings = $this->getExaminerWeightings($stationID, Exams::EXAMINER_WEIGHTING_GROUP_STATION);

        if (empty($station)) {
            error_log(__METHOD__ . "Failed to get details for station with ID $stationID");
            return false;
        }

        /* Get the specified user role and check that if they are school admin
         * or super admin who has access to all student records in assessment
         */
        $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_EXAM_ADMINS);
        $user = $this->db->users->getUser($examinerID, "user_id", true);

        /* If the current user is not allowed to examine this station, then that's that.
         * (Unless they're a super user, at least). No data for them. This should only
         * happen if examiners are specifically assigned to the station in question and
         * the current examiner isn't one of them. If not, then we have to assume that
         * anyone in the right department can do it.
         */
        if (!$adminRoles->containsRole($user["user_role"])) {

            $stationExaminers = $this->getStationExaminers($stationID);

            $requiredNumExaminers = $this->db->stations->getExaminersRequired(
                $stationID,
                Exams::INCLUDE_OBSERVER_RECORDS
            );

            $examinerCount = count($stationExaminers);

            if ($examinerCount > 0 && $requiredNumExaminers == $examinerCount && !in_array($examinerID, $stationExaminers)) {

                // @TODO Log this to the system error log.
                error_log(__METHOD__ . "Examiner $examinerID is not assigned to station $stationID");
                return false;

            }

        }

        // This determines if the exam uses multiple scenarios or not

        // fix this
        $multipleScenarios = false;

        // $multipleScenarios = $this->multiScenarioStation($stationID);


        /* Get a list of all stations that have the same station number as this one; in
         * other words, find out if there's more than one scenario in play.
         * getStationsByFilter() returns a list for each $session_id passed in (it
         * accepts arrays) so we need to grab only those results that are at index
         * $session_id.
         */

        if ($multipleScenarios) {

            $groupStations = $this->db->stations->getStationsByFilter(
                $station["session_id"],
                "station_number",
                $station["station_number"]
            );

            if (empty($groupStations)) {
                // @TODO Log this to the system error log.
                error_log(
                    __METHOD__ . ": Failed to find a matching scenario for station "
                    . $stationID . ". This shouldn't happen..."
                );
                return false;
            }

            /* The array is indexed by session ID, and we only have one session.
             * So, the first item contains all of the items we want.
             */
            $groupStations = array_shift($groupStations);
        } else {
            /* If there aren't multiple scenarios going on, then this check is
             * not needed, so we just pass along the existing station as our "group"
             */
            $groupStations = [$station];
        }

        /* Slice out the station_ids. If there's more than one, we need to re-order them
         * so that the station ID of the current station is at the end so that we check
         * all the other stations in the group first (if the student shows up in
         * any other station/scenario in the group, then they can't be in this one).
         */
        $stationIDs = array_column($groupStations, "station_id");

        if (count($stationIDs) > 1) {
            $stationIDs = array_diff($stationIDs, [$stationID]);
            $stationIDs[] = $stationID;
        }

        /* Get the two types of user that we need to worry about in terms of
         * what a specific single examiner can see:
         *
         * 1. Unused blank students
         * 2. Non-blank students.
         *
         * This works for single and multiple scenarios.
         */
        $students = $this->db->groups->getStudents($studentGroupID, null, true, $_SESSION["cterm"]);
        $unusedBlanks = $this->db->blankStudents->getUnusedStudentBlanks($station["station_number"], $studentGroupID);

        /* Here are the three different "statuses" a student in a group assigned
         * to a station can have:
         * 1. Unexamined: Basically, either they're an unused blank or they're a
         *    "regular" (i.e. actual) student with no result records against
         *    them for this station.
         * 2. Incomplete: If they're marked as currently being examined by the
         *    specified examiner-level user.
         * 3. Submitted: If they've been marked by the specified examiner-level
         *    user and that user has submitted that result as being "complete".
         *
         * The sum of these three arrays, when we're finished, will only show
         * what students are available to the current examiner - any students
         * who have already reached the required quota of results or any "used
         * blanks" aren't included and therefore this is not a "get all data for
         * a station" method.
         */
        $studentsUnexamined = [];
        $studentsIncomplete = [];
        $studentsSubmitted = [];

        /* Get all of the unused blank students and put them in the "available
         * for selection" pile. This is correct regardless of how many scenarios
         * there are.
         */
        while ($blankStudent = $this->db->fetch_row($unusedBlanks)) {
            $studentsUnexamined[] = [
                "student_id" => $blankStudent["blank_student_id"],
                "student_order" => $blankStudent["student_order"],
                "blank" => true,
                "forename" => "STUDENT",
                "surname" => "BLANK",
                "absent" => false
            ];
        }

        /* Get the numbers of results required for each station rather than
         * requesting them over and over again... should yield a slight
         * performance increase.
         */
        $resultsRequiredStations = [];
        foreach ($stationIDs as $id) {
            $resultsRequiredStations[$id] = $this->db->stations->getExaminersRequired(
                $id,
                Exams::INCLUDE_OBSERVER_RECORDS
            );
        }

        // Iterate through all of the non-blank students.
        foreach ($students as $student) {

            // Get the student id and position.
            $studentID = $student["student_id"];
            $order = $student["student_order"];

            // This is the student's data. Where it goes has yet to be determined.
            $studentRecord = [
                "student_id" => $studentID,
                "exam_number" => $student["student_exam_number"],
                "candidate_number" => $student["candidate_number"],
                "student_order" => $order,
                "student_image" => $student["student_image"],
                "blank" => false,
                "forename" => $student["forename"],
                "surname" => $student["surname"],
                "absent" => false,
                "updated_at" => null
            ];

            /* Iterate through all the stations. Any given student should only
             * have a record for one single scenario; if that's not the case,
             * this will fail HORRIBLY if the first record the loop comes across
             * exists but is not the "correct" one.
             */
            foreach ($stationIDs as $scenarioStationID) {

                // Find out how many results we need to have for this station.
                $resultsRequired = $resultsRequiredStations[$scenarioStationID];

                // Fix this .....
                //$resultsRequired++;

                // Get the list of all results for this student at this station.
                $results = $this->db->results->getResults($studentID, $scenarioStationID);

                /* If there are no results for this student at this station,
                 * then they're treated as being provisionally available for
                 * selection. If there are no results for any station, then
                 * we'll make them "officially" available. Otherwise, more
                 * granular decision making will take care of determining the
                 * status of this particular student.
                 */
                if (empty($results) || (mysqli_num_rows($results) == 0)) {
                    // Go to next scenario/station.
                    continue;
                }

                // So, once we get to here, there are results. Okay.
                $resultCount = mysqli_num_rows($results);


                // Get array version of the student results for this station
                $resultsArray = CoreDB::into_array($results);

                /* If we find that the current student has records in a station
                 * in this scenario/group other than this one, then it can't be
                 * available for selection in this station...
                 */
                if ($stationID != $scenarioStationID) {
                    continue 2;
                }

                /* On the other hand, if we have more than zero results, then we
                 * need to check if any are incomplete and that they're for this
                 * specific examiner...
                 */
                $examiners = [];

                /* Get a list of examiners and whether they've submitted a final
                 * result or not. Also determine whether the currently logged in
                 * examiner has marked the student as being absent.
                 */

                foreach ($resultsArray as $result) {
                    $examiners[$result["examiner_id"]] = ($result["is_complete"] == 1);

                    /* If the result currently being examined was created by the
                     * currently logged in user, and that user has marked the
                     * student as being absent, make a note of that so that we
                     * know to set that when presenting students for selection.
                     */
                    if (($result["examiner_id"] == $examinerID) && ($result["absent"] == 1)) {
                        $studentRecord["absent"] = true;
                    }

                    // Capture the updated_at timestamp only for the examiner
                    if ($result["examiner_id"] == $examinerID) {
                        $studentRecord["updated_at"] = $result["updated_at"];
                    }

                }

                /* Check if the current examiner has logged anything for this
                 * student at this station.
                 */
                if (in_array($examinerID, array_keys($examiners))) {
                    /* Current examiner has submitted something about this
                     * student for this station...
                     */
                    //if ($scenarioStationID == $stationID) {
                    $complete = $examiners[$examinerID];
                    if ($complete) {
                        // True = Result is final/submitted...
                        $studentsSubmitted[] = $studentRecord;
                    } else {
                        // False = Result is partial/incomplete
                        $studentsIncomplete[] = $studentRecord;
                    }
                    //}

                    // Go to next student.
                    continue 2;
                }

                /* If the current examiner hasn't been involved, then check if
                 * the required number of results have been at least started (so
                 * either complete or incomplete is fine). If so, ignore this
                 * record.
                 */

                if ($resultCount < $resultsRequired) {
                    continue;
                }

                /* If we get to here, there are at least as many results as we
                 * require. From here, we need to look at the exceptions to the
                 * result count limit.
                 */

                /* If there's no weighting recorded for this examiner, then
                 * do not allow this user to examine this student.
                 */
                if (!isset($examinerWeightings[$examinerID])) {
                    continue 2;
                }

                /* If we get here, there is a weighting value for this
                 * examiner. If it's non-zero, then they shouldn't be let
                 * examine this student. This allows assigned observers to
                 * examine students even when the required number of results have
                 * been entered.
                 */
                if ($examinerWeightings[$examinerID] > 0) {
                    continue 2;
                }
            }

            /* If we get to here, there are no results, and we've already
             * verified that this examiner is "allowed" to mark this station,
             * then we add this student as being available for selection.
             */
            $studentsUnexamined[] = $studentRecord;
        }

        // Put all of the results into a single array to be navigated.
        $compositeResults = [
            "unexamined" => $studentsUnexamined,
            "incomplete" => $studentsIncomplete,
            "submitted" => $studentsSubmitted
        ];

        return $compositeResults;
    }

    /**
     * Remove all examiners from the specified station.
     *
     * @param int $stationID ID number of station to remove examiner assignments from
     * @return true|false query success|failure
     */
    public function removeStationExaminers($stationID)
    {
        $stationID = $this->db->clean($stationID);
        $sql = "DELETE FROM station_examiners "
            . "WHERE station_id = '".$stationID."'";

        // Run query
        if ($this->db->query($sql)) {
            \Analog::info(
                $_SESSION['user_identifier'] . " removed all examiners "
                . " from station with ID: " . $stationID
            );
            return true;
        } else {
            return false;
        }
    }

    /**
     * Assign station examiner
     *
     * @param string $userID      ID number for examiner
     * @param int $stationID      ID number for station
     * @param int $weight         weight value
     * @return bool true|false    success|failure
     */
    public function assignStationExaminer($userID, $stationID, $weight)
    {
        $userID = $this->db->clean($userID);
        $stationID = $this->db->clean($stationID);
        $weight = $this->db->clean($weight);

        $sql = "INSERT INTO station_examiners (station_id, examiner_id, "
            . "scoring_weight) VALUES ('" . $stationID . "', '" . $userID
            . "', '" . $weight . "')";

        if ($this->db->query($sql)) {
            \Analog::info(
                $_SESSION['user_identifier'] . " attached examiner with ID: "
                . $userID . " to station with ID: " . $stationID
            );
            return \TRUE;
        } else {
            return \FALSE;
        }
    }

    /**
     * Return the list of examiners assigned to this station.
     *
     * @param int  $stationID ID number of station.
     * @param bool $asArray   If TRUE, return results as an associative array
     *
     * @return mysqliresult|mixed[] depends on $asArray
     */
    public function getExaminersForStation($stationID, $asArray = false)
    {
        $stationID = $this->db->clean($stationID);

        $sql = "SELECT * FROM station_examiners "
            . "INNER JOIN users ON station_examiners.examiner_id = users.user_id "
            . "INNER JOIN weighting USING(scoring_weight) "
            . "WHERE station_examiners.station_id = '$stationID' "
            . "ORDER BY scoring_weight DESC";

        $mysqliResult = $this->db->query($sql);
        if ($asArray) {
            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['examiner_id'], true, true);
        } else {
            return $mysqliResult;
        }
    }

    /**
     * Get exam examiner weightings
     * Return the list of examiners with their weightings for an exam.
     *
     * @param int|int[] $stationIds ID number(s) of stations to retrieve
     *                              examiner weightings for
     * @param int (optional) Grouping arrangement - see EXAMINER_WEIGHTING_GROUP_*
     *                       constants. Group by station or examiner.
     * @return mixed[] Associative array of examiner station weights, grouped by
     *                 examiner ID.
     */
    public function getExaminerWeightings($stationIds, $groupBy = Exams::EXAMINER_WEIGHTING_GROUP_STATION)
    {

        $returnResult = [];

        $sql = "SELECT station_examiners.* FROM station_examiners ";

        if (is_array($stationIds)) {

            $sql .= "WHERE station_id IN (" . implode(", ", $stationIds) . ") ";

        } else {

            $sql .= "WHERE station_id = $stationIds ";

        }

        $sql .= "GROUP BY station_id, examiner_id";

        $resultSet = $this->db->query($sql);

        // Compile examiner weighting return array
        while ($eachRecord = $this->db->fetch_row($resultSet)) {

            list($stationID, $examinerID, $scoringWeight) = array_values($eachRecord);

            // Set station and examiner keys in return array
            switch ($groupBy) {

                case Exams::EXAMINER_WEIGHTING_GROUP_EXAMINER:
                    // Group stations by examiner
                    setA($returnResult, $examinerID, []);
                    setA($returnResult[$examinerID], $stationID, $scoringWeight);
                    break;
                case Exams::EXAMINER_WEIGHTING_GROUP_STATION:
                    // Group examiners by station
                    setA($returnResult, $stationID, []);
                    setA($returnResult[$stationID], $examinerID, $scoringWeight);
                    break;
                default:
                    // Invalid key selected. Bail out.
                    error_log(__METHOD__ . ": Invalid group_by parameter '$groupBy'");
                    return [];

            }

        }

        /* Return a single set of results if a single station's data is requested
         * and the grouping scheme is by station.
         */
        if (is_array($stationIds) || ($groupBy != Exams::EXAMINER_WEIGHTING_GROUP_STATION)) {

            return $returnResult;

        } else {

            return array_shift($returnResult);

        }

    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds()
    {
        return ['station_id', 'examiner_id'];
    }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'station_examiners';
    }
}