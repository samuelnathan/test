<?php

namespace OMIS\Database;

use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class Sessions extends AbstractEntityDAO
{

    /**
     * Get Session Record
     */
    public function getSession($sessionID)
    {
        $sql = "SELECT * FROM (exam_sessions INNER JOIN exams USING(exam_id)) "
            . "INNER JOIN departments USING(dept_id) "
            . "WHERE session_id = '" . $this->db->clean($sessionID) . "'";

        return $this->db->fetch_row($this->db->query($sql));
    }

    //get station type query

    public function getStationType($id){
        $sql = "SELECT `station_type` FROM `session_stations` WHERE `station_id` = $id";
         return $this->db->fetch_row($this->db->query($sql));
    }

    /**
     * Insert Exam Session
     *
     * @param string $date - Session date value
     * @param string $start - Session start time
     * @param string $end - Session end time
     * @param int $exam - Exam ID
     * @param int $published - Session published (0|1)
     * @param string $description - Session description
     * @param string $colour - Session color
     * @return boolean true|false
     */
    public function insert($date, $start, $end, $exam, $published, $description, $colour = '')
    {
        
        $date = $this->db->clean($date);
        $exam = $this->db->clean($exam);
        $published = $this->db->clean($published);
        $description = $this->db->clean($description);
        $start = $this->db->clean($start);
        $end = $this->db->clean($end);
        $start = strtotime($start) ? "'$start'" : "null";
        $end = strtotime($end) ? "'$end'" : "null";

        // Circuit Colour
        $colour = ltrim($this->db->clean($colour), '#');

        $sql = "INSERT INTO exam_sessions (session_date, start_time, end_time, "
          . "exam_id, session_published, session_description, circuit_colour) "
          . "VALUES ('$date', $start, $end, '$exam', '$published', '$description', '$colour')";

        $examDetails = $this->db->exams->getExam($exam);

        if (!$this->db->query($sql)) {
            $log_message = "failed to insert session $description into exam "
                . $examDetails['exam_name'];
            \Analog::error($log_message);
            error_log(__METHOD__ . ": $log_message");
            return false;
        } else {
            \Analog::info(
                $_SESSION['user_identifier'] . ' added session: ' . $description
                . ' - ' . $date . ' in Exam: ' . $examDetails['exam_name']
            );
            return $this->db->inserted_id();
        }
    }


    /**
     * Update Exam Session
     *
     * @param int $id - Session ID
     * @param string $date - Session date value
     * @param string $start - Session start time
     * @param string $end - Session end time
     * @param int $published - Session published (0|1)
     * @param string $description - Session description
     * @param string $colour - Session color
     * @return boolean true|false
     */
    public function update($id, $date, $start, $end, $published, $description, $colour)
    {
    
        $id = $this->db->clean($id);
        $date = $this->db->clean($date);
        $start = $this->db->clean($start);
        $end = $this->db->clean($end);
        $published = $this->db->clean($published);
        $description = $this->db->clean($description);
        $colour = ltrim($this->db->clean($colour), '#');

        $startTimeSql = strtotime($start) ? "start_time = '" . $start . "', " : "";
        $endTimeSql = strtotime($end) ? "end_time = '" . $end . "', " : "";

        $examDetails = $this->db->exams->getExam(
            $this->getSessionExamID($id)
        );

        $sql = "UPDATE exam_sessions SET "
            . "session_date = '" . $date . "', "
            . $startTimeSql . $endTimeSql
            . "session_description = '" . $description . "', "
            . "session_published = '" . $published . "', "
            . "circuit_colour = '" . $colour . "' "
            . "WHERE session_id = '" . $id . "'";

        if ($this->db->query($sql)) {

            \Analog::info(
                $_SESSION['user_identifier'] . ' updated session: '
                . $description . ' - ' . $date . ' in Exam: '
                . $examDetails['exam_name']
            );
            return true;

        } else {

            $log_message = "failed to update session $description in "
                . "Exam " . $examDetails['exam_name'];
            \Analog::error($log_message);
            error_log(__METHOD__ . ": $log_message");
            return false;

        }
    }

    //Get Session ID by Session Station ID [Updated 27/01/2014]
    public function getSessionIDByStationID($station_id)
    {
        $sql = "SELECT session_id FROM session_stations WHERE station_id = '"
            . $this->db->clean($station_id) . "'";
        return CoreDB::single_result($this->db->query($sql));
    }

    //Does Session Exist [Updated 28/01/2014]
    public function doesSessionExist($session_id)
    {
        $sql = "SELECT COUNT(*) FROM exam_sessions WHERE session_id = '"
            . $this->db->clean($session_id) . "'";

        $count = CoreDB::single_result($this->db->query($sql));
        return ($count > 0);
    }

    // Delete Sessions [Updated 15/07/2014]
    public function deleteSessions($toDelete)
    {
        // Calendar Enabled?
        global $config;
        $calendarEnabled = $config->eventQueue['enabled'];
        $updatedExams = [];  // Exams that are affected by the session deletions

        $count = count($toDelete);
        $deleteCount = 0;
        $index = 0;

        while ($index < $count) {
            $sessionID = $toDelete[$index];
            if ($this->db->results->doSessionsHaveResult([$sessionID], 'num')) {
                // Sessions that cannot be deleted
                setA($_SESSION, 'sessions_with_results_warning', []);
                $_SESSION['sessions_with_results_warning'][] = $sessionID;
                continue;
            }

            $examID = $this->getSessionExamID($sessionID);
            $examDetails = $this->db->exams->getExam($examID);

            $sql = "DELETE FROM exam_sessions WHERE session_id = '" . $this->db->clean($sessionID) . "'";
            if ($this->db->query($sql)) {
                \Analog::info(
                    $_SESSION['user_identifier'] . ' removed a session from Exam '
                    . $examDetails['exam_name']
                );

                $deleteCount++;
                array_push($updatedExams, $examID);
            }

            $index++;
        }

        // If the calendar feature is disabled, just return the number of
        // deleted rows
        if (!$calendarEnabled) return $deleteCount;

        // Instantiate the event notifier
        $eventNotifier = new \OMIS\Events\Calendar\CalendarEventNotifier();

        // Update each of the exams that have been affected by the session
        // deletion
        foreach (array_unique($updatedExams) as $updatedExam) {
            $examTimeRange = $this->db->exams->getExamTimeRange($updatedExam);
            $exam = $this->db->exams->getExam($updatedExam);

            $eventNotifier->notifyExamUpdate(
                $exam,
                $examTimeRange['time_start_exam'],
                $examTimeRange['time_end_exam']);
        }

        return $deleteCount;
    }

    //Get Student Session By Exam [Updated 10/02/2014]
    public function getStudentSession($student, $exam)
    {
        $student = $this->db->clean($student);
        $exam = $this->db->clean($exam);
        $sql = "SELECT session_id, session_date, session_description FROM "
            . "(exam_sessions INNER JOIN session_stations USING(session_id)) "
            . "INNER JOIN student_results USING(station_id) "
            . "WHERE exam_sessions.exam_id ='" . $exam . "' "
            . "AND student_results.student_id = '" . $student . "' "
            . "GROUP BY exam_sessions.session_id";

        return $this->db->query($sql);
    }

    /**
     * Get Variables for last session inserted
     *
     * @param int $exam Exam identifier
     * @return array|false if query fails
     */
    public function getLastSessionInsertedVariables($exam)
    {
        
        $exam = $this->db->clean($exam);
        $sql = "SELECT session_published, session_date FROM exam_sessions " .
            "WHERE exam_id = '" . $exam . "' ORDER BY session_id DESC LIMIT 1";

        return $this->db->fetch_row($this->db->query($sql));

    }

    /**
     * Get the Exam ID that is associated with a specific Session
     *
     * @param int $session_id Session ID to search for
     * @return type
     */
    public function getSessionExamID($session_id)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT exam_sessions.exam_id FROM exam_sessions "
            . "WHERE exam_sessions.session_id = '" . $session_id . "' ";
        return CoreDB::single_result($this->db->query($sql));
    }

    //Number of occurrences of form in session [Updated 03/02/2014]
    public function sessionFormCount($form_id, $session_id)
    {
        $sql = "SELECT COUNT(*) FROM session_stations WHERE form_id = '"
            . $this->db->clean($form_id). "' AND session_id = '"
            . $this->db->clean($session_id) . "'";

        return CoreDB::single_result($this->db->query($sql));
    }

    //get Student Count In Session [Updated 27/01/2014]
    public function getStudentCountInSession($session_id)
    {
        $sql = "SELECT COUNT(student_id) FROM group_students INNER JOIN session_groups"
            . " USING(group_id) WHERE session_id = '" . $this->db->clean($session_id)
            . "'";
        return CoreDB::single_result($this->db->query($sql));
    }

    //Does Session have Stations [Updated 27/01/2014]
    public function doesSessionHaveStations($session_id, $return_count = false)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT COUNT(*) FROM session_stations WHERE session_id = '" . $session_id . "'";
        $cnt = CoreDB::single_result($this->db->query($sql));
        if ($return_count === true) {
            return $cnt;
        } else {
            return ($cnt > 0);
        }
    }

    //Does Session have Groups [Updated 27/01/2014]
    public function doesSessionHaveGroups($session_id)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT COUNT(*) FROM session_groups WHERE session_id = '" . $session_id . "'";
        $cnt = CoreDB::single_result($this->db->query($sql));
        return ($cnt > 0);
    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds() { return ['session_id']; }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName() { return 'exam_sessions'; }

}