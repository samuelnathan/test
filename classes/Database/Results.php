<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * @Database function calls relating to Exam Results & Student Feedback
 */

namespace OMIS\Database;

// (DW) constants for how stations are combined/grouped...
use Carbon\Carbon;
use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../analysis/analysis_definitions.php';

class Results extends AbstractEntityDAO
{

    /**
     * Get the results for a given station/student combination.
     *
     * @param  string                    $studentID     ID number of student
     * @param  int                       $stationID     ID number of station.
     * @param  mixed                     $examinerID    ID number of examiner (optional)
     * @param  boolean                   $convertToArray = false (optional)
     * @return array|mysqli_result|false FALSE if no matches, otherwise mysqli_result.
     */
    public function getResults($studentID, $stationID, $examinerID = null, $convertToArray = false)
    {
        $studentID = $this->db->clean($studentID);
        $stationID = $this->db->clean($stationID);

        $sql = "SELECT * FROM student_results "
             . "WHERE station_id = " . $stationID
             . " AND student_id = '$studentID'";

        if (!is_null($examinerID)) {
            $examinerID = $this->db->clean($examinerID);
            $sql .= " AND examiner_id = '$examinerID'";
        }

        $sql .= " ORDER BY examiner_id";

        $resultSet = $this->db->query($sql);

        if (!empty($resultSet) && $convertToArray) {
            return CoreDB::into_array($resultSet);
        } else {
            return $resultSet;
        }
    }

    /**
     * Get results by exam
     *
     * @param  string  $examID        ID number of examination
     * @param  string  $examinerID    ID number of examiner
     * @param  array  $stationIDs     ID numbers for stations
     * @return array|false            FALSE if no matches, otherwise array.
     */
    public function getAllResults($examID, $examinerID = null, $stationIDs = [])
    {
        $examID = $this->db->clean($examID);

        if ($examinerID != null) {
          $examinerSql = " AND examiner_id = '".$this->db->clean($examinerID)."'";
        } else {
          $examinerSql = "";
        }

        if (count($stationIDs) > 0) {
          $stationsSql = " AND station_id IN (".implode(', ', $stationIDs).")";
        } else {
          $stationsSql = "";
        }

        $resultsSql = "(SELECT * FROM student_results WHERE 1 "
                    . $examinerSql.$stationsSql. ")";

        $groupsSql = "(SELECT student_id, group_name "
                   . "FROM (session_groups "
                   . "INNER JOIN group_students USING (group_id)) "
                   . "INNER JOIN exam_sessions USING (session_id) "
                   . "WHERE exam_id = '".$examID."' "
                   . "GROUP BY student_id)";

        $query = "SELECT exam_results.*, students.forename, students.surname, groups.group_name "
               . "FROM "
               . "(((((SELECT results.examiner_id, results.student_id, results.result_id, results.station_id, "
               . "results.absent, results.score, session_stations.session_id, session_stations.form_id, "
               . "session_stations.pass_value, session_stations.station_number, exam_sessions.session_date, "
               . "exam_sessions.session_description, forms.form_name, forms.total_value "
               . "FROM (".$resultsSql." AS results "
               . "INNER JOIN session_stations USING(station_id)) "
               . "INNER JOIN exam_sessions USING(session_id) "
               . "INNER JOIN forms USING(form_id) "
               . "WHERE exam_id = '".$examID."') AS exam_results) "
               . "INNER JOIN ".$groupsSql." AS groups USING (student_id)) "
               . "INNER JOIN students USING (student_id))) "
               . "ORDER BY students.student_id";

        $resultset = $this->db->query($query);

        if (!empty($resultset)) {
            return CoreDB::into_array($resultset);
        } else {
            return false;
        }

    }


    /**
     * Get student result record by Result ID
     *
     * @param int $resultID   identifier number for a student result
     * @return results|null  returns results or null if none found
     */
    public function getResultByID($resultID) { return $this->getById($resultID); }

    /**
     * Get the ID number of a result for a given student / station / session /
     * examiner combination.
     *
     * @param  string   $studentID      The ID number of the student
     * @param  int      $sessionID      The ID number of the session
     * @param  int      $stationNum     The number of the station (i.e. position in station order)
     * @param  int      $stationID      The ID number of the station
     * @param  int      $examinerID     The examiner
     * @return int|null The ID number of the result if one exists, otherwise NULL.
     */
    public function getResultID($studentID, $sessionID, $stationNum, $stationID, $examinerID = null)
    {
        $studentID = $this->db->clean($studentID);
        $sessionID = $this->db->clean($sessionID);
        $stationNum = $this->db->clean($stationNum);

        //If to filter by specific $station_id (station set ID)
        if (!empty($stationID)) {
            $stationID = $this->db->clean($stationID);
            $stationFilterID = " AND station_id = '$stationID'";
        } else {
            $stationFilterID = "";
        }

        // If the examiner ID is specified, then filter by that.
        if (!empty($examinerID)) {
            $examinerID = $this->db->clean($examinerID);
            $examinerFilter = " AND examiner_id = '$examinerID'";
        } else {
            $examinerFilter = "";
        }

        // Complete Query
        $sql = "SELECT result_id FROM student_results "
             . "INNER JOIN session_stations USING(station_id) "
             . "WHERE student_id = '$studentID' "
             . "AND session_id = '$sessionID' "
             . "AND station_number = '$stationNum'"
             . $stationFilterID . $examinerFilter . " LIMIT 1";


        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Calculate the aggregate score for a station/student pair across all examiners
     *
     * @param  string $student ID number of student
     * @param  int    $station ID number of station
     * @return float sum score across all examiners
     */
    public function getAggregateScoreForStudentStation($student, $station)
    {

        $stationSan = $this->db->clean($station);

        return CoreDB::single_result(
            $this->db->query(
             "SELECT SUM(score) FROM student_results
              WHERE station_id = '$stationSan' AND student_id = '{$this->db->clean($student)}'
              AND examiner_id NOT IN (SELECT examiner_id FROM station_examiners
              WHERE station_id = '$stationSan' AND scoring_weight = 0)"
            )
        );

    }


    /**
     * Calculate the aggregate possible for a exam/student/station across all examiners
     *
     * @param  int    $exam ID number of exam
     * @param  string $student ID number of student
     * @param  int    $station ID number of station
     * @return float sum possible across all examiners (non observers)
     */
    public function getAggregatePossibleForStudentStation($exam, $student, $station)
    {

        $possibleData =
            $this->db->query(
            "SELECT student_results.examiner_id AS examiner, scoring_weight,
               forms.form_id AS form, total_value AS possible
               FROM student_results
                 INNER JOIN session_stations USING(station_id)
                 INNER JOIN forms USING(form_id)
                 LEFT JOIN station_examiners
                 ON (student_results.examiner_id = station_examiners.examiner_id
                 AND student_results.station_id = station_examiners.station_id)
                 WHERE student_results.station_id = '{$this->db->clean($station)}'
                 AND student_id = '{$this->db->clean($student)}'"
        );

        $form = null;
        $totalPossible = $nonOwnerPossible = 0;
        $selfAssess = false;

        while ($result = mysqli_fetch_assoc($possibleData)) {

           // Is observer
           if ($result['scoring_weight'] === "0") continue;

           // Form data
           if (is_null($form)) {

               $form = $result['form'];
               $selfAssess = $this->db->selfAssessments->scoresheetHasSelfAssessment($form);

               if ($selfAssess) $nonOwnerPossible = $this->db->selfAssessments->getNonOwnerPossible($form);

           }

           // Self assessment
           if ($selfAssess && $this->db->selfAssessments->getOwnerByStudentExam($student, $exam) != $result['examiner']) {

               $totalPossible += $nonOwnerPossible;

            } else {

               $totalPossible += $result['possible'];

            }

        }

        return $totalPossible;

    }


    /**
     * Calculate the mean score for a station/student pair across all examiners
     *
     * @param  string $student_id ID number of student
     * @param  int    $station_id ID number of station
     * @param  bool   $weighted   If TRUE, computes a weighted average, otherwise unweighted.
     * @return float Mean score across all examiners, weighted or unweighted as required.
     */
    public function getMeanScoreForStudentStation($student_id, $station_id, $weighted = \TRUE)
    {
        /* Define the names of fields in the table so that the code is easier to
         * adapt later if the DB changes.
         */
        if (!defined("EXAMINER_FIELD")) {
            define("EXAMINER_FIELD", "examiner_id");
        }
        if (!defined("SCORE_FIELD")) {
            define("SCORE_FIELD", "score");
        }

        /* Get the scores for the student at this station and shovel them into
         * an array.
         */
        $q = "SELECT " . EXAMINER_FIELD . ", score FROM student_results WHERE "
            . "station_id = $station_id AND student_id = '$student_id' ";

        $scores = CoreDB::into_array($this->db->query($q));

        /* If there's only one score, then there's no averaging to do. Return
         * that score as the average. This applies whether the average is to be
         * weighted or not.
         */
        if (count($scores) == 1) {
            return (float) $scores[0]['score'];
        }

        // If we get to here, then there's more than one score.
        if ($weighted) {
            $weights = $this->db->exams->getExaminerWeightings($station_id);
        } else {
            // Ascribe a default weighting to every score.
            $weights = [];
            foreach ($scores as $score) {
                $weights[$score[EXAMINER_FIELD]] = Exams::EXAMINER_WEIGHTING_DEFAULT;
            }
        }

        /* Compute each score's weighted contribution to the total. The sum of
         * these will constitute our weighted average.
         */
        $weight_total = 0;
        foreach (array_keys($scores) as $index) {
            $score = $scores[$index];

            /* This is to handle when an examiner who hasn't been assigned to
             * the station examines it, for example if an assigned examiner
             * cannot do it for whatever reason.
             */
            if (isset($weights[$score[EXAMINER_FIELD]])) {
                $weight = $weights[$score[EXAMINER_FIELD]];
            } else {
                /* The examiner we're expecting to find a weighting for isn't
                 * assigned to this exam. All we can do is give them a default
                 * weighting and carry on.
                 */
                error_log(__METHOD__ . ": No examiner weighting on file for examiner '"
                    . $score[EXAMINER_FIELD] . "' for station $station_id. Using default ("
                    . Exams::EXAMINER_WEIGHTING_DEFAULT . ")");
                $weight = Exams::EXAMINER_WEIGHTING_DEFAULT;
            }
            $score[SCORE_FIELD] = (float) $score[SCORE_FIELD] * $weight;
            // Write the weighted score back to the array.
            $scores[$index] = $score;

            // Add the selected weight to the total.
            $weight_total += $weight;
        }

        /* We have to wait until the very end of the process before we know what
         * actual weighting total is (particularly if the set of assigned
         * examiners isn't a dead ringer for the set of actual examiners who
         * examined a station).
         */
        return array_sum(array_column($scores, SCORE_FIELD)) / (float) $weight_total;
    }

    /*
     * Get Students With Results By Stations and Groups
     */
    public function getResultsByStationsGroups($stationIDs, $sessionGroups, $orderIDs = null,
            $orderField = "score", $orderType = "DESC", $termID = null)
    {

        // Stations Query
        $stationsSql = implode(", ", $stationIDs);

        // Groups Query
        $groupsSql = implode(", ", $sessionGroups);

        // Order By Clause
        $orderSql = (is_array($orderIDs)) ? "ORDER BY field(students.student_id, '"
            . implode("', '", $orderIDs) . "'), examiner_id ASC" : "ORDER BY " . $orderField . " " . $orderType;

        // Candidate clause and field
        $candidateSql = $candidateField = "";
        if (!is_null($termID)) {

          $candidateSql = "LEFT JOIN candidate_numbers "
                        . "ON (students.student_id = candidate_numbers.student_id "
                        . "AND candidate_numbers.term_id = '$termID') ";
          $candidateField = "candidate_number, ";

        }

        // Query
        $sql = "SELECT students.student_id, forename, surname, nationality, $candidateField"
            . "student_image, score, weighted_score, passed, student_results.created_at,  "
            . "station_id, result_id, is_complete, group_students.group_id, examiner_id, "
            . "group_students.student_order, student_exam_number "
            . "FROM students INNER JOIN student_results USING(student_id) "
            . "INNER JOIN group_students USING(student_id) "
            . $candidateSql
            . "WHERE station_id IN (" . $stationsSql . ") AND group_students.group_id "
            . "IN (" . $groupsSql . ") AND absent='0' " . $orderSql;

        return $this->db->query($sql);

    }

    /**
     * Get results by session
     *
     * @param string $student         Identifier for the student
     * @param int $session            Identifier for the session
     * @param bool $array             Return as array
     * @param string $indexColumn     Index to use (optional)
     * @param bool $noAbsents         Exclude absent results
     * @param bool $complete          Complete results only
     * @return mixed[]|mysqliResult
     */
    public function bySession($student, $session, $array = false, $indexColumn = null, $noAbsents = false, $complete = false)
    {

        $studentSan = $this->db->clean($student);
        $sessionSan = $this->db->clean($session);

        $noAbsentsSql = $noAbsents ? "AND absent = 0" : "";
        $completeSql = $complete ? "AND is_complete = 1" : "";

        $sql = "SELECT A.result_id, A.station_id, A.absent, A.examiner_id, A.score, A.scoring_weight, "
             . "session_stations.session_id, session_stations.form_id, session_stations.station_code, "
             . "session_stations.pass_value, session_stations.exclude_from_grading, "
             . "session_stations.station_number, forms.form_name, forms.total_value "
             . "FROM ((SELECT student_results.*, station_examiners.scoring_weight "
             . "FROM student_results "
             . "LEFT JOIN station_examiners "
             . "ON (student_results.examiner_id = station_examiners.examiner_id) "
             . "AND (student_results.station_id = station_examiners.station_id) "
             . "WHERE student_id = '$studentSan' $noAbsentsSql $completeSql "
             . "GROUP BY result_id) as A "
             . "INNER JOIN session_stations USING(station_id)) "
             . "INNER JOIN forms USING(form_id) "
             . "WHERE session_stations.session_id = '$sessionSan' "
             . "ORDER BY session_stations.station_number, A.examiner_id ASC";

        $mysqliResult = $this->db->query($sql);

        if ($array) {

            if ($indexColumn !== null) {

               $resultArray = CoreDB::into_array($mysqliResult);
               $returnData = CoreDB::group($resultArray, [$indexColumn], true);
               unset($resultArray);

            } else {

               $returnData = CoreDB::into_array($mysqliResult);

            }

        } else {

            $returnData = $mysqliResult;

        }

        return $returnData;

    }

    /**
     * Get student results by exam
     *
     * @param string $studentID    ID number for the student
     * @param int $examID          ID number for the exam
     * @param bool $complete       Only show completed (submitted) data (optional)
     * @return mysqli_result       Returns student results as mysqli_result object
     */
    public function getResultsByExam($studentID, $examID, $complete = true)
    {

        $student = $this->db->clean($studentID);
        $exam = $this->db->clean($examID);
        $completeClause = $complete ? " AND is_complete='1'" : "";

        $sql = "SELECT A.result_id, A.station_id, A.examiner_id, A.score, A.weighted_score, exam_id, "
             . "A.possible, A.weighted_possible, session_stations.session_id, "
             . "session_stations.form_id, session_stations.pass_value, weighting, "
             . "session_stations.station_number, session_stations.station_code, exam_sessions.session_id, "
             . "exam_sessions.session_date, exam_sessions.session_description, forms.form_name, "
             . "forms.total_value, COALESCE(forms.total_weighted_value, forms.total_value) AS total_weighted_value "
             . "FROM (((SELECT * FROM student_results WHERE "
             . "student_id = '$student' AND absent = '0' "
             . "$completeClause) as A "
             . "INNER JOIN session_stations USING(station_id)) "
             . "INNER JOIN exam_sessions USING(session_id)) "
             . "INNER JOIN forms USING(form_id) "
             . "WHERE exam_id = '$exam' "
             . "ORDER BY session_stations.station_number, A.examiner_id";

        return $this->db->query($sql);

    }

    /**
     * Get overall score for a station per exam or per session
     *
     * @param int $examID              ID number for an exam or session
     * @param int $formID              ID number for a form (optional)
     * @param string $stationFilter    Station filter (station_number, form_id, station_code etc)
     * @return int overall score
     */
    public function getOverallScore($examID, $formID = null, $stationFilter = 'station_number')
    {

        // Form ID and Session ID
        $formIDSan = $this->db->clean($formID);
        $examIDSan = $this->db->clean($examID);

        $formFilterSql = is_null($formID) ? "" :
        "WHERE session_stations.$stationFilter = '$formIDSan' ";

        /**
         * Weightings:
         * 1 -> Low weighting
         * 2 -> Regular weighting
         * 3 -> High weighting
         */

        // Sql query sub parts
        $sql = "SELECT sum(sub.score) as score, sum(sub.total_value) as possible "
             . "FROM (SELECT student_results.score, forms.total_value, station_examiners.scoring_weight ";

        $part1 = "LEFT JOIN station_examiners "
               . "ON (student_results.examiner_id = station_examiners.examiner_id) "
               . "AND (student_results.station_id = station_examiners.station_id) "
               . $formFilterSql;

        $part2 = "AND student_results.absent = '0') AS sub " .
                 "WHERE sub.scoring_weight IS NULL OR sub.scoring_weight IN (1,2,3)";

        // Complete Exam Query
        $sql .= "FROM ((((student_results " .
                "INNER JOIN session_stations USING(station_id)) " .
                "INNER JOIN forms USING(form_id)) " .
                "INNER JOIN exam_sessions USING(session_id)) " .
                "INNER JOIN exams USING(exam_id)) " .
                $part1 .
                "AND exams.exam_id = '$examIDSan' " .
                $part2;

        return $this->db->fetch_row($this->db->query($sql));

    }

    /* Get Student Exam Data
     *
     * @param string $student_id  Student number
     * @param int $session_id  Session identifier
     * @param string $index_specified  the index for the data toi return
     *
     * @return array  if successful; FALSE otherwise
     */
    public function getStudentResultsData($studentID, $sessionID, $index_specified = STATION_COMBINE_SCENARIO)
    {

        $studentID = CoreDB::clean($studentID);
        $sessionID = CoreDB::clean($sessionID);

        $sql = "SELECT student_id, station_id, examiner_id, station_number, form_id, "
             . "station_code, result_id, is_complete, student_results.created_at, absent, "
             . "score, weighted_score, passed, possible, weighted_possible, flag_count "
             . "FROM student_results "
             . "INNER JOIN session_stations USING (station_id) "
             . "WHERE session_id = '$sessionID' AND student_id = '$studentID' "
             . "ORDER BY examiner_id";

        switch ($index_specified) {
            case STATION_COMBINE_TAG:
                $index = "station_code";
                break;
            case STATION_COMBINE_DEFAULT:
                $index = "station_number";
                break;
            default:
                $index = "form_id";
        }

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, [$index], true);

    }

    /**
     * Get the maximum multiple result count for all exam stations
     *
     * @param  array  $sessions    (session IDs).
     * @param  string $combineType (SCENARIO, STATION, TAG)
     * @return array  if successful; FALSE otherwise
     */
    public function getMaxMultiResultCounts($sessions, $combineType)
    {
        $sessionList = implode(", ", $sessions);

        $sql = "SELECT exam_results." . $combineType . ", GREATEST(MAX(exam_results.student_result_count), 1) AS maximum "
            . "FROM (SELECT student_id, " . $combineType . ", COUNT(result_id) AS student_result_count "
            . "FROM (student_results RIGHT OUTER JOIN session_stations "
            . "USING(station_id)) INNER JOIN exam_sessions USING(session_id) "
            . "WHERE session_stations.session_id IN (" . $sessionList . ") "
            . " GROUP BY student_id, " . $combineType . ") AS exam_results "
            . "GROUP BY " . $combineType;

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);

        $returnData = [];
        foreach ($resultArray as $row) {
           $returnData[$row[$combineType]] = $row['maximum'];
        }

        return $returnData;
    }

    //Get all student exam results [Updated 11/02/2014]
    public function getAllStudentResults($student_id)
    {
        $student_id = $this->db->clean($student_id);

        $q = "SELECT exams.*, exam_sessions.* " .
                "FROM ((student_results " .
                "INNER JOIN session_stations USING(station_id)) " .
                "INNER JOIN exam_sessions USING(session_id)) " .
                "INNER JOIN exams USING(exam_id) " .
                "WHERE student_results.student_id = '" . $student_id . "' AND student_results.absent = '0' " .
                "GROUP BY exams.exam_id " .
                "ORDER BY exams.term_id DESC";

        return $this->db->query($q);
    }

    /**
     * Insert a student result
     *
     * @param  string    $examinerID        ID number of examiner
     * @param  string    $studentID         ID number of student
     * @param  int       $stationID         ID number of station
     * @param  int       $absent            1 = student absent, 0 = student present
     * @param  float     $totalResult       Score student achieved in the station
     * @param  int       $isComplete        1 = yes, 0 = no
     * @return int|false FALSE if the insert fails, otherwise returns the result_id of the new record.
     */
    public function insertResult($examinerID, $studentID, $stationID, $absent, $totalResult, $isComplete = 0)
    {
        $examinerID = $this->db->clean($examinerID);
        $studentID = $this->db->clean($studentID);
        $stationID = $this->db->clean($stationID);
        $absent = $this->db->clean($absent);
        $totalResult = $this->db->clean($totalResult);
        $isComplete = $this->db->clean($isComplete);

        $sql = "INSERT INTO student_results (station_id, student_id, examiner_id, absent, score, "
             . " is_complete) "
             . "VALUES ('$stationID', '$studentID', '$examinerID', '$absent', "
             . "'$totalResult', '$isComplete')";

        $result = $this->db->query($sql);

        if (!$result) {
            return false;
        }

        return $this->db->inserted_id();
    }
    public function newInsertResult(
            $stationID,
            $studentID,
            $examinerID,
            $attendance,
            $isComplete)
    {

        $sql = "INSERT INTO student_results(station_id, student_id,
                examiner_id, absent,
                is_complete)VALUES(?,?,?,?,?);";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param(
            'sssss',
            $stationID,
            $studentID,
            $examinerID,
            $attendance,
            $isComplete
        );
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($result);
        $stmt->fetch();
        return $this->db->inserted_id();
    }
    /**
     * Update a student result with new data.
     *
     * @param  int    $result_id        ID number of result record
     * @param  int    $absent           1 = Student absent, 0 = Student present.
     * @param  float  $score            Total score for station for this examiner
     * @param  string $examiner_id          ID of examiner
     * @param  int    $station_id       ID number of station.
     * @param  int    $is_complete      1 = is complete, 0 = incomplete
     * @param  int    $flagCount    flag count
     * @return bool   TRUE on success,  FALSE on failure.
     */
    public function updateResult($result_id, $absent = null, $score = null, $examiner_id = null, $station_id = null, $is_complete = null, $flag_count = null)
    {
        $parameters = get_defined_vars();

        $name_params = [];
        foreach ($parameters as $name => $parameter) {
            if (($name != "result_id") && ($parameter !== null)) {
                $parameter = $this->db->clean($parameter);
                $name_params[] = "$name = '$parameter'";
            }
        }

        $sql = "UPDATE student_results SET " . implode(", ", $name_params)
             . " WHERE result_id = '" . $result_id . "'";

        return ($this->db->query($sql) !== false);
    }
    function newUpdateResult(
            $attendance,
            $possibleScore,
            $possibleWeightedScore,
            $resultId,
            $isComplete)
    {
        $sql = "UPDATE student_results SET
            absent = ?,
            possible = ?,
            weighted_possible = ?,
            is_complete = ?
            WHERE result_id = ? ;";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param(
            'sssss',
            $attendance,
            $possibleScore,
            $possibleWeightedScore,
            $isComplete,
            $resultId
            );
        $stmt->execute();
    }
    function selfAssessmentCheck($resultId)
    {
        /* Check if this result is linked to a self assessment section
         *
         */
        $sql = "SELECT student_id, examiner_id FROM form_sections
        JOIN session_stations on form_sections.form_id = session_stations.form_id
        JOIN student_results on session_stations.station_id=student_results.station_id
        WHERE self_assessment = 1 AND result_id ='$resultId';";
        return $this->db->fetch_row($this->db->query($sql));
    }

/*
 * Update the possible score to reflect a self assessment section included
 */
    function setPossScoreSelfAssess($possScore, $possWeight, $resultId) {
        $sql = "UPDATE student_results SET
                possible= '$possScore',
                weighted_possible= '$possWeight'
                WHERE result_id= '$resultId' ;";
        return ($this->db->query($sql) !== false);
    }


    function insertCompleteResult(
            $examinerGrade,
            $weightedScore,
            $isComplete,
            $flagCount,
            $resultId)
    {

            $sql = "UPDATE student_results SET
                passed = ?,
                weighted_score = ?,
                is_complete = ?,
                flag_count = ?
                WHERE result_id = ?
                ;";
            $stmt = $this->db->prepare($sql);
            $stmt->bind_param(
                'sssss',
                $examinerGrade,
                $weightedScore,
                $isComplete,
                $flagCount,
                $resultId
            );
            $stmt->execute();

        }
    /**
     * Set absent and total result fields for specified result ID.
     *
     * @param  int  $result_id      ID number of result to make a
     * @param  int  $absent_setting Absent setting. 1 = absent, 0 = present.
     * @param  int  $total_result   The new overall score for this result ID.
     * @return bool TRUE on success, FALSE on failure.
     */
    public function setAbsentScoreValue($result_id, $absent_setting, $total_result)
    {
        $result_id = $this->db->clean($result_id);
        $absent_setting = $this->db->clean($absent_setting);
        $total_result = $this->db->clean($total_result);

        // Sanity check absent_setting
        if (!in_array((int) $absent_setting, [0, 1])) {
            error_log(
                __METHOD__ . ": Warning - attempting to set absent field "
                . "of result ID $result_id to unexpected value $absent_setting"
            );
        }

        if ($absent_setting == 1 && (((int) $total_result) > 0)) {
            error_log(
                __METHOD__ . ": Warning - attempting to set non-zero "
                . "total result $total_result for result $result_id which "
                . "is being marked absent"
            );
        }

        $q = "UPDATE student_results SET absent = '" . $absent_setting . "', "
            . "score = '" . $total_result . "', is_complete = '1' WHERE result_id = '"
            . $result_id . "' ";

        return $this->db->query($q);
    }

    /**
     * Delete the database records associated with a specific student's result
     * for a specific examiner at a specific station in a specific OSCE, as
     * specified by the $id.
     *
     * @param  int  $id   ID number of result record.
     * @param  bool $resultDataOnly If TRUE, only delete the individual item scores and feedback.
     *                           If FALSE, also delete the master result record.
     * @return bool TRUE if the delete was successful; FALSE otherwise
     */
    public function deleteResult($id, $resultDataOnly = true, $backup = false)
    {

        $idSan = $this->db->clean($id);

        // Backup result record before we delete
        $this->db->startTransaction();

        if ($backup) {

            $this->backupResultsData([
                'result' => $id
            ]);

        }

        $success = $this->db->query(
            "DELETE FROM item_scores WHERE result_id = '$idSan'"
        );

        $success &= $this->db->query(
            "DELETE FROM section_feedback WHERE result_id = '$idSan'"
        );

        $success &= $this->db->query(
            "DELETE FROM item_feedback WHERE result_id = '$idSan'"
        );

        /* If the last query was successful, and we don't want to delete just
         * the scores, then attempt to delete the "master" result record as well.
         */
        if ($success && !$resultDataOnly) {

            $success &= $this->db->query(
                "DELETE FROM student_results WHERE result_id = '$idSan'"
            );

        }

        // Commit or rollback depending on outcome
        if ($success) {

            $this->db->commit();

        } else {

            $this->db->rollback();

        }

        return $success;

    }

    /**
     * Backup deleted results records (+ Feedback) specified by $params[] array
     *
     * @param  int  $result       ID number of result record.
     * @param  int  $station      ID number of station record.
     * @param  int  $session      ID number of session record.
     * @param  int  $group        ID number of group record.
     * @param  string  $student   ID number of student record.
     * @return bool TRUE if the backup was successful; FALSE otherwise
     */
    public function backupResultsData($params)
    {

        $where = "";

        // Construct where clause from method params passed in
        foreach ($params as $name => $param) {

            if ($name == "group") {

                $where .= " AND student_id IN (SELECT
                student_id FROM group_students WHERE
                group_id = '{$this->db->clean($param)}')";

            } elseif ($name == "session") {

                $where .= " AND station_id
                IN (SELECT station_id
                FROM session_stations WHERE
                session_id = '{$this->db->clean($param)}')";

            // everything else
            } else {

                $where .= " AND {$name}_id =
                '{$this->db->clean($param)}'";

            }

        }

        /* Params not set, or we don't have a
           where clause, then get out of here */
        if (empty($where)) return;

        /* Run sql queries to backup results records
         * Feb 2019 added new weighted fields. Kelvin Nunn
         */

        $status = $this->db->query(
            "INSERT INTO student_results_deleted (result_id, station_id,
             student_id, examiner_id, absent, score, weighted_score,
             possible, weighted_possible, is_complete,
             flag_count, created_at, updated_at, deleted_by)
             SELECT result_id, station_id, student_id, examiner_id, absent,
             score, weighted_score, possible, weighted_possible,
             is_complete, flag_count, created_at, updated_at, '" .
             $_SESSION['user_identifier'] . "' AS deleted_by
             FROM student_results
             WHERE absent = '0' $where
             GROUP BY result_id"
        );

        $status &= $this->db->query(
            "INSERT INTO item_scores_deleted (score_id, result_id,
             item_id, option_id, option_value, option_weighted_value, created_at, updated_at)
             SELECT score_id, item_scores.result_id, item_id, option_id, option_value,
             option_weighted_value, item_scores.created_at, item_scores.updated_at
             FROM item_scores INNER JOIN student_results USING(result_id)
             WHERE absent = '0' $where
             GROUP BY score_id"
        );

        $status &= $this->db->query(
            "INSERT INTO section_feedback_deleted (feedback_id, section_id, result_id,
             feedback_text, created_at, updated_at)
             SELECT feedback_id, section_id, section_feedback.result_id, feedback_text,
             section_feedback.created_at, section_feedback.updated_at
             FROM section_feedback INNER JOIN student_results USING(result_id)
             WHERE absent = '0' $where
             GROUP BY feedback_id"
        );

        $status &= $this->db->query(
            "INSERT INTO item_feedback_deleted (item_feedback_id, result_id, item_id,
             feedback_text, created_at, updated_at)
             SELECT item_feedback_id, item_feedback.result_id, item_feedback.item_id, feedback_text,
             item_feedback.created_at, item_feedback.updated_at
             FROM item_feedback INNER JOIN student_results USING(result_id)
             WHERE absent = '0' $where
             GROUP BY item_feedback_id"
        );


        return $status;

    }

    /**
     * Delete Session Results by group and station
     *
     * @param int $group    ID of group
     * @param int $station  ID of station
     */
    public function deleteSessionResults($group, $station)
    {

        // Backup result record before we delete
        $this->db->startTransaction();
        $this->backupResultsData([
            'group' => $group,
            'station' => $station
        ]);

        $success = $this->db->query(
            "DELETE FROM student_results
             WHERE station_id = '{$this->db->clean($station)}'
             AND student_id IN (SELECT student_id
             FROM group_students WHERE
             group_id = '{$this->db->clean($group)}')"
        );

        // Commit or rollback depending on outcome
        if ($success) {

            $this->db->commit();

        } else {

            $this->db->rollback();

        }

        return $success;

    }

    /**
     * Delete student session results
     * @param int $session
     * @param string $student
     * @return boolean results deleted true|false
     */
    public function deleteStudentSessionResults($session, $student)
    {

        // Backup result record before we delete
        $this->db->startTransaction();
        $this->backupResultsData([
            'session' => $session,
            'student' => $student
        ]);

        $success = $this->db->query(
            "DELETE FROM student_results
             WHERE student_id = '{$this->db->clean($student)}'
             AND station_id IN (SELECT station_id
             FROM session_stations WHERE session_id = '{$this->db->clean($session)}')"
        );

        if ($success) {

           $this->db->commit();
           $exam = $this->db->sessions->getSessionExamID($session);
           $examRecord = $this->db->exams->getExam($exam);
           \Analog::info($_SESSION['user_identifier']
                         . " deleted all results for student: " . $student
                         . " in exam: " . $examRecord['exam_name'] . " (ID $exam)"
           );

        } else {

            $this->db->rollback();

        }

        return $success;

    }

    /**
     * Mark all possible student results as absent in session
     * @param int $session
     * @param string $student
     * @return boolean results deleted true|false
     */
    public function markStudentAbsentSession($session, $student)
    {

        // Examiner ID (current user)
        $examiner = $_SESSION['user_identifier'];

        // Success (default)
        $success = false;

        // Deleted results (if any)
        $deletedExistingResults = $this->db->results->deleteStudentSessionResults(
                $session,
                $student
        );

        // Failed to remove existing results [FAIL]
        if (!$deletedExistingResults) return false;

        // Mark stations absent for student
        $stations = $this->db->stations->getStations([$session]);
        while ($station = mysqli_fetch_assoc($stations)) {

           $success = true; // (mmmm..success status needs to be improved)

           $this->db->results->insertResult(
              $examiner, $student, $station['station_id'],
              true,
              0
           );

        }

        // Logging
        if ($success) {

           $exam = $this->db->sessions->getSessionExamID($session);
           $examRecord = $this->db->exams->getExam($exam);
           \Analog::info($_SESSION['user_identifier']
                       . " marked student: $student absent for all stations"
                       . " in exam: " . $examRecord['exam_name']
           );

        }

        return $success;

    }

    //Does Student Result Exist [Updated 26/01/2014]
    public function doesResultExist($result_id) { return $this->existsById($result_id); }

    /**
     * Count how many results are entered for a given combination of session_stations
     * and student.
     *
     * @param  string  $studentID              Student ID
     * @param  int     $sessionID              exam Session ID
     * @param  int     $stationNum             Index of session_stations (position in exam sequence)
     * @param  int     $stationID              ID of session_stations
     * @param  int     $examinerID             ID number of examiner
     * @param  boolean $excludeAbsentResults   TRUE = Don't count absent results.
     * @param  int     $complete   (null => all, 0 => incomplete, 1 => complete)
     * @return int     Number of results stored in the database.
     */
    public function doesStudentHaveResult($studentID, $sessionID = null, $stationNum = null, $stationID = null, $examinerID = null, $excludeAbsentResults = false, $complete = null)
    {
        $studentID = $this->db->clean($studentID);
        $sql = "SELECT COUNT(*) FROM student_results";

        if (!is_null($sessionID)) {
            $sql .= " INNER JOIN session_stations USING (station_id)";
        }

        $sql .= " WHERE ";

        $conditions = ["student_id = '$studentID'"];

        if (!is_null($sessionID)) {
            $conditions[] = "session_id = '$sessionID'";
        }

        if (!is_null($stationNum)) {
            $stationNum = $this->db->clean($stationNum);
            $conditions[] = "station_number = '$stationNum'";
        }

        if (!is_null($stationID)) {
            $stationID = $this->db->clean($stationID);
            $conditions[] = "station_id = '$stationID'";
        }

        if (!is_null($examinerID)) {
            $examinerID = $this->db->clean($examinerID);
            $conditions[] = "examiner_id = '$examinerID'";
        }

        if ($excludeAbsentResults) {
            $conditions[] = "absent = '0'";
        }

        if ($complete !== null) {
            $complete = $this->db->clean($complete);
            $conditions[] = "is_complete = $complete";
        }

        $sql .= implode(" AND ", $conditions);

        /* (DW) Changed this to return an int rather than a boolean. As PHP evaluates
         * any non-zero integer as TRUE, this won't change the existing behaviour while
         * at the same time allows us to have an idea as to how many results there are
         * (if we want/need that) rather than just whether there are some or not.
         */
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Does form have results
     * @param int $formID           ID number for form
     * @param array $groups         array of group ID numbers
     * @param array $sessions       array of session ID numbers
     * @param string $termID        academic term ID
     * @param string $departmentID  number identifier for the department
     * @return true|false
     */
    public function doesFormHaveResult($formID, $groups = null, $sessions = null, $termID = null, $departmentID = null)
    {
        $formID = $this->db->clean($formID);

        // No group or session filters ?
        if (is_null($groups) && is_null($sessions)) {

          // Are we checking for results by term and/or department?
          $and = !is_null($termID) ? " AND exams.term_id = '"
               . $this->db->clean($termID) . "' " : "";

          $and .= !is_null($departmentID) ? " AND exams.dept_id = '"
               . $this->db->clean($departmentID) . "' " : "";

          $inner = (!is_null($termID) || !is_null($departmentID)) ?
                   "INNER JOIN exam_sessions USING(session_id) "
                 . "INNER JOIN exams USING(exam_id) " : "";

          $sub = "SELECT station_id "
               . "FROM session_stations $inner"
               . "WHERE form_id = '$formID'$and";

          $sql = "SELECT COUNT(*) "
               . "FROM student_results WHERE station_id IN "
               . "(". $sub .")";

        } else {

          $groupSql = (is_null($groups)) ?  "1" : "session_groups.group_id IN ("
                    . implode(", ", $groups) . ")";

          $sessionSql = (is_null($sessions)) ? "1" : "session_stations.session_id IN ("
                      . implode(", ", $sessions) . ")";

          $sql = "SELECT COUNT(*) FROM (SELECT student_id FROM group_students "
               . "INNER JOIN session_groups USING(group_id) WHERE " . $groupSql
               . ") as A INNER JOIN (SELECT student_id FROM session_stations "
               . "INNER JOIN student_results USING(station_id) WHERE " . $sessionSql
               . " AND session_stations.form_id = '" . $formID
               . "' AND absent = '0') as B USING(student_id)";

        }

        return (CoreDB::single_result($this->db->query($sql)) > 0);
    }

    /**
     * Does session station have result
     */
    public function doesStationHaveResults($station_id, $returntype)
    {
        $station_id = $this->db->clean($station_id);

        $q = "SELECT COUNT(*) FROM student_results WHERE station_id = '" . $station_id . "'";
        $count = CoreDB::single_result($this->db->query($q));

        if ($returntype == 'num') {
            return (int) ($count > 0);
        } else {
            return ($count > 0);
        }
    }

    /**
     * Does Exam have results
     * @param int examID  identifier number for exam
     */
    public function doesExamHaveResults($examID)
    {
        $sql = "SELECT COUNT(*) FROM student_results WHERE station_id"
            . " IN (SELECT station_id FROM session_stations"
            . " INNER JOIN exam_sessions USING(session_id) "
            . " WHERE exam_id = '" . $this->db->clean($examID) . "')";
        $count = CoreDB::single_result($this->db->query($sql));

        return (int) ($count > 0);
    }

    /**
     *  Do sessions have results
     */
    public function doSessionsHaveResult($sessions, $returntype = 'bool')
    {
        $totalCount = 0;
        foreach ($sessions as $session_id) {
            $session_id = $this->db->clean($session_id);
            $q = "SELECT COUNT(*) FROM student_results WHERE station_id IN"
                . " (SELECT station_id FROM session_stations"
                . " WHERE session_id = '" . $session_id . "')";

            $totalCount = $totalCount + (int) CoreDB::single_result($this->db->query($q));
        }

        if ($returntype == 'bool') {
            return ($totalCount > 0);
        } else {
            return (int) ($totalCount > 0);
        }
    }

    /**
     * Recalculate Student Total
     * @param int resultID  identifier number for student results
     * @return bool         update status
     */
    public function recalcStudentResult($resultID)
    {
        $resultID = $this->db->clean($resultID);

        // Radio values
        $radio = "SELECT SUM(item_options.option_value) AS sum " .
                 "FROM item_options, item_scores, section_items " .
                 "WHERE item_scores.result_id = '$resultID' " .
                 "AND section_items.item_id = item_scores.item_id " .
                 "AND item_scores.option_id = item_options.option_id " .
                 "AND section_items.item_id = item_options.item_id " .
                 "AND type = 'radio'";

         // Slider and text input values
        $other = "SELECT SUM(item_scores.option_value) AS sum " .
                 "FROM item_scores, section_items " .
                 "WHERE item_scores.result_id = '$resultID' " .
                 "AND section_items.item_id = item_scores.item_id " .
                 "AND (type = 'slider' OR type = 'text')";


        $radioResult = $this->db->query($radio);
        $otherResult = $this->db->query($other);

        if ($radioResult && $otherResult) {

            $radioScore = CoreDB::single_result($radioResult);
            $otherScore = CoreDB::single_result($otherResult);

            $sql = "UPDATE student_results "
                . "SET score = '" . ($radioScore + $otherScore) . "' "
                 . "WHERE result_id = '$resultID'";

            return $this->db->query($sql);
        }

        return false;
    }

    /**
     * Get item scores
     *
     * @param int $itemID            Identifier number for item
     * @param int $resultID          Identifier number for result
     * @return mysqliresult|false
     */
    public function getItemScoreData($itemID, $resultID)
    {
        $itemIDSan = $this->db->clean($itemID);
        $resultIDSan = $this->db->clean($resultID);

        $sql = "SELECT * FROM item_options WHERE option_id = "
             . "(SELECT option_id "
             . "FROM item_scores "
             . "WHERE result_id = '$resultIDSan' AND item_id = '$itemIDSan')";

        return $this->db->query($sql);

    }

    /**
     * Get item score value
     *
     * @param int $itemID       Identifier number for item
     * @param int $resultID     Identifier number for result
     *
     * @return int score value|empty
     */
    public function getItemScore($itemID, $resultID)
    {

        $itemIDSan = $this->db->clean($itemID);
        $resultIDSan = $this->db->clean($resultID);

        $sql = "SELECT option_id, option_value "
             . "FROM item_scores WHERE item_id = '$itemIDSan' "
             . "AND result_id = '$resultIDSan'";

        return $this->db->fetch_row($this->db->query($sql));

    }

    /**
     * Get item scores by stations groups
     *
     * @param int[] $stations  List of stations identifiers
     * @param int[] $groups    List of groups identifiers
     *
     * @return matrix[] scores matrix
     */
    public function getItemScoresByStationsGroups($stations, $groups)
    {
        $returnMatrix = [];
        $studentsWithResults = $this->getResultsByStationsGroups($stations, $groups);

        while ($student = $this->db->fetch_row($studentsWithResults)) {

            $itemScores = $this->getItemScoresPerResult($student['result_id'], false, true);

            while ($itemScore = $this->db->fetch_row($itemScores)) {

                $returnMatrix[$itemScore['item_id']][$student['student_id']] = $itemScore['option_value'];

            }

        }

        return $returnMatrix;
    }

    /**
     * Get item scores per student results
     *
     * @param int $id
     * @param bool $includeNonScores
     * @param bool $excludeScales
     * @param bool $array
     *
     * @return mysqliresult|false
     *
     */
    public function getItemScoresPerResult($id, $nonScores = false, $noScales = false, $array = false)
    {

        $nonScoresSql = $nonScores ? "" : " AND section_items.type NOT IN ('radiotext', 'checkbox')";
        $excludeScalesSql = $noScales ? " AND section_items.grs = '0'" : "";
        $returnScaleSql = $noScales ? " " : ", grs ";
        $id = $this->db->clean($id);

        $sql = "SELECT section_items.item_id, " .
               "section_items.type, option_value, option_weighted_value " . $returnScaleSql .
               "FROM item_scores, section_items " .
               "WHERE result_id = '$id' AND " .
               "item_scores.item_id = section_items.item_id" .
               $nonScoresSql . $excludeScalesSql;

        $mysqliResult = $this->db->query($sql);

        if ($array) {

            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['item_id'], true, true);

        } else {

            return $mysqliResult;

        }
    }

    /*
     * Get item scores per form in exam (excludes observer scores!)
     *
     * @param int    $formID             identifier for the form
     * @param int[]  $sessions           identifiers for the sessions
     * @param int[]  $groups             identifiers for the groups
     * @return mysqli_result|FALSE if query failed
     */
    public function getItemScoresMatrix($formID, $sessions, $groups)
    {

        // Return Vales
        $matrix = [];

        // Groups
        $groupsClause = "group_students.group_id IN (" . implode(", ", $groups) . ")";

        // Sessions
        $sessionsClause = "session_stations.session_id IN (" . implode(", ", $sessions) . ")";

        $sql = "SELECT student_results.result_id, item_scores.option_value "
            . "FROM (((group_students INNER JOIN student_results USING (student_id)) "
            . "INNER JOIN session_stations USING (station_id)) "
            . "INNER JOIN item_scores USING (result_id)) "
            . "INNER JOIN section_items USING (item_id) "
            . "LEFT JOIN station_examiners "
            . "ON (student_results.examiner_id = station_examiners.examiner_id) "
            . "AND (student_results.station_id = station_examiners.station_id) "
            . "WHERE $groupsClause "
            . "AND $sessionsClause AND form_id = '{$this->db->clean($formID)}' "
            . "AND section_items.type NOT IN ('radiotext', 'checkbox') "
            . "AND is_complete=1 "
            . "AND (scoring_weight IS NULL OR scoring_weight <> '0') "
            . "GROUP BY student_results.result_id, score_id";

        $scores = $this->db->query($sql);

        // Loop through all values and place them in a return matrix array
        $resultTracking = null;
        $count1 = -1;

        while ($score = mysqli_fetch_assoc($scores)) {

            // Counts Change
            if ($score['result_id'] != $resultTracking) {

                $resultTracking = $score['result_id'];
                $count1++;
                $count2 = 0;

            }

            $matrix[$count2][$count1] = $score['option_value'];
            $count2++;

        }

        return $matrix;

    }


    /**
     * Get item scores by student and session
     * @param string $studentID     Identifier number for student
     * @param int $sessionID        Identifier number for session
     * @param int[] $formIds        Identifier numbers for forms
     * @return mixed[] $data         Outcome data to set
     */
    public function getItemScoresByStudentSession($studentID, $sessionID, array $formIDs)
    {

        $formsParam = implode(",", $formIDs);

        $sql = "SELECT item_scores.* FROM item_scores "
             . "INNER JOIN student_results USING (result_id) "
             . "INNER JOIN session_stations USING (station_id) "
             . "INNER JOIN section_items USING (item_id) "
             . "WHERE student_id = ? AND session_id = ? AND form_id IN (?) "
             . "AND section_items.type NOT IN ('radiotext', 'checkbox') "
             . "AND section_items.grs = '0' "
             . "GROUP BY item_id, result_id";

        $stmt = $this->db->prepare($sql);
        $stmt->bind_param(
            'sis', $studentID, $sessionID, $formsParam
        );

        $stmt->execute();
        $mysqliResults = $stmt->get_result();

        $data = CoreDB::group(
            CoreDB::into_array($mysqliResults),
            ['item_id'], true
        );

        $mysqliResults->close();
        $stmt->close();

        return $data;

    }

   /**
     * Get form section item marks.
     *
     * @param int   $result     identifier for student form result
     * @param int   $section    identifier for form section
     * @return mysqli_result|FALSE if query failed
     */
    public function getSectionItemMarks($result, $section)
    {

        $sql = "SELECT section_items.item_id, section_items.weighted_value, item_options.descriptor, item_scores.option_value, "
             . "GREATEST(section_items.highest_value, section_items.lowest_value) AS item_possible, "
             . "section_items.type, section_items.grs "
             . "FROM (item_scores INNER JOIN item_options "
             . "USING (option_id)) INNER JOIN section_items "
             . "ON (item_options.item_id = section_items.item_id) "
             . "WHERE item_scores.result_id = '{$this->db->clean($result)}' "
             . "AND section_items.section_id = '{$this->db->clean($section)}' "
             . "GROUP BY section_items.item_id";

        return $this->db->query($sql);

    }

    /**
     * Insert an item mark into the database for a result.
     *
     * @param int   $result_id     Result ID
     * @param int   $item_id       Item ID
     * @param int   $option_id ID number of answer option
     * @param mixed $value         Value to insert into the database.
     * @return bool TRUE if inserted successfully; FALSE otherwise.
     */
    public function insertItemMark($result_id, $item_id, $option_id, $value)
    {
        $args = [];
        foreach (func_get_args() as $argument) {
            $args[] = sprintf("'%s'", $this->db->clean($argument));
        }

        $q = "INSERT INTO item_scores (result_id, item_id, option_id, option_value) "
            . "VALUES (" . implode(",", $args) . ");";

        return $this->db->query($q);
    }

   /**
     * Get Global Rating Scale Data
     *
     * @param  int     $exam_id    ID of exam
     * @param  int     $examiner_id     ID of examiner
     * @return array global rating scale data. Empty array if none.
     */
    public function getGlobalRatingScaleData($examID, $examinerID = NULL)
    {

        $examIDSan = $this->db->clean($examID);
        $examinerClause = "";
        if ($examinerID !== NULL) {

          $examinerClause = " AND student_results.examiner_id = '" . $this->db->clean($examinerID) . "' ";

        }

        $sql = "SELECT result_id, grs_ref_id, borderline, fail, descriptor, item_scores.option_value "
             . "FROM (((exam_sessions INNER JOIN session_stations USING (session_id)) "
             . "INNER JOIN student_results USING (station_id)) "
             . "INNER JOIN item_scores USING (result_id) "
             . "INNER JOIN section_items USING (item_id)) "
             . "INNER JOIN item_options USING (option_id) "
             . "WHERE exam_id = '$examIDSan'$examinerClause AND section_items.grs = 1 "
             . "GROUP BY result_id ";

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['result_id'], true, true);

    }

    /**
     * Determine if the student passed or failed the item within the limits set by
     * the "grace interval" set in the configuration.
     *
     * @param float     $awarded        Marks awarded for the item.
     * @param float     $pass_pc        The percentage specified for this item
     *                                  as being a "pass"
     * @param float     $maximum        The maximum marks possible for the item.
     *                                  Default is 100 to allow percentages to
     *                                  be easily processed.
     * @param int       $decimal_places How many places to round when doing pass
     *                                  /fail comparison
     * @param float     $grace_pc       How far below the actual pass grade a
     *                                  mark can be before it's deemed a "fail".
     * @return boolean TRUE if the student passed, FALSE if they fail.
     * @throws InvalidArgumentException
     */
    public static function determinePassOrFail($awarded, $pass_pc, $maximum = 100, $decimal_places = 1, $grace_pc = null)
    {
        global $config;

        /* If $grace_pc is not set, it pulls the default value from the
         * configuration and uses that.
         */
        if (empty($grace_pc)) {
            $grace_pc = (float) $config->station_defaults['grace_percent'];
        }

        // If the constant 'FUDGE_FACTOR' is not already defined then define it
        if (!defined('FUDGE_FACTOR')) {
          define('FUDGE_FACTOR', 0.00001);
        }

        // Some sanity checking.
        // Checking that the awarded marks make sense versus the possible max.
        // This approach means that there's a "fudge factor" to allow for the
        // possibility that there have been issues expressing the figure as an
        // IEEE-754 floating point number. Rather than an "x > y" test we're
        // using "x - y > e" test which should be more forgivng as the precision
        // of e is far fewer decimal places.
        if (($awarded - $maximum) > FUDGE_FACTOR) {
            $error_message = sprintf("Awarded marks %.2f is greater than "
                . "specified maximum of %f", $awarded, $maximum);
            //throw new \InvalidArgumentException($error_message);
            error_log($error_message);
            return true;
        }

        // Make sure that the number of decimal places is sane.
        if (!is_int($decimal_places) || $decimal_places < 0) {
            $error_message = "Decimal places value of $decimal_places is invalid"
                . " / impossible";
            //throw new \InvalidArgumentException($error_message);
            error_log($error_message);
        }

        // Make sure that the grace percentage is positive or zero.
        if (!is_numeric($grace_pc) || ($grace_pc < 0)) {
            $error_message = "Grace percentage value of $grace_pc is invalid "
                . "/ impossible";
            //throw new \InvalidArgumentException($error_message);
            error_log($error_message);
        }

        /* Subtract the grace percentage from the pass percentage to get the
         * actual grace percentage.
         */
        $pass_pc -= $grace_pc;

        /* Calculate the score, rounded to the appropriate number of decimal
         * places.
         */
        $awarded_pc = @((float) $awarded * 100 / $maximum);

        /* Round number up to the appropriate number of decimal places. Can't
         * use builtin round() function here as we want to round up no matter
         * what, e.g. rounding 3.21 to one decimal place to result in 3.3, not
         * 3.2.
         */
        $scaling = pow(10, $decimal_places);
        $awarded_pc_rounded = round($awarded_pc * $scaling) / $scaling;

        // Make the "big decision"...
        return ($awarded_pc_rounded - $pass_pc) > (0 - FUDGE_FACTOR);
    }

    /**
     * Determine final score and possible either weighted or aggregated
     *
     * @param bool  $weighted       Weighted rule (yes/no)
     * @param mixed[] $scores       List of score values and their weightings
     * @param int[] $possibles      List of possible values
     *
     * @return int[] returns final station score and possible calculated
     * @throws InvalidArgumentException
     */
    public static function determineScoreAndPossible($weighted, $scores, $possibles = [])
    {

        $finalScore = $finalPossible = 0;

        /**
        * (#Rule 1) WEIGHT (AVG) the multiple scores
        */
        if ($weighted) {

            // Only if # of form possibles are > 0
            if (count($possibles) > 0) {

                $finalPossible = max($possibles);

            }

            $weightingTotal = array_sum(array_column($scores, 'weighting'));

            // Only include weighted scores
            if ($weightingTotal > 0) {

                $finalScore = (array_sum(array_map("array_product", $scores)) / $weightingTotal);

            }

        /**
        * (#Rule 2) AGGREGATE the multiple scores
        */
        } else {

            // Only if # of form possibles are > 0
            if (count($possibles) > 0) {

                $finalPossible = array_sum($possibles);

            }

            $finalScore = array_reduce($scores, function ($carry, $value) {

                return ($carry + ($value['weighting'] > 0 ? $value['score'] : 0));

            });

        }

        return [$finalScore, $finalPossible];

    }

    /**
     * Do the values exceed the divergence threshold
     *
     * @int[] $values  array of values
     * @int $values    threshold value
     * @param bool true|false
     */
    public static function exceedsDivergenceThreshold($values, $threshold)
    {

        if (empty($values) || count($values) == 1 || strlen($threshold) == 0) {
            return false;
        }

        return ((max($values) - min($values)) >= $threshold);

    }

    /**
     * Rewire exam scores from 1 form to another
     *
     * @int $id               ID number of form
     * @int $examID           ID number of exam
     * @int[] $itemIDMap      Item ID mapping (replace old IDs with new ones)
     * @int[] $optionIDMap    Option ID mapping (replace old IDs with new ones)
     * @param bool true|false
     */
    public function switchFormScores($id, $examID, $itemIDMap, $optionIDMap)
    {

        $success = true;

        /*
         * Need to get result IDs first not to upset triggers
         * when updating same table
         */
        $resultIDs = CoreDB::into_values(
            $this->db->query(
               "SELECT DISTINCT result_id
                FROM student_results
                INNER JOIN session_stations USING (station_id)
                INNER JOIN exam_sessions USING (session_id)
                WHERE exam_id = '" . $this->db->clean($examID) . "'
                AND form_id = '" . $this->db->clean($id) . "'"
            )
        );

        if (empty($itemIDMap) || empty($optionIDMap)) {

            return false;

        } elseif (empty($resultIDs)) {

            return true;

        }

        // Items
        foreach ($itemIDMap as $oldItemID => $newItemID) {

            $success &= $this->db->query(
                "UPDATE item_scores
                 SET item_id = '" . $this->db->clean($newItemID) . "'
                 WHERE result_id IN (" . implode(",", $resultIDs) . ")
                 AND item_id  = '" . $this->db->clean($oldItemID) . "'"
            );

        }

        // Options
        foreach ($optionIDMap as $oldOptionID => $newOptionID) {

            $success &= $this->db->query(
                "UPDATE item_scores
                 SET option_id = '" . $this->db->clean($newOptionID) . "'
                 WHERE result_id IN (" . implode(",", $resultIDs) . ")
                 AND option_id  = '" . $this->db->clean($oldOptionID) . "'"
            );

        }

        return $success;

    }

    /**
     * @param Carbon $dateFrom
     * @param Carbon $dateTo
     * @return int
     */
    public function getCompleteResultsCount(Carbon $dateFrom = null, Carbon $dateTo = null)
    {

        if ($dateFrom === null)
            $dateFrom = Carbon::minValue();
        if ($dateTo === null)
            $dateTo = Carbon::maxValue();

        $dateFrom->hour(0)->minute(0)->second(0);
        $dateTo->hour(23)->minute(59)->second(59);

        $from = $this->db->toSqlDate($dateFrom);
        $to = $this->db->toSqlDate($dateTo);

        $sql = 'SELECT SUM(live.record_count + deleted.record_count) AS result FROM
                 (SELECT COUNT(*) AS record_count FROM student_results
                    WHERE is_complete = 1 AND created_at BETWEEN ? AND ?) AS live,
                 (SELECT COUNT(*) AS record_count FROM student_results_deleted
                    WHERE is_complete = 1 AND created_at BETWEEN ? AND ?) AS deleted';

        $stmt = $this->db->prepare($sql);

        $stmt->bind_param('ssss', $from, $to, $from, $to);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();

        $stmt->close();

        return (int)$row['result'];

    }

    public function isAbsent($studentID, $stationID) {

        $stationIdSan = $this->db->clean($stationID);
        $studentIdSan = $this->db->clean($studentID);

        $query = "SELECT absent "
            ."FROM student_results "
            . "WHERE station_id = $stationIdSan AND student_id = '$studentIdSan'";

        $result = CoreDB::single_result($this->db->query($query));

        if ($result === "1") return true;
        else if ($result === "0") return false;
        else return null;

    }

    /**
     * Does student result exist
     * @param int $station_id
     * @param string $student_id
     * @param string $examiner_id
     * @return bool true|false
     */
    public function existsByStationStudentAndExaminer($station_id, $student_id, $examiner_id)
    {
        $sql = "SELECT COUNT(*) FROM student_results WHERE examiner_id = '"
            . $this->db->clean($examiner_id) . "' AND student_id = '"
            . $this->db->clean($student_id) . "' AND station_id = '"
            . $this->db->clean($station_id) . "'";

        $count = CoreDB::single_result($this->db->query($sql));
        return ($count > 0);
    }
    /*
     * Get the sum of item weighted scores for the station
     * @param int $resultId
     */
    function getStationWeightedItemScores($resultId)
    {
        $sql= "SELECT SUM(item_scores.option_weighted_value) AS sum
                 FROM item_options, item_scores, section_items
                 WHERE item_scores.result_id = ?
                 AND section_items.item_id = item_scores.item_id
                 AND item_scores.option_id = item_options.option_id
                 AND section_items.item_id = item_options.item_id
                 AND section_items.type = 'radio' || 'text' || 'slider'
                 ;";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $resultId);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($result);
        $stmt->fetch();
        return $result;
    }
    /*
     * get the possible weighted score for the station items only
     * @param int $formId
     */
    function getTotalPossibleWeightedScore($formId)
    {
        $sql= "SELECT SUM(section_items.highest_value*section_items.weighted_value) AS sum
                 FROM section_items
                 JOIN form_sections ON section_items.section_id = form_sections.section_id
                 WHERE form_sections.form_id = ?
                 AND section_items.type NOT IN ('radiotext', 'checkbox');
                 ";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param('i', $formId);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($result);
        $stmt->fetch();
        return $result;

    }
    /**
     * Insert weighted score for items on update of item scores
     * @param string $itemId   Item identifier
     * @param string $resultId     Result identifier
     * @param $optionValue     Item score to be weighted
     * @return bool status
     */
    function setItemWeightedScore($itemId, $optionValue, $resultId)
    {
        $sql = "Update item_scores set option_weighted_value =
            ((select weighted_value from section_items where item_id = ?)*(?))WHERE result_id = ? and item_id = ?;";

        $stmt = $this->db->prepare($sql);

        $stmt->bind_param('ssss', $itemId, $optionValue, $resultId, $itemId);
        $stmt->execute();


    }

    public function sectionHasWeightedItems($section_id)
    {
        $section_id = $this->db->clean($section_id);

        $sql = 'SELECT IF(COUNT(*) > 0, 1, 0) AS has_weighted_items '
             . 'FROM section_items '
             . "WHERE section_id = $section_id AND weighted_value <> 1.00";

        $result = CoreDB::single_result($this->db->query($sql));

        return $result === '1';
    }

    protected function getTableName() { return 'student_results'; }

    protected function getIds() { return ['result_id']; }

}
