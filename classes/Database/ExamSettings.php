<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to exam settings
 */

namespace OMIS\Database;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class ExamSettings
{
    //Core Database 'coreDB' reference
    private $db;

    //Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get exam settings
     *
     * @param int  $examID   ID number of exam
     * @return array|false   array of settings. If query fails then bool false is returned.
     */
    public function get($examID)
    {
        $examID = $this->db->clean($examID);

        $sql = "SELECT * FROM exam_settings "
             . "WHERE exam_id = '$examID'";

        $resultset = $this->db->query($sql);
        
        if (!$resultset) {
            return false;
        } else {
            return $this->db->fetch_row($resultset);
        }
    }
    

    /**
     * Update exam settings
     *
     * @param int  $examID - ID number of exam
     * @param array $settings - Array of settings, field names and values
     * @return true|false If query fails then bool false is returned.
     */
    public function update($examID, $settings)
    {
        $settingsString = "";

        // Loop through settings and build query (Could use implode() or something similar here)
        foreach ($settings as $setting) {
            $field = $this->db->clean($setting['field']);
            $value = $this->db->clean($setting['value']);
            $settingsString .= "$field = '$value', ";
        }

        $settingsSql = substr($settingsString, 0, -2);

        $sql = "UPDATE exam_settings SET " . $settingsSql . " " .
               "WHERE exam_id = '" . $this->db->clean($examID) ."'";
        return $this->db->query($sql);
    }
    
    /**
     * Set the default number of examiners for each station
     * 
     * @param int $examID       ID number for the exam
     * @param int $number       number of examiners as default
     * @return bool success     true|false (yes|no)
     */
    public function setDefaultNumExaminersStation($examID, $number)
    {
        $number = ((1 <= $number) && ($number <= 30)) ? $number : 1;
        $stations = $this->db->exams->getStationsByExam($examID);

        $sql = "";

        // Loop through all stations
        foreach ($stations as $stationID => $station) {

            // The current required number of examiners at this station
            $currentExaminersRequired = $station['examiners_required'];
            
            // Get station examiners
            $examinersAssignedtoStation = $this->db->exams->getStationExaminers($stationID);
            $numOfExaminersAssigned = count($examinersAssignedtoStation);
            
            // Only apply this setting to stations that have not been set multiple examiners
            if ($currentExaminersRequired != $number && $number > $numOfExaminersAssigned) {
                $sql .= "UPDATE session_stations SET examiners_required = '$number' " .
                        "WHERE station_id = '$stationID'; ";
            }
        }
        
        return $this->db->multi_query($sql);
    }
    
    /**
     * Set Sessions Published yes/no
     * @param int[] $sessions  List of session identifiers
     * @param bool $published  Published, true|false
     * @return int
     */
    public function setSessionsPublished($sessions, $published)
    {
        $updateCount = 0;
        foreach ($sessions as $sessionID) {
            $sql = "UPDATE exam_sessions SET session_published = '$published' "
                 . "WHERE session_id = '$sessionID'";

            if ($this->db->query($sql)) {
                $updateCount++;
            }
        }
        return $updateCount;
    }
    
    /**
     * Set Stations Notes Auto Pop Up Yes/No
     * @param int[] $stations   list of station identifiers
     * @param bool  $autoPopup  station notes auto pop up
     * @return int
     */
    public function setStationsNotesAutoPopup($stations, $autoPopup)
    {
        $updateCount = 0;
        foreach ($stations as $stationID) {
            $sql = "UPDATE session_stations SET desc_auto_popup = " . (int) $autoPopup
                 . " WHERE station_id = '$stationID'";
            if ($this->db->query($sql)) {
                $updateCount++;
            }
        }
        return $updateCount;
    }
    
}
