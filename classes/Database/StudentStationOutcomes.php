<?php

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class StudentStationOutcomes extends AbstractEntityDAO
 {

     protected function getTableName() { return 'student_results_outcomes'; }
     protected function getIds() { return ['outcome_id']; }

     /**
      * Get outcome data by student and station id
      * @param string $student Identifier for student
      * @param int $session Identifier for session
      * @param $station_number int
      * @return mixed[]
      */
     public function byStudentAndStation($student, $session, $station_number)
     {

         $scoresResult = [];

         // Query to get the final scores for the station
         $query = 'SELECT score, adjusted_score, possible, adjusted_possible, passed, adjusted_passed, pass_possible '
                . 'FROM student_results_outcomes '
                . 'WHERE student_id = ? AND session_id = ? AND combined_by = ?';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('sii', $student, $session, $station_number);
         $stmt->execute();

         $result = $stmt->get_result();
         $row = $result->fetch_assoc();

         if ($row === null)
             return null;

         $scoresResult['raw'] = [
             'possible' => (float)$row['possible'],
             'score' => (float)$row['score']
         ];

         $scoresResult['weighted'] = [
             'possible' => (float)$row['adjusted_possible'],
             'score' => (float)$row['adjusted_score']
         ];

         $scoresResult['passed'] = $this->passedToBool($row['passed']);
         $scoresResult['passPossible'] = (float)$row['pass_possible'];

         $result->close();
         $stmt->close();

         // Query to get the weighted breakdown by item
         $query = 'SELECT student_results_outcomes.combined_by, form_sections.section_id, section_items.item_id, '
                . '       student_item_outcomes.score, student_item_outcomes.weighted_score, student_results.examiner_id, '
                . '       item_scores.option_value, item_scores.option_weighted_value '

                . 'FROM student_results_outcomes JOIN '
                . '     session_stations JOIN '
                . '     form_sections JOIN '
                . '     section_items JOIN '
                . '     student_item_outcomes JOIN '
                . '     item_scores JOIN '
                . '     student_results '

                . 'ON student_results_outcomes.session_id = session_stations.session_id AND '
                . '   student_results_outcomes.session_id = student_item_outcomes.session_id AND '
                . '   student_results_outcomes.combined_by = session_stations.station_number AND '
                . '   session_stations.form_id = form_sections.form_id AND '
                . '   form_sections.section_id = section_items.section_id AND '
                . '   section_items.item_id = item_scores.item_id AND '
                . '   section_items.item_id = student_item_outcomes.item_id AND '
                . '   student_item_outcomes.item_id = item_scores.item_id AND '
                . '   student_results_outcomes.student_id = student_results.student_id AND '
                . '   student_results_outcomes.student_id = student_item_outcomes.student_id AND '
                . '   item_scores.result_id = student_results.result_id '

                . 'WHERE student_results_outcomes.student_id = ? AND '
                . '      student_results_outcomes.session_id = ? AND '
                . '      student_results_outcomes.combined_by = ? '

                . 'ORDER BY student_results_outcomes.combined_by, form_sections.form_id, section_items.item_id, student_results.examiner_id';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('sii', $student, $session, $station_number);

         $stmt->execute();
         $result = $stmt->get_result();

         $groupedItemScores = CoreDB::groupBySortedColumns(
             $result,
             ['combined_by', 'section_id', 'item_id', 'examiner_id'],
             ['item_id' => ['score', 'weighted_score'], 'examiner_id' => ['option_value', 'option_weighted_value']],
             [
                 'option_value' => function ($value) { return is_numeric($value) ? (float)$value : $value; },
                 'option_weighted_value' => 'floatval',
                 'score' => 'floatval',
                 'weighted_score' => 'floatval'
             ]
         );
         $scoresResult['stationScores'] = $groupedItemScores;

         $result->close();
         $stmt->close();

         return $scoresResult;
     }


     /**
      * Get all station outcomes by exam
      * @param int $exam       Identifier for exam
      * @return mysqli_result
      */
     public function byExam($exam)
     {

          $stmt = $this->db->prepare(
              "SELECT * FROM student_results_outcomes 
                INNER JOIN exam_sessions USING(session_id) 
                WHERE exam_id = ? "
          );

          $stmt->bind_param('i', $exam);
          $stmt->execute();
          $result = $stmt->get_result();

          $stmt->close();

          return $result;

     }

     /**
      * Get the outcome of a given exam for a given student
      *
      * @param string $studentId
      * @param int $examId
      * @return array|null
      */
     public function byStudentAndExam($studentId, $examId)
     {

         $query = 'SELECT score, adjusted_score, possible, '
                . 'adjusted_possible, pass, grade, adjusted_grade, '
                . 'grade_trigger, grade_author, notes '
                . 'FROM student_exam_data '
                . 'WHERE student_id = ? AND exam_id = ?';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('si', $studentId, $examId);
         $stmt->execute();

         $result = $stmt->get_result();
         $row = $result->fetch_assoc();

         if (!$row) {
             $result->close();
             $stmt->close();
             return null;
         }

         $outcome = [
             'rawScore' => (float)$row['score'],
             'adjustedScore' => (float)$row['adjusted_score'],
             'possibleScore' => (float)$row['possible'],
             'adjustedPossible' => (float)$row['adjusted_possible'],
             'grade' => $row['grade'],
             'weightedGrade' => $row['adjusted_grade'],
             'gradeTrigger' => $row['grade_trigger'],
             'gradeAuthor' => $row['grade_author'],
             'notes' => $row['notes'],
             'overallPass' => (float)$row['pass'],
             'grade' => $row['grade'],
             'weightedGrade' => $row['adjusted_grade']
         ];

         $result->close();
         $stmt->close();

         $stations = $this->byStudentAndExamWithStations($studentId, $examId);
         $outcome['stations'] = $stations;

         return $outcome;
     }

     /**
      * @param string $studentID
      * @param int $examID
      * @return array
      */
     public function byStudentAndExamWithStations($studentID, $examID)
     {
         $query = 'SELECT student_results_outcomes.* '
             . 'FROM student_results_outcomes JOIN session_stations JOIN exam_sessions '
             . 'ON student_results_outcomes.session_id = session_stations.session_id AND '
             . '   student_results_outcomes.combined_by = session_stations.station_number AND '
             . '    session_stations.session_id = exam_sessions.session_id AND '
             . '    student_results_outcomes.session_id = exam_sessions.session_id '

             . 'WHERE student_results_outcomes.student_id = ? AND exam_sessions.exam_id = ? '
             . 'ORDER BY student_results_outcomes.combined_by';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('si', $studentID, $examID);
         $stmt->execute();

         $result = $stmt->get_result();
         $passedToBool = function ($v) { return $this->passedToBool($v); };
         $groupedByStation = CoreDB::groupBySortedColumns(
             $result,
             ['combined_by'],
             ['combined_by' => ['session_id', 'score', 'adjusted_score', 'adjusted_possible', 'possible', 'passed', 'adjusted_passed', 'pass_possible']],
             ['adjusted_score' => 'floatval', 'adjusted_possible' => 'floatval', 'possible' => 'floatval', 'passed' => $passedToBool, 'adjusted_passed' => $passedToBool]
         );

         $result->close();
         $stmt->close();

         return !empty($groupedByStation) ?
            $groupedByStation['combined_by'] : [];
     }

     public function setUpToDateByStation($studentID, $sessionId, $stationNumber, $isUpToDate)
     {
         $query = 'UPDATE session_stations st JOIN exam_sessions s JOIN student_exam_data e '
                . 'ON st.session_id = s.session_id AND '
                . '   s.exam_id = e.exam_id '
                . 'SET e.up_to_date = ? '
                . 'WHERE st.session_id = ? and st.station_number = ? AND e.student_id = ?';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('iiis', $isUpToDate, $sessionId, $stationNumber, $studentID);
         $stmt->execute();

         $numberOfRowsAffected = $stmt->num_rows;

         $stmt->close();

         return $numberOfRowsAffected;
     }

     /**
      * @param $formID
      * @param $isUpToDate
      * @return int
      */
     public function setUpToDateByForm($formID, $isUpToDate)
     {
         $query = 'UPDATE student_exam_data e '
                . 'JOIN exam_sessions s JOIN session_stations st '
                . 'ON e.exam_id = s.exam_id AND '
                . '   s.session_id = st.session_id '
                . 'SET e.up_to_date = ? '
                . 'WHERE st.form_id = ?';

         $isUpToDateInt = $isUpToDate ? 1 : 0;

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('ii', $isUpToDateInt, $formID);
         $stmt->execute();

         $numberOfRowsAffected = $stmt->num_rows;

         $stmt->close();

         return $numberOfRowsAffected;
     }

     /**
      * Update the station outcome record for a student
      * @param string $studentID     Identifier number for student
      * @param int $sessionID        Identifier number for session
      * @param int $stationNumber    Station number
      * @param mixed[] $data         Outcome data to set
      * @return bool
      */
     public function update($studentID, $sessionID, $stationNumber, array $data)
     {

         $sql = 'INSERT INTO student_results_outcomes '
             . '(student_id, session_id, combined_by, score, adjusted_score, '
             . 'possible, adjusted_possible, pass_possible, '
             . 'passed, adjusted_passed) '
             . 'VALUES (?,?,?,?,?,?,?,?,?,?) '
             . 'ON DUPLICATE KEY UPDATE '
             . 'score = ?, adjusted_score = ?, possible = ?, '
             . 'adjusted_possible = ?, pass_possible = ?, passed = ?, adjusted_passed = ?';

         $dataValues = array_values($data);

         $stmt = $this->db->prepare($sql);
         $stmt->bind_param(
             'siidddddiidddddii', $studentID, $sessionID,
             $stationNumber, ...$dataValues, ...$dataValues
         );

         $status = $stmt->execute();

         $stmt->close();

         return $status;

     }

     public function isUpToDate($studentID, $examID)
     {
         $query = 'SELECT up_to_date '
                . 'FROM student_exam_data '
                . 'WHERE student_id = ? AND exam_id = ?';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('si', $studentID, $examID);
         $stmt->execute();

         $result = $stmt->get_result();
         $upToDate = $result->fetch_assoc()['up_to_date'];

         $result->close();
         $stmt->close();

         return (bool) $upToDate;
     }

     public function isUpToDateByStation($studentId, $sessionId, $stationNumber)
     {
         $query = 'SELECT up_to_date '
                . 'FROM student_exam_data JOIN exam_sessions JOIN session_stations '
                . 'ON student_exam_data.exam_id = exam_sessions.exam_id AND '
                . '   exam_sessions.session_id = session_stations.session_id '
                . 'WHERE student_exam_data.student_id = ? AND '
                . '      session_stations.session_id = ? AND '
                . '      session_stations.station_number = ?';

         $stmt = $this->db->prepare($query);
         $stmt->bind_param('sii', $studentId, $sessionId, $stationNumber);
         $stmt->execute();

         $result = $stmt->get_result();
         $upToDate = $result->fetch_assoc()['up_to_date'];

         $result->close();
         $stmt->close();

         return (bool) $upToDate;
     }

     private function passedToBool($passed) {
         if ($passed === null)
             return null;

         return $passed === 1;
     }

 }
