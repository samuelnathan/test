<?php

namespace OMIS\Database;


use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class Groups extends AbstractEntityDAO
{

    //Get Group Details [Updated 27/01/2014]
    public function getGroup($group_id) { return $this->getById($group_id); }

    /**
     * Get a list of sessions records by session_id
     *
     * @param mixed[]|null $sessionID ID number of the session
     * @param boolean $asArray        convert mysqliresultset into an array
     * @return mixed[]|mysqli_result|false
     */
    public function getSessionGroups($sessionID, $asArray = false)
    {
        $sql = "SELECT group_id, group_name, group_order "
            . "FROM session_groups "
            . "WHERE session_id = '" . $this->db->clean($sessionID)
            . "' ORDER BY group_order";

        $mysqliResult = $this->db->query($sql);
        if ($asArray) {
            $resultArray = CoreDB::into_array($mysqliResult);
            return $resultArray;
        } else {
            return $mysqliResult;
        }
    }

    /** Get Groups Data (+ session data)
     * @param int[] $ids List of group identifiers
     * @return mixed[]
     */
    public function getData($ids)
    {

        $groupData = $sessionData =
        $examData = $settings = [];

        // Process selected Groups
        foreach ($ids as $id) {

            // Grab Group data
            $group = $this->db->groups->getGroup($id);
            $groupData[$id] = [
                'group_id' => $group['group_id'],
                'session_id' => $group['session_id'],
                'group_name' => $group['group_name']
            ];

            // Grab Session data
            if (!isset($sessionData[$group['session_id']])) {

                $session = $this->db->sessions->getSession($group['session_id']);
                $dateFormatted = formatDate($session['session_date']);

                $sessionData[$group['session_id']] = [
                    'session_id' => $group['session_id'],
                    'session_date' => formatDate($session['session_date']),
                    'session_description' => $session['session_description'],
                    'dateDesc' => $dateFormatted . ' ' . $session['session_description']
                ];

                // Grab Exam data
                if (!isset($examData[$session['exam_id']])) {

                    $examData = $this->db->exams->getExam($session['exam_id']);
                    $settings = $this->db->examSettings->get($session['exam_id']);

                }

            }

        }

        return [$groupData, $sessionData, $examData, $settings];

    }

    /**
     * Add new group to session
     */
    public function addSessionGroup($session_id, $group_name, $group_order = null)
    {
        $session_id = $this->db->clean($session_id);
        $group_name = $this->db->clean($group_name);

        if (is_null($group_order)) {
            $group_order = $this->getSessionGroupCount($session_id);
            $group_order++;
        } else {
            $group_order = $this->db->clean($group_order);
        }

        $group_order = $this->db->clean($group_order);

        $sql = "INSERT INTO session_groups (session_id, group_name, group_order) "
            . "VALUES ('" . $session_id . "', '" . $group_name . "', '"
            . $group_order . "')";

        if ($this->db->query($sql)) {
            $inserted_group_id = $this->db->inserted_id();
            $sessionRecord = $this->db->sessions->getSession($session_id);
            $examRecord = $this->db->exams->getExam($this->db->sessions->getSessionExamID($session_id));
            $logString = $_SESSION['user_identifier'] . ' added Group ' . $group_name
                . ' to Exam ' . $examRecord['exam_name'] . ' '
                . formatDate($sessionRecord['session_date']) . ' '
                . $sessionRecord['session_description'];

            \Analog::info($logString);
            return $inserted_group_id;
        }

        return false;
    }

    //Update Group [Updated 05/02/2014]
    public function updateGroup($group_id, $group_name, $group_order)
    {
        $group_id = $this->db->clean($group_id);
        $group_name = $this->db->clean($group_name);
        $group_order = $this->db->clean($group_order);

        $sql = "UPDATE session_groups SET group_name = '" . $group_name
            . "', group_order = '" . $group_order . "' WHERE group_id = '"
            . $group_id . "'";

        return (bool) $this->db->query($sql);
    }

    /**
     * Delete old groups
     *
     * @param int $sessionID
     * @param array $exceptGroups list of group IDs to ignore
     * @return boolean true|false
     */
    public function deleteOldGroups($sessionID, $exceptGroups)
    {
        $sessionIDSan = $this->db->clean($sessionID);
        $sql = "DELETE FROM session_groups WHERE session_id = '$sessionIDSan'";

        if (sizeof($exceptGroups) > 0) {
            $sql .= " AND group_id NOT IN (". implode(',', $exceptGroups) .")";
        }

        $success = $this->db->query($sql);
        $affectedRowCount = $this->db->getAffectedRowCount();

        // Log success deletions
        if ($success &&  $affectedRowCount > 0) {

            $session = $this->db->sessions->getSession($sessionID);
            $name = formatDate($session['session_date']) . ' ' . $session['session_description'];
            $logString = $_SESSION['user_identifier'] . ' removed '.$affectedRowCount.' group(s) in session ' .$name;
            \Analog::info($logString);

        }

        return $success;
    }

    //Do Groups Exist [Updated 03/02/2014]
    public function doGroupsExist($session_groups, $session_id = null)
    {
        $sessionQ = ($session_id == null) ? '' : " AND session_id = '" . $session_id . "'";

        foreach ($session_groups as $group_id) {
            $q = "SELECT COUNT(*) FROM session_groups WHERE group_id = '"
                . $this->db->clean($group_id) . "'" . $sessionQ;
            $count = CoreDB::single_result($this->db->query($q));
            if (!($count > 0)) {
                return false;
            }
        }
        return true;
    }

    //Delete Students in Group [Updated 05/02/2014]
    public function deleteStudentsInGroup($group_id)
    {
        $sql = "DELETE FROM group_students WHERE group_id = '"
            . $this->db->clean($group_id) . "'";
        return $this->db->query($sql);
    }

    //Does Student Exist In Group [Updated 10/02/2014]
    /**
     * Checks if a student is a member of a particular student group.
     *
     * @param string $student_id Student ID number
     * @param int $group_id Group ID number
     * @return bool TRUE if student is a member of specified group otherwise FALSE.
     */
    public function doesStudentExistInGroup($student_id, $group_id)
    {
        $student_id = $this->db->clean($student_id);
        $group_id = $this->db->clean($group_id);
        $sql = "SELECT COUNT(*) FROM group_students WHERE student_id = '" . $student_id
            . "' AND group_id = '" . $group_id . "'";

        $count = CoreDB::single_result($this->db->query($sql));
        return ($count > 0);
    }

    /**
     * Return the (non-blank) students in a given group/session pairing.
     *
     * @param mixed $groupIDs      One or more group IDs (scalar or array) or NULL
     *                             to return all groups.
     * @param mixed $sessionID     Either a session ID or null
     * @param bool  $returnArray   If TRUE, return an array, otherwise a mysqli_result
     * @param string  $termID      Term identifier (optional)
     * @return mixed               Either an array or a mysqli_result depending on $returnArray
     */
    public function getStudents($groupIDs = null, $sessionID = null, $returnArray = true, $termID = null)
    {
        // List of criteria for the WHERE part of the query.
        $whereCriteria = [];

        // Groups query
        if (!is_null($groupIDs)) {
            /* If $group_ids isn't an array, promote it to be one for the
             * purposes of the code to follow.
             */
            if (!is_array($groupIDs)) {
                $groupIDs = [$groupIDs];
            }

            $whereCriteria[] = "group_students.group_id IN (" . implode(', ', $groupIDs) . ")";
        }

        // Session query
        if (!is_null($sessionID)) {
            $whereCriteria[] = "session_groups.session_id = '$sessionID'";
        }

        // Where clause
        $whereClause = implode(" AND ", $whereCriteria);

        $sql = "SELECT students.student_id, group_students.student_exam_number, candidate_numbers.candidate_number, "
            . "group_students.student_order, students.forename, students.surname, "
            . "students.dob, students.gender, students.email, students.nationality, "
            . "students.student_image, exam_sessions.exam_id, group_students.group_id, "
            . "session_groups.group_name, session_groups.group_order, exam_sessions.session_id "

            . "FROM (((group_students INNER JOIN session_groups USING(group_id)) "
            . "INNER JOIN students USING(student_id)) "
            . "INNER JOIN exam_sessions USING(session_id)) "
            . "LEFT JOIN candidate_numbers "
            . "ON (group_students.student_id = candidate_numbers.student_id AND candidate_numbers.term_id = '$termID') "
            . "WHERE " . $whereClause . " "

            . "GROUP BY group_students.group_id, group_students.student_id "
            . "ORDER BY session_date, session_id, session_groups.group_order, group_students.student_order";

        // Decide what to return to the user.
        $result = $this->db->query($sql);
        return ($returnArray) ? CoreDB::into_array($result) : $result;
    }

    /**
     * Get student group record
     *
     * @param string $studentID    number to identifier the student
     * @param int $groupID         number to identifier the group
     * @return mixed[]
     */
    public function getStudentGroupRecord($studentID, $groupID) {
        $sql = "SELECT * FROM group_students "
            . "WHERE student_id = '". $this->db->clean($studentID) ."' "
            . "AND group_id = ". $this->db->clean($groupID);

        return $this->db->fetch_row($this->db->query($sql));
    }

    /**
     * Get students not in session groups
     *
     * @param int $sessionID
     * @param string $moduleID
     * @param int $yearID
     * @return bool true|false
     */
    public function getStudentsNotInGroups($sessionID, $moduleID, $yearID)
    {
        $examID = $this->db->sessions->getSessionExamID($sessionID);
        $moduleID = $this->db->clean($moduleID);
        $yearID = $this->db->clean($yearID);

        $sql = "SELECT students.*, candidate_number "
            . "FROM ((student_courses "
            . "INNER JOIN students USING(student_id)) "
            . "INNER JOIN course_years USING(year_id)) "
            . "LEFT JOIN candidate_numbers ON (student_courses.student_id = candidate_numbers.student_id "
            . "AND course_years.term_id = candidate_numbers.term_id) "
            . "WHERE year_id = '$yearID' AND module_id = '$moduleID' "
            . "AND students.student_id NOT IN (SELECT student_id FROM (group_students "
            . "INNER JOIN session_groups USING(group_id)) "
            . "INNER JOIN exam_sessions USING(session_id) "
            . "WHERE exam_sessions.exam_id = '$examID') "
            . "GROUP BY students.student_id "
            . "ORDER BY students.surname";

        return $this->db->query($sql);
    }

    //get Group count [Updated 27/01/2014]
    public function getSessionGroupCount($session_id)
    {
        $sql = "SELECT COUNT(*) FROM session_groups WHERE session_id = '"
            . $this->db->clean($session_id) . "'";

        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Add Students to Group
     *
     * @param array $students
     * @param int $groupID
     * @param string[] $studentExamNumbers
     * @return mixed[] students added
     */
    public function addStudentsToGroup($students, $groupID, $studentExamNumbers = [])
    {
        $summary = [];
        $groupIDSan = $this->db->clean($groupID);
        $count = count($students);
        $index = 0;

        while ($index < $count) {
            $order = $index + 1;
            $id = $this->db->clean($students[$index]);
            if (substr($id, 0, 4) == ":bs:") {
                $sql = "INSERT INTO blank_students (group_id, student_order) "
                    . "VALUES ('$groupIDSan', '$order')";
                $success = $this->db->query($sql);
            } else {
                $examNumber = (isset($studentExamNumbers[$id])) ? $studentExamNumbers[$id] : "";
                $sql = "INSERT INTO group_students (group_id, student_id, student_order, student_exam_number) "
                    . "VALUES ('$groupIDSan', '$id', '$order', '$examNumber')";
                $success = ($this->db->students->doesStudentExist($id)) ? $this->db->query($sql) : false;
            }

            $summary[] = [
                'id' => $id,
                'inserted' => $success
            ];

            $index++;
        }

        return $summary;
    }

    /**
     * Count the number of students in a particular student group.
     *
     * @param int  $group_id       ID number of student group
     * @param bool $include_blanks If TRUE, include blank students in counts (if
     *                             used with $detailed = TRUE, then actual and
     *                             blank student coutns are included separately
     *                             in the totals)
     * @param bool $detailed       If TRUE, return results as an associative array.
     * @return int|mixed[]         If $detailed is TRUE, return an array; if not, a single integer.
     */
    public function getGroupSize($group_id, $include_blanks = false, $detailed = false)
    {
        $group_id = $this->db->clean($group_id);
        $actual_student_query = "SELECT COUNT(*) as count FROM group_students WHERE group_id = $group_id";

        $actual_total = CoreDB::single_result($this->db->query($actual_student_query));

        //Include blank student count ?
        if ($include_blanks) {
            $blank_student_query = "SELECT COUNT(*) as count FROM blank_students WHERE group_id = $group_id";
            $blank_total = CoreDB::single_result($this->db->query($blank_student_query));
        } else {
            $blank_total = 0;
        }

        if ($detailed) {
            $result = array('total' => $actual_total + $blank_total);
            if ($include_blanks) {
                $result['actual'] = $actual_total;
                $result['blanks'] = $blank_total;
            }
        } else {
            $result = $actual_total + $blank_total;
        }

        return $result;
    }

    //Re Order Groups [Updated 05/02/2014]
    public function reOrderGroups($session_id)
    {
        $sql = "SELECT group_id FROM session_groups WHERE session_id = '"
            . $this->db->clean($session_id) . "' ORDER BY group_order ASC";
        $groupDB = $this->db->query($sql);
        $order = 0;

        while ($groupRow = mysqli_fetch_assoc($groupDB)) {
            $order++;
            $update_query = "UPDATE session_groups SET group_order = '" . $order
                . "' WHERE group_id = '" . $groupRow['group_id'] . "'";
            $this->db->query($update_query);
        }
    }

    //Get Session ID By Group ID [Updated 05/02/2014]
    public function getSessionIDByGroupID($group_id)
    {
        $sql = "SELECT session_id FROM session_groups WHERE group_id = '"
            . $this->db->clean($group_id) . "'";

        return CoreDB::single_result($this->db->query($sql));
    }

    //Clone Group Names [Updated 06/02/2014]
    public function cloneGroupNames($new_session_id, $org_session_id)
    {
        $new_session_id = $this->db->clean($new_session_id);

        if ($this->db->sessions->doesSessionExist($org_session_id)) {
            $session_groups_db = $this->getSessionGroups($org_session_id);

            while ($session_groups = mysqli_fetch_assoc($session_groups_db)) {
                $this->addSessionGroup($new_session_id, $session_groups['group_name']);
            }
        }
    }

    // Sort Groups Order
    public function sortGroupsOrder($groups)
    {

        // Remove Duplicate Groups
        $groups = array_unique($groups);
        $updateCount = 0;

        foreach ($groups as $group) {

            $students = $this->getStudents(array($group));
            $newOrder = 1;

            foreach ($students as $student) {

                $sql = "UPDATE group_students SET student_order = '" . $newOrder . "' WHERE student_id = '"
                    . $student['student_id'] . "' AND group_id = '" . $this->db->clean($group) . "'";

                if ($this->db->query($sql)) {

                    $updateCount++;

                }

                $newOrder++;

            }

        }

        return $updateCount;

    }

    //Get Student Groups [Updated 04/02/2014]
    public function getStudentGroups($student_id)
    {
        $student_groups = array();
        $sql = $this->db->query(
            "SELECT group_id FROM group_students WHERE student_id = '"
            . $this->db->clean($student_id) . "' GROUP BY group_id"
        );

        while ($result = $this->db->fetch_row($sql)) {
            $student_groups[] = $result['group_id'];
        }
        return $student_groups;
    }

    /**
     * Get students & blanks, one array
     * @param int $groupID
     * @param string $termID
     * @return students[]
     */
    public function getStudentsAndBlanks($groupID, $termID = null)
    {
        $students = [];
        $studentsDB = $this->getStudents([$groupID], null, true, $termID);
        $blankStudentsDB = $this->db->blankStudents->getBlankStudents($groupID);

        foreach ($studentsDB as $student) {
            $students[] = [
                "student_order" => $student["student_order"],
                "type" => 0,
                "student_id" => $student["student_id"],
                "exam_number" => $student["student_exam_number"],
                "candidate_number" => $student["candidate_number"],
                "forename" => $student["forename"],
                "surname" => $student["surname"],
                "gender" => $student["gender"],
                "dob" => $student["dob"],
                "nationality" => $student["nationality"],
                "email" => $student["email"]
            ];
        }

        foreach ($blankStudentsDB as $blank) {
            $students[] = [
                "student_order" => $blank["student_order"],
                "type" => 1
            ];
        }
        unset($studentsDB);
        unset($blankStudentsDB);
        orderArrayColumn($students, "student_order", "standard", "ASC");

        return $students;
    }

    /**
     * Gets the list of students in a group given its ID
     * @param $groupID
     * @param $sorted boolean Boolean flag indicating whether the result must be sorted by student order in group
     * @return array
     */
    public function getStudentsByGroup($groupID, $sorted = true) {
        $idSan = $this->db->clean($groupID);
        $query = "SELECT group_id, group_students.student_id, student_exam_number, student_order, forename, surname "
            . "FROM group_students JOIN students ON group_students.student_id = students.student_id "
            . "WHERE group_id = $idSan ";

        if ($sorted)
            $query .= "ORDER BY student_order ";

        return CoreDB::into_array($this->db->query($query));
    }

    public function getStudentsByGroupWithCandidateNumber($groupID, $sorted = true) {
        $groupID = $this->db->clean($groupID);
        $query = "SELECT group_students.group_id, group_students.student_id, student_exam_number, candidate_numbers.candidate_number, 
                         student_order, forename, surname "
               . "FROM group_students JOIN students JOIN candidate_numbers JOIN exams JOIN exam_sessions JOIN session_groups "
               . "ON group_students.student_id = students.student_id AND "
               . "  students.student_id = candidate_numbers.student_id AND "
               . "  candidate_numbers.term_id = exams.term_id AND "
               . "  exams.exam_id = exam_sessions.exam_id AND "
               . "  exam_sessions.session_id = session_groups.session_id AND "
               . "  session_groups.group_id = group_students.group_id "
               . "WHERE group_students.group_id = $groupID ";

        if ($sorted)
            $query .= 'ORDER BY student_order ';

        return CoreDB::into_array($this->db->query($query));
    }

    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds()
    {
        return ['group_id'];
    }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'session_groups';
    }
}