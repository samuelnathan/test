<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 *
 * Database function calls relating to Assessment forms
 */

namespace OMIS\Database;

use OMIS\Database\DAO\AbstractEntityDAO;

class Forms extends AbstractEntityDAO
{

    /**
     * Get form by identifier
     *
     * @param array $id       id number of form required
     * @return array          array of form data
     */
    public function getForm($id)
    {

        $id = $this->db->clean($id);
        $sql = "SELECT * "
             . "FROM forms "
             . "INNER JOIN form_department_term "
             . "USING(form_id) "
             . "WHERE forms.form_id = '$id' "
             . "LIMIT 1";

        return $this->db->fetch_row($this->db->query($sql));

    }

    /**
     * Get number of form occurances for form
     * in department/term
     *
     * @param array $id       id number of form required
     * @return int            number of occurances
     */
    public function formOccurances($id)
    {

        return mysqli_num_rows(
            $this->db->query("SELECT *
             FROM forms INNER JOIN form_department_term
             USING(form_id)
             WHERE forms.form_id = '{$this->db->clean($id)}'")
        );

    }

    /**
     * Get multiple forms by identifiers
     *
     * @param array $formIDs  array of form identifier numbers
     * @return array|false    array of form data
     */
    public function getMultipleForms($formIDs)
    {
        // Empty array then abort
        if (empty($formIDs)) {
            error_log(__method__ . "FormIDs parameter is empty");
            return [];
        }

        // Santize form indentifers
        foreach ($formIDs as &$formID) {
            $formID = $this->db->clean($formID);
        }

        $idsString = implode(", ", $formIDs);

        $sql = "SELECT * "
                . "FROM forms "
                . "INNER JOIN form_department_term "
                . "USING(form_id) "
                . "WHERE forms.form_id IN ($idsString) "
                . "GROUP BY form_id";

        $resultset = $this->db->query($sql);
        if (!$resultset) {
            return false;
        }

        $resultArray = CoreDB::into_array($resultset);
        return CoreDB::group($resultArray, ['form_id'], true, true);
    }

    /**
     * Get assessment forms
     *
     * @param string $term            ID number for term
     * @param string $dept            ID number for department
     * @param string $order           order by field
     * @param string $type            order by type
     * @param bool $results           include has_result status
     * @param bool $tags              include linked tags
     * @return mysqliresults|mixed[]
     */
    public function getForms($term, $dept = null, $order = null, $type = null, $results = false, $tags = false)
    {

        $termsql = (strlen($term) > 0 && $this->db->academicterms->existsById($term))
                ? "form_department_term.term_id = '" . $this->db->clean($term) . "' " : '';
        $deptsql = (!is_null($dept))
                ? "form_department_term.dept_id = '" . $this->db->clean($dept) . "' " : '';
        $wheresql = (strlen($termsql) > 0 || strlen($deptsql) > 0)
                ? 'WHERE ' : '';
        $andsql = (strlen($termsql) > 0 && strlen($deptsql) > 0)
                ? 'AND ' : '';
        $ordersql = (!is_null($order) && !is_null($type))
                ? "ORDER BY " . $this->db->clean($order) . " " . $this->db->clean($type) : '';

        // With has result value
        if ($results) {

            $sql = "SELECT form_data.*, IF(station_data.has_result > 0, 1, 0) AS has_result "
                 . "FROM (SELECT forms.*, form_department_term.term_id, form_department_term.dept_id FROM forms "
                 . "INNER JOIN form_department_term USING (form_id) "
                 . $wheresql . $termsql . $andsql . $deptsql . " "
                 . "GROUP BY form_id) AS form_data "
                 . "LEFT JOIN (SELECT form_id AS has_result "
                 . "FROM session_stations "
                 . "INNER JOIN student_results USING (station_id) "
                 . "GROUP BY form_id) AS station_data "
                 . "ON (form_data.form_id = station_data.has_result) "
                 . $ordersql;

            return CoreDB::into_array($this->db->query($sql));

        // Without has result value
        } else {

            $tagJoin = $tags ? "LEFT JOIN form_tags USING(form_id) "
            . "LEFT JOIN tags ON (form_tags.tag_id = tags.id)" : "";
            $tagGroup = $tags ? ", tag_id" : "";
            $ordersql = $tags ? "ORDER BY -name DESC, form_name ASC" : $ordersql;
            $sql = "SELECT * FROM forms INNER JOIN "
                 . "form_department_term USING(form_id) $tagJoin"
                 . $wheresql . $termsql . $andsql . $deptsql . " "
                 . "GROUP BY form_id$tagGroup $ordersql";
            return $this->db->query($sql);

        }

    }

    /**
     * Get assessment form tags
     *
     * @param bool $id  Assessment form identifier
     * @return mixed[] List of tags
     */
    public function getFormTags($id)
    {

      return CoreDB::into_array(
       $this->db->query(
          "SELECT tags.id AS id, tags.name AS name FROM form_tags
          INNER JOIN tags ON (form_tags.tag_id = tags.id)
          WHERE form_id = " . $this->db->clean($id) . "
          GROUP BY form_tags.id"
      ));

    }

    /**
     * Get tags per academic year and department
     * @param string $term        Academic Year identifier (optional)
     * @param string $department  Department identifier (optional)
     *
     * @return mixed[] List of tags
     */
    public function getTags($term = null, $department = null)
    {

      if (!is_null($term) && !is_null($department)) {

        $clause = "INNER JOIN form_tags
          ON (form_tags.tag_id = tags.id)
          INNER JOIN form_department_term USING(form_id)
          WHERE dept_id = '" . $this->db->clean($department) . "'
          AND term_id = '" . $this->db->clean($term) . "'";

      } else {
        $clause = "";
      }
      return CoreDB::into_array(
       $this->db->query(
          "SELECT * FROM tags $clause
          GROUP BY tags.id"
      ));

    }

    /**
     * Add/update form tags
     * @param int $form         Identifier for assessment form
     * @param string[] $tags    Array of strings that represent list of tags
     * @return bool true|false
     */
    public function linkFormTags($form, $tags)
    {

      if (empty($form)) {
         return false;
      }

      $ignoreTagIDs = [];
      $formID = $this->db->clean($form);
      foreach ($tags as $tag) {

         if (empty($tag)) {
            continue;
         }

         /**
          *  Tidy up tag, remove any illegal characters that may
          *  have reached this stage
          */
         $tagSan = preg_replace("/[^a-z\d\-]+/", "", $tag);

         list($exists, $tagID) = $this->doesTagExist($tagSan);
         $inserted = true;
         if (!$exists) {
            $inserted = $this->db->query(
               "INSERT INTO tags (name) VALUES('" .
               $this->db->clean($tagSan) . "')"
            );
            $tagID = $this->db->inserted_id();
         }

         if ($inserted) {
           $ignoreTagIDs[] = $tagID;
           $this->db->query(
              "INSERT IGNORE INTO form_tags (form_id, tag_id)VALUES" .
              "('$formID', '" . $tagID . "')"
           );
         }

     }

     $this->deleteFormTags($formID, $ignoreTagIDs);
     $this->pruneTags();

    }

    /**
     * Delete form tags
     * @param int $form                     Identifier for assessment form
     * @param int[] $ignore (optional)      List of tag identifiers to ignore
     * @return bool true|false
     */
    public function deleteFormTags($form, $ignore = [])
    {

        $ignoreClause = (count($ignore) > 0)
        ? "AND tag_id NOT IN (" . implode(", ", $ignore) . ")" : "";

        return $this->db->query(
           "DELETE FROM form_tags WHERE form_id = '" .
           $this->db->clean($form) . "' " . $ignoreClause
        );

    }

    /**
     * Prune all tag records
     * @return bool true|false
     */
    public function pruneTags()
    {
      return $this->db->query(
         "DELETE FROM tags WHERE id " .
         "NOT IN (SELECT tag_id FROM form_tags)"
      );
    }

    /**
     * Check for existence of tag
     * @param string $tag    tag name
     * @return mixed[] status and id
     */
    public function doesTagExist($tag)
    {

      $name = $this->db->clean($tag);
      $result = $this->db->query("SELECT id FROM tags WHERE name = '$name'");
      $exists = (mysqli_num_rows($result) == 1);
      return ($exists ? [true, CoreDB::single_result($result)] : [false, null]);

    }

    /**
     * Insert Assessment Form
     *
     * @param string $name           Name of form
     * @param string $name           Identifier of author
     * @param string $dept           Identifier of department
     * @param string $term           Identifier of term
     * @param string $desc           Form description (optional)
     *
     * @return [] success and new form identifier, FALSE and null otherwise.
     */
    public function insertForm($name, $author, $dept, $term, $desc = '')
    {

        $name = $this->db->clean($name);
        $desc = $this->db->clean($desc);
        $author = $this->db->clean($author);

        $insert = "INSERT INTO forms (form_name, form_description, author) "
                . "VALUES ('$name', '$desc', '$author')";

        if ($this->db->query($insert)) {

            $form = $this->db->inserted_id();

            if ($this->attachFormsToDeptTerm([$form], $dept, $term)) {

                \Analog::info($_SESSION['user_identifier'] . ' added a form '
                        . $name . ' to department ' . $dept);

                return [true, $form];

            }

        }

        return [false, null];

    }

    /**
     * Determine whether a form exists in the database with the specified ID
     *
     * @param int $form_id Form ID number
     * @return bool TRUE if form exists, FALSE otherwise.
     */
    public function doesFormExist($id) { return $this->existsById($id); }

    // Get Form IDs By Session (Session ID)
    public function getFormIDsBySession($id)
    {

        $sql = "SELECT DISTINCT(form_id) "
             . "FROM session_stations WHERE session_id = '"
             . $this->db->clean($id) . "'";

        return CoreDB::into_values($this->db->query($sql));

    }

    // Is form used
    public function isFormUsed($id)
    {

        $id = $this->db->clean($id);
        $sql = "SELECT COUNT(*) AS COUNT "
             . "FROM session_stations WHERE form_id = '$id'";

        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    /**
     * Update form
     *
     * @param int $id           Identifier number of form
     * @param string $name      Name of form
     * @param bool $complete    Design complete
     * @param string $userID    Author identifier
     * @param string $notes     Station notes
     * @param string $dept      Department Identifier
     * @return bool status      Update status
     */
    public function updateForm($id, $name, $complete, $poptype, $userID, $notes = null, $dept = null)
    {

        $id = $this->db->clean($id);
        $name = $this->db->clean($name);
        $complete = $this->db->clean($complete);
        $poptype = $this->db->clean($poptype);
        $userID = $this->db->clean($userID);
        $notes = $this->db->clean($notes);
        $descSql = $notes === null ? " " : ", form_description = '$notes' ";
        $runDeptSql = ($dept !== null);
        $formRecord = $this->getForm($id);

        $sql = "UPDATE forms "
             . "SET form_name = '$name', author = '$userID', "
             . "design_complete = '$complete', notes_video = '$poptype'" . $descSql
             . "WHERE form_id = '$id'";

        if ($runDeptSql) {

            $deptSql = "UPDATE form_department_term "
                     . "SET dept_id = '" . $this->db->clean($dept) . "' "
                     . "WHERE form_id = '$id'";

        }

        $updated = ($this->db->query($sql) && (!$runDeptSql || ($runDeptSql && $this->db->query($deptSql))));

        if ($updated) {

            \Analog::info(
                $_SESSION['user_identifier'] . " updated form(id:$id) " .
                $formRecord['form_name'] . " => $name"
            );

        }

        return $updated;

    }

    /**
     * Change author identifier for all forms
     * they are linked to
     *
     * @param string $newID     New identifier string
     * @param string $orgID     Old identifier string
     * @return bool update status
     */
    public function changeAuthorID($newID, $orgID)
    {

        return $this->db->query(
            "UPDATE forms SET author = '" . $this->db->clean($newID) . "' " .
            "WHERE author = '" . $this->db->clean($orgID) . "'"
        );

    }

    /**
     * Update form author for this form
     *
     * @param int $id           Number identifier for form
     * @param string $author    String Identifier for author
     * @return bool update status
     */
    public function updateAuthor($id, $author)
    {

        return $this->db->query(
            "UPDATE forms SET author = '" . $this->db->clean($author) . "' " .
            "WHERE form_id = '" . $this->db->clean($id) . "'"
        );

    }

    /**
     * Delete forms
     * @param type $toDelete    forms to delete
     * @return int              number of forms deleted deleted
     */
    public function deleteForms($toDelete)
    {

        $n = count($toDelete);
        $deleteCount = 0;
        $i = 0;

        // Loop through form IDs and delete
        while ($i < $n) {

            $formIDRaw = $toDelete[$i];

            if (!$this->db->results->doesFormHaveResult($formIDRaw)) {

                $formRecord = $this->getForm($formIDRaw);

                // Sanitize form ID
                $formID = $this->db->clean($formIDRaw);
                $deletedFromTermList = $this->db->query("DELETE FROM form_department_term WHERE form_id = '$formID'");
                $deletedFromFormList = $this->db->query("DELETE FROM forms WHERE form_id = '$formID'");

                if ($deletedFromTermList && $deletedFromFormList) {

                    \Analog::info(
                       $_SESSION['user_identifier'] . ' deleted form ' .
                       $formRecord['form_name'] . ' from department ' . $formRecord['dept_id']
                    );

                    $deleteCount++;

                }

            // Forms that cannot be deleted
            } else {

                setA($_SESSION, 'forms_warning', []);
                $_SESSION['form_warnings'][$formIDRaw]['results'] = true;

            }

            $i++;

        }

        $this->pruneTags();

        return $deleteCount;

    }

    /**
     * Update form total
     *
     * @param int $id    id number for form
     * @return bool      update status
     */
    public function reCalcFormTotal($id)
    {
        $idSan = $this->db->clean($id);

        $sql = "SELECT SUM(GREATEST(lowest_value, highest_value)) "
             . "AS total FROM form_sections, section_items "
             . "WHERE form_sections.section_id = section_items.section_id "
             . "AND form_id = '$idSan' AND section_items.type "
             . "NOT IN ('radiotext', 'checkbox')";

        $queryResult = $this->db->query($sql);

        if ($queryResult) {

            $total = CoreDB::single_result($queryResult);
            return $this->db->query(
               "UPDATE forms SET total_value = '$total' " .
               "WHERE form_id = '$idSan'"
            );

        } else {

            return false;

        }

    }

    // Get station names by department term
    public function getFormNamesByDeptTerm($dept, $term)
    {

        $sql = "SELECT DISTINCT(form_name) FROM form_department_term "
             . "INNER JOIN forms USING(form_id) WHERE dept_id = '"
             . $this->db->clean($dept) . "' AND term_id = '"
             . $this->db->clean($term) . "'";

        return CoreDB::into_values($this->db->query($sql));

    }

    // Attach forms to department & term
    public function attachFormsToDeptTerm($forms, $dept, $term)
    {

        $dept = $this->db->clean($dept);
        $term = $this->db->clean($term);
        $valuesAdded = false;

        foreach ($forms as $form) {

            $sql = "INSERT IGNORE INTO form_department_term (form_id, dept_id, term_id) "
                 . "VALUES ('" . $this->db->clean($form) . "', '$dept', '$term')";
            $this->db->query($sql);
            $valuesAdded = true;

        }

        return $valuesAdded;

    }

    /**
     * Remove forms from academic term
     *
     * @param array $toRemove        forms to remove
     * @param string $departmentID   department ID
     * @param string $termID         academic term ID
     * @return int                   number of forms removed
     */
    public function removeFormsFromDeptTerm($toRemove, $departmentID, $termID)
    {

        $deptIDSan = $this->db->clean($departmentID);
        $termIDSan = $this->db->clean($termID);
        $deleteCount = 0;

        // Loop through forms to remove from term
        foreach ($toRemove as $formID) {

            /**
             * Does form exist? If not continue on to the next loop
             */
            if (!$this->doesFormExist($formID)) {

                continue;

            }

            /**
             * Is form linked to results for this term then add
             * to warning list and do not remove
             */
            $resultTermDept = $this->db->results->doesFormHaveResult(
                    $formID,
                    null,
                    null,
                    $termID,
                    $departmentID
            );

            if ($resultTermDept) {

                setA($_SESSION, 'form_warnings', []);
                $_SESSION['form_warnings'][$formID]['results'] = true;
                continue;

            }

            /**
             * Number of occurances equals one, then don't remove
             */
            if ($this->formOccurances($formID) == 1) {

                setA($_SESSION, 'form_warnings', []);
                $_SESSION['form_warnings'][$formID]['last'] = true;
                continue;

            }

            // Sanitize form ID
            $formIDSan = $this->db->clean($formID);

            // Remove forms from terms
            $sqlTerms = "DELETE FROM form_department_term "
                      . "WHERE form_id = '$formIDSan' "
                      . "AND term_id = '$termIDSan' "
                      . "AND dept_id = '$deptIDSan'";

            if (!$this->db->query($sqlTerms)) {

                continue;

            }

            $message = $_SESSION['user_identifier'] . " "
                   . "removed form with ID $formID "
                   . "from term with ID $termID "
                   . "from department with ID $departmentID";

            \Analog::info($message);
            $deleteCount++;

        }

        return $deleteCount;

    }

    /**
     * Set form design complete
     * @param int[] $forms
     * @param bool $complete
     * @return int  update count
     */
    public function setFormsDesignComplete($forms, $complete)
    {

        $updateCount = 0;
        foreach ($forms as $form) {

            $sql = "UPDATE forms SET design_complete = '" . (INT) $complete . "' "
                 . "WHERE form_id = '$form'";

            if ($this->db->query($sql)) {

                $updateCount++;

            }

        }

        return $updateCount;

    }

    // Does form have scale section
    public function doesFormHaveScaleSection($id)
    {

        $sql = "SELECT COUNT(*) FROM competencies INNER JOIN form_sections "
             . "USING(competence_id) WHERE competencies.competence_type = 'scale' "
             . "AND form_sections.form_id = '" . $this->db->clean($id) . "'";

        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    // Get form session records
    public function getFormStations($form, $sessions)
    {

        $sql = "SELECT session_stations.station_id, session_stations.pass_value "
              . "FROM session_stations WHERE session_stations.form_id = '"
              . $this->db->clean($form) . "' AND (session_stations.session_id "
              . "IN (" . implode(", ", $sessions) . "))";

        return $this->db->query($sql);

    }

    /**
     * Return list of Departments that have Forms
     *
     * @param int   $termID       ID number academic term
     * @param bool  $asArray      Return output as an array?
     *
     * @return mixed[]|mysqli_result|bool if $return_array is TRUE and there are results, return them
     * as an associative array, otherwise as a mysqli_result.
     */
    public function getDepartmentsWithForms($termID, $asArray = false)
    {

        $termExists = $this->db->academicterms->existsById($termID);
        $termID = $this->db->clean($termID);

        $termSql = $termExists ? "WHERE form_department_term.term_id = '$termID' " : '';
        $sql = "SELECT departments.dept_id, departments.dept_name "
             . "FROM (forms INNER JOIN form_department_term USING(form_id)) "
             . "INNER JOIN departments USING(dept_id) " . $termSql
             . "GROUP BY departments.dept_id";

        $mysqliResult = $this->db->query($sql);

        if (($mysqliResult !== \FALSE) && $asArray) {

            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['dept_id'], true, true);

        } else {

            return $mysqliResult;

        }

    }

    /**
     * Get form section
     *
     * @param int $id   Identifier number of section
     * @return bool     Update status
     */
    public function getFormSection($id)
    {

        $sql = "SELECT * FROM form_sections INNER JOIN competencies USING(competence_id) "
             . "WHERE section_id = '" . $this->db->clean($id) . "'";


        return $this->db->query($sql);

    }


    /**
     * Return all form sections (formerly known as "competencies") that are
     * associated with specific station form whose id is $form_id
     *
     * @param array $formIDs   list of station ID numbers
     * @param bool $toArray    return output as an array?
     * @param array $indexes   array indexes to use e.g. 'form_id' or 'section_id'
     *
     * @return mixed[]|mysqli_result|bool if $convertToArray is TRUE and there are results, return them
     * as an associative array, otherwise as a mysqli_result.
     */
    public function getFormSections($formIDs, $toArray = false, $indexes = null)
    {

        $formIDs = array_map("\OMIS\Database\CoreDB::clean", $formIDs);
        $idsString = implode(",", $formIDs);

        $sql = "SELECT * "
             . "FROM form_sections "
             . "INNER JOIN competencies "
             . "USING (competence_id) "
             . "WHERE form_id IN ($idsString) "
             . "GROUP BY form_id, section_id "
             . "ORDER BY form_id, section_order";

        $result = $this->db->query($sql);

        if (($result !== \FALSE) && $toArray) {

            if ($indexes == null) {

                return CoreDB::into_array($result);

            } else {

                $resultArray = CoreDB::into_array($result);
                return CoreDB::group($resultArray, $indexes, true, true);

            }

        } else {

            return $result;

        }

    }

    /**
     * Add form section
     *
     * @param int $id                 ID number for a form
     * @param int $order              Station order in form
     * @param bool $reSortOrder       Re-sort order
     * @param string $title           Section title
     * @param int $competence         ID number for the competency
     * @param bool $selfAssess        Sets a section as self assessment if selected
     * @param int $sectionFeedback    Section feedback enabled
     * @param int $itemFeedback       Item feedback enabled
     * @param int $feedbackRequired   Feedback required
     * @param int $internalFeedback   Internal feedback only (not released to students)
     *
     * @return bool success true|false
     */
    public function insertFormSection($id, $settings = [])
    {

        $settingSan = array_map(
            "\OMIS\Database\CoreDB::clean",
            array_values($settings)
            );

        list(
            $order, $reSortOrder, $title, $competence, $sectionFeedback,
            $itemFeedback, $feedbackRequired, $internalFeedback,
            $selfAssessment
         ) = $settingSan;

            $competenceSan = $this->db->clean($competence);

          // New order (Great Band !!)
          if ($order === null) {

              $newOrder = $this->getFormSectionCount($id) + 1;

          } else {

              $newOrder = $this->db->clean($order);
          }

          $sql = "INSERT INTO form_sections (form_id, competence_id, "
               . "section_text, section_order, section_feedback, item_feedback, "
               . "feedback_required, internal_feedback_only, self_assessment) VALUES ("
               . $this->db->clean($id) . ", '$competenceSan', '$title', '$newOrder', '$sectionFeedback', "
               . "'$itemFeedback', '$feedbackRequired', '$internalFeedback', '$selfAssessment')";

            $formRecord = $this->getForm($id);

            if (!$this->db->query($sql)) {

                return false;
            }

            \Analog::info(
                $_SESSION['user_identifier'] . " added section #$newOrder " .
                "$title to form(id:$id) " . $formRecord['form_name']
             );

            $sectionID = $this->db->inserted_id();

            $this->updateAuthor($id, $_SESSION['user_identifier']);

            if ($reSortOrder) {

                $this->reOrderFormSections($sectionID, $newOrder);

            }

         return $sectionID;

    }



    /*
     * Delete form section
     *
     * @param int $sectionID   Identifier number of section
     * @param int $formID      Identifier number of form
     *
     * @return bool success true|false
     */
    public function deleteFormSection($sectionID, $formID)
    {

        $formRecord = $this->getForm($formID);
        $sectionRecord = $this->db->fetch_row(
              $this->getFormSection($sectionID)
        );

        $result = $this->db->query(
            "DELETE FROM form_sections WHERE section_id = '"
            . $this->db->clean($sectionID) . "' LIMIT 1"
        );

        if (!$result) {

            return false;

        }

        \Analog::info(
            $_SESSION['user_identifier'] . " deleted section(id:" . $sectionRecord['section_id'] .
            ") #" . $sectionRecord['section_order'] . " " .
            $sectionRecord['section_text'] . " from form(id:$formID) " . $formRecord['form_name']
        );

        $this->updateAuthor($formID, $_SESSION['user_identifier']);
        $this->sortSectionOrder($formID);
        return true;

    }

    /**
     * Delete all form sections
     *
     * @param int $id     Section identifier
     * @return bool status
     */
    public function deleteAllFormSections($id)
    {
        return $this->db->query(
           "DELETE FROM form_sections WHERE form_id = '"
           . $this->db->clean($id) . "'"
        );
    }

    /**
     * Sort section order
     *
     * @param int $id     Form identifier
     */
    public function sortSectionOrder($id)
    {

        $sectionsIDs = $this->db->query(
           "SELECT section_id FROM form_sections WHERE form_id = '"
           . $this->db->clean($id) . "' ORDER BY section_order"
        );

        $count = 0;
        while ($formSection = mysqli_fetch_assoc($sectionsIDs)) {

            $count++;
            $this->db->query(
              "UPDATE form_sections SET section_order = '$count' " .
              "WHERE section_id = '" . $formSection['section_id'] . "'"
            );

        }

    }

    /**
     * Update form section
     *
     * @param int $id                 ID number for a form
     * @param int $order              Station order in form
     * @param string $title           Section title
     * @param int $competence         ID number for the competency
     *
     * @param int $sectionFeedback    Feedback enabled
     * @param int $feedbackRequired   Feedback required
     * @param int $internalFeedback   Internal feedback only (not released to students)
     *
     * @return bool success true|false
     */
    public function updateFormSection($id, $settings)
    {

        $settingSan = array_map(
            "\OMIS\Database\CoreDB::clean",
            array_values($settings)
        );

        list(
            $order, $title, $competence, $sectionFeedback, $itemFeedback,
            $feedbackRequired, $internalFeedback, $selfAssess
        ) = $settingSan;

        $selfAssessSql = $selfAssess === null ? " " : ", self_assessment = '$selfAssess' ";

        $success = $this->db->query(
            "UPDATE form_sections SET " .
            "competence_id = '$competence', " .
            "section_text = '$title', " .
            "section_feedback = '$sectionFeedback', " .
            "item_feedback = '$itemFeedback', " .
            "feedback_required = '$feedbackRequired', " .
            "internal_feedback_only = '$internalFeedback'" .
            $selfAssessSql .
            "WHERE section_id = '" . $this->db->clean($id) . "'"
        );

        if (!$success) {

            return false;

        }

        $this->logSectionUpdate($id);
        $this->updateAuthor(
              $this->getSectionFormID($id),
              $_SESSION['user_identifier']
        );
        $this->reOrderFormSections($id, $settings['order']);

        return true;

    }

    /**
     * Log item update
     *
     * @param int $id     Section Identifier
     */
    private function logSectionUpdate($id)
    {

        $section = $this->db->fetch_row(
            $this->getFormSection($id)
        );

        $form = $this->getForm($section['form_id']);

        \Analog::info(
            $_SESSION['user_identifier'] . " updated section(id:$id) #" .
            $section['section_order'] . " " . $section['section_text'] . " " .
            "in form(id:" . $section['form_id'] . ") " . $form['form_name']
        );

    }

    /**
     * Get section form id
     */
    public function getSectionFormID($id)
    {

       return CoreDB::single_result(
          $this->db->query(
             "SELECT form_id FROM form_sections WHERE section_id = '" .
             $this->db->clean($id) . "'"
          )
       );

    }

    /**
    * Re-sort form sections
    *
    * @param bool $id      Section ID number
    * @param bool $order   Order number
    *
    * @return bool FALSE on failure, true on success
    */
    public function reOrderFormSections($id, $order)
    {

        $formID = $this->getSectionFormID($id);
        $sectionIDsDb = $this->db->query(
            "SELECT section_id FROM form_sections " .
            "WHERE form_id = '$formID' ORDER BY section_order"
        );

        $count = mysqli_num_rows($sectionIDsDb);

        if (!($order > $count)) {

            $sectionIDsArray = [];
            while ($rw = mysqli_fetch_assoc($sectionIDsDb)) {

                if ($rw['section_id'] != $id) {
                    $sectionIDsArray[] = $rw['section_id'];
                }

            }

            array_splice($sectionIDsArray, $order - 1, 0, $id);

            $newOrder = 1;
            for ($i = 0; $i < sizeof($sectionIDsArray); $i++) {

                $sql = "UPDATE form_sections SET section_order = '$newOrder' " .
                       "WHERE section_id = '" . $sectionIDsArray[$i] . "'";
                $this->db->query($sql);
                $newOrder++;

            }

        }

    }

    /**
     * Update form section order
     *
     * @param int $id     Section identifier
     * @param int $order  Order number
     * @return bool       Status
     */
    public function updateFormSectionOrder($id, $order)
    {

        return $this->db->query(
          "UPDATE form_sections SET section_order = '" . $this->db->clean($order) . "' " .
          "WHERE section_id = '" . $this->db->clean($id) . "'"
        );

    }

    /**
     * Does form section exist
     *
     * @param int $id     Section identifier
     * @return bool       Status
     */
    public function doesFormSectionExist($id)
    {

        $sql = "SELECT COUNT(*) FROM form_sections WHERE section_id = '"
             . $this->db->clean($id) . "'";
        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    /**
     * Get form section count
     *
     * @param int $id     Section identifier
     * @return int        Number of sections
     */
    public function getFormSectionCount($id)
    {

        $sql = "SELECT COUNT(section_order) FROM form_sections WHERE form_id = '"
             . $this->db->clean($id) . "' ORDER BY section_order";
        return CoreDB::single_result($this->db->query($sql));

    }

    /**
     * Does section have grs item
     * @param int $sectionID
     * @return bool true|false
     */
    public function doesSectionHaveScaleItem($sectionID)
    {
        $sql = "SELECT COUNT(*) FROM section_items "
             . "WHERE grs = 1 AND section_items.section_id = '"
             . $this->db->clean($sectionID) . "'";
        return (CoreDB::single_result($this->db->query($sql)) > 0);
    }

    //Get Form Section Possible Marks
    public function getSectionPossibleMarks($section_id)
    {
        $items = $this->getSectionItems([$section_id]);
        $mark = 0;
        while ($item = mysqli_fetch_assoc($items)) {

            if (in_array($item['type'], ['radiotext', 'checkbox'])) {
                continue;
            }

            $mark += max($item['lowest_value'], $item['highest_value']);

        }
        return $mark;
    }

    /* Get Competencies
     *
     * @param string $competenceType The competence type (optional)
     * @param bool $toArray Convert resultset to array TRUE|FALSE
     *
     * @return mysqli_result|bool FALSE on failure, mysqli_result on success.
     */

    public function getCompetencies($competenceType = null, $toArray = false)
    {

        $competenceType = $this->db->clean($competenceType);

        if ($competenceType != null) {

            $typeSql = "WHERE competence_type = '$competenceType' ";

        } else {

            $typeSql = "";

        }

        $sql = "SELECT * "
             . "FROM competencies "
             . $typeSql
             . "ORDER BY competence_desc";

        $mysqliResult = $this->db->query($sql);

        if ($toArray) {

            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['competence_type'], true);

        } else {

            return $mysqliResult;

        }

    }

    // Does competence exist
    public function doesCompetenceExist($id)
    {

        $sql = "SELECT COUNT(*) FROM competencies WHERE competence_id = '"
             . $this->db->clean($id) . "'";
        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    /**
     * Get item record
     * @param bool $id Identifier number of the item
     * @return mysqli_result
     */
    public function getItem($id)
    {

        $idSan = $this->db->clean($id);
        $sql = "SELECT item_id, section_id, text, item_order, type, "
             . "highest_value, lowest_value, grs, weighted_value "
             . "FROM section_items WHERE item_id = '$idSan'";
        return $this->db->query($sql);

    }

    /**
     * Get all of the items within form sections, ordered as they
     * should be on the station form
     *
     * @param array $sectionIDs array of ID numbers for the form sections
     * @param bool $toArray Convert resultset to array
     * @param array $indices list of indices to group by
     * @return mysqli_result|array|bool FALSE if the query fails, otherwise a mysqli_result|array
     * containing the section's items.
     */
    public function getSectionItems($sectionIDs, $toArray = false, $indices = null)
    {

        $idList = array_map("\OMIS\Database\CoreDB::clean", $sectionIDs);

        $idsString = implode(', ', $idList);
        $sql = "SELECT * FROM section_items "
                . "WHERE section_id IN ($idsString) "
                . "GROUP BY section_id, section_items.item_id "
                . "ORDER BY section_id, section_items.item_order";

        $mysqliResult = $this->db->query($sql);

        if (($mysqliResult !== \FALSE) && $toArray) {

            if ($indices == null) {

                return CoreDB::into_array($mysqliResult);

            } else {

                $resultArray = CoreDB::into_array($mysqliResult);
                return CoreDB::group($resultArray, $indices, true, true);

            }

        } else {

            return $mysqliResult;

        }
    }

    /**
     * Insert item (DC: Needs refactoring)
     *
     * @param int $id
     * @param string $desc
     * @param string $type
     * @param double $highest
     * @param double $lowest
     * @param int $order
     * @param int $grs
     * @param int $critical
     * @param bool $sort
     * @return bool status
     */
    public function insertItem($id, $desc, $type, $highest, $lowest, $order = 0, $grs = 0, $critical = 0, $sort = false,$weightedVal = 1)
    {

        $id = $this->db->clean($id);
        $desc = $this->db->clean($desc);
        $type = $this->db->clean($type);
        $highest = $this->db->clean($highest);
        $lowest = $this->db->clean($lowest);
        $grs = $this->db->clean($grs);
        $critical = $this->db->clean($critical);
        $weightedVal = $this->db->clean($weightedVal);
        $itemID = false;

        $sql = "INSERT INTO section_items (section_id, text, "
             . "item_order, type, highest_value, lowest_value, "
             . "grs, critical, weighted_value) VALUES ('$id', '$desc', '" . $this->db->clean($order)
             . "', '$type', '$highest', '$lowest', '$grs', '$critical', '$weightedVal')";

        if ($this->db->query($sql)) {

            $itemID = $this->db->inserted_id();
            $this->logItem($itemID, $id, $order, 'added');
            $this->updateAuthor(
                 $this->getSectionFormID($id),
                 $_SESSION['user_identifier']
            );

            // Reorder Items
            if ($sort) {

                $this->updateItemOrder($itemID, $order);

            }

        }

        return $itemID;

    }

    /**
     *  Update section item
     *
     * @param int $id              id number for item
     * @param int $order           order number for item
     * @param string $type         item type
     * @param int $highest         highest range value
     * @param int $lowest          lowest range value
     * @param string $desc         item description
     * @param int $grs           item grs type > 0
     * @param int $itemweight      item weighting value default = 1
     * @param bool $sort           re-order item order true|false
     * @param bool $weighting      weighting per item true/false
     * @return bool
     */
    public function updateItem($id, $order, $type, $highest, $lowest, $desc, $grs = null, $sort = false, $itemWeight)
    {

        $type = $this->db->clean($type);
        $highest = $this->db->clean($highest);
        $lowest = $this->db->clean($lowest);
        $desc = $this->db->clean($desc);
        $grs = $this->db->clean($grs);
        $grsClause = $grs === null ? " " : ", grs = '$grs' ";
        $itemWeight = $this->db->clean($itemWeight);

        $sql = "UPDATE section_items SET item_order = '" . $this->db->clean($order) . "', text = '$desc', "
             . "type = '$type', highest_value = '$highest', lowest_value = '$lowest', weighted_value = '$itemWeight'"
             . $grsClause
             . "WHERE item_id = '" . $this->db->clean($id) . "'";

        if ($this->db->query($sql)) {

            $sectionID = $this->getSectionIDByItem($id);
            $this->logItem($id, $sectionID, $order, 'updated');

            $this->updateAuthor(
                  $this->getSectionFormID($sectionID),
                  $_SESSION['user_identifier']
            );

            if ($sort) {

                $this->updateItemOrder($id, $order);

            }

        } else {

            return false;

        }

    }

    /**
     * Update item basic
     *
     * @param int $id
     * @param string $desc
     * @param int $order
     * @return boolean
     */
    public function updateItemBasic($id, $desc, $order)
    {

        $sql = "UPDATE section_items SET text = '" . $this->db->clean($desc) . "' "
             . "WHERE item_id = '$id'";

        if ($this->db->query($sql)) {

            $sectionID = $this->getSectionIDByItem($id);
            $this->logItem($id, $sectionID, $order, 'updated');

            $this->updateAuthor(
                  $this->getSectionFormID($sectionID),
                  $_SESSION['user_identifier']
            );

            return $this->updateItemOrder($id, $order);

        }

       return false;

    }

    /**
     * Log item add/update
     *
     * @param int $id          Item ID
     * @param int $order       Item Order
     * @param int $mode  (added, updated, deleted)
     */
    private function logItem($id, $sectionID, $order, $mode)
    {

        $section = $this->db->fetch_row(
            $this->getFormSection($sectionID)
        );

        $form = $this->getForm($section['form_id']);

        \Analog::info(
            $_SESSION['user_identifier'] . " $mode " .
            "item(id:$id) #$order in section(id:$sectionID) #" .
            $section['section_order'] . " " .
            $section['section_text'] . " " .
            "in form(id:" . $section['form_id'] . ") " . $form['form_name']
        );

    }

    // Update item order
    public function updateItemOrder($id, $order)
    {

        $sectionID = $this->getSectionIDByItem($id);
        $itemsDB = $this->getItemIDsOrder($sectionID);
        $count = mysqli_num_rows($itemsDB);

        if ($order > $count) {

            $highestItemNumber = $this->getHighestItemNumber($sectionID);
            $highestItemNumber++;
            $this->db->query(
               "UPDATE section_items SET item_order = '$highestItemNumber' " .
               "WHERE item_id = '" . $this->db->clean($id) . "'"
            );

        } else {

            // Put into array
            $itemsArray = [];
            while ($item = mysqli_fetch_assoc($itemsDB)) {

                if ($item['item_id'] != $id) {

                    $itemsArray[] = $item['item_id'];

                }
            }

            array_splice($itemsArray, $order - 1, 0, $id);

            $newOrder = 1;
            for ($i = 0; $i < sizeof($itemsArray); $i++) {

                $this->db->query(
                    "UPDATE section_items SET item_order = '$newOrder' " .
                    "WHERE item_id = '" . $itemsArray[$i] . "'"
                );
                $newOrder++;

            }

        }

    }

    // Update section item order
    public function updateSectionItemsOrder($id)
    {

        $itemsDB = $this->getItemIDsOrder($id);
        $count = mysqli_num_rows($itemsDB);

        if ($count > 0) {

            $newOrder = 1;
            while ($item = mysqli_fetch_assoc($itemsDB)) {

                $this->db->query(
                   "UPDATE section_items SET item_order = '$newOrder' " .
                   "WHERE item_id = '" . $item['item_id'] . "'"
                );

                $newOrder++;

            }

        }

    }

    // Get item IDS and Order
    public function getItemIDsOrder($sectionID)
    {

        return $this->db->query(
          "SELECT item_id FROM section_items WHERE section_id = '" .
          $this->db->clean($sectionID) . "' ORDER BY item_order"
        );

    }

    /**
     * Delete items by section
     *
     * @param int[] $items        List of item identifiers
     * @param int $id             Identifier for section
     * @return int delete count
     */
    public function deleteItems($items, $id)
    {

        $n = count($items);
        $i = 0;

        while ($i < $n) {

            $itemID = $this->db->clean($items[$i]);

            $sql = "DELETE FROM section_items
                    WHERE section_id = '" . $this->db->clean($id) . "'
                    AND item_id = '$itemID' LIMIT 1";

            $item = $this->db->fetch_row(
                $this->getItem($itemID)
            );

            if ($this->db->query($sql)) {

                $this->logItem($itemID, $id, $item['item_order'], 'deleted');
                $i++;

            }

        }

        if ($i > 0) {

            $this->updateAuthor(
                $this->getSectionFormID($id),
                $_SESSION['user_identifier']
            );

            $this->updateSectionItemsOrder($id);

            $this->reCalcFormTotal(
               $this->getSectionFormID($id)
            );

        }

        return $i;

    }

    // Does item exist
    public function doesItemExist($id)
    {

        $sql = "SELECT COUNT(*) FROM section_items WHERE item_id = '"
                . $this->db->clean($id) . "'";
        $count = CoreDB::single_result($this->db->query($sql));
        return ($count > 0);

    }

    // Get form section ID by item
    public function getSectionIDByItem($id)
    {

        $sql = "SELECT section_id FROM section_items WHERE item_id = '"
             . $this->db->clean($id) . "'";
        return CoreDB::single_result($this->db->query($sql));

    }

    // Get form section count
    public function getSectionItemCount($id)
    {

        $sql = "SELECT COUNT(item_id) as count FROM section_items "
             . "WHERE section_id = '" . $this->db->clean($id) . "'";
        return CoreDB::single_result($this->db->query($sql));

    }

    // Get form section grs item
    public function getSectionScaleItem($id)
    {

        $sql = "SELECT * FROM section_items "
             . "WHERE section_items.section_id = '"
             . $this->db->clean($id) . "' "
             . "AND section_items.grs = 1";

        return $this->db->query($sql);

    }

    // Does form section have items
    public function doesSectionHaveItems($sectionID)
    {

        $sql = "SELECT COUNT(*) FROM section_items WHERE section_items.section_id = '"
                . $this->db->clean($sectionID) . "'";
        return (CoreDB::single_result($this->db->query($sql)) > 0);

    }

    // Get highest Item Number per Form Section
    public function getHighestItemNumber($sectionID)
    {

        return CoreDB::single_result($this->db->query(
           "SELECT MAX(item_order) as max FROM section_items WHERE section_id = '" .
           $this->db->clean($sectionID) . "'"
        ));

    }

    /**
     * Get form items
     *
     * @param int $id               Number identifier for form
     * @param bool $array           Return data as array
     * @param bool $idIndex         Identifiers as index
     * @param bool $numericItems    Numeric items only
     * @param bool $excludeGrs      Exclude grs data
     * @return bool (status)
     */
    public function getFormItems($id, $array = false, $idIndex = false, $numericItems = false, $excludeScales = false)
    {

        $numClause = $numericItems ? " AND type NOT IN ('radiotext', 'checkbox') " : " ";
        $grsClause = $excludeScales ? " AND grs = '0' " : ' ';

        $id = $this->db->clean($id);

        $sql = "SELECT * FROM section_items INNER JOIN form_sections USING(section_id) "
             . "WHERE form_id = '$id'" . $numClause . $grsClause
             . "GROUP BY section_items.item_id ORDER BY section_order, item_order";

        $mysqliResult = $this->db->query($sql);

        if ($array && $idIndex) {

            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['section_id'], true);

        } elseif ($array) {

            return CoreDB::into_array($mysqliResult);

        } else {

            return $mysqliResult;

        }

    }

    /**
     * Clear assessment form format
     * @param int $id
     * @return int update count
     */
    public function clearItemsFormat($id)
    {

        $idSan = $this->db->clean($id);
        $updateCount = 0;

        $sql = "SELECT item_id, text FROM section_items INNER JOIN form_sections "
             . "USING(section_id) WHERE form_id = '$idSan'";

        $resultset = $this->db->query($sql);

        while ($item = mysqli_fetch_assoc($resultset)) {

            $newText = $this->db->clean(
               strip_only($item['text'],
               ['bold', 'span', 'strong']
            ));

            $sql = "UPDATE section_items SET text = '" . $newText . "' "
                 . "WHERE item_id = '" . $item['item_id'] . "'";

            if ($this->db->query($sql)) {

                $updateCount++;

            }

        }

        if ($updateCount > 0) {

          $this->updateAuthor($id, $_SESSION['user_identifier']);

        }

        return $updateCount;

    }

    /**
     * Does form have items
     * @param int Number identifier for form
     * @return bool status
     */
    public function doesFormHaveItems($id)
    {

      return (bool) CoreDB::single_result(
        $this->db->query(
           "SELECT IF(COUNT(section_items.item_id) > 0, 1, 0) AS has
            FROM section_items
            INNER JOIN form_sections USING(section_id)
            WHERE form_id = '" . $this->db->clean($id) . "'"
      ));

    }

    /**
     * Does form have content (at least sections)
     * @param int Number identifier for form
     * @return bool status
     */
    public function hasContent($id)
    {

      return (bool) CoreDB::single_result(
        $this->db->query(
          "SELECT IF(COUNT(*) > 0, 1, 0) AS has FROM form_sections
           WHERE form_id = '" . $this->db->clean($id) . "'"
      ));

    }

    /**
     * Does form have weighted items
     * @param int $id           Number identifier for form
     * @param int $sectionID    Number identifier for section (optional)
     * @return bool status
     */
    public function hasWeightedItems($id, $sectionID = null)
    {

        $filterSection = ($sectionID === null) ? "" :
            " AND section_id = '{$this->db->clean($sectionID)}'";

        return (bool) CoreDB::single_result(
            $this->db->query(
        "SELECT IF(COUNT(*) > 0, 1, 0) AS has_weighting 
                FROM form_sections 
                INNER JOIN section_items USING(section_id) 
                WHERE form_id = '{$this->db->clean($id)}' 
                  $filterSection AND weighted_value != 1"
        ));

    }

    /**
     * Get form item count
     */
    public function getFormItemCount($id)
    {

        $sql = "SELECT COUNT(item_id) AS tCOUNT FROM section_items, form_sections "
             . "WHERE form_id = '" . $this->db->clean($id) . "' "
             . "AND section_items.section_id = form_sections.section_id";

        return CoreDB::single_result($this->db->query($sql));

    }

    /**
     * Update item option
     *
     * @param int $id             ID number for option
     * @param string $descriptor  Option descriptor
     * @param string $value       Option value (optional)
     *
     * @return mysqli_result|FALSE on failure, array otherwise.
     */
    public function updateOption($id, $descriptor, $value = null)
    {

        $valueSql = ($value === null) ? "" :
         ", option_value = '" . $this->db->clean($value) . "'";

        return $this->db->query(
            "UPDATE item_options SET descriptor = '" .
            $this->db->clean($descriptor) . "'$valueSql " .
            "WHERE item_id = '" . $this->db->clean($id) . "'"
        );

    }

    /**
     * Add option to item
     *
     * @param int $id              ID number for item record
     * @param int $order           Order number
     * @param string $descriptor   Option label
     * @param string $value        Option value
     * @param int $flag            Option flag
     * @return mysqli_result|FALSE on failure, array otherwise.
     */
    public function insertOption($id, $order, $descriptor, $value = 'value', $flag = 0)
    {

        $idSan = $this->db->clean($id);
        $orderSan = $this->db->clean($order);
        $descriptorSan = $this->db->clean($descriptor);
        $valueSan = $this->db->clean($value);
        $flagSan = $this->db->clean($flag);

        $sql = "INSERT INTO item_options "
             . "(item_id, option_order, descriptor, option_value, flag) "
             . " VALUES "
             . "('$idSan', '$orderSan', '$descriptorSan', '$valueSan', '$flagSan')";

        return [
            $this->db->query($sql),
            $this->db->inserted_id()
        ];

    }

    /**
     * Update radio options
     */
    public function updateRadioOptions($itemID, $radioFields, $updateValues = true)
    {

        $options = $this->getItemOptions($itemID);
        $optionsUpdated = 0;
        $index = 0;
        $valueClause = "";

        while ($option = mysqli_fetch_assoc($options)) {
            $optionID = $option['option_id'];

            // Update values if specified
            if ($updateValues) {
                $valueClause = "option_value = '" . $this->db->clean($radioFields[$index][0]) . "',";
            }

            $text = $this->db->clean($radioFields[$index][1]);
            $flag = $this->db->clean($radioFields[$index][2]);

            $sql = "UPDATE item_options SET " . $valueClause . " descriptor = '"
                 . $text . "', flag = $flag WHERE option_id = '" . $optionID . "'";

            if ($this->db->query($sql)) {
                $optionsUpdated++;
            }

            $index++;
        }

        return $optionsUpdated;

    }

    /**
     * Insert radio options
     */
    public function insertRadioOptions($itemID, $radioFields)
    {

        $itemID = $this->db->clean($itemID);
        $numOfFields = sizeof($radioFields);

        $sql = "INSERT INTO item_options (item_id, option_order, option_value, "
                . "descriptor, flag) VALUES ";
        $optionOrder = 0;

        foreach ($radioFields as $optionArray) {
            $optionOrder++;

            $optionValue = $this->db->clean($optionArray[0]);
            $optionText = $this->db->clean($optionArray[1]);
            $optionFlag = $this->db->clean($optionArray[2]);

            $sql .= "('" . $itemID . "', '" . $optionOrder . "', '"
                 . $optionValue . "', '" . $optionText . "', $optionFlag),";

            //If last set of values then replace "'" with ";"
            if ($numOfFields == $optionOrder) {
                $sql = substr_replace($sql, ';', -1);
            }
        }

        return $this->db->query($sql);

    }

    /**
     * Get item option record
     * @param int $id  ID number for option record.
     * @return bool|array FALSE on failure, array otherwise.
     */
    public function getOption($id)
    {

        $sql = "SELECT * FROM item_options WHERE option_id = '"
             . $this->db->clean($id) . "'";
        return $this->db->fetch_row($this->db->query($sql));

    }

    // Delete item options
    public function deleteItemOptions($id)
    {

        return $this->db->query(
          "DELETE FROM item_options WHERE item_id = '" .
          $this->db->clean($id) . "'"
        );

    }

    // Get item option count
    public function getItemOptionCount($id)
    {

        $sql = "SELECT COUNT(*) AS COUNT FROM item_options WHERE item_id = '"
             . $this->db->clean($id) . "'";
        return CoreDB::single_result($this->db->query($sql));

    }

    /**
     * Get a list of all options for the selected form item, in the order they
     * should be displayed
     *
     * @param int $itemID Unique ID number for the form item.
     * @param bool $toArray Convert resultset to array.
     * @return mysqli_result|bool|array FALSE on failure, mysqli_result|array otherwise.
     */
    public function getItemOptions($itemID, $toArray = false)
    {

        $sql = "SELECT option_id, item_id, option_value, flag,"
                . "descriptor, option_order FROM item_options "
                . "WHERE item_id = '" . $this->db->clean($itemID) . "' "
                . "ORDER BY option_order";

        $mysqliResult = $this->db->query($sql);

        if ($toArray) {

            return CoreDB::into_array($mysqliResult);

        } else {

            return $mysqliResult;

        }

    }

    // Get item option ID (choosen)
    public function getItemOptionID($id)
    {

        $sql = "SELECT option_id FROM item_options WHERE item_id = '"
             . $this->db->clean($id) . "' LIMIT 1";

        return CoreDB::single_result($this->db->query($sql));

    }

    // Get form item options
    public function getFormItemOptions($id)
    {

        $returnData = [];
        $sql = "SELECT section_id, section_order, item_order, item_id, " .
               "option_id, option_order, option_value, descriptor " .
               "FROM (item_options " .
               "INNER JOIN section_items USING(item_id)) " .
               "INNER JOIN form_sections USING(section_id) " .
               "WHERE form_id = '" . $this->db->clean($id) . "' " .
               "GROUP BY option_id " .
               "ORDER BY section_order, item_order, option_order";

        $mysqliResult = $this->db->query($sql);

        if ($mysqliResult) {

            while ($res = $this->db->fetch_row($mysqliResult)) {

                setA($returnData, $res['section_id'], []);
                setA($returnData[$res['section_id']], $res['item_id'], []);

                $returnData[$res['section_id']][$res['item_id']][] = [
                    'option_id' => $res['option_id'],
                    'option_order' => $res['option_order'],
                    'option_value' => $res['option_value'],
                    'descriptor' => $res['descriptor']
                ];

            }

        }

        return $returnData;

    }

    // Get highest answer option count for form
    public function getHighestAnswerOptionCount($id)
    {

        $sql = "SELECT MAX(option_order) FROM (form_sections " .
               "INNER JOIN section_items USING(section_id)) " .
               "INNER JOIN item_options USING(item_id) " .
               "WHERE form_sections.form_id = '" . $this->db->clean($id) . "' " .
               "AND (section_items.type = 'radio' " .
               "OR section_items.type = 'radiotext')";

        return CoreDB::single_result($this->db->query($sql));

    }

    // Get rating scales defaults
    public function getRatingScaleDefault($iso)
    {

        $sql = "SELECT * FROM rating_scale_defaults WHERE rsd_iso = '$iso'";
        return $this->db->fetch_row($this->db->query($sql));

    }

    /**
     * Get rating scale types
     * @param integer $id retrieve specific GRS record
     * returns array
     */
    public function getRatingScaleTypes($id = null)
    {

        $sql = "SELECT * FROM rating_scale_types";

        if (!is_null($id)) {

            $sql .= " WHERE scale_type_id = '" . $this->db->clean($id) . "'";

        }

        $resultSet = $this->db->query($sql);

        // Return single record
        if (!is_null($id)) {

            $returnResult = $this->db->fetch_row($resultSet);


        // Return all records
        } else {

            $returnResult = CoreDB::into_array($resultSet);

        }

        return $returnResult;

    }

    /**
     * Insert new rating scale type
     *
     * @param int $id
     * @param string $name
     * @param string $colour
     * @return boolean true|false
     */
    public function insertRatingScaleType($id, $name, $colour)
    {

        $id = $this->db->clean($id);
        $name = $this->db->clean($name);
        $colour = $this->db->clean($colour);

        $sql = "INSERT INTO rating_scale_types " .
               "(scale_type_id, scale_type_name, scale_type_colour) " .
               "VALUES ('$id', '$name', '$colour')";

        return $this->db->query($sql);

    }

    /**
     * Update rating scale type
     *
     * @param int $id
     * @param int $typeField
     * @param string $name
     * @param string $colour
     * @return boolean true|false
     */
    public function updateRatingScaleType($id, $typeField, $name, $colour)
    {

        $id = $this->db->clean($id);
        $typeField = $this->db->clean($typeField);
        $name = $this->db->clean($name);
        $colour = $this->db->clean($colour);
        $scaleIDUpdate = "";

        // Scale type ID has changed
        if ($id !== $typeField) {

            $scaleIDUpdate = "scale_type_id = '$typeField', ";

        }

        $sql = "UPDATE rating_scale_types SET " .
               $scaleIDUpdate .
               "scale_type_name = '$name', " .
               "scale_type_colour = '$colour' " .
               "WHERE scale_type_id = '$id'";

        return $this->db->query($sql);

    }

    /**
     * Delete rating scale types
     *
     * @param array $ids
     * @return boolean true|false
     */
    public function deleteRatingScaleTypes($ids)
    {

        // Assemble Multi Query string
        $sql = "";
        foreach ($ids as $id) {

            $sql .= "DELETE FROM rating_scale_types " .
                    "WHERE scale_type_id = '" . $this->db->clean($id) . "'; ";

        }

        // Delete and return
        return ((strlen($sql) > 0) ? $this->db->multi_query($sql) : false);

    }

    /**
     * Get Rating Scale Values
     *
     * @param integer $type scale type ID
     * @param integer $valueID retrieve specific GRS value record
     * @param boolean $includeTypes if set to true the fields from the 'rating_scale_types' table are included
     * @param string $index specify associative array index for return array
     * returns array
     */
    public function getRatingScaleValues($type = null, $valueID = null, $includeTypes = true, $index = null)
    {

        if ($includeTypes) {

            $typesSql = "INNER JOIN rating_scale_types USING (scale_type_id) ";
            $columns = "*";

        } else {

            $typesSql = "";
            $columns = "scale_value_id, scale_value, scale_value_description, "
                     . "scale_value_borderline, scale_value_fail";

        }

        $sql = "SELECT $columns FROM rating_scale_values $typesSql WHERE ";

        // Filter by Scale Type ID
        if (!is_null($type)) {

            $sql .= "scale_type_id = '" . $this->db->clean($type) . "'";

        }

        // Do we need an 'AND' ?
        if (!is_null($type) && !is_null($valueID)) {

            $sql .= " AND ";

        }

        // Filter by Scale Value ID (record ID)
        if (!is_null($valueID)) {

            $sql .= "scale_value_id = '" . $this->db->clean($valueID) . "'";

        }

        $mysqliResult = $this->db->query($sql);

        // Return single record
        if (!is_null($valueID)) {

            $returnResult = $this->db->fetch_row($mysqliResult);

        }

        // Return all records
        elseif (is_null($index)) {

            $returnResult = CoreDB::into_array($mysqliResult);

        // Return all records with a specific field as an index for each record
        } else {

            $resultArray = CoreDB::into_array($mysqliResult);
            $returnResult = CoreDB::group($resultArray, [$index], true, true);

        }

        return $returnResult;

    }

    /**
     * Insert new GRS value
     *
     * @param integer $type
     * @param string $value
     * @param string $desc
     * @param boolean $borderline
     * @param boolean $fail
     * @return boolean true|false
     */
    public function insertRatingScaleValue($type, $value, $desc, $borderline, $fail)
    {

        list($id, $value, $desc, $borderline, $fail)
                = array_map("\OMIS\Database\CoreDB::clean", func_get_args());

        $sql = "INSERT INTO rating_scale_values "
             . "(scale_type_id, scale_value, scale_value_description, "
             . "scale_value_borderline, scale_value_fail) "
             . "VALUES ('$id', '$value', '$desc', '".(int)$borderline."', '".(int)$fail."')";

        return $this->db->query($sql);

    }

    /**
     * Update Rating Scale Value
     *
     * @param integer $id
     * @param integer $value
     * @param string $desc
     * @param boolean $borderline
     * @param boolean $fail
     * @return boolean true|false
     */
    public function updateRatingScaleValue($id, $value, $desc, $borderline, $fail)
    {

        list($id, $value, $desc, $borderline, $fail)
                = array_map("\OMIS\Database\CoreDB::clean", func_get_args());

        $sql = "UPDATE rating_scale_values SET "
             . "scale_value = '$value', "
             . "scale_value_description = '$desc', "
             . "scale_value_borderline = '" . (int) $borderline . "', "
             . "scale_value_fail = '" . (int) $fail . "' "
             . "WHERE scale_value_id = '$id'";

        return $this->db->query($sql);

    }

    /**
     * Delete rating scale values
     *
     * @param array $ids
     * @return boolean true|false
     */
    public function deleteRatingScaleValues($ids)
    {

        $sql = "";
        foreach ($ids as $id) {

            $sql .= "DELETE FROM rating_scale_values " .
                    "WHERE scale_value_id = '" . $this->db->clean($id) . "'; ";

        }

        // Delete and return
        return ((strlen($sql) > 0) ? $this->db->multi_query($sql) : false);

    }

    /**
     * Get linked exams
     *
     * @param int $id           Number identifier for form
     * @param int $currentExam  Number identifier for exam (sort to top)
     * @return mysqli_results|false
     */
    public function getLinkedExams($id, $currentExam = null)
    {

        $examTop = !is_null($currentExam) ?
           "exam_id = " . $this->db->clean($currentExam) . " DESC, " : "";

        return $this->db->query(
            "SELECT exams.* FROM exams
            INNER JOIN exam_sessions USING (exam_id)
            INNER JOIN session_stations USING (session_id)
            WHERE form_id= '" . $this->db->clean($id) . "'
            GROUP BY exam_id
            ORDER BY term_id DESC,
            $examTop
            exam_id DESC"
        );

    }

    /**
     * Form name is in use
     *
     * @param string $name           Form name to check
     * @param string $department     Department identifier
     * @param string $term           Academic term (year)
     * @return bool true|false
     */
    public function nameTaken($name, $department, $term) {

        return $this->db->fetch_row(
        $this->db->query(
          "SELECT IF(COUNT(*) > 0,1,0) AS exist, form_id
           FROM forms
           INNER JOIN form_department_term USING (form_id)
           WHERE form_name = '{$this->db->clean($name)}'
             AND dept_id = '{$this->db->clean($department)}'
             AND term_id = '{$this->db->clean($term)}'")
        );

    }


    /**
     * Form name already in use in session
     *
     * @param string $name    Form name to check
     * @param int $session    Session to check
     * @return bool true|false
     */
    public function nameTakenSession($name, $session) {

        $count = CoreDB::single_result(
        $this->db->query(
            "SELECT count(*) FROM forms
           INNER JOIN form_department_term USING (form_id)
           WHERE form_name = '{$this->db->clean($name)}' AND form_id IN
             (SELECT form_id
             FROM session_stations
             WHERE session_id = {$this->db->clean($session)})")
        );

        return ($count > 0);

    }

    protected function getTableName() { return 'forms'; }

    protected function getIds() { return ['form_id']; }

}
