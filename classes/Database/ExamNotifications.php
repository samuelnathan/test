<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * Database function calls relating to Exam Notifications
 */

namespace OMIS\Database;

class ExamNotifications
{
  // Core Database 'coreDB' reference
  private $db;
  private $lastNotificationID;
  
  // Constructor
  public function __construct($db)
  {
      $this->db = $db;
  }

  /**
   * Log Notification for exam
   * 
   * @param int $stationID       Unique Identifier for station record
   * @param string $studentID    Unique Identifier for student record
   * @param string $examinerID   Unique Identifier for examiner record
   * @param mixed[] $data        Flag/bit fields and settings
   */
  public function notify($stationID, $studentID, $examinerID, $data = [])
  {
    $stationSan = $this->db->clean($stationID);
    $studentSan = $this->db->clean($studentID);
    $examinerSan = is_null($examinerID) ? NULL : $this->db->clean($examinerID);
    $dataClause = $dataKeys = $dataValues = "";
    
    // Data clauses
    if (count($data) > 0) {
       $dataClause = " AND " . http_build_query($data, "", " AND ");
       $dataKeys = ", " . implode(",", array_keys($data));
       $dataValues = ", " . implode(",", array_values($data));
    }
    
    // Check for existing records
    $existing = $this->db->query(
       "SELECT id FROM exam_notifications WHERE station_id='$stationSan' " .
       "AND student_id='$studentSan' AND examiner_id='$examinerSan'$dataClause LIMIT 1"
    );

    if (mysqli_num_rows($existing) == 1) {
       $this->lastNotificationID = $this->db->fetch_row($existing)['id'];
       return true;
    }
    
    // Insert new record if we've got this far
    $status = $this->db->query(
       "INSERT INTO exam_notifications (station_id, student_id, examiner_id$dataKeys)" .
       "VALUES('$stationSan', '$studentSan', '$examinerSan'$dataValues)"
    );
    
    // Set last/new notfication ID
    $this->lastNotificationID = $this->db->inserted_id();
    return $status;
    
  }
  
  /**
   * Log SMS notification record
   * 
   * @param mixed[] $recipients   Array of unique identifiers and number for recipients
   * @param string $message       Message content
   * @return bool true|false      Status
   */
  public function logSMS($recipients, $message) 
  {
    
    $notificationID = $this->lastNotificationID;
    $messageSan = $this->db->clean($message);
    $success = false;
    
    foreach ($recipients as $recipient) {
         
       $idSan = $this->db->clean($recipient['user_id']);
       $numberSan = $this->db->clean($recipient['contact_number']);
       
       $success &= $this->db->query(
          "INSERT INTO exam_notifications_sms (notification_id, recipient_id, " .
          "sms_message, sms_number) VALUES('$notificationID', " .
          "'$idSan', '$messageSan', '$numberSan')"
       );
       
    }
    
    return $success;
    
  }
 
  /**
   * Get exam notifications/alerts
   * 
   * @param int[] $sessions  list of selected session identifiers
   * @param string $order     default 'DESC'
   */
  public function bySessions($sessions, $order = "DESC")
  {
    
    if (count($sessions) == 0) {
      return [];
    }
    
    return CoreDB::into_array(
       $this->db->query(
         "SELECT exam_notifications.*, session_stations.station_number 
          FROM exam_notifications 
          INNER JOIN session_stations USING (station_id) 
          WHERE session_id IN (" . implode(", ", $sessions) . ") 
          ORDER BY exam_notifications.updated_at $order"
     ));

  }

  /**
   * Drop exam notifications by group and station
   * 
   * @param int $group     Group identifier
   * @param int $station   Station identifier
   * @return bool true|false      Status
   */  
  public function dropByGroupStation($group, $station) 
  {
      $sql = "DELETE FROM exam_notifications WHERE station_id = '"
           . $this->db->clean($station) . "' AND student_id IN "
           . "(SELECT student_id FROM group_students WHERE group_id = '"
           . $this->db->clean($group) . "')";

      return $this->db->query($sql);
  }
  
  /**
   * Drop exam notifications by session and student
   * 
   * @param int $session      Session identifier
   * @param string $student   Student identifier
   * @return bool true|false      Status
   */  
  public function dropBySessionStudent($session, $student) 
  {
      $sql = "DELETE FROM exam_notifications WHERE student_id = '"
           . $this->db->clean($student) . "' AND station_id IN "
           . "(SELECT station_id FROM session_stations WHERE session_id = '"
           . $this->db->clean($session) . "')";

      return $this->db->query($sql);
  }
  
  /**
   * Drop exam notifications by result identifiers
   * 
   * @param int[] $ids         Student Result IDs
   * @param string $student    Student identifier
   * @return bool true|false   Status
   */  
  public function dropByResultIDs($ids, $student) 
  {
    
      if (empty($ids) || empty($student)) {
         return false;
      } else {
        $idsSan = array_map(function ($resultID) {
           return $this->db->clean($resultID);     
        }, $ids);
      }
    
      return $this->db->query(
        "DELETE FROM exam_notifications " .
        "WHERE student_id = '" . $this->db->clean($student) . "' " .
        "AND station_id IN (SELECT DISTINCT(station_id) FROM student_results " .
        "WHERE result_id IN (" . implode(", ", $idsSan) . ")) "
     );
      
  }
  

}