<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

namespace OMIS\Database;

class Competencies {
    
// Core Database 'coreDB' reference
    private $db;

    // Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }
    
    /**
     * Link competencies to an item
     * 
     * @param int $itemID
     * @param array $competencies
     * @return boolean success
     */
    public function linkToItem($itemID, $competencies) {
       $itemID = CoreDB::clean($itemID);
       
       // Remove old links (if any)
       $removeSql = "DELETE FROM item_competencies "
                   . "WHERE item_id = $itemID"; 
       $removeSuccess = $this->db->query($removeSql);
       
       $insertSql = "INSERT INTO item_competencies (item_id, competency_id) VALUES ";
       if ($removeSuccess && is_array($competencies) && count($competencies) > 0) {
          foreach ($competencies as $competencyID) {
             CoreDB::clean($competencyID);
             $insertSql .= "('$itemID', '$competencyID'),";
          }          
         return $this->db->query(chop($insertSql, ','));
       }
       return false;
    }
  
    /**
     * Get framework level ID
     * 
     * @param int $frameworkID
     * @return int framework level ID
     */    
    public function getFrameworkLevelID($frameworkID) {
        $frameworkIDSan = CoreDB::clean($frameworkID);
        $sql = "SELECT id " 
             . "FROM framework_level_treepaths " 
             . "INNER JOIN competency_framework_levels " 
             . "ON (framework_level_treepaths.ancestor = competency_framework_levels.id) "
             . "WHERE framework_level_treepaths.tree_length = 0 "
             . "AND competency_framework_levels.framework_id = $frameworkIDSan "
             . "LIMIT 1";
        return CoreDB::single_result($this->db->query($sql));
    }
    
    /**
     * Get list of competencies
     * 
     * @param int $frameworkID
     * @param int $ancestor
     * @param int $level 
     * @return mixed[][]  Array containing query results.
     */
    public function getList($frameworkID, $ancestor, $level) {
        
       $frameworkID = CoreDB::clean($frameworkID);
       $ancestor = CoreDB::clean($ancestor);
       $level = CoreDB::clean($level);
  
       $sql = "SELECT id, name, level_colour_code "
            . "FROM competency_framework_levels "
            . "INNER JOIN framework_level_treepaths "
            . "ON (competency_framework_levels.id = framework_level_treepaths.descendant) "     
            . "WHERE framework_id = $frameworkID "
            . "AND ancestor = $ancestor "
            . "AND tree_length = $level";
       
        return CoreDB::into_array($this->db->query($sql));
    }
 
    /**
     * Get Item Competencies
     * 
     * @param int $itemID
     * @return mixed[][]  Array containing query results.
     */
    public function getItemCompetencies($itemID) {
        
       $itemID = CoreDB::clean($itemID);
  
       $sql = "SELECT competency_id "
            . "FROM item_competencies "
            . "WHERE item_id = $itemID";
       
        return CoreDB::into_values($this->db->query($sql));
    }

    /**
     * Get Section item Competencies
     * 
     * @param int $sectionID
     * @return mixed[][]  Array containing query results.
     */
    public function getSectionItemCompetencies($sectionID) {
        
       $sectionID = CoreDB::clean($sectionID);
  
       $sql = "SELECT item_id, identifiers.identifier, names.name, names.level_colour_code "
            . "FROM (SELECT item_id, ancestor AS identifier "
            . "FROM ((section_items "
            . "INNER JOIN item_competencies USING(item_id)) "
            . "INNER JOIN competency_framework_levels "
            . "ON (item_competencies.competency_id = competency_framework_levels.id)) "
            . "INNER JOIN framework_level_treepaths "
            . "ON (competency_framework_levels.id = framework_level_treepaths.descendant) "
            . "WHERE section_id = $sectionID AND tree_length = 2) AS identifiers "
            . "INNER JOIN (SELECT id, name, level_colour_code "
            . "FROM competency_framework_levels) AS names "
            . "ON (identifiers.identifier = names.id) "
            . "GROUP BY item_id, identifier";

       $mysqliResults = CoreDB::into_array($this->db->query($sql));
       return CoreDB::group($mysqliResults, ['item_id'], true, false);
       
    }
    
    /**
     * Get Exam Competency results (level 1)
     * 
     * @param int[] $stationIDs
     * @param string $studentID
     * @return mixed[][]  Array containing query results.
     */
    public function getExamCompetencyResults($stationIDs, $studentID = null) {
   
        $studentIDSan = CoreDB::clean($studentID);
        $studentClause = ($studentID === null) ? "" : "AND student_results.student_id = '$studentIDSan' ";
        
        $sql = "SELECT competency_id AS id, scoresPerItem.name, scoresPerItem.level_colour_code, SUM(scoresPerItem.ItemScore) AS score, "
             . "SUM(scoresPerItem.ItemPossible) AS possible "
             . "FROM (SELECT competency_framework_levels.name, competency_framework_levels.level_colour_code, competency_id, SummedScores.* "
             . "FROM "
             . "((SELECT item_id, SUM(option_value) AS ItemScore, SUM(highest_value) AS ItemPossible "
             . "FROM (item_scores INNER JOIN section_items USING (item_id)) "
             . "INNER JOIN student_results USING (result_id) "
             . "LEFT JOIN station_examiners "
             . "ON (student_results.examiner_id = station_examiners.examiner_id) "
             . "AND (student_results.station_id = station_examiners.station_id) "
             . "WHERE student_results.station_id IN (" . implode(", ", $stationIDs) . ") AND grs = 0 "
             . "AND (scoring_weight IS NULL OR scoring_weight <> '0')  "
             . $studentClause
             . "GROUP BY item_id) AS SummedScores "
             . "INNER JOIN item_competencies USING (item_id)) "
             . "INNER JOIN competency_framework_levels "
             . "ON (item_competencies.competency_id = competency_framework_levels.id) "
             . "INNER JOIN framework_level_treepaths "
             . "ON (item_competencies.competency_id = framework_level_treepaths.descendant) "
             . "WHERE tree_length = 1) AS scoresPerItem "
             . "GROUP BY competency_id";
       
        return CoreDB::into_array($this->db->query($sql));
    }
    
    /**
     * Get Exam Competencies (level 1)
     * 
     * @param int[]       form ids
     * @return mixed[][]  Array containing query results.
     */    
    public function getExamCompetencies($formIDs) {
        
      $sql = "SELECT items.competency_id AS id, competency_framework_levels.name AS name, "
           . "competency_framework_levels.level_colour_code AS colour_code "
           . "FROM competency_framework_levels INNER JOIN "
           . "(SELECT item_id, competency_id "
            . "FROM item_competencies "
            . "INNER JOIN section_items USING(item_id) "
            . "INNER JOIN form_sections USING(section_id) "
            . "WHERE form_id IN (" . implode(", ", $formIDs) . ")) AS items "
            . "ON (competency_framework_levels.id = items.competency_id) "
            . "INNER JOIN framework_level_treepaths "
            . "ON (items.competency_id = framework_level_treepaths.descendant) "
            . "WHERE tree_length = 1 "
            . "GROUP BY competency_id";
       return CoreDB::into_array($this->db->query($sql)); 
    }
        
}


