<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * Database function calls relating to grade rules
 */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class GradeRules extends AbstractEntityDAO
 {

    const GRADE_VALUE_APPOINTABLE = 3;        # Appointable;
    const GRADE_VALUE_NON_APPOINTABLE = 4;    # Not Appointable;
    const GRADE_VALUE_PANEL_TO_DECIDE = 5;    # Panel to Decide;

    const GRADE_RULE_DEFAULT = 1;        # default
    const GRADE_RULE_THREE_TWOS = 2;     # no more than three scores of a 2
    const GRADE_RULE_TWO_TWOS = 3;       # no more than two scores of a 2
    const GRADE_RULE_SCORE_ONLY = 4;     # based on score only

    protected function getTableName() { return 'grade_rules'; }
    protected function getIds() { return ['grade_rule_id']; }

    /**
     * Get all grade rules
     * @return mixed[]
     */
    public function all() 
    {
    
       return CoreDB::into_array(
          $this->db->query("SELECT * FROM grade_rules")  
       );
       
    }

    /**
     * Calculate student grades by groups
     * @param int $id       identifier numbers for group
     * @param int $session  identifier numbers for session
     */
    public function calcByGroup($id, $session)
    {

        $students = $this->db->groups->getStudentsByGroup($id, false);

        foreach ($students as $student) {

            $this->calc($session, $student['student_id']);

        }

    }

    /**
     * Calculate grade for student at a particular session
     * @param int $session      session identifier number
     * @param string $student   student identifier number
     * @return bool true|false  success
     */
    public function calc($session, $student) 
    {

       // No grade rule other than default applied, then abort!
       $sessionRecord = $this->db->sessions->getSession($session);

       if (empty($sessionRecord) || !$this->db->students->existsById($student) ||
           !($sessionRecord['grade_rule'] > self::GRADE_RULE_DEFAULT)) return;

       $grade = self::GRADE_VALUE_APPOINTABLE;
       $exam = $sessionRecord['exam_id'];

       $twoCount =  0;
       $scoreOfOne = false;
       $trigger = $itemsString = "";

       $this->db->startTransaction();

       $stations = CoreDB::into_array($this->db->stations->getStations(
            [$session], false, 'station_number',
            'form_name', false, true
       ));

       // Get scoresheet results 
       $results = CoreDB::into_array($this->db->results->bySession(
           $student, $session, false, null,
           true, true
       ));


        // capture gradeable results only
        $gradeableResults = array_filter($results, function($result) {

            return ($result['exclude_from_grading'] == 0);

        });


       /**
        * Get expected number of stations to complete and exclude observer scores
        */
       /*$expectedResultCount = array_reduce($stations, function($carry, $station) {

           return ($carry + $this->db->stations->getExaminersRequired($station['station_id']));

       });*/

       // Assumed that there 2 results per station for all grade/appoimntability rules
       $expectedResultCount = count($stations) * 2;

       /**
        * If the count between the number of stations and results
        * is inconsistent, then abort (all scoresheets are not complete)
        */
       if ($expectedResultCount != count($gradeableResults)) {

           $this->db->rollback();

           // Has grade, then reset
           if ($this->hasGrade($exam, $student)) {

               $this->resetGrade($exam, $student);
               \Analog::warning(
                   "system reset grade for student(id:$student) in exam(id:$exam) " .
                    "as number of results no longer match expected number"
               );

           }

           return;

       }

       // Tot up the values
       foreach ($gradeableResults as $result) {

          $itemScores = $this->db->results->getItemScoresPerResult($result['result_id']);

          while ($score = mysqli_fetch_assoc($itemScores)) {

              $itemsString .= $score['option_value'];

              if ($score['option_value'] == 1) $scoreOfOne = true;
              if ($score['option_value'] == 2) $twoCount++;

          }

       }

       // Check against the grade rules
       switch ($sessionRecord['grade_rule']) {
 
          case self::GRADE_RULE_THREE_TWOS:

                if ($scoreOfOne) {

                    $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                    $trigger = "score of one";

                } elseif ($twoCount >= 4) {

                    $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                    $trigger = "2 count ≥ 4";

                } elseif ($twoCount > 0) {

                    $grade = self::GRADE_VALUE_PANEL_TO_DECIDE;
                    $trigger = "2 count > 0";

                }

            break;

          case self::GRADE_RULE_TWO_TWOS:

               if ($scoreOfOne) {

                   $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                   $trigger = "score of one";

               } elseif ($twoCount >= 3) {

                   $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                   $trigger = "2 count ≥ 3";

               } elseif ($twoCount > 0) {

                   $grade = self::GRADE_VALUE_PANEL_TO_DECIDE;
                   $trigger = "2 count > 0";

               }

               break;

          case self::GRADE_RULE_SCORE_ONLY:

              $rawScore = array_sum(
                  array_column($gradeableResults, 'score')
              );

              $itemsString = $rawScore . $itemsString;

              if ($rawScore < 36) {

                  $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                  $trigger =  gettext('grade') . " " . gettext('forms')
                      . " total = $rawScore < min. 36";

              } elseif ($scoreOfOne) {

                  $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                  $trigger = "score of one";

              } elseif ($twoCount >= 3) {

                  $grade = self::GRADE_VALUE_NON_APPOINTABLE;
                  $trigger = "2 count ≥ 3";

              }


            break;                     
          default:
            // do nothing
          break;


       }

       // Only update if scoring criteria has changed on any form/scoresheet
       if ($this->getGradeItemsString($exam, $student) !== $itemsString) {

           $gradeRecord = $this->db->gradeValues->getById($grade);

           $this->updateGrade(
               $exam, $student, $gradeRecord['grade_value'],
               true, $trigger, $itemsString
           );

           $this->db->commit();

       } else {

           $this->db->rollback();

       }

       return true;
       
    }

    /**
     * Update grade for student
     * @param int $exam             exam identifier number
     * @param string $student       student identifier number
     * @param string $grade         appointability string
     * @param bool $system          is system update
     * @param string $trigger       rule triggered
     * @param string $itemsString   item scores string
     */
    public function updateGrade($exam, $student, $grade, $system = true, $trigger = "", $itemsString = null)
    {

        // Register student exam record (in if non-existent)
        $this->db->examdata->registerStudentData(
            [$student],
            $exam
        );

        $triggerSql = $system ? "NULL" : "'{$_SESSION['user_identifier']}'";
        $itemStringSql = $itemsString !== null ? ", grade_scores_string = '$itemsString'" : "";

        $updated = $this->db->query(
          "UPDATE student_exam_data SET grade = '$grade', 
            grade_author = $triggerSql, 
            grade_updated = '" . date("Y-m-d H:i:s") . "', 
            grade_trigger = '$trigger'$itemStringSql  
          WHERE student_id = '{$this->db->clean($student)}' 
          AND exam_id = '{$this->db->clean($exam)}'"
        );

        if ($updated) {

            $examRecord = $this->db->exams->getExam($exam);
            \Analog::info(
                ($system ? "system": $_SESSION['user_identifier']) .
                " determined grade: " . $grade . " for student: " . $student .
                " in exam (id:$exam) " . $examRecord['exam_name'] .
                (strlen($trigger) > 0 ? " rule triggered: " . $trigger : "") .
                ($system ? ", item scores string: " . $itemsString : "")
            );

        }

    }

    /**
     * Reset grade for student
     * @param int $exam         exam identifier number
     * @param string $student   student identifier number
     */
    public function resetGrade($exam, $student)
    {

        return $this->db->query(
            "UPDATE student_exam_data SET grade = '', grade_author = NULL, 
             grade_updated = '" . date("Y-m-d H:i:s") . "' 
             WHERE student_id = '{$this->db->clean($student)}' 
             AND exam_id = '{$this->db->clean($exam)}'"
        );

    }

     /**
      * Has grade
      * @param int $exam         exam identifier number
      * @param string $student   student identifier number
      * @return bool true|false  has grade
      */
     public function hasGrade($exam, $student)
     {

         $grade = CoreDB::single_result($this->db->query(
             "SELECT grade FROM student_exam_data 
             WHERE student_id = '{$this->db->clean($student)}' 
             AND exam_id = '{$this->db->clean($exam)}'"
         ));

         return (strlen($grade)  > 0);

     }

     /**
      *Get grade item scores string
      * @param int $exam         exam identifier number
      * @param string $student   student identifier number
      * @return string           items scores string
      */
     public function getGradeItemsString($exam, $student)
     {

         $result = CoreDB::single_result($this->db->query(
             "SELECT grade_scores_string FROM student_exam_data 
             WHERE student_id = '{$this->db->clean($student)}' 
             AND exam_id = '{$this->db->clean($exam)}'"
         ));

         return ($result === null ? "" : $result);

     }

 }

