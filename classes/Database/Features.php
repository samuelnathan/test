<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

namespace OMIS\Database;
use OMIS\Auth\Role as Role;

class Features
{

//Core Database 'coreDB' reference
private $db;

    //Constructor
    public function __construct($db)
    {
       $this->db = $db;
    }

    
    /**
     * Get All Omis Features [Updated 25/06/2014]
     * @returns array of features
     */
    public function getFeatures()
    {
        $query = "SELECT * FROM features";
        return CoreDB::into_array($this->db->query($query)); 
    }
    
   /**
    * Get All Features & Roles
    * @returns array of features and roles
    */
    public function getFeaturesandRoles()
    {
        $returnData = array();
        
        // Get features
        $featuresDB = $this->db->query('SELECT * FROM features'); 
        while ($feature = $this->db->fetch_row($featuresDB)) {
            
            // Feature ID
            $featureID = $feature['feature_id'];
            
            $returnData[$featureID] = $feature;

            // Get this feature's list of roles
            $featureRolesDB = $this->getFeatureRoles($featureID); 
           
            // Set Roles
            setA($returnData[$featureID], 'roles', array());
            while ($role = $this->db->fetch_row($featureRolesDB)) {
               $featureRoleID = $role['feature_role']; 
               $returnData[$featureID]['roles'][$featureRoleID] = $role['role_enabled'];
            }
        }
        return $returnData;
    }

   /**
    * Get roles for feature
    * @returns array of roles
    */
    public function getFeatureRoles($featureID = null)
    {
        $query = "SELECT * FROM feature_roles WHERE feature_role <> " . Role::USER_ROLE_SUPER_ADMIN;

        if ($featureID !== null) {
            $query .= " AND feature_id = '" . $this->db->clean($featureID) . "'";
        }

        return $this->db->query($query);
    }

   /**
    * Get feature groups
    * @returns array of groups
    */
    public function getFeatureGroups()
    {
        return CoreDB::into_array($this->db->query(
           "SELECT * FROM feature_groups " .
           "ORDER BY feature_group_name"
        ));
    }
    
    // Is Feature Allowed
    /**
     * Determines whether a user can access a particular named feature or features of Qpercom Observe.
     * Returns 1 for yes and 0 for no based on the user's role. Super admin user
     * always gets 1 (yes).
     * 
     * @param string[]|string $ids list of identifiers, can be string or array of strings
     * @param int    $role ID of role of user to check
     * @return mixed[] or int 1 = role has access, 0 = role does not have access (1 or many)
     */
    public function enabled($ids, $role)
    {

        /* If the user is a Super Admin, then it goes without saying that they
         * have unlimited access....
         */
        if (empty($ids) || empty($role)) {
            return 0;
        } else if ($role == Role::USER_ROLE_SUPER_ADMIN) {
            return is_array($ids) ? array_fill(0, sizeof($ids), 1) :  1;
        }
        
        $features = is_array($ids) ? $ids : [$ids];
        
        $sql = "SELECT role_enabled FROM feature_roles "
             . "WHERE feature_id IN ('" . implode("','", $features) .  "') "
             . "AND feature_role = '" . $this->db->clean($role) . "' "
             . "ORDER BY FIELD (feature_id, '" . implode("','", $features) .  "')";

        $results = $this->db->query($sql);
        
        return (is_array($ids)) ? CoreDB::into_values($results) :
          CoreDB::single_result($results);
        
    }

    /**
     * Update system wide features
     * 
     * @param int $featureRolePairings
     * @return boolean status
     */
    public function updateFeatures($featureRolePairings)
    {
        $userRolesCanLogin = (new \OMIS\Auth\RoleCategory(null, false, true))->role_ids;
        $featureIDs = [];
        $insertData = [];

        // Get current list of features
        $allFeatures = $this->getFeatures();
        
        // Loop through all features 
        foreach ($allFeatures as $eachFeature) {
      
          $featureID = $eachFeature['feature_id'];
          $featureIDs[] = "'$featureID'";
         
          // If we have feature roles to insert
          // $roleIDsEnable is a list of Role IDs that are supposed to
          // be allowed to access a particular feature.
          $roleIDsEnable = (isset($featureRolePairings[$featureID])) ?
                  $featureRolePairings[$featureID] : [];

          // Go through all of the roles 
          foreach ($userRolesCanLogin as $roleID) {
              $roleEnabled = ((in_array($roleID, $roleIDsEnable)) ? 1 : 0);
              $insertData[] = "('$featureID', $roleID, $roleEnabled)";
          }
          
        }
       
        // Remove all data related to the specified features...
        $resetFeatureRoleQuery = "DELETE FROM `feature_roles` WHERE " .
                                  "`feature_id` IN (" . implode(", ", $featureIDs) . ");";

        // If the delete query worked, re-insert the desired records.
        if ($this->db->query($resetFeatureRoleQuery) && sizeof($insertData) > 0) {
            $insertQuery = "INSERT INTO feature_roles (feature_id, " .
                           "feature_role, role_enabled) VALUES " .
                           implode(", ", $insertData) . ";";

            // Return false if the insert fails.
            if (!$this->db->query($insertQuery)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

}
