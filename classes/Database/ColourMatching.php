<?php

namespace OMIS\Database;


use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class ColourMatching extends AbstractEntityDAO
{

    /**
     * Get Colour Name
     */
    public function getColourName($colour_code)
    {
        $sql = "SELECT colour_name FROM colour_matching WHERE circuit_colour = '"
            . $this->db->clean($colour_code) . "'";
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Get all colour records
     * @param string $indexField index field
     * @param  array                circuit colour records
     * @return array|bool|mixed[]
     */
    public function getAllCircuitColours($indexField = null)
    {
        $mysqliResult = $this->db->query("SELECT * FROM colour_matching");

        if (!$mysqliResult) {
            return false;
        }

        $returnData = CoreDB::into_array($mysqliResult);

        // Group by specific index field
        if ($indexField !== null) {
            $returnData = CoreDB::group($returnData, [$indexField], true, true);
        }

        return $returnData;
    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     * @return array
     */
    protected function getIds()
    {
        // Table doesn't have a primary key
        return null;
    }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName()
    {
        return 'colour_matching';
    }
}