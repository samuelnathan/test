<?php

namespace OMIS\Database;


use OMIS\Database\DAO\AbstractEntityDAO;

include_once __DIR__ . '/../../extra/usefulphpfunc.php';

class Stations extends AbstractEntityDAO
{

    /**
     * Get the database record for a specific station.
     *
     * @param int  $id     ID number of station
     * @param bool $array If TRUE, casts the mysql_result to an array.
     * @return bool|mixed|mysqli_result FALSE if the station doesn't exist.
     *          If there is a station and $toArray is TRUE, an associative
     *          array with the station's attributes or otherwise a mysqli_result
     *          object.
     */
    public function getStation($id, $array = true)
    {
        $idSan = $this->db->clean($id);
        $sql = "SELECT session_id, session_stations.station_id, session_stations.form_id,"
            . "forms.form_name, session_stations.station_code, forms.form_description, exams.dept_id, "
            . "exams.term_id, total_value, pass_value, weighting, station_time, visual_alarm1, visual_alarm2, "
            . "desc_auto_popup, station_number, station_order, design_complete, author, exclude_from_grading, "
            . "session_stations.updated_at, session_stations.examiners_required "
            . "FROM (session_stations INNER JOIN forms USING(form_id) "
            . "INNER JOIN exam_sessions USING(session_id)) "
            . "INNER JOIN exams USING(exam_id)"
            . "WHERE station_id = '$idSan' "
            . "GROUP BY session_stations.form_id LIMIT 1";

        // Get the result and figure out what to do with it.
        $result = $this->db->query($sql);

        /* If the result isn't FALSE (i.e. there's actually a result), and the
         * calling code wants an array back, then make an array out of it.
         * Otherwise, return the result as-is. This ensures the consistency of
         * the "failed" response so that it's always FALSE rather than FALSE or
         * NULL depending on the value of the $toArray parameter.
         */
        if ($array && (!empty($result))) {
            return $this->db->fetch_row($result);
        } else {
            return $result;
        }
    }

    /**
     * Get Exam Stations By Sessions

     * @param int[] $sessions Array of session IDs to search for
     * @param bool $array If TRUE, return results as an array, otherwise a mysqli_result.
     * @param string $filter (Optional) Extra field name that can be used to further group results
     * @param string $order (Optional) Name of field to order results by
     * @param bool $rests If TRUE, include rest station data.
     * @param bool $grades If TRUE, include only grade rule stations
     * @return mixed[]|mysqli_result Depends on value of $array.
     */
    public function getStations($sessions, $array = false, $filter = 'station_number', $order = 'form_name', $rests = false, $grades = false) {

        if (empty($sessions)) {
            return ($array ? [] : false);
        }

        // Promote $sessions to an array if needs be...
        if (!is_array($sessions)) {
            $sessions = [$sessions];
        }

        $sessionList = implode(", ", $sessions);

        // Include Rest Stations SQL Query
        $restSql = $rests ?  "" : "AND rest_station = 0 ";
        $gradeSql = $grades ?  "AND exclude_from_grading = 0 " : "";

        $sql = "SELECT station_id, session_id, session_stations.form_id, form_name, "
            . "pass_value, weighting, total_value, station_number, station_code, station_order, "
            . "station_time, visual_alarm1, visual_alarm2, desc_auto_popup, rest_station, author, "
            . "session_stations.updated_at AS station_last_updated, examiners_required, exclude_from_grading "
            . "FROM session_stations LEFT JOIN forms USING (form_id) "
            . "WHERE session_stations.session_id IN (" . $sessionList . ") " . $restSql . $gradeSql
            . "GROUP BY session_stations.form_id, forms.form_name, session_stations." . $filter . " "
            . "ORDER BY station_number, LENGTH(" . $order . "), " . $order . "";

        $mysqliResult = $this->db->query($sql);

        if ($array) {

            $resultArray = CoreDB::into_array($mysqliResult);
            return CoreDB::group($resultArray, ['station_number'], true);

        } else {

            return $mysqliResult;

        }
    }

    /**
     * Add station to session
     *
     * @param string $sessionID       ID number for a session
     * @param int $formID             ID number for a form
     * @param int $pass               Station pass value
     * @param int $number             Station number
     * @param string $tag             Station tag (group name)
     * @param string $time            Station time limit
     * @param string $alarm1          Alarm one
     * @param string $alarm2          Alarm two
     * @param bool $autoPopup         Description auto popup
     * @param bool $rest              Rest station
     * @param int $examinersRequired  Number of examiners required
     * @param int $weighting          Percentage weight value
     * @param int $excludeGrading     Exclude from grading
     *
     * @return bool success true|false
     */
    public function addOne($sessionID, $formID, $settings)
    {

        $settingSan = array_map(
            "\OMIS\Database\CoreDB::clean",
            array_values($settings)
        );

        list(
            $pass, $number, $tag, $time, $alarm1, $alarm2,
            $autoPopup, $station_type, $rest, $examinersRequired, $weighting,
            $excludeGrading
            ) = $settingSan;

        $mainFields = [
            "session_id",
            "station_number",
            "station_order",
            "rest_station",
            "examiners_required"
        ];

        $mainValues = [
            $sessionID,
            $number,
            0,
            $rest,
            $examinersRequired
        ];

        // Add remaining fields and values if it is not a rest station
        if ($rest == 0) {
            // Add remaining fields
            $remainingFields = [
                "form_id",
                "pass_value",
                "station_code",
                "station_time",
                "visual_alarm1",
                "visual_alarm2",
                "desc_auto_popup",
                "station_type",
                "weighting",
                "exclude_from_grading"
            ];

            $mainFields = array_merge($mainFields, $remainingFields);

            // Add remaining values
            $remainingValues = [
                $formID,
                $pass,
                $tag,
                $time,
                $alarm1,
                $alarm2,
                $autoPopup,
                $station_type,
                $weighting,
                $excludeGrading
            ];

            // Merge and clean values
            $mainValues = array_merge($mainValues, $remainingValues);
            $mainValues = array_map("\OMIS\Database\CoreDB::clean", $mainValues);
        }

        $sql = "INSERT INTO session_stations (" . implode(",", $mainFields)
            . ") VALUES ('" . implode("','", $mainValues) . "')";

        // Log via analog
        $success = $this->db->query($sql);
        if ($success) {
            $formRecord = $this->db->forms->getForm($formID);
            $sessionRecord = $this->db->exams->getSession($sessionID);
            $examRecord = $this->db->exams->getExam($this->db->sessions->getSessionExamID($sessionID));

            $logString = $_SESSION['user_identifier'] . ' added Station '
                . $formRecord['form_name'] . ' to Exam ' . $examRecord['exam_name']
                . ' ' . formatDate($sessionRecord['session_date']) . ' '
                . $sessionRecord['session_description'];
            \Analog::info($logString);
        }

        return $success;
    }

    /**
     * Update station record
     *
     * @param int $id               ID number for station
     * @param int $formID           the ID number for the form
     * @param int $number           the station number
     * @param string $tag           the station tag
     * @param int $pass             the station pass value
     * @param time_stamp $time      the station time limit
     * @param time_stamp $alarm1    alarm 1 for the station time limit
     * @param time_stamp $alarm2    alarm 2 for the station time limit
     * @param int $complete         station complete, yes or no
     * @param int $popup            auto popup of form description/notes
     * @param int $excludeGrading   exclude from grading
     * @param int $weighting        the station weighting value (default: 0%)
     *
     * @return boolean              station updated true|false
     */
    public function updateOne($id, $formID, $settings)
    {

        $formIDSan = $this->db->clean($formID);

        $settingSan = array_map(
            "\OMIS\Database\CoreDB::clean",
            array_values($settings)
        );

        list(
            $number, $tag, $pass, $edit_station_type, $time, $alarm1, $alarm2,
            $autoPopup, $weighting, $excludeGrading
            ) = $settingSan;

        if ($autoPopup == "NO_UPDATE") {

            $popupSql = "";

        } else {

            $popupSql = ", desc_auto_popup = '" . $this->db->clean($autoPopup) . "'";

        }

        if ($excludeGrading == "NO_UPDATE") {

            $gradeSql = "";

        } else {

            $gradeSql = ", exclude_from_grading = '" . $this->db->clean($excludeGrading) . "'";
            
        }        

        $stationSql = "UPDATE session_stations SET station_number='$number', "
            . "station_code='$tag', form_id='$formIDSan', "
            . "pass_value='$pass', weighting='$weighting', station_time='$time', "
            . "visual_alarm1='$alarm1', visual_alarm2='$alarm2', "
            . " station_type='$edit_station_type' "
            . "$popupSql $gradeSql WHERE station_id='". $this->db->clean($id) . "'";


        if (!$this->db->query($stationSql)) {

            return false;

        }

        $this->reOrderStations($this->db->sessions->getSessionIDByStationID($id));
        return true;

    }

    /**
     * Delete session stations
     * @param type $toDelete       stations to delete
     * @param type $sessionID     ID number for exam session
     * @return int count          number of stations deleted
     */
    public function deleteStations($toDelete, $sessionID)
    {
        // Station delete count
        $deleteCount = 0;

        // Get details for the exam.
        $examID = $this->db->sessions->getSessionExamID($sessionID);
        $examDetails = $this->db->exams->getExam($examID);

        // Loop through the passed in station ID numbers and delete
        foreach ($toDelete as $stationIDRaw) {
            $stationID = $this->db->clean($stationIDRaw);

            // (DW) Need to handle rest stations properly.
            if ($this->isRestStation($stationID)) {
                $formDescription = "Rest station #$stationID";
            } else {
                // Does station have results ?
                if ($this->db->results->doesStationHaveResults($stationID, 'num')) {
                    // Stations that cannot be deleted
                    setA($_SESSION, 'stations_with_results_warning', []);
                    $_SESSION['stations_with_results_warning'][] = $stationID;
                    continue;
                }

                // Get station information
                $station = $this->getStation($stationID, true);
                if (empty($station)) {
                    error_log(__METHOD__ . ": Station $stationID not found. Skipping to next record");
                    continue;
                }
                $formDetails = $this->db->forms->getForm($station['form_id']);
                $formDescription = $formDetails['form_name'];
            }

            $sql = "DELETE FROM session_stations WHERE station_id = '$stationID'";

            if ($this->db->query($sql)) {
                $logMessage = sprintf(
                    "%s - removed %s from %s",
                    $_SESSION['user_identifier'],
                    $formDescription,
                    $examDetails['exam_name']
                );

                \Analog::info($logMessage);
                $deleteCount++;
            }
        }

        $this->reOrderStations($sessionID);
        return $deleteCount;
    }

    //Reorder Stations [Updated 28/08/2014]
    /**
     * Collapse/fix session station ordering.
     *
     * @param int $session_id ID number of session.
     * @return bool TRUE if sessions were renumbered successfully, FALSE otherwise
     */
    public function reOrderStations($session_id)
    {
        $session_stations = $this->getStations([$session_id], false, 'station_number', 'form_name', true);
        $updateCount = 0;
        $stationOrder = 1;

        //Go through Stations
        $stationCount = 0;
        while ($station = mysqli_fetch_assoc($session_stations)) {
            $stationCount++;
            $sql = "UPDATE session_stations SET station_order = '" . $stationOrder
                . "' WHERE station_id = '" . $station['station_id'] . "'";

            $updateCount += (int) $this->db->query($sql);
            $stationOrder++;
        }

        return ($updateCount == $stationCount);
    }

    //Get Session Stations Information by form_id, station_number & session id [Updated 26/01/2014]
    public function getStationsBySessionID($station_key, $session_id)
    {
        list($station_number, $form_id) = explode(':', $station_key);
        $sql = "SELECT session_stations.station_id FROM session_stations "
            . "WHERE session_stations.form_id = '" . $this->db->clean($form_id)
            . "' AND session_stations.station_number = '" . $this->db->clean($station_number)
            . "' AND session_stations.session_id = '" . $this->db->clean($session_id)
            . "' LIMIT 1";
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Clone session stations
     *
     * @param int $new_session_id   new session ID
     * @param int $org_session_id   original session ID
     * @param string $dept_id       ID for the departmrnt
     * @param string $term_id       ID for the academic term
     */
    public function cloneStations($new_session_id, $org_session_id, $dept_id, $term_id)
    {
        $new_session_id = $this->db->clean($new_session_id);
        $prev_form_ids = [];

        // Does the session exist? then we can clone the stations
        if ($this->db->sessions->doesSessionExist($org_session_id)) {

            // Get stations for session to copy
            $stationsDB = $this->getStations(
                [$org_session_id], false,
                'station_number', 'form_name', true
            );

            // Get form IDs by session
            $existingFormIDs = $this->db->forms->getFormIDsBySession($new_session_id);

            // Loop through all stations to clone
            while ($station = mysqli_fetch_assoc($stationsDB)) {

                // If not in current list
                if (!in_array($station['form_id'], $existingFormIDs)) {

                    // Is rest station
                    $isRestStation = (bool) $station['rest_station'];

                    // Add station to the session
                    $this->addOne(
                        $new_session_id,
                        $station['form_id'],
                        [
                            "pass" => $station['pass_value'],
                            "number" => $station['station_number'],
                            "tag" => $station['station_code'],
                            "time" => $station['station_time'],
                            "alarm1" => $station['visual_alarm1'],
                            "alarm2" => $station['visual_alarm2'],
                            "popup" => $station['desc_auto_popup'],
                            "rest" => $station['rest_station'],
                            "examiners" => $station['examiners_required'],
                            "weighting" => $station['weighting'],
                            "exclude_from_grading" => $station['exclude_from_grading']
                        ]
                    );

                    // If it's a rest station then we don't have an actual form ID attached
                    if (!$isRestStation) {
                        $prev_form_ids[] = $station['form_id'];
                    }
                }
            }

            // Add Stations to current term
            $this->db->forms->attachFormsToDeptTerm($prev_form_ids, $dept_id, $term_id);
        }

        // Reorder station numbers
        $this->reOrderStations($new_session_id);
    }

    /**
     * Get a list of exam stations filtered by session and a second user-definable
     * parameter
     *
     * @param mixed  $sessions         Integer/string (single session ID) or array of session IDs
     * @param string $filter_parameter Name of field to filter by
     * @param string $filter_value     Value to filter for in field.
     * @return mixed|null NULL if no records match. Otherwise, an array of associative arrays.
     */
    public function getStationsByFilter($sessions, $filter_parameter, $filter_value)
    {
        $filter_parameter = $this->db->clean($filter_parameter);
        $filter_value = $this->db->clean($filter_value);

        // Unpack single-element arrays.
        if (is_array($sessions) && (count($sessions) == 1)) {
            $sessions = $sessions[0];
        }

        $session_filter = "session_stations.session_id ";
        if (!is_array($sessions)) {
            $session_filter .= "= $sessions";
        } else {
            $session_filter .= "IN (" . implode(",", $sessions) . ")";
        }

        $sql = "SELECT session_stations.station_id, session_stations.form_id, "
            . "session_stations.session_id, session_stations.pass_value, "
            . "session_stations.station_number, session_stations.station_code, "
            . "forms.form_name, session_stations.weighting, forms.total_value "
            . "FROM session_stations INNER JOIN forms USING (form_id) "
            . "WHERE ($session_filter) AND $filter_parameter = '$filter_value' "
            . "ORDER BY session_stations.station_number, LENGTH(forms.form_name), forms.form_name";

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['session_id'], true);
    }

    /**
     * Get stations by exam
     *
     * @param string $examID             id number for the exam
     * @param array $excludeStationNums  station numbers to exclude (optional)
     * @return array                     list of unique station records
     */
    public function getStationsByExam($examID, $excludeStationNums = [])
    {
        $examID = $this->db->clean($examID);
        $excludeNums = array_map("\OMIS\Database\CoreDB::clean", $excludeStationNums);

        // Exclude stations clause
        if (count($excludeNums) > 0) {
            $idsString = implode(', ', $excludeNums);
            $numsSql = "AND station_number NOT IN ($idsString) ";
        } else {
            $numsSql = "";
        }

        $sql = "SELECT session_stations.station_id, station_number, form_id, examiners_required, "
            . "station_order, exam_sessions.session_id, pass_value, station_code, weighting "
            . "FROM session_stations "
            . "INNER JOIN forms USING (form_id) "
            . "INNER JOIN exam_sessions USING (session_id) "
            . "WHERE exam_id = '$examID' "
            . $numsSql
            . "AND rest_station = 0 "
            . "GROUP BY station_id";

        $mysqliResult = $this->db->query($sql);

        if (!$mysqliResult || mysqli_num_rows($mysqliResult) == 0) {
            return [];
        }

        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['station_id'], true, true);
    }

    //Does Session Station Exist [Updated 03/02/2014]
    public function doesStationExist($station_id, $session_id = null)
    {
        $station_id = $this->db->clean($station_id);
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT COUNT(*) FROM session_stations WHERE station_id = '$station_id'";

        if (!empty($session_id)) {
            $sql .= " AND session_id = '$session_id'";
        }

        return (CoreDB::single_result($this->db->query($sql)) > 0);
    }

    /**
     * Is station multi-scenarion
     * Note: only 1 scenario result can be submitted per station
     *
     * @param int $stationID      ID number of session_stations
     * return bool true|false
     */
    public function multiScenarioStation($stationID)
    {

        // Sanitize station ID
        $stationID = $this->db->clean($stationID);

        // Get station record
        $stationRecord = $this->getStation($stationID, true);
        $stationNumber = $stationRecord['station_number'];
        $sessionID = $stationRecord['session_id'];

        // Sql query
        $sql = "SELECT COUNT(form_id) "
            . "FROM session_stations "
            . "WHERE station_number = '$stationNumber' "
            . "AND session_id = '$sessionID'";

        // Returns true if we have more than one scenarios in the station
        return ((int)CoreDB::single_result($this->db->query($sql)) > 1);
    }

    /**
     * Get station scenarios
     *
     * @param int $stationID  id number for the station
     * @param int $sessionID  id number for the session
     * @return array          array of station scenarios
     */
    public function getStationScenarios($stationID, $sessionID)
    {

        // Get station record
        $record = $this->getStation($stationID, true);

        $sql = "SELECT * "
            . "FROM session_stations "
            . "WHERE session_id = '" . $this->db->clean($sessionID) . "' "
            . "AND station_number = '" . $record['station_number'] . "'";

        $mysqliResult = $this->db->query($sql);
        $resultArray = CoreDB::into_array($mysqliResult);
        return CoreDB::group($resultArray, ['station_id'], true, true);
    }

    /**
     * Get station tags by session
     *
     * @param int $examID    number identifier for exam
     * @return string[]
     */
    public function getStationGroups($examID)
    {
        $sql = "SELECT DISTINCT(station_code) FROM session_stations INNER JOIN "
            . "exam_sessions USING(session_id) WHERE exam_id = '"
            . $this->db->clean($examID) . "'";

        return array_filter(
            CoreDB::into_values($this->db->query($sql)),
            create_function('$a', 'return (trim($a) !== "");')
        );
    }

    //Get Highest Station Number [Updated 26/01/2014]
    public function getHighestStationNumber($session_id)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT MAX(station_number) as max FROM session_stations WHERE session_id = '$session_id'";
        return CoreDB::single_result($this->db->query($sql));
    }

    //Get Lowest Station Number [Updated 04/02/2014]
    public function getLowestStationNum($session_id)
    {
        $session_id = $this->db->clean($session_id);
        $sql = "SELECT MIN(station_number) FROM session_stations WHERE session_id = '" . $session_id . "'";
        return CoreDB::single_result($this->db->query($sql));
    }

    /**
     * Get the number of examiners required for a given station.
     *
     * @param int  $station_id  ID number of session_stations.
     * @param bool $include_observers   If TRUE, include the number of zero-weighted
     *                                  examiners (i.e. observers) are included
     *                                  in the returned count.
     * @param bool $return_zero If TRUE, returns the number of examiners deemed
     *                          required for this station. If FALSE, it sets 1
     *                          as the effective minimum value it returns.
     *
     * @return int Number of examiners required for this examiner; minimum is 1
     *             if $return_zero is FALSE.
     */
    public function getExaminersRequired($station_id, $include_observers = false, $return_zero = false)
    {
        $station_id = $this->db->clean($station_id);
        $sql = "SELECT examiners_required FROM session_stations WHERE station_id = '$station_id'";
        $examiners_required = (int) CoreDB::single_result($this->db->query($sql));

        if ($include_observers) {
            $examinerWeightings = $this->db->stationExaminers->getExaminerWeightings($station_id);

            $observerCount = 0;
            if (sizeof($examinerWeightings) > 0) {
                foreach ($examinerWeightings as $examinerID => $weighting) {
                    if ($weighting == 0) {
                        $observerCount++;
                    }
                }
            }

            $examiners_required += $observerCount;
        }

        if ($return_zero) {
            return $examiners_required;
        } else {
            return max(1, $examiners_required);
        }
    }

    /**
     * Check if a station is a rest station
     *
     * @param int $station_id Station ID to check
     * @return bool TRUE if the station is a rest station, FALSE otherwise.
     */
    public function isRestStation($station_id)
    {
        $sql = sprintf(
            "SELECT rest_station FROM session_stations WHERE (station_id = %d)",
            $station_id
        );

        $result = CoreDB::single_result($this->db->query($sql));
        if ($result === \FALSE) {
            return \FALSE;
        } else {
            return ((int) $result == 1);
        }
    }

    /**
     * Set the number of examiners required for a particular station (default is 1)
     *
     * @param int $station_id     ID number of station
     * @param int $examiner_count Number of examiners required (optional, default is 1)
     * @return bool TRUE on success, FALSE otherwise.
     */
    public function setExaminersRequired($station_id, $examiner_count = 1)
    {
        $sql = "UPDATE session_stations SET examiners_required = '" . $this->db->clean($examiner_count)
            . "' WHERE station_id = '" . $this->db->clean($station_id) . "'";
        return $this->db->query($sql);
    }

    /**
     * Get Unique Station Numbers
     */
    public function getUniqueStationNumbers($exam_id, $session_id = null)
    {
        if (is_null($session_id)) {
            $sessionQ = "";
        } else {
            $sessionQ = " AND session_stations.session_id = '$session_id'";
        }
        $sql = "SELECT DISTINCT station_number FROM exam_sessions INNER JOIN "
            . "session_stations USING(session_id) WHERE exam_id = '"
            . $this->db->clean($exam_id) . "'" . $sessionQ
            . " ORDER BY station_number";

        return CoreDB::into_values($this->db->query($sql));
    }

    /**
     * Gets the stations given the ID of the session they're assigned to
     * 
     * @param $id int                         ID of the session
     * @param bool $sorted boolean            Flag indicating whether the result must be sorted
     *                                        by station order in the session
     * @return array
     */
    public function bySession($id, $sorted = true) {
    
        $sql = "SELECT station_id, session_id, session_stations.form_id, station_number, "
             . "station_order, examiners_required, rest_station, form_name, station_time "
             . "FROM session_stations LEFT OUTER JOIN forms "
             . "ON session_stations.form_id = forms.form_id "
             . "WHERE session_id = '{$this->db->clean($id)}'";

        if ($sorted)
           $sql .= "ORDER BY station_order";

        return CoreDB::into_array($this->db->query($sql));
    }


    /**
     * Get the name of the columns that form the primary key of the entity in the database
     *
     * @return array
     */
    protected function getIds() { return ['station_id']; }

    /**
     * Get the name of the table that represents the entity in the database
     *
     * @return string
     */
    protected function getTableName() { return 'session_stations'; }

}