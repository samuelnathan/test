<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  * Database function calls relating to Schools & Departments
  */

 namespace OMIS\Database;

 use OMIS\Database\DAO\AbstractEntityDAO;

 class Departments extends AbstractEntityDAO
 {

    /**
     * Get a list of department records, either all of them or filtered by a list
     * of department IDs.
     *
     * @param  mixed[]|null   $ids (optional) If NULL, function returns ALL departments.
     * @param  bool           $toArray (optional) If TRUE, converts return result to array
     * @param  int[]          $schools (optional) List of schools Identifiers to filter

     * @return mysqli_result|mixed[]|false Depending on success and value of $toArray.
     */
    public function getDepartments($ids = null, $toArray = false, $schools = [])
    {
        $sql = "SELECT * FROM departments INNER JOIN schools " .
               "USING (school_id) WHERE 1";

        /* If the user has specified a list of departments to filter by, then
         * use that as a filter criterion to reduce the overall list.
         */
        if (!empty($ids)) {
            // Promote $ids to an array if needs be.
            if (!is_array($ids)) {
                $ids = [$ids];
            }

            // Quote all the department IDs before injecting them into the SQL
            $ids = array_map(
                function ($value) {
                    return "'" . trim($this->db->clean($value), "'\"") . "'";
                },
                $ids
            );

            $sql .= " AND dept_id IN (" . implode(",", $ids) . ")";
        }

        /**
         * Filter by schools
         */
        if (!empty($schools) && is_array($schools)) {
            $schoolsSan = array_map(
                function ($value) {
                    return "'" . trim($this->db->clean($value), "'\"") . "'";
                },
                $schools
            );
            $sql .= " AND school_id IN (" . implode(",", $schoolsSan) . ")";
        }

        // Finish off the query...
        $sql .= " ORDER BY school_description, dept_name";

        $results = $this->db->query($sql);
        if ($toArray) {
            // If we want an array, return one.
            $results = CoreDB::into_array($results);
        }

        return $results;
    }
    
    /**
     * Get all departments in the system
     * 
     * @param bool $toArray if TRUE return an array, if FALSE, a mysqli_result.
     * @return mixed[]|mysqli_result Depends on value of $toArray
     */
    public function getAllDepartments($toArray = false)
    {
      
        return $this->getDepartments(null, $toArray);
    
    }

    /**
     * Get the list of Departments for a given School identified by $schoolID
     * 
     * @param int $schoolID ID number of school
     * @return mysqli_result Query result.
     */
    public function getDepartmentsBySchool($id)
    {
        $sql = "SELECT * FROM departments WHERE school_id = '" . $this->db->clean($id)
             . "' ORDER BY dept_id, dept_name";

        return $this->db->query($sql);
    }

    /**
     * Get Department Record
     *  
     * @param int $id
     * @param bool $toArray
     * @return bool
     */
    public function getDepartment($id, $toArray = false)
    {
        
        $sql = "SELECT * FROM departments INNER JOIN schools USING(school_id) "
             . "WHERE dept_id = '" . $this->db->clean($id) . "'";

        return ($toArray) ? $this->db->fetch_row($this->db->query($sql)) 
               : $this->db->query($sql);
        
    }

    /**
     * Get Department Name
     * 
     * @param int $id
     * @return mixed[]
     */
    public function getDepartmentName($id)
    {
        
        $sql = "SELECT dept_name FROM departments WHERE dept_id = '"
             . $this->db->clean($id) . "'";

        return CoreDB::single_result($this->db->query($sql));
        
    }
    
    /**
     * Get Department by Session ID
     * 
     * @param int $id   Session identity number
     * @return string   Department identity number
     */
    public function getDepartIDBySessionID($id)
    {
      
        $sql = "SELECT dept_id FROM exams WHERE exam_id = (SELECT exam_id "
            . "FROM exam_sessions WHERE session_id = '"
            . $this->db->clean($id) . "')";

        return CoreDB::single_result($this->db->query($sql));
        
    }

    /**
     * Update a department's details.
     * 
     * @param string $newID      New ID to use for this department
     * @param string $currentID  Current ID of department
     * @param string $name       New Name of department
     * @return bool TRUE on success, FALSE on failure
     * @throws \InvalidArgumentException
     */
    public function updateDepartment($newID, $currentID, $name)
    {

        global $config;

        // Check that the old department ID exists.
        if (!$this->existsById(trim($currentID))) {
            throw new \InvalidArgumentException("Department with id '$currentID' does not exist");
        }
        
        // Check that the selected "new" department DOES NOT exist.
        if (($newID !== $currentID) && $this->existsById(trim($newID))) {
            throw new \InvalidArgumentException("Department with id '$newID' already exists");
        }
        
        // Check if the deparment name isn't set properly.
        if ((empty($name) || trim($name) === '')) {
            throw new \InvalidArgumentException("Department Name not specified");
        }
        
        $newIDSan = trim($this->db->clean($newID));
        $currentIDSan = trim($this->db->clean($currentID));
        $nameSan = trim($this->db->clean($name));        
       
        // Use the appropriate SQL depending on whether the ID is changing.
        if ($newIDSan !== $currentIDSan) {
            $sql = "UPDATE departments SET dept_name = '$nameSan', 
                    dept_id = '$newIDSan' WHERE dept_id = '$currentIDSan'";
        } else {
            $sql = "UPDATE departments SET dept_name = '$nameSan' 
                    WHERE dept_id = '$currentIDSan'";
        }
        
        // Update status
        $updateSuccess = $this->db->query($sql);
        
        // Update success
        if ($updateSuccess) {

          \Analog::info($_SESSION['user_identifier'] 
          . " updated department $nameSan with identifier $currentID");

          // Couple course if enabled
          if ($config->db['couple_depts_courses']) {

             $this->db->courses->updateCourse(
                $newIDSan, 
                $currentIDSan, 
                $nameSan
             );

          }

          return true;
         
        } else {
          return false;  
        }
        
    }

    /**
     * Insert a new department into the database.
     * 
     * @param int $schoolID   School ID
     * @param string $deptID  Department ID
     * @param string $name    Department Name
     * @return bool
     * @throws \InvalidArgumentException If one or more parameters is incorrect.
     */
    public function insertDepartment($schoolID, $deptID, $name)
    {

        global $config;

        // Check that the school ID has been specified.
        if (empty($schoolID) || ((int) $schoolID < 1)) {
            throw new \InvalidArgumentException("School ID not specified");
        }
        
        // Check if the specified school exists.
        if (!$this->db->schools->doesSchoolExist((int) $schoolID)) {
            throw new \InvalidArgumentException("School with id '$schoolID' does not exist");
        }
        
        // Check that a department ID has been specified.
        if (empty($deptID) || (trim($deptID) === '')) {
            throw new \InvalidArgumentException("Department ID not specified");
        }
        
        // Check if the department DOES NOT already exist.
        if ($this->existsById($deptID)) {
            throw new \InvalidArgumentException("Department with id '$deptID' already exists");
        }
        
        // Check if the name field is empty.
        if (empty($name) || (trim($name) === '')) {
            throw new \InvalidArgumentException("Department name is empty");
        }
        
        // Construct the query
        $sql = "INSERT INTO departments (dept_id, dept_name, school_id) "
            . "VALUES ('" . $this->db->clean($deptID) . "', '"
            . $this->db->clean($name) . "', '" . ((int) $schoolID) . "')";

        // Attempt to insert the new department.
        if ($this->db->query($sql)) {
            \Analog::info($_SESSION['user_identifier'] . " created department with name = " . $name);

            // Couple course if enabled
            if ($config->db['couple_depts_courses']) {
                $this->db->courses->insertCourse($deptID, $name);
            }

            return \True;

        } else {
            return \False;
        }

    }

    /**
     * Delete the departments
     * 
     * @global object $config
     * @param int[] $ids array of department identifiers to delete
     * @return int       the delete count
     */
    public function deleteDepartments($ids)
    {
        
        global $config;
        $deleteCount = 0;

        foreach ($ids as $id) {

            $idSan = $this->db->clean($id);
            $sql = "DELETE FROM departments WHERE dept_id = '$idSan';";
            
            // Remove course if coupling is enabled and if linked
            if ($config->db['couple_depts_courses']) {
                $sql .= "DELETE FROM courses WHERE course_id = '$idSan';";
            }
            
            $sql .= "DELETE FROM form_department_term WHERE dept_id = '$idSan';";
            $sql .= "DELETE FROM examiner_department_term WHERE dept_id = '$idSan';";
            $sql .= "DELETE FROM exams WHERE dept_id = '$idSan';";
            
            if ($this->db->multi_query($sql)) {
                \Analog::info(
                    $_SESSION['user_identifier'] . " deleted department with identifier "
                    . $idSan . " from " . $config->getName()
                );
                $deleteCount++;
            }

        }
       
        return $deleteCount;
        
    }

    /**
     * Get a list of department records for a given examiner (user) id and, if
     * desired, for a specific term.
     *
     * @param  string              $userID ID of examiner/user.
     * @param  string              $termID ID of term (if desired) or NULL for all terms
     * @param  string              $toArray whether to convert the mysqli_result to an array
     * @return mysqli_result|array|false FALSE on failure, mysqli_result|array otherwise.
     */
    public function getExaminerDepartments($userID, $termID = null, $toArray = false)
    {
        // Term SQL clause
        if (is_null($termID) || strlen($termID) == 0) {
            $term_clause = "";
        } else {
            $termID = $this->db->clean($termID);
            $term_clause = " AND examiner_department_term.term_id = '" . $termID . "'";
        }
       
        $userID = $this->db->clean($userID);
        $sql = "SELECT examiner_department_term.dept_id, "
              ."departments.dept_name, departments.school_id, "
              ."schools.school_description "
              ."FROM (examiner_department_term "
              ."INNER JOIN departments USING(dept_id)) "
              ."INNER JOIN schools USING(school_id) "
              ."WHERE examiner_department_term.user_id = '" . $userID . "'"
              . $term_clause . " "
              ."GROUP BY examiner_department_term.dept_id "
              ."ORDER BY departments.dept_name";
        
        $mysqliResult = $this->db->query($sql);
        
        if ($toArray) {
           return CoreDB::into_array($mysqliResult); 
        } else {
           return $mysqliResult; 
        }
    }

    /**
     * Get examiner department Identifiers
     * 
     * @param string $user   Identifier for the user
     * @param string $term   Identifier for the term (optional)
     * @return string[]      Array of department identifiers
     */
    public function getExaminerDepartmentIDs($user, $term = null)
    {
        if (is_null($term)) {
            $termSql = '';
        } else {
            $termSql = " AND examiner_department_term.term_id = '"
                . $this->db->clean($term) . "'";
        }

        $sql = "SELECT examiner_department_term.dept_id "
             . "FROM examiner_department_term"
             . " WHERE examiner_department_term.user_id = '" . $this->db->clean($user)
             . "'" . $termSql . " GROUP BY examiner_department_term.dept_id";

        return CoreDB::into_values($this->db->query($sql));
    }

    /**
     * Get Recently Active Department
     */
    public function getLastActiveDepartment()
    {
        //First Try exams
        $sql = "SELECT dept_id FROM exam_sessions INNER JOIN exams USING(exam_id) "
            . "ORDER BY session_date DESC LIMIT 1";
        $or = $this->db->query($sql);
        if ($or && mysqli_num_rows($or) == 1) {
            return CoreDB::single_result($or);
        } else { //Or Get First Available Dept
            $dr = $this->db->query("SELECT dept_id FROM departments LIMIT 1");
            if ($dr && mysqli_num_rows($dr) == 1) {
                return CoreDB::single_result($dr);
            } else { //Or If No Dept Return Nothing

                return '';
            }
        }
    }

    protected function getTableName() { return 'departments'; }

    protected function getIds() { return ['dept_id']; }

 }
