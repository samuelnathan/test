<?php

/* Original Author: David Cunningham
  For Qpercom Ltd
  Date: 03/03/2014
  © 2014 Qpercom Limited.  All rights reserved.
  Database function calls relating to ISO
 */

namespace OMIS\Database;

class ISO
{
//Core Database 'coreDB' reference
    private $db;

    //Constructor
    public function __construct($db)
    {
        $this->db = $db;
    }

    //Get All ISO country codes [Updated 05/02/2014]
    public function getISOCountryCodes()
    {
        return CoreDB::into_array($this->db->query("SELECT * FROM iso_country_codes"));
    }

    //Get Country record [Updated 05/02/2014]
    public function getISOCountryCode($code)
    {
        $r = $this->db->query("SELECT * FROM iso_country_codes WHERE iso = '" . $this->db->clean($code) . "'");
        if ($r && mysqli_num_rows($r) > 0) {
            return $this->db->fetch_row($r);
        } else {
            return "Not Known";
        }
    }
}
