<?php
/**
 * @author Brendan Devane 
 * @updated David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018 Qpercom Limited
 * @note: Badly needs refactoring
 */
class Breadcrumbs
{
    public $crumbs;
    
    // page names
    public $page = array(
        // Menu Main
        "main" => array(
            array("crumb_desc" => "Main",
                  "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Home Page",
                  "crumb_url" => "manage.php?page=main"
            ),
        ),
        "account" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Account",
                "crumb_url" => "manage.php?page=account"
            ),
        ),
        "manage_features" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Features",
                "crumb_url" => "manage.php?page=manage_features"
            ),
        ),
        "manage_terms" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Terms",
                "crumb_url" => "manage.php?page=manage_terms"
            )
        ),
        
        // Menu Admin Tools
        "admin_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "javascript:void(0)"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "admin_tool"
            )
        ),
        "swap_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "Swap Tool",
                "crumb_url" => "manage.php?page=swap_tool"
            )
        ),
        "feedback_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "Feedback",
                "crumb_url" => "manage.php?page=feedback_tool"
            )
        ),
        "station_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "Import &amp; Export",
                "crumb_url" => "manage.php?page=station_tool"
            )
        ),
        "loginman_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "Clear Login Timeouts",
                "crumb_url" => "manage.php?page=loginman_tool"
            )
        ),
        "userlog_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "View User Logs",
                "crumb_url" => "manage.php?page=userlog_tool"
            )
        ),
        "role_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "Roles",
                "crumb_url" => "role_tool"
            )
        ),
        "grs_types_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "GRS Types",
                "crumb_url" => "grs_types_tool"
            )
        ),
        "grs_values_tool" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=main"
            ),
            array("crumb_desc" => "Admin Tools",
                "crumb_url" => "manage.php?page=admin_tool"
            ),
            array("crumb_desc" => "GRS Types",
                "crumb_url" => "manage.php?page=grs_types_tool"
            ),
            array("crumb_desc" => "GRS Values",
                "crumb_url" => "grs_values_tool"
            )
        ),
        "importingStep1" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Import Data",
                "crumb_url" => "manage.php?page=importingStep1"
            )
        ),
        "importingStep2" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Import Data",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Step 2",
                "crumb_url" => "manage.php?page=importingStep2"
            )
        ),
        "importingStep3" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Import Data",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Step 3",
                "crumb_url" => "manage.php?page=importingStep3"
            )
        ),
        "importingStep4" => array(
            array("crumb_desc" => "Main",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Import Data",
                "crumb_url" => "manage.php?page=importingStep1"
            ),
            array("crumb_desc" => "Step 4",
                "crumb_url" => "manage.php?page=importingStep4"
            )
        ),
        
        // Menu Schools
        "manage_schools" => array(
            array("crumb_desc" => "Schools",
                "crumb_url" => "manage.php?page=manage_schools"
            ),
            array("crumb_desc" => "List",
                "crumb_url" => "manage.php?page=manage_schools"
            )
        ),
        "manage_departments" => array(
            array("crumb_desc" => "Schools",
                "crumb_url" => "manage.php?page=manage_schools"
            ),
            array("crumb_desc" => "Departments",
                "crumb_url" => "manage.php?page=manage_departments"
            )
        ),
        
        // Menu Users
        "man_admin" => array(
            array("crumb_desc" => "Users",
                "crumb_url" => "manage.php?page=man_admin"
            ),
            array("crumb_desc" => "Administrators",
                "crumb_url" => "manage.php?page=man_admin"
            )
        ),
        "assist" => array(
            array("crumb_desc" => "Users",
                "crumb_url" => "manage.php?page=assist"
            ),
            array("crumb_desc" => "Assistants",
                "crumb_url" => "manage.php?page=assist"
            )
        ),
        "manage_examiners" => array(
            array("crumb_desc" => "Users",
                "crumb_url" => "manage.php?page=manage_examiners"
            ),
            array("crumb_desc" => "Exam Team",
                "crumb_url" => "manage.php?page=manage_examiners"
            )
        ),
        "edit_examiners" => array(
            array("crumb_desc" => "Users",
                "crumb_url" => "manage.php?page=manage_examiners"
            ),
            array("crumb_desc" => "Exam Team",
                "crumb_url" => "manage.php?page=manage_examiners"
            ),
            array("crumb_desc" => "Edit",
                "crumb_url" => "manage.php?page=edit_examiners"
            )
        ),
        
        // Menu Students
        "manage_examinees" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "List",
                "crumb_url" => "manage.php?page=manage_examinees"
            )
        ),
        "update_examinees" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "List",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "Add/Update",
                "crumb_url" => "manage.php?page=update_examinees"
            )
        ),
        "examinees_uploadimages" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "Images",
                "crumb_url" => "manage.php?page=examinees_uploadimages"
            )
        ),        
        "manage_courses" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "Course &amp; Modules",
                "crumb_url" => "manage.php?page=manage_courses"
            )
        ),
        "manage_years" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "Course &amp; Modules",
                "crumb_url" => "manage.php?page=manage_courses"
            ),
            array("crumb_desc" => "Course Years",
                "crumb_url" => "manage.php?page=manage_years"
            )
        ),
        "manage_modules" => array(
            array("crumb_desc" => "Students",
                "crumb_url" => "manage.php?page=manage_examinees"
            ),
            array("crumb_desc" => "Course &amp; Modules",
                "crumb_url" => "manage.php?page=manage_courses"
            ),
            array("crumb_desc" => "Course Years",
                "crumb_url" => "manage.php?page=manage_years"
            ),
            array("crumb_desc" => "Year Modules",
                "crumb_url" => "javascript:void(0)"
            )
        ),        
        
        
        // Menu OSCE=>Manage & Analysis
        "manage_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            )
        ),
        "settings_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Settings",
                "crumb_url" => "javascript:void(0)"
            )
        ),
        "sessions_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            )
        ),
        "stations_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            ),
            array("crumb_desc" => "Stations",
                "crumb_url" => "manage.php?page=stations_osce"
            )
        ),
        "examiner_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            ),
            array("crumb_desc" => "Stations",
                "crumb_url" => "manage.php?page=stations_osce"
            ),
            array("crumb_desc" => "Examiners",
                "crumb_url" => "manage.php?page=examiner_osce"
            )
        ),
        "groups_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            ),
            array("crumb_desc" => "Groups",
                "crumb_url" => "manage.php?page=groups_osce"
            )
        ),
        "assistants_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            ),
            array("crumb_desc" => "Assistants",
                "crumb_url" => "manage.php?page=assistants_osce"
            )
        ),
        "forms_osce" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Station Bank",
                "crumb_url" => "manage.php?page=forms_osce"
            )
        ),
        "bankform" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Station Bank",
                "crumb_url" => "manage.php?page=forms_osce"
            ),
            array("crumb_desc" => "Edit Content",
                "crumb_url" => "manage.php?page=forms_osce"
            )
        ),
        "examform" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Days/Circuits",
                "crumb_url" => "manage.php?page=sessions_osce"
            ),            
            array("crumb_desc" => "Edit Content",
                "crumb_url" => "manage.php?page=forms_osce"
            )
        ),    
         "upload" => array(
            array("crumb_desc" => "OSCE",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
            array("crumb_desc" => "Manage &amp; Analysis",
                "crumb_url" => "manage.php?page=manage_osce"
            ),
             array("crumb_desc" => "Upload Media",
                "crumb_url" => "manage.php?page=upload"
            )
        )    
        
    );

    public function __construct()
    {
        
        /**
         *  Apply translations to breadcrumbs,
         *  There are more efficient ways of doing this, but due to time contraints
         *  we'll have to leave this to a later date
         */

        // Manage terms
        $this->page['manage_terms'][1]['crumb_desc'] = gettext('Terms');

        // 'OSCE' Crumb Translation
        $this->page['manage_osce'][0]['crumb_desc'] = gettext('Osces');
        $this->page['settings_osce'][0]['crumb_desc'] = gettext('Osces');
        $this->page['examform'][0]['crumb_desc'] = gettext('Osces');
        $this->page['formosce'][0]['crumb_desc'] = gettext('Osces');
        $this->page['bankform'][0]['crumb_desc'] = gettext('Osces');
        $this->page['forms_osce'][0]['crumb_desc'] = gettext('Osces');
        $this->page['stations_osce'][0]['crumb_desc'] = gettext('Osces');
        $this->page['sessions_osce'][0]['crumb_desc'] = gettext('Osces');
        
        // 'Station Bank' Crumb Translation
        $this->page['forms_osce'][1]['crumb_desc'] = ucwords(gettext('form')) . " Bank";
        $this->page['bankform'][1]['crumb_desc'] = ucwords(gettext('form')) . " Bank";
        
        // 'Students' Crumb Translation
        $this->page['manage_examinees'][0]['crumb_desc'] = gettext('Students');
        $this->page['update_examinees'][0]['crumb_desc'] = gettext('Students');
        $this->page['examinees_uploadimages'][0]['crumb_desc'] = gettext('Students'); 

        // Courses & Modules
        $this->page['manage_courses'][0]['crumb_desc'] = gettext('Students');
        $this->page['manage_courses'][1]['crumb_desc'] = gettext('Course & Modules');

        $this->page['manage_years'][0]['crumb_desc'] = gettext('Students');
        $this->page['manage_years'][1]['crumb_desc'] = gettext('Course & Modules');
        $this->page['manage_years'][2]['crumb_desc'] = gettext('Course Years');

        $this->page['manage_modules'][0]['crumb_desc'] = gettext('Students');
        $this->page['manage_modules'][1]['crumb_desc'] = gettext('Course & Modules');
        $this->page['manage_modules'][2]['crumb_desc'] = gettext('Course Years');
        $this->page['manage_modules'][3]['crumb_desc'] = gettext('Year Modules');
    
        // Schools Crumb Translation
        $this->page['manage_schools'][0]['crumb_desc'] = ucwords(gettext('schools'));

        // 'Departments' Crumb Translation
        $this->page['manage_departments'][0]['crumb_desc'] = ucwords(gettext('schools'));

        // 'Departments' Crumb Translation
        $this->page['manage_departments'][1]['crumb_desc'] = gettext('Departments');     
        
        // 'Exam Team' Crumb Translation
        $this->page['manage_examiners'][1]['crumb_desc'] = gettext('Exam-Team-Placeholder') . gettext(' Team');
        $this->page['edit_examiners'][1]['crumb_desc'] = gettext('Exam-Team-Placeholder') . gettext(' Team');
        
        // 'Session Groups' Crumb Translation
        $this->page['groups_osce'][0]['crumb_desc'] = gettext('Osces');

        $this->page['groups_osce'][3]['crumb_desc'] = gettext('Groups');
        
        // 'Session Assistants' Crumb Translation
        $this->page['assistants_osce'][0]['crumb_desc'] = gettext('Osces');
        
        // 'Station Examiners' Crumb Translation
        $this->page['examiner_osce'][0]['crumb_desc'] = gettext('Osces');     
        $this->page['examiner_osce'][4]['crumb_desc'] = ucwords(gettext('examiners')); 
       
        $this->setParameters();
        $this->addParameters();

        $this->setBreadCrumb();
        $this->outputBreadCrumb();
    }

    // Prepare Parameters
    public function setParameters()
    {
        //Selected course year
        if ($_GET['page'] == 'manage_years' && isset($_GET['crs'])) {
            $_SESSION['breadcrumbs_course'] = $_GET['crs'];
        }

        //Selected exam
        if (in_array($_GET['page'], array('stations_osce', 'groups_osce', 'assistants_osce', 'sessions_osce')) && isset($_GET['exam_id'])) {
            $_SESSION['breadcrumbs_exam'] = $_GET['exam_id'];
        }

        //Session to display in session list page
        if (in_array($_GET['page'], array('stations_osce', 'groups_osce', 'assistants_osce', 'sessions_osce', 'examiner_osce')) && isset($_GET['show_session'])) {
            $_SESSION['breadcrumbs_session_show'] = $_GET['show_session'];
        }

        //Selected session
        if (in_array($_GET['page'], array('stations_osce', 'groups_osce', 'assistants_osce', 'sessions_osce', 'examiner_osce')) && isset($_GET['session_id'])) {
            $_SESSION['breadcrumbs_session'] = $_GET['session_id'];
        }
    }

// Parameters to be Appended
    public function addParameters()
    {
        $page_id = filter_input(INPUT_GET, 'page');
        
        switch ($page_id) {
            case 'stations_osce':
            case 'groups_osce':
            case 'assistants_osce':
            case 'examform':
                $parameters = '&amp;exam_id=' . $_SESSION['breadcrumbs_exam'] 
                            . '&amp;show_session=' . $_SESSION['breadcrumbs_session_show'];
                $this->page[$page_id][2]['crumb_url'] .= $parameters;
                break;
            
            case 'examiner_osce':
                // Manage Sessions link
                $parameters = '&amp;exam_id=' . $_SESSION['breadcrumbs_exam'] 
                            . '&amp;show_session=' . $_SESSION['breadcrumbs_session_show'];
                $this->page['examiner_osce'][2]['crumb_url'] .= $parameters;

                // Manage Stations Link
                $parameters = '&amp;session_id=' 
                            . $_SESSION['breadcrumbs_session'] . '&amp;show_session=' 
                            . $_SESSION['breadcrumbs_session_show'];
                $this->page['examiner_osce'][3]['crumb_url'] .= $parameters;
                break;
            
            case 'manage_modules':
                $parameters = '&amp;crs=' . $_SESSION['breadcrumbs_course'];
                $this->page['manage_modules'][2]['crumb_url'] .= $parameters;
                break;
            
            default:
                // Do nothing.
        }
    }

    public function setBreadCrumb()
    {
        if (array_key_exists($_GET['page'], $this->page)) {
            $numItems = count($this->page[$_GET['page']]);
            $index = 0;

            foreach ($this->page[$_GET['page']] as $part) {
                // create breadcrumb string
                $index++;
                if ($index === $numItems) {
                    $this->crumbs .= "<li class='breadcrumb-item active' id='breadcrumb_last'>" . $part["crumb_desc"] . "</li>";
                } else {
                    $this->crumbs .= "<li class='breadcrumb-item'><a class='breadcrumb-link' href=\"" . $part["crumb_url"] . "\">" . $part["crumb_desc"] . "</a></li>";
                }
            }
            
        }
    }

    public function outputBreadCrumb()
    {
        if (strlen($this->crumbs) > 0) {
             ?><div><ul id="breadcrumbs" class="breadcrumb"><?=$this->crumbs?></ul></div><?php
        }
    }

}

