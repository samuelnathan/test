<?php
namespace OMIS\Session;
/**
 * Message object used to store and render flash messages.
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class FlashMessage
{
    /* Numbering scheme for the SESSION_FLASH constants matches the list of 
     * PSR-3 log levels of the same names assuming they're arranged in the
     * usual way in a zero-based list.
     */
    const SESSION_FLASH_ERROR = 3;
    const SESSION_FLASH_WARNING = 4;
    const SESSION_FLASH_INFO = 6;
    
    public $id;
    public $type;
    public $message;
    
    private $valid_message_types = [
        self::SESSION_FLASH_ERROR,
        self::SESSION_FLASH_WARNING,
        self::SESSION_FLASH_INFO
    ];
    
    /**
     * Constructor function.
     * 
     * @param int    $id      The message's ID
     * @param int    $type    The message type
     * @param string $message The message body
     * @throws \InvalidArgumentException
     */
    public function __construct($id, $type, $message) {
        // Sanity check the message type...
        if (!in_array($type, $this->valid_message_types)) {
            $error_message = "Specified message type '$type' is invalid.";
            error_log(__METHOD__ . ": $error_message");
            throw new \InvalidArgumentException($error_message);
        }
        
        // Make sure the message ID is a valid unsigned integer.
        if (!is_int($id) && ($id < 0)) {
            throw new \InvalidArgumentException("Invalid message ID");
        }
        
        $this->type = $type;
        
        $message = trim($message);
        
        if (empty($message)) {
            throw new \InvalidArgumentException("No message specified");
        }
        
        $this->id = $id;
        $this->message = $message;
    }
    
    /**
     * Magic method used by echo, print_r, etc.
     * 
     * @return string String representation of the object.
     */
    public function __toString()
    {
        switch ($this->type) {
            case self::SESSION_FLASH_ERROR:
                $message_type = "error";
                break;
            case self::SESSION_FLASH_INFO:
                $message_type = "info";
                break;
            case self::SESSION_FLASH_WARNING:
                $message_type = "warning";
                break;
            default:
                $message_type = "unknown";
        }
        
        return sprintf("#%d - (%s) %s", $this->id, $message_type, $this->message);
    }
}
