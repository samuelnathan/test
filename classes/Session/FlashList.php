<?php
namespace OMIS\Session;

use \OMIS\Utilities\Path as Path;
/**
 * List of flash objects as stored in the $_Session object.
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */

class FlashList
{
    /* Define the $_SESSION variable we're going to use to store the list of
     * messages.
     */
    const SESSION_FLASH_STORAGE = 'flash';
    
    public function __construct()
    {
        if (isset($_SESSION[self::SESSION_FLASH_STORAGE])) {
            // Session object already exists...
            if (!is_array($_SESSION[self::SESSION_FLASH_STORAGE])) {
                // If it's not an array, promote it to one
                $messages = array($_SESSION[self::SESSION_FLASH_STORAGE]);
            } else {
                $messages = $_SESSION[self::SESSION_FLASH_STORAGE];
            }
            
            /* $correct_type will be TRUE if all of the items in the array are
             * of the correct type.
             */
            $bad_message_count = count(array_filter(
                $messages,
                function ($v) { return (!($v instanceof \OMIS\Session\FlashMessage)); }
            ));
            
            if ($bad_message_count > 0) {
                throw new \RuntimeException("Bad data found in flash storage");
            }
        }
    }
    
    /**
     * Get the number of messages in the flash buffer.
     * 
     * @return type
     */
    public function count() {
        $count = (isset($_SESSION[self::SESSION_FLASH_STORAGE])) ? count($_SESSION[self::SESSION_FLASH_STORAGE]) : 0;
        return $count;
    }
    
    /**
     * Clear the flash message buffer
     * 
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function clear() {
        try {
            unset($_SESSION[self::SESSION_FLASH_STORAGE]);
            $_SESSION[self::SESSION_FLASH_STORAGE] = [];
            return true;
        } catch (Exception $e) {
            error_log(__METHOD__ . ": Failed to clear flash message buffer - " . $e->getMessage());
            return false;
        }
    }
    
    /**
     * Get a list of the message IDs known to the system.
     * 
     * @return int[] List of IDs for the messages in the system.
     */
    private function getAllIDs() {
        if (empty($_SESSION[self::SESSION_FLASH_STORAGE])) {
            return [];
        }
        
        $ids = [];
        foreach ($_SESSION[self::SESSION_FLASH_STORAGE] as $message) {
            $ids[] = $message->id;
        }
        
        return $ids;
    }
    
    /**
     * Attempt to select an ID number to use. Ideally, the message should have a
     * pre-defined ID (such that it can be used with i18n stuff etc.) but if it
     * doesn't, then...
     * 
     * @return int The ID number to use for the message.
     * @throws \OutOfBoundsException
     */
    private function generateID() {
        $chosen_id = 1;
        $used_ids = $this->getAllIDs();
        
        if (count($used_ids) >= PHP_INT_MAX) {
            error_log(__METHOD__ . ": No ID available to assign to new message");
            throw new \OutOfBoundsException("How big are you trying to make this??");
        }
        
        // (DW) Potential for infinite loop here, but shouldn't happen...
        do {
            $chosen_id = mt_rand(0, PHP_INT_MAX);
        } while (in_array($chosen_id, $used_ids));
        
        return $chosen_id;
    }
    
    /**
     * Add a new message to the flash queue.
     * 
     * @param string   $message Message body
     * @param int      $type    Message Type (warning, error, etc.)
     * @param int|null $id      If NULL, then generate an ID. Otherwise an integer.
     * @return mixed The message's ID number on success, FALSE on failure.
     */
    public function add($message, $type = FlashMessage::SESSION_FLASH_INFO, $id = null) {
        // Sanity check the message body
        if (empty($message) || (trim($message) == "")) {
            error_log(__METHOD__ . ": Attempted to add empty flash message");
            return FALSE;
        }
        
        if (is_null($id)) {
            try {
                $id = $this->generateID();
            } catch (\OutOfBoundsException $ex) {
                return FALSE;
            }
        }
        
        try {
            $message_item = new FlashMessage($id, $type, $message);
        } catch (Exception $ex) {
            error_log(__METHOD__ . ": Failed to instantiate new message item" . $ex->getMessage());
            return FALSE;
        }
        
        try {
            $_SESSION[self::SESSION_FLASH_STORAGE][] = $message_item;
        } catch (Exception $ex) {
            error_log(__METHOD__ . ": Failed to add item - " . $ex->getMessage());
            return FALSE;
        }
        
        return $id;
    }
    
    /**
     * Delete a message with the specified ID.
     * 
     * @param int $message_id ID number of message to delete
     * @return TRUE on success, FALSE otherwise.
     */
    public function delete($message_id) {
        $message_ids = $this->getAllIDs();
        if (in_array($message_id, $message_ids)) {
            // The message exists...
            $deleted = FALSE;
            foreach ($_SESSION[self::SESSION_FLASH_STORAGE] as $index => $message) {
                if ($message->id === $message_id) {
                    unset($_SESSION[self::SESSION_FLASH_STORAGE][$index]);
                    $deleted = TRUE;
                    break;
                }
            }
            
            return $deleted;
        } else {
            // Message doesn't exist.
            return FALSE;
        }
    }
    
    public function display() {
        if (empty($_SESSION[self::SESSION_FLASH_STORAGE])) {
            // If there's nothing to display, there's nothing to display...
            return;
        }
        
        // If we get to here, then there's something to display.
        global $config;
        if (!isset($config)) {
            $config = new \OMIS\Config();
        }
        
        $template_path = "flash.html.twig";
        try {
            $template = new \OMIS\Template($template_path);
        } catch (Exception $ex) {
            error_log(__METHOD__ . ": Failed to render flash messages - " . $ex->getMessage());
            return FALSE;
        }
        
        $template->data['image_folder'] = "assets/images";
        $template->data['messages'] = $_SESSION[self::SESSION_FLASH_STORAGE];
        $template->render();
        
        // Clear the message buffer.
        $this->clear();
    }
}
