<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 07/01/2014
 * Â© 2014 Qpercom Limited. All rights reserved.
 * @Omis Session Management Class introduced for Omis versions 1.8.5 +
 */
namespace OMIS;

use \OMIS\Utilities\Request as Request;
use \OMIS\Utilities\Path as Path;

class Session
{
    const SESSION_EXIT_WARNING = 1;
    const SESSION_EXIT_REDIRECT = 2;
    
    private $config;
    private $page;
    private $exitActionType;
    private static $installationid_key = 'instance_identifier';

    #Constructor
    public function __construct()
    {
        // Start session. This is the right way to do it for PHP 5.4 and up.
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        #Load config file if not done so already
        global $config;
        $this->config = (isset($config)) ? $config : new Config;
    }
    
    /**
     * Verifies that the Session variable set for the current user has login
     * credentials for this specific instance of OMIS rather than another one.
     * 
     * @return boolean TRUE if user is logged into this session; FALSE if not.
     */
    private function correctInstance()
    {
        if (isset($_SESSION) && in_array(self::$installationid_key, array_keys($_SESSION))) {
            $instance_id = $_SESSION[self::$installationid_key];
            if (empty($instance_id)) {
                // Session not set.
                return false;
            } else {
                return ($instance_id == $this->config->installation_id);
            }
        }
        
        // If we get to there, then the session is not set.
        return false;
    }

    #Critical Session Check
    public function check($exitAction = self::SESSION_EXIT_WARNING)
    {
        #Global Page handle
        global $page;
        $this->page = $page;
        $this->exitActionType = $exitAction;

        /* NB: With regards to the 'instance_identifier' not matching with
         * config file 'installation_id' setting then deny access and destroy 
         * session.
         * (This might require a rethink, not sure if it is a good idea to 
         * destroy the users session if a user (e.g. Thomas/Liam) jumps to
         * a client's system on the same web server while logged into another 
         * client's system already) */
        if (!$this->correctInstance()) {
            /* Destroy session even if 'instance_identifier' is not set in case
             * there any other session variables set
             */
            session_destroy();
            $this->exitAction(); #Call exit action
        }
    }

    private function exitAction()
    {
        /* It turns out that the HTTP protocol doesn't support an explicit
         * message that says "your session has timed out". 403 means "your
         * credentials are no good, don't bother me again" whereas 401 (a more
         * promising "Unauthorized") insists on falling back on creaky
         * authentication built into HTTP which is both insecure and won't work
         * with our system. So, plan B is to force a redirect to the login page.
         */
        // If the request is for something other than a web page, then return
        // a bare HTTP response code rather than a redirect.
        if (Request::preferredType() != "text/html") {
            http_response_code(403);
            die();
        }

        // If we are looking for HTML, then we have options.
        switch ($this->exitActionType) {
            case self::SESSION_EXIT_WARNING:
                #echo out warning message and exit script
                die("<div class = 'errordiv'>Session timed out / Not logged in</div>");
                break;
            case self::SESSION_EXIT_REDIRECT:
            default:
                $url = Path::toURL(Path::join($this->config->base_path, 'index.php'));
                header("Location: $url", true, 302);
                exit();
        }
    }
    
    // Decode a session file into an array.
    // unserialize(), unserialize_php() and unserialize_phpbinary() were
    public static function unserialize($session_data)
    {
        $method = ini_get("session.serialize_handler");
        switch ($method) {
            case "php":
                return self::unserialize_php($session_data);
                break;
            case "php_binary":
                return self::unserialize_phpbinary($session_data);
                break;
            default:
                throw new Exception("Unsupported session.serialize_handler: " . $method . ". Supported: php, php_binary");
        }
    }

    private static function unserialize_php($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            if (!strstr(substr($session_data, $offset), "|")) {
                throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
            }
            $pos = strpos($session_data, "|", $offset);
            $num = $pos - $offset;
            $varname = substr($session_data, $offset, $num);
            $offset += $num + 1;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }

    private static function unserialize_phpbinary($session_data) {
        $return_data = array();
        $offset = 0;
        while ($offset < strlen($session_data)) {
            $num = ord($session_data[$offset]);
            $offset += 1;
            $varname = substr($session_data, $offset, $num);
            $offset += $num;
            $data = unserialize(substr($session_data, $offset));
            $return_data[$varname] = $data;
            $offset += strlen(serialize($data));
        }
        return $return_data;
    }
}