<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

/**
 * Check if the option for weighted item scores is included
 * @package OMIS\Feedback\Options\Impl
 */
class WeightedItemFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $this->getConfig()->exam_defaults['item_weighting'] && $db->exams->hasWeightedItem($examId);
    }

}