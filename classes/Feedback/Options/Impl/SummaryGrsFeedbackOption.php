<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

class SummaryGrsFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->hasGRSSection($examId);
    }

}