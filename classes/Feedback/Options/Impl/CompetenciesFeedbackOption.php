<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

class CompetenciesFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->hasLinkedCompetencies($examId);
    }

}