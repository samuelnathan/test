<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

class ObserverRatingsFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->hasObserverResults($examId);
    }

}