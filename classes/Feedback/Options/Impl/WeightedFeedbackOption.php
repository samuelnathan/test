<?php

namespace OMIS\Feedback\Options\Impl;

use OMIS\Database\CoreDB;

/**
 * Check if the option for weighted scores is included
 * @package OMIS\Feedback\Options\Impl
 */
class WeightedFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->examHasWeightingStations($examId) || $db->exams->hasAdjustedOutcomes($examId)
        || (new WeightedItemFeedbackOption($db, $this->getConfig()))->isOptionEnabledForExam($examId);
    }

}