<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

class ExaminerCommentsFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->hasFeedback($examId);
    }

}