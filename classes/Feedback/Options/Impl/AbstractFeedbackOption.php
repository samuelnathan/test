<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Config;
use OMIS\Database\CoreDB;
use OMIS\Feedback\Options\FeedbackOption;

/**
 * Partial implementation of FeedbackOption that relies on the database to perform the check
 *
 * @package OMIS\Feedback\Options\Impl
 */
abstract class AbstractFeedbackOption implements FeedbackOption
{

    /**
     * @var CoreDB
     */
    private $db;

    /**
     * @var Config
     */
    private $config;

    public function __construct(CoreDB $db, Config $config)
    {
        $this->db = $db;
        $this->config = $config;
    }

    public function isOptionEnabledForExam($examId)
    {
        return $this->isOptionEnabledForExamWithDb($this->db, $examId);
    }

    abstract protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId);

    protected function getConfig() {
        return $this->config;
    }

}