<?php

namespace OMIS\Feedback\Options\Impl;


use OMIS\Database\CoreDB;

class FormNotesFeedbackOption extends AbstractFeedbackOption
{

    protected function isOptionEnabledForExamWithDb(CoreDB $db, $examId)
    {
        return $db->exams->examHasFormsWithNotes($examId);
    }

}