<?php

namespace OMIS\Feedback\Options;

use OMIS\Config;
use OMIS\Database\CoreDB;

class FeedbackOptionFilter
{

    /**
     * @var Config
     */
    private $config;

    /**
     * Array that associates the option name with the class that implements the logic to check whether it's enabled
     * or not
     * @var array
     */
    private static $optionClasses = [
        'weighted' => 'OMIS\Feedback\Options\Impl\WeightedFeedbackOption',
        'weighted-items' => 'OMIS\Feedback\Options\Impl\WeightedItemFeedbackOption',
        'notes'=> 'OMIS\Feedback\Options\Impl\FormNotesFeedbackOption',
        'multiple-examiners' => 'OMIS\Feedback\Options\Impl\MultipleExaminersFeedbackOption',
        'examiner-comments' => 'OMIS\Feedback\Options\Impl\ExaminerCommentsFeedbackOption',
        'observer-ratings' => 'OMIS\Feedback\Options\Impl\ObserverRatingsFeedbackOption',
        'competencies' => 'OMIS\Feedback\Options\Impl\CompetenciesFeedbackOption',
        'summary-grs' => 'OMIS\Feedback\Options\Impl\SummaryGrsFeedbackOption'
    ];

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Given a list of named options, returns the subset of options that are enabled for a given exam
     *
     * @param array $options
     * @param $examId
     * @return array
     */
    public function filterEnabledOptions(array $options, $examId) {
        $optionInstances = $this->getOptionInstances();

        return array_values(array_filter($options, function ($optionName) use ($optionInstances, $examId) {
            // If the option is not implemented, exclude it
            if (!array_key_exists($optionName, $optionInstances))
                return false;

            /** @var FeedbackOption $option */
            $option = $optionInstances[$optionName];

            return $option->isOptionEnabledForExam($examId);
        }));
    }

    private function getOptionInstances() {
        $db = CoreDB::getInstance();
        $result = [];

        foreach (self::$optionClasses as $optionName => $optionClass) {
            $reflectionClass = new \ReflectionClass($optionClass);
            $result[$optionName] = $reflectionClass->newInstanceArgs([$db, $this->config]);
        }

        return $result;
    }

}