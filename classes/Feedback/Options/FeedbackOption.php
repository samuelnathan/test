<?php

namespace OMIS\Feedback\Options;


interface FeedbackOption
{

    /**
     * Checks if the option is enabled for a given exam
     *
     * @param $examId
     * @return boolean
     */
    public function isOptionEnabledForExam($examId);

}