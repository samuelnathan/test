<?php
namespace OMIS\Imaging;

use \OMIS\Utilities\Path as Path;
/**
 * Image manipulation class using the ImageMagick imaging library
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
class ImageMagick extends ImageManipulator
{
    const IMAGEMAGICK_RESIZE_ALGORITHM = \IMagick::FILTER_CATROM;
    const IMAGEMAGICK_RESIZE_SHARPNESS = 0.9;
    
    public function __construct($path)
    {
        /* Pass the file to ImageMagick and see what it thinks. If the image
         * loads, try to get its attributes.
         */
        try {
            $this->image = new \Imagick($path);
            $attributes = $this->image->identifyImage();
        } catch (ImagickException $e) {
            $error = "Cannot open/parse $path with ImageMagick - " . $e->getMessage();
            $this->logMessage($error);
            return false;
        }
        
        list($this->width, $this->height) = array_values($attributes['geometry']);
        $this->format = strtoupper(explode(" ", $attributes['format'])[0]);
        
        if (!in_array($this->format, $this->knownFormats)) {
            $error = "File type $this->format is unsupported, sorry";
            error_log(__METHOD__ . ": $error");
            throw new RuntimeException($error);
        }
        
        // Call the parent's constructor.
        parent::__construct($path);
    }
    
    public function __destruct()
    {
        /* If the object is probably an ImageMagick image, then destroy it the
         * "correct" way; if not, assume it's already destroyed, so success!
         */
        if (is_object($this->image)) {
            $success = ($this->image->clear() === true);
        } else {
            $success = true;
        }
        
        if (!$success) {
            $error = "Failed to destroy ImageMagick image object for some reason";
            error_log(__METHOD__ . ": $error");
            throw new \RuntimeException($error);
        }
        
        parent::__destruct();
    }
    
    /**
     * Converts an image from one format to another using the ImageMagick 
     * Library.
     * 
     * @param string $format      One of: "JPEG", "GIF", "PNG".
     * @param string $destination Path to the desired destination image.
     * @return boolean True on success, false on failure.
     */
    public function convert($format, $destination)
    {
        return $this->image->writeImage($format . ":" . $destination);
    }
    
    /**
     * Crop the image so that what's left is the area between ($top, $left) and
     * ($right, $bottom).
     * 
     * Throws an exception if the crop process fails.
     * 
     * @param resource $src_image GD resource representing the source image.
     * @param int      $top       y-coordinate of top of crop box
     * @param int      $left      x-coordinate of top of crop box
     * @param int      $bottom    y-coordinate of bottom of crop box
     * @param int      $right     x-coordinate of bottom of crop box
     * @return resource GD Image resource representing the destination image.
     * @throws \Exception
     */
    protected function crop($src_image, $top, $left, $bottom, $right)
    {
        // Clone the source image before cropping.
        $dst_image = clone $src_image;
        
        // Calculate the width and height of the cropped area
        $width = abs($right - $left);
        $height = abs($bottom - $top);
        
        //Do the crop.
        $success = $dst_image->cropImage($width, $height, $top, $left);
        if (!$success) {
            $error = "Failed to crop $this->path to ($top, $left, $bottom, $right)";
            error_log(__METHOD__ . ": $error");
            throw new Exception($error);
        } else {
            return $dst_image;
        }
    }
    
    /**
     * Resize an image to $dst_w x $dst_h using GD.
     * 
     * @param resource $src_image GD Image Resource specifying image to resize
     * @param type     $dst_w     Desired width of destination image
     * @param type     $dst_h     Desired height of destination image
     * @return resource GD Image resource specifying destination image
     * @throws \Exception
     */
    protected function resize($src_image, $dst_w, $dst_h)
    {
        // Clone the source image before resizing.
        $dst_image = clone $src_image;
        
        $success = $dst_image->resizeImage(
            $dst_w,
            $dst_h,
            self::IMAGEMAGICK_RESIZE_ALGORITHM,
            self::IMAGEMAGICK_RESIZE_SHARPNESS,
            true
        );
        
        if (!$success) {
            $error = "Failed to resize $this->path to ($dst_w, $dst_h)";
            error_log(__METHOD__ . ": $error");
            throw new Exception($error);
        } else {
            return $dst_image;
        }
    }
    
    /**
     * Write an ImageMagick image to file.
     * 
     * @param resource $image       Imagemagick object
     * @param string   $destination Path to desired output file.
     * @param string   $format      "PNG", "GIF" etc.
     * @return boolean TRUE on success, FALSE on failure.
     */
    protected function save($image, $destination, $format = null)
    {
        if ($format == null) {
            if (empty($this->format)) {
                $error = "No format specified for image to be written!";
                error_log(__METHOD__ . ": $error");
                return false;
            } else {
                $format = $this->format;
            }
        }
        
        // Ensure that the relevant folder is creatable and created before saving
        if (!Path::mkdir($destination)) {
            $error = "Cannot create directory for '$destination'";
            error_log(__METHOD__ . ": $error");
            return false;
        }
        
        return $image->writeImage("$format:$destination");
    }
}
