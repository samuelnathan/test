<?php
namespace OMIS\Imaging;

use \OMIS\Utilities\Path as Path;

/**
 * Base class for image manipulation classes
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
abstract class ImageManipulator
{
    // Define constants for image file formats.
    const IMAGE_FORMAT_JPEG = "JPEG";
    const IMAGE_FORMAT_GIF = "GIF";
    const IMAGE_FORMAT_PNG = "PNG";

    // Speciality settings for specific image types
    // @TODO Move this to a configuration file?
    const IMAGE_JPEG_QUALITY = 90;

    // How many digits to round aspect ratios to (useful for comparison purposes)
    const IMAGE_ASPECT_PRECISION = 3;

    protected $image = null;
    protected $width = -1;
    protected $height = -1;
    protected $path = "";
    protected $format = "";
    protected $filesize = -1;

    protected $knownFormats = array(
        self::IMAGE_FORMAT_JPEG,
        self::IMAGE_FORMAT_GIF,
        self::IMAGE_FORMAT_PNG
    );

    /* These functions need to be defined in any class that subclasses this one
     * to implement another imaging library/system.
     */

    abstract public function convert($format, $destination);
    abstract protected function crop($src_image, $top, $left, $bottom, $right);
    abstract protected function resize($src_image, $dst_w, $dst_h);
    abstract protected function save($image, $destination, $format = null);

    /**
     * Constructor.
     *
     * @param string $path Path to the image file.
     */
    public function __construct($path)
    {
        // If the file exists, get its size.
        if (file_exists($path)) {
            $this->filesize = filesize($path);
        }
    }

    public function __destruct()
    {
        /* This is needed as PHP caches the state of files when they're operated
         * on and can potentially return invalid status information See
         * http://www.php.net/manual/en/function.clearstatcache.php for more.
         */
        clearstatcache();
    }

    /**
     * Calculate the aspect ratio of an image, size $width x $height, to
     * $precision decimal places.
     *
     * @param  int   $width     Width of image
     * @param  int   $height    Height of image
     * @param  int   $precision Number of decimal places to round to
     * @return float
     */
    public static function calculateAspectRatio($width, $height, $precision = self::IMAGE_ASPECT_PRECISION)
    {
        return round($width / $height, $precision, PHP_ROUND_HALF_UP);
    }

    /**
     * Copy current file to a new destination.
     *
     * @param  string  $destination Destination path for the copied file.
     * @return boolean TRUE on success, FALSE on failure.
     */
    protected function copy($destination)
    {
        if (trim($this->path) == '') {
            return false;
        } else {
            // Make sure the destination folder exists.
            if (!Path::mkdir($destination)) {
                return false;
            }

            // Perform the copy using PHP's built-in function.
            return copy($this->path, $destination);
        }
    }

    /**
     * Calculates and returns the aspect ratio of the current image.
     *
     * @return float The aspect ratio of the image.
     */
    public function getAspectRatio($precision = self::IMAGE_ASPECT_PRECISION)
    {
        return self::calculateAspectRatio($this->width, $this->height, $precision);
    }

    /**
     * Return the dimensions of the image.
     *
     * @return int[] Width and Height of the image.
     */
    public function getDimensions()
    {
        return [$this->width, $this->height];
    }

    /**
     * Returns the file format (JPG, GIF, etc.)
     *
     * @return string A shortcode for the type of file.
     */
    public function getFileFormat()
    {
        return $this->format;
    }

    /**
     * Get the size of the file.
     *
     * @return int Size of the file in bytes. -1 = no such file.
     */
    public function getFileSize()
    {
        if (($this->filesize == -1) && (trim($this->path) != '')) {
            if (file_exists($this->path)) {
                $this->filesize = filesize($this->path);
            }
        }

        return $this->filesize;
    }

    /**
     * Get the path to the image file.
     *
     * @return string Path to the image file.
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Copy a cropped version of this image to $destination.
     *
     * @param  string  $destination Path to destination for cropped file.
     * @param  int     $top         y-coordinate of top of crop box
     * @param  int     $left        x-coordinate of top of crop box
     * @param  int     $bottom      y-coordinate of bottom of crop box
     * @param  int     $right       x-coordinate of bottom of crop box
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copyCropped($destination, $top, $left, $bottom, $right)
    {
        try {
            $cropped_image = $this->crop($this->image, $top, $left, $bottom, $right);

            return $this->save($cropped_image, $destination);
        } catch (Exception $e) {
            $error = "Failed to copy cropped image to $destination - " . $e->getMessage();
            error_log(__METHOD__ . ": $error");

            return false;
        }
    }

    /**
     * Copy a resized version of this image to $destination.
     *
     * @param  string  $destination Path to destination file
     * @param  int     $width       Width of desired image
     * @param  int     $height      Height of desired image
     * @return boolean TRUE on success, FALSE on failure
     */
    public function copyResized($destination, $width, $height)
    {
        try {
            $resized_image = $this->resize($this->image, $width, $height);

            return $this->save($resized_image, $destination);
        } catch (Exception $e) {
            $error = "Failed to copy cropped image to $destination - " . $e->getMessage();
            error_log(__METHOD__ . ": $error");

            return false;
        }
    }

    /**
     * Copy a cropped & resized version of this image to $destination.
     *
     * @param  string  $destination Path to destination for cropped file.
     * @param  int     $top         y-coordinate of top of crop box
     * @param  int     $left        x-coordinate of top of crop box
     * @param  int     $bottom      y-coordinate of bottom of crop box
     * @param  int     $right       x-coordinate of bottom of crop box
     * @param  int     $width       Width of desired image
     * @param  int     $height      Height of desired image
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copyCroppedResized($destination, $top, $left, $bottom, $right, $width, $height)
    {
        try {
            $cropped_image = $this->crop($this->image, $top, $left, $bottom, $right);
            $resized_image = $this->resize($cropped_image, $width, $height);

            return $this->save($resized_image, $destination);
        } catch (Exception $e) {
            $error = "Failed to copy cropped & resized image to $destination - " . $e->getMessage();
            error_log(__METHOD__ . ": $error");

            return false;
        }
    }

    /**
     * Validate that the image dimensions are valid.
     *
     * @param  int     $width
     * @param  int     $height
     * @param  boolean $allow_zeroes Let zeroes be valid?
     * @return boolean
     */
    public static function validateDimensions($width, $height, $allow_zeroes = false)
    {
        foreach (array($width, $height) as $dimension) {
            if (!is_int($dimension) || ($dimension < 0) || (!$allow_zeroes && $dimension == 0)) {
                return false;
            }
        }

        return true;
    }
}
