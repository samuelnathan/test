<?php
/**
 * Image Editing and Manipulation class, used to get image information and to 
 * perform image resizing, crops, and the like.
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
namespace OMIS\Imaging;

require_once __DIR__ . '/../../extra/usefulphpfunc.php';
use OMIS\Utilities\Path as Path;

/**
 * Front-end class for general-purpose image manipulation using whatever
 * supported image manipulation library is available.
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class Image
{
    /* Internet Explorer 8 has a size limit of 32kB on images it can serve
     * inline in a page as base64. None of the other supported major browsers
     * have this limit.
     */
    const IMAGE_MAXSIZE_INLINE = 32768;
    
    private $imageExtension = '';
    /* Supported PHP image libraries/extensions, in decreasing order of
     * preference (most desirable first).
     */
    private $knownExtensions = ['gd', 'imagick'];
    private $knownFormats = [
        ImageManipulator::IMAGE_FORMAT_GIF,
        ImageManipulator::IMAGE_FORMAT_JPEG,
        ImageManipulator::IMAGE_FORMAT_PNG
    ];

    // Which of $known_extensions are available on this system, enumerated on startup.
    private $availableExtensions = array();
    
    // Path to the actual image.
    private $path;
    
    // Image and its dimensions;
    private $image;     // Could be either a GD object or an ImageMagick one.
    
    // Reference to the system's primary configuration.
    private $config;
    
    /**
     * Class constructor
     * 
     * @global OMIS\Config $config
     * @param string $imagepath Path to the iamge to load.
     * @param string $preferExtension Either "gd" or "imagick" right now, more to follow.
     * @throws \RuntimeException
     * @throws \Exception
     */
    
    public function __construct($imagepath, $preferExtension = null)
    {
        // Determine which image extensions are available.
        $this->availableExtensions = array_filter(
            $this->knownExtensions,
            "extension_loaded"
        );
        
        global $config;
        if (isset($config)) {
            $this->config = $config;
        } else {
            $this->config = new \OMIS\Config;
        }
        
        // If there are no available libraries, give up.
        if ($this->availableExtensions === []) {
            $error = "No known PHP image libraries available!";
            if ($this->config->debug_mode) {
                error_log(__METHOD__ . ": $error");
            }
            throw new \RuntimeException($error);
        }
        
        /**
         * Decide what image extension we're using to manipulate images.
         */
        if (!is_null($preferExtension)) {
            if (in_array($preferExtension, $this->availableExtensions)) {
                // If the preferred extension is available, choose that.
                $this->imageExtension = $preferExtension;
            } else {
                // If not, use the first available.
                $this->imageExtension = $this->availableExtensions[0];
                if ($this->config->debug_mode) {
                    $error = "Preferred image library $preferExtension not available. "
                        . "Falling back on available option " . $this->imageExtension;
                    error_log(__METHOD__ . ": $error");
                }
            }
        } else {
            // Use the first available one.
            $this->imageExtension = $this->availableExtensions[0];
        }
        
        // Load the image.
        try {
            // Attempt to load the image. Log the error if needs be.
            if (!$this->load($imagepath) && $this->config->debug_mode) {
                $error = "Cannot load image $imagepath.";
                error_log(__METHOD__ . ": $error");
            }
        } catch (\Exception $e) {
            if ($this->config->debug_mode) {
                $error = "An exception occurred loading $imagepath: " . $e->getMessage;
                error_log(__METHOD__ . ": $error");
            }
            /* Now that we've logged the problem, re-throw the exception so that
             * it can be caught
             */
            throw $e;
        }
    }
    
    /**
     * Destructor.
     */
    public function __destruct()
    {
        unset($this->image);
    }
    
    /**
     * Attempts to load the image with the selected image library. Throws a
     * RuntimeException if it doesn't recognize the specified image library.
     * 
     * @return boolean TRUE if load succeeded; FALSE if not.
     */
    private function load($path)
    {
        if (!file_exists($path)) {
            $path = Path::simplify($path);
            if (!file_exists($path)) {
                $error = "Image file $path not found!";
                if ($this->config->debug_mode) {
                    error_log(__METHOD__ . ": $error");
                }
                throw new \RuntimeException($error);
            }
        }
        
        try {
            switch ($this->imageExtension) {
                case 'gd':
                    $this->image = new GD($path);
                    $success = true;
                    break;
                case 'imagick':
                    $this->image = new ImageMagick($path);
                    $success = true;
                    break;
                default:
                    $error = "Unexpected error - load() called with unknown image sizer module '$this->imageExtension'";
                    if ($this->config->debug_mode) {
                        error_log(__METHOD__ . ": $error");
                    }
                    throw new RuntimeException($error);
            }
        } catch (Exception $e) {
            $success = false;
        }
        
        // If the load worked, store the file's details internally.
        if ($success) {
            $this->path = $path;
            $this->filesize = $this->image->getFileSize($path);
        }
        return $success;
    }
    
    /**
     * Returns the aspect ratio (width:height) of the image.
     * 
     * @return float The aspect ratio of the image.
     */
    public function getAspectRatio()
    {
        return $this->image->getAspectRatio();
    }
    
    /**
     * Return the dimensions of the image.
     * 
     * @return int[] Width and Height of the image.
     */
    public function getDimensions()
    {
        return $this->image->getDimensions();
    }
    
    /**
     * Returns the file format (JPG, GIF, etc.)
     * 
     * @return string A shortcode for the type of file.
     */
    public function getFileFormat()
    {
        return $this->image->getFileFormat();
    }
    
    /**
     * Returns the size of the image file.
     * 
     * @return int Size of file.
     */
    public function getFileSize()
    {
        return $this->image->getFileSize();
    }
    
    /**
     * Returns the name of the PHP extension being used to size the image.
     * 
     * @return string The name of the extension chosen to use for image manipulation.
     */
    public function getImageExtension()
    {
        return $this->imageExtension;
    }
    
    /**
     * Convert the currently loaded image to another of the supported formats
     * using our chosen Imaging library.
     * 
     * @param string $format      One of the IMAGE_FORMAT_ constants
     * @param type   $destination Path to the outputted file.
     * @return boolean TRUE if successful, FALSE if not.
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function convert($format, $destination)
    {
        // Check that the desired format is known...
        if (!in_array($format, $this->knownFormats)) {
            $error = "Cannot convert to unknown format $format";
            error_log(__METHOD__ . ": $error");
            return false;
        }
        
        // Sanity check the path to ensure it's not empty or doesn't exist.
        if (empty($destination) || (!file_exists(dirname($destination)))) {
            $error = "Destination is empty/null or destination folder doesn't exist!";
            error_log(__METHOD__ . ": $error");
            return false;
        }
        
        // Do the conversion with our image library of choice.
        return $this->image->convert($format, $destination);
    }
    
    /**
     * Copy the current image to $destination.
     * 
     * @param string $destination Path to copy target
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copy($destination)
    {
        return $this->image->copy($destination);
    }
    
    /**
     * Crop the current image to ($top, $left) -> ($bottom, $right) and write
     * that image to $destination.
     * 
     * @param string $destination Path to store cropped image
     * @param int    $top         Upper y co-ordinate
     * @param int    $left        Left x co-ordinate 
     * @param int    $bottom      Lower y co-ordinate
     * @param int    $right       Right x co-ordinate
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copyCropped($destination, $top, $left, $bottom, $right)
    {
        return $this->image->copyCropped($destination, $top, $left, $bottom, $right);
    }
    
    /**
     * Resize the image to ($width x $height) and save the resulting image as
     * $destination.
     * 
     * @param string $destination Path to file
     * @param int    $width       Width of image after resizing
     * @param int    $height      Height of image after resizing
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copyResized($destination, $width, $height)
    {
        return $this->image->copyResized($destination, $width, $height);
    }
    
    /**
     * Crop the current image to ($top, $left) -> ($bottom, $right). Then resize
     * that to ($width x $height), and save the resulting image as $destination.
     * 
     * @param string $destination Path to file to crop & resize.
     * @param int    $top         Upper y co-ordinate
     * @param int    $left        Left x co-ordinate 
     * @param int    $bottom      Lower y co-ordinate
     * @param int    $right       Right x co-ordinate
     * @param int    $width       Width of image after resizing
     * @param int    $height      Height of image after resizing
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function copyCroppedResized($destination, $top, $left, $bottom, $right, $width, $height)
    {
        return $this->image->copyCroppedResized($destination, $top, $left, $bottom, $right, $width, $height);
    }
    
    /**
     * This determines the MIME type of the image, using the PECL FileInfo
     * extension (where available), otherwise it interprets whatever the
     * image manipulator class thinks the file is.
     * 
     * @param string $path Path to the file.
     * @return mixed MIME type string on success, False on failure.
     */
    private static function getMimeType($path)
    {
        if (function_exists("finfo_open")) {
            // FileInfo PECL extension available...
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime_type = finfo_file($finfo, $path);
            finfo_close($finfo);
        } else {
            /* Interrogate the ImageResizer class to ask it what type the image
             * is and return the appropriate MIME type. If we don't know, throw
             * back "application/octet-stream" as the default response.
             */
            $image = new Image($path);
            switch ($image->getFileFormat()) {
                case ImageManipulator::IMAGE_FORMAT_GIF:
                    $mime_type = "image/gif";
                    break;
                case ImageManipulator::IMAGE_FORMAT_JPEG:
                    $mime_type = "image/jpeg";
                    break;
                case ImageManipulator::IMAGE_FORMAT_PNG:
                    $mime_type = "image/png";
                    break;
                default:
                    // If we don't know what it is, fall back on a default.
                    $mime_type = "application/octet-stream";
            }
        }
        return $mime_type;
    }
    
    /**
     * Render an image as a suitably-formatted Base 64 string and return it, but
     * only if it's small enough to be rendered properly by all of the browsers
     * we support (in particular, IE8 which has a 32kB limit)
     * 
     * @param string $path Path to the image file
     * @return string Base64 encoded image with a small header specifying the
     *                MIME type.
     * @throws \RuntimeException If the image cannot be rendered, this exception is thrown.
     */
    public static function getImageAsInline($path)
    {
        $filesize = filesize($path);
        if ($filesize === false) {
            // The file doesn't exist, or can't be read. Complain and abort.
            $error = __METHOD__ . ": Unable to get file size for '$path'";
            throw new \RuntimeException($error);
        }
        
        /* If the file is definitely larger than the limit, then complain and
         * give up. We're checking against 3/4 of the maximum limit as converting
         * from binary (8 bits per byte) to base64 (6 bits per encoded byte)
         * the outputted string is a third larger.
         */
        if ($filesize >= round(self::IMAGE_MAXSIZE_INLINE * 0.75)) {
            $error = __METHOD__ . ": File too large to render inline ($filesize bytes)";
            throw new \RuntimeException($error);
        }
        
        // Get the MIME type...
        $mime_type = self::getMimeType($path);
        
        /* Generate the full header for the image for it to be rendered inline
         * to see how long the header is...
         */
        $header = "data:$mime_type;base64,";
        
        /* Get the file's contents and convert them to base64 to see how big
         * that'd be...
         */
        $file_contents = file_get_contents($path);
        if ($file_contents === false) {
            // Getting the file contents failed.
            $error = __METHOD__ . ": Failed to read file contents for '$path'";
            throw new \RuntimeException($error);
        }
        
        /* Calculate the length of the file as encoded in base 64, and add that
         * to the length of the header to figure out how large the base64 string
         * that would get returned would be. If it's too large, throw an 
         * exception. If not, return it.
         */
        $file_base64 = base64_encode($file_contents);
        $expected_length = strlen($header) + strlen($file_base64);
        if ($expected_length >= self::IMAGE_MAXSIZE_INLINE) {
            $error = __METHOD__ . ": File too large to render inline ($filesize bytes)";
            throw new \RuntimeException($error);
        } else {
            return $header . $file_base64;
        }
    }
}
