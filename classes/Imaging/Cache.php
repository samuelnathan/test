<?php
namespace OMIS\Imaging;

// Only load the autoloader if it doesn't already exist.
global $loader;
if (!isset($loader)) {
    $loader = require_once __DIR__ . '/../../vendor/autoload.php';
}

use OMIS\Imaging\Image as Image;
use OMIS\Imaging\ImageManipulator as ImageManipulator;
use OMIS\Utilities\Path as Path;

/**
 * Image Cache. Basically, allows for multiple images (PNG, GIF or JPEG) to be
 * stored in a common folder and then retrieved at arbitrary sizes. Every time
 * an image is rendered in a new size, that copy is cached to speed up subsequent
 * requests.
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
class Cache
{
    // Be sure to include the trailing slash here.
    const CACHE_MAIN_DIRECTORY = "originals/";
    
    private $cache_path;
    private $main_folder = self::CACHE_MAIN_DIRECTORY;
    
    /**
     * Constructor for Image cache object.
     * 
     * @global \OMIS\Imaging\OMIS\Config $config Configuration data
     * @param string $path Path to root folder of image cache
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function __construct($path)
    {
        // Get the global configuration settings.
        global $config;
        if (!isset($config)) {
            $config = new OMIS\Config;
        }
        
        $error = "";
        $path = Path::fixSeparators($path);
        
        if ((file_exists($path)) && is_dir($path)) {
            if (!is_readable($path)) {
                $error = "Path '$path' is not readable!";
            }
        } else {
            $error = "Path '$path' does not exist or is not a directory!";
        }
        
        // If we've generated an error, throw it.
        if ($error != '') {
            throw new \InvalidArgumentException($error);
        }
        
        // Specify the path, but also ensure it's got a trailing separator.
        $this->cache_path = rtrim($path, "\\/") . "/";
        
        // Check if the "main" folder exists for this image cache.
        $main_dir_path = Path::join($this->cache_path, self::CACHE_MAIN_DIRECTORY);
        if (!is_dir($main_dir_path)) {
            /* The main folder doesn't exist, so we should try to create it and
             * put in the default image to be ready.
             */
            
            try {
                $failed = (!mkdir($main_dir_path, 0777, true) || !is_dir($main_dir_path));
                $mkdir_exception = "";
            } catch (ErrorException $ex) {
                /* This is to capture the scenario where mkdir() throws an
                 * exception for some reason. Came across this when trying to
                 * create a path separated with backslashes on a Unix system
                 * (NUS' AWS instance, specifically) where such paths are not
                 * allowed. This should not happen now that we're using the
                 * Path::fixSeparators method above, but just in case...
                 */
                $mkdir_exception = $ex->getMessage();
                $failed = true;
            }
            
            // This handles when mkdir() fails for whatever reason.
            if ($failed) {
                // Failed to create the directory.
                $error = __METHOD__ . "Could not create main folder in cache! $mkdir_exception";
                error_log($error);
                throw new \RuntimeException($error);
            }
            
            if ($config->debug_mode) {
                error_log(__METHOD__ . ": Initialized main folder for image cache at $this->cache_path succecssfully");
            }
        }
        
        /* Since we've gotten to here, then we now have the main folder. Now
         * check if the default and error images are present, and if not, copy
         * them in...
         */
        $required_images = array_values($config->imagecache['images']);
        
        foreach ($required_images as $required_image) {
            $required_name = basename($required_image);
            if (!$this->contains($required_name)) {
                $copy_success = copy(
                    $required_image,
                    $main_dir_path . DIRECTORY_SEPARATOR . $required_name
                );
                
                if (!$copy_success) {
                    $error = __METHOD__ . ": Failed to copy imaage $required_name to main folder";
                    error_log($error);
                    throw new \RuntimeException($error);
                }
            }
        }
    }
    
    public function __destruct()
    {
        // Remove any empty directories.
        $this->cleanup();
    }
    
    /**
     * Finds all images matching $target (i.e. at all resolutions) as stored in 
     * this image cache.
     * 
     * @param string $target Name of file to search for.
     * @param type $flags \FilesystemIterator flags. Defaults to SKIP_DOTS.
     * @return string[] Array of matching file paths.
     */
    private function findFiles($target, $flags = \FilesystemIterator::SKIP_DOTS)
    {
        // Instantiate an iterator to traverse the cache level by level.
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($this->cache_path, $flags),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        
        var_dump($iterator);
        
        $matches = array();
        foreach ($iterator as $file) {
            // If the file doesn't match, skip...
            if (basename($file) == $target) {
                $matches[] = $file;
            }
        }
        
        return $matches;
    }
    
    /**
     * Remove any empty directories in the cache.
     * 
     * @return int|boolean FALSE if the cleanup wasn't completely successful, 
     *                     or the number of deleted folders if it was.
     */
    public function cleanup()
    {
        /* Instantiate an iterator that goes through all of the files in the
         * cache depth first, and removes empty directories. Going depth first,
         * ensures that the deletes will be thorough.
         */
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $this->cache_path,
                \FilesystemIterator::SKIP_DOTS
            ),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        $delete_count = 0;
        $found_count = 0;
        foreach ($iterator as $file) {
            /* If we're not dealing with a directory, or we're dealing with an
             * empty directory (an empty dir contains two files, "." and ".."), 
             * a non-empty one will contain more) then skip it.
             */
            if (!$file->isDir() || (count(scandir($file)) > 2)) {
                continue;
            }
            
            $found_count++;
            if (rmdir((string) $file)) {
                $delete_count++;
            }
        }
        
        /* If we delete the same number of matching files as we find, then we
         * consider that successful.
         */
        return ($delete_count == $found_count) ? $delete_count : false;
    }

    /**
     * Add a file to the image cache. Overwrites any existing file with the same
     * name.
     * 
     * @param string $source Path to the original file
     * @param string $file   (Optional) name of file to create in the image cache.
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function add($source, $file = null)
    {
        $error = "";
        if (!file_exists($source)) {
            $error = __METHOD__ . ": File '$source' does not exist!";
        } else {
            // Try to load the image to see if it's actually an image...
            try {
                $image = new Image($source);
            } catch (\Exception $e) {
                $error = __METHOD__ . ": Failed to load '$source' - " . $e->getMessage();
            }
            /* We don't need the image object now that the file's been verified
             * as being a valid image.
             */
            unset($image);
        }
        
        if ($error != "") {
            echo $error;
            error_log($error);
            return false;
        }
        
        /* If we get to here, then we have  a valid image of a supported type.
         * If no alternative name has been specified for the file, use the one
         * it's got.
         */
        if (is_null($file)) {
            $file = basename($source);
        }
        
        // Finally, perform the file copy.
        $destination = Path::join($this->cache_path, $this->main_folder, $file);
        return copy($source, $destination);
    }
    
    /**
     * Removes an image from the image cache. Removes every version of the file
     * at every resolution.
     * 
     * @param string $target Name of file to delete
     * @return int|boolean FALSE on failure, number of deleted files on success
     */
    public function delete($target)
    {
        echo __METHOD__ . ": Want to delete $target\n";
        
        // Find any files that look like the target.
        $matches = $this->findFiles($target);
        
        var_dump($matches);
        
        $delete_count = 0;
        foreach ($matches as $match) {
            if (unlink($match)) {
                // File was deleted, increment the counter.
                $delete_count++;
            } else {
                // File not deleted...
                $error = __METHOD__ . ": Failed to delete $match";
                error_log($error);
            }
        }
        
        /* Remove any empty directories. No need to check the return value as
         * it's not wildly important.
         */
        $this->cleanup();
        
        /* Return false if we didn't delete all of the files we found, otherwise
         * return the number of files deleted.
         */
        return ((count($matches) == $delete_count) ? $delete_count : false);
    }
    
    /**
     * Check if a file exists in the cache. If $width and $height are integer,
     * it checks to see if that specific size exists; if not, it assumes you're
     * looking for the original.
     * 
     * @param string $file   Name of file.
     * @param int    $width  Width of desired image
     * @param int    $height Height of desired image
     * @return string|boolean FALSE if file doesn't exist, full path if it does.
     */
    public function contains($file, $width = null, $height = null)
    {
        if (is_null($width) && is_null($height)) {
            /* If the image width and height are both null, then assume we want
             * the original image.
             */
            $folder = $this->main_folder;
        } elseif (trim($file) == '') {
            /* If the passed-in file to check is empty, then we can assume that
             * the user doesn't want us to check for an empty file name.
             */
            $error = __METHOD__ . ": No file specified";
            error_log($error);
            return false;
        } elseif (!ImageManipulator::validateDimensions($width, $height)) {
            // In case of invalid/nonsensical image dimensions, eject.
            $error = __METHOD__ . ": Invalid width and/or height specified for $file!";
            error_log($error);
            return false;
        } else {
            $folder = sprintf("%dx%d/", $width, $height);
        }
        
        $path = Path::join($this->cache_path, $folder, $file);
        return (file_exists($path) ? $path : false);
    }
    
    /**
     * Get the path to a specific file, optionally at a specific resolution.
     * Unlike $this->contains(), this function will resize the original file (if
     * available) to create the desired file if it doesn't exist. If it does 
     * exist, it'll return it at the specified resolution.
     * 
     * @param string   $file   Name of the image file being requested.
     * @param int|null $width  Desired width for returned image. NULL = infer from other parameter
     * @param int|null $height Desired height for returned image. NULL = infer from other parameter
     * @return string|boolean Path to image on success, FALSE on failure.
     */
    public function get($file, $width = null, $height = null)
    {
        /* If the width and height are their default values (null), assume that
         * we want to get the original file. If they're some other form of 
         * invalid value, throw an Exception. Otherwise, try and get the file
         * at the desired resolution.
         */
        $error = "";
        if (is_null($width) && is_null($height)) {
            return $this->contains($file);
        } else {
            // Force all numerical values to integer to make sure they're valid.
            if (is_numeric($width) && is_numeric($height)) {
                $width = (int) $width;
                $height = (int) $height;
            }
            
            if (!ImageManipulator::validateDimensions($width, $height)) {
                $error = __METHOD__ . ": Width [$width] and/or height [$height] values are invalid";
            }
        }
        
        // If we've encountered an error, bail out now.
        if ($error != "") {
            error_log($error);
            return false;
        }
        
        /* Once we get to here, then we can assume that the user wants the file
         * at a specific resolution. If it's available, return it. If not, we'll
         * attempt to locate the original image in the cache and resize that to
         * suit.
         */
//        $path = $this->contains($file, $width, $height);
//        if ($path !== false) {
//            error_log("Found the file... $file, $width, $height");
//             // Found it!
//            return $path;
//        }
        
        /* We don't have the image (yet). Find the original and try to resize
         * it...
         */
        $path = $this->contains($file);
        if ($path === false) {
            /* We don't have an original to match this file. Nothing more we can
             * do except inform the user.
             */
            return false;
        }

        /* If we get to here, we have an original we can work with. Resize and
         * return that.
         */
        $image = new Image($path);
        $path = Path::join($this->cache_path, sprintf("%dx%d", $width, $height), $file);
        
        return ($image->copyResized($path, $width, $height)) ? $path : false;
    }
    
    /**
     * Get an image from the cache, but sized to be the size set for standard
     * thumbnails in the configuration file.
     * 
     * @param string $file
     * @return string|boolean Path to image on success, FALSE on failure.
     */
    public function getThumbnail($file) {
        /* Get the path to the thumbnail, generating it from the original if
         * necessary.
         */
        global $config;
        
        return $this->get(
            $file,
            $config->imagecache['thumbnail']['width'],
            $config->imagecache['thumbnail']['height']
        );
    }
    
    /**
     * Return the path to the cache's root folder.
     * 
     * @return string 
     */
    public function getPath()
    {
        return $this->cache_path;
    }
}
