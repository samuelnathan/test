<?php
namespace OMIS\Imaging;

use \OMIS\Utilities\Path as Path;

/**
 * Image manipulation class using the GD imaging library
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
class GD extends ImageManipulator
{
    public function __construct($path)
    {
        // Get the image's attributes, and check if that has succeeded.
        try {
            $attributes = self::getImageAttributesGD($path);
            if ($attributes === false) {
                throw new Exception("getimagesize() failed");
            }
        } catch (Exception $e) {
            $error = "Failed to get image attributes for $path - " . $e->getMessage();
            error_log(__METHOD__ . ": $error");
            throw new RuntimeException($error);
        }
        
        // Next, get the image's height, width, and type:
        list($this->width, $this->height, $imagetype, ) = $attributes;
        switch ($imagetype) {
            case IMAGETYPE_JPEG:
                $this->image = imagecreatefromjpeg($path);
                $this->format = self::IMAGE_FORMAT_JPEG;
                break;
            case IMAGETYPE_GIF:
                $this->image = imagecreatefromgif($path);
                $this->format = self::IMAGE_FORMAT_GIF;
                break;
            case IMAGETYPE_PNG:
                $this->image = imagecreatefrompng($path);
                $this->format = self::IMAGE_FORMAT_PNG;
                break;
            default:
                /* Get the file's extension, without the dot. If GD doesn't
                 * recognize the image format constant, this returns an empty
                 * string.
                 */
                $file_extension = image_type_to_extension($imagetype, false);
                if (trim($file_extension) == "") {
                    $error = "Failed to load unknown file type $path";
                } else {
                    $error = "File type $file_extension is unsupported, sorry.";
                }
                error_log(__METHOD__ . ": $error");
                throw new RuntimeException($error);
        }
        
        // Call the parent's constructor.
        parent::__construct($path);
    }
    
    public function __destruct()
    {
        /* If the object is definitely a GD image, then destroy it the "correct"
         * way; if not, assume it's already destroyed, so success!
         */
        if ((!is_object($this->image)) && (get_resource_type($this->image) == 'gd')) {
            $success = imagedestroy($this->image);
        } else {
            $success = true;
        }
        
        if (!$success) {
            $error = "Failed to destroy GD image object for some reason";
            error_log(__METHOD__ . ": $error");
        }
        
        // Call the parent's destructor.
        parent::__destruct();
    }
    
    /**
     * Custom exception handler to wrap around functions that generate E_NOTICE
     * (and possibly E_WARNING, haven't checked) messages and elevate them to
     * Exceptions so that they can be trapped in a try..catch statement.
     * 
     * @param int    $severity   Message code
     * @param string $message    Message Body
     * @param string $filename   File in which exception was thrown
     * @param int    $linenumber Line number in $filename where error was thrown
     * @throws \ErrorException
     * @usedby self::getImageAttributesGD()
     */
    private static function exceptionsErrorHandler($severity, $message, $filename, $linenumber)
    {
        if (error_reporting() == 0) {
            return;
        }

        if (error_reporting() & $severity) {
            throw new \ErrorException($message, 0, $severity, $filename, $linenumber);
        }
    }
    
    /**
     * Uses the GD library to get the attributes of a file (resolution, format,
     * etc.). It's basically a wrapper for getimagesize(), but because that can
     * generate notices and warnings when it fails, we need to wrap it in a
     * custom error handler which can elevate those to exceptions, which is what
     * this is.
     * 
     * @param string $path Path to the (presumably image) file.
     * @return mixed[] Output of GD's getimagesize() function.
     * @uses self::exceptionsErrorHandler()
     */
    private static function getImageAttributesGD($path)
    {
        /* Temporarily change the error handler so that E_NOTICE errors can be
         * trapped as exceptions. Then get the info we need, and restore the
         * previous error handling setup.
         */
        set_error_handler("self::exceptionsErrorHandler");
        $attributes = getimagesize($path);
        restore_error_handler();
        
        return $attributes;
    }
    
    /**
     * Converts an image from one format to another using the GD Library.
     * 
     * @param string $format      One of: "JPEG", "GIF", "PNG".
     * @param string $destination Path to the desired destination image.
     * @return boolean True on success, false on failure.
     */
    public function convert($format, $destination)
    {
        if ($format == $this->format) {
            return $this->copy($destination);
        } elseif (empty($format)) {
            $error = "No file format specified for convert!";
            error_log(__METHOD__ . ": $error");
            return false;
        } else {
            return $this->save($this->image, $destination, $format);
        }
    }
    
    /**
     * Crop the image so that what's left is the area between ($top, $left) and
     * ($right, $bottom).
     * 
     * Throws an exception if the crop process fails.
     * 
     * @param resource $src_image GD resource representing the source image.
     * @param int      $top       y-coordinate of top of crop box
     * @param int      $left      x-coordinate of top of crop box
     * @param int      $bottom    y-coordinate of bottom of crop box
     * @param int      $right     x-coordinate of bottom of crop box
     * @return resource GD Image resource representing the destination image.
     * @throws \Exception
     */
    protected function crop($src_image, $top, $left, $bottom, $right)
    {
        // Calculate the size of the cropped area
        $width = $right - $left;
        $height = $bottom - $top;
        
        // Create a temporary image
        $dst_image = imagecreatetruecolor($width, $height);
        
        // Copy the original image and crop while you're at it...
        $success = imagecopy($dst_image, $src_image, 0, 0, $top, $left, $width, $height);
        
        if (!$success) {
            $error = "Failed to crop $this->path to ($top, $left, $bottom, $right)";
            error_log(__METHOD__ . ": $error");
            throw new Exception($error);
        } else {
            return $dst_image;
        }
    }
    
    /**
     * Resize an image to $dst_w x $dst_h using GD.
     * 
     * @param resource $src_image GD Image Resource specifying image to resize
     * @param type     $dst_w     Desired width of destination image
     * @param type     $dst_h     Desired height of destination image
     * @return resource GD Image resource specifying destination image
     * @throws \Exception
     */
    protected function resize($src_image, $dst_w, $dst_h)
    {
        // Get the dimensions of the source image.
        $src_w = imagesx($src_image);
        $src_h = imagesy($src_image);
        
        $dst_image = imagecreatetruecolor($dst_w, $dst_h);
        $success = imagecopyresampled($dst_image, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);
        
        if (!$success) {
            $error = "Failed to resize $this->path to ($dst_w, $dst_h)";
            error_log(__METHOD__ . ": $error");
            throw new Exception($error);
        } else {
            return $dst_image;
        }
    }
    
    /**
     * Write a GD image resource to file.
     * 
     * @param resource $image       GD Image resource
     * @param string   $destination Path to desired output file.
     * @param string   $format      "PNG", "GIF" etc.
     * @return boolean TRUE on success, FALSE on failure.
     */
    protected function save($image, $destination, $format = null)
    {
        if ($format == null) {
            if (empty($this->format)) {
                $error = "No format specified for image to be written!";
                error_log(__METHOD__ . ": $error");
                return false;
            } else {
                $format = $this->format;
            }
        }
        
        // Ensure that the relevant folder is creatable and created before saving
        if (!Path::mkdir($destination)) {
            $error = "Cannot create directory for '$destination'";
            error_log(__METHOD__ . ": $error");
            return false;
        }
        
        switch ($format) {
            case self::IMAGE_FORMAT_GIF:
                return imagegif($image, $destination);
            case self::IMAGE_FORMAT_JPEG:
                return imagejpeg($image, $destination);
            case self::IMAGE_FORMAT_PNG:
                return imagepng($image, $destination);
            default:
                $error = "File type $format is unsupported, sorry";
                error_log(__METHOD__ . ": $error");
                return false;
        }
    }
}
