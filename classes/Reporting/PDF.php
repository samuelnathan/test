<?php
/* Original Author: Domhnall Walsh
 * For Qpercom Ltd
 * Date: 27/07/2015
 * © 2015 Qpercom Limited.  All rights reserved
 */

namespace OMIS\Reporting;

/**
 * Subclass of TCPDF designed to add some features...
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class PDF extends \TCPDF
{
    // Variable which
    private $writeContinueFooter;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        $this->writeContinueFooter = false;

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    public function Footer()
    {
        if ($this->writeContinueFooter) {
            $this->SetY(-15);
            $this->Cell(0, 5, "(section continued on next page)", "", "L", false);
            /* Make sure we don't accidentally print this message on the wrong
             * pages...
             */
            $this->writeContinueFooter = false;
        }
        parent::Footer();
    }
    
    /**
     *  Write continue footer now
     */
    public function writeContinueFooter() {
        $this->writeContinueFooter = true;
    }
    

    /**
     * Modify or create a border definition for TCPDF
     *
     * @param mixed   $border TCPDF border definition or empty string
     * @param boolean $top    TRUE = show top border, FALSE = don't.
     * @param boolean $bottom (see above)
     * @param boolean $left   (see above)
     * @param boolean $right  (see above)
     * @return mixed Associative array or string depending on type of $border.
     */
    public static function updateBorder($border, $top = null, $bottom = null, $left = null, $right = null)
    {
        /* The border is either a string or an array; if it's an array, then it's
         * an associative array with one key and one value - the key is the same
         * as what the border would be expressed as a simple string, so that's
         * what we update in the case of
         */

        if (is_array($border)) {
            $borders_set = str_split(array_keys(strtoupper($border))[0]);
        } elseif (empty($border)) {
            $borders_set = array();
        } else {
            $borders_set = str_split(strtoupper($border));
        }

        // Left, Top, Right, Bottom
        $border_sides = array('L' => $left, 'T' => $top, 'R' => $right, 'B' => $bottom);

        // Borders
        $output_borders = '';
        foreach ($border_sides as $side => $enabled) {
            /* If we want this side explicitly or it's already set and we don't
             * care (i.e. the relevant border param is null) then pass it through.
             */
            if ($enabled || (in_array($side, $borders_set) && is_null($enabled))) {
                $output_borders .= $side;
            }
        }

        /* Return the original additional parameters if they're set. Otherwise
         * just return the string.
         */
        if (is_array($border)) {
            $result = array($output_borders => array_values($borders_set)[0]);
        } else {
            $result = $output_borders;
        }

        return $result;
    }
    
    /**
     * Version of TCPDF's internal writeHTMLCell() method that has an internal
     * mechanism to force TCPDF to push cells that don't fit to the next page.
     * See TCPDF's writeHTMLCell() for what the various parameters do.
     */
    public function writeHTMLCellAutoWrap($w, $h, $x, $y, $html = '', $border = 0, $ln = 0, $fill = false, $reseth = true, $align = '', $autopadding = true)
    {     
        
        // Get page Y (height) values
        list($available_height, $y_max, $y_position) = $this->getYValues();
        $this->startTransaction();
        $result = $this->writeHTMLCell($w, $h, $x, $y, $html, $border, $ln, $fill, $reseth, $align, $autopadding);
        $y_current = $this->getY();
        $block_height = $h;
        
        $graceHeight = 4;
        if (($y_current >= $y_max) || (($block_height + $graceHeight) > $available_height)) {
            
            /* This cell would force the page to roll over, so let's push it to
             * the next page rather than split half-way through.
             */
            $this->rollbackTransaction(true);
            $this->writeContinueFooter = true;
            $this->AddPage();
            
            // Modify the border to include a top row.
            $border = self::updateBorder($border, true);
            $result = $this->writeHTMLCell($w, $h, $x, $y, $html, $border, $ln, $fill, $reseth, $align, $autopadding);
        } else {
            // The cell is fine, no problem, render as normal.
            $this->commitTransaction();
        }

        return $result;
    }
    
    /**
     * Get all page y (height values)
     * 
     * @return array $y_max (height possible) 
     *               $y_remaining (remaining page height)
     *               $y_position (current position)
     */
    public function getYValues() {
        
        // Get Y (height) max possible height
        $y_max = $this->getPageHeight() - $this->getFooterMargin();
        
        // Current Y position
        $y_position = $this->GetY();
        
        // Get remaining Y (height)
        $y_remaining = $y_max - $y_position;
        
        return [$y_remaining, $y_max, $y_position];
    }
    
    
    /**
     * Calculate the printable width available on the current page.
     * It's possible this will need to be amended to include cell padding or
     * some such.
     *
     * @return int|float the available width in the configured units.
     */
    public function getAvailableWidth()
    {
        $margins = $this->getMargins();
        return $this->getPageWidth() - $margins['left'] - $margins['right'];
    }

    /**
     * Guesstimate (and set) how much time is made available to execute a PDF
     * generation script given that it has to generate $itemCount results and
     * that each result requires (roughly speaking) $kbPerItem memory.
     *
     * @param int       $itemCount      How many items need to be generated
     * @param int|float $secondsPerItem Experimentally derived scaling factor - how long per item.
     * @return int Number of seconds that PHP's execution limit has been set to for this script.
     */
    public static function guessTimeLimit($itemCount, $secondsPerItem) {
        $current_limit = (int) ini_get('max_execution_time');
        $computed_limit = round($itemCount * $secondsPerItem);
        //error_log(__METHOD__ . ": Execution Time Limits - Current: $current_limit, Calculated: $computed_limit");

        $chosen_limit = max($current_limit, $computed_limit);
        //error_log(__METHOD__ . ": Execution time limit estimated as $chosen_limit seconds");
        set_time_limit($chosen_limit);
        return $chosen_limit;
    }

    /**
     * Guesstimate (and set) how much memory is made available to execute a PDF
     * generation script given that it has to generate $itemCount results and
     * that each result requires (roughly speaking) $kbPerItem memory.
     *
     * @param int       $itemCount How many items need to be generated
     * @param int|float $kbPerItem Experimentally derived scaling factor - how much RAM per item.
     * @return string Memory limit expressed in PHP's own shorthand, e.g. "32M", "8G"
     */
    public static function guessMemoryRequirements($itemCount, $kbPerItem) {
        $current_limit = self::shorthandToBytes(ini_get('memory_limit'));
        $computed_limit = ($itemCount * $kbPerItem * 1024);
        //error_log(__METHOD__ . ": Memory Limits - Current: $current_limit, Calculated: $computed_limit");

        $chosen_limit = self::bytesToShorthand(max($current_limit, $computed_limit));
        //error_log(__METHOD__ . ": Memory limit estimated as $chosen_limit");
        ini_set('memory_limit', $chosen_limit);
        return $chosen_limit;
    }

    /**
     * Using a clone of the current PDF to get the correct margins and related
     * settings, try to render a chunk of HTML into a textbox of a specified
     * width to see how tall that piece of rendered HTML would be.
     *
     * @param TCPDF   $pdfToClone   PDF document instance to clone.
     * @param int     $boxWidth     Width of area into which to render the html
     * @param string  $html         HTML to render in order to calculate its height
     * @return float Height of rendered HTML text in document's selected units (usually mm)
     */
    public static function calculateHtmlHeight(&$pdfToClone, $boxWidth, $html) {
        $tempPdf = clone $pdfToClone;
        $tempPdf->addPage();
        $tempPdf->writeHTMLCell($boxWidth, '', '', '', $html, 'RLB', 0, false, true, 'L', true);
        $textHeight = $tempPdf->getLastH();
        unset($tempPdf);
        return $textHeight;
    }

    /**
     * Convert PHP size shorthand (e.g. "32M") to a byte value.
     *
     * @param string $shorthand Shorthand size descriptor, e.g. "32M" or "8G".
     * @return int Numbe of bytes in $shorthand.
     */
    private static function shorthandToBytes($shorthand) {
        $result = trim($shorthand);
        $modifier = strtolower($shorthand[strlen($shorthand) - 1]);
        switch ($modifier) {
            case 'g':
                $result *= 1024;
            case 'm':
                $result *= 1024;
            case 'k':
                $result *= 1024;
        }

        return $result;
    }

    /**
     * Converts a file/memory chunk size in bytes into PHP Shorthand, e.g. "32M",
     * "8G", etc.
     *
     * @param int|float $bytes Number of bytes to convert
     * @return string Resulting shorthand string
     * @throws \OutOfBoundsException If $bytes is negative or too large.
     */
    private static function bytesToShorthand($bytes, $roundUp = true) {
        $result = (float) $bytes;
        $modifiers = array('K', 'M', 'G');

        // Make sure someone hasn't handed the function a negative value
        if ($bytes < 0) {
            throw new \OutOfBoundsException("Invalid file/memory size $bytes");
        }

        // If it's smaller than any of the shorthand units, return it as is.
        if ($bytes < 1024) {
            if ($roundUp) {
                return round($bytes, 0, PHP_ROUND_HALF_UP);
            } else {
                return $bytes;
            }
        }

        foreach ($modifiers as $modifier) {
            $result /= 1024;
            // Go around again if the result is still on the large side...
            if ($result >= 1024) {
                continue;
            }

            // If not, check if we need to round the value.
            if ($roundUp) {
                $result = round($result, 0, PHP_ROUND_HALF_UP);
            }
            return $result . $modifier;
        }

        throw new \OutOfBoundsException("Invalid file/memory size $bytes");
    }
}
