<?php
namespace OMIS;

use OMIS\EarlyWarning\EarlyWarningFactory;
use OMIS\Log\RemoteIpHandler;
use Analog\Handler\File;
use OMIS\Log\UserLogHandler;
use OMIS\Monitor\DisabledMonitorUpdateNotifier;
use OMIS\Monitor\Http\Auth\TimestampAuthMiddleware;
use OMIS\Monitor\Http\HttpMonitorUpdateNotifier;
use OMIS\Monitor\MonitorUpdateNotifier;
use OMIS\Stats\LastActiveUsers;
use OMIS\Stats\LastActiveUsersLog;
use OMIS\Storage\DirectoryImportTemplatesSource;
use OMIS\Storage\DirectoryManualsSource;
use OMIS\Utilities\Path as Path;
use OMIS\Utilities\JSON as JSON;

/**
 * Configuration class for OMIS. Will import properties from files in the extra/ folder.
 *
 * @author Domhnall Walsh <domnhnall.walsh@qpercom.ie>
 *
 * @property string[] $db                       Database connection settings (mysqli)
 * @property mixed[]  $localization             Settings for this specific instance
 * @property mixed[]  $tcpdf_environment        Associative array containing TCPDF settings (language & page size)
 * @property mixed[]  $station_defaults         Default settings for OSCE Stations
 * @property mixed[]  $imagecache               Settings for the image cache.
 * @property mixed[]  $login                    Login attempt settings
 * @property mixed[]  $email                    Settings for e-mail sending
 * @property mixed[]  $sms                      Settings for e-mail sending
 * @property string   $base_path                Full path to the OMIS root.
 * @property string   $base_path_rel            Path to the OMIS root relative to the app's root.
 * @property string   $base_path_url            URL to base path of app.
 * @property string   $tmp_path                 Full path to the OMIS temporary folder.
 * @property string   $tmp_path_rel             Path to the OMIS temporary folder relative to the app's root.
 * @property string   $template_path            Path to the shared template folder
 * @property string   $analog_log_file          Path to the OMIS internal logging system's log file.
 * @property string   $twig_cache               Path to the folder where Twig keeps its compiled templates.
 * @property string   $installationid_path      Path to JSON file containing instance ID for this OMIS installation.
 */
class Config
{
    const FEEDBACK_NONE = 0;
    const FEEDBACK_ALL = 1;
    const FEEDBACK_GRS_FAIL = 2;
    const FEEDBACK_GRS_BORDERLINE = 3;
    const FEEDBACK_GRS_BORDERLINE_AND_FAIL = 4;
    const FEEDBACK_FLAG_OPTION = 5;

    // Settings to be pulled from /extra/configuration.php
    public $db;
    public $mailserver;
    public $localization;
    public $paths;
    public $page_titles;
    public $tcpdf_environment;
    public $station_defaults;
    public $imagecache;
    public $email;
    public $login;
    public $passwords;
    public $sms;
    public $feature_disabled_message;
    public $assessment;

    public $base_path;
    public $base_path_rel;
    public $base_path_url;

    public $tmp_path;
    public $tmp_path_rel;

    public $borderline_templates_path;

    public $template_path;

    public $log_folder;
    public $analog_log_file;
    public $twig_cache;

    public $exam_defaults;

    /* Installation ID - Unique 28 bit ID for Omis installation. Used to create
     * a separated instance ID for each session.
     */
    public $installation_id;
    public $installationid_path;

    public $compatibility;

    private $samlSettings;

    /**
     * Debug Mode
     *
     * Set to TRUE to enable additional logging where that option exists.
     */
    public $debug_mode = false;

    /**
     * Array with the specification of the properties that can be obtained lazily via the '__get' magic method.
     * In order to make an attribute lazy loaded, add it as a key to this array, being its value another array
     * with the key 'load'. The value of 'load' is the function that returns the value of the attribute.
     * @var array
     */
    private $lazyLoad;

    /**
     * Static function that randomly generates a 28-bit instance ID in the form
     * of seven hex-encoded characters. This allows for 268.4 million unique
     * instances of OMIS on one server, should be enough :)
     *
     * @return string Seven randomly generated hex numbers as a string.
     */
    public static function generateInstallationId()
    {
        return str_pad(dechex(mt_rand(0, 0xFFFFFFF)), 7, "0", STR_PAD_LEFT);
    }

    /**
     * Get the current instance ID for this installation of OMIS. If it's not
     * set, then generate one and set that. If that doesn't work, throw an
     * Exception as this should be a show-stopper.
     *
     * @return string 7-character hex string defining instance ID.
     * @throws \RuntimeException
     */
    public function getInstallationId()
    {
        try {
            $json = JSON::read($this->installationid_path, true);
        } catch (\BadFunctionCallException $ex) {
            error_log(__METHOD__ . ": Failed to load instance ID. Reason: " . $ex->getMessage());
            $instance_id = self::generateInstallationId();
            error_log(__METHOD__ . ": Generated new instance ID ($instance_id). Saving.");

            if ($this->setInstallationId($instance_id)) {
                return $instance_id;
            } else {
                error_log(__METHOD__ . ": Failed to write instance ID to file $this->installationid_path.");
                throw new \RuntimeException("Cannot set/get instance ID. Please contact your administrator");
            }
        }

        return $json['instance_id'];
    }

    /**
     * Write a new Instance ID to file.
     * The Instance ID is a 28-bit value, expressed as seven hex characters,
     * that differentiates instances of OMIS running on the same server.
     *
     * @param string $instance_id New Instance ID to write to file
     * @return boolean TRUE on success, FALSE on failure.
     */
    public function setInstallationId($instance_id = null)
    {
        if (is_null($instance_id)) {
            $instance_id = self::generateInstallationId();
        }

        $data = array("instance_id" => $instance_id);
        try {
            JSON::write($this->installationid_path, $data);
        } catch (Exception $ex) {
            error_log(__METHOD__ . ": Failed to write new instance ID to file. Reason: " . $ex->getMessage());
            return false;
        }

        // If we get to here, then we succeeded.
        return true;
    }


    /**
     * Is in offline mode (No internet connection, LAN web server setup)
     *
     * @return bool true|false
     */
    public function inOfflineMode() {

      $offlineSetting = isset($this->localization['modes']['offline']);
      return ($offlineSetting && $this->localization['modes']['offline']);

    }

    /**
     * Get CDN setting url, fallback location etc
     *
     * @param string $cdn_name       the name of cdn
     * @param string $setting_name   the setting name
     * @return string                the setting value
     */
    public function getCDNSetting($cdn_name, $setting_name) {

      // If the config file contains a setting then return it
      if (isset($this->cdn[$cdn_name][$setting_name])) {
        return $this->cdn[$cdn_name][$setting_name];

      // Setting is not found in the config fil, then pass the defaults
      // Cloud flare CDN
      } else if ($cdn_name == "cloudflare") {

         if ($setting_name == "url") {
            return "https://cdnjs.cloudflare.com";
         } elseif ($setting_name == "fallback_local") {
            return "cdn_fallback/cloudflare";
         }

      }

      error_log(__METHOD__ . ": Unable to retrieve CDN settings");
      return '';
    }

    /**
     * Get the name of the app as far as this instance is concerned
     *
     * @param bool $long if TRUE, returns the long name of the app, otherwise
     *                   returns the short form (usually just "OMIS").
     * @return string Name of the app (from config)
     */
    public function getName($long = false)
    {
        if ($long) {
            $value_key = 'app_name_full';
        } else {
            $value_key = 'app_name';
        }

        if (isset($this->localization['skin'][$value_key])) {
            return $this->localization['skin'][$value_key];
        } else {
            return "(not set)";
        }
    }

    /**
     * Get the name of the client (full name for now)
     *
     * @return string Name of client from config file
     */
    public function getClientName()
    {
        if (isset($this->localization['client']['full_name'])) {
            return $this->localization['client']['full_name'];
        } else {
            return "Client name not configured";
        }
    }

    /**
     * Prepend a specific path to the front of each entry in a list of paths
     * This is really used to make the settings file easier to handle.
     *
     * @param string   $prefix Prefix to add to each entry in $paths
     * @param string[] $paths  Associative array of paths (modified in-place)
     */
    private static function prefixPaths($prefix, &$paths)
    {
        for ($i = 0; $i < count($paths); $i++) {
            $key = array_keys($paths)[$i];
            $paths[$key] = Path::join($prefix, $paths[$key]);
        }
    }

    /**
     * Check whether the application is in Debug mode. Useful mainly to
     * determine if additional logging should be used or not.
     *
     * @return boolean TRUE if debug mode, FALSE if not or debug mode is not set.
     */
    public function debugMode()
    {
        if (isset($this->localization['modes'])) {
            if (isset($this->localization['modes']['debug'])) {
                return $this->localization['modes']['debug'];
            } else {
                return \FALSE;
            }
        }
    }

    /**
     * Get the settings for the SAML Service Provider
     */
    public function getSAMLSettings()
    {
        if ($this->samlSettings == null) {
            $path = Path::join($this->base_path, 'extra/saml_settings.php');
            if (!file_exists($path)) {
	        die("ERROR: SAML Configuration file does not exist or has not been set up.\n");
	    }
            require_once $path;
            $this->samlSettings = $settings;
        }

        return $this->samlSettings;
    }

    /**
     * Creates an instance of {@see MonitorUpdateNotifier} for a given session
     * @param $sessionId int ID of the session
     * @return MonitorUpdateNotifier Instance for that session according to the configuration
     */
    public function getMonitorUpdateNotifier($sessionId)
    {
        if (!isset($this->monitorNotification) || !$this->monitorNotification['enabled'])
            return new DisabledMonitorUpdateNotifier();

        $endpoint = $this->monitorNotification['httpEndpoint'];
        $httpTimeout = $this->monitorNotification['httpTimeout'];

        return new HttpMonitorUpdateNotifier($endpoint, $sessionId, $this->db['schema'], $httpTimeout, $this->getMonitorAuthMiddleware());
    }

    /**
     * Creates an instance of {@see HttpMonitorAuthMiddleware} to authenticate requests to the monitor service
     * @return TimestampAuthMiddleware
     */
    public function getMonitorAuthMiddleware() {
        return new TimestampAuthMiddleware(
            $this->monitorNotification['apiKey'],
            (int)$this->monitorNotification['auth']['threshold']);
    }

    /**
     * Get the implementation of the early warning system based on the current configuration
     * @return EarlyWarning\EarlyWarningCheck
     */
    public function getEarlyWarning() {
        return EarlyWarningFactory::createWarningSystem(isset($this->earlyWarning) ? $this->earlyWarning : []);
    }

    public function isSSOEnabled()
    {
        return $this->ssoEnabled;
    }

    /**
     * @return LastActiveUsers
     */
    public function lastActiveUsers() {
        return new LastActiveUsersLog($this->analog_log_file, $this->log_folder, $this->auditTrail['userAsField']);
    }

    /**
     * Constructor for the config class. This is for stuff that needs to be
     * configured at app launch or at least can't be hard-coded.
     */
    public function __construct($config_path = __DIR__)
    {
        // Figure out what the root OMIS folder path is.
        $this->base_path = Path::simplify(dirname($config_path) . DIRECTORY_SEPARATOR);

        /* Define where we expect to find the configuration file, and die
         * semi-gracefully if the config file doesn't exist.
         */
        $configuration_file = Path::join($this->base_path, 'extra/configuration.php');

        //$configuration_file = Path::simplify(__DIR__ . '/../extra/configuration.php');
        if (!file_exists($configuration_file)) {
            die("ERROR: Configuration file does not exist or has not been set up.\n");
        }

        /* @TODO (DW) Loading configuration data as a PHP file has definite speed
         * advantages, but there's no complete/uniform solution for handling
         * errors (e.g. syntax errors) in the configuration file so as to produce
         * a "pretty" error message rather than just imploding. We may need to
         * look at using a different mechanism (for example, JSON) in the future.
         */
        require_once $configuration_file;

         // Set the path to the customised borderline files folder
        $this->borderline_templates_path = $this->paths['borderline_templates'];

        // Prefix all of the source folder paths with the base path to OMIS.
        self::prefixPaths($this->base_path, $this->paths);
        self::prefixPaths(Path::join($this->base_path, 'storage/cache/student_images'), $this->imagecache['images']);
        self::prefixPaths($this->base_path, $this->imagecache['caches']);

        // Figure out what the root OMIS folder path is.
        $this->base_path_rel = Path::toURL($this->base_path);
        $this->base_path_url = Path::toURL($this->base_path, true);

        // Set the relative URL path and full file path to the tmp folder...
        $this->tmp_path_rel = 'storage/app/tmp';
        $this->tmp_path = Path::join($this->base_path, 'storage/app/tmp');

        // Set the path to the shared template folder
        $this->template_path = Path::join($this->base_path, "templates");

        // Set the path to the log files.
        $this->log_folder = Path::join($this->base_path, 'storage/logs');
        $this->analog_log_file = Path::join($this->log_folder, 'omis_analog.txt');

        // Cache for compiled TwiG templates.
        $this->twig_cache = Path::join($this->base_path, 'storage/cache/twig_compilation_cache');

        /* Set the path to the instance ID file (JSON format) and get the
         * current installation ID. If the session already knows, great; if not,
         * grab it from the file.
         */
        $this->installationid_path = Path::join($this->base_path, 'installation_id.json');
        $this->installation_id = $this->getInstallationId();


        // Set default values for the audit trail format configuration if it's not present
        if (!isset($this->auditTrail))
            $this->auditTrail = [];
        if (!isset($this->auditTrail['userAsField']))
            $this->auditTrail['userAsField'] = false;

        // Instantiate the logger and point it to the file we wish to log to.
        $fileHandler = File::init($this->analog_log_file);

        // Wrap the handler with the user log handler if the log format has the user as a separate field
        if ($this->auditTrail['userAsField'])
            $fileHandler = UserLogHandler::init($fileHandler);
        // Set the log handler
        \Analog::handler(RemoteIpHandler::init($fileHandler));

        // Set logging timezone if set in the config file
        if (in_array($this->localization['timezone'], \DateTimeZone::listIdentifiers())) {
            \Analog::$timezone = $this->localization['timezone'];
            \Analog::$date_format .= ' e';
        }

        /**
         * Set default for exam setting: exam_student_id_type
         * Which type of identifier to display during assessment:
         * NOTE: codes are case sensitive
         *  S => Student Identifier (default)
         *  E => Exam Number
         *  C => Candidate Number
         */
        if (!in_array($this->exam_defaults['exam_student_id_type'], ['S', 'E', 'C'])) {
            $this->exam_defaults['exam_student_id_type'] = 'S';
        }

        /**
         *  Set the locale as configured in localization
         *  @TODO (DC) add translations file check
         */
        $languageSet = !empty($this->localization['language']);
        $countrySet = !empty($this->localization['country']);
        $domainSet = !empty($this->localization['domain']);

        if ($languageSet && $countrySet && $domainSet) {
            $localeAll = $this->localization['language'] . "_"
                       . $this->localization['country'] . ".UTF-8";
            $domain = $this->localization['domain'];

        // Default locale [language:en, country:IE, domain:default]
        } else {
            $localeAll = "en_IE.UTF-8";
            $domain = "default";
        }

        putenv("LC_ALL=".$localeAll);
        setlocale(LC_ALL, $localeAll);

        bindtextdomain($domain, realpath(__DIR__ . '/..') . "/locale");
        textdomain($domain);

        $this->setupLazyLoad();
    }

    /**
     * Compute the relative path to a named path in the Config object.
     *
     * @param string $path_name Name of the configuration path to make relative
     * @return string Relative path to specified configuration path relative to the calling function.
     * @throws \InvalidArgumentException If the path name is not set or is not valid.
     */
    public function getRelativePath($path_name)
    {
        $valid_paths = [
            'base_path',
            'tmp_path',
            'template_path'
        ];

        // Sanity check the supplied path name.
        if (empty($path_name) || !in_array($path_name, $valid_paths) || !isset($this->$path_name)) {
            throw new \InvalidArgumentException("Config object does not have a '$path_name' path");
        }

        /* debug_backtrace() gets the chain of function/method calls that describe
         * how this particular class method gets called. The very last entry in
         * the chain is the originally calling function, so if we get the directory
         * that _that_ script is in we should have the correct value to compute a
         * relative path to our (fixed) temp folder path.
         */
        $backtrace = debug_backtrace();
        $calling_item = end($backtrace);

        $calling_path = dirname($calling_item['file']);

        return Path::relativeTo($this->$path_name, $calling_path);
    }

    private function setupLazyLoad() {
        $this->lazyLoad = [
            'manuals' => [
                'load' => [$this, 'getManuals']
            ],
            'import_files' => [
                'load' => [$this, 'getImportFiles']
            ]
        ];
    }

    /**
     * Obtains a configuration property. If the requested property is set up to be lazy loaded, it's obtained
     * from the lazy loading container. Otherwise is gotten as a normal attribute
     *
     * @param $name string
     * @return mixed
     */
    public function __get($name)
    {
        // If it's not lazy loaded, directly return the attribute value
        if (!array_key_exists($name, $this->lazyLoad))
            return $this->$name;

        // Get the lazy load configuration for the attribute
        $prop = $this->lazyLoad[$name];

        // If the attribute doesn't have a value, obtain it and save it for future calls
        if (!array_key_exists('value', $prop))
            $prop['value'] = $prop['load']();

        // Return the value
        return $prop['value'];
    }

    private function getManuals() {
        if (!isset($this->paths['manuals']) || empty($this->paths['manuals']))
            return [];

        return (new DirectoryManualsSource($this->paths['manuals']))
            ->getManualPaths();
    }

    private function getImportFiles() {
        if (!isset($this->paths['import_files']) || empty($this->paths['import_files']))
            return [];

        return (new DirectoryImportTemplatesSource($this->paths['import_files']))
            ->getImportTemplatesPath();
    }

}
