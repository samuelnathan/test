<?php

namespace OMIS\EarlyWarning;

/**
 * Represents a check to make against an exam to verify if a specific inconsistency
 * @package OMIS\EarlyWarning
 */
interface EarlyWarningCheck
{

    /**
     * Check if there's an inconsistency in an exam
     * @param int $examId ID of the exam in the database
     * @return array List with the found warnings. If the returned array is empty, no warnings were found
     */
    public function checkExam($examId);

}