<?php

namespace OMIS\EarlyWarning;


use OMIS\Database\CoreDB;
use OMIS\EarlyWarning\Impl\EarlyWarningComposite;
use OMIS\EarlyWarning\Impl\FilterWarningCheck;

class EarlyWarningFactory
{

    /**
     * Associates the name in the configuration with the class that implements the check
     * @var array
     */
    private static $classesByName = [
        'grs' => 'OMIS\EarlyWarning\Impl\GRSEarlyWarning',
        'emptyForms' => 'OMIS\EarlyWarning\Impl\EmptyFormEarlyWarning',
        'emptyGroups' => 'OMIS\EarlyWarning\Impl\EmptyGroupEarlyWarning',
        'noGroups' => 'OMIS\EarlyWarning\Impl\NoGroupsEarlyWarning',
        'noStations' => 'OMIS\EarlyWarning\Impl\NoStationsEarlyWarning'
    ];

    /**
     * Create an instance of the early warning system given a list of implementation names to use
     * @param $configuration
     * @return EarlyWarningCheck
     */
    public static function createWarningSystem($configuration) {
        $db = CoreDB::getInstance();

        // List of check names to perform. Defaults to empty list
        $checkList = array_key_exists('checks', $configuration) ?
            $configuration['checks'] : [];

        // Instantiates the checks from the list
        $checks = new EarlyWarningComposite(array_map(function ($item) use ($db) {
            return self::createByClassName($item, $db);
        }, $checkList));

        // If necessary, wraps the checks with the filter specified in the configuration
        return self::withoutIgnoredWarnings($db, array_key_exists('when', $configuration) ?
            new FilterWarningCheck($db, $checks, $configuration['when']) :
            $checks);
    }

    /**
     * Creates an instance of EarlyWarningCheck from it's configuration name
     *
     * @param $itemName
     * @param CoreDB $db
     * @return EarlyWarningCheck
     * @throws \ReflectionException
     */
    private static function createByClassName($itemName, CoreDB $db) {
        $className = self::$classesByName[$itemName];
        $class = new \ReflectionClass($className);

        return $class->newInstanceArgs([$db]);
    }

    /**
     * If the user has selected to not show again warnings for an exam, wraps the checks with a filter that
     * won't perform checks if the given exam is marked to ignore
     *
     * @param CoreDB $db
     * @param EarlyWarningCheck $inner
     * @return EarlyWarningCheck|FilterWarningCheck
     */
    private static function withoutIgnoredWarnings(CoreDB $db, EarlyWarningCheck $inner) {
        // If the list of exam warnings to ignore from the user in session is not empty, returns the inner
        // check wrapped with the filter to check that the exam is not in that list
        if (array_key_exists('warnings_ignored', $_SESSION)  && count($_SESSION['warnings_ignored']) !== 0)
            return new FilterWarningCheck($db, $inner, function(array $exam) {
                return !(in_array($exam['exam_id'], $_SESSION['warnings_ignored']));
            });
        // Otherwise directly returns the inner check
        else
            return $inner;
    }

}