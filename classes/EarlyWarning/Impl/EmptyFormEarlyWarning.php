<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

/**
 * Early Warning check that verifies if there are any empty forms being used by the stations in any of the exam sessions
 *
 * @package OMIS\EarlyWarning\Impl
 */
class EmptyFormEarlyWarning extends AbstractArrayWarningCheck
{

    /**
     * Get a name for the inconsistency found
     *
     * @param $form
     * @return mixed
     */
    protected function nameInconsistency($form)
    {
        return $form['form_name'];
    }

    /**
     * Get an array that represents the inconsistencies found
     *
     * @param $examId int
     * @param CoreDB $db
     * @return array
     */
    protected function getInconsistencies($examId, CoreDB $db)
    {
        return $db->exams->getEmptyFormsForExam($examId);
    }

    /**
     * Get the message to display with the inconsistencies found
     *
     * @param $inconsistencies string Inconsistencies found
     * @return string
     */
    protected function getWarningFor($inconsistencies)
    {
        return 'The following forms associated with the exam are empty: ' . $inconsistencies;
    }
}