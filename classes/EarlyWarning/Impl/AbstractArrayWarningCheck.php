<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

abstract class AbstractArrayWarningCheck extends AbstractEarlyWarningCheck
{

    protected function doCheckExam($examId, CoreDB $db) {
        // Get the inconsistencies in the exam
        $inconsistencies = $this->getInconsistencies($examId, $db);

        // If none are found, finish
        if (count($inconsistencies) === 0)
            return [];

        // Map the inconsistencies into an array with their names
        $inconsistencyNames = array_map(function ($inconsistency) {
            return '"' . $this->nameInconsistency($inconsistency)  . '"';
        }, $inconsistencies);

        /// Return the warning message that specifies the inconsistencies
        return [
            $this->getWarningFor(implode(', ', $inconsistencyNames))
        ];
    }

    /**
     * Get an array that represents the inconsistencies found
     *
     * @param $examId int
     * @param CoreDB $db
     * @return array
     */
    abstract protected function getInconsistencies($examId, CoreDB $db);

    /**
     * Get a name for the inconsistency found
     *
     * @param $inconsistency
     * @return string
     */
    abstract protected function nameInconsistency($inconsistency);

    /**
     * Get the message to display with the inconsistencies found
     *
     * @param $inconsistencies string Inconsistencies found
     * @return string
     */
    abstract protected function getWarningFor($inconsistencies);

}