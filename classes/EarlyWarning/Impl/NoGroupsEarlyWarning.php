<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

/**
 * Early Warning check that verifies if any of the session in the exam has no groups attached
 *
 * @package OMIS\EarlyWarning\Impl
 */
class NoGroupsEarlyWarning extends AbstractArrayWarningCheck
{

    /**
     * Get an array that represents the inconsistencies found
     *
     * @param $examId int
     * @param CoreDB $db
     * @return array
     */
    protected function getInconsistencies($examId, CoreDB $db)
    {
        return $db->exams->getSessionsWithoutGroups($examId);
    }

    /**
     * Get a name for the inconsistency found
     *
     * @param $session
     * @return mixed
     */
    protected function nameInconsistency($session)
    {
        return $session['session_description'];
    }

    /**
     * Get the message to display with the inconsistencies found
     *
     * @param $inconsistencies string Inconsistencies found
     * @return string
     */
    protected function getWarningFor($inconsistencies)
    {
        return  'The following sessions have no groups attached to them: ' . $inconsistencies;
    }
}