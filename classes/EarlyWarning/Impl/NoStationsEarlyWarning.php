<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

class NoStationsEarlyWarning extends AbstractArrayWarningCheck
{

    /**
     * Get an array that represents the inconsistencies found
     *
     * @param $examId int
     * @param CoreDB $db
     * @return array
     */
    protected function getInconsistencies($examId, CoreDB $db)
    {
        return $db->exams->getSessionsWithoutStations($examId);
    }

    /**
     * Get a name for the inconsistency found
     *
     * @param $session
     * @return mixed
     */
    protected function nameInconsistency($session)
    {
        return $session['session_description'];
    }

    /**
     * Get the message to display with the inconsistencies found
     *
     * @param $inconsistencies string Inconsistencies found
     * @return string
     */
    protected function getWarningFor($inconsistencies)
    {
        return  'The following sessions have no stations attached to them: ' . $inconsistencies;
    }
}