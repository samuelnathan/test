<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

/**
 * Early Warning check that verifies the consistency of GRS sections among the exam scoresheets
 * @package OMIS\EarlyWarning\Impl
 */
class GRSEarlyWarning extends AbstractEarlyWarningCheck
{

    /**
     * @param $examId
     * @param CoreDB $db
     * @return array
     * @throws \Exception
     */
    protected function doCheckExam($examId, CoreDB $db)
    {
        $formSections = $db->exams->getFormSectionsByExam($examId);

        // Get the number of GRS sections per form in the exam
        $formsGrs = array_reduce($formSections, function($forms, $section) {
            $formId = $section['form_id'];
            $isGrs = $this->isGRS($section);

            if (array_key_exists($formId, $forms))
                $forms[$formId] += $isGrs ? 1 : 0;
            else
                $forms[$formId] = $isGrs ? 1 : 0;

            return $forms;
        }, []);

        $noGrs = $this->haveNoGrs($formsGrs);
        $allGrs = $this->haveAllGrs($formsGrs);
        $equalGrs = $this->equalGrs($formsGrs);

        // If there are no GRS at all or all of the forms have GRS with the same amount, it's consistent. Return an
        // empty array
        if ($noGrs || ($allGrs && $equalGrs))
            return [];

        if (!$allGrs)
            return ['Some scoresheets have a Global Rating Scale, but not all of them'];

        if (!$equalGrs)
            return ['All scoresheets have Global Rating Scale, but the number of GRS sections is not equally distributed among scoresheets'];

        throw new \Exception('This point won\'t be reached');
    }

    private function isGRS($section) {
        return $section['competence_type'] == 'scale';
    }

    /**
     * Check that none of the forms have any GRS section
     * @param $forms
     * @return boolean
     */
    private function haveNoGrs($forms) {
        return array_reduce($forms, function($no, $numOfGrs) {
            return $no && ($numOfGrs === 0);
        }, true);
    }

    /**
     * Check that all the forms have at least one GRS section
     * @param $forms
     * @return boolean
     */
    private function haveAllGrs($forms) {
        return array_reduce($forms, function($yes, $numOfGrs) {
           return $yes && ($numOfGrs >= 1);
        }, true);
    }

    /**
     * Check that all the forms have the same amount of GRS sections
     * @param $forms
     * @return bool
     */
    private function equalGrs($forms) {
        $distribution = array_reduce($forms, function ($acc, $numOfGrs) {
            $acc[$numOfGrs] = array_key_exists($numOfGrs, $acc) ? $acc[$numOfGrs] + 1 : 1;
            return $acc;
        }, []);

        return (count($distribution) === 1 && array_values($distribution)[0] === count($forms));
    }

}