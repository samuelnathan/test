<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;

class EmptyGroupEarlyWarning extends AbstractArrayWarningCheck
{

    /**
     * Get an array that represents the inconsistencies found
     *
     * @param $examId int
     * @param CoreDB $db
     * @return array
     */
    protected function getInconsistencies($examId, CoreDB $db)
    {
        return $db->exams->getEmptyGroupsForExam($examId);
    }

    /**
     * Get a name for the inconsistency found
     *
     * @param $group
     * @return mixed
     */
    protected function nameInconsistency($group)
    {
        return $group['group_name'];
    }

    /**
     * Get the message to display with the inconsistencies found
     *
     * @param $inconsistencies string Inconsistencies found
     * @return string
     */
    protected function getWarningFor($inconsistencies)
    {
        return 'The following groups associated with the exam have no students attached: ' . $inconsistencies;
    }
}