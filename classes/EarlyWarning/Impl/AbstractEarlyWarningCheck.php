<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 04/04/18
 * Time: 15:44
 */

namespace OMIS\EarlyWarning\Impl;


use OMIS\Database\CoreDB;
use OMIS\EarlyWarning\EarlyWarningCheck;

/**
 * Partial implementation of an early warning check that receives an instance of the database layer
 * @package OMIS\EarlyWarning\Impl
 */
abstract class AbstractEarlyWarningCheck implements EarlyWarningCheck
{

    /**
     * @var CoreDB
     */
    private $db;

    public function __construct(CoreDB $db)
    {
        $this->db = $db;
    }

    /**
     * Check if there's an inconsistency in an exam
     * @param int $examId ID of the exam in the database
     * @return array
     */
    public function checkExam($examId) {
        return $this->doCheckExam($examId, $this->db);
    }

    /**
     * @param $examId
     * @param CoreDB $db
     * @return array
     */
    abstract protected function doCheckExam($examId, CoreDB $db);

}