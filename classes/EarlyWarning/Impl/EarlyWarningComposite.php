<?php

namespace OMIS\EarlyWarning\Impl;


use OMIS\EarlyWarning\EarlyWarningCheck;

/**
 * Composes different early warning checks into one by merging the warnings into a single list
 * @package OMIS\EarlyWarning\Impl
 */
class EarlyWarningComposite implements EarlyWarningCheck
{

    /**
     * @var array Inner checks
     */
    private $predicates;

    /**
     * EarlyWarningComposite constructor.
     * @param array $predicates
     */
    public function __construct(array $predicates)
    {
        $this->predicates = $predicates;
    }

    /**
     * Check if there's an inconsistency in an exam
     * @param int $examId ID of the exam in the database
     * @return array
     */
    public function checkExam($examId)
    {
        return array_reduce($this->predicates, function ($warnings, $predicate) use ($examId) {
            return array_merge($warnings, $predicate->checkExam($examId));
        }, []);
    }
}