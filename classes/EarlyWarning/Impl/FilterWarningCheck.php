<?php

namespace OMIS\EarlyWarning\Impl;


use Carbon\Carbon;
use OMIS\Database\CoreDB;
use OMIS\EarlyWarning\EarlyWarningCheck;

/**
 * Wraps a check an only runs it when a predicate is true. Otherwise, returns no warnings
 * @package OMIS\EarlyWarning\Impl
 */
class FilterWarningCheck extends AbstractEarlyWarningCheck
{

    /**
     * Inner check that is performed if the predicate is satisfied
     * @var EarlyWarningCheck
     */
    private $check;

    /**
     * Predicate that decides if the inner check is performed or not
     * @var \Closure
     */
    private $predicate;

    public function __construct(CoreDB $db, EarlyWarningCheck $check, \Closure $predicate)
    {
        parent::__construct($db);
        $this->check = $check;
        $this->predicate = $predicate;
    }

    /**
     * @param $examId
     * @param CoreDB $db
     * @return array
     */
    protected function doCheckExam($examId, CoreDB $db)
    {
        $exam = $db->exams->getExam($examId);
        $timeRange = $db->exams->getExamTimeRange($examId);

        $startTime = Carbon::createFromFormat('Y-m-d H:i:s', $timeRange['time_start_exam']);
        $endTime = Carbon::createFromFormat('Y-m-d H:i:s', $timeRange['time_end_exam']);

        $predicate = $this->predicate;

        // If the predicate is not satisfied, do not perform the inner check and return no warnings, otherwise
        // return the result of running the check
        return $predicate($exam, $startTime, $endTime, Carbon::now()) ? $this->check->checkExam($examId) : [];
    }
}