<?php
/**
 * Class to open, read, write and unpack ZIP files
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @updated David Cunningham <david.cunningham@qpercom.ie>
 */
namespace OMIS\Utilities;

class ZipFile
{
    private $path;
    private $archive;
    private $filelist;
    
    /**
     * Constructor for the Zip file class
     * 
     * @param string $zipfile Path to ZIP file to open.
     * @param int $mode The mode to use to open the archive (default null). Flags as follows: 
     *  ZipArchive::OVERWRITE   Always start a new archive, this mode will overwrite the file if it already exists.
     *  ZipArchive::CREATE      Create the archive if it does not exist.
     *  ZipArchive::EXCL        Error if archive already exists.
     *  ZipArchive::CHECKCONS   Perform additional consistency checks on the archive, and error if they fail.
     * @throws \RuntimeException
     */
    public function __construct($zipfile, $mode = null)
    {
        $this->archive = new \ZipArchive();
        $pathToZip = Path::fixSeparators($zipfile);

        // Open archive, include or exclude flags
        if ($mode == null) {
          $success = $this->archive->open($pathToZip);  
        } else {
          $success = $this->archive->open($pathToZip, $mode);
        }
    
        // Try to open the ZIP file to see what we've got.
        if ($success !== true) {
            switch ($success) {
                case \ZipArchive::ER_INCONS:
                    $error = "Zip file $zipfile is inconsistent!";
                    break;
                case \ZipArchive::ER_INVAL:
                    $error = "Invalid Argument";
                    break;
                case \ZipArchive::ER_MEMORY:
                    $error = "Memory Allocation failure when opening $zipfile";
                    break;
                case \ZipArchive::ER_NOENT:
                    $error = "No such file $zipfile";
                    break;
                case \ZipArchive::ER_NOZIP:
                    $error = "$zipfile is not a ZIP Archive!";
                    break;
                case \ZipArchive::ER_OPEN:
                    $error = "Error when opening $zipfile";
                    break;
                case \ZipArchive::ER_READ:
                    $error = "Read error when opening $zipfile";
                    break;
                case \ZipArchive::ER_SEEK:
                    $error = "Seek error when opening $zipfile";
                    break;
                default:
                    $error = "Unknown error code $success returned when opening $zipfile";
            }
            error_log(__METHOD__ . ": " . $error);
            throw new \RuntimeException($error);
        }
        
        $this->path = $zipfile;
        
        // Get the list of files in the archive.
        $this->index();
    }
    
    public function __destruct()
    {
        // Be nice about it and destroy the ZipArchive object.
        $this->archive->close();
    }
    
    /**
     * Private function that gets a list of all of the files in the archive, 
     * and returns an array containing those files.
     */
    private function index()
    {
        // Iterate through the files listed in the Zip file and add to an array.
        $this->filelist = [];
        for ($i = 0; $i < $this->archive->numFiles; $i++) {
            $stat = $this->archive->statIndex($i);
            $this->filelist[] = $stat['name'];
        }
    }
    
    /**
     * Check if the Zip archive contains $file.
     * 
     * @param string $file Filename or (relative) path to search for.
     * @return mixed FALSE if the file couldn't be found, otherwise the relative path to the file.
     */
    public function contains($file)
    {
        if (in_array($file, $this->filelist)) {
            // If the exact (relative) path matches...
            return $file;
        } else {
            /* If $file is a bare file with no path, then check if it's in any
             * subfolders within the ZIP file. If not, assume we don't have it.
             */
            if ($file !== basename($file)) {
                /* If the passed-in parameter isn't a bare file reference, then
                 * there's no point in hunting for it in subdirectories, so bale
                 * out.
                 */
                return false;
            }
            
            /* If it is a bare file name, see if it's in a (non-unique) list of
             * the bare filenames in the Zip...
             */
            $index = array_search($file, array_map("basename", $this->filelist));
            return (($index === false) ? false : $this->filelist[$index]);
        }
    }
    
    /**
     * Extract one, more or all files from the zip to $destination.
     * 
     * @param string $destination
     * @param mixed $file String (single file) or array of strings (multiple files)
     * @return boolean true on success, false on failure
     */
    public function extract($destination, $file = null)
    {
        $destination = Path::fixSeparators($destination);
        if ($file == null) {
            // Assume that the user wants all files.
            return $this->archive->extractTo($destination);
        } else {
            // Assume that the user wants specific file(s).
            return $this->archive->extractTo($destination, $file);
        }
    }
    
    /**
     * Return a list of files matching $pattern. If $pattern isn't specified,
     * then return all files.
     * 
     * @param string|string[]|null $patterns Pattern(s) to match (leave empty to return all files)
     * @param int   $pattern_flags See flags for fnmatch() in PHP docs
     * @return string[] Array of files matching the pattern (if any) or all files.
     */
    public function files(&$patterns = null, $pattern_flags = 0)
    {
        // If there's no pattern specified, just return the entire file list.
        if (is_null($patterns)) {
            return $this->filelist;
        }
        
        // If the passed-in $pattern is not an array, then "promote" it to be one.
        $search_patterns = (is_array($patterns) ? $patterns : array($patterns));
        
        /* Loop through the files, and then in each case, the pattern(s), to see
         * if each file matches any of the patterns. This arrangement should
         * preserve the existing file order.
         */
        
        $matches = [];
        foreach ($this->filelist as $file) {
            foreach ($search_patterns as $pattern) {
                if (fnmatch($pattern, $file, $pattern_flags)) {
                    $matches[] = $file;
                }
            }
        }
        
        // Return the results, if there are any.
        return $matches;
    }
    
    /**
     * Add file to archive
     * @param string $filePath  path to file to add
     * @param string $fileName  name of file, can include relative path 
     *                          which defines the file structure within archive
     * @return bool             status
     */
    public function addFile($filePath, $fileName)
    {
        // Add file to archive
        return $this->archive->addFile($filePath, $fileName);
    }


    /**
     * Add a file to a ZIP archive using its contents
     * @param string $fileName     name of file, can include relative path 
     *                             which defines the file structure within archive
     * @param string $contents     contents to add to file
     * @return bool                status
     */
    public function createFile($fileName, $contents = "")
    {
        // Create new file in archive
        return $this->archive->addFromString($fileName, $contents);
    }

    /**
     * Get the number of files in the zip
     * 
     * @return int Number of files in the ZIP file.
     */
    public function getFileCount()
    {
        return count($this->filelist);
    }
    
    /**
     * Return the path to the opened zip.
     * 
     * @return string Path to opened ZIP file.
     */
    public function getPath()
    {
        return $this->path;
    }

}
