<?php
namespace OMIS\Utilities;

/**
 * Helper class for HTTP-related stuff...
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class Request
{
    /**
     * Get a list of MIME types accepted by the browser issuing the current request.
     * 
     * @return string|mixed[]
     */
    public static function getAcceptTypes()
    {
        $results = array();
        $accept_types = explode(",", $_SERVER['HTTP_ACCEPT']);
        foreach ($accept_types as $accept_type) {
            $parts = explode(";", $accept_type);
            
            switch(count($parts)) {
                case 2:
                    list($mime, $q) = $parts;
                    break;
                case 1:
                    $mime = $parts[0];
                    $q = 1.0;
                    break;
                default:
                    continue;
            }
            
            $results[] = array("mime" => $mime, "q" => $q);
        }
        
        return $results;
    }
    
    /**
     * Determines if the browser making the current request will accept a given
     * MIME type in response.
     * 
     * @param string $mime MIME type we're looking for
     * @return boolean TRUE if MIME type is accepted, FALSE if not.
     */
    public static function acceptsType($mime)
    {
        $mime_types = self::getAcceptTypes();
        foreach ($mime_types as $mime_type) {
            if ($mime_type['mime'] == $mime) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Return the MIME type that the browser's expecting.
     * 
     * @return string MIME type that the browser's accepting.
     */
    public static function preferredType()
    {
        $accept_types = self::getAcceptTypes();
        $result = array_shift($accept_types);
        return $result['mime'];
    }
    
    /**
     * Try to establish the name of the page from the current URL. The returned
     * value is what we pass to the access control system to see if the current
     * user's role is allowed to access the page (or not).
     * 
     * @return string The guessed page ID.
     */
    public static function getPageID()
    {
        $page = explode(".", basename($_SERVER['REQUEST_URI']))[0];
        /* If it's not either manage or assess, then the script name is the
         * page ID.
         */
        if (!in_array($page, array('manage', 'assess'))) {
            // Don't know what the passed-in page is, assume the answer's no.
            return trim($page);
        }

        /* Otherwise, we need to pin down the page by a parameter - either
         * 'page' or 'p'
         */
        foreach (array('page', 'p') as $pagekey) {
            if (in_array($pagekey, array_keys($_REQUEST))) {
                return $_REQUEST[$pagekey];
            }
        }
        
        // If we get to here, we're in trouble - we can't find a page ID.
        return '';
    }
    
    /**
     * Returns the largest file upload currently possible, based on PHP's
     * settings. The returned value will be the smallest of these values, in
     * bytes:
     * 1. Setting for Maximum Upload File Size
     * 2. Setting for Maximum POST Request Size
     * 3. Memory Limit of PHP process accepting the file (assuming this method
     *    is called by that process)
     * 
     * @return int Maximum upload in bytes.
     */
    public static function getUploadMaxSize() {
        function to_bytes($val) {
            $val = trim($val);
            $last = strtolower($val[strlen($val)-1]);
            switch($last) {
                // The 'G' modifier is available since PHP 5.1.0
                case 'g':
                    $val *= 1024;
                case 'm':
                    $val *= 1024;
                case 'k':
                    $val *= 1024;
            }
            return $val;
        }

        $relevant_settings = array(
            'upload_max_size',
            'post_max_size',
            'memory_limit'
        );
        
        $limits = [];
        foreach ($relevant_settings as $setting) {
            $limits[] = to_bytes(ini_get($setting));
        }
        
        return min($limits);
    }
    
    public static function hasKeys($test_keys, $request_type = INPUT_REQUEST) {
        // Make sure that the request type is valid.
        if (!in_array($request_type, [INPUT_GET, INPUT_POST, INPUT_REQUEST])) {
            throw new \InvalidArgumentException("Specified request type is not valid");
        }
        
        // Promote $test_keys to an array if it isn't one already.
        if (!is_array($test_keys)) {
            $test_keys = [$test_keys];
        }
        
        switch ($request_type) {
            case INPUT_GET:
                $request_keys = array_keys($_GET);
                break;
            case INPUT_POST:
                $request_keys = array_keys($_POST);
                break;
            default: // INPUT_REQUEST...
                $request_keys = array_keys($_REQUEST);
        }
        
        /* Find which keys of the keys to test for are in the request object's
         * list. Should return a list of the same length as $test_keys.
         */
        $common_keys = array_intersect($request_keys, $test_keys);
        
        return (array_diff($common_keys, $test_keys) === []);
    }
}
