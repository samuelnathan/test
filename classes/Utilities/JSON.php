<?php
namespace OMIS\Utilities;

/**
 * JSON-related helper functions to encode & decode JSON as well as read/write
 * JSON data to/from files. These methods check for errors in the encoding
 * and decoding processes, and escalate any issues to exceptions so that they
 * can be caught by the calling code.
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
class JSON
{
    /**
     * Converts an error code from json_encode() or json_decode() into a text 
     * string that can be incorporated into logs, exceptions, etc.
     * 
     * @param int $error_code Error code from PHP's json functions
     * @return string Equivalent error message in text form.
     */
    private static function getErrorMessage($error_code)
    {
        switch ($error_code) {
            case JSON_ERROR_DEPTH:
                $error_message = "Maximum Stack Depth Exceeded";
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error_message = "Unexpected Control Character Found";
                break;
            case JSON_ERROR_SYNTAX:
            case JSON_ERROR_STATE_MISMATCH:
                $error_message = "Syntax Error (Malformed JSON)";
                break;
            case JSON_ERROR_UTF8:
                $error_message = "Malformed UTF-8";
                break;
            default:
                $error_message = "";
        }
        
        return $error_message;
    }
    
    /**
     * Check if the passed-in data is JSON or not by attempting to decode it.
     * 
     * @param mixed   $json           Data to be checked to see if it's JSON
     * @param boolean $return_boolean If TRUE, return TRUE if data is JSON; 
     *                                otherwise return the data.
     * @return boolean|mixed Either the decoded JSON, TRUE or FALSE.
     */
    public static function isJson($json, $return_boolean = true)
    {
        // If the data isn't a string, then it's not JSON.
        if (!is_string($json)) {
            return false;
        }
        
        try {
            $data = self::decode($json);
        } catch (\InvalidArgumentException $ex) {
            global $config;
            if (isset($config) && $config->debug_mode) {
                error_log(__METHOD__ . ": Got error '" . $ex->getMessage());
            }
            return false;
        }
        
        /* If we get to here, then the supplied variable contains valid JSON.
         * If the user wants a boolean value for logical test purposes, return
         * true, otherwise, return the parsed JSON as whatever it decodes as.
         */
        return ($return_boolean ? true : $data);
    }
    
    /**
     * Read JSON directly from a file into the equivalent PHP data structure.
     * 
     * @param string  $filepath       Path to JSON file.
     * @param boolean $as_associative If TRUE, return data as an associative array
     * @return mixed[] Array of data decoded from JSON file.
     * @throws \BadFunctionCallException If the file doesn't exist or can't be read.
     */
    public static function read($filepath, $as_associative = false)
    {
        // Make sure that the file exists...
        if (!file_exists($filepath)) {
            throw new \BadFunctionCallException(__METHOD__ . ": File $filepath not found!");
        }
        
        // Get the JSON to decode.
        $json = file_get_contents($filepath);
        if ($json === false) {
            // Something went wrong reading the file...
            throw new \BadFunctionCallException(__METHOD__ . ": Error reading file $filepath!");
        }
        return self::decode($json, $as_associative);
    }
    
    /**
     * Parse JSON data contained in a string. Escalates JSON decoding errors
     * into exceptions so that they can be handled by calling code more simply.
     * 
     * @param string  $json           Data (hopefully encoded in JSON format)
     * @param boolean $as_associative If TRUE, return data as an associative array
     * @return mixed[] The decoded result, all going well
     * @throws \InvalidArgumentException If the JSON is invalid.
     */
    public static function decode($json, $as_associative = false)
    {
        // Just in case the file is empty.
        if (trim($json) == "") {
            return "";
        }
        
        // Decode the JSON, or at least try to...
        $data = json_decode($json, $as_associative);
        
        $result_code = json_last_error();
        if ($result_code != JSON_ERROR_NONE) {
            $error_message = self::getErrorMessage($result_code);
            // If there was a problem parsing the JSON, then throw an exception.
            $exception_message = "Invalid JSON - Got error '$error_message'";
            throw new \InvalidArgumentException($exception_message);
        }
        
        // If we get to here, we had valid JSON. Woohoo. Return that.
        return $data;
    }
    
    /**
     * Takes mixed data and encodes it as JSON, or at least tries to.
     * 
     * @param mixed[] $data    Data to be encoded
     * @param int     $options Bitmask of options for json_encode().
     * @return string JSON encoded Data
     * @throws \InvalidArgumentException If the JSON encode process fails.
     */
    public static function encode($data, $options = 0)
    {
        // Handle when there's no actual data.
        if (is_null($data) || (is_string($data) && trim($data) == "")) {
            return "";
        }
        
        // Otherwise, let's have a go at encoding the JSON.
        if (!self::isJson($data, true)) {
            $json = json_encode($data, $options);
        }
        
        $result_code = json_last_error();
        if ($result_code != JSON_ERROR_NONE) {
            $error_message = self::getErrorMessage($result_code);
            // If there was a problem parsing the JSON, then throw an exception.
            $exception_message = "Invalid JSON - Got error '$error_message'";
            throw new \InvalidArgumentException($exception_message);
        }
        
        // If we get to here, then the JSON encode worked. Return that.
        return $json;
    }
    
    /**
     * Write a chunk of data to a file as JSON. If the data isn't already in
     * JSON format, try to encode it as such.
     * @param string  $filepath Path to file to write to
     * @param mixed   $data     Data to write to file as JSON
     * @param int     $options  Bitmask of options for json_encode().
     * @throws \BadFunctionCallException
     */
    public static function write($filepath, $data, $options = 0)
    {
        // If the file exists, make sure it's writable...
        if (file_exists($filepath) && !is_writable($filepath)) {
            throw new \BadFunctionCallException(__METHOD__ . ":File $filepath is not writeable!");
        }
        
        // Check if the passed-in data is JSON or not...
        if (!self::isJson($data, true)) {
            /* It's not JSON, attempt to encode it as JSON. This will throw an
             * exception if the data can't be encoded as JSON.
             */
            $json = self::encode($data, $options);
        } else {
            $json = $data;
        }
        
        // Write the JSON to the file.
        if (file_put_contents($filepath, $json) === false) {
            throw new \BadFunctionCallException(__METHOD__ . ": Error writing file $filepath!");
        }
    }
}
