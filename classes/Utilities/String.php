<?php
namespace OMIS\Utilities;

/**
 * Class to provide static helper function to do string-related things.
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class String
{
    /**
     * Check if the string ends with another string.
     * 
     * @param string $string         String to check for presence of $test
     * @param string $test           Check if current string ends with $test
     * @param bool   $case_sensitive Be case sensitive about checking (default: FALSE)
     * @return boolean TRUE if string ends with $test
     */
    public static function endsWith($string, $test, $case_sensitive = false)
    {
        $strlen = strlen($string);
        $testlen = strlen($test);
        if ($testlen > $strlen) {
            return false;
        }
        if (!$case_sensitive) {
            return substr_compare(
                strtolower($string),
                strtolower($test),
                -$testlen
            ) === 0;
        } else {
            return substr_compare($string, $test, -$testlen) === 0;
        }
    }
}
