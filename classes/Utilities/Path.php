<?php
namespace OMIS\Utilities;

/**
 * Utility class for functions related to sorting out path-related issues
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * @since 1.9.0
 */
class Path
{
    private $path;

    /**
     * Constructor
     *
     * @param string $path Absolute file path for file on the local server
     */
    public function __construct($path, $simplify = true)
    {
        // Only bother trying to create a path if it has a good chance of existing.
        if (is_string($path) && !empty($path) && is_dir($path)) {
            $path = self::setSeparator($path, DIRECTORY_SEPARATOR);
            $this->path = ($simplify ? self::simplify($path) : $path);
        } else {
            throw new \RuntimeException("Path $path is either not a directory or doesn't exist");
        }
    }

    /**
     * "Magic" method called when object is cast to a string.
     *
     * @return string The actual path referred to.
     */
    public function __toString()
    {
        return $this->path;
    }

    /**
     * Remove any incorrect path separators (e.g. forward slashes on Windows,
     * Backslashes on Unix/Linux) and replace them with the correct separator.
     *
     * @param  string $path Path to fix.
     * @return string "Fixed" path
     */
    public static function fixSeparators($path)
    {
        return self::simplify(self::setSeparator($path, DIRECTORY_SEPARATOR));
    }

    /**
     * Split a path into its constituent elements with a regex. Works for both
     * Windows and Linux (as well as badly formed paths with mixed separators).
     * Might come a cropper on Unix systems where filenames could theoretically
     * contain backslashes, but that should be rare enough to discount.
     *
     * @param  string   $path File/folder path
     * @return string[] Array of file path parts.
     */
    public static function split($path)
    {
        // PREG_SPLIT_NO_EMPTY conveniently handles trailing and double
        // directory separators.
        $parts = preg_split("/[\\\\\\/]/", $path, 0, PREG_SPLIT_NO_EMPTY);
        
        // Make sure that we'll have a leading slash later...
        $fromUnixRoot = (DIRECTORY_SEPARATOR == '/') ? (substr($path, 0, 1) == '/') : \FALSE;
        if ($fromUnixRoot) {
            array_unshift($parts, "");
        }

        return $parts;
    }

    /**
     * Concatenate an arbitrary number of path elements into
     *
     * @param  string $path_bit1,... Path elements to concatenate.
     * @return type
     */
    public static function join()
    {
        $elements = func_get_args();
        $path_parts = array();

        switch (count($elements)) {
            case 0:
                // Nothing to do! Could throw an exception, but this is better.
                return "";
            case 1:
                // Only one element, nothing to concatenate.
                return $elements[0];
            default:
                /* Do nothing here, and let flow continue out of the switch
                 * statement so that we're not excessively nesting levels.
                 */
        }

        /* Normally, we'll have two or more elements to concatenate. We do that
         * here.
         */
        for ($i = 0; $i < count($elements); $i++) {
            // Just in case someone tries something stupid...
            if (!is_string($elements[$i]) && !is_a($elements[$i], get_class())) {
                throw new \InvalidArgumentException("Path elements must be strings!");
            }

            if ($i == 0) {
                /* If we're using a UNIX system (where file paths can legally
                 * start with a forward slash) then check for this on the first
                 * element we parse and remember it for later.
                 */
                $element = rtrim($elements[$i], "\\/");
            } else {
                $element = trim($elements[$i], "\\/");
            }

            // By exploding each piece, we get it to fix the separators for free.
            $element_parts = self::split($element);
            foreach ($element_parts as $part) {
                $path_parts[] = $part;
            }
        }

        $result = self::simplify(implode(DIRECTORY_SEPARATOR, $path_parts));

        return $result;
    }

    /**
     * Takes a path containing empty slashes, '.' or '..' elements and reduces
     * it to it's minimal interpretation, as much as possible. The optional
     * $relativeTo path is the path it treats as a starting off point, so '..'
     * relative to '/a/b/c/' will be '/a/b', for example.
     *
     * @param  string      $path       File path to simplify.
     * @return string      Simplest interpretation of $path
     */
    public static function simplify($path)
    {
        // Bail out if the path to resolve is empty...
        if (empty($path)) {
            return "";
        }
        // Slice up the path into its constituent folders, and reverse them.
        $path_parts = self::split($path);

        // Loop until all components of $path are accounted for.
        $result_path_parts = [];
        while (count($path_parts) > 0) {
            $part = array_shift($path_parts);
            // Decide what to do with each part based on what it is.
            switch ($part) {
                case '.':
                    // '.' = 'this folder', so it's a non-path. Do nothing...
                    break;
                case '..':
                    /* '..' = 'up one directory'. If there's no path to identify
                     * what the ".." refers to, then pass it through untouched.
                     * Otherwise, remove the last element of the "resolved" path
                     * and don't pass along the "..".
                     */
                    if (!in_array(end($result_path_parts), [false, '..'])) {
                        array_pop($result_path_parts);
                        break;
                    }
                    // Fall through to add the ".." to the final path...
                default:
                    // Otherwise, it's a directory or file name. Add it to the list.
                    $result_path_parts[] = $part;
            }
        }

        $result = implode(DIRECTORY_SEPARATOR, $result_path_parts);
        
        return $result;
    }

    /**
     * Compute the relative path from $relativeTo to $path.
     *
     * @param  mixed  $path       Finishing point for relative path calculation
     * @param  string $relativeTo Starting point for relative path calculation. Usually the web root.
     * @param  string $separator  Folder separator, defaults to system default.
     * @return string Relative path to traverse from  $relativeTo to $path.
     */
    public static function relativeTo($path, $relativeTo = null, $separator = DIRECTORY_SEPARATOR)
    {
        // Some sanity checks.
        $path = self::simplify($path);

        /* If the folder to generate a path relative to is not specified, assume
         * it's the web root...
         */
        if (empty($relativeTo) || (is_string($relativeTo) && (trim($relativeTo) == ''))) {
            $relativeTo = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
        } else {
            $relativeTo = self::simplify($relativeTo);
        }

        // Figure out of any of the passed-in parameters is empty...
        $non_empty = array_filter(array($path, $relativeTo));

        switch (count($non_empty)) {
            case 0:
                // Both are empty. Nothing to do.
                return '';
            case 1:
                // One or other is empty. Return the other (it makes sense, honest!)
                return self::setSeparator($non_empty[0], $separator);
            default:
                // Do nothing.
        }

        list($path_parts, $relative_parts) = array_map("self::split", [$path, $relativeTo]);

        // Remove any common elements off the top of the two arrays.
        while ((count($relative_parts) > 0) && $path_parts[0] == $relative_parts[0]) {
            array_shift($path_parts);
            array_shift($relative_parts);
        }
        
        // $result_parts will ultimately store the parts of the computed relative
        // path. Where the two paths diverge, $break_paths will store the
        // divergent path elements so that they can be re-added later.
        $result_parts = array();
        $break_parts = array();
        while (count($path_parts) > 0) {
            // Get the first element remaining in the path parts.
            $part = array_shift($path_parts);

            if (count($relative_parts) == 0) {
                // We've run out of relative path to process, so everything else
                // goes straight to the result.
                $result_parts[] = $part;
                continue;
            }

            /* If the current part is a break from the path, or we've already
             * experienced a break, then continue the break.
             */
            if ($part != array_shift($relative_parts) || (count($break_parts) > 0)) {
                /* The current folder is a break from the relative path. To deal
                 * with this, we add a ".." to the current path, and add the
                 * current folder to a list we append to the end of the list of
                 * ".."'s once we find a matching folder
                 */
                $result_parts[] = "..";
                $break_parts[] = $part;
            }

            /* If we get to here, then the two paths are the same and the two
             * paths haven't diverged relative to each other, so simply removing
             * that element from the final path is sufficient for this to work,
             * so, we do nothing here.
             */
        }

        // Add the "extra" parts onto the end of the result thus far if needs be
        $result_parts = array_merge($result_parts, $break_parts);

        /* If there are spare $relative_parts, then the $relative_to path is
         * deeper than the path we want to check against, so we need to look
         * "up" the folder tree for the correct path.
         */
        while (count($relative_parts) > 0) {
            array_pop($relative_parts);
            // Prepend a '..' onto the result path.
            array_unshift($result_parts, "..");
        }

        // If the paths are identical, then $result_paths will be empty and
        // we'll need to inject a "." to indicate "the same folder"
        if (count($result_parts) == 0) {
            $result_parts[] = ".";
        }

        // Return the result.
        return implode($separator, $result_parts);
    }

    /**
     * Take a path and return it as a url relative to the web root, with forward
     * slashes as a delimiter
     *
     * @param  string $path     An (absolute) path to the file.
     * @param  bool   $absolute If true, then the URL should be a full one
     *                          starting at http...
     * @return string A relative (or absolute) URL depending on the value of $absolute.
     */
    public static function toURL($path, $absolute = false)
    {
        /* If there's no host name set, then an absolute URL is impossible, so
         * we need to fall back on a relative one.
         */
        if ($absolute && !empty($_SERVER['HTTP_HOST'])) {
            // Figure out if it's HTTPS or HTTP...
            $protocol = (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
            $url = $protocol . '://' . $_SERVER['HTTP_HOST'];
        } else {
            $url = '';
        }

        return $url . "/" . self::relativeTo($path, $_SERVER['DOCUMENT_ROOT'], "/");
    }

    /**
     * Force the separators on a path to be the separator desired.
     *
     * @param  string|\OMIS\Utilities\Path $path              The path to process
     * @param  string                      $desired_separator What separator to use in the output
     * @return string The reformatted url.
     */
    public static function setSeparator($path, $desired_separator = '/')
    {
        // If this is a Path object, then get the actual back out.
        if (is_a($path, get_class())) {
            $path = $path->path;
        }
        
        if (!is_string($path) || empty($path) || trim($path) == "") {
            return "";
        }

        return implode($desired_separator, self::split($path));
    }
    
    /**
     * If we're about to write a file in a folder that doesn't exist, this
     * should create it.
     *
     * @param  string  $path Path to file
     * @return boolean TRUE on success/nothing to do, FALSE on failure.
     */
    public static function mkdir($path)
    {
        if (!is_dir(dirname($path))) {
            return mkdir(dirname($path), 0777, true);
        } else {
            /* Folder already exists, so if we wanted the folder created, that's
             * "done", so the result is essentially true.
             */
            return true;
        }
    }
}
