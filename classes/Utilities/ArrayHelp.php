<?php
namespace OMIS\Utilities;

/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */


class ArrayHelp
{
    private $array = [];
    
    /**
     * Constructor
     *
     * @param array pass in array to work with
     */
    public function __construct($array)
    {   
         $this->array = $array;
    }
       
    /**
     * Get previous and next array value based on current value
     * 
     * @param string $currentValue     current value in array
     * @return previousNext[]|NULL     previous and next values in an array (NULL if none found)
     */
    public function previousAndNext($currentValue)
    {
        
       if (empty($this->array)) {
          return [NULL, NULL];
       }
       
       $currentIndex = array_search($currentValue, $this->array);
       if ($currentIndex === false) {
          return [NULL, NULL];
       }
           
       $previousIndex = $currentIndex - 1;
       if (isset($this->array[$previousIndex])) {
          $previousValue = $this->array[$previousIndex];
       } else {
          $previousValue = NULL;
       }
       
       $nextIndex = $currentIndex + 1;
       if (isset($this->array[$nextIndex])) {
          $nextValue = $this->array[$nextIndex];
       } else {
          $nextValue = NULL; 
       }
            
       return [$previousValue, $nextValue];
    }

}
