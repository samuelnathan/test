<?php
namespace OMIS\Services;

use OMIS\Database\Groups;
use OMIS\Database\Results;
use OMIS\Database\Stations;
use OMIS\Database\Students;

final class AssessmentFormService {

    private $studentDao;

    private $groupsDAO;

    private $resultDAO;

    private $stationDAO;

    public function __construct(
        Students $students,
        Groups $groups,
        Results $results,
        Stations $stations
    ){
        $this->studentDao = $students;
        $this->groupsDAO = $groups;
        $this->resultDAO = $results;
        $this->stationDAO = $stations;
    }

    /**
     * Verify whether students exist on the database
     * @param $studentsID
     * @return null or ids that do not exist
     */
    public function existStudents($studentsID)
    {

        // verify if students exist
        $notExistStudentsID = array_reduce($studentsID, function($carry, $item) {
            if (!$this->studentDao->doesStudentExist($item)) {
                $carry[] = $item;
                return $carry;
            }
        });

        if ($notExistStudentsID) {
            \Analog::info(
                __FILE__ . ": Student(s) ID(s) " . json_encode($notExistStudentsID)
                . " are/is not set or student doesn't exist. Aborting."
            );
        }

        return $notExistStudentsID;
    }

    /**
     * @param $studentsID
     * @param $groupID
     * @return array
     */
    public function getStudentsExamNumber($studentsID, $groupID)
    {
        $studentsName = [];

        foreach ($studentsID as $studentID) {
            $studentGroup = $this->groupsDAO->getStudentGroupRecord($studentID, $groupID);
            $student = $this->studentDao->getStudent($studentID);

            $student = [
                'id' => $student['id'],
                'studentId' => $studentID,
                'name' => $studentGroup["student_exam_number"],
                'student_image' => $student['student_image']
            ];
            $studentsName[] = $student;
        }

        return $studentsName;
    }

    public function getStudentsCandidateNumber($studentsID, $term)
    {
        $studentsName = [];

        foreach ($studentsID as $studentID) {
            $student = $this->studentDao->getStudent(
                $studentID,
                $term
            );

            $idSeparated = strlen($student['surname']) > 0 || strlen($student['forename']) > 0 ? " - " : "";
            $nameSeparated = strlen($student['surname']) > 0 && strlen($student['forename']) > 0 ? ", " : "";
            $fullName = $student['surname'] . $nameSeparated . $student['forename'];

            $nameAndID = $fullName . $idSeparated .
                (empty($student["candidate_number"]) ? "unknown" : $student["candidate_number"]);

            $student = [
                'id' => $student['id'],
                'studentId' => $studentID,
                'name' => $nameAndID,
                'student_image' => $student['student_image']
            ];
            $studentsName[] = $student;
        }

        return $studentsName;
    }

    public function getStudentsName($studentsID, $term)
    {
        $studentsName = [];
        foreach ($studentsID as $studentID) {
            $student = $this->studentDao->getStudent(
                $studentID,
                $term
            );
            $idSeparated = strlen($student['surname']) > 0 || strlen($student['forename']) > 0 ? " - " : "";
            $nameSeparated = strlen($student['surname']) > 0 && strlen($student['forename']) > 0 ? ", " : "";
            $fullName = $student['surname'] . $nameSeparated . $student['forename'];

            $nameAndID = $fullName . $idSeparated . $studentID;

            $student = [
                'id' => $student['id'],
                'studentId' => $studentID,
                'name' => $nameAndID,
                'student_image' => $student['student_image']
            ];
            $studentsName[] = $student;
        }

        return $studentsName;
    }

    /**
     * @param $studentIDs
     * @param $sessionID
     * @param $stationNumber
     * @param $stationID
     * @param $examinerID
     * @param $includeAbsentResult
     * @param $assessmentComplete
     * @return bool
     */
    public function doesStudentsHaveResult(
        $studentIDs,
        $sessionID,
        $stationNumber,
        $stationID,
        $examinerID,
        $includeAbsentResult,
        $assessmentComplete
    ){

        // If to filter by station_id
        $multiScenarioStation = $this->stationDAO->multiScenarioStation($stationID);
        $filterByStationID = ($multiScenarioStation) ? $stationID : NULL;

        $studentIDsWithResult = [];
        foreach ($studentIDs as $studentID) {
            $hasResult = $this->resultDAO->doesStudentHaveResult(
                $studentID,
                $sessionID,
                $stationNumber,
                $filterByStationID,
                $examinerID,
                $includeAbsentResult,
                $assessmentComplete
            );

            if ($hasResult) {
                $studentIDsWithResult = $studentID;
            }
        }

        if (!empty($studentIDsWithResult)) {
            \Analog::info(
                "Students " . json_encode($studentIDsWithResult) . " already examined. aborting..."
            );
        }

        return !empty($studentIDsWithResult);
    }



}