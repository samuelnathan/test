<?php
namespace OMIS\Services\test;

use OMIS\Database\Groups;
use OMIS\Database\Students;
use OMIS\Services\AssessmentFormService;
use \PHPUnit\Framework\TestCase;

final class AssessmentFormServiceTest extends TestCase {

    /**
     * @param $candidatesID
     * @dataProvider candidatesProvider
     */
    public function testExistStudents($candidatesID, $exist)
    {

        $studentDAO = $this->createMock(Students::class);
        $groupDAO = $this->createMock(Groups::class);

        $studentDAO->method("doesStudentExist")
            ->willReturn($exist);

        $assessmentFormService = new AssessmentFormService(
            $studentDAO,
            $groupDAO
        );

        if ($exist == true) {
            $this->assertEmpty($assessmentFormService->existStudents($candidatesID));
        } else {
            $this->assertEquals($candidatesID, $assessmentFormService->existStudents($candidatesID));
        }
    }


    public function candidatesProvider()
    {
        return [
            'non existem candidate' => [['12312', '1'], true],
            'existem candidate' => [['16040900', '1'], false],
        ];
    }

}