<?php

namespace OMIS\Monitor;

/**
 * Façade of the logic that pushes the notifications of updates during an exam
 * @package OMIS\Monitor
 */
interface MonitorUpdateNotifier
{

    /**
     * Notifies the update in the scoring progress of a given student in a given station
     * @param $studentId int ID of the student
     * @param $stationId int ID of the station
     * @param $newProgress array Data of the scoring process
     */
    function updateProgress($studentId, $stationId, $newProgress);

    /**
     * Notifies that a student in a station has completed his scoring
     * @param $studentId int ID of the student
     * @param $stationId int ID of the station
     * @param $examinerId string ID of the examiner that completed the scoring
     * @param $complete boolean True if completed, false otherwise
     * @return
     */
    function updateCompletion($studentId, $stationId, $examinerId, $complete);

    /**
     * Notifies a change in the 'absent' flag for a student result
     * @param $studentId int ID of the student
     * @param $stationId int ID of the station
     * @param $absent boolean True if new value is absent
     */
    function updateAbsence($studentId, $stationId, $absent);

    /**
     * Notifies that a student's result has been discarded
     * @param $studentId int ID of the student
     * @param $stationId int ID of the station
     */
    function updateDiscard($studentId, $stationId);

    /**
     * Notifies that a blank student has been discarded or not
     * @param $blankStudentId int ID of the blank student
     * @param $stationId int ID of the station
     * @param $discarded boolean True if it has been discarded
     */
    function updateDiscardBlank($blankStudentId, $stationId, $discarded);

    /**
     * Notifies that the user has logged out of the system
     * @param $userId string ID of the logged out user
     * @return void
     */
    function logout($userId);

}