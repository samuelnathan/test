<?php

namespace OMIS\Monitor\Http;


use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use OMIS\Monitor\Http\Auth\HttpMonitorAuthMiddleware;
use OMIS\Monitor\ID;
use OMIS\Monitor\Identifier;
use OMIS\Monitor\MonitorUpdateNotifier;
use GuzzleHttp\Client;

/**
 * Implementation of MonitorUpdateNotifier that sends the notifications to an http server
 * @package OMIS\Monitor\Http
 */
class HttpMonitorUpdateNotifier implements MonitorUpdateNotifier
{

    /**
     * @var string Base URL of the http server
     */
    private $endpoint;

    /**
     * @var int
     */
    private $sessionId;

    /**
     * @var string
     */
    private $namespaceId;

    /**
     * @var double
     */
    private $httpTimeout;

    private $authMiddleware;

    /**
     * @var Client;
     */
    private $httpClient;

    /**
     * HttpMonitorUpdateNotifier constructor.
     * @param $endpoint string Base URL of the http server
     * @param $sessionId int ID of the session
     * @param $namespaceId string ID of the client that performs the exam
     * @param  $httpTimeout double Timeout for the HTTP Client
     * @param $authMiddleware HttpMonitorAuthMiddleware Implementation of authentication middleware
     */
    public function __construct($endpoint, $sessionId, $namespaceId, $httpTimeout, HttpMonitorAuthMiddleware $authMiddleware)
    {
        $this->endpoint = $endpoint;
        $this->sessionId = $sessionId;
        $this->namespaceId = $namespaceId;
        $this->httpTimeout = $httpTimeout;
        $this->authMiddleware = $authMiddleware;

        $this->createHttpClient();
    }

    function updateProgress($studentId, $stationId, $newProgress)
    {
        $this->sendRequest('progress', [
            'student_id' => "$studentId",
            'station_id' => "$stationId",
            'progress' => $newProgress
        ]);
    }

    function updateCompletion($studentId, $stationId, $examinerId, $complete)
    {
        $this->sendRequest('completion', [
            'student_id' => "$studentId",
            'station_id' => "$stationId",
            'examiner_id' => "$examinerId",
            'newValue' => $complete
        ]);
    }

    function updateAbsence($studentId, $stationId, $absent)
    {
        $this->sendRequest('absence', [
            'student_id' => "$studentId",
            'station_id' => "$stationId",
            'newValue' => $absent
        ]);
    }

    function updateDiscard($studentId, $stationId)
    {
        $this->sendRequest('discard', [
           'student_id' => "$studentId",
           'station_id' => "$stationId"
        ]);
    }

    function updateDiscardBlank($blankStudentId, $stationId, $discarded)
    {
        $this->sendRequest('discardBlank', [
           'blank_student_id' => "$blankStudentId",
           'station_id' => "$stationId",
           'newValue' => $discarded
        ]);
    }

    /**
     * @param $action string
     * @param $data array
     */
    private function sendRequest($action, $data) {
        $this->httpClient->postAsync("/update/$this->namespaceId/$this->sessionId", [
            'json' => [
                'action' => $action,
                'data' => $data
            ]
        ]);
    }

    /**
     * Creates the http client to push the updates in the monitor service. Registers the authentication middleware
     * of this instance
     */
    private function createHttpClient() {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());
        $stack->push($this->authMiddleware->authRequest());

        $this->httpClient = new Client([
            'base_uri' => $this->endpoint,
            'handler' => $stack,
            'timeout' => $this->httpTimeout
        ]);
    }

    /**
     * Notifies that the user has logged out of the system
     * @param $userId string ID of the logged out user
     * @return void
     */
    function logout($userId)
    {
        $this->httpClient->deleteAsync("/client", [
            'json' => [
                'user' => $userId,
                'namespace' => $this->namespaceId
            ]
        ]);
    }

}