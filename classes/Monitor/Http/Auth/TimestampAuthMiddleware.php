<?php

namespace OMIS\Monitor\Http\Auth;


use Psr\Http\Message\RequestInterface;

/**
 * Implementation of the authentication middleware that adds an 'Authorization' header to the request
 * with a token made with the API key and the current timestamp
 * @package OMIS\Monitor\Http\Auth
 */
class TimestampAuthMiddleware implements HttpMonitorAuthMiddleware
{

    private $apiKey;

    /**
     * @var int Number of digits from the timestamp to take when hashing the API Key. The greater this number is, the smaller
     *      the time threshold is
     */
    private $threshold;


    /**
     * TimestampAuthMiddleware constructor.
     * @param $apiKey string
     * @param $threshold int
     * @throws \InvalidArgumentException
     */
    public function __construct($apiKey, $threshold)
    {
        if ($threshold <= 0)
            throw new \InvalidArgumentException('Invalid value for threshold');

        $this->apiKey = $apiKey;
        $this->threshold = $threshold;
    }

    /**
     * Returns the function that authorizes a request
     * @return callable
     */
    public function authRequest()
    {
        return function (callable $handler) {
          return function (RequestInterface $req, array $options) use ($handler) {
              $req = $req->withHeader('Authorization', $this->makeAuthHeader());
              return $handler($req, $options);
          };
        };
    }

    /**
     * Create the value of the Authorization header
     * @return string
     */
    private function makeAuthHeader() {
        $timestamp = date_timestamp_get(new \DateTime());
        $timestampStr = "$timestamp";
        $tsField = substr($timestampStr, 0, $this->threshold);

        return hash_hmac('sha512', $tsField, $this->apiKey);
    }

}