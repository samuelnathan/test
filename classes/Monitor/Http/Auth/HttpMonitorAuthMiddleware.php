<?php

namespace OMIS\Monitor\Http\Auth;

/**
 * Interface HttpMonitorAuthMiddleware.
 * Implementations of this interface modify a given http request to add the authorization in the monitor service
 * @package OMIS\Monitor\Http
 */
interface HttpMonitorAuthMiddleware
{

    /**
     * Returns the function that authorizes a request
     * @return callable
     */
    public function authRequest();

}