<?php

namespace OMIS\Monitor;

/**
 * Implementation of MonitorUpdateNotifier that does nothing. Instances of this class are used when the notifications
 * are disabled
 * @package OMIS\Monitor
 */
class DisabledMonitorUpdateNotifier implements MonitorUpdateNotifier
{

    function updateProgress($studentId, $stationId, $newProgress)
    {
    }

    function updateCompletion($studentId, $stationId, $examinerId, $completed)
    {
    }

    function updateAbsence($studentId, $stationId, $absent)
    {
    }

    function updateDiscard($studentId, $stationId)
    {
    }

    function updateDiscardBlank($blankStudentId, $stationId, $discarded)
    {
    }

    function logout($userId) {}

}