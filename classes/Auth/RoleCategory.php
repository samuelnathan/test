<?php
namespace OMIS\Auth;

/**
 * PHP version 5
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * DW: (10/9/2013) This class loads the details for a Role category from the
 * database and returns the list of roles if needed.
 */

class RoleCategory
{
    /* Defining the three user groups that OMIS knows how to handle (i.e. that there
     * are administrator
     */
    const ROLE_GROUP_ADMINISTRATORS = 1;
    const ROLE_GROUP_ASSISTANTS = 2;
    const ROLE_GROUP_EXAMINERS = 3;
    const ROLE_GROUP_STUDENTS = 4;
    const ROLE_GROUP_EXAM_ADMINS = 5;
    
    private $category_id = null;
    private $name;
    private $role_ids = [];
    private $can_login = false;
    private $database;
    
    /**
     * Constructor for the role. Default will be to return a group with all 
     * roles included.
     * 
     * @param mixed   $category_id        Either an unsigned int (group id) or 
     * <null> to get back a pseudo-group containing all roles.
     * @param boolean $include_superadmin Set to true if you want the super
     * admin user in the role list.
     * @param boolean $login_only         Filter so that only groups who can 
     */
    public function __construct($category_id = null, $include_superadmin = true, $login_only = false)
    {
        $this->category_id = $category_id;
        
        // See if there's a database instance available. If not, create one.
        global $db;
        if (!isset($db)) {
            // No global database object set. Create one.
            global $config;
            if (!isset($config)) {
                // No global configuration object. Get one.
                $config = new \OMIS\Config;
            }
            
            // Instantiate a local copy of the database object.
            $this->database = \OMIS\Database\CoreDB::getInstance(
                $config->db,
                $config->localization['modes']['production']
            );
        } else {
            $this->database = $db;
        }
        
        $where_queries = [];
        
        $query = "SELECT category_name, login_permitted FROM role_categories";
        if (is_null($category_id)) {
            /* There is no category. Get the full list of role IDs (applying the
             * necessary filters) and abort...
             */
            $this->category_id = null;
            $this->role_ids = $this->getRoleIDs($include_superadmin, $login_only);
            return true;
        } else {
            $this->category_id = $this->database->clean($category_id);
            $where_queries[] = "category_id = $this->category_id";
        }
        
        if ($login_only) {
            $where_queries[] = "login_permitted = 1";
        }
        
        if (!empty($where_queries)) {
            $query .= " WHERE (" . implode(" AND ", $where_queries) . ")";
        }
        
        // Wrap everything in an exception handler so that the program always
        // gets _something_ back.
        try {
            // Temporarily set the error handling to include exceptions...
            set_error_handler(array('\OMIS\Auth\RoleCategory', 'exceptionErrorHandler'));
            $result = $this->database->query($query)->fetch_row();
            $this->name = $result[0];
            $this->can_login = ($result[1] == 1);

            $this->role_ids = $this->getRoleIDs($include_superadmin, $login_only);
        } catch (Exception $e) {
            error_log(__FILE__ . ": " . __METHOD__ . ": " . $e->getCode() . " = " . $e->getMessage());
        }

        // Resume normal error-handling.
        restore_error_handler();
    }
    
    /**
     * Private exception handler for when 
     * 
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     * @throws ErrorException
     */
    public static function exceptionErrorHandler($errno, $errstr, $errfile, $errline)
    {
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
    
    /**
     * Property getter method
     * 
     * @param string $property The Name of the property.
     * 
     * @return string The property's value.
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    /**
     * Property setter method
     * 
     * @param string $property Name of the property
     * @param string $value    Property's new value
     */
    public function __set($property, $value)
    {
        // Need to prevent the user from setting the "dirty" property.
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
    
    /**
     * Get a list of the role IDs within this category.
     * 
     * @param bool $include_superadmin If TRUE, add the Super Admin role if it's
     *                                 not already there.
     * @param bool $login_only         If TRUE, only include roles that can log
     *                                 into the system (exclude assistants, for
     *                                 example)
     * @return int[] List of Role IDs.
     */
    private function getRoleIDs($include_superadmin = true, $login_only = false)
    {
        $where_clauses = [];
        $query = "SELECT DISTINCT role_id FROM roles INNER JOIN role_category_link "
            . "USING (role_id) INNER JOIN role_categories USING (category_id)";
        
        if (!is_null($this->category_id)) {
            $where_clauses[] = "category_id = " . (int) $this->category_id;
        }
        
        if ($login_only) {
            $where_clauses[] = "login_permitted = 1";
        }
        
        // Finish assembling the query...
        if (!empty($where_clauses)) {
            $query .= " WHERE (" . implode(" AND ", $where_clauses) . ")";
        }
        $query .= " ORDER BY role_id";
        // Wrap everything in an exception handler so that the program always
        // gets _something_ back.
        try {
            // Temporarily set the error handling to include exceptions...
            set_error_handler(array('\OMIS\Auth\RoleCategory', 'exceptionErrorHandler'));
            $query_results = $this->database->query($query)->fetch_all();
        } catch (Exception $e) {
            error_log(__FILE__ . ": " . __METHOD__ . ": " . $e->getCode() . " = " . $e->getMessage());
            return [];
        }
        
        restore_error_handler();
        
        // Populate the return list...
        $role_ids = [];
        foreach ($query_results as $result) {
            $role_ids[] = (int) $result[0];
        }
        
        /* Remove super admin if not wanted, similarly ensure it's there if it
         * is wanted...
         */
        if ($include_superadmin) {
            if (!in_array(Role::USER_ROLE_SUPER_ADMIN, $role_ids)) {
                array_push($role_ids, Role::USER_ROLE_SUPER_ADMIN);
            }
        } else {
            if (in_array(Role::USER_ROLE_SUPER_ADMIN, $role_ids)) {
                $role_ids = array_diff($role_ids, (array) Role::USER_ROLE_SUPER_ADMIN);
            }
        }
        
        return $role_ids;
    }
    
    /**
     * Get a list of roles in the category. if $exclude_user_role is TRUE, then 
     * the currently logged in user's role is excluded from the list.
     * 
     * @return \OMIS\Auth\Role[] List of roles in the category
     */
    public function getRoles()
    {
        return array_map("\OMIS\Auth\Role::loadID", $this->role_ids);
    }
    
    public function containsRole($role_id)
    {
        return (in_array((int) $role_id, $this->role_ids));
    }
}

