<?php
namespace OMIS\Auth;

/**
 * Class to manage page-level access to OMIS components.
 *
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */
class Page
{
    /**
     * Function to check if a page that's being routed through manage.php or
     * assess.php by shortcode (e.g. manage.php?page=something) to see if that
     * page is restricted behind role-level access.
     * 
     * @global \OMIS\Database\CoreDB $db Database instance.
     * @param string $page_name
     * @return boolean TRUE if protected, FALSE if not
     */
    public static function isProtected($page_name)
    {
        global $db;
        if (!isset($db)) {
            $db = \OMIS\Database\CoreDB::getInstance();
        }
        
        $page_name = $db->clean($page_name);

        $q = "SELECT COUNT(`page_id`) FROM `pages` INNER JOIN `page_levels` "
                . "USING (`page_id`) WHERE `page_name` = '" . $page_name . "';";

        $result = \OMIS\Database\CoreDB::single_result($db->query($q));
        return ($result > 0);
    }
    
    /**
     * Function to check with the database if the user is permitted to access
     * a given page or not.
     * 
     * @global \OMIS\Database\CoreDB $db Database instance.
     * @param string $page_name  Shortcode for page being requested
     * @param int    $role_id    Role type (Examiner, Admin, etc.)
     * 
     * @return boolean Returns true if the user's role is permitted to view page
     */
    public static function hasAccess($page_name, $role_id)
    {
        global $db;
        if (!isset($db)) {
            $db = \OMIS\Database\CoreDB::getInstance();
        }
        
        $page_name = $db->clean($page_name);
        $role_id = $db->clean($role_id);
        $q = "SELECT `level_enabled` AS `allowed` FROM `pages` INNER JOIN "
            . "`page_levels` USING (`page_id`) WHERE (`page_name` = '"
            . $page_name . "') " . "AND (`page_level` = " . $role_id . ");";
        $result = \OMIS\Database\CoreDB::single_result($db->query($q));
        return ($result == 1);
    }
}
