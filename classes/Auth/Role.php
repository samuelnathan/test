<?php
namespace OMIS\Auth;

/**
 * PHP version 5
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * DW: (14/8/2013) Define a simple class that contains the details for a given 
 * role so as to avoid repeated database calls, just get the whole chunk of data
 * for the role.
 */

class Role
{
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_EXAMINER = 2;
    const USER_ROLE_EXAM_ADMIN = 3;
    const USER_ROLE_EXAM_ASSISTANT = 4;
    const USER_ROLE_STATION_MANAGER = 5;
    const USER_ROLE_SUPER_ADMIN = 6;
    const USER_ROLE_SCHOOL_ADMIN = 7;
    
    private $role_id;
    private $name;
    private $shortname;
    private $description;
    private $importexport_field;
    private $database;
    private $config;
    private $hidden_properties;
    /* Denotes whether the current record is deemed "dirty" or not relative to
     * how it was when it was constructed.
     */
    private $dirty;
    
    /** Default constructor
     * 
     * @param int    $role_id            Role ID
     * @param string $name               Name of Role
     * @param string $shortname          Shortened name of role
     * @param string $description        Short description of the role
     * @param string $importexport_field Column name for delimited CSV etc.
     */
    public function __construct(
        $role_id = null,
        $name = null,
        $shortname = "Not Found/Set",
        $description = "(Not Found/Set)",
        $importexport_field = null
    ) {
        $this->role_id = $role_id;
        $this->name = (is_null($name) ? "(Not Found/Set)" : $name);
        $this->shortname = $shortname;
        $this->description = $description;
        $this->importexport_field = $importexport_field;
        
        /* If the field name specified for import and export is not specified
         * and the name is, use an, erm, "educated guess" to give it a name.
         * This convention is based on what's been done before the numeric role 
         * ID system was introduced.
         * Essentially, lower case the name and replace spaces with underscores.
         */
        if (is_null($importexport_field) && !is_null($name)) {
            $this->importexport_field = str_replace(" ", "_", strtolower($name));
        }
        
        /* If there's no role ID, assume it's a new record, and is therefore
         * "dirty" with respect to the database.
         */
        $this->dirty = ($role_id === null);
        
        $this->hidden_properties = array('hidden_properties', 'config', 'database');
        
        /* Automatically obtain configuration and database objects if they're
         * not already set.
         */
        global $config, $db;
        if (!isset($config)) {
            $this->config = new \OMIS\Config;
        } else {
            $this->config = $config;
        }
        
        if (!isset($db)) {
            $this->database = \OMIS\Database\CoreDB::getInstance($this->config->db);
        } else {
            $this->database = $db;
        }
    }
    
    /**
     * Property getter method
     * 
     * @param string $property The Name of the property.
     * 
     * @return string The property's value.
     */
    public function __get($property)
    {
        if (property_exists($this, $property) && (!in_array($property, $this->hidden_properties))) {
            return $this->$property;
        }
    }
    
    /**
     * Property setter method
     * 
     * @param string $property Name of the property
     * @param string $value    Property's new value
     */
    public function __set($property, $value)
    {
        // Need to prevent the user from setting the "dirty" property.
        if (property_exists($this, $property) && ($property != 'dirty') && (!in_array($property, $this->hidden_properties))) {

            // Flag the Role as dirty if needs be...
            if ($this->$property == $value) {
                // Do nothing.
            }
            
            /* If we get to here, the property's value would need to be changed
             * and therefore _could_ get "dirty"...
             */
            try {
                $this->$property = $value;
                $this->dirty = true;
            } catch (Exception $ex) {
                $this->dirty |= false;
                // @TODO (DW) Log this error.
                // Do nothing to trap the exception.
            }
        }
    }

    /**
     * Private exception handler for when 
     * 
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     * @throws ErrorException
     */
    public static function exceptionErrorHandler($errno, $errstr, $errfile, $errline)
    {
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }

    /**
     * Magic method to override how the object is represented as a string when
     * using print_r(), echo(), etc. Supersedes Role::summarize()
     * 
     * @return string text-based summary of the role.
     */
    public function __toString()
    {
        return "$this->name ($this->shortname) Role: $this->description";
    }
    
    /**
     * Function to load a role from a given ID. Returns an empty Role if it
     * fails.
     * 
     * @param int $id ID number of role to load.
     * @return \Role
     * @uses Role::exceptionErrorHandler
     */
    public static function loadID($role_id)
    {
        /* A custom error handler to help this function properly identify when
         * something's gone wrong as standard PHP exception handlers annoyingly
         * don't handle notices or warnings.
         */
        global $config;
        if (!isset($config)) {
            $config = new \OMIS\Config();
        }
        $database = \OMIS\Database\CoreDB::getInstance($config->db);
        
        // Make sure the role ID isn't suspicious looking.
        $role_id = $database->clean($role_id);
        
        // Wrap everything in an exception handler so that the program always
        // gets _something_ back.
        try {
            // Temporarily set the error handling to include exceptions...
            set_error_handler(array('\OMIS\Auth\Role', 'exceptionErrorHandler'));
            $query = "SELECT role_name, role_shortname, role_description FROM roles WHERE role_id = $role_id";
            list($role_name, $role_short_name, $role_description) = $database->query($query)->fetch_row();
            $role = new Role($role_id, $role_name, $role_short_name, $role_description);
        } catch (Exception $e) {
            error_log(__FILE__ . ": " . __METHOD__ . ": " . $e->getCode() . " = " . $e->getMessage());
            $role = new Role();
        }

        // Resume normal error-handling.
        restore_error_handler();
        
        return $role;
    }
    
    public static function delete($role_ids)
    {
        $database = \OMIS\Database\CoreDB::getInstance();
        
        // Array to contain the IDs of roles that have been successfully deleted.
        $successful_deletions = [];
        
        // If we get a single integere
        if ((!is_array($role_ids)) && (is_int($role_ids))) {
            $role_ids = array($role_ids);
        }
        
        // Clean up the ids (just in case)
        $role_ids = array_map("\OMIS\Database\CoreDB::clean", $role_ids);
        
        // Bail out if the ids aren't positive integers.
        if (!isArrayInteger($role_ids, true)) {
            error_log(__METHOD__ . ": Passed an invalid array of role IDs to delete: " . print_r($role_ids, true));
            return [];
        }
        
        /* Delete each one in turn to see what happens. Doing it one at a time
         * to see how many succeed.
         */
        foreach ($role_ids as $role_id) {
            $query = "DELETE FROM `roles` WHERE `role_id` = $role_id;";
            if ($database->query($query)) {
                // Want to return each value as an integer...
                $successful_deletions[] = (int) $role_id;
            }
        }
        
        return $successful_deletions;
    }
    
    /**
     * Save the current Role to the database.
     * 
     * @return boolean
     */
    public function save()
    {
        // Don't bother saving if the record isn't dirty...
        if (!$this->dirty) {
            return true;
        }
        
        /* Define the query based on whether we're updating the record or
         * inserting a new one.
         */
        if (($this->role_id == null) || ($this->role_id == 0)) {
            /* We're inserting a new record if the role_id is null (i.e.
             * undefined) or zero (as there can't be a record with ID number
             * zero in the database anyway, the autoincrement starts at one
             * and counts upwards...
             */
            $query = "INSERT INTO roles (role_name, role_shortname, role_description, "
                . "role_importexport_field) VALUES ('" . $this->name
                . "', '" . $this->shortname . "', '"
                . $this->description . "', '" . $this->importexport_field . "');";
        } else {
            // We're updating an existing record.
            $query = "UPDATE roles SET role_name = '" . $this->name
                . "', role_shortname = '" . $this->shortname
                . "', role_description = '" . $this->description
                . "', role_importexport_field = '" . $this->importexport_field
                . "' WHERE role_id = " . $this->role_id . ";";
        }
        
        $success = $this->database->query($query);
        if ($success) {
            /* If there's no role_id, then we're looking at a new record. That
             * being the case, we need to get the insert ID of the record we
             * just added
             */
            if (($this->role_id === null) || ($this->role_id === 0)) {
                $this->role_id = $this->database->inserted_id();
            }
            
            /* If the record has been saved successfully, then it's no longer
             * dirty as far as the database is concerned.
             */
            $this->dirty = false;
        }
        
        return $success;
    }
    
    
    /**
     * Check if the current user's role is permitted to access the page
     * 
     * @param string $page   Short name of page.
     * @param bool   $silent TRUE = Don't show error DIV and don't log. Used for
     *                       rendering menus to determine whether a page can be
     *                       accessed or not.
     * @return boolean TRUE if this role can access $page.
     */
    public function canAccess($page = null, $silent = false)
    {
        if (is_null($page)) {
            $page = \OMIS\Utilities\Request::getPageID();
        } else {
            $page = trim($page);
        }
        
        if ($page == "") {
            return false;
        }
        
        try {
            $has_access = Page::hasAccess($page, $this->role_id);
        } catch (Exception $e) {
            error_log(__CLASS__ . ": " . $e->getMessage());
            $has_access = false;
        }
        
        if (!$has_access) {
            if (!$silent) {
                $log_message = "access denied to page $page for role '" 
                    . $this->name . "' (" . $this->role_id. ")";
                \Analog::notice($log_message);
                echo '<div class="errordiv fc">Not authorized to view this section</div>';
            }
        }
        
        return $has_access;
    }
    
    /**
     * Return the object's important values (as well as whether they tally with
     * what's in the database or have been changed yet (e.g. the "dirty" 
     * attribute) as an array for use in templates, etc. If $associative is false,
     * then a set of values is returned, otherwise an associative array of
     * key/value pairs is returned.
     * 
     * @param bool $associative Return keys (names) to go with values
     * @param bool $show_dirty  If TRUE, include a field to denote if the values
     *                          are those in the database for this role or not.
     * @return mixed[] Array of values describing current role.
     */
    public function toArray($associative = true, $show_dirty = false)
    {
        $result = array(
            'role_id' => $this->role_id,
            'name' => $this->name,
            'shortname' => $this->shortname,
            'description' => $this->description,
            'importexport_field' => $this->importexport_field
        );
        
        if ($show_dirty) {
            $result['dirty'] = $this->dirty;
        }
        
        if ($associative) {
            return $result;
        } else {
            return array_values($result);
        }
    }
    
    public static function isRole($object) {
        return (get_class($object) === get_class());
    }
}
