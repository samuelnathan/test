<?php

namespace OMIS\Storage;


interface ImportTemplatesSource
{

    /**
     * Gets an array with the paths for the import templates, with the key being the import type and the value
     * being the path to the template
     *
     * @return array
     */
    public function getImportTemplatesPath();

}