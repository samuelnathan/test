<?php

namespace OMIS\Storage;


interface ManualsSource
{

    /**
     * Gets an array with the manuals for the given instance. Each item in the array is an associative array with the
     * values 'name', 'link' and 'timestamp'
     *
     * @return array Array with the manual paths
     */
    public function getManualPaths();

}