<?php

namespace OMIS\Storage;

use OMIS\Utilities\Path;

/**
 * Implementation of ManualsSource that obtains the manuals from a specified folder
 * @package OMIS\Storage
 */
class DirectoryManualsSource implements ManualsSource
{

    private $manualsDirectory;

    public function __construct($manualsDirectory)
    {
        $this->manualsDirectory = $manualsDirectory;
    }

    /**
     * Populates the manual array with the pdf files contained in the $manualsDirectory folder. Gets the manual
     * name from the filename and sorts the array by modification time and name
     *
     * @return array Array with the manual paths
     */
    public function getManualPaths()
    {
        $dir = new \DirectoryIterator($this->manualsDirectory);
        $manuals = [];

        // Iterate through the manuals directory
        foreach ($dir as $fileinfo) {
            // Ignore directories or dots
            if ($fileinfo->isDir() || $fileinfo->isDot() || strtolower($fileinfo->getExtension()) !== 'pdf') continue;

            // Get the manual info from the file
            $ext = $fileinfo->getExtension();
            $name = $fileinfo->getBasename($ext ? '.' . $ext : null);
            $path = Path::toURL($fileinfo->getPathname());

            // Add it to the resulting array
            $manuals[] = [
                'link' => $path,
                'name' => $name,
                'timestamp' => $fileinfo->getMTime()
            ];
        }

        // Sort the array by timestamp, name ascending
        uasort($manuals, function ($m1, $m2) {
            if ($m1['timestamp'] !== $m2['timestamp'])
                return $m1['timestamp'] - $m2['timestamp'];

            return strcmp($m1['name'], $m2['name']);
        });

        return $manuals;
    }

}