<?php

namespace OMIS\Storage;


use OMIS\Utilities\Path;

/**
 * Implementation of ImportTemplatesSource that obtains the templates file from a specified folder
 *
 * Class DirectoryImportTemplatesSource
 * @package OMIS\Storage
 */
class DirectoryImportTemplatesSource implements ImportTemplatesSource
{

    private $importFilesDirectory;

    public function __construct($importFilesDirectory)
    {
        $this->importFilesDirectory = $importFilesDirectory;
    }

    /**
     * Popoulates the template path array with the subdirectories inside $importFilesDirectory and the first file inside
     * each subdirectory
     *
     * @return array
     */
    public function getImportTemplatesPath()
    {
        $dir = new \DirectoryIterator($this->importFilesDirectory);
        $result = [];

        // Iterate through the 'import_files' directory
        foreach ($dir as $fileinfo) {
            // Ignore files or dots
            if (!$fileinfo->isDir() || $fileinfo->isDot()) continue;

            // Get the name of the template from the directory name
            $name = $fileinfo->getFilename();

            // Get the first file from the directory
            $templateDir = new \DirectoryIterator($fileinfo->getPathname());
            $template = null;
            foreach ($templateDir as $templateFile) {
                if (!($templateFile->isDir() || $templateFile->isDot())) {
                    $template = $templateFile;
                    break;
                }
            }

            // If the directory has no files, ignore it
            if ($template === null) continue;

            // Get the path
            $path = Path::toURL($template->getPathname());
            // Add it to the resulting array
            $result[$name] = $path;
        }

        return $result;
    }

}