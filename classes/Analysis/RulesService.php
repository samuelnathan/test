<?php
 namespace OMIS\Analysis;
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 class RulesService
 {
    private $db;

    /**
     * Constructor
     */
    public function __construct()
    {   
       $this->db = \OMIS\Database\CoreDB::getInstance();
    }

    /**
     * Update analysis rules
     *
     * @param mixed $exam   exam record
     * @return bool status  true|false
     */
    public function update($exam)
    {

        if (isset($_POST['station_values']) && count($_POST['station_values']) > 0)
            $this->db->examRules->updateStationValues($_POST['station_values']);

        // Update competency rules (pass values for now until new settings are added)
        if (isset($_POST['competency_pass_values']) && count($_POST['competency_pass_values']) > 0)

            $this->db->examRules->updateCompetencyRules($_POST['competency_pass_values']);

        $this->db->examRules->updateRules($exam['exam_id'], [
            ['field' => "max_grs_fail", 'value' => filter_input(INPUT_POST, 'grs_fail')],
            ['field' => "min_stations_to_pass", 'value' => filter_input(INPUT_POST, 'min_stations_pass')],
            ['field' => "multi_examiner_results_averaged", 'value' => filter_input(INPUT_POST, 'results_averaged')],
            ['field' => "divergence_threshold", 'value' => filter_input(INPUT_POST, 'divergence_threshold')],
            ['field' => "grade_weightings", 'value' => filter_input(INPUT_POST, 'grade_weightings')],
            ['field' => "results_published", 'value' => filter_input(INPUT_POST, 'results_published')],
        ]);

        // Determine again all outcomes with rules changes
        (new \OMIS\Outcomes\OutcomesService($this->db))->setUpToDateByExam($exam['exam_id'],false);

    }

 }
