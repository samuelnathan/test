<?php

namespace OMIS\Log;

/**
 * Analog handler that mutates the entry to use the clients remote IP address instead of the
 * local machines one.
 * Wraps another analog handler
 *
 * Class RemoteIpHandler
 * @package OMIS\Log
 */
class RemoteIpHandler
{

    /**
     * @param \Closure $innerHandler Inner Analog handler that actually performs the log writing
     * @return \Closure
     */
    public static function init(\Closure $innerHandler) {
        return function ($info, $buffered = false) use ($innerHandler) {
            $info['machine'] = $_SERVER['REMOTE_ADDR'];
            $innerHandler($info, $buffered);
        };
    }

}