<?php

namespace OMIS\Log;

/**
 * Analog handler that adds the user in session to the information that is logged
 *
 * @package OMIS\Log
 */
class UserLogHandler
{

    public static function init(\Closure $innerHandler)
    {
        // Change the logging format to show the new field
        \Analog::$format = '%1$s - %2$s - %3$d - %5$s - %4$s' . "\n";

        return function($info, $buffered = false) use ($innerHandler) {
            $info['user'] = $_SESSION['user_identifier'] ?: '<Anonymous>';
            $innerHandler($info, $buffered);
        };
    }

}