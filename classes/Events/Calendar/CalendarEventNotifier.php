<?php

namespace OMIS\Events\Calendar;

require_once __DIR__ . '/../../../vendor/autoload.php';

use \Carbon\Carbon;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Implementation of EventNotifier that sends the notifications to the
 * Calendar queue to be added to the Qpercom Calendar
 */
class CalendarEventNotifier extends \OMIS\Events\ConnectionEventNotifier {

    private $config;        // Observe configuration          

    private $connection;    // Connection to the RabbitMQ queue        
    private $channel;       // RabbitMQ channel
    
    public function __construct() {
        // Get the configuration
        global $config;
        if (!isset($config)) {
            $this->config = new \OMIS\Config;
        } else {
            $this->config = $config;
        }

        parent::__construct();
    }

   /**
    * Attempt to establish a connection with a notification service
    * @return boolean true if the connection was successful. False otherwise
    */
    protected function tryConnect() {
        try {
            // Initialize the connection to the queue
            $this->connection = new AMQPStreamConnection(
                $this->config->eventQueue['host'],
                $this->config->eventQueue['port'],
                $this->config->eventQueue['userName'],
                $this->config->eventQueue['password']);

            // Get the channel
            $this->channel = $this->connection->channel();

            // Declare the queue in case it didn't exist previously
            $this->channel->queue_declare(
                $this->config->eventQueue['queueName'],
                false, false, false, false
            );

            return true;
        } catch (\Exception $e) {
            \Analog::error('Exception connecting to the queue: ' . $e);
            return false;
        }
    }
   

    /**
    * Notifies the creation or updating of an exam
    * @param array $exam Associative array with the fields of the exam to notify
    * @param string $dateFrom String representation of the date when the exam begins
    * @param string $dateEnd String representation of the date when the exam ends
    */
    public function doNotifyExamUpdate($exam, $dateFrom, $dateTo)
    {
        if (!$dateFrom || !$dateTo) {
            $this->notifyExamDelete($exam['exam_id']);
            return;
        }

        // Create the event
        $event = [
            "id" => $exam['exam_id'],
            "data_source" => $this->config->db['schema'],
            "type" => "createOrUpdate",
            "client" => $this->config->localization['client']['full_name'],
            "event" => $exam['exam_name'],
            "start_time" => $this->getTimeMs($dateFrom),
            "end_time" => $this->getTimeMs($dateTo),
            "description" => $exam['exam_description']
        ];

        // Specify colour in the event if it's set in the configuration
        if ($this->config->eventQueue['eventColour'])
            $event['colour'] = strtolower(trim($this->config->eventQueue['eventColour']));

        $this->publicMessage($event);
    }

    /**
    * Notifies the deletion of a previously notified exam
    * @param int $examId
    */
    public function doNotifyExamDelete($examId)
    {
        $event = [
            "type" => "delete",
            "id" => $examId,
            "data_source" => $this->config->db['schema']
        ];

        $this->publicMessage($event);
    }

    /**
     * Public a message in the queue
     * @param array $message
     */
    private function publicMessage($message) {

        // Create and publish the message
        $msg = new AMQPMessage(json_encode($message));

        try {
            $this->channel
                ->basic_publish($msg, '', $this->config->eventQueue['queueName']);
        } catch (\Exception $e) {
            Analog::error("Exception thrown when trying to publish message to the event queue: " . $e);
        }
    }

    /**
     * Gets the absolute time in milliseconds from a string date and using the
     * timezone from the observe configuration
     * @param string $date Date in format yyyy-mm-dd hh:mm:ss
     */
    private function getTimeMs($date) {
        $cDate = Carbon::createFromFormat('Y-m-d H:i:s', $date, $this->config->localization['timezone']);
        return $cDate->timestamp;
    }

}