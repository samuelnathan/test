<?php

namespace OMIS\Events;

require_once __DIR__ . '/../../vendor/autoload.php';

/**
 * Abstract implementation of EventNotifier that attempts to establish a
 * connection to a service. If the connection fails, the events are not
 * notified
 */
abstract class ConnectionEventNotifier implements \OMIS\Events\EventNotifier {

  /**
   * Boolean flag that indicates if the notifier established a connection
   * successfully
   */
  private $connected;

  public function __construct() {
    // Try to establish a connection and save the result
    $this->connected = $this->tryConnect();
  }

  public function notifyExamUpdate($exam, $dateFrom, $dateTo) {
    // Delegate the notification logic on the concrete implementation, if the
    // connection was successful
    if ($this->connected)
      $this->doNotifyExamUpdate($exam, $dateFrom, $dateTo);
  }

  public function notifyExamDelete($examId) {
    // Delegate the notification logic on the concrete implementation, if the
    // connection was successful
    if ($this->connected)
      $this->doNotifyExamDelete($examId);
  }

  /**
   * Attempt to establish a connection with a notification service
   * @return boolean true if the connection was successful. False otherwise
   */
  protected abstract function tryConnect();

  protected abstract function doNotifyExamUpdate($exam, $dateFrom, $dateTo);

  protected abstract function doNotifyExamDelete($examId);

}