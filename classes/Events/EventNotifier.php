<?

namespace OMIS\Events;

interface EventNotifier
{

  /**
   * Notifies the creation or updating of an exam
   * @param array $examData Associative array with the fields of the
   *                        exam to notify
   */
  public function notifyExamUpdate($exam, $dateFrom, $dateTo);

  /**
   * Notifies the deletion of a previously notified exam
   * @param int $examId
   */
  public function notifyExamDelete($examId);

}