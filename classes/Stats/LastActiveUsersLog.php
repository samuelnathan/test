<?php

namespace OMIS\Stats;


use Carbon\Carbon;

class LastActiveUsersLog implements LastActiveUsers
{

    private $logFile;

    private $logDir;

    private $userAsField;

    public function __construct($logFile, $logDir, $userAsField)
    {
        $this->logFile = $logFile;
        $this->logDir = $logDir;
        $this->userAsField = $userAsField;
    }

    /**
     * @param $offset
     * @param $length int
     * @return array
     */
    public function getLastActiveUsers($length, $offset = 0)
    {
        $result = [];

        // Get log files to scan
        $logFiles = scandir($this->logDir, SCANDIR_SORT_ASCENDING);
        $logFileName = basename($this->logFile);
        $logFilesFiltered = array_values(
            array_filter($logFiles, function($logFile) use ($logFileName) {
                return (strpos($logFile, $logFileName) !== false);
            }
            ));
        $logFilesTake = array_reverse(array_slice($logFilesFiltered, 0, 2));

        //
        foreach ($this->iterateFile($logFilesTake) as $entry) {
            if (strtolower($entry['user']) !== '<anonymous>')
                $result[$entry['user']] = [
                    'date' => $entry['date'],
                    'ip' => $entry['ip']
                ];
        }

        // Sort the users by activity date descending
        uasort($result, function (array $u1, array $u2) {
           if ($u1['date']->eq($u2['date'])) return 0;
           return $u1['date']->lt($u2['date']) ? 1 : -1;
        });

        return array_slice($result, $offset, $length, true);
    }

    private function iterateFile(array $files) {
        $directory = dirname($this->logFile);

        foreach ($files as $file) {
            $handle = fopen($directory . '/' . $file, 'r');

            while (($buffer = fgets($handle, 16384)) !== false) {
                $fields = explode(' - ', $buffer);

                $user = $this->userAsField ?
                    $fields[3] :
                    explode(' ', trim($fields[3]))[0];

                yield [
                    'user' => $user,
                    'date' => Carbon::parse($fields[1]),
                    'ip' => $fields[0]
                ];
            }
        }
    }

}