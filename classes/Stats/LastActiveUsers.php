<?php

namespace OMIS\Stats;


interface LastActiveUsers
{

    /**
     * @param $offset int Initial offset
     * @param $length int Maximum length of the list to get
     * @return array
     */
    public function getLastActiveUsers($length, $offset = 0);

}