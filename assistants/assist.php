<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    return false;
}

include __DIR__ . "/../extra/helper.php";

$url = "manage.php?page=assist";
$fields = [
    "user_id",
    "surname",
    "forename",
    "email",
    "contact_number"
];
list($order_type, $order_index, $order_map) = prepareListOrdering("desc", 0, $fields);

// Get all assistant accounts
$admins = $db->users->getAllAssistants();
$count = count($admins);
$rowCount = 0;

?>
<div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Manage Assistants
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      Manage all assistants that can assist/support <?=gettext('exams')?>
    </div>   
  </div> 
</div> 
 <div class="topmain-clear"></div>
</div>
<div class="contentdiv">
<form name="assist_list_form" id="assist_list_form" method="post" action="assistants/assistants_redirect.php" onsubmit="return(ValidateForm(this))">
 <table id="assist_table" class="table-border">
    <tr id="title-row" class="title-row">
      <td class="all-checkbox-td">
         <input type="checkbox"  id="check-all"  <?php if($count == 0) { ?>disabled="disabled"<?php } ?>/>
      </td>
      <th><?=makeColumnOrderable("Identifier", $order_map[$order_index], "user_id", 1, $order_type, $admins, $url, "standard", NULL, NULL); ?></th>
      <th><?=makeColumnOrderable("Surname", $order_map[$order_index], "surname", 2, $order_type, $admins, $url, "standard", NULL, NULL); ?></th>
      <th><?=makeColumnOrderable("Forenames", $order_map[$order_index], "forename", 3, $order_type, $admins, $url, "standard", NULL, NULL); ?></th>
      <th><?=makeColumnOrderable("Email Address", $order_map[$order_index], "email", 4, $order_type, $admins, $url, "standard", NULL, NULL); ?></th>
      <th><?=makeColumnOrderable("Mobile Number", $order_map[$order_index], "contact_number", 5, $order_type, $admins, $url, "standard", NULL, NULL); ?></th>
      <td class="title-edit-top">&nbsp;</td>
    </tr>
    <?php 
	   
   // Render records
   if ($admins && $count > 0) {
      foreach ($admins as $admin) {
	  $rowCount++;
	 ?><tr id="assist_row_<?=$rowCount?>" class="datarow">
             <td class="checkb">
		<input type="checkbox" name="assist_check[]" id="assist-check-<?=$rowCount?>" value="<?=$admin["user_id"]?>"/>
	     </td>
             <td class="c"><?=$admin["user_id"]?></td>
             <td><?=$admin["surname"]?></td>
             <td><?=$admin["forename"]?></td>
	     <td class="c"><?php 
               if (strlen($admin["email"]) > 0) { 
                  echo $admin["email"]; 
               } else { 
                  ?><span class="red">Not found</span><?php 
               } ?>
             </td>
	     <td class="c"><?php 
               if (strlen($admin["contact_number"]) > 0) {
                  echo $admin["contact_number"];
               } else { 
                  ?><span class="red">Not found</span><?php 
               } 
             ?></td>
	     <td class="titleedit height30">
	       <?php editButton("edit_".$rowCount, "Edit", "Edit")?>
             </td>
	  </tr>
          <?php
      }
   }
   
   // Render options row
   if ($rowCount > 0) {	  
        ?><tr id="base-tr">
            <td colspan="7" class="base-button-options" id="base-button-td">
            <div class="fl">
               <select name="todo" id="todo" disabled="disabled" class="custom-select custom-select-sm"><option value="delete">delete from system</option></select>
               <input type="submit" value="go" class="btn btn-outline-default btn-sm" name="go_assist" id="go-assist" disabled="disabled"/>
            </div>	
            <div class="fr" id="add-div">
               <input type="button" value="add +" name="add" id="add" class="rb btn btn-success btn-sm/>
               <input type="hidden" value="<?=$rowCount?>" name="count" id="count"/>
            </div>			 
            </td>
          </tr><?php
   } else {
        ?><tr id="first_assist_row">
            <td colspan="7" class="mess">
               <div id="assist_row_div" class="tablestatus2">No assistants attached, please click the 'add' button to add new assistants</div>
            </td>
          </tr>
          <tr id="base-tr">
            <td colspan="7" class="base-button-options" id="base-button-td">	
             <div class="fr" id="add-div">
              <input type="button" value="add +" name="add" id="add" class="rb btn btn-success btn-sm"/>
             </div>			 
            </td>
          </tr>
        <?php
    }
 ?> 
 </table>
</form>
</div>
