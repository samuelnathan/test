<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 23/03/2014
 * © 2014
 * Redirect file for Assistants Redirect Section
 */
/* Defined to tell any included files that they have been included rather than
 * being invoked directly from the browser. Need to declare it anywhere that a
 * file is directly invoked by the user (via AJAX).
 */
define('_OMIS', 1);

use \OMIS\Session\FlashList as FlashList;
use \OMIS\Session\FlashMessage as FlashMessage;

$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

$flashList = new FlashList();

#Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess('assistants_redirect')) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = array(
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    );
    $template->render($data);
    return;
}

// Delete Assistants
if (!filter_has_var(INPUT_POST, 'go_assist')) {
    header('Location: ../');
}

$todo = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING);

if ($todo == "delete") { //get selected -- if delete
    $to_delete = filter_input(INPUT_POST, 'assist_check', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    
    if (!empty($to_delete)) {
        $deleted_count = $db->users->deleteAssistants($to_delete);
        if ($deleted_count == count($to_delete)) {
            $flashList->add(
                "$deleted_count assistant(s) deleted successfully.",
                FlashMessage::SESSION_FLASH_INFO
            );
        } else {
            $flashList->add(
                "$deleted_count of " . count($to_delete) . " assistants deleted successfully.",
                FlashMessage::SESSION_FLASH_WARNING
            );
        }
    } else {
        $flashlist->add(
            "Please specify one or more assistants to delete",
            FlashMessage::SESSION_FLASH_INFO
        );
    }
}
header("Location: ../manage.php?page=assist");
