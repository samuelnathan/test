<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 3012/2015
 * © 2015 Qpercom Limited.  All rights reserved.
 */
define('_OMIS', 1);

$mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);
if (!is_null($mode)) {
    include __DIR__ . '/../extra/essentials.php';
    #Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// This will be NULL if there's no such parameter.
$user_id = filter_input(INPUT_POST, 'org_user_id');


/**
 * Add record mode
 */
if ($mode == 'add') {
    $template = new OMIS\Template('ajx_assist_add.html.twig');
    $template->render();
    
/**
 * Submit record mode
 */    
} elseif ($mode == 'submit') {

    $user_id = filter_input(INPUT_POST, 'add_id');
    
    // Invalid Identifier, then abort
    if (!identifierValid($user_id)) {
       error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
       exit;
    }
   
    if (!$db->users->doesUserExist($user_id)) {
        $db->users->insertAssistant(
                $user_id,
                $_POST['add_fn'],
                $_POST['add_sn'],
                $_POST['add_email'],
                $_POST['add_mobile']
        );
    } else {
        echo 'USER_ID_EXISTS';
    }
    
/**
 * Edit record mode
 */
} elseif ($mode == 'edit') {
    if (is_null($user_id) || !$db->users->doesUserExist($user_id)) {
        echo 'USER_NON_EXIST';
        return;
    }
    
    $user = $db->users->getUser($user_id, 'user_id', true);
    $template = new OMIS\Template('ajx_assist_edit.html.twig');
    $template->render(['user' => $user]);
    
/**
 * Update record mode
 */    
} elseif ($mode == 'update') {
    /* @TODO (DW) There's no handler here for if we try to edit a non-existent 
     * user. I guess that's not the end of the world but at the same time it's
     * incomplete.
     */
    if ($db->users->doesUserExist($user_id)) {
        $editor_id = trim($_POST['edit_id']);
        
        // Invalid Identifier, then abort
        if (!identifierValid($editor_id)) {
           error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
           exit;
        }
        
        if ($editor_id != $user_id && $db->users->doesUserExist($editor_id)) {
            echo 'USERID_DOES_EXIST';
        } else {
            $db->users->updateUser(
                    $user_id,
                    $editor_id,
                    $_POST['edit_sn'],
                    $_POST['edit_fn'],
                    $_POST['edit_email'],
                    $_POST['edit_mobile']
            );
        }
    }
}
