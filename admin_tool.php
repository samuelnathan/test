<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

use \OMIS\Auth\Role as Role;

global $config;
if (!isset($config)) {
    $config = new OMIS\Config;
}

global $db;
if (!isset($db)) {
    $db = \OMIS\Database\CoreDB::getInstance();
}

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
$user_role = Role::loadID($_SESSION['user_role']);
if (!$user_role->canAccess()) {
    return false;
}

/* This is the list of page tools to render. The key of each entry in this array
 * is the page_id as far as the access control and manage.php go; "feature_id" is
 * the paid-for feature that the tool links to (if any, otherwise set it to null),
 * "name" and "description" should be obvious.
 */
$tools = [
    "swap_tool" => [
        "name" => "Swap Tool",
        "feature_id" => 'swap-tool',
        "description" => "Swap " . gettext('exam') . " " . gettext('students') . " and/or results where " . gettext('students') . " were incorrectly assessed"
    ],
    "feedback_tool" => [
        "name" =>  gettext('Exam') . " Feedback Tool",
        "feature_id" => "feedback-system",
        "description" => "Email " . gettext('exam') . " feedback to the " . gettext('students') . " (or download feedback)"
    ],
    "station_tool" =>[
        "name" => "Import &amp; Export Assessment " . ucwords(gettext('forms')),
        "feature_id" => "import-export-forms",
        "description" => "Import &amp; export assessment " . gettext('forms') . " to/from your computer"
    ],
    "loginman_tool" => [
        "name" => "Clear All Login Timeouts",
        "feature_id" => null,
        "description" => "Reset attempted login attempt counts and unblock users who have tried to log in too often"
    ],
    "userlog_tool" => [
        "name" => "View User Logs",
        "feature_id" => null,
        "description" => "Review user activity within " . $config->getName()
    ],
    "role_tool" => [
        "name" => "Manage User Roles",
        "feature_id" => null,
        "description" => "Add, edit and remove user roles"
    ],
    "grs_types_tool" => [
        "name" => "Manage Global Rating Scales",
        "feature_id" => "grs-management",
        "description" => "Add, edit and remove global rating scales"]
];



$templateData = ["tools" => []];

foreach ($tools as $page_id => $tool_details) {
    // Assume the user can see the page.
    $can_access = \TRUE;
    $feature_id = $tool_details["feature_id"];
    if (!empty($feature_id) && $feature_id != "feedback-system") {
        // This tool is tied to a paid-for feature. See if this feature is 
        // enabled for this user and this role.
        $can_access &= $db->features->enabled($feature_id, $user_role->role_id);
    }
    
    // If we're still okay, or if there's no $$$ feature associated with this
    // tool, make sure that the user is allowed see the page as a final check.
    if ($can_access && ($user_role->canAccess($page_id, \TRUE))) {
        $tool_details["url"] = "manage.php?page=" . $page_id;
        $templateData["tools"][] = $tool_details;
    }
}

// Load and render the template.
$template = new OMIS\Template("admin_tool.html.twig");
$template->render($templateData);