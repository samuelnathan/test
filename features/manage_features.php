<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
global $db;
global $page;

// Critical Session Check
$session = new OMIS\Session;
$session->check();
 
// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess($page)) {
    return false;
}

// Use template class
use OMIS\Template as Template;

// Filter group
$filterGroup = filter_input(INPUT_GET, 'filter', FILTER_SANITIZE_STRIPPED);

// Need this to get a list of roles.
$features = $db->features->getFeaturesandRoles();

// Feature groups
$groups = $db->features->getFeatureGroups();

// Get a list of the available roles that can log in that aren't the super admin.
$roles = (new OMIS\Auth\RoleCategory(null, $include_superadmin = false, $login_only = true))->getRoles();

// Unset features updated if it's set
if (isset($_SESSION['features_updated'])) {
  unset($_SESSION['features_updated']);
  $buttonText = 'updated';
} else {
  $buttonText = 'update';  
}

// Prepare a combined features and roles array to pass to template
$featuresRoles = [];
foreach ($features as &$feature) {
 
 $featureID = $feature['feature_id'];
         
 /* Iterate through the possible roles. 
  */
 $tempRoles = [];
 foreach ($roles as $role) {
   $roleID = $role->role_id;
   /* If the role exists, then set it to be enabled
    * (or not) based on its value. If it's not 
    * there, then assume it's not enabled.
    */
   if (array_key_exists($roleID, $feature['roles'])) {
     $roleEnabled = ($feature['roles'][$roleID] == 1);
   } else {
     $roleEnabled = false; 
   }
      
   $tempRoles[$roleID] = ['name' => $role->name,
                          'enabled' => $roleEnabled];
   
 }
 
 $feature['roles'] = $tempRoles;
 $featuresRoles[$featureID] = $feature;
}

// Template data
$templateData = ['feature_groups' => $groups,
                 'features_roles' => $featuresRoles,
                 'filter_group' => $filterGroup,
                 'button_text' => $buttonText];

// Load and render the template.
$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($templateData);
