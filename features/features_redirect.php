<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 05/04/2015
 * © 2015 Qpercom Limited.  All rights reserved.
 * Redirect file for system features section
 */
/* Defined to tell any included files that they have been included rather than
 * being invoked directly from the browser. Need to declare it anywhere that a
 * file is directly invoked by the user (via AJAX).
 */
define('_OMIS', 1);

$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

//Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess('features_redirect')) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = array(
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    );
    $template->render($data);
    return;
}

// Update Features
if (isset($_POST['update_features'])) {
    
    $features = (!isset($_POST['features']) || !is_array($_POST['features'])) ? array() : $_POST['features']; 
        
    if ($db->features->updateFeatures($features)) {
        $_SESSION['features_updated'] = true;
    }

    $filterGroup = $_POST['filter_group'];
    header('Location: ../manage.php?page=manage_features&filter=' . $filterGroup);
    
} else {
    //If none of the above return to index page    
    header('Location: ../');
}
