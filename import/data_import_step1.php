 <?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 use \OMIS\Auth\Role as Role;
 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!Role::loadID($_SESSION["user_role"])->canAccess()) {
   
    return false;
    
 }
 
 setA($_SESSION, "dataType", "none");
 $title = "data";
 
 $dataTypes = [
   "5" => gettext("Exam") . " Matrix"
 ];
 
 list($importExamTeam, $importCoursesAccess, $importStudentsAccess, $importScoreSheets) 
 = $db->features->enabled([
     'exam-team', 'student-courses', 
     'student-manage', 'self-assessments'
   ], $_SESSION['user_role']
 );
 
 if ($importExamTeam) {
   
   $dataTypes['4'] = gettext('Exam-Team-Placeholder') . gettext(' Team');
   
 }
 
 if ($importCoursesAccess) {
   
   $dataTypes['2'] = gettext("Courses & Modules");
   
 }
 
 if ($importStudentsAccess) {
   
   $dataTypes['3'] = ucwords(gettext("students"));
   
 }
 
 if ($importScoreSheets) {
 
   $dataTypes['6'] = gettext("Self Assessment Scoresheets");
   
 }
 
 if (filter_has_var(INPUT_GET, "dataType")) {
   
    $_SESSION["dataType"] = trim(filter_input(INPUT_GET, "dataType"));
    
    if (isset($dataTypes[$_SESSION["dataType"]])) {
      
        $title = $dataTypes[$_SESSION["dataType"]];
        
    }
    
 }
 
 // If we have a valid data type then grab it"s template data
 if (array_key_exists($_SESSION["dataType"], $dataTypes)) {
   
   $typeTemplateData = (new ImportFileFields($_SESSION["dataType"]))->getInputFileTemplateDescription();
   
 } else {
   
   $typeTemplateData = [];
   
 }

 $templateData = array_merge(
    [
        "dataLabel"     => $title,
        "dataTypes"     => $dataTypes,
        "selectedType"  => $_SESSION["dataType"],
        "pubbleLink" => $config->support["pubble"]["enabled"]
    ],
    $typeTemplateData
 );
 
 // Only include a reference to the current term if there's one defined.
 if (isset($_SESSION["cterm"]) && sizeof($_SESSION["cterm"]) > 0) {
   
    $templateData["currentTerm"] = $_SESSION["cterm"];
    
 }
 
 // Render template
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($templateData);
 