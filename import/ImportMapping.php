<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 // Need this for the data type definition constants
 require_once "ImportFileFields.php";

 class ImportMapping
 {

     private $_m_customTableHeaders;
     private $_m_tableHeaders;
     private $_m_requiredFields;
     private $_m_fieldRefs;

     public function __construct($dataType, \OMIS\Database\CoreDB $db)
     {

         global $config;
         if (!isset($config)) {
             $config = \OMIS\Config::getInstance();
         }

         $this->_m_tableHeaders = [];
         /* The keys in this array seem to be the database fields, and the arrays
          * associated with them seem to be the field names it understands.
          */
         $this->_m_customTableHeaders = [
             "course_id"            => ["COURSE_ID", "COURSE_CODE", "SPECIALTY_ID"],
             "year_name"          => ["COURSE_YEAR", "ROUND_YEAR"],
             "user_id"           => ["USER_ID", "EXAMINER_ID", "ID", "IDENTIFIER"],
             "course_name"      => ["COURSE_NAME", "SPECIALTY_NAME"],
             "module_id"         => ["MODULE_ID", "mod_id", "MODULE_CODE", "MOD_CODE", "CIRCUIT_GROUPING_ID"],
             "module_name"          => ["MODULE_NAME", "CIRCUIT_GROUPING_NAME"],
             "dept_id"           => ["DEPARTMENT_ID","EXAM_TYPE_ID", "DEPT_ID", "SPECIALTY_ID"],
             "student_id"        => ["STUDENT_ID", "TRAINEE_ID", "ID", "APPLICANT_ID"],
             "candidate_number"  => ["CANDIDATE_NUMBER"],
             "forename"         => ["FORENAMES", "FIRST_NAME", "FORENAME"],
             "surname"       => ["SURNAME"],
             "forename"             => ["FORENAMES", "FIRST_NAME", "FORENAME", "GIVEN_NAME"],
             "surname"             => ["SURNAME", "FAMILY_NAME"],
             "gender"        => ["GENDER", "SEX"],
             "email"         => ["EMAIL", "E-MAIL", "MAIL"],
             "dob"           => ["DOB", "DATE_OF_BIRTH"],
             "activated"        => ["ENABLED", "ACTIVATED"],
             "password"     => ["PASSWORD"],
             "contact_number"       => ["contact_number", "CONTACT_NUMBER", "PHONE"],
             "user_role"         => ["LEVEL", "USER_ROLE"],
             "nationality"       => ["NATIONALITY"],
             "email"             => ["EMAIL"],

             "applicant_id"      => ["APPLICANT_ID"],
             "question"          => ["QUESTION"],
             "response"          => ["RESPONSE"],
             "score"             => ["SCORE"]
         ];

         $tableColumns = [];
         // get the headers of the table containing data to import from db
         switch ($dataType) {

             case IMPORTEXPORT_DATATYPE_SCHOOL_DEPARTMENTS:

                 $tableColumns = $db->schools->getSchoolDepartmentColumns();
                 $this->_m_requiredFields = ["school_id", "dept_id", "dept_name"];

                 break;

             case IMPORTEXPORT_DATATYPE_COURSES_MODULES:

                 $tableColumns = $db->courses->getYearModuleColumns();
                 $this->_m_requiredFields = ["course_id", "course_name", "module_id", "module_name", "year_name"];

                 /**
                  * 'COURSE_NAME' field not required if linked to departments
                  */
                  if ($config->db['couple_depts_courses']) {

                     array_splice($this->_m_requiredFields, 1, 1);
                     $tableColumns = array_diff($this->_m_requiredFields, ['course_name']);

                  }

                 break;

             case IMPORTEXPORT_DATATYPE_STUDENTS:

                 $tableColumns = $db->students->getStudentColumns();
                 $this->_m_tableHeaders["candidate_number"] = $this->_m_customTableHeaders["candidate_number"];
                 $this->_m_tableHeaders["course_id"] = $this->_m_customTableHeaders["course_id"];
                 $this->_m_tableHeaders["module_id"] = $this->_m_customTableHeaders["module_id"];
                 $this->_m_tableHeaders["year_name"] = $this->_m_customTableHeaders["year_name"];
                 $this->_m_requiredFields = ["student_id", "course_id", "year_name", "module_id"];

                 break;

             case IMPORTEXPORT_DATATYPE_EXAMINERS:

                 $this->_m_tableHeaders["user_id"] = $this->_m_customTableHeaders["user_id"];
                 $this->_m_tableHeaders["dept_id"] = $this->_m_customTableHeaders["dept_id"];
                 $this->_m_requiredFields = ["user_id", "dept_id", "user_role"];

                 // If processing password column in the .csv import file
                 if (isset($config->import["password_by_file"]) && $config->import['password_by_file'] == true) {

                    array_push($this->_m_requiredFields, "password");
                    $tableColumns = $db->users->getExaminerColumns();

                 } else {

                    $tableColumns = array_filter($db->users->getExaminerColumns(), function($column) {
                         return strtolower($column) !== 'password';
                    });

                 }

                 break;

             case IMPORTEXPORT_DATATYPE_SCORESHEETS:

                 $tableColumns = ['applicant_id', 'question', 'response', 'score'];
                 $this->_m_requiredFields = ['applicant_id', 'question', 'response', 'score'];

                 $this->_m_tableHeaders["applicant_id"] = $this->_m_customTableHeaders["applicant_id"];
                 $this->_m_tableHeaders["question"] = $this->_m_customTableHeaders["question"];
                 $this->_m_tableHeaders["response"] = $this->_m_customTableHeaders["response"];
                 $this->_m_tableHeaders["score"] = $this->_m_customTableHeaders["score"];

                 break;

         }

         // create a array containing the headers from db
         foreach ($tableColumns as $column) {

             $customHeaders = $this->_m_customTableHeaders[$column];

             if (!is_array($customHeaders)) {

                 $this->_m_tableHeaders[$column] = [$column];

             } else {

                 array_push($customHeaders, $column);
                 $this->_m_tableHeaders[$column] = $customHeaders;

             }

         }

     }

     public function getTableHeaders()
     {
         return $this->_m_tableHeaders;
     }

     public function getNrOfTableRows()
     {
         sizeof($this->_m_tableHeaders);
     }

     public function getRequiredFields()
     {
         return $this->_m_requiredFields;
     }

     public function setFieldRefs($fieldRefs)
     {
         $this->_m_fieldRefs = $fieldRefs;
     }

     public function getFieldRefs()
     {
         return $this->_m_fieldRefs;
     }

 }
