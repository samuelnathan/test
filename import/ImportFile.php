<?php
/* Original Author: Jan-Paul Eikelenboom
 * For Qpercom Ltd
 * Date: 16/01/2008
 * Updated: 29/07/2016
 * Version: 0.1
 */

class ImportFile
{
    // name of the file that is going to be imported
    private $m_fileName;
    // path of the file that is going to be imported
    private $m_filePath;
    // pointer to the file that is going to be imported
    private $m_filePointer;
    // pointer to the file format
    private $m_fileFormat;
    // type of data that is going to be imported
    private $m_dataType;
    // desc of data that is going to be imported
    private $m_descDataType;
    // The source of the data that is going to be imported
    private $m_delimiter;
    // The field qualifier in the data Source
    private $m_enclosure;
    // The current row that is going to be read
    private $m_currentRow;
    // The extextions that are allowed for the file
    private $m_allowedExt;
    private $m_rowCount;
    
    /**
     * Sometimes ASCII text can contain non-breaking spaces (ASCII 160). This
     * removes them, and replaces them with conventional spaces (ASCII 32). If
     * $trim_result is TRUE, excess whitespace at the start and end are removed.
     * 
     * @param mixed $data If $data is a string, then we parse it, otherwise let it through.
     * @param bool  $trim_result
     * @return mixed Whatever type $data was.
     */
    private static function replaceNbsp($data, $trim_result = true) {
        if (is_string($data)) {
            /* Might as well trim the leading and trailing whitespace while
             * we're at it.
             */
            $result = str_replace(chr(160), " ", $data);
            if ($trim_result) {
                return trim($result);
            } else {
                return $result;
            }
        } else {
            return $data;
        }
    }
    
    public function __construct($dataType, $descDataType, $format, $customDelimiter, $enclosure)
    {
        $this->m_allowedExt = ["txt", "csv", "dat", "xls", "xlsx"];
        $this->m_dataType = $dataType;
        $this->m_descDataType = $descDataType;
        $this->m_rowCount = 0;
        $this->m_fileFormat = $format;
        
        if ($format == "comma") {
            $this->m_delimiter = ",";
        } elseif ($format == "tab") {
            $this->m_delimiter = "\t";
        } elseif ($format == "excel") {
            $this->m_delimiter = "";
        } else {
            $this->m_delimiter = $customDelimiter;
        }
        $this->m_enclosure = $enclosure;
    }
    

    public function fileExists()
    {
        return !$this->m_filePointer ? false : true;
    }

    // @uses ImportFile::replaceNbsp()
    public function getNextRow()
    {
        if (!$this->fileExists() && !$this->createFilePointer()) {
            $this->m_currentRow = null;
            return $this->m_currentRow;
        }

        if ($this->m_enclosure == 'double') {
            $encl = '"';
        } elseif ($this->m_enclosure == 'single') {
            $encl = "'";
        } else {
            $encl = "";
        }

        if (strlen($encl) > 0) {
            $row = fgetcsv($this->m_filePointer, 8192, $this->m_delimiter, $encl);
        } else {
            $row = fgetcsv($this->m_filePointer, 8192, $this->m_delimiter);
        }
        
        if ($row == false && sizeof($row) == 1) {
            $this->m_currentRow = null;
            return $this->m_currentRow;
        }
        
        // Remove any non-breaking spaces from the text in any elements that contain text.
        $row = array_map("self::replaceNbsp", $row);
        
        $this->m_currentRow = $row;
        $this->m_rowCount++;
        return $this->m_currentRow;
    }
    
    public function closeFilePointer()
    {
        if ($this->fileExists()) {
            fclose($this->m_filePointer);
            return true;
        }
        return false;
    }

    public function getFirstDataRow()
    {
        if (null == $this->getNextRow()) {
            return $this->m_filePointer;
        }
        return $this->getNextRow();
    }

    public function getFileName()
    {
        return $this->m_fileName;
    }
    
    public function getFilePath()
    {
        return $this->m_filePath;
    }

    public function getFileFormat()
    {
        return $this->m_fileFormat;
    }    
    
    public function setFileName($fileName)
    {
        $this->m_fileName = $fileName;
    }

    private function createFilePointer()
    {
        if (strlen($this->m_filePath) == 0 || !file_exists($this->m_filePath)) {
            return false;
        }

        $pointer = fopen($this->m_filePath, "r");
        if (!$pointer) {
            return false;
        }
        $this->m_filePointer = $pointer;
        $this->m_rowCount = 0;
        return true;
    }

    public function setfilePath($filePath)
    {
        $this->deleteCurrentFile();
        $this->saveFile($filePath);
        $this->createFilePointer();
    }

    private function saveFile($filePath)
    {
        
        $saveFilename = preg_replace(
            ["/\s+/", "/[^-\.\w]+/"],
            ["_", ""],
            trim($this->m_fileName)
        );

        $targetPath = "storage/app/tmp/" . $saveFilename;

        if (move_uploaded_file($filePath, $targetPath)) {
            $this->m_filePath = $targetPath;
            return true;
        } else {
            return false;
        }
        
    }

    public function deleteCurrentFile()
    {
        if (file_exists($this->m_filePath) && unlink($this->m_filePath)) {
            $this->m_filePath = "";
            $this->m_fileName = "";
            return true;
        } else {
            return false;
        }
    }

    public function getRowCount()
    {
        return $this->m_rowCount;
    }

    public function getDataType()
    {
        return $this->m_dataType;
    }

    public function getDescDataType()
    {
        return $this->m_descDataType;
    }

    public function getNrOfFields()
    {
        return sizeof($this->m_currentRow);
    }

    public function getCurrentRow()
    {
        return $this->m_currentRow;
    }

    public function validateFile($fileName, $uploaded_type)
    {
        $default_mime_types = ["text/php", "application/octet-stream"];

        // Check for return types.
        if (in_array($uploaded_type, $default_mime_types)) {
            return false;
        }

        // Bail out if the file suffix isn't recognized.
        if (!in_array(strtolower(pathinfo($fileName, PATHINFO_EXTENSION)), $this->m_allowedExt)) {
            return false;
        }

        // If we get to here, then we recognize the file type. In theory.
        return true;
    }
}
