<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define("IMPORTEXPORT_DATATYPE_SCHOOL_DEPARTMENTS", 1);
 define("IMPORTEXPORT_DATATYPE_COURSES_MODULES", 2);
 define("IMPORTEXPORT_DATATYPE_STUDENTS", 3);
 define("IMPORTEXPORT_DATATYPE_EXAMINERS", 4);
 define("IMPORTEXPORT_DATATYPE_EXAM_MATRIX", 5);
 define("IMPORTEXPORT_DATATYPE_SCORESHEETS", 6);

 class ImportFileFields
 {

     private $_m_requiredFileFields;
     private $_m_dataType;

     public function __construct($dataType)
     {

           global $config;
           if (!isset($config))
             $config = \OMIS\Config::getInstance();

           $this->_m_dataType = $dataType;
           $this->_m_requiredFileFields = [
             "2" => ["COURSE_ID", "COURSE_NAME", "COURSE_YEAR", "MODULE_ID", "MODULE_NAME"],
             "3" => ["STUDENT_ID", "COURSE_ID", "COURSE_YEAR", "MODULE_ID"],
             "4" => ["USER_ID", 'DEPARTMENT_ID', "USER_ROLE"],
             "5" => ["SESSION", "DATE", "CIRCUIT_COLOUR", "GROUP", "START", "END", "STATION #"],
             "6" => ["APPLICANT_ID", "QUESTION", "RESPONSE", "SCORE"]
           ];

           // Apply localisation translations
           array_walk_recursive($this->_m_requiredFileFields, function(&$value) {
               $value = gettext($value);
           });

           // If reading password columns in the csv file
           if (isset($config->import["password_by_file"]) && $config->import['password_by_file'] == true)
               array_push($this->_m_requiredFileFields[4], "PASSWORD");

     }

     /**
      * Simple private function to wrap the name of a field in a suitable <span>
      *
      * @param string  $fieldname    Name of the field wrap.
      * @return string               Field name wrapped in <span> tag for the file
      *                              field list
      *
      */
     private static function _toSpan($fieldname)
     {
         return "<span class=\"red normal\">$fieldname</span>";
     }

     public function getInputFileTemplateDescription()
     {
         global $config;
         if (!isset($config)) {
             $config = \OMIS\Config::getInstance();
         }

         global $db;
         if (!isset($db)) {
             $db = \OMIS\Database\CoreDB::getInstance();
         }

         // Candidate number feature
         $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);

         switch ($this->_m_dataType) {
             case IMPORTEXPORT_DATATYPE_COURSES_MODULES:
                 $templateUrl = $this->getTemplateFile('courses_modules', 'courses_modules_import_template.csv', $config);
                 $warningMessage = "";

                 break;
             case IMPORTEXPORT_DATATYPE_STUDENTS:
                 $templateUrl = $this->getTemplateFile('students', 'student_import_template' . ($candidateNumberFeature ? '_candidate.csv' : '.csv'), $config);
                 $warningMessage = "";

                 break;
             case IMPORTEXPORT_DATATYPE_EXAMINERS:
                 $templateUrl = $this->getTemplateFile('exam_team', gettext('exam') . '_team_import_template.csv', $config);
                 $warningMessage = "Please make sure that the " . gettext('department') . " identifiers in your "
                         . "import file match the " . gettext('department') . " identifiers in the system";

                 break;
             case IMPORTEXPORT_DATATYPE_EXAM_MATRIX:

                 $templateUrl = $this->getTemplateFile('matrix', 'exam_matrix_import_template.xlsx', $config);
                 $warningMessage = "Please note that the import file will overwrite any day/circuits and "
                         . gettext('student') . " groups within the choosen " . gettext('exams') . " shell";

                 break;
             case IMPORTEXPORT_DATATYPE_SCORESHEETS:
                 $templateUrl = $this->getTemplateFile('self_assessment', 'self_assessment_import_template.csv', $config);
                 $warningMessage = "Please make sure to include all self assessment scoresheets for your " . gettext('department') . " "
                     . gettext('exams') . " in a single import file";
                 break;
             default:
                 // Shouldn't get here...
                 $templateUrl = "javascript:void(0);";
         }

         $fields = $this->_m_requiredFileFields[$this->_m_dataType];

         return [
             "templateUrl"       => $templateUrl,
             "requiredFields"    => array_map("ImportFileFields::_toSpan", $fields),
             "warningMessage"    => $warningMessage
         ];
     }

     /**
      * Get a template file path from the configuration
      *
      * @param $templateKey string Key of the template in the configuration
      * @param $defaultName string Default filename if the file is not in the configuration
      * @param \OMIS\Config $config Instance of the configuration
      * @return string
      */
     private function getTemplateFile($templateKey, $defaultName, \OMIS\Config $config) {
         if (isset($config->import_files[$templateKey]) && !empty($config->import_files[$templateKey])) {
             return $config->import_files[$templateKey];
         }

         return './files/' . $defaultName;
     }
 }

 ?>
