<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 // Next step
 if (isset($_REQUEST['nextStep'])) {

     $importFile = new ImportFile(
         iS($_REQUEST, 'dataType'),
         iS($_REQUEST, 'descDataType'),
         iS($_REQUEST, 'format'),
         iS($_REQUEST, 'customDelimiter'),
         iS($_REQUEST, 'fieldQualifie_sel')
     );

     $_SESSION['importFile'] = serialize($importFile);

 } else {

     $importFile = unserialize($_SESSION['importFile']);

 }

 $dataLabel = $importFile->getDescDataType();
 $dataLabelOutput = empty($dataLabel) ? "Data" : $importFile->getDescDataType();
 $terms = $db->academicterms->getAllTerms();

 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render([
     'dataLabel' => $dataLabelOutput,
     'dataType'  => $importFile->getDataType(),
     'terms' => $terms,
     'selected_term' => $_SESSION['cterm'],
     'pubbleLink' => $config->support['pubble']['enabled']
 ]);
