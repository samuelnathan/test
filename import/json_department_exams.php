<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// JSON Helper functions.
use \OMIS\Utilities\JSON as JSON;
use \OMIS\Database\CoreDB as CoreDB;

define('_OMIS', 1);

// Return data
$returnData = [];

// Check if user is authorized to make this call
$department = filter_input(INPUT_POST, 'department', FILTER_SANITIZE_STRING);
if (!empty($department)) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

// json_encode takes care of Html Entities
$db->set_Convert_Html_Entities(false); 

// Does department exist ?
if (!$db->departments->existsById($department)) {
    echo JSON::encode([]);
    exit();
}

// Get exams
 $exams = CoreDB::group(
             CoreDB::into_array(
      $db->exams->getList(
         [$department], $_SESSION['cterm']
      )
 ), ['exam_id'], true);

// Return exam records
foreach ($exams as $examID => $exam) {

    if (!$db->results->doesExamHaveResults($examID)) {
        $returnData[] = [
            'id' => $examID,
            'name' => $exam[0]['exam_name']
        ];
    }
    
}

try {
    $json = JSON::encode($returnData);
} catch (Exception $exception) {
    $json = "";
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnData, true));
    exit();
}

echo $json;
