<?php
/* ajax page for data import step 1
 * Original Author: Jan-Paul Eikelenboom
 * For Qpercom Ltd 2016
 * Created: 07/08/2009
 * Update: 28/07/2016 
 */

use \OMIS\Template as Template;
define('_OMIS', 1);

// Check if user is authorized to make this call
$source = filter_input(INPUT_POST, 'source', FILTER_SANITIZE_STRING);
if (!empty($source)) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render([
    'source' => trim($source)
]);
