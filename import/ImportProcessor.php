<?php
/**
 * @author Jan-Paul Eikelenboom
 * @Updated: David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * @Import Processing for students, examiners, courses etc
 */


/* This is to get the constants for the various import types - Student, 
 * Examiner, etc.
 */
require_once "ImportFileFields.php";

class ImportProcessor
{
    private $studentsInsert = [];
    private $studentsUpdate = [];
    private $studentCoursesInsert = [];
    private $modulesUpdate = [];
    private $modulesInsert = [];
    private $coursesUpdate = [];
    private $coursesInsert = [];
    private $yearsInsert = [];
    private $moduleYearsInsert = [];
    private $candidateNumbersInsert = [];
    
    private $coursesDB = [];
    private $yearsDB = [];
    private $modulesDB = [];
    private $studentDB = [];
    private $studentCoursesDB = [];
    private $moduleYearsDB = [];
    
    // Skipped Rows
    private $skippedRows = [];

    // Database link
    private static $db;

    // Import map containing the mapping of the file to the db fields
    private $importMap;
    
    // Import class containing file and file read methods
    private $importFile;

    // Constructor
    public function __construct($importMap, $importFile, $fileEncoding, $db = null)
    {
        
        if (is_null($db)) {
            $this::$db = \OMIS\Database\CoreDB::getInstance();
        } else {
            $this::$db = $db;
        }

        // Change 'From' Excel Charset
        $this::$db->set_Charset_From($fileEncoding);
        $this->importMap = $importMap;
        $this->importFile = $importFile;
    }

    /**
     * Get skipped file rows
     * 
     */
    public function getSkippedRows()
    {
        return $this->skippedRows;
    }

    /**
     * Reads the delimited data file, and based on the type figures out which
     * type of data it might be modifying, and then checks what data there 
     * already is so that it can determine which records need to be changed and
     * which are new.
     * 
     * @return mixed Results of whichever import function is called (depends on data type)
     */
    public function importDataFile()
    {
        /* Depending on which type of data we're importing, get the current
         * information in the database from all relevant tables.
         */
        switch ($this->importFile->getDataType()) {
            case IMPORTEXPORT_DATATYPE_COURSES_MODULES:
                $this->getCoursesDB();
                $this->getYearsDB();
                $this->getModulesDB();
                $this->getModuleYearsDB();
                break;
            case IMPORTEXPORT_DATATYPE_STUDENTS:
                $this->getCoursesDB();
                $this->getYearsDB();
                $this->getModuleYearsDB();
                $this->getModulesDB();
                $this->getStudentsDB();
                $this->getStudentCoursesDB();
                break;
        }

        /* Go through the data file row by row, deciding what needs updating 
         * and what needs inserting.
         */
        $currentRow = $this->importFile->getFirstDataRow();
        while ($currentRow != NULL) {
            $this->processFileRow($currentRow, $this->importMap->getFieldRefs());
            $currentRow = $this->importFile->getNextRow();
        }

        // Close the file; we're done with it.
        $this->importFile->closeFilePointer();

        /* Invoke the necessary import function depending on the type of data
         * being imported.
         */
        switch ($this->importFile->getDataType()) {
            case IMPORTEXPORT_DATATYPE_COURSES_MODULES:
                return $this::$db->courses->importCourseModuleData($this->coursesInsert,
                                                                   $this->coursesUpdate,
                                                                   $this->yearsInsert,
                                                                   $this->modulesInsert,
                                                                   $this->modulesUpdate,
                                                                   $this->moduleYearsInsert);
            case IMPORTEXPORT_DATATYPE_STUDENTS:
                return $this::$db->students->importStudentData($this->studentsInsert,
                                                               $this->studentsUpdate,
                                                               $this->studentCoursesInsert,
                                                               $this->candidateNumbersInsert);

        }
    }

    /**
     * Process import file row
     * @param array $currentRow
     * @param array $fieldrefs
     */
    private function processFileRow($currentRow, $fieldrefs)
    {
       
      // Identifier characters allowed
      $idCharactersAllowedText = "Valid characters are 0-9|a-z|-|_";  

      // If any of the required fields are not empty
      if ($this->requiredFieldsNotEmpty($currentRow, $fieldrefs)) {
                
        /**
         * Import Courses, Years and Modules
         */
        switch ($this->importFile->getDataType()) {
           
          case IMPORTEXPORT_DATATYPE_COURSES_MODULES:    
          
           // Get the required fields
           $courseID = trim($currentRow[$fieldrefs["course_id"]]);
           $moduleID = trim($currentRow[$fieldrefs["module_id"]]);
           $year = trim($currentRow[$fieldrefs["year_name"]]);
           
            // Validate course ID
            if (!identifierValid($courseID)) {
                array_push($this->skippedRows,
                    [$this->importFile->getRowCount(),
                    "Course ID in file found to be invalid. $idCharactersAllowedText"]);          
                break;
            }
            
            // Validate module ID
            if (!identifierValid($moduleID)) {
                array_push($this->skippedRows,
                    [$this->importFile->getRowCount(),
                    "Module ID in file found to be invalid. $idCharactersAllowedText"]);          
                break;
            }          
           
           $courseYearKey = $courseID . "_" . $year;
           $newModule = false;
           $updateModule = false;
                
           // Module Data (Name)
           $includeName = isset($fieldrefs["module_name"]);
           $moduleName = "undefined";
                
           // Set module name
           if ($includeName && isset($currentRow[$fieldrefs["module_name"]])) {
              $moduleName = trim($currentRow[$fieldrefs["module_name"]]);
           }
                               
           // Module data array
           $moduleData = [$moduleName];
                             
           // Course Data (Name)
           $includeCrsName = isset($fieldrefs["course_name"]);
           $courseName = "undefined";
                
           // Set course name
           if ($includeCrsName && isset($currentRow[$fieldrefs["course_name"]])) {
              $courseName = trim($currentRow[$fieldrefs["course_name"]]);
           }
                                        
           // Course data array
           $courseData = [$courseName];
                
           /**
            * Check if course is not yet in database or inserted by previous rows than insert, 
            * else if it's not yet updated by other rows than update course
            */
           $courseInsertExists = array_key_exists($courseID, $this->coursesInsert);
           if (!in_array($courseID, $this->coursesDB, true) && !$courseInsertExists) {
                    
             // Add course for insert list
             $this->coursesInsert[$courseID] = $courseData;
                    
            } else if (!array_key_exists($courseID, $this->coursesUpdate) && !$courseInsertExists) {
                    
             // Add course for insert list
             $this->coursesUpdate[$courseID] = $courseData;
                    
            }

            // Insert new course years
            if (!array_key_exists($courseYearKey, $this->yearsDB)
                && !array_key_exists($courseYearKey, $this->yearsInsert)) {
                    
                // Add course year to insert list
                $this->yearsInsert[$courseYearKey] = [$courseID, $year];
                    
            }

            // Insert new modules
            if (!in_array($moduleID, $this->modulesDB, true)) {
               if (!array_key_exists($moduleID, $this->modulesInsert)) {
                        
                 // Add module to insert list
                 $newModule = true;
                 $this->modulesInsert[$moduleID] = $moduleData;
                        
               }

            // Update existing modules in the database
            } else if (!array_key_exists($moduleID, $this->modulesUpdate)) {

                 // Add module to update list
                 $updateModule = true;
                 $this->modulesUpdate[$moduleID] = $moduleData;
                    
            }

            // Attach module to course year if it's a new module
            if ($newModule 
                && (!array_key_exists($moduleID, $this->moduleYearsInsert) 
                || !in_array($courseYearKey, $this->moduleYearsInsert[$moduleID], true))) { 
                    
                // Link course year with module
                $this->moduleYearsInsert[$moduleID] = $courseYearKey;
                    
            // If it's a module to update
            } else if ($updateModule
                && (!array_key_exists($moduleID, $this->moduleYearsDB) 
                || !array_key_exists($courseYearKey, $this->moduleYearsDB[$moduleID]))
                && (!array_key_exists($moduleID, $this->moduleYearsInsert)
                || !in_array($courseYearKey, $this->moduleYearsInsert[$moduleID], true))) {
 
                  // Link course year with module
                  $this->moduleYearsInsert[$moduleID] = $courseYearKey;
                    
            }
            break;
            
         /**
          * Import students
          */
         case IMPORTEXPORT_DATATYPE_STUDENTS:

            // Grab key fields
            $studentID = trim($currentRow[$fieldrefs['student_id']]);
            $courseID = trim($currentRow[$fieldrefs["course_id"]]);
            $moduleID = trim($currentRow[$fieldrefs["module_id"]]);
            $year = trim($currentRow[$fieldrefs["year_name"]]);
            
            // Validate student ID
            if (!identifierValid($studentID)) {
                array_push($this->skippedRows,
                    [$this->importFile->getRowCount(),
                    gettext('Student') . " ID in file found to be invalid. $idCharactersAllowedText"]);
                break;
            }
            
            // Validate course ID
            if (!identifierValid($courseID)) {
                array_push($this->skippedRows,
                    [$this->importFile->getRowCount(),
                    gettext('Course') ." ID in file found to be invalid. $idCharactersAllowedText"]);          
                break;
            }
            
            // Validate module ID
            if (!identifierValid($moduleID)) {
                array_push($this->skippedRows,
                    [$this->importFile->getRowCount(),
                    gettext('Module') . " ID in file found to be invalid. $idCharactersAllowedText"]);          
                break;
            } 
            
            // Check for an existing course and year combination
            if (!array_key_exists($courseID . "_" . $year, $this->yearsDB)) {

                array_push(
                   $this->skippedRows,
                   [
                      $this->importFile->getRowCount(),
                      gettext('Course') . " and " . gettext('Course Year') . " combination in file does not match\nany " .
                      gettext('Course') . " and " . gettext('Course Year') . " combination in system, please review"
                   ]
                );

                break;

            }
            
            // Check and existing module and course year combination
            if (!array_key_exists($moduleID, $this->moduleYearsDB) || 
                 !array_key_exists($courseID . "_" . $year, $this->moduleYearsDB[$moduleID])) {

                    array_push(
                       $this->skippedRows,
                       [
                         $this->importFile->getRowCount(),
                         gettext('Course Year') . " and " . gettext('Module') . " combination in file does not match\nany " .
                         gettext('Course Year') . " and " . gettext('Module') . " combination in system, please review"
                       ]
                    );
                
                break;
             
            }

            // Get course year ID
            $yearID = $this->moduleYearsDB[$moduleID][$courseID . "_" . $year];
        
            // Fields to include
            $includeCandidateNumber = isset($fieldrefs["candidate_number"]); 
            $includeForenames = isset($fieldrefs["forename"]);
            $includeSurname = isset($fieldrefs["surname"]);
            $includeDob = isset($fieldrefs["dob"]);
            $includeGender = isset($fieldrefs["gender"]);
            $includeNationality = isset($fieldrefs["nationality"]);
            $includeEmail = isset($fieldrefs["email"]);
    
            // Validate candidate number
            if ($includeCandidateNumber) {
            $candidateNumber = trim($currentRow[$fieldrefs["candidate_number"]]);
                if (strlen($candidateNumber) > 0 && !identifierValid($candidateNumber)) {
                        array_push($this->skippedRows,
                            [$this->importFile->getRowCount(),
                            "Candidate Number in file found to be invalid. $idCharactersAllowedText"]);
                        break;
                } elseif (!in_array($studentID, $this->candidateNumbersInsert, true)) {
                    $this->candidateNumbersInsert[$studentID] = $candidateNumber;
                }
            }
            
            // Fornames and surname
            $forenames = $includeForenames ? trim($currentRow[$fieldrefs["forename"]]) : "";
            $surname = $includeSurname ? trim($currentRow[$fieldrefs["surname"]]) : "";
                
            // Date of Birth
            $dob = $includeDob ? $this->formatDOB($currentRow[$fieldrefs["dob"]]) : "";
                
            // Gender
            $gender = $includeGender ? trim($currentRow[$fieldrefs["gender"]]) : "";
                
            // Nationality
            $nationality = $includeNationality ? $currentRow[$fieldrefs["nationality"]] : "";
                
            // Email plus validation
            if ($includeEmail) {
                $email = mb_strtolower(trim($currentRow[$fieldrefs["email"]]));
                if (strlen($email) > 0 && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                array_push(
                    $this->skippedRows,
                    [$this->importFile->getRowCount(), "Email address is invalid"]);          
                break;
                }
            } else {
            $email = "";
            }
                
            // Put data into an array
            $studentData = [
                $studentID,
                $forenames,
                $surname,
                $dob,
                $gender,
                $nationality,
                $email
            ];

            // Students for Insert and Update
            $studentIDlc = strtolower($studentID);
            if (!in_array($studentIDlc, $this->studentDB, true)) {
                if (!array_key_exists($studentIDlc, $this->studentsInsert)) {
                        
                // Add student to insert list
                $this->studentsInsert[$studentIDlc] = $studentData;
                        
                }
            } else if (!in_array($studentIDlc, $this->studentsUpdate, true)) {
                    
                // Add student to update list
                $this->studentsUpdate[$studentIDlc] = $studentData;
                    
            }

            // Process Student and their course information
            $studentCourseKey = $studentIDlc . $yearID . $moduleID;
            $yearModuleKey = $yearID . "_" . $moduleID;
            
            if ((!array_key_exists($studentIDlc, $this->studentCoursesDB) 
                || !in_array($yearModuleKey, $this->studentCoursesDB[$studentIDlc], true))
                && !array_key_exists($studentCourseKey, $this->studentCoursesInsert)) {
                    
              // Add new just student, year and module relationship to database
              $this->studentCoursesInsert[$studentCourseKey] = [
                    $studentID,
                    $yearID,
                    $moduleID
              ];

            }
         
            break;
            
        }
        
       } else {
          array_push($this->skippedRows, 
                     [$this->importFile->getRowCount(),
                     "A required field is empty"]);
       }
    }


    /**
     * Check to see if the required fields are not empty in a row of delimited
     * data
     * 
     * @param Array $currentRow     Row returned from fgetcsv()
     * @param Array $fieldrefs      List of fields used in the import file.
     * @return boolean
     */
    private function requiredFieldsNotEmpty($currentRow, $fieldrefs)
    {
        foreach ($this->importMap->getRequiredFields() as $reqField) {
            if (strlen($currentRow[$fieldrefs[$reqField]]) == 0) {
                return false;
            }
        }
        return true;
    }
    
    /** 
     * Get Course Years
     */
    private function getYearsDB()
    {
        $result = $this::$db->courses->getCourseYears(null, $_SESSION["cterm"], false);
        while ($courseYear = $this::$db->fetch_row($result)) {
            $this->yearsDB[$courseYear["course_id"] . "_" . $courseYear["year_name"]] = $courseYear["year_id"];
        }
    }
   
    /** 
     * Get the Courses that are in the database 
     */
    private function getCoursesDB()
    {
        $results = $this::$db->courses->getAllCourses();

        foreach ($results as $course) {
            array_push($this->coursesDB, $course["course_id"]);
        }

    }

    /** 
     * Get the Modules that are in the system
     */
    private function getModulesDB()
    {
        $results = $this::$db->courses->getAllModules();
        while ($mod_row = $this::$db->fetch_row($results)) {
            array_push($this->modulesDB, $mod_row["module_id"]);
        }
    }
        
    /** 
     * Get the Students that are in the database
     */
    private function getStudentsDB()
    {
        $results = $this::$db->students->getAllStudents();
        while ($data = $this::$db->fetch_row($results)) {
            array_push($this->studentDB, strtolower($data["student_id"]));
        }
    }

    /** 
     * Get student course data from the database
     */
    private function getStudentCoursesDB()
    {
        $records = $this::$db->students->getAllStudentCourseData($_SESSION["cterm"]);
        
        // Form student, course year and module relationship 
        while ($record = $this::$db->fetch_row($records)) {
            $studentID = strtolower($record["student_id"]);
            $yearID = $record["year_id"];
            $moduleID = $record["module_id"];
            $this->studentCoursesDB[$studentID][] = $yearID . "_" . $moduleID;
        }
       
    }

    /** 
     * Get module years from the database
     */
    private function getModuleYearsDB()
    {
        
        $result = $this::$db->courses->getModulesYears($_SESSION["cterm"]);
        while ($each = $this::$db->fetch_row($result)) {
         
            $this->moduleYearsDB[$each["module_id"]][$each["course_id"] . "_" . $each["year_name"]]
            = $each["year_id"];
        
        }
    
    }

    /**
     * Attempts to convert an arbitrarily formatted date string and convert it
     * into something that looks like an ISO8601 style date (i.e. YYYY-MM-DD)
     * 
     * @param string $dateString Date encoded as a string
     * @return string $dateString in YYYY-MM-DD format or an empty string on failure.
     */
    private function formatDOB($dateString)
    {
        $dateString = trim($dateString);
        
        /**
         * No point in continuing if we have an empty date string
         */
        if (empty($dateString)) {
           return "";
        }
        
        #format needed  '0000-00-00' check 2 formats found so far

        /* The DateTime constructor doesn't natively understand the DD/MM/YYYY
         * date format (but it does understand "MM/DD/YYYY" just fine), so we
         * need to handle these specially.
         */
        if (preg_match('`^(\d{1,2})/(\d{1,2})/(\d{4})$`', $dateString, $matches)) {
            list($day, $month, $year) = array_map("intval", $matches);
            // Assume it's DD/MM/YYYY unless it's provably otherwise.
            if ($month <= 12) {
                $dateOfBirth = DateTime::createFromFormat("d/m/Y", $dateString);
            }
            
            if (isset($dateOfBirth) && empty($dateOfBirth)) {
                /* DateTime::createFromFormat() returns FALSE if it can't parse
                 * the string passed in, so we need to log this and return an empty string.
                 */
                error_log(__METHOD__ . ": Don't know how to cast '$dateString' as a date");
                return "";
            }
        }

        /* If we haven't already got a DateTime object, then the special case
         * hasn't been applied so we just try to cast the string to a DateTime
         * and see what happens.
         */
        if (!isset($dateOfBirth)) {
            try {
                $dateOfBirth = new DateTime($dateString);
            } catch (Exception $ex) {
                /* DateTime didn't understand the format. Log this for future
                 * reference in case we need other "special cases" and return
                 * an empty date string.
                 */
                error_log(__METHOD__ . ": Don't know how to cast '$dateString' as a date");
                return "";
            }
        }

        return $dateOfBirth->format("Y-m-d");
    }
    
}
