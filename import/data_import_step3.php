<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 define('IMPORT_EXAM_MATRIX', 5);
 define('RETURN_DEPTS_AS_ARRAY', true);

 use \OMIS\Auth\Role as Role;
 use \OMIS\Template as Template;

 /* Define Byte Order Mark (BOM) strings for UCS2 encoded files, for example
  * Excel "Unicode Text" files.
  */
 define('BOM_UCS2LE', pack("CC", 0xFF, 0xFE));
 define('BOM_UCS2BE', pack("CC", 0xFE, 0xFF));

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 // Validate import term
 $importTerm = filter_input(INPUT_POST, 'import_term', FILTER_SANITIZE_STRING);
 if (!is_null($importTerm) && $db->academicterms->existsById($importTerm)) {

     $_SESSION['cterm'] = $importTerm;

 }

 // Save term prefix for next time
 $db->userPresets->update(['filter_term' => $_SESSION['cterm']]);

 $importFile = unserialize($_SESSION['importFile']);
 /* DW 2013-09-13: According to http://en.wikipedia.org/wiki/ISO/IEC_8859-12
  * ISO-8859-12 doesn't actually exist, and iconv() was complaining about it
  * being invalid, so I removed it from $encoding_list below.
  */
 $encodingList = [
     'Windows-1252',
     'Windows-1251',
     'ISO-8859-1',
     'ISO-8859-2',
     'ISO-8859-3',
     'ISO-8859-4',
     'ISO-8859-5',
     'ISO-8859-6',
     'ISO-8859-7',
     'ISO-8859-8',
     'ISO-8859-9',
     'ISO-8859-10',
     'ISO-8859-13',
     'ISO-8859-14',
     'ISO-8859-15',
     'BIG-5',
     'EUC-CN',
     'EUC-JP',
     'EUC-KR',
     'EUC-TW',
     'ISO-2022-JP',
     'ISO-2022-KR',
     'KOI8-R',
     'KOI8-U',
     'SJIS',
     'UTF-8',
     'UCS-2LE',
     'UCS-2BE'
 ];

 if ($_FILES['data_file']['error'] > 0 || !$importFile->validateFile($_FILES['data_file']['name'], $_FILES['data_file']['type'])) {

     (new Template('data_import_step3.error.html.twig'))->render();
     exit();

 /**
  * Import exam matrix (.xls or .xlsx)
  * Use PHPExcel to read the excel file
  */
 } else if ($importFile->getDataType() == IMPORT_EXAM_MATRIX && $importFile->getFileFormat() == "excel") {

     // Import file
     $importFile->setFileName($_FILES['data_file']['name']);
     $importFile->setFilePath($_FILES['data_file']['tmp_name']);
     $importFileLocation = $importFile->getFilePath();

     try {
        $inputFileType = PHPExcel_IOFactory::identify($importFileLocation);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($importFileLocation);
     } catch(Exception $exception) {
        error_log(__FILE__ .' Error loading import file "'.pathinfo($importFileLocation, PATHINFO_BASENAME).'": '.$exception->getMessage());
        (new Template('data_import_step3.error.html.twig'))->render();
        exit();
     }

     /**
      *  Check for valid worksheet
      */
     $sheet = $objPHPExcel->getSheet(0);
     $numOfRows = $sheet->getHighestDataRow();

     $expectedColumnTitles = [
         'A1' => 'session',
         'B1' => 'date',
         'C1' => 'circuit colour',
         'D1' => 'group',
         'E1' => 'start',
         'F1' => 'end'
     ];

     /**
      * Validate column titles
      */
     $invalidTitles = false;
     foreach($expectedColumnTitles as $cellRef => $columnTitle) {
         $cellValue = $sheet->getCell($cellRef)->getValue();
         $titleValue = preg_replace('/\s+/', '', strtolower($cellValue));

         if ($titleValue != preg_replace('/\s+/', '', $columnTitle)) {
             error_log(__FILE__ . " Invalid title: " . $cellValue);
             $invalidTitles = true;
             break;
         }
     }

     // At least 1 station column title
     $firstStationTitle = strtolower($sheet->getCell('G1')->getValue());

     if ($invalidTitles || $firstStationTitle != 'station 1' || $numOfRows <= 2) {
          error_log(__FILE__ .' Invalid import file: "'.pathinfo($importFileLocation, PATHINFO_BASENAME));
          (new Template('data_import_step3.invalid.html.twig'))->render();
          exit();
     }

     (new Template('data_import_step3.exam.matrix.html.twig'))->render([
         'departments' => $db->departments->getAllDepartments(RETURN_DEPTS_AS_ARRAY)
     ]);

 } else {

     // Attempt to detect the text encoding on this CSV file.
     $fileData = file_get_contents($_FILES['data_file']['tmp_name']);

     /* If someone exports Excel "Unicode Text", it'll be UCS2-LE, so let's check
      * for a BOM (Byte Order Mark) to accurately detect either little-endian
      * or big-endian UCS2/UTF-16.
      */


     /* (DW) This is a special-case hack for right single quotes in files
      * generated with Microsoft tools. It's possible there's a better
      * solution to this but PHP's file encoding detection is rubbish.
      * Still need a good way of detecting MacRoman...
      */
     if (substr($fileData, 0, 2) == BOM_UCS2LE) {
         // UCS2/UTF-16, Little Endian
         $detectedEncoding = 'UCS-2LE';
     } elseif (substr($fileData, 0, 2) == BOM_UCS2BE) {
         // UCS2/UTF-16, Big Endian
         $detectedEncoding = 'UCS-2BE';
     } elseif (strpos($fileData, "\x92") !== \FALSE) {
         $detectedEncoding = "Windows-1252";
     } else {
         // In the absence of concrete data, ask mb_detect_encoding() to have a go.
         try {
             $detectedEncoding = mb_detect_encoding($fileData, $encodingList, \TRUE);
         } catch (ErrorException $ex) {
             die($ex->getMessage());
         }
     }

     // If our data doesn't seem to be UTF-8, let's see if we can convert it that.
     if ($detectedEncoding !== 'UTF-8') {
         $fileDataUtf8 = @iconv($detectedEncoding, 'UTF-8', $fileData);

         /* If iconv didn't explode, then overwrite the source file with the
          * re-encoded content.
          */
         if ($fileDataUtf8 !== \FALSE) {
             file_put_contents($_FILES['data_file']['tmp_name'], $fileDataUtf8);
         }
     }

     // Import file
     $importFile->setFileName($_FILES['data_file']['name']);
     $importFile->setFilePath($_FILES['data_file']['tmp_name']);

     // Import mapping (csv files only)
     $importMapping = new ImportMapping($importFile->getDataType(), $db);
     $reqFields;

     // Get the header and first data row so that we can check if it is empty
     /* (DW) Need to go to this rigmarole because the first column might have a BOM
      * that we need to remove. By now, we _should_ have UTF-8, and all our DB column
      * names should be ASCII, so this conversion _should_ be safe.
      */
     $fileFieldNames = array_map(
         function ($value) {
             return trim(@iconv('UTF-8', 'ASCII//IGNORE', $value));
         },
         $importFile->getNextRow()
     );
     $firstData_row = $importFile->getNextRow();

     $currentRow = $importFile->getCurrentRow();
     if (empty($currentRow)) {
         (new Template('data_import_step3.error.html.twig'))->render();
         exit();
     }

     $tableMapping = $importMapping->getTableHeaders();
     $suggestedMapping = [];

     /* Iterate through the provided column names and attempt to map the names in the
      * file to the database columns we have.
      */
     foreach ($fileFieldNames as $fileFieldName) {
         foreach (array_keys($tableMapping) as $dbColumn) {
             // Check if the column name matches a database column exactly.
             if (strcasecmp($dbColumn, $fileFieldName) == 0) {
                 $suggestedMapping[$fileFieldName] = $dbColumn;
                 // Skip to the next column title.
                 continue 2;
             }

             // See if the column name matches an alternative name.
             foreach ($tableMapping[$dbColumn] as $alternativeName) {
                 if (strcasecmp($alternativeName, $fileFieldName) == 0) {
                     $suggestedMapping[$fileFieldName] = $dbColumn;

                     // Skip to the next column title.
                     continue 3;
                 }
             }
         }
     }

     $dataLabel = "Data";
     if (strcmp($importFile->getDescDataType(), '') != 0) {
         $dataLabel = $importFile->getDescDataType();
     }

     // Update session with latest verison of the the import map object
     $_SESSION['importMap'] = serialize($importMapping);

     // Apply translations to required fields
     $requiredFields = $importMapping->getRequiredFields();
     $requiredFieldsTranslated = array_map(function ($field) {
       return gettext($field);
     }, $requiredFields);

     // Get department data by role
     if (in_array($_SESSION["user_role"], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {

         $deptRecords = $db->departments->getAllDepartments(RETURN_DEPTS_AS_ARRAY);

     } else if ($_SESSION["user_role"] == Role::USER_ROLE_SCHOOL_ADMIN) {

         $adminSchools = $db->schools->getAdminSchools($_SESSION["user_identifier"]);
         $schoolIDs = array_unique(array_column($adminSchools, 'school_id'));
         $deptRecords = $db->departments->getDepartments(null, RETURN_DEPTS_AS_ARRAY, $schoolIDs);

     } else {

         $deptRecords = $db->departments->getExaminerDepartments(
             trim($_SESSION["user_identifier"]),
             null,
             RETURN_DEPTS_AS_ARRAY
         );

     }

     $groupedDepartments = [];

     foreach ($deptRecords as $dept) {
         if (array_key_exists($dept['school_description'], $groupedDepartments))
             $groupedDepartments[$dept['school_description']][] = $dept;
         else
             $groupedDepartments[$dept['school_description']] = [$dept];
     }

     /**
      * Additional parameters to send in the form to step4
      */
     $additionalParameters = null;

     if ($importFile->getDataType() == IMPORTEXPORT_DATATYPE_SCORESHEETS) {

         // Get exams
         $exams = \OMIS\Database\CoreDB::into_array(
             $db->exams->getList(
                 [$_SESSION['selected_dept']],
                 $_SESSION['cterm']
             )
         );

         $additionalParameters = [
             'template' => 'data_import_step3.self.assessment.html.twig',
             'data' => [
                 'schools' => $groupedDepartments,
                 'exams' => $exams,
                 'currentDept' => $_SESSION['selected_dept']
             ]
         ];

     // Only show if we require a default password
     } else if ($importFile->getDataType() == IMPORTEXPORT_DATATYPE_EXAMINERS) {

       if (!isset($config->import['password_by_file']) || !$config->import['password_by_file']) {

           $additionalParameters = [
               'template' => 'data_import_step3.examiners.html.twig',
               'data' => []
           ];

       }

     }

     $template = new Template(Template::findMatchingTemplate(__FILE__));
     $template->render([
         'dataLabel'         => $dataLabel,
         'requiredFields'    => $requiredFields,
         'requiredFieldsTranslated'   => $requiredFieldsTranslated,
         'fileFields'        => $fileFieldNames,
         'dbFields'          => array_keys($tableMapping),
         'tableMapping'      => $suggestedMapping,
         'sampleData'        => $firstData_row,
         'fileEncodings'     => $encodingList,
         'detectedEncoding'  => $detectedEncoding,
         'additionalParameters' => $additionalParameters,
         'pubbleLink' => $config->support['pubble']['enabled'],
         'selectedTerm' => $_SESSION['cterm']
     ]);

     /* Password strength settings for the password fields
     * Settings for the examiners import password field
     * Plugin: pwstrength bootstrap Jquery
     */
     if ($importFile->getDataType() == IMPORTEXPORT_DATATYPE_EXAMINERS && isset($config->passwords)) {
     ?>
         <script>
         // pass PHP variable declared above to JavaScript variable
         var verdictList = <?=json_encode(['weak', 'normal', 'medium', 'strong', 'very strong'])?>;
         var passwordSettings = <?=json_encode($config->passwords)?>;
         </script>
     <?php
     }

 }

 /**
  * Update session with the latest version of the import file object
  */
 $_SESSION['importFile'] = serialize($importFile);
