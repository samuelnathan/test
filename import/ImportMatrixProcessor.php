<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

use \Carbon\Carbon;

class ImportMatrixProcessor
{
    
    // All the constants we need
    const RETURN_DATA_AS_ARRAY = true;
    const SESSION_UNPUBLISHED = 0;
   
    // Database link
    private static $db;
    
    // Other private variables
    private $importExam;
    private $importFile;
    private $PHPExcel;
    private $sheet;
    private $defaultDate;
    private $requiredColumns = [];
    private $colours = [];
    private $colourData = [];
    private $matrix = [];
    private $groupCount = 0;
    private $studentCount = 0;    
    private $numberOfRows = 0;
    private $numberOfCols = 0;
    private $failedStudents = [];
    private $uniqueStudents = [];
    private $duplicateStudents = [];

    // Constructor
    public function __construct($importExam, $importFile, $fileEncoding, $db = null)
    {
        
        if (is_null($db)) {
            $this::$db = \OMIS\Database\CoreDB::getInstance();
        } else {
            $this::$db = $db;
        }
        
        // Change 'From' Excel Charset
        $this::$db->set_Charset_From($fileEncoding);
        $this->importFile = $importFile;
        $this->importExam = $importExam;
        
        // Populate the required fields
        $this->requiredColumns = [
             'A1' => 'session',
             'B1' => 'date',
             'C1' => 'circuit colour',
             'D1' => 'group',
             'E1' => 'start',
             'F1' => 'end'     
        ];
        
        // Circuit colour database records
        $this->colourData = 
              array_change_key_case(
                $this->db()->exams->getAllCircuitColours('colour_name'),
                CASE_LOWER
        );
        
        // Get all circuit colour names
        $this->colours = array_keys($this->colourData);
        
        // Set default date as current dat
        $this->defaultDate = date('Y-m-d');
        
    }

    /**
     * Imports the .xls matrix file
     * @return bool status
     */
    public function importDataFile()
    {

        // Return data template
        $returnData =  [
           'complete' => false,
           'exam_name' => '',
           'insertedSessions' => 0,
           'insertedGroups' => 0,
           'insertedStudents' => 0,
           'failedStudents' => [],
           'duplicateStudents' => []
        ];
        
        // Load Worksheet (objectPHPExcel and sheet)
        $workSheet = $this->loadWorksheet();
        if (!$workSheet) {
           return ['errorMsg' => "Error loading import file"]; 
        }
        
        // Invalid PHPExcel sheet, then abort and notify user
        if (!$this->validateSheet($this->sheet)) {
           $errorMsg = "Invalid import file, please use the template provided in step 1 as a guide";
           return ['errorMsg' => $errorMsg];  
        }
        
        // Does exam exist
        if (!$this->db()->exams->doesExamExist($this->importExam)) {
           return ['errorMsg' => "Chosen exam does not exist"];
        } else {
           $exam = $this->db()->exams->getExam($this->importExam);
           $returnData['exam_name'] = $exam['exam_name']; 
        }
              
        // Does exam have results
        if ($this->db()->results->doesExamHaveResults($this->importExam)) {
           return ['errorMsg' => "Chosen exam has results attached"];
        }
        
       // Start database transaction
       $this->db()->startTransaction();
              
       // Delete existing exam sessions
       $this->removeExistingSessions();
     
       // Build matrix data
       $this->buildMatrixData();
       
       // Insert matrix data to database
       $result = $this->insertMatrixData();
       if ($result != true) {
           $this->db()->rollback();         
           return $result['errorMsg'];
       }
                    
       // Commit database changes
       $this->db()->commit();
       
       // Close the file; we're done with it.
       $this->importFile->closeFilePointer();       
       
       // Get exam name
       $examRecord = $this->db()->exams->getExam($this->importExam);
       
       // Log import success
       \Analog::info(
         $_SESSION['user_identifier'] . " imported exam matrix into exam "
         . $examRecord['exam_name'] . "(ID:$this->importExam)"
       );
       
       // Finish up with a complete success return
       $returnData['complete'] = true;
       $returnData['insertedSessions'] = count($this->matrix);
       $returnData['insertedGroups'] = $this->groupCount;
       $returnData['insertedStudents'] = $this->studentCount;
              
       // Remove duplicates
       $this->failedStudents = array_unique($this->failedStudents);
       $this->duplicateStudents = array_unique($this->duplicateStudents);                    
      
       // Register appropropriate error messages to log file
       if (count($this->failedStudents) > 0) {
           error_log(__FILE__ 
             . ' failed to import students from exam matrix: ' 
             . implode(",", $this->failedStudents)
           );
       }
       
       if (count($this->duplicateStudents) > 0) {
             error_log(__FILE__ 
             . ' duplicate students found in exam matrix: ' 
             . implode(",", $this->duplicateStudents)
           );
       }
                             
       $returnData['failedStudents'] = $this->failedStudents;
       $returnData['duplicateStudents'] = $this->duplicateStudents;
       return $returnData;
       
    }
        
    /**
     * Build Matrix data
     */
    private function buildMatrixData() {
     
        // Gather new exam sessions
        for($row=2; $row<=$this->numberOfRows; $row++) {
           
           // Current session
           $sessionName = $this->sheet->getCellByColumnAndRow(0,$row)->getValue();
           $sessionStripped = preg_replace('/\s+/', '', $sessionName);
           $sessionDate = trim($this->sheet->getCellByColumnAndRow(1,$row)->getValue());
           $sessionkey = $sessionStripped . preg_replace('/[^0-9]/','', $sessionDate);

           // Add new found session to list
           if (!empty($sessionkey) && !array_key_exists($sessionkey, $this->matrix)) {
                             
               // Circuit Colour Cell
               $colourCell = strtolower(
                    trim($this->sheet->getCellByColumnAndRow(2,$row)->getValue())
               );
               
               // Validate circuit colour (if found in circuit colour list)
               if (count(preg_grep( "/" . $colourCell . "/i", $this->colours)) > 0) {
                 $circuitColour = strtolower($this->colourData[$colourCell]['circuit_colour']);
               } else {
                 $circuitColour = '';
               }
        
               // Start label
               $start = trim($this->sheet->getCellByColumnAndRow(4,$row)->getValue());
              
               // End label
               $end = trim($this->sheet->getCellByColumnAndRow(5,$row)->getValue());              
               
               // Session date (formatted)
               try {
                   $dateData = explode('/', $sessionDate);
                   $reversedFormat = $dateData[1] . "/" . $dateData[0] . "/" . $dateData[2];
                   $dateFormatted = (new Carbon($reversedFormat))->format('Y-m-d');
               } catch (Exception $exception) {
                    error_log(__FILE__ . ' format date: ' . $exception->getMessage());
                    $dateFormatted = $this->defaultDate;
               }
              
               $this->matrix[$sessionkey] = [
                    'session_name' => $sessionName,
                    'session_date' => $dateFormatted,
                    'circuit_colour' => $circuitColour,
                    'start' => $start,
                    'end' => $end,
                    'groups' => []
               ];
            
           }
           
           // Current group
           $group = trim($this->sheet->getCellByColumnAndRow(3,$row)->getValue());          
           
           // Add groups to matrix data
           if (!empty($group) && !array_key_exists($group, $this->matrix[$sessionkey]['groups'])) {
               $this->matrix[$sessionkey]['groups'][$group] = [];
              
               // Add students to group
               $blankStudentCount = 0;
               
               for($col=6; $col<$this->numberOfCols; $col++) {
                   
                  // Student ID cell value
                  $studentID = trim($this->sheet->getCellByColumnAndRow($col,$row)->getValue());       
                   
                  /**
                   * A student can only appear in an OSCE exam once,
                   * Skip if duplicate found in excel sheet (in other groups)
                   */
                  if ($studentID == "BLANK") {
                    // Ignore blanks, do nothing 
                  } else if (in_array($studentID, $this->uniqueStudents)) {
                      $this->duplicateStudents[] = $studentID; 
                      continue;
                  } else {
                      $this->uniqueStudents[] = $studentID;
                  }
                  
                  // Deal with blank student, 'BLANK' cell
                  $blankStudentCount++;
                  $studentIDFinal = ($studentID == "BLANK") ? ":bs:" . $blankStudentCount : $studentID;
                  $emptyID = empty($studentIDFinal);   
                  $existsInGroup = in_array($studentIDFinal, $this->matrix[$sessionkey]['groups'][$group]);
                  
                  if (!$emptyID && !$existsInGroup) {
                      $this->matrix[$sessionkey]['groups'][$group][] = $studentIDFinal;
                  }
                  
               }
               
               
           }
        
       }
       
        // Get remaining 'end' time
        if (!empty($sessionkey)) {
          $this->matrix[$sessionkey]['end'] = 
                trim($this->sheet->getCellByColumnAndRow(4,$this->numberOfRows)->getValue());      
        }
        
        return true;
    }
  
    /**
     * Generate Prefixes for student exam numbers
     * A to ZZ (a little dirty)
     * @return string[] prefixes A to ZZ
     */    
    private function generatePrefixes() {
      $prefixRange = range('A', 'Z');
      $completePrefixes = $prefixRange;
      foreach($prefixRange as $prefixA) {
        foreach($prefixRange as $prefixB) {  
           $completePrefixes[] = $prefixA.$prefixB;
        }
      }
      return $completePrefixes;
    }
    
    /**
     * Insert Matrix data
     */
    private function insertMatrixData() {
         
       // Generate prefixes
       $prefixes = $this->generatePrefixes();
      
       // Process Matrix data
       $prefixIndex = 0;
       foreach ($this->matrix as $data) {
            
            $sessionDate = $data['session_date'];
            $sessionName = $data['session_name'];
            $circuitColour = $data['circuit_colour'];
            
            $newSessionID = $this->db()->sessions->insert(
                $sessionDate,
                $data['start'],
                $data['end'],
                $this->importExam,
                self::SESSION_UNPUBLISHED,
                $sessionName,
                $circuitColour,
                1
            );
            
            // Failed to insert session, abort
            if (!$newSessionID) {
               return ['errorMsg' => "Failed to insert session: '$sessionName - $sessionDate'"];
            }
            
            // Prepare student exam numbers
            $studentExamNumbers = [];
            $prefix = $prefixes[$prefixIndex];
            $postfix = 1;
            foreach($data['groups'] as $students) {
               foreach($students as $student) {
                 $studentExamNumbers[$student] = $prefix . $postfix;
                 $postfix++;
               }
            }
            
            // Insert group (add to session)
            foreach($data['groups'] as $group => $students) {

                $this->groupCount++;
              
                $groupID = $this->db()->groups->addSessionGroup($newSessionID, $group);
               
                if (!$groupID) {
                  return ['errorMsg' => "Failed to insert group: '$group'"];
                }
                
                // Attach students to group
                $insertStatus = $this->db()->groups->addStudentsToGroup(
                       $students,
                       $groupID,
                       $studentExamNumbers
                );
               
                // Inserted students (count)
                $filtered = array_filter($insertStatus, function($inserted) {
                       return ($inserted['inserted'] == 1);    
                });
                
                // Failed to import students
                $groupFailedList = array_column(array_filter($insertStatus, function($inserted) {
                       return ($inserted['inserted'] == 0);    
                }), 'id');
                
                // Update failed list
                $this->failedStudents = array_merge($this->failedStudents, $groupFailedList);
                
                $insertedCount = count($filtered);

                $this->studentCount += $insertedCount;
               
            }
          $prefixIndex++;
       }        
      
       return true;
    }
    
   /**
    * Validate the PHPExcel sheet
    * @param PHPExcel $sheet
    * @return bool valid
    */
    private function validateSheet($sheet) 
    {
       
        // Import file location
        $importFileLocation = $this->importFile->getFilePath();
        
        // Number of rows
        $numberOfRows = $sheet->getHighestDataRow();
        $this->numberOfRows = $numberOfRows;
        
        // Number of columns
        $this->numberOfCols = PHPExcel_Cell::columnIndexFromString(
                $sheet->getHighestDataColumn()
        );
       
       /**
        * Validate column titles
        */
       $invalidTitles = false;
       foreach($this->requiredColumns as $cellRef => $columnTitle) {
          $cellValue = $sheet->getCell($cellRef)->getValue();
          $titleValue = preg_replace('/\s+/', '', strtolower($cellValue));
          if ($titleValue != preg_replace('/\s+/', '', $columnTitle)) {
            error_log(__FILE__ . " Invalid title: " . $cellValue);
            $invalidTitles = true;
            break;
          }
       }
      
       // At least 1 station column title
       $firstStationTitle = strtolower($sheet->getCell('G1')->getValue());

       if ($invalidTitles || $firstStationTitle != 'station 1' || $numberOfRows <= 2) {
           error_log(__FILE__ 
               .' Invalid import file: "'.pathinfo($importFileLocation, PATHINFO_BASENAME)
           );
           return false;
       }
        
      return true;
    }
    
    /**
     * Load Worksheet
     * 
     * @return object PHPExcel
     */
    function loadWorksheet() {
        
        $importFileLocation = $this->importFile->getFilePath();
        
        try {
           $inputFileType = PHPExcel_IOFactory::identify($importFileLocation);
           $objectReader = PHPExcel_IOFactory::createReader($inputFileType);
           $objectPHPExcel = $objectReader->load($importFileLocation);
           
        } catch(Exception $exception) {
           error_log(__FILE__ 
                .' Error loading import file "'.pathinfo($importFileLocation, PATHINFO_BASENAME).'": '
                .$exception->getMessage()
           );
           return false;
        }
        
        // Get work sheet
        $sheet = $objectPHPExcel->getSheet(0);
       
        $this->PHPExcel = $objectPHPExcel;
        $this->sheet = $sheet;
        
        return true;
    }
    
    /**
     * Remove existing sessions
     */
    private function removeExistingSessions() {
       $examSessions = $this->db()->exams->getExamSessions(
               $this->importExam,
               self::RETURN_DATA_AS_ARRAY
       );
       
       return $this->db()->sessions->deleteSessions(
               array_column($examSessions, 'session_id')
       );
       
    }
    
    
    /**
     * Get database object
     */
    private function db() {
       return $this::$db;
    }
 
}
