<?php

 require_once "ImportFileFields.php";

 class ImportExaminersProcessor
 {

     // Skipped Rows
     private $skippedRows = [];
     private $skippedInfo = [];

     /**
      * @var \OMIS\Database\CoreDB
      * Database link
      */
     private static $db;

     private $pwdByFile;

     // Import map containing the mapping of the file to the db fields
     private $importMap;

     // Import class containing file and file read methods
     private $importFile;

     private $examinerDeptsDB = [];
     private $deptsDB = [];
     private $examinersDB = [];

     private $examinersInsert = [];
     private $examinersUpdate = [];
     private $examinerDeptsInsert = [];

     private $defaultPassword;

     public function __construct($importMap, $importFile, $fileEncoding, $defaultPassword)
     {

         // Get DB Instance
         if (!isset($db) || is_null($db)) {

             $this::$db = \OMIS\Database\CoreDB::getInstance();

         } else {

             $this::$db = $db;

         }

         // Get config Instance
         global $config;
         if (!isset($config))
             $config = \OMIS\Config::getInstance();

         $this->pwdByFile = !isset($config->import["password_by_file"]) ?
             false : $config->import["password_by_file"];

         // Change 'From' Excel Charset
         $this::$db->set_Charset_From($fileEncoding);
         $this->importMap = $importMap;
         $this->importFile = $importFile;
         $this->defaultPassword = $defaultPassword;

     }

     /**
      * Get skipped file rows
      *
      */
     public function getSkippedRows()
     {
         return $this->skippedRows;
     }

     /**
      * Get skipped file rows information
      *
      */
     public function getSkippedRowsInfo()
     {
         return $this->skippedInfo;
     }

     /**
      * Reads the delimited data file, and based on the type figures out which
      * type of data it might be modifying, and then checks what data there
      * already is so that it can determine which records need to be changed and
      * which are new.
      *
      * @return mixed Results of whichever import function is called (depends on data type)
      */
     public function importDataFile()
     {

         $this->getExaminersDB();
         $this->getDepartmentsDB();
         $this->getExaminerDeptsDB();

         /* Go through the data file row by row, deciding what needs updating
          * and what needs inserting.
          */
         $currentRow = $this->importFile->getFirstDataRow();
         while ($currentRow != NULL) {

             $this->processFileRow($currentRow, $this->importMap->getFieldRefs());
             $currentRow = $this->importFile->getNextRow();

         }

         // Close the file; we're done with it.
         $this->importFile->closeFilePointer();

         return $this::$db->users->importExaminerData(
             $this->examinersInsert,
             $this->examinersUpdate,
             $this->examinerDeptsInsert
         );

     }

     /**
      * Process import file row
      * @param array $currentRow
      * @param array $fieldrefs
      */
     private function processFileRow($currentRow, $fieldrefs)
     {

         // Identifier characters allowed
         $idCharactersAllowedText = "Valid characters are 0-9|a-z|-|_";

         // If any of the required fields are empty
         if (!$this->requiredFieldsNotEmpty($currentRow, $fieldrefs)) {
             $this->skippedRows[] = [$this->importFile->getRowCount(),
                 "A required field is empty"];
             return;
         }

         // Get Department ID
         $examinerID = trim($currentRow[$fieldrefs["user_id"]]);
         $deptID = trim($currentRow[$fieldrefs["dept_id"]]);

         // Validate examiner ID
         if (!identifierValid($examinerID)) {
             $this->skippedRows[] = [$this->importFile->getRowCount(),
                 gettext('Examiner') ." ID in file found to be invalid. $idCharactersAllowedText"];
             return;
         }

         // Validate department ID
         if (!identifierValid($deptID)) {
             $this->skippedRows[] = [$this->importFile->getRowCount(),
                 gettext('Department') . " ID in file found to be invalid. $idCharactersAllowedText"];
             return;
         }

         // Department Exists in the database?
         $deptExists = in_array($deptID, $this->deptsDB, true);

         if (!$deptExists) {
             // Collect skipped rows detailed information
             $this->skippedRows[] = [$this->importFile->getRowCount(),
                 gettext('Department') .  " ID (s) do not exist in system"];

             setA($this->skippedInfo, $deptID, 0);
             $this->skippedInfo[$deptID]++;

             return;
         }

         // Required fields Values
         $defaultPassword = $this->pwdByFile ?
             trim($currentRow[$fieldrefs["password"]]) : $this->defaultPassword;

         $userRole = trim($currentRow[$fieldrefs["user_role"]]);

         // Optional fields to include
         $includeForenames = isset($fieldrefs["forename"]);
         $includeSurname = isset($fieldrefs["surname"]);
         $includeEmail = isset($fieldrefs["email"]);
         $includeNumber = isset($fieldrefs["contact_number"]);
         $includeEnabled = isset($fieldrefs["activated"]);

         // Optional fields values
         $forenames = $includeForenames ? trim($currentRow[$fieldrefs["forename"]]) : "";
         $surname = $includeSurname ? trim($currentRow[$fieldrefs["surname"]]) : "";
         $email = $includeEmail ? trim($currentRow[$fieldrefs["email"]]) : "";
         $contactNumber = $includeNumber ? trim($currentRow[$fieldrefs["contact_number"]]) : "";
         $accountEnabled = $includeEnabled ? trim($currentRow[$fieldrefs["activated"]]) : "NO";

         // Examiner Data
         $examinerData = [
             $examinerID,
             $forenames,
             $surname,
             $defaultPassword,
             $email,
             $contactNumber,
             $userRole,
             $accountEnabled
         ];

         // If Examiners not in database add to insert list
         if (!in_array($examinerID, $this->examinersDB, true)) {

             // Examiners to Insert
             if (!array_key_exists($examinerID, $this->examinersInsert)) {
                 $this->examinersInsert[$examinerID] = $examinerData;
             }

         }

         // Examiners to Update
         else if (!in_array($examinerID, $this->examinersUpdate, true)) {
             $this->examinersUpdate[$examinerID] = $examinerData;
         }

         // Examiner Department key combined
         $exaDeptKey = $examinerID . $deptID;

         // Examiner exists in department?
         $examinerInDept = (array_key_exists($examinerID, $this->examinerDeptsDB) &&
             in_array($deptID, $this->examinerDeptsDB[$examinerID], true));

         // Examiner Department key stored for insertion into the database?
         $examinerKeyReady = array_key_exists($exaDeptKey, $this->examinerDeptsInsert);

         // Add key and data to DB insert array
         if (!$examinerInDept && !$examinerKeyReady) {

             $this->examinerDeptsInsert[$exaDeptKey] = [
                 $examinerID,
                 $deptID
             ];

         }

     }

     /**
      * Check to see if the required fields are not empty in a row of delimited
      * data
      *
      * @param Array $currentRow     Row returned from fgetcsv()
      * @param Array $fieldrefs      List of fields used in the import file.
      * @return boolean
      */
     private function requiredFieldsNotEmpty($currentRow, $fieldrefs)
     {
         foreach ($this->importMap->getRequiredFields() as $reqField) {
             if (strlen($currentRow[$fieldrefs[$reqField]]) == 0) {
                 return false;
             }
         }
         return true;
     }

     /**
      * Get the Examiners that are in the database
      */
     private function getExaminersDB()
     {
         $examinersDB = $this::$db->users->getExaminerUserAccounts();
         foreach ($examinersDB as $examiner) {
             $this->examinersDB[] = $examiner["user_id"];
         }
     }

     /**
      * Get the Departments that are in the database
      */
     private function getDepartmentsDB()
     {
         $results = $this::$db->departments->getAllDepartments();
         while ($dept_row = $this::$db->fetch_row($results)) {
             $this->deptsDB[] = $dept_row["dept_id"];
         }
     }

     /**
      * Get examiner departments
      */
     private function getExaminerDeptsDB()
     {
         $results = $this::$db->users->getExaminersDepartments($_SESSION["cterm"]);
         foreach ($results as $row) {
             $user_id = $row["user_id"];
             $deptID = $row["dept_id"];
             if (!array_key_exists($user_id, $this->examinerDeptsDB)) {
                 $this->examinerDeptsDB[$user_id] = [];
             }
             $this->examinerDeptsDB[$user_id][] = $deptID;
         }
     }

 }