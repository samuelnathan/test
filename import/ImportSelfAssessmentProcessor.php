<?php
/* This is to get the constants for the various import types - Student,
 * Examiner, etc.
 */
require_once "ImportFileFields.php";

class ImportSelfAssessmentProcessor
{

    private $selfAssessmentQuestions = [];

    private $studentExistValidations = [];

    private $examId;
    private $title;
    private $maxScore;

    // Skipped Rows
    private $skippedRows = [];
    private $skippedInfo = [];

    /**
     * @var \OMIS\Database\CoreDB
     * Database link
     */
    private static $db;

    // Import map containing the mapping of the file to the db fields
    private $importMap;

    // Import class containing file and file read methods
    private $importFile;

    private $report = [
        "insertedScoresheets" => 0,
        "warningMessage" => null,
        "errorMsg" => null,
        "complete" => false
    ];

    // Constructor
    public function __construct($importMap, $importFile, $fileEncoding, $examId, $title, $maxScore = null, $db = null)
    {

        if (is_null($db)) {
            $this::$db = \OMIS\Database\CoreDB::getInstance();
        } else {
            $this::$db = $db;
        }

        // Change 'From' Excel Charset
        $this::$db->set_Charset_From($fileEncoding);
        $this->importMap = $importMap;
        $this->importFile = $importFile;

        $this->examId = $examId;
        $this->title = $title;
        $this->maxScore = $maxScore;
    }

    /**
     * Get skipped file rows
     *
     */
    public function getSkippedRows()
    {
        return $this->skippedRows;
    }

    /**
     * Get skipped file rows information
     *
     */
    public function getSkippedRowsInfo()
    {
        return $this->skippedInfo;
    }

    /**
     * Reads the delimited data file, and based on the type figures out which
     * type of data it might be modifying, and then checks what data there
     * already is so that it can determine which records need to be changed and
     * which are new.
     *
     * @return mixed Results of whichever import function is called (depends on data type)
     */
    public function importDataFile()
    {
        /* Go through the data file row by row, deciding what needs updating
         * and what needs inserting.
         */
        $currentRow = $this->importFile->getFirstDataRow();
        while ($currentRow != NULL) {
            $this->processFileRow($currentRow, $this->importMap->getFieldRefs());
            $currentRow = $this->importFile->getNextRow();
        }

        // Close the file; we're done with it.
        $this->importFile->closeFilePointer();

        // Validate the consistency of the number of items of each scoresheet
        if (!$this->validateLengthsConsistency()) {
            $this->report["warningMsg"] = "Detected inconsistency on the number of items for each applicant";
        }

        $invalidTotalScoreApplicants = [];
        if (!$this->validateMaxScore($invalidTotalScoreApplicants)) {
            $this->report["warningMsg"] = "Total score for applicants " . implode(", ", $invalidTotalScoreApplicants) . " surpasses maximum score";
        }

        $this->importSelfAssessmentScoresheet();

        $this->report["insertedScoresheets"] = $this->getScoresheetsImportedCount();

        return $this->report;
    }

    /**
     * Process import file row
     * @param array $currentRow
     * @param array $fieldrefs
     */
    private function processFileRow($currentRow, $fieldrefs)
    {
        $validResponse = true;


        // Identifier characters allowed
        $idCharactersAllowedText = "Valid characters are 0-9|a-z|-|_";

        // If any of the required fields are empty
        if (!$this->requiredFieldsNotEmpty($currentRow, $fieldrefs)) {
            $this->skippedRows[] = [$this->importFile->getRowCount(),
                "A required field is empty"];
            return;
        }

        $applicantId = trim($currentRow[$fieldrefs["applicant_id"]]);
        $question = trim($currentRow[$fieldrefs["question"]]);
        $response = trim($currentRow[$fieldrefs["response"]]);
        $score = trim($currentRow[$fieldrefs["score"]]);

        if (!$this->validateScore($score)) {
            $this->skippedRows[] = [$this->importFile->getRowCount(),
                "Invalid value for score. It must be a number greater or equal than zero"];
            $validResponse = false;
        }

        // Validate that the applicant exists in the database
        $studentExists = $this->studentExists($applicantId);
        if (!$studentExists) {
            $this->skippedRows[] = [$this->importFile->getRowCount(),
                "Applicant $applicantId was not found in the system"];
            $validResponse = false;
        }

        $item = [
            "student_id" => $applicantId,
            "score" => $score,
            "response" => $response,
            "valid" => $validResponse
        ];

        $existingQuestionIndex = $this->getQuestionIndex($question);

        // Add the response to it's question.
        // - If the question already exists. Push the item to its responses
        if ($existingQuestionIndex !== null)
            $this->selfAssessmentQuestions[$existingQuestionIndex]['responses'][] = $item;
        // - If it's the first time this question appears, create it with the item in its responses
        else
            $this->selfAssessmentQuestions[] = [
                'question' => $question,
                'responses' => [$item]
            ];
    }

    private function importSelfAssessmentScoresheet() {
        $this::$db->startTransaction();

        // Delete the previous scoresheets for the exam, if necessary
        if (!$this->deletePreviousScoresheets()) {
            $this->unexpectedError();
            return;
        }

        // Insert the scoresheet
        $scoresheetId = $this::$db->selfAssessments
            ->insertScoresheet($this->examId, $this->title, $this->maxScore);

        // Validate no errors occurred
        if ($scoresheetId === null) {
            $this->unexpectedError();
            return;
        }

        // Insert each question and it's responses
        foreach ($this->selfAssessmentQuestions as $questionOrd => $question) {
            // Insert the question
            $questionId = $this::$db->selfAssessments
                ->insertScoresheetQuestion($scoresheetId, $question['question'], $questionOrd + 1);

            // Validate no errors occurred
            if ($questionId === null) {
                $this->unexpectedError();
                return;
            }

            // Insert the questions responses
            foreach ($question['responses'] as $response) {
                if (!$response['valid']) continue;  // Skip invalid responses

                // Insert the response and keep looping if it was successful
                if ($this::$db->selfAssessments->insertScoresheetResponse(
                    $questionId,
                    $response['student_id'],
                    $response['score'],
                    $response['response'])) continue;

                $this->unexpectedError();
                return;
            }
        }

        $this::$db->commit();
        $this->report['complete'] = true;
    }

    /**
     * Check if the exam has scoresheets and, if it has, delete them
     * @return bool Success of the operation. If the exam didn't have previous scoresheets, it's considered success
     */
    private function deletePreviousScoresheets() {
        return $this::$db->selfAssessments->existsByExam($this->examId) ?
            $this::$db->selfAssessments->deleteScoresheetsForExam($this->examId) : true;
    }

    /**
     * Get the index of a question. If the question is not found, returns null
     * @param $question
     * @return int|null
     */
    private function getQuestionIndex($question) {
        foreach ($this->selfAssessmentQuestions as $index => $existingQuestion) {
            if ($this->normalizeQuestion($existingQuestion['question']) === $this->normalizeQuestion($question)) {
                return $index;
            }
        }

        return null;
    }

    private function normalizeQuestion($question) {
        $question = trim($question);                                                       // Trim
        $question = preg_replace('/\s+/', ' ', $question);              // Remove extra spaces
        $question = preg_replace('/[^a-zA-Z0-9\s]/', '', $question);    // Remove non alphanumeric characters

        return strtolower($question);
    }

    /**
     * Check that a given student exists in the database
     * @param $studentId
     * @return mixed
     */
    private function studentExists($studentId) {
        if (!array_key_exists($studentId, $this->studentExistValidations))
            $this->studentExistValidations[$studentId] = $this::$db->students->doesStudentExist($studentId);

        return $this->studentExistValidations[$studentId];
    }

    /**
     * Validate that the number of items of each scoresheet is the same for all of them
     * @return bool
     */
    private function validateLengthsConsistency() {
        $lengths = array_map(function($question) { return count($question['responses']); }, array_values($this->selfAssessmentQuestions));
        return array_reduce($lengths, function ($expected, $length) {
                return $expected === null || $expected !== $length ? null : $length;
            }, $lengths[0]) !== null;
    }

    /**
     * Validates that the total score for the scoresheets is less than or equal to the maximum score
     * @param $invalidApplicants Array where the IDs of the applicants whose scoretheets are invalid will be stored
     * @return bool Result of the validation. True if successful, false otherwise
     */
    private function validateMaxScore(&$invalidApplicants) {
        // If there's no maximum score, don't perform the validation
        if ($this->maxScore === null)
            return true;

        // Array of the type <Applicant ID> => <Applicant total score>
        $applicantScores = [];

        // Build the applicant scores array
        foreach ($this->selfAssessmentQuestions as $question) {
            foreach ($question['responses'] as $response) {
                $applicantId = $response['student_id'];
                $applicantScore = array_key_exists($applicantId, $applicantScores) ?
                    $applicantScores[$applicantId] :
                    0;

                $applicantScores[$applicantId] = $applicantScore + $response['score'];
            }
        }

        // Take the applicants whose score surpases the maximum score
        $invalidApplicants = array_keys(array_filter($applicantScores, function ($score) {
           return  $score > $this->maxScore;
        }));

        // It fails if the invalid applicants list is not empty
        return count($invalidApplicants) > 0;
    }

    /**
     * Validate the value of the score field
     * @param $score string
     * @return bool True if the score field is a number greater or equal than zero
     */
    private function validateScore($score) {
        $asInt = (int)$score;
        // Geniuses behind PHP thought that the best return value in case the conversion to integer fails is ZERO,
        // so if the value after the casting is 0 we must check that the original string is "0"
        if ($asInt === 0) return $score === '0';
        return $asInt >= 0;
    }

    private function getScoresheetsImportedCount() {
        $scoresheets = [];

        foreach ($this->selfAssessmentQuestions as $question) {
            foreach ($question['responses'] as $response) {
                if ($response['valid']) $scoresheets[$response['student_id']] = true;
            }
        }

        return count($scoresheets);
    }

    /**
     * Sets the error message explaining an unexpected error and rolls back the database changes
     */
    private function unexpectedError() {
        $this->report['errorMsg'] = "An unexpected error occurred, please try again";
        $this::$db->rollback();
    }

    /**
     * Check to see if the required fields are not empty in a row of delimited
     * data
     *
     * @param Array $currentRow     Row returned from fgetcsv()
     * @param Array $fieldrefs      List of fields used in the import file.
     * @return boolean
     */
    private function requiredFieldsNotEmpty($currentRow, $fieldrefs)
    {
        foreach ($this->importMap->getRequiredFields() as $reqField) {
            if (strlen($currentRow[$fieldrefs[$reqField]]) == 0) {
                return false;
            }
        }
        return true;
    }

}