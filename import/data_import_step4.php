<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define("IMPORT_EXAM_MATRIX", 5);

 // Template
 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

     return false;

 }

 // Validate import term
 $importTerm = filter_input(INPUT_POST, 'term', FILTER_SANITIZE_STRING);
 if (!is_null($importTerm) && $db->academicterms->existsById($importTerm)) {

     $_SESSION['cterm'] = $importTerm;

 }

 $importFile = unserialize($_SESSION["importFile"]);

 $filename = $importFile->getFileName();

 $dataImportType = $importFile->getDataType();
 $importTypeDesc = $importFile->getDescDataType();

 $courseVars = $studentVars = $examinerVars
     = $skippedRowInfo = $skippedRows = $fieldsRefs = [];

 $courseUpdatesMade = $studentUpdatesMade
     = $examinerUpdatesMade = false;

 $skippedRowCount = 0;
 $fileEncoding = "UTF-8";
 $choosenExam = null;

 // Process fields references of file made at step 3 in importing process
 for ($i=0; $i<$importFile->getNrOfFields(); $i++) {

     $dataField = filter_input(INPUT_POST, "dataField_" . $i);
     $fieldsRefs[$dataField] = $i;

 }

 $startTime = microtime();
 $startTime = explode(" ", $startTime);
 $startTime = $startTime[1] + $startTime[0];

 // Create a import process class and start import into db
 if ($dataImportType == IMPORT_EXAM_MATRIX) {

     $choosenExam = filter_input(INPUT_POST, "exam_matrix", FILTER_SANITIZE_NUMBER_INT);
     $importProcessor = new ImportMatrixProcessor(
         $choosenExam,
         $importFile,
         $fileEncoding
     );

     $report = $importProcessor->importDataFile();

 } else if ($dataImportType == IMPORTEXPORT_DATATYPE_SCORESHEETS) {

     require_once "ImportSelfAssessmentProcessor.php";

     $importMap = unserialize($_SESSION["importMap"]);
     $importMap->setFieldRefs($fieldsRefs);

     $choosenExam = filter_input(INPUT_POST, "exam_selfassessment", FILTER_SANITIZE_NUMBER_INT);

     $title = trim(filter_input(INPUT_POST, "title_selfassessment", FILTER_SANITIZE_STRING));
     $title = $title !== '' ? $title : null;

     $maxScore = trim(filter_input(INPUT_POST, "maxscore_selfassessment", FILTER_SANITIZE_NUMBER_INT));
     $maxScore = $maxScore !== '' ? $maxScore : null;

     $importProcessor = new ImportSelfAssessmentProcessor($importMap, $importFile, $fileEncoding, $choosenExam, $title, $maxScore);
     $report = $importProcessor->importDataFile();
     $skippedRowInfo = $importProcessor->getSkippedRowsInfo();
     $skippedRows = $importProcessor->getSkippedRows();
     $skippedRowCount = count($skippedRows);

 } else if ($dataImportType == IMPORTEXPORT_DATATYPE_EXAMINERS) {

     require_once "ImportExaminersProcessor.php";

     $importMap = unserialize($_SESSION["importMap"]);
     $importMap->setFieldRefs($fieldsRefs);

     $password = trim(filter_input(INPUT_POST, "password_examiners", FILTER_SANITIZE_STRING));

     $importProcessor = new ImportExaminersProcessor($importMap, $importFile, $fileEncoding, $password);

     $report = $importProcessor->importDataFile();
     $skippedRowInfo = $importProcessor->getSkippedRowsInfo();
     $skippedRows = $importProcessor->getSkippedRows();
     $skippedRowCount = count($skippedRows);

 } else {

    $importMap = unserialize($_SESSION["importMap"]);

    // Save fields references of file in the importMap class
    $importMap->setFieldRefs($fieldsRefs);

    $importProcessor = new ImportProcessor(
            $importMap,
            $importFile,
            $fileEncoding
    );

    // Skipped Row Information for Examiner Importing
    $report = $importProcessor->importDataFile();
    $skippedRows = $importProcessor->getSkippedRows();
    $skippedRowCount = count($skippedRows);


 }

 $endTime = microtime();
 $endTime = explode(" ", $endTime);
 $endTime = $endTime[1] + $endTime[0];
 $importTime = round(($endTime - $startTime), 2);

 /*
  * When importing is done delete the file from the server and
  * all references in the importFile class
  */
 $importFile->deleteCurrentFile();

 $totalDataRows = ($importFile->getRowCount() - 1);

 $messageData = array_column($skippedRows, 1);
 $skippedErrorMessages = array_unique($messageData);

 $importComplete = (isset($report["complete"]) && $report["complete"]);

 // Course & modules import variables
 $courseVarsReady = isset($report["insertedCourses"],
                          $report["updatedCourses"],
                          $report["insertedYears"],
                          $report["insertedModules"],
                          $report["updatedModules"],
                          $report["insertedModuleYears"]);

 if ($courseVarsReady) {

     $courseVars = [$report["insertedCourses"],
                    $report["updatedCourses"],
                    $report["insertedYears"],
                    $report["insertedModules"],
                    $report["updatedModules"],
                    $report["insertedModuleYears"]];

     $courseUpdatesMade = (max($courseVars) > 0);

 }

 // Student import variables
 $studentVarsReady = isset($report["insertedStudents"],
                           $report["updatedStudents"],
                           $report["studentYearsInserted"],
                           $report["studentModulesInserted"]);

 if ($studentVarsReady) {

     $studentVars = [$report["insertedStudents"],
                     $report["updatedStudents"],
                     $report["studentYearsInserted"],
                     $report["studentModulesInserted"]];

     $studentUpdatesMade = (max($studentVars) > 0);

 }

 // Examiner import variables
 $examinerVarsReady = isset($report["insertedExaminers"],
                            $report["updatedExaminers"]);

 if ($examinerVarsReady) {

     $examinerVars = [$report["insertedExaminers"],
                      $report["updatedExaminers"]];

     $examinerUpdatesMade = (max($examinerVars) > 0);

 }

 // Register log entry
 if ($importComplete && $courseVarsReady && $courseUpdatesMade) {
      \Analog::info ($_SESSION["user_identifier"] . " performed a courses & modules import");
 } elseif ($importComplete && $studentVarsReady && $studentUpdatesMade) {
      \Analog::info ($_SESSION["user_identifier"] . " performed a student import");
 } elseif ($importComplete && $examinerVarsReady && $examinerUpdatesMade) {
      \Analog::info ($_SESSION["user_identifier"] . " performed an examiner import");
 } elseif ($dataImportType == IMPORT_EXAM_MATRIX && $importComplete) {
      \Analog::info ($_SESSION["user_identifier"] . " performed an exam matrix import");
 } elseif($dataImportType == IMPORTEXPORT_DATATYPE_SCORESHEETS && $importComplete) {
     \Analog::info($_SESSION["user_identifier"] . " performed a self assessment scoresheet import");
 }

 // Template for import report
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render([

     "file_name" => $filename,
     "data_row_count" => $totalDataRows,
     "skipped_row_count" => $skippedRowCount,
     "skipped_row_info" => $skippedRowInfo,
     "skipped_row_messages" => $skippedErrorMessages,
     "import_time" => $importTime,
     "data_import_type" => $dataImportType,
     "import_type_desc" => $importTypeDesc,
     "system_name" => $config->getName(),
     "report" => $report,
     "import_complete" => $importComplete,

     "course_vars_ready" => $courseVarsReady,
     "course_updates_made" => $courseUpdatesMade,

     "student_vars_ready" => $studentVarsReady,
     "student_updates_made" => $studentUpdatesMade,

     "examiner_vars_ready" => $examinerVarsReady,
     "examiner_updates_made" => $examinerUpdatesMade,

     "choosen_exam" => $choosenExam,

     "import_user_id" => $_SESSION['user_identifier']

 ]);
