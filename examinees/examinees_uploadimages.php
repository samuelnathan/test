<?php
/**
 * Upload form page for student images
 * 
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 */

// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

// Make sure we have an instance of the configuration object.
global $config;
if (!isset($config)) {
    $config = new OMIS\Config();
}

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Student images feature enabled for the current user role ? If not then inform user
$studentImagesAllowed = $db->features->enabled('student-images', $_SESSION['user_role']);
if (!$studentImagesAllowed) {
 
    $message = "You are not permitted to use this feature.<br/>". 
               "please contact the administrator for further assistance";  
    $templateData = ['message' => $message,
                     'return_url' => "",
                     'content_wrapper' => true]; 
    $errorTemplate = new OMIS\Template('error.html.twig');
    $errorTemplate->render($templateData);  
   
} else {

// Get the image size limit in friendly units.
$image_size_limit = formatFileSize($config->imagecache['maxfilesize'], null, false);
?>
<div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Import <?=gettext('Student')?> Images
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      In this section you can import <?=gettext('student')?> images into the <?= $config->getName()?> system.<br/>
        <ul>
            <li>Images must be in GIF, JPEG or PNG (.gif, .jpe, .jpeg, .jpg, .png) format.</li>
            <li>Images must not exceed <?=$image_size_limit ?></li>
            <li>Images must be uploaded as a single ZIP archive containing one or more images.</li>
        </ul>
        <div class="helpimport">
            Need help with importing? Please contact us at 
            <a href="mailto:support@qpercom.com">support@qpercom.com</a>
        </div>
    </div>   
  </div> 
</div> 
    <br class="clearv2"/>
</div>
<div class="contentdiv">
    <form enctype="multipart/form-data" action="manage.php?page=examinees_processimages" method="post" onsubmit="return(validateForm())">
        <div class="h6">
            File selector
        </div>    
        <table class="table-border">
            <tr>
                <th class="title">
                    Select a File to Import
                </th>
                <td>
                    <input name="data_file" id="data_file" type="file" size="41"/>
                </td>
                <td class="infotd" id="error_list"></td>
            </tr>
            <tr id="lastrow">
                <td class="base-button-options ggrey" colspan="2">
                    <div id="button_row" class="fr">
                        <input type="submit" class="bml btn btn-sm btn-success" value="begin import"/>
                    </div>
                </td>
                <td class="infotd"></td>
            </tr>
        </table>
    </form>
</div>
<?php 
}