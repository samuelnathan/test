<?php
/**
 * Tasks:
 * 1. Accept uploaded image
 * 2. Verify that it's a ZIP file.
 * 3. Get a list of all the files in there. See if any are images, and if so,
 *    which among those have student ID-like names.
 * 4. Extract those files to the image cache, potentially overwriting any images
 *    that were there previously for that user.
 */
require_once __DIR__ . '/../extra/helper.php';

$zip = new OMIS\Utilities\ZipFile(__DIR__ . '/../scripts/tools/student_generator/students.zip');

var_dump($zip->files());