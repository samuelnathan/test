<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
require_once __DIR__ . "/../extra/helper.php";

use OMIS\Template as Template;
use OMIS\Utilities\Path as Path;
use OMIS\Imaging\Image as Image;
use OMIS\Imaging\Cache as ImageCache;

/*
 *  Output Examinee Editing Rows
 */
function outputExamineeRows($action, $number, $course, $year)
{
    // Get the configuration settings for OMIS.
    global $config;
    if (!isset($config)) {
      
        $config = new OMIS\Config;
        
    }
    
    // Define the image cache...
    $cache = new ImageCache($config->imagecache["caches"]["students"]);
    
    // Set up the database connection, grab some details.
    $db = \OMIS\Database\CoreDB::getInstance();
    $countries = $db->iso->getISOCountryCodes();

    // Candidate number feature
    $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);
    
    $selectedTerm = selectedTerm("t");
    
    // Get Course Years
    $courseYears = $db->courses->getCourseYears($course, $selectedTerm, false);
  
    $coursesModules = $db->courses->getCourseModules(
        $course,
        $selectedTerm,
        ($action == "add" && $db->courses->doesCourseYearExist($year)) ? $year : null
    );
        
    // Set up Twig...
    try {
      
        $template = new OMIS\Template("edit_examinee_row.html.twig");
        
    } catch (RuntimeException $e) {
      
        $error_message = $e->getMessage();
        /* As the templating engine didn't instantiate properly, there's no point in
         * trying to use a template to render the error message.
         */
        die("Failed to instantiate templating. Error message was $error_message");
        
    }
    
    // Iterate through the records...
    for ($index = 0; $index < $number; $index++) {
      
        $examineeYears = [];
        $examineeModules = [];
        
        // Set some variables based on whether we're adding or updating examinees.
        // Update action
        if ($action == "update" && isset($_SESSION["examinees_to_update"])) {
          
            $examineeID = trim($_SESSION["examinees_to_update"][$index]);
            $examineeRecord = $db->students->getStudent($examineeID, $selectedTerm);
            
            if ($examineeRecord["dob"] == "0000-00-00") {
              
                $dob = ["", "", ""];
                
            } else {
              
                $dob = explode("-", $examineeRecord["dob"]);
                
            }
            
            /* Get examinee course data by examinee identifier,
             * course identifier and current academic term Identifier  
             */
            $examineeCourseData = $db->students->getStudentCourseData(
                $examineeRecord["student_id"],
                $course,
                $selectedTerm
            );
                        
            /* Let's grab the year and module identifiers 
             * and put them in separate arrays
             */
            if (is_array($examineeCourseData) && sizeof($examineeCourseData) > 0) {
              
               $examineeYears = array_unique(
                    array_column($examineeCourseData, "year_id")
               );
  
               $examineeModules = array_unique(
                    array_column($examineeCourseData, "module_id")
               );
               
            }
            
            // Examinee image
            $examineeImage = $examineeRecord["student_image"];
            if ((trim($examineeImage) != "") && $cache->contains($examineeImage)) {
              
                $imagePath = $examineeImage;
                
            } else {
              
                /* This should always exist as the Cache's constructor should
                 * make sure of it.
                 */
                $imagePath = basename($config->imagecache["images"]["default"]);
                
            }
            
        // Add action
        } else {
          
            $examineeID = $index + 1;
            $examineeYears[] = $year;
            $examineeRecord = [
                "student_id" => "",
                "candidate_number" => "",
                "surname" => "",
                "forename" => "",
                "gender" => "",
                "nationality" => "",
                "email" => ""
            ];
            $imagePath = basename($config->imagecache["images"]["default"]);
            $dob = ["", "", ""];
            
        }
        
        /* Get the path to the thumbnail, generating it from the original if
         * necessary.
         */
        $thumbnailPath = $cache->getThumbnail($imagePath);

        // Make sure the student details know what's going on.
        $imageFullPath = Path::simplify($cache->get($imagePath));
        $imageRelPath = Path::toURL($imageFullPath);
        $examineeRecord["image"] = [
                "thumbnail" => Image::getImageAsInline($thumbnailPath),
                "large" => $imageRelPath
        ];
        
        // Template data
        if ($action == "add" || ($action == "update" && $examineeRecord !== NULL)) {
          
            $data = [
                "action" => $action,
                "index" => $index + 1,
                "examinee_id" => $examineeID,
                "examinee_record" => $examineeRecord,
                "examinee_years" => $examineeYears,
                "examinee_modules" => $examineeModules,
                "examinee_dob" => $dob,
                "countries" => $countries,
                "course_years" => $courseYears,
                "course_modules" => $coursesModules,
                "selected_term" => $selectedTerm,
                "candidate_number_feature" => $candidateNumberFeature
            ];
            
            // Render template
            $template->render($data);
            
        }
        
        // Unset years and modules
        unset($examineeYears);
        unset($examineeModules);
        
    }
    
}

/*
 * Output Bottom Row
 */
function outputBottomRow($action, $number, $course, $year, $module, $orderIndex, $orderType, $pageNumber, $searchString)
{
        
    // Set up Twig...
    try {
        $template = new Template("edit_footer_row.html.twig");
    } catch (RuntimeException $e) {
        $error_message = $e->getMessage();
        /* As the templating engine didn't instantiate properly, there's no point in
         * trying to use a template to render the error message.
         */
        die("Failed to instantiate templating. Error message was $error_message");
    }
    
    // Candidate number feature
    $db = \OMIS\Database\CoreDB::getInstance();
    $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);
    
    // Button Label
    $buttonLabel = ($action == "add") ? "add " . gettext("student") : "update " . gettext("student");
    
    // Add 's' to end of multiple
    $buttonLabel .= ($number > 1) ? "s" : ""; 
    
    // Template data
    $data = [
        "action" => $action,
        "button_label" => $buttonLabel,
        "course" => $course,
        "year" => $year,
        "module" => $module,
        "pagenr" => $pageNumber,
        "search_string" => $searchString,
        "order_index" => $orderIndex,
        "order_type" => $orderType,
        "candidate_number_feature" => $candidateNumberFeature
    ];
    
    // Render template
    $template->render($data);
}
