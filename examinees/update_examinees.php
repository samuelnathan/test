<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  */

 use OMIS\Template as Template;
 use OMIS\Database\CoreDB as CoreDB;
 
 // Critical Session Check
 $session = new OMIS\Session();
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
   
     return false;
     
 }
 
 // What's required
 require_once __DIR__ . "/examinees_funcs.php";
 
 // Initial Variables
 $actionParm = filter_input(INPUT_GET, "a", FILTER_SANITIZE_STRING);
 if (in_array($actionParm, ["add", "update"])) {
   
     $action = $actionParm;
     
 } else {
   
     $action = false;
     
 }
 
 $course = filter_input(INPUT_GET, "c", FILTER_SANITIZE_STRING);
 $year = filter_input(INPUT_GET, "y", FILTER_SANITIZE_NUMBER_INT);
 $module = filter_input(INPUT_GET, "m", FILTER_SANITIZE_STRING);
 $pageNumber = filter_input(INPUT_GET, "p", FILTER_SANITIZE_NUMBER_INT);
 $search = filter_input(INPUT_GET, "s", FILTER_SANITIZE_STRING);
 $orderIndex = filter_input(INPUT_GET, "i", FILTER_SANITIZE_STRING);
 $orderType = filter_input(INPUT_GET, "o", FILTER_SANITIZE_STRING);
 
 // Course record 
 $courseRecord = $db->courses->getCourse($course);
 
 // Candidate number feature
 $candidateNumberFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);
 
 // which action [add, update]
 switch ($action) {
   
     case "add":
         $legend = "Add " . gettext('Students') . " to " . gettext('Course');
         
         $numberRequired = filter_input(INPUT_GET, "n", FILTER_SANITIZE_NUMBER_INT);
         if (!is_numeric($numberRequired) || $numberRequired <= 0) {
           
             $numberRequired = 1;
             
         }
         break;
     case "update":
         $legend = "Update " . gettext('Students');
         
         // If single update (come back to)
         if (filter_has_var(INPUT_GET, "e")) {
           
             unset($_SESSION["examinees_to_update"]);
             $_SESSION["examinees_to_update"][] = trim(
                 filter_input(INPUT_GET, "e", FILTER_SANITIZE_STRING)
             );
             
         }
         
         // Number of examinees to update
         if (isset($_SESSION["examinees_to_update"]) && sizeof($_SESSION["examinees_to_update"]) > 0) {
           
             $numberRequired = sizeof($_SESSION["examinees_to_update"]);
             
         } else {
           
             $numberRequired = 1; 
             
         }
         break;
         
     default:
        $legend = "Error";
        
 }
 
 // Can continue check
 $canContinue = (!is_null($courseRecord) && $action !== false);
 
 ?><div class="topmain">
<div class="card d-inline-block align-top mr-2">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          <?=$legend?>
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      <?php
 
 if ($canContinue) {
   
     ?>   
      <span class="red f12">Please choose correct <?=gettext('term')?> and <?=gettext('course')?> 
          <u>before</u> completing fields for each <?=gettext('student')?></span><br/>
     <?php
     
 } else {
   
     ?>
     <span class="red">
         The <?=gettext('course')?> was not found in the system. Please click the back button<br/>
         to return to the <?=gettext('student')?> list page
     </span>
     <?php
     
 }
 
 ?>
    </div>   
  </div> 
</div> 
     <br class="clearv2"/>
     <br/>
 </div>
 <div class="contentdiv">
 <?php
 
 if ($canContinue && (in_array($action, ["add", "update"]))) :
   
     // Use the selected term if provided.
     $selectedTerm = filter_input(INPUT_GET, "t", FILTER_SANITIZE_STRING);
     
     if (empty($selectedTerm)) {
       
         $selectedTerm = selectedTerm("t");
         
     }
    
     // Get all courses linked to term
     $courses = $db->courses->getAllCourses($selectedTerm);
     
     $coursesGrouped = $config->db['couple_depts_courses'] ? 
         CoreDB::group($courses, ['school_description'], true) : $courses;   
     
     // Get all terms
     $terms = $db->academicterms->getAllTerms();
 
     $data = [
         "terms" => $terms,
         "selectedTerm" => $selectedTerm,
         "courses" => $coursesGrouped,
         "coupleDeptsCourses" => $config->db['couple_depts_courses'],
         "selectedCourse" => $course,
         "queryparams" => $_REQUEST
     ];
 
     // Set up Twig...
     try {
       
         $template = new Template("selections.html.twig");
         
     } catch (RuntimeException $e) {
       
         $error_message = $e->getMessage();
         /* As the templating engine didn't instantiate properly, there's no point in
          * trying to use a template to render the error message.
          */
         die("Failed to instantiate templating. Error message was $error_message");
         
     }
     
     // Render template
     $template->render($data);
     
     ?>
     <form name="update_form" method="post" action="examinees/examinees_redirect.php" onsubmit="return(ValidateForm(this))">
     <table id="update-table" class="table-border">
     <tr id="title-row" class="title-row">
      <td class="all-checkbox-td">&nbsp;</td>
      <th>Image</th>
      <th>Identifier</th>
      <?php
      
      if ($candidateNumberFeature) {
        
        ?><th>Candidate #</th><?php
        
      }
      
      ?>
      <th>Surname</th>
      <th>Forenames</th>
      <th>DOB</th>
      <th>Gender</th>
      <th>Nationality</th>
      <th class="nbr">Email</th>
     </tr>
      <?php
      
        // Examinee Rows
        outputExamineeRows($action, $numberRequired, $course, $year);
      
        // Bottom Row
        outputBottomRow($action, $numberRequired, $course, $year, $module, $orderIndex, $orderType, $pageNumber, $search);
        
       ?>
     </table>
     </form>
     <?php
 endif;
 ?>
 </div>
 