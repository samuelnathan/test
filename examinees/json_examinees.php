<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define("_OMIS", 1);
header("Content-Type: application/json");

$isfor = filter_input(INPUT_POST, "isfor", FILTER_SANITIZE_STRIPPED);

if (!empty($isfor)) {
  
    require_once __DIR__ . "/../extra/essentials.php";
    require_once __DIR__ . "/../extra/helper.php";
    
    if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
      
        return false;
        
    }
    
} else {
  
    http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
    exit();
    
}

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Utilities\JSON as JSON;

// json_encode takes care of Html Entities
$db->set_Convert_Html_Entities(false);

/**
 * Get Module Only
 */
if ($isfor == "get_modules") {
  
    $return = [];
    // Get the ID of the course...
    $courseID = filter_input(INPUT_POST, "course", FILTER_SANITIZE_STRING);
    
    //Do we have a valid course ID and does the course exist?
    if (!is_null($courseID) && $db->courses->doesCourseExist($courseID)) {
      
        if (in_array("year", array_keys($_POST))) {
          
            /* The unique Course Year ID has been specified, that's enough to 
             * get a valid list of available modules...
             */
            $courseYear = filter_input(INPUT_POST, "year", FILTER_VALIDATE_INT, ["default" => 0]);
            $modulesDB = $db->courses->getModulesByCourseYear($courseYear, "module_id");
            
        } else {
          
            // No course year, so get all modules for the specified course.
            $modulesDB = $db->courses->getModulesByCourse($courseID, $_SESSION["cterm"]);
        }
        
        // Extract the useful bits from the returned data.
        foreach ($modulesDB as $module) {
          
            $return[] =  ["module_id" => $module["module_id"], "module_name" => $module["module_name"]];
            
        }
        
    } else {
      
        // The request is bad.
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
        exit();
        
    }

/**
 * Get term courses
 */
} elseif ($isfor == "get_term_courses") {

    $return = [
      "groupBySchool" => false,
      "data" => []
    ];

    $term = filter_input(INPUT_POST, "term", FILTER_SANITIZE_STRING);
    
    if (is_null($term)) {
        // No parameter passed, so it's a bad request.
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
        exit();
    } else {
       selectedTerm();
    }
 
    /**
     * If coupling courses with departments (HEE for example) get all courses
     */
    if ($config->db['couple_depts_courses']) {
        
        $return["groupBySchool"] = true;

        $schoolCourses = \OMIS\Database\CoreDB::group(
           $db->courses->getAllCourses($term),
           ['school_description'], true
        );

        foreach ($schoolCourses as $school => $courses) {
          
          foreach ($courses as $course) {

            $return["data"][$school][] = [
                 "id" => $course["course_id"],
                 "name" => $course["course_name"]
             ];
             
          }
         
        }
    
    } else {
    
        $courses = $db->courses->getCoursesYearsModules($term);

        foreach ($courses as $courseID => $course) {
            
            $return["data"][] = [
                "id" => $courseID, 
                "name" => $course[0]["course_name"]
            ];
            
        }
    
    }

/**
 *  Get Course Years & Modules
 */
} elseif ($isfor == "get_years_and_modules") {
    $course = filter_input(INPUT_POST, "course", FILTER_SANITIZE_STRIPPED);
    if (is_null($course)) {
        // No parameter passed, so it's a bad request.
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
        exit();
    }
    
    $modules = [];
    $years = [];

    // Does course exist
    if ($db->courses->doesCourseExist($course)) {
        $courseYearsDB = $db->courses->getCourseYears($course, $_SESSION["cterm"]);
        $modulesDB = $db->courses->getModulesByCourse($course, $_SESSION["cterm"]);

        // Course Years
        foreach ($courseYearsDB as $year) {
            $years[] = ["year_id" => $year["year_id"], "year_name" => $year["year_name"]];
        }

        // Modules
        foreach ($modulesDB as $module) {
            $modules[] = ["module_id" => $module["module_id"], "module_name" => $module["module_name"]];
        }
    }
    $return = [$years, $modules];
    
/**
 *  Get Modules Grouped by Years
 */
} elseif ($isfor == "get_modules_by_years") {
    $return = [];
    $courseID = filter_input(INPUT_POST, "course", FILTER_SANITIZE_STRIPPED);
    $courseYears = $_POST["years"];
    $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRIPPED);
    $examineeID = filter_input(INPUT_POST, "examinee", FILTER_SANITIZE_STRIPPED);
    $examineeExists = false;

    // If we are updating the examinee find out if they are linked to modules
    if ($action == "update" && $db->students->doesStudentExist($examineeID)) {
        $termID = filter_input(INPUT_POST, "term", FILTER_SANITIZE_STRIPPED);
        $examineeYearModules = $db->students->getStudentCourseData($examineeID, $courseID, $termID, "year_id");
        $examineeExists = true;
    }

       
    // Loop through course years
    foreach ($courseYears as $courseYear) {
       if ($db->courses->doesCourseYearExist($courseYear, $courseID)) {
           $yearModules = [];
           $yearDB = $db->courses->getCourseYear($courseYear);
           $yearModules["id"] = $courseYear;
           
           // Add year description
           $yearModules["description"] = gettext('Course Year') . " " . $yearDB["year_name"];
            
           // Add modules
           $modulesDB = $db->courses->getModulesByCourseYear($courseYear, "modules.module_id");

           // Form module data
           foreach ($modulesDB as $module) {
              
              $moduleID = $module["module_id"];
              $moduleName = $module["module_name"];
              
              // Module linked to examinee for this course year ?
              if ($examineeExists && array_key_exists($courseYear, $examineeYearModules)) {
                  $linkedToExaminee = array_key_exists($moduleID, $examineeYearModules[$courseYear]);
              } else {
                  $linkedToExaminee = false;
              }
               
              // Module data for each year
              $yearModules["modules"][] = [
                   "module_id" => $moduleID,
                   "module_name" => $moduleName,
                   "examinee_has" => $linkedToExaminee
              ];
              
           }

          $return[] = $yearModules;
       }
    }
    

/**
 *  Do the passed in examinee identifiers exist in the database? 
 */
} elseif ($isfor == "examinees_exist") {
    
    $return = [];
        
    if (isset($_POST["ids"])) {
      
     $examineeIDs = $_POST["ids"];
     
     // If is in fact an array
     if (is_array($examineeIDs)) {
        // Loop through identifiers
        foreach ($examineeIDs as $examineeID) {
            if ($db->students->doesStudentExist($examineeID)) {
                $return[] = ["examinee_id" => $examineeID];
            }
        }
     } else {
        // The request is bad.
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
        exit();
     }
 
   }
  
/**
 *  Are the passed in student candidate numbers already taken? 
 */   
} elseif ($isfor == "candidate_numbers_taken") {
    
    $return = [];
        
    if (isset($_POST["numbers"])) {
      
     $term = filter_input(INPUT_POST, "term", FILTER_SANITIZE_STRING);   
     $newCandidateNumbers = $_POST["numbers"];
     
     // If is in fact an array
     if (is_array($newCandidateNumbers)) {
        
       $currentCandidateNumbers = $db->students->getCandidateNumbers($term);

       foreach ($newCandidateNumbers as $new) {
          foreach ($currentCandidateNumbers as $current) {
             
             if ($new["candidateNumber"] == $current["candidate_number"] && $new["currentID"] != $current["student_id"]) {
                $return[] = $new;
             }
             
          }
      }
      
     } else {
        // The request is bad.
        http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
        exit();
     }
 
   }
}

try {
    $json = JSON::encode($return);
} catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($return, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
}

echo $json;
