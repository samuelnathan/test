<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use OMIS\Database\CoreDB as CoreDB;
use OMIS\Imaging\Cache as Cache;
use OMIS\Imaging\Image as Image;
use OMIS\Utilities\Path as Path;
use OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// User session variables
$userRole = $_SESSION["user_role"];
$userID = $_SESSION["user_identifier"];

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($userRole)->canAccess()) {
    return false;
}

// Required
require_once __DIR__ . "/../extra/helper.php";

// Student images feature enabled for the current user role ?
$studentImagesAllowed = $db->features->enabled("student-images", $userRole);

// Candidate number feature
$candidateNumberFeature = $db->features->enabled("candidate-number", $userRole);

// Students that have results attached / deletion prohibited
$examineesDeletionProhibited = [];
if (isset($_SESSION["students_with_results_warning"])) {
   $examineesDeletionProhibited = $_SESSION["students_with_results_warning"];
   unset($_SESSION["students_with_results_warning"]);
}

// Define the image cache...
$cache = new Cache($config->imagecache["caches"]["students"]);
$noimageFull = new Image($config->imagecache["images"]["default"]);
$badimageFull = new Image($config->imagecache["images"]["error"]);

// Define variables required later
$canShowList = false;
$examineeRecords = false;
$examinees = [];
$urlParams = "";
$examineeCount = 0;
$rowsPerPage = 40;
$pageNumber = 1;
$totalCount = 0;
$selectedYear = "";
$selectedModule = "";

// default index : students.updated_at
$defaultSortIndex = 9;

// Search string (surname, forenames or examinee identifier)
$searchString = filter_input(INPUT_GET, "s", FILTER_SANITIZE_STRING);

// Course selected
$selectedCourse = filter_input(INPUT_GET, "c", FILTER_SANITIZE_STRING);
if (strlen($selectedCourse) == 0) {
    $selectedCourse = $_SESSION["selected_course"];
}

/**
 * Check for selected course, year and module exists
 */
if ($db->courses->doesCourseExist($selectedCourse)) {
    
    $canShowList = true;
    $_SESSION['selected_course'] = $selectedCourse;

    // Year selected
    $yearParam = filter_input(INPUT_GET, 'y', FILTER_SANITIZE_NUMBER_INT);
    if (!is_null($yearParam) && (strlen($yearParam) == 0 || $db->courses->doesCourseYearExist($yearParam, $selectedCourse))) {
        $selectedYear = $yearParam;
        $_SESSION['selected_year'] = $selectedYear;
    } elseif (is_null($yearParam)) {
        $selectedYear = $_SESSION['selected_year'];
    }
   
    // Module selected
    $moduleParam = filter_input(INPUT_GET, 'm', FILTER_SANITIZE_STRING);
    if (!is_null($moduleParam) && (strlen($moduleParam) == 0 || $db->courses->doesModuleExist($moduleParam, $selectedYear))) {
        $selectedModule = $moduleParam;
        $_SESSION['selected_module'] = $selectedModule;
    } elseif (is_null($moduleParam)) {
        $selectedModule = $_SESSION['selected_module']; 
    }
     
    $urlParams = "&amp;c=" . $selectedCourse 
               . "&amp;y=" . $selectedYear
               . "&amp;m=" . $selectedModule
               . "&amp;s=" . $searchString;
    
    $db->userPresets->update([
        'filter_course' => $selectedCourse, 
        'filter_year' => $selectedYear,
        'filter_module' => $selectedModule,
    ]);
   
}

// Prepare Ordering
$orderHeaders = [
    "students.student_id",
    "students.surname",
    "students.forename",
    "students.dob",
    "students.gender",
    "students.nationality",
    "students.email",
    "course_years.year_name",
    "students.updated_at"
];

// Add column if feature enabled
if ($candidateNumberFeature) {
   array_splice($orderHeaders, 1, 0, "candidate_numbers.candidate_number");
   $defaultSortIndex++;
}

$url = "manage.php?page=manage_examinees"
     . "&amp;p=1"
     . "$urlParams";

$paramNames = [
   "index" => "i",
   "type" => "o"
];

list($orderType, $orderIndex, $orderMap) = prepareListOrdering("desc", $defaultSortIndex, $orderHeaders, $paramNames);

// Get Terms
$terms = $db->academicterms->getAllTerms();
$termParam = "t";
$selectedTerm = selectedTerm($termParam);

// Paging Code
if ($canShowList) {
    $totalCount = $db->students->searchStudentsTotalCount(
         $selectedCourse,
         $selectedYear,
         $selectedModule,
         $searchString,
         $selectedTerm
    );
       
    $maxPageCount = ceil($totalCount / $rowsPerPage);

    if ($totalCount == 0) {
        $offset = 0;
    } else {
        $pageNumberParam = filter_input(INPUT_GET, "p", FILTER_SANITIZE_NUMBER_INT);
        if (is_numeric($pageNumberParam)) {
            $pageNumber = min($maxPageCount, $pageNumberParam);
        }
        $offset = ($pageNumber - 1) * $rowsPerPage;
    }

    $examineeRecords = $db->students->searchStudents(
        $selectedCourse,
        $selectedYear,
        $selectedModule,
        $searchString,
        $offset,
        $rowsPerPage,
        $orderMap[$orderIndex],
        strtoupper($orderType),
        $selectedTerm
    );
    $examineeCount = mysqli_num_rows($examineeRecords);
    $urlParams .= "&amp;p=" . $pageNumber;
}

// If no listings
if (!$canShowList || $examineeCount == 0) {
    // Disable Ordering
    $orderIndex = null; 
    $maxPageCount = 1;
}

// Get Courses
$courses = $db->courses->getAllCourses($selectedTerm);

// Get Years By Course and Term
$years = $db->courses->getCourseYears($selectedCourse, $selectedTerm);

// Get Modules
if ($selectedYear == "") {
    $modules = $db->courses->getModulesByCourse($selectedCourse, $selectedTerm);
} else {
    $modules = $db->courses->getModulesByCourseYear($selectedYear, "modules.module_id");
}

// Column header titles
$columnHeaderTitles = [
    "Identifier",
    "Surname",
    "Forenames",
    "DOB",
    "Gender",
    "Nationality",
    "Email Address",
    gettext('Course Year')
];

// Add candidate column if feature enabled
if ($candidateNumberFeature) {
   array_splice($columnHeaderTitles, 1, 0, "Candidate #");       
}

// Gather all student data
while ($examinee = $db->fetch_row($examineeRecords)) {
    
    $examineeID = $examinee["student_id"];
    $nationality = $examinee["nationality"];
    $nationalityRecord = $db->iso->getISOCountryCode($nationality);
    $nationalityDescription = (is_array($nationalityRecord)) ? $nationalityRecord["iso_name"] : "";
    $dob = ($examinee["dob"] == "0000-00-00") ? "" : formatDate($examinee["dob"]);
   
    // Get examinee course years
    $examineeYears = $db->students->getStudentCourseYears(
            $examineeID,
            $selectedCourse,
            $selectedTerm
    );    
    
    // Get examinee modules
    $examineeModules = $db->students->getStudentModules(
            $examineeID,
            $selectedCourse,
            $selectedTerm
    );
    
    /*
     * Put together the list of modules that the student is linked to 
     * in a string to display in the hover message
     */
    $hoverModulesString = "";
    while ($eachModule = $db->fetch_row($examineeModules)) {
       $hoverModulesString .= $eachModule["module_id"] . ": " . $eachModule["module_name"] . "<br/>";
    } 
    $hoverModules = cleanout(($hoverModulesString == "") ? "&nbsp;" : $hoverModulesString);    
    $examineeModuleCount = mysqli_num_rows($examineeModules);
    $hoverName = cleanout($examinee["forename"] . " " . $examinee["surname"]);
   
    // Student image
    $studentImage = $examinee["student_image"];
    if ($studentImagesAllowed && (trim($studentImage) != "") && $cache->contains($studentImage)) {
        $imagePath = $studentImage;
    } else {
        /* This should always exist as the Cache's constructor should make
         * sure of it.
         */
        $imagePath = basename($config->imagecache["images"]["default"]);
    }

    /* Get the path to the thumbnail, generating it from the original if
     * necessary.
     */
     $thumbnailPath = $cache->getThumbnail($imagePath);

     // Make sure the student details know what's going on.
     $thumbnailSrc = Image::getImageAsInline($thumbnailPath);
     $imageSrc = Path::toURL($cache->get($imagePath));
     
     $studentHasResults = $db->results->doesStudentHaveResult($examineeID, null, null, null, null, true);
     $showHistoryIcon = ($db->features->enabled("student-history", $userRole) && $studentHasResults);
          
     // Modules link
     $modulesLink = "manage.php?page=update_examinees"
                  . "&amp;a=update" // action = update
                  . "&amp;e=".$examineeID
                  . "&amp;i=".$orderIndex
                  . "&amp;o=".$orderType
                  . $urlParams;
     
     $examinees[] = [
          "id" => $examineeID,
          "candidate_number" => $examinee["candidate_number"],  
          "surname" => $examinee["surname"],
          "forenames" => $examinee["forename"],
          "dob" => $dob,
          "gender" => $examinee["gender"],
          "nationality" => $nationality,
          "nationality_description" => $nationalityDescription,
          "email" => $examinee["email"],
          "image_src" => $imageSrc,
          "thumbnail_src" => $thumbnailSrc,
          "show_history" => $showHistoryIcon,
          "years" => implode(", ", $examineeYears),
          "modules_count" => $examineeModuleCount,
          "edit_link" => $modulesLink,
          "hover_modules" => $hoverModules,
          "hover_name" => $hoverName,
     ];
    
}

$coursesGrouped = $config->db['couple_depts_courses'] ? 
        CoreDB::group($courses, ['school_description'], true) : $courses;

// Template data
$data = [
   "terms" => $terms,
   "courses" => $coursesGrouped,
   "years" => $years,
   "modules" => $modules,
   "selectedTerm" => $selectedTerm,
   "selectedCourse" => $selectedCourse,
   "selectedYear" => $selectedYear,
   "selectedModule" => $selectedModule,
   "orderIndex" => $orderIndex,
   "orderType" => $orderType,
   "canShowList" => $canShowList,
   "searchString" => $searchString,
   "examinees" => $examinees,
   "url" => $url,
   "page_selected" => $pageNumber,
   "page_count" => $maxPageCount,
   "total_count" => $totalCount,
   "item_singular" => "student",
   "item_plural" => "students",
   "coupleDeptsCourses" => $config->db['couple_depts_courses'],
   "columnHeaderTitles" => $columnHeaderTitles,
   "examineesDeletionProhibited" => $examineesDeletionProhibited,
   "candidateNumberFeature" => $candidateNumberFeature
];
 
// Render the log entries using a template.
$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($data);

?>
