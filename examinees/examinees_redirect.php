<?php
/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 03/10/2016
 * © 2016 Qpercom Limited.  All rights reserved
 * Redirect file for Examinees (Students) section
 */

define("_OMIS", 1);

$redirectOnSessionTimeOut = true;
include __DIR__ . "/../extra/essentials.php";
require_once __DIR__ . "/../extra/helper.php";

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = [
        "logged_in" => true,
        "root_url" => $config->base_path_url,
        "username" => $_SESSION["user_identifier"],
        "page" => \OMIS\Utilities\Request::getPageID()
    ];
    $template->render($data);
    return;
}

/**
 * Go button action
 * Actions: Edit, delete examinees from system or remove them from a course 
 */
if (isset($_POST["go_examinee"])) {
   
   // Action to take
   if (in_array($_POST["todo"], ["delete", "remove_from_course"])) {
      
      // Delete examinees
      if ($_POST["todo"] == "delete") {
                    
         $db->students->deleteStudents($_POST["examinee_check"]);
         
      // Remove Examinees from course
      } elseif ($_POST["todo"] == "remove_from_course") {

         $db->students->removeStudentsFromCourse(
                     $_POST["examinee_check"],
                     $_POST["course"],
                     $_SESSION["cterm"]
         );
      }
        
      // Return URL
      $UL = "../manage.php?page=manage_examinees"
          . "&c=" . trim($_POST["course"])
          . "&y=" . trim($_POST["year"])
          . "&m=" . trim($_POST["module"])
          . "&p=" . trim($_POST["pagenr"])
          . "&s=" . trim($_POST["search"])
          . "&i=" . trim($_POST["order_index"])
          . "&o=" . trim($_POST["order_type"]);

    /**
     * Edit examinee records 
     */
    } elseif ($_POST["todo"] == "edit") {
              
      // Examinees to update
      $_SESSION["examinees_to_update"] = $_POST["examinee_check"];

      // Return URL
      $UL = "../manage.php?page=update_examinees"
          . "&a=update" // action is set to 'update'
          . "&c=" . trim($_POST["course"])
          . "&y=" . trim($_POST["year"])
          . "&m=" . trim($_POST["module"])
          . "&p=" . trim($_POST["pagenr"])
          . "&s=" . trim($_POST["search"])
          . "&i=" . trim($_POST["order_index"])
          . "&o=" . trim($_POST["order_type"]);
    }

    header("Location: " . $UL);
    
/**
 *  Add or update examinees records
 */
} elseif (isset($_POST["submit_examinees"])) {
    
    // Course ID
    $courseID = filter_input(INPUT_POST, "course", FILTER_SANITIZE_STRING);
    
    // Year
    $year = filter_input(INPUT_POST, "year", FILTER_SANITIZE_STRING);
    
    // Module
    $module = filter_input(INPUT_POST, "module", FILTER_SANITIZE_STRING);
    
    // Add or edit action
    $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);
    
    // Examinee data (array)
    $examineeData = $_POST["examinee"];
    
    // Get selected term
    $termParam = "t";
    $selectedTerm = selectedTerm($termParam);
    
    /*
     * Adding Examinees (Students) to the database
     */
    if ($action == "add") {
       
       // Loop through all new Examinees
       foreach ($examineeData as $examinee) {
         
         // Examinee date of birth
         $dobData = array_map("trim", [$examinee["year"], $examinee["month"], $examinee["day"]]);
         $examineeDOB = implode("-", $dobData);

         // Does student exist already? If so, do not continue         
         if (identifierValid($examinee["id"]) && !$db->students->doesStudentExist($examinee["id"])) {
            
            // Insert Examinee into database
            $examineeInserted = $db->students->insertStudent(
                 $examinee["id"],          // Examinee ID
                 $examinee["forenames"],    // Forenames
                 $examinee["surname"],     // Surname
                 $examineeDOB,             // Date of Birth
                 $examinee["gender"],      // Gender
                 $examinee["nationality"], // Nationality
                 $examinee["email"]        // Email
            );
            
           /** 
            * If new examinee record inserted then add course data (year and modules)
            * and candidate number, if any
            */
           if ($examineeInserted) {
                 
              // Grab course data
              $courseData = $examinee["course_data"];
              
              // Insert into database course data for the examinee 
              $examineeInsert = $db->students->insertStudentCourseData(
                  $examinee["id"],
                  $courseData
              );
              
              // Retrieve candidate number, if any
              $candidateNumber = isset($examinee["candidate"]) ? $examinee["candidate"] : "";
              
              // then insert candidate number
              $skipped = $db->students->insertCandidateNumbers(
                   [$examinee["id"] => $candidateNumber],
                   $selectedTerm
              );
              
              foreach ($skipped as $student => $candidate) {
                  error_log("Could not insert candidate number: $candidate for " .gettext("student") . " $student"); 
              }
             
           }
         }
      }
     
      // Back to the 'manage_examinees' page
      $UL = "../manage.php?"
          . "page=manage_examinees"
          . "&c=" . $courseID 
          . "&y=" . $year 
          . "&m=" . $module
          . "&p=1"
          . "&s=";

    // Update selected Examinees
    } elseif ($action == "update") {
                
       // Loop through each examinee to update
       foreach ($examineeData as $originalID => $examinee) {

          // Examinee date of birth
          $dobData = array_map("trim", [$examinee["year"], $examinee["month"], $examinee["day"]]);
          $examineeDOB = implode("-", $dobData);
          
          // Edited examinee ID
          $editedID = $examinee["id"];

          /*
           * Do not update if the examinee ID has changed and it's already
           * in use as an identifier for another examinee record
           */
          if (identifierValid($editedID) && !($editedID != $originalID && $db->students->doesStudentExist($editedID))) {
               
             $examineeDB = [
                "originalID" => $originalID,                // Original Examiner ID
                "id" => $editedID,                          // New Examiner ID
                "forenames" => $examinee["forenames"],      // Forenames
                "surname" => $examinee["surname"],          // Surname
                "dob" => $examineeDOB,                      // Date of Birth
                "gender" => $examinee["gender"],            // Gender
                "nationality" => $examinee["nationality"],  // Nationality
                "email" => $examinee["email"]               // Email
             ];
              
             // Update examinee in database
             $examineeUpdated = $db->students->updateStudent($examineeDB);
             
             // Examinee record updated? If so then update the examinee course information 
             if ($examineeUpdated) {
                 
                 // Clear old course data for examinee
                 $db->startTransaction();
                 $courseRemoved = $db->students->removeStudentCourseData(
                                                 $editedID,     // Examinee ID
                                                 $courseID,     // Course ID
                                                 $selectedTerm  // Current academic term
                 );
               
                // If course data was removed then add new data
                if ($courseRemoved) {
                   
                  // Grab course data
                  $courseData = $examinee["course_data"];
              
                  // Insert into database course data for the examinee 
                  $inserted = $db->students->insertStudentCourseData(
                                                 $editedID,   // Examinee ID
                                                 $courseData  // Course data
                  );
                  
                  // Retrieve candidate number, if any
                  $candidateNumber = isset($examinee["candidate"]) ? $examinee["candidate"] : "";
              
                  // then insert candidate number
                  $skipped = $db->students->insertCandidateNumbers(
                     [$examinee["id"] => $candidateNumber],
                     $selectedTerm
                  );
                  
                  foreach ($skipped as $student => $candidate) {
                    error_log("Could not update candidate number: $candidate for student: $student"); 
                  }
                 
                  if ($inserted) {
                    $db->commit();
                  } else {
                    $db->rollback();
                  }
                    
                } else {
                  $db->rollback(); 
                }

             }
           }
       }

       // Back to the 'manage_examinees' page
       $UL = "../manage.php?"
           . "page=manage_examinees"
           . "&c=" . trim($_POST["course"])
           . "&y=" . trim($_POST["year"]) 
           . "&m=" . trim($_POST["module"])
           . "&p=" . trim($_POST["pagenr"]) 
           . "&s=" . trim($_POST["search"]) 
           . "&i=" . trim($_POST["order_index"]) 
           . "&o=" . trim($_POST["order_type"]);
    }
    header("Location: " . $UL);
    
/**
 * If none of the above return to index page   
 */
} else { 
    header("Location: ../");
}
