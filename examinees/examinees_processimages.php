<?php
/* Process the uploaded ZIP, extract the files from it that seem to match our
 * student images, and then apply them in turn.
 */
// This file is only included, not invoked directly. Ensure it only runs then...
defined('_OMIS') or die('Restricted Access');

// Make sure we have a working autoloader without declaring it twice...
global $loader;
if (!isset($loader)) {
    $loader = require __DIR__ . '/../vendor/autoload.php';
}

require_once __DIR__ . "/../extra/usefulphpfunc.php";
use OMIS\Utilities\Path as Path;

// Check to see what happened with the upload, whether it's okay or not.
if (array_key_exists('data_file', $_FILES)) {
    $error_code = $_FILES['data_file']['error'];
    if ($error_code !== UPLOAD_ERR_OK) {
        switch($error_code) {
            case UPLOAD_ERR_INI_SIZE:
                $fail_reason = "File exceeds maximum file upload limit";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $fail_reason = "Uploaded file size exceeds maximum set on upload form.";
                break;
            case UPLOAD_ERR_PARTIAL:
                $fail_reason = "Uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $fail_reason = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $fail_reason = "Couldn't find a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $fail_reason = "Can't write temporary file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $fail_reason = "A PHP extension stopped the upload";
                break;
            default:
                $fail_reason = "Unknown error code $error_code returned";
        }
        die("Upload of file failed: $fail_reason");
    }
}

/**
 * Rather than write this code over and over, this is a sort of "error handler"
 * that, if an error has been identified, renders out the
 * 
 * @global string $error    Error Message string (if defined)
 * @global mixed  $log      A file handle pointing to an open log file (if it exists)
 * @global string $log_file Path to the open log file.
 * @param OMIS\Template $template Template object to render with the appropriate messages.
 */
function abortIfNeeded($template)
{
    global $error;
    global $log, $log_file;
    
    // Set up the data to be passed to the template.
    $data = array();
    
    // If there's been an error, abandon ship...
    if ($error != "") {
        $data['error_message'] = $error;
        
        /* If the log file is open, then close it (so that the contents are
         * definitely available) and make it available to the error message
         * template.
         */
        if (is_resource($log) && (get_resource_type($log) == 'file')) {
            fclose($log);
            $data['logfile'] = $log_file;
        }
        
        // Render the template and exit.
        $template->render($data);
        exit();
    }
}

/**
 * Write a message to the log file, generate an error message if this fails.
 * 
 * @global string $logfile Path to the log file.
 * @global string $error   Error message variable
 * @param resource $filehandle File handle for the log file.
 * @param string   $message    Message to write to the log.
 */
function log_message($filehandle, $message)
{
    printf("%s\n", $message);
    flush();
    
    global $logfile;
    global $error;
    $bytes = fwrite($filehandle, "$message\n");
    if ($bytes === false) {
        if ($error != '') {
            // Force a new line if needed.
            $error .= "\n";
        }
        $error .= "Failed to write log message to $logfile!";
    }
}

/* Detect if GZIP compression is enabled on this server and disable it for the
 * duration of the script as it'll mess up our ability to flush() output
 * messages.
 */
if (function_exists('ob_gzhandler') && ini_get('zlib.output_compression')) {
    /* I hate using @ error suppression but the fact is that this script will
     * still "work" even if this bit completely fails, but that the status
     * reporting will all happen at the end. It's not ideal but it's not a total
     * deal-breaker.
     */
    @ini_set('zlib.output_compression', 'Off');
    @ini_set('output_buffering', 'Off');
    @ini_set('output_handler', '');
    @apache_setenv('no-gzip', 1);
}

?>

<div class="contentdiv">
    <h3 class="v2">Attempting Image Import</h3>
    <p>Status messages will appear below.</p>
    <pre>
<?php
/* The list of MIME types that we know should mean a ZIP file. 
 * "application/octet-stream" is MIME-speak for "it looks like a binary file, 
 * otherwise no clue", so if we get that we need to further checks. Under unix
 * type systems, we can use GNU file and file magic numbers to do this job.
 */
$zip_mime_types = ['application/zip', 'application/x-zip-compressed'];

// Increase the maximum execution limit on this script, as it may take a while 
// to run.
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

// If $error is not an empty string, then something has gone wrong.
$error = "";

// Load the config settings if needed.
global $config;
if (!isset($config)) {
    $config = new OMIS\Config();
}

// Load the SQL functions class if needed.
global $db;
if (!isset($db)) {
    $db = CoreDB::getInstance();
}

// Set up Twig... or at least try to...
try {
    $error_template = new OMIS\Template("upload_error.html.twig");
} catch (Twig_Error $e) {
    $error_message = $e->getMessage();
    /* As the templating engine didn't instantiate properly, there's no point in
     * trying to use a template to render the error message.
     */
    die("Failed to instantiate templating. Error message was $error_message");
}

// Do some basic sanity checks before we try to open the ZIP file.
if (!isset($_FILES['data_file'])) {
    // No uploaded file.
    $error = "No file uploaded!";
} elseif (!in_array($_FILES['data_file']['type'], $zip_mime_types)) {
    /* If the MIME type isn't one we know absolutely to be a ZIP file, try to 
     * detect the MIME type in case it's wrong. Most often this will only get
     * used for "application/octet-stream", aka "No idea", situations.
     */
    $mime_type_detected = getFileMimeType($_FILES['data_file']['tmp_name']);
    $mime_type = ($mime_type_detected === \FALSE) ? $_FILES['data_file']['type'] : $mime_type_detected;
    
    error_log(__FILE__ . ": MIME type of " . $_FILES['data_file']['name'] . " is '$mime_type'");
    
    // Check (again) if the MIME type is now okay. If not, complain.
    if (!in_array($mime_type, $zip_mime_types)) {
        // File doesn't seem to be a ZIP file...
        $error = "Uploaded file (detected type '$mime_type') is not a ZIP file!";
    }
} elseif ($_FILES['data_file']['size'] < 22) {
    // According to Wikipedia, the shortest possible valid ZIP file is 22 bytes
    $error = "File is empty or too small to be a valid ZIP file";
}

// Check to see if an error message has been set. If so, abort.
abortIfNeeded($error_template);

/* If we get to here, then it's reasonable to assume that we have a ZIP file we
 * can at least try to open. So let's start a log and define some stuff...
 */
$images_used = array();
$images_unused = array();

$logfile = tempnam($config->tmp_path, "log");
$log = @fopen($logfile, "w");

if ($log === false) {
    $error = "Failed to open log file $logfile!";
} else {
    $now = date('l jS \of F Y h:i:s A');
    log_message($log, "Import started at $now (Server time)");
}

// Check to see if an error message has been set. If so, abort.
abortIfNeeded($error_template);

// Get details about the uploaded file.
$zip_path = $_FILES['data_file']['tmp_name'];
$zip_name = $_FILES['data_file']['name'];
$zip_size = formatFileSize($_FILES['data_file']['size'], null, true);

// Try to open the ZIP file
log_message($log, "Attempting to open uploaded ZIP Archive $zip_path ($zip_size)...");
try {
    $zip = new OMIS\Utilities\ZipFile($zip_path);
} catch (RuntimeException $e) {
    $error_message = $e->getMessage();
    $error = "Caught exception when opening Zip archive: $error_message";
    log_message($log, $error);
}

// Check to see if an error message has been set. If so, abort.
abortIfNeeded($error_template);

// Try to open the Image Cache...
try {
    $cache = new OMIS\Imaging\Cache($config->imagecache['caches']['students']);
} catch (RuntimeException $e) {
    $error_message = $e->getMessage();
    $error = "Failed to instantiate the student image cache: $error_message";
    log_message($log, $error);
}

// Check to see if an error message has been set. If so, abort.
abortIfNeeded($error_template);

// Opened the zip file. Woohoo.
$file_count = $zip->getFileCount();
log_message($log, "Opened ZIP successfully. Found $file_count files.\n");

// Find the list of image files.
$patterns = array("*.gif", "*.jpg", "*.jpe", "*.jpeg", "*.png");
$image_files = $zip->files($patterns);

// If there are no images in the ZIP, abort.
if (count($image_files) == 0) {
    $error = "No image files found in the ZIP file.";
    log_message($log, $error);
    abortIfNeeded($error_template);
} else {
    log_message($log, "Found " . count($image_files) . " image files.");
}

// Loop through the image files and try to import each one in turn.
foreach ($image_files as $image_file) {
    $student_id = pathinfo($image_file)['filename'];
    log_message($log, "Found image $image_file. Checking if there's a student with id $student_id");
    if ($db->students->doesStudentExist($student_id)) {
        log_message($log, "Student $student_id found in database. Attempting to add image.");
        // Build the full path to the image file as it should be extracted.
        $tempfile = Path::join($config->tmp_path, $image_file);
        
        // Extract the file to the temporary file name.
        if (!$zip->extract($config->tmp_path, $image_file)) {
            // File extraction failed. Crapola.
            $error = "Failed to export $image_file in Zip to $tempfile";
            log_message($log, $error);
            abortIfNeeded($error_template);
        }

        // Push the image into the cache.
        // @TODO Verify that the image file is in fact an image file.
        // @TODO Verify that the image file is smaller than the pre-ordained limit.
        if (!$cache->add($tempfile, basename($image_file))) {
            // Failed to add the image to the cache. Argh.
            $error = "Failed to add image $image_file (in temp file $tempfile) to student image cache";
            log_message($log, $error);
            abortIfNeeded($error_template);
        }

        // Assign the image to the student.
        if (!$db->students->updateStudentImage($student_id, basename($image_file))) {
            // Failed to assign the image to the student. Grr.
            $error = "Failed to assign image $image_file to examinee $student_id";
            log_message($log, $error);
            abortIfNeeded($error_template);
        }

        /* If we get to here, then the image was added okay. Log that and
         * move to the next image (if there is one). Also we need to delete
         * the temporary file.
         */
        log_message($log, "Image $image_file assigned to student $student_id\n");
        /* Not too worried if this works or not, it can always be cleaned up
         * periodically later...
         */
        unlink($tempfile);

        $images_used[] = $image_file;
    } else {
        // Record this image as not having been used.
        log_message($log, "Student $student_id NOT found in database. Skipping.\n");
        $images_unused[] = $image_file;
    }
}

// We're done. Close the log file.
$now = date('l jS \of F Y h:i:s A');
log_message($log, "Import finished at $now (Server Time)");
fclose($log);
?>
    </pre>
    <p><?=count($images_used);?> images imported successfully; <?=count($images_unused);?> not used.</p>
</div>
