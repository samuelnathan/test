<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 $termID = filter_input(INPUT_GET, 't', FILTER_SANITIZE_STRING);
 $courseID = filter_input(INPUT_GET, 'crs', FILTER_SANITIZE_STRING);

 $course = $db->courses->getCourse($courseID);
 $determinedTerm = $db->academicterms->existsById($termID) ? $termID : $_SESSION['cterm'];
 $courseYears = $yearModules = [];

 if ((!is_null($course))) {

    $courseYears = $db->courses->getCourseYears(
         $courseID,
         $determinedTerm
    );

    foreach ($courseYears as $year) {

      $yearID = $year['year_id'];
      $yearModules[$yearID] = $db->courses->getModulesByCourseYear(
             $yearID,
             'LENGTH(modules.module_id), modules.module_id'
      );

    }

 }

 $data = [
   'term' => $db->academicterms->getById($determinedTerm),
   'course' => $course,
   'years' => $courseYears,
   'yearModules' => $yearModules
 ];

 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($data);
