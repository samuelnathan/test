<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define('_OMIS', 1);

 $mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);
 if (!is_null($mode)) {
     include __DIR__ . '/../extra/essentials.php';
     if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
         return false;
     }
 } else {
     include __DIR__ . '/../extra/noaccess.php';
 }

 /**
  * Edit record mode
  */
 if ($mode == "edit"){
     $courseYearID = filter_input(INPUT_POST, 'crsyr');
     $yearRecord = $db->courses->getCourseYear($courseYearID);
     $modules = $db->courses->getModulesByCourseYear(
           $courseYearID,
           'LENGTH(modules.module_id), modules.module_id'
     );
     $yearName = $yearRecord['year_name'];

    // Course year record exists
    if (!is_null($yearRecord)) {
     ?>
     <td class="checkb">&nbsp;</td>
     <td>
       <input type="hidden" id="crsyrid" value="<?=$courseYearID?>"/>
       <input type="text" id="year_name" value="<?=$yearName?>" class="tf1"/>
     </td>
     <td class="tal opacedit">
     <div>
     <?php

      $template = new \OMIS\Template('year_modules_list.html.twig');
      $template->render([
         'yearID' => $courseYearID,
         'modules' => $modules
      ]);

     ?>
     </div>
     </td>
     <td class="titleedit2">&nbsp;</td>
     <?php
    } else {
         echo "CRSYR_NON_EXIST";
    }

 /**
  * Update record mode
  */
 } else if ($mode == "update") {

     $courseYearID = filter_input(INPUT_POST, 'crsyrid');
     $yearName = filter_input(INPUT_POST, 'year_name');

     if ($db->courses->doesCourseYearExist($courseYearID)) {
         $db->courses->updateCourseYear($courseYearID, $yearName);
     } else {
         echo "CRSYR_NON_EXIST";
     }

 /**
  * Add record mode
  */
 } else if ($mode == "add") {

     $courseYearID = filter_input(INPUT_POST, 'course_id');
     if ($db->courses->doesCourseExist($courseYearID)) {
      ?>
      <td class="checkb">&nbsp;</td>
      <td>
        <input type="text" id="add_yr" class="tf1"/>
      </td>
      <td class="spacetd">&nbsp;</td>
      <td class="titleedit">&nbsp;</td>
     <?php
    } else {
         echo "CRS_NON_EXIST";
    }

 /**
  * Submit record mode
  */
 } elseif ($mode == "submit") {

     $termID = filter_input(INPUT_POST, 'term', FILTER_SANITIZE_STRING);
     $courseID = filter_input(INPUT_POST, 'course_id');
     $year = filter_input(INPUT_POST, 'add_yr');

     if ($db->courses->doesCourseExist($courseID)) {

         $determinedTerm = $db->academicterms->existsById($termID) ? $termID : $_SESSION['cterm'];
         $db->courses->insertCourseYear($courseID, $year, $determinedTerm);

     } else {

         echo "CRS_NON_EXIST";

     }

 }
