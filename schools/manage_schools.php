<?php 
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */
 use \OMIS\Template as Template;
 use \OMIS\Database\CoreDB as CoreDB; 

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }

 require_once __DIR__ . '/../extra/helper.php';
 require_once 'schools_functions.php';

 if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SCHOOL_ADMIN) {
  $showEditFields = false;
  $schools = $db->schools->getAdminSchools($_SESSION['user_identifier'], false);

 } else {

  $showEditFields = true;
  $schools = $db->schools->getAllSchools();

 }

 $count = mysqli_num_rows($schools);

 $template_data = [
    'records' => []
 ];

 while ($school = $db->fetch_row($schools)) {
    $school_id = $school['school_id'];
    $departments = CoreDB::into_array($db->departments->getDepartmentsBySchool($school_id));
    $template_data['records'][] = [
        'school' => $school,
        'departments' => $departments
    ];
 }

 $template_data['showEditFields'] = $showEditFields;

 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($template_data);
