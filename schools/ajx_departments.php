<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('_OMIS', 1);
require_once __DIR__ . '/../extra/essentials.php';
require_once 'schools_functions.php';

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Template as Template;

// Grab the isfor parameter from the query. Should be a single lower-case word.
$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

// Check that the user is logged in and can access this page.
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
  
    return false;
    
}

switch (strtolower(trim($isfor))) {
  
    case 'edit':
        // Display form to edit the selected department.
        // Grab the department ID and verify that it's specified.
        $id = filter_input(INPUT_POST, 'dept');
        if (empty($id) || (trim($id) === '')) {
            returnJsonStatus("'dept' parameter not specified", HttpStatusCode::HTTP_BAD_REQUEST);
        }
        
        $dept_id = trim($id);
        
        // Check if the department exists...
        if (!$db->departments->existsById($dept_id)) {
            returnJsonStatus("Department '$dept_id' not found", HttpStatusCode::HTTP_NOT_FOUND);
        }
        
        // Load the template, populate the required data, and render it.
        $template = new Template('ajx_departments_edit.html.twig');
        $data = [
            'id'        => $dept_id,
            'details'   => $db->departments->getDepartment($dept_id, \TRUE)
        ];
        $template->render($data);
        
        break;
    case 'update':
        // Update a department's details.
        // Get the current ID of the department.
        $current_dept_id = filter_input(INPUT_POST, 'org_dept_id');
        $new_dept_id = filter_input(INPUT_POST, 'dept_id');
        $name = filter_input(INPUT_POST, 'dept_name', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        
        // Invalid Identifier, then abort
        if (!identifierValid($new_dept_id)) {
           error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
           exit;
        }
       
        // Check if the department to be updated exists...
        if (!$db->departments->existsById($current_dept_id)) {
            returnJsonStatus("Department '$current_dept_id' not found", HttpStatusCode::HTTP_NOT_FOUND);
        }
                
        // Now that we think that the department exists, try to update it.
        try {
            if ($db->departments->updateDepartment($new_dept_id, $current_dept_id, $name)) {
                returnJsonStatus("Department updated successfully");
            } else {
                /* Return an Internal Server Error if the update fails even
                 * though the update "looks" valid...
                 */
                returnJsonStatus("Update Failed", HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
            }
        } catch (\InvalidArgumentException $exception) {
            /* If the arguments are bad, then the change is "forbidden", so
             * we'll return a HTTP 403 along with the message.
             */
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_FORBIDDEN);
        } catch (Exception $exception) {
            /* Generic exception handler. Something we weren't expecting has
             * gone wrong. Deal with it as an internal server error.
             */
            error_log(__FILE__ . ": Update: " . $exception->getMessage());
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        break;
    case 'add':
        // Display template to define a new department.
        
        // Make sure we've selected a school before we continue.
        $school_id = filter_input(INPUT_POST, 'school_id', FILTER_VALIDATE_INT);
        if (!$db->schools->doesSchoolExist((int) $school_id)) {
            returnJsonStatus("Selected school id '$school_id' does not exist", HttpStatusCode::HTTP_NOT_FOUND);
        }
        
        // Display form to create new school
        (new \OMIS\Template('ajx_departments_add.html.twig'))->render();
        break;
    case 'submit':
        // Save a department we're trying to create to the database.
        
        // Check that a school has been selected and that it exists.
        $school_id = filter_input(INPUT_POST, 'school_id', FILTER_VALIDATE_INT);
        $dept_id = filter_input(INPUT_POST, 'add_id');
        
        // Invalid Identifier, then abort
        if (!identifierValid($dept_id)) {
           returnJsonStatus("Record Identifier invalid", HttpStatusCode::HTTP_FORBIDDEN); 
        }
        
        $name = htmlspecialchars_decode(filter_input(INPUT_POST, 'add_deptname', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
        
        // Attempt to insert the department.
        try {
            $success = $db->departments->insertDepartment($school_id, $dept_id, $name);
            returnJsonStatus("Department updated successfully");
        } catch (\InvalidArgumentException $exception) {
            /* If the arguments are bad, then the change is "forbidden", so
             * we'll return a HTTP 403 along with the message.
             */
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_FORBIDDEN);
        } catch (Exception $exception) {
            /* Generic exception handler. Something we weren't expecting has
             * gone wrong. Deal with it as an internal server error.
             */
            error_log(__FILE__ . ": Submit: " . $exception->getMessage());
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        break;
    default:
        // Just in case someone tries something weird.
        error_log(__FILE__ . ": Unknown 'isfor' parameter '$isfor'");
        returnJsonStatus("Invalid action '$isfor'", HttpStatusCode::HTTP_BAD_REQUEST);
        break;
}
