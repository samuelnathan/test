<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Database\CoreDB as CoreDB;

define('_OMIS', 1);
require_once __DIR__ . '/../extra/essentials.php';
require_once 'schools_functions.php';

// Grab the isfor parameter from the query. Should be a single lower-case word.
$isfor = filter_input(
    INPUT_POST,
    'isfor',
    FILTER_VALIDATE_REGEXP, 
    ['options' => ['regexp' => '/^[a-z]+$/']]
);

// If $isfor is false, null, or an empty string, then the request is bad.
if (empty($isfor) || (trim($isfor) === '')) {
    returnJsonStatus("Invalid action '$isfor'", HttpStatusCode::HTTP_BAD_REQUEST);
}

// Check that the user is logged in and can access this page.
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

switch (strtolower(trim($isfor))) {
    case 'edit':
        // Display form to edit existing school
        $id = filter_input(INPUT_POST, 'school_id', FILTER_VALIDATE_INT);
        
        // Check that the school exists.
        if (!$db->schools->doesSchoolExist($id)) {
            // 404 = Not Found. Seems right.
            returnJsonStatus("Specified School with ID '$id' not found", HttpStatusCode::HTTP_NOT_FOUND);
        }
        
        $data = [
            'school'        => $db->schools->getSchool($id),
            'departments'   => CoreDB::into_array($db->departments->getDepartmentsBySchool($id))
        ];
        $template = new \OMIS\Template('ajx_schools_edit.html.twig');
        $template->render($data);
        break;
    case 'update':
        // Update an existing school's description (i.e. name)
        $id = filter_input(INPUT_POST, 'school_id', FILTER_VALIDATE_INT);
        $description = trim(filter_input(INPUT_POST, 'school_desc', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
        if ($db->schools->doesSchoolExist($id)) {
            try {
                $success = $db->schools->updateSchool($id, $description);
            } catch (\InvalidArgumentException $exception) {
                /* If the arguments are bad, then the change is "forbidden", so we'll
                 * return a HTTP 403 along with the message.
                 */
                returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_FORBIDDEN);
            }
            
            if ($success) {
                returnJsonStatus("Update Successful");
            } else {
                /* Return an Internal Server Error if the update fails even
                 * though the school exists.
                 */
                returnJsonStatus("Update Failed", HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
            }
        } else {
            returnJsonStatus(
                "Specified School with ID '$id' not found",
                HttpStatusCode::HTTP_NOT_FOUND
            );
        }
        break;
    case 'add':
        // Display form to create new school
        (new \OMIS\Template('ajx_schools_add.html.twig'))->render();
        break;
    case 'submit':
        // Save new school's data to the database.
        $description = trim(filter_input(INPUT_POST, 'school_desc', FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));
        try {
            $success = $db->schools->insertSchool($description);
        } catch (\InvalidArgumentException $exception) {
            /* If the arguments are bad, then the change is "forbidden", so we'll
             * return a HTTP 403 along with the message.
             */
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_FORBIDDEN);
        } catch (Exception $exception) {
            // Generic exception handler. Something we weren't expecing has gone wrong.
            error_log(__FILE__ . ": Submit: " . $exception->getMessage());
            returnJsonStatus($exception->getMessage(), HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        if ($success) {
            returnJsonStatus("Insert Successful");
        } else {
            /* @TODO (DW) Replace this with better error checking and more
             * meaningful messages. For now, a fail is a fail.
             */
            returnJsonStatus("Update Failed", HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
        }
        break;
    default:
        // Just in case someone tries something weird.
        error_log(__FILE__ . ": Unknown 'isfor' parameter '$isfor'");
        returnJsonStatus("Invalid action '$isfor'", HttpStatusCode::HTTP_BAD_REQUEST);
        break;
}
