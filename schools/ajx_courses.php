<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define('_OMIS', 1);

 $mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);

 if (!is_null($mode)) {
     include __DIR__ . '/../extra/essentials.php';
     // Page Access Check / Can User Access this Section?
     if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
         return false;
     }
 } else {
     include __DIR__ . '/../extra/noaccess.php';
 }

 /**
  * Edit record mode
  */
 if ($mode == "edit") {
     $courseID = filter_input(INPUT_POST, 'crs');
     $courseRecord = $db->courses->getCourse($courseID);
     $moduleCounts = [];

     $years = $db->courses->getCourseYears(
          $courseID,
          $_SESSION['cterm']
     );

     foreach ($years as $year) {
        $yearID = $year['year_id'];
        $moduleCounts[$yearID] = $db->courses->getCourseYearModuleCount($yearID);
     }

     if (!is_null($courseRecord)) {
      ?><td class="checkb">&nbsp;</td>
      <td>
       <input type="text" id="edit_id" value="<?=$courseID?>" class="tf1 form-control form-control-sm"/>
       <input type="hidden" id="org_id" value="<?=$courseID?>"/>
      </td>
      <td>
        <input type="text" id="edit_name" value="<?=$courseRecord['course_name']?>" class="tf6 form-control form-control-sm"/>
      </td>
      <td class="tal opacedit">
       <div>
      <?php

       $template = new \OMIS\Template('course_years_list.html.twig');
       $template->render([
           'courseID' => $courseID,
           'years' => [$courseID => $years],
           'moduleCounts' => $moduleCounts
       ]);

      ?>
       </div>
   </td>
   <td class="titleedit2">&nbsp;</td>
 <?php
    } else {
       echo "CRS_NON_EXIST";
    }

 /**
  * Update record mode
  */
 } elseif ($mode == "update"){

    $originalID = filter_input(INPUT_POST, 'org_id');
    $courseID = filter_input(INPUT_POST, 'edit_id');
    $courseName = filter_input(INPUT_POST, 'edit_name');

    // Invalid Identifier, then abort
    if (!identifierValid($courseID)) {
        error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
        exit;
    }

    // If the course exists then update
    if ($db->courses->doesCourseExist($originalID)){
       echo $db->courses->updateCourse($courseID, $originalID, $courseName);
    }

 /**
  * Add record mode
  */
 } elseif ($mode == "add"){
     ?><td class="checkb">&nbsp;</td>
       <td><input type="text" id="add_id" class="tf1 form-control form-control-sm"/></td>
       <td><input type="text" id="add_crsname" class="tf6 form-control form-control-sm"/></td>
       <td>&nbsp;</td>
       <td class="titleedit">&nbsp;</td>
     <?php

 /**
  * Submit record mode
  */
 } elseif ($mode == "submit") {

     $courseID = filter_input(INPUT_POST, 'add_id');
     $courseName = filter_input(INPUT_POST, 'add_name');

     // Invalid Identifier, then abort
     if (!identifierValid($courseID)) {
        error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
        exit;
     }

     if (!$db->courses->doesCourseExist($courseID)) {
         $db->courses->insertCourse($courseID, $courseName);
     } else {
         echo "CRS_EXIST";
     }

 }
