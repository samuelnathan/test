<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

/**
 * Return a status/error message as JSON with the appropriate status code if 
 * required.
 * 
 * @param type $message
 * @param type $http_code
 */
function returnJsonStatus($message, $http_code = 200) {
  
    // Tell browser that the data is JSON
    header('Content-Type: application/json');
    
    // Encode and return the message
    echo json_encode(['message' => $message]);
    
    // If there's a code that isn't 200 ("OK" in HTTP), then include that too.
    if ($http_code !== 200) {
      
        http_response_code($http_code);
        
    }
    
    exit;
}
