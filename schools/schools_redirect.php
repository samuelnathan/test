<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('_OMIS', 1);
$redirectOnSessionTimeOut = true;
include __DIR__ . '/../extra/essentials.php';

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = [
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    ];
    $template->render($data);
    return;
}

if (isset($_POST['go_school'])) {
    //Delete Schools    
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->schools->deleteSchools($_POST['school_check']);
    }
    header('Location: ../manage.php?page=manage_schools');
} elseif (isset($_POST['go_dept'])) {
    //Delete Departments
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->departments->deleteDepartments($_POST['dept_check']);
    }
    $UL = '../manage.php?page=manage_departments&select=' . $_POST['selschool'];
    header('Location: ' . $UL);
} elseif (isset($_POST['go_crs'])) {
    //Delete Courses    
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->courses->deleteCourses($_POST['crs_check']);
    }
    header('Location: ../manage.php?page=manage_courses');
} elseif (isset($_POST['go_crsyr'])) {
    //Delete Course Years    
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->courses->deleteCourseYears($_POST['crsyr_check']);
    }
    header('Location: ../manage.php?page=manage_years&crs=' . $_POST['crs']);
} elseif (isset($_POST['go_modules'])) {
    //Delete Course Year Modules    
    if ($_POST['todo'] == "delete") { //get selected -- if delete
        $db->courses->deleteModules($_POST['cmod_check'], $_POST['crsyr']);
    }
    header('Location: ../manage.php?page=manage_modules&crsyr=' . trim($_POST['crsyr']));
} else {
    //If none of the above return to index page    
    header('Location: ../');
}
