<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
 
 use \OMIS\Database\CoreDB as CoreDB;
 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

    return false;

 }

 require_once __DIR__ . '/../extra/helper.php';
 
 $years = $moduleCounts = [];
 $courses = $db->courses->getAllCourses();
 $selectedTerm = selectedTerm('t');
  
 // Compile courses years and module counts
 foreach ($courses as $course) {
   
    $courseID = $course['course_id'];
    $years[$courseID] = $db->courses->getCourseYears(
         $courseID,
         $selectedTerm
    );
    
    foreach ($years[$courseID] as $year) {

       $yearID = $year['year_id'];
       $moduleCounts[$yearID] = $db->courses->getCourseYearModuleCount($yearID);

    }
    
 }
 
 /* If are coupling the courses with the schools
  * then we have a different view
  */
 if ($config->db['couple_depts_courses']) {
       
    $template = new Template('manage_school_courses.html.twig');
        
    $template->render([
       'terms' => $db->academicterms->getAllTerms(),
       'currentTerm' => $selectedTerm,
       'coursesGrouped' => CoreDB::group($courses, ['school_description'], true),
       'years' => $years,
       'moduleCounts' => $moduleCounts
    ]);
    
 } else {

    $template = new Template(Template::findMatchingTemplate(__FILE__));
    $template->render([
       'terms' => $db->academicterms->getAllTerms(),
       'currentTerm' => $selectedTerm,
       'courses' => $courses,
       'years' => $years,
       'moduleCounts' => $moduleCounts
    ]);
    
 }
 