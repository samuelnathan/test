<?php 
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
  use \OMIS\Template as Template;

  // Critical Session Check
  $session = new OMIS\Session;
  $session->check();

  // Page Access Check / Can User Access this Section?
  if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

    return false;

  }
  
  $yearID = filter_input(INPUT_GET, 'crsyr', FILTER_SANITIZE_NUMBER_INT);
  $year = $db->courses->getCourseYear($yearID); 
  $modules = $db->courses->getModulesByCourseYear(
          $yearID, 
          "LENGTH(modules.module_id), modules.module_id"
  ); 

  $template = new Template(Template::findMatchingTemplate(__FILE__));
  $template->render([
      'term' => $db->academicterms->getById($year['term_id']),
      'year' => $year,
      'modules' => $modules
  ]);
