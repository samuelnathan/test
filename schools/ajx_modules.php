<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 * To be twig templated
 */
 
define('_OMIS', 1);

$mode = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRING);
if (!is_null($mode)) {
    include __DIR__ . '/../extra/essentials.php';
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
        return false;
    }
} else {
    include __DIR__ . '/../extra/noaccess.php';
}
require_once 'schools_functions.php';

/**
 * Edit record mode
 */
if ($mode == 'edit') {
    $newmoduleID = filter_input(INPUT_POST, 'editid');
    $moduleRecord = $db->courses->getModule($newmoduleID);
    $moduleName = $moduleRecord['module_name'];
 if (!is_null($moduleRecord)) {
    ?><td class="checkb">&nbsp;</td>
      <td>
      <input type="text" id="editid" value="<?=$newmoduleID?>" maxlength="35" class="tf6 form-control form-control-sm"/>
      <input type="hidden" id="orgid" value="<?=$newmoduleID?>"/>
      </td>
      <td class="tdpadd2"><input type="text" id="editname" value="<?=$moduleName?>" class="tf6 form-control form-control-sm"/></td>
      <td class="titleedit2">&nbsp;</td>
    <?php
  } else {
     echo "MOD_NON_EXIST";
  }

/**
 * Update record mode
 */   
} elseif ($mode == "update") {
    
    $originalModuleID = filter_input(INPUT_POST, 'orgid');
    $newmoduleID = filter_input(INPUT_POST, 'editid');
    $moduleName = filter_input(INPUT_POST, 'editname');
    
    if ($db->courses->doesModuleExist($originalModuleID)) {
        
        // Invalid Identifier, then abort
        if (!identifierValid($newmoduleID)) {
             error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
             exit;
        }
        
        if ($newmoduleID != $originalModuleID && $db->courses->doesModuleExist($newmoduleID)) {
            echo "MOD_EXISTS_ALREADY";
        } else {
            $db->courses->updateModule($newmoduleID, $originalModuleID, $moduleName);
        }
    } else {
        echo "MOD_NON_EXIST";
    }
    
/**
 * Add record mode
 */
} elseif ($mode == "add") {
    $courseYearID = filter_input(INPUT_POST, 'crsyr');
    if ($db->courses->doesCourseYearExist($courseYearID)) {
    
        // Current Modules
        $currentYearModulesDB = $db->courses->getModulesByCourseYear(
                $courseYearID,
                "LENGTH(modules.module_id), modules.module_id"
        );
        $currentYearModules = array_column($currentYearModulesDB, 'module_id');
       
        //All Modules
        $modulesDB = $db->courses->getAllModules();
        
        ?><td class="checkb">&nbsp;</td>
		<td>
        
         <div id="new-module-div">
           <label for="add-id">New</label>
          <input type="text" id="add-id" class="tf6" maxlength="35"/>
         </div>
         
         <div id="existing-module-div">
          <label for="existing-module">Existing</label>
          <select id="existing-module">
           
           <optgroup label="New <?=gettext('Module')?> ">
             <option value="--">--</option>
           </optgroup>
           
           <optgroup label="Existing <?=gettext('Module')?> "><?php
           
           //Loop through all modules
           while($module = $db->fetch_row($modulesDB)) {
             
             //Check to see if module exists already in year and if not add to existing module list.
             if(!in_array($module['module_id'], $currentYearModules)) {
             
               ?><option value="<?=$module['module_id']?>" title="<?=$module['module_name']?>"><?=$module['module_id']?></option><?php
             
             }
           }
           
           ?></optgroup>
          </select>
         </div>
         
        </td>
		<td class="tdpadd2" id="module-name-field"><input type="text" id="add-name" class="tf6 form-control form-control-sm"/><span id="existing-module-name"></span></td>
		<td class="titleedit">&nbsp;</td>
	 <?php
   } else {
        echo "CRSYR_NON_EXIST";
    }
  
/**
 * Submit record mode
 */
} else if ($mode == "submit") {
    
    $courseYearID = filter_input(INPUT_POST, 'crsyr');
    $moduleID = filter_input(INPUT_POST, 'add_id');
    $existingModule = filter_input(INPUT_POST, 'existing_module');
    $moduleName = filter_input(INPUT_POST, 'add_name');
            
    // Does course year exist
    if ($db->courses->doesCourseYearExist($courseYearID)) {
                 
        // Inserting new module
        if ($existingModule == "--" && !$db->courses->doesModuleExist($moduleID)) {
        
          // Invalid Identifier, then abort
          if (!identifierValid($moduleID)) {
               error_log(__FILE__ . ": Invalid identifier entered. Aborting.");
               exit;
          }
            
          $db->courses->insertModule($moduleID, $moduleName, $courseYearID);
        
        // Attaching existing module
        } else if($existingModule != "--" && $db->courses->doesModuleExist($existingModule)) {
        
          $db->courses->attachModuleToYear($existingModule, $courseYearID);
        
        } else {
            echo "MOD_EXISTS";
        }
        
        
    } else {
        echo "CRSYR_NON_EXIST";
    }
}
