<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 use \OMIS\Database\CoreDB as CoreDB;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }

 // Get a list of schools, make sure that they exist.
 if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SCHOOL_ADMIN) {
  $schools = $db->schools->getAdminSchools($_SESSION['user_identifier'], false);
 } else {
  $schools = $db->schools->getAllSchools();
 }

 if (empty($schools)) {
    die(
       "No " . gettext('schools'). " set up on this system. Please create a " . 
       gettext('schools') . "."
    );
 }

 // Get the selected school.
 $selectedSchoolParam = filter_input(
    INPUT_GET,
    'select',
    FILTER_SANITIZE_STRING,
    ['flags' => FILTER_FLAG_STRIP_LOW + FILTER_FLAG_STRIP_HIGH]
 );

 if (!is_null($selectedSchoolParam)) {
    $selectedSchool = $_SESSION['selected_school'] = $selectedSchoolParam;
 } elseif (isset($_SESSION['selected_school'])) {
    $selectedSchool = $_SESSION['selected_school'];
 } else {
    $selectedSchool = "";
 }

 if (!empty($selectedSchool) && $db->schools->doesSchoolExist($selectedSchool)) {
    $departments = CoreDB::into_array(
         $db->departments->getDepartmentsBySchool($selectedSchool)      
    );
    $db->userPresets->update(['filter_school' => $selectedSchool]);
 } else {
    $departments = [];
    $_SESSION['selected_school'] = "";
 }

 $template = new \OMIS\Template(\OMIS\Template::findMatchingTemplate(__FILE__));
 $template->render([
    'schools' => CoreDB::into_array($schools),
    'selected_school_id' => $selectedSchool,
    'departments' => $departments
 ]);
