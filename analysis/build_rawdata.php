<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use \OMIS\Database\Results as Results;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 // Initiate outcome service
 $outcomesService = new \OMIS\Outcomes\OutcomesService($db);

 $examinerMarks = $rawResults = $rawTitles = [];
 $statusCounts = [
     'failed' => 0,
     'incomplete' => 0,
     'passed' => 0
 ];

 $areResults = $areSubmittedResults = false;
 $studentCounter = 0;

 // Get unique station ids
 $uniqueStationIDs = array_keys($allStationsData);

 // Loop through student list
 foreach ($studentList as $eachStudent) {

     $eachStudentRow = [];
     $studentHasResult = $studentHasAbsent = false;
     $eachStudentRow['studentid'] = $studentID = $eachStudent['student_id'];
     $numStationsFailed = $grsFailCount = 0;

     // Student outcome
     $outcome = $outcomesService->getOutcomesForStudentAndExam($studentID, $examIDCurrent);

     if ($displayalternativenum) {
 
         if ($settings['exam_student_id_type'] == EXAM_NUMBER_TYPE) {
 
           $eachStudentRow['altnumber'] = $eachStudent['student_exam_number'];
 
         } elseif ($settings['exam_student_id_type'] == CANDIDATE_NUMBER_TYPE) {
 
           $eachStudentRow['altnumber'] = $eachStudent['candidate_number'];
 
         } else {
 
           $eachStudentRow['altnumber'] = "";
 
         }

     }

     if ($displaystudentname) {
 
         $eachStudentRow['surname'] = $eachStudent['surname'];
         $eachStudentRow['forename'] = $eachStudent['forename'];
 
     }
 
     if ($displaynationality)
         $eachStudentRow['nationality'] = $eachStudent['nationality'];
 
     if ($displaygroups)
         $eachStudentRow['group'] = $eachStudent['group_name'];
 
     if ($displayorder)
         $eachStudentRow['order'] = ++$studentCounter;
 
     if ($displaysession) {
 
         $eachStudentRow['date'] = $sessionsData[$eachStudent['session_id']]['session_date'];
         $eachStudentRow['session'] = $sessionsData[$eachStudent['session_id']]['session_description'];
 
     }
     
     if ($displaynotes)
         $eachStudentRow['notes'] = isset($outcome) ? $outcome['notes'] : "";

     // Overall grade source
     $eachStudentRow['overallgrade'] = strtolower($outcome['grade']);

     // Grade trigger if requested
     if (($displaygrade && $examHasGradeRules) || $displayweighted) {

         $ruleTrigger = strlen($outcome['gradeTrigger']) > 0 ? " : " . $outcome['gradeTrigger'] : "";
         $eachStudentRow['trigger'] = (strlen($outcome['grade']) > 0 && empty($outcome['gradeAuthor'])) ?
                 "system" . $ruleTrigger : $outcome['gradeAuthor'];

     }

     // Overall total and failed columns
     $eachStudentRow['overallscore'] = "NA";

     // Display weighted
     if ($displayweighted)
         $eachStudentRow['overallweighted'] = "NA";
 
     $eachStudentRow['overallpercent'] = "NA";

     // Assessments Failed
     if ($displaystationsfailed)
         $eachStudentRow['failed'] = 0;

     // Get Student Exam Data
     $studentResults = $db->results->getStudentResultsData(
         $studentID,
         $eachStudent['session_id'],
         $cnm
     );

     foreach ($stationIdentifiers as $id) {

         // Viewpoint outcome
         $vpOutcome = $outcome['stations'][$id];

         // Maximum number of multiple examiners per station
         $multiMax = $stationMultiExaminerCount[$id];

         // Defaults for station number, tag and scenario/form ID
         $eachStudentRow['stationid_' . $id] = "NA";
         $eachStudentRow['stationnumber_' . $id] = "NA";
         $eachStudentRow['stationcode_' . $id] = "NA";
         $eachStudentRow['stationform_' . $id] = "NA";
         
         // Station Percent, weighting & Passed
         $eachStudentRow['stationscore_' . $id] = "NA";
 
         if ($displayweighted)
             $eachStudentRow['stationweighting_' . $id] = (isset($vpOutcome['adjusted_score'])) ?
                 roundAndFormat($vpOutcome['adjusted_score']) : "NA";

         $eachStudentRow['stationpercent_' . $id] = "NA";
         $eachStudentRow['stationpassed_' . $id] = "NA";
              
         $eachStudentRow['stationgrsfail_' . $id] = 0;
         $eachStudentRow['stationdivergence_' . $id] = 0;
         $divergenceValues = [];

         // Station has result
         $stationHasResult = false;

         // Each station absent count
         $stationAbsentCount = 0;

         // Loop through multi results
         for ($multiCount=1; $multiCount<=$multiMax; $multiCount++) {

             // Result Index
             $multiIndex = $multiCount - 1;

             // Multi Result ID
             $multiResultID = "_" . $id . "_" . $multiCount;

             // Defaults for each station
             $eachStudentRow['examinerscore' . $multiResultID] = "NA";
             $eachStudentRow['examinerpercent' . $multiResultID] = "NA";
 
             $eachStudentRow['examinerpassed' . $multiResultID] = "NA";

             if ($displayexaminers)
                 $eachStudentRow['examinerid' . $multiResultID] = "NA";
             
             if ($displaygrs) {
               
                 $eachStudentRow['grs' . $multiResultID] = "NA";
                 $eachStudentRow['failedgrs' . $multiResultID] = "NA";
                 
             }
             
             if ($displayflags)
                 $eachStudentRow['flags' . $multiResultID] = "NA";
             
             if ($displayfeedback)
                 $eachStudentRow['feedback' . $multiResultID] = "NA";
 
             if ($displaytimestamp)
                 $eachStudentRow['time' . $multiResultID] = "NA";
             
             // Assessment form completed (1) or incomplete (0) 
             $eachStudentRow['completed' . $multiResultID] = "NA";
             
             // Result ID
             $eachStudentRow['stationresultid' . $multiResultID] = "NA";
             
             // Does student have result ?
             if (isset($studentResults[$id], $studentResults[$id][$multiIndex])) {

                 $resultData = $studentResults[$id][$multiIndex];
                 $resultID = $resultData['result_id'];
                 $stationInfo = $allStationsData[$resultData['station_id']];
                 
                 /* 
                  * Determine max obtainable station score for a mult-examiner setup 
                  * i.e. 1 station with 2 or more examiners using different forms.
                  * This should never be the case as each form in a multi-examiner
                  * setup should have the same maximum obtainable score
                  */
                       
                 $passValue = $stationInfo['pass_value'];
                 $weightingValue = $stationInfo['weighting'];
                 
                 $formID = $stationInfo['form_id'];
                 $examinerID = $resultData['examiner_id'];
                 $resultIsComplete = $resultData['is_complete'];
                 $formPossible = $resultData['possible'];

                 // Station ID, Number, Tag & form
                 $eachStudentRow['stationid_' . $id] = $resultData['station_id'];
                 $eachStudentRow['stationnumber_' . $id] = $stationInfo['station_number'];
                 $eachStudentRow['stationcode_' . $id] = $stationInfo['station_code'];
                 $eachStudentRow['stationform_' . $id] = $stationInfo['form_name'];
                 
                 // Student is Absent
                 if ($resultData['absent'] == 1) {
                   
                     $eachStudentRow['examinerscore' . $multiResultID] = "ABSENT";
                     $eachStudentRow['examinerpercent' . $multiResultID] = "ABSENT";
                     $studentHasAbsent = true;
                     $stationAbsentCount++;

                 // Student is Present
                 } else {

                     /**
                      * Examiner Variables, see examiner_analysis.php page
                      */
                     if ($displayexaminers && $resultIsComplete) {

                         // Preset "examinerMarking" Array
                         $avgID = $examinerID.$id;
                         setA($examinerMarks, $avgID, [
                               'examinerID' => $examinerID,
                               'combineID' => $id,
                               'score' => 0,
                               'possible' => 0,
                               'pass' => 0,
                               'count' => 0,
                               'sessionGroups' => []
                         ]);

                         // Score, Possible. Total pass number of results
                         $examinerMarks[$avgID]['score'] += $resultData['score'];
                         $examinerMarks[$avgID]['possible'] += $formPossible;
                         $examinerMarks[$avgID]['pass'] += $passValue;
                         $examinerMarks[$avgID]['count'] += 1;

                         // Accumulate session and group scores
                         $exSessionID = $eachStudent['session_id'];
                         $exGroupID = $eachStudent['group_id'];
                         setA($examinerMarks[$avgID]['sessionGroups'], $exSessionID, []);
                         setA($examinerMarks[$avgID]['sessionGroups'][$exSessionID], $exGroupID, [
                             'score' => 0,
                             'possible' => 0,
                             'pass' => 0,
                             'count' => 0
                         ]);

                         $exSession = &$examinerMarks[$avgID]['sessionGroups'][$exSessionID][$exGroupID];
                         $exSession['score'] += $resultData['score'];
                         $exSession['possible'] += $formPossible;
                         $exSession['pass'] += $passValue;
                         $exSession['count'] += 1;

                     }

                     // Submitted results
                     if ($resultIsComplete) {

                        $areSubmittedResults = true;
                        $divergenceValues[] = (float) $resultData['score'];

                     } else {

                        $statusCounts['incomplete']++;

                     }

                     // There is at least 1 result (can be incomplete)
                     $areResults = true;

                     // Student has result
                     $studentHasResult = true;

                     // Station has result
                     $stationHasResult = true;

                     // Station has result
                     $formsFiltersMap[$formID]['has_result'] = true;

                     // Score percentage calculation
                     $scorePercentage = percent($resultData['score'], $formPossible);

                     // Set Examiner score and percentage
                     $eachStudentRow['examinerscore' . $multiResultID] = roundAndFormat($resultData['score']);
                     
                     $eachStudentRow['examinerpercent' . $multiResultID] = $scorePercentage;
 
                     /** Pass/fail Record **************************************/
                     if ($resultData['passed'] == 1) {
                       
                         $eachStudentRow['examinerpassed' . $multiResultID] = "pass";
                         
                         if ($resultIsComplete) $statusCounts['passed']++;
                         
                     } else {
                       
                         $eachStudentRow['examinerpassed' . $multiResultID] = "fail";
                         
                         if ($resultIsComplete) $statusCounts['failed']++;
                         
                     }

                    // Red flags count per result
                    if ($displayflags)
                        $eachStudentRow['flags' . $multiResultID] = $resultData['flag_count'];
                     
                     
                 }

                 // Examiner ID Value
                 if ($displayexaminers)
                     $eachStudentRow['examinerid' . $multiResultID] = $examinerID;

                 // Global Rating Scale (Clear Fails Only)
                 if (isset($grsData[$resultID])) {
                   
                     if ($grsData[$resultID]['fail'] == 1 &&
                         $grsData[$resultID]['borderline'] == 0) {
                       
                         $grsFailCount++;
                         $eachStudentRow['stationgrsfail_' . $id] = 1;
                         
                     }

                     if ($displaygrs) {
                       
                         $eachStudentRow['grs' . $multiResultID]
                                 = $grsData[$resultID]['descriptor'];

                         $eachStudentRow['failedgrs' . $multiResultID]
                                 = $grsData[$resultID]['fail'];
                         
                     }

                 }

                 // Examiner Feedback Column
                 if ($displayfeedback && isset($feedbackData[$resultID])) {
                   
                     $eachStudentRow['feedback' . $multiResultID]
                         = $feedbackData[$resultID]['item_feedback']
                         . " " . $feedbackData[$resultID]['section_feedback'];
                     
                 }

                 // Submission Timestamp Value
                 if ($displaytimestamp) {
                   
                     $eachStudentRow['time' . $multiResultID] = date_format(
                         date_create($resultData['created_at']),
                         "Y/m/d H:i:s"
                     );

                 }
                 
                 // Assessment form is completed (1) or incomplete (0) 
                 $eachStudentRow['completed' . $multiResultID] = $resultIsComplete;
                 
                 // Station Result ID Value
                 $eachStudentRow['stationresultid' . $multiResultID] = $resultID;
                 
             }

         }

         // Each Accumulated/formulated Station Score
         // Pass Station Totals
         if ($stationHasResult) {

             $finalStationScore = $vpOutcome['score'];
             $stationPossible = $vpOutcome['possible'];

             // Divergence calculation per station
             $eachStudentRow['stationdivergence_' . $id] = (int) Results::exceedsDivergenceThreshold(
                     $divergenceValues, 
                     $rules['divergence_threshold']
             );
             
             $eachStudentRow['stationscore_' . $id] = roundAndFormat($finalStationScore);
             
             // Final Station Percent
             $eachStudentRow['stationpercent_' . $id] = percent($finalStationScore, $stationPossible);

             // Stations Passed/Failed
             if ($vpOutcome['passed'] == 1) {

                 // Passed the station
                 $eachStudentRow['stationpassed_' . $id] = "pass";
                 
             } else {
               
                 $eachStudentRow['stationpassed_' . $id] = "fail";
                 $numStationsFailed++;

             }

         // Only 1 result but it is absent
         } elseif ($multiMax == $stationAbsentCount) {

            $eachStudentRow['stationscore_' . $id] = "ABSENT";

         }

     }

     // Has Result - Calculate Total% and add failed count
     if ($studentHasResult) {

         // Overalls
         $studentTotal = $outcome['rawScore'];
         $studentPossible = $outcome['possibleScore'];
         $eachStudentRow['overallscore'] = roundAndFormat($studentTotal);

         // Station weightings (not to be confused with examiner weightings)
         if ($displayweighted)
             $eachStudentRow['overallweighted'] = roundAndFormat($outcome['adjustedScore']);
 
         // Overall Percent and Grade
         $eachStudentRow['overallpercent'] = percent($studentTotal, $studentPossible);
 
         if ($displaystationsfailed)
             $eachStudentRow['failed'] = $numStationsFailed;

         $passedOverall = (isset($outcome['grade'])) ? $outcome['grade'] : "";

         // If Passed Overall / How many stations failed / for Graph stations_failed.php
         if ($displaystationsfailed && $passedOverall == "pass" && $numStationsFailed != 0) {
           
             if (isset($failedGraphData['failed_count'][$numStationsFailed])) {

                 $failedGraphData['failed_count'][$numStationsFailed] += 1;
                 
             } else {
               
                 $failedGraphData['failed_count'][$numStationsFailed] = 1;
                 
             }

         }

     } elseif ($studentHasAbsent) {
 
        $eachStudentRow['overallgrade'] = "ABSENT";
        $eachStudentRow['overallscore'] = "ABSENT";
        $eachStudentRow['overallweighted'] = "ABSENT";
        $eachStudentRow['overallpercent'] = "ABSENT";
          
     }

     // Add student row to array
     $rawResults[] = $eachStudentRow;

 }
