<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use \OMIS\Template as Template;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;

 $data = $formStationIDs =
 $formStationData = $gradeData = [];

 // Display options
 $displayalternativenum = (isset($_POST['displayalternativenum']) && $_POST['displayalternativenum'] == 1);
 $displaystudentname = (isset($_POST['displaystudentname']) && $_POST['displaystudentname'] == 1);
 $displaynationality = (isset($_POST['displaynationality']) && $_POST['displaynationality'] == 1);
 $displaygroups = (isset($_POST['displaygroups']) && $_POST['displaygroups'] == 1);
 $displayorder = (isset($_POST['displayorder']) && $_POST['displayorder'] == 1);
 $displaysession = (isset($_POST['displaysession']) && $_POST['displaysession'] == 1);
 $displayexaminers = (isset($_POST['displayexaminers']) && $_POST['displayexaminers'] == 1);
 $displaytimestamp = (isset($_POST['displaytimestamp']) && $_POST['displaytimestamp'] == 1);
 $displayweighted = (isset($_POST['displayweighted']) && $_POST['displayweighted'] == 1);

 // Selected form
 $selectedForm = (isset($_POST['sel'])) ? trim($_POST['sel']) : "";
 $selectedGroups = explode(',', $_POST['grps']);

 ?><div id="resdetdv"><?php

    // Do we have Groups, if not inform user
    if (empty($selectedGroups) || !$db->groups->doGroupsExist($selectedGroups)) {

        $template = new Template("standalone_error.html.twig");
        $template->render(["message" => "The groups selected do not exist"]);
        exit;

    }

    // Get selected groups related data
    list($groupData, $sessionData, $examRecord,
        $settingData) = $db->groups->getData($selectedGroups);

    $sessionIDs = array_column($sessionData, 'session_id');

    // Form doesn't exist, then abort
    if (empty($selectedForm) || !$db->forms->doesFormExist($selectedForm)) {

        $template = new Template("standalone_error.html.twig");
        $template->render(["message" => "The assessment " . gettext('form') . " selected does not exist"]);
        exit;

    }

    // Form doesn't have result, then abort
    if (!$db->results->doesFormHaveResult($selectedForm, $selectedGroups, $sessionIDs)) {

        $template = new Template("standalone_error.html.twig");
        $template->render(["message" => "The assessment " . gettext('form') . " selected has no results"]);
        exit;

    }

    // Retrieve form data
    $formData = $db->forms->getForm($selectedForm);

    // Has weighted items
    $formWeighting = ($displayweighted && $db->forms->hasWeightedItems($selectedForm));

    // Get form station info
    $formStations = $db->forms->getFormStations($selectedForm, $sessionIDs);

    // Loop through form station info
    while ($eachStation = $db->fetch_row($formStations)) {

        $formStationIDs[] = $eachStation['station_id'];
        $formStationData[$eachStation['station_id']] = ['pass_value' => $eachStation['pass_value']];

    }

    // Empty cell count
    $emptyCellsCount = count(array_filter([
        $displayalternativenum, $displaystudentname,
        $displaystudentname, $displaynationality,
        $displaygroups, $displayorder, $displaysession,
        $displayexaminers, $displaytimestamp, $formWeighting]
    )) + 3;


    // Loop through Sections
    $formSections = $db->forms->getFormSections([$selectedForm], true);
    $itemNum = 1;
    foreach ($formSections as &$section) {

        $items = $db->forms->getSectionItems([$section['section_id']], true);
        $section['items'] = [];
        $subColumns = 0;

        if (count($items) == 0)
            continue;

        $section['isScale'] = $db->forms->doesSectionHaveScaleItem($section['section_id']);

        foreach ($items as &$item) {

            // True low and high values
            $item['trueHigh'] = (float)max($item['highest_value'], $item['lowest_value']);
            $item['trueLow'] = (float)min($item['highest_value'], $item['lowest_value']);

            // If item id of type Radio or Text or Slider (scoring)
            if (in_array($item['type'], ['radio', 'text', 'slider'])) {

                $item['scoring'] = true;
                $item['number'] = $itemNum++;
                $item['cols'] = (1 + (int)$formWeighting);
                $subColumns += $item['cols'];

                // If item is of type radiotext | checkbox (non-scoring)
            } elseif (in_array($item['type'], ['radiotext', 'checkbox'])) {

                $item['scoring'] = false;
                $item['cols'] = 1;
                $subColumns++;

            }

            // Self assessment question
            if ($section['self_assessment']) {

                $question = $db->selfAssessments->getQuestion($examRecord['exam_id'], $item['item_order']);
                $item['text'] = (empty($question) ? "No self assessment question data found" : $question['question']);

            }

        }

        // Pack items into section data
        $section['items'] = $items;
        $section['cols'] = $subColumns;

    }

    // Header titles
    $titles = ['Identifier'];

    // If Alternative student identifier
    if ($displayalternativenum)
        $titles[] = "Number";

    // If Student Order
    if ($displayorder)
        $titles[] = "Order";

    // If Student Names
    if ($displaystudentname) {
        $titles[] = "Surname";
        $titles[] = "Forenames";
    }

    // If Student Nationality
    if ($displaynationality)
        $titles[] = "Nationality";


    // If Group Names
    if ($displaygroups)
        $titles[] = "Group";


    // If Day/Circuit
    if ($displaysession)
        $titles[] = "Day/Circuit";

    // If Examiner ID
    if ($displayexaminers)
        $titles[] = gettext('Examiner');


    // If First Submission
    if ($displaytimestamp)
        $titles[] = "Submitted";


    $titles[] = gettext('Examiner') . " Total";
    $titles[] = gettext('Examiner') . " Total %";


    // Weighting title
    if ($formWeighting)
        $titles[] = "Weighting Total";

     // Get students with results
     $studentsResults = $db->results->getResultsByStationsGroups(
         $formStationIDs,
         $selectedGroups,
         NULL,
         "students.student_id",
         "ASC",
         $examRecord['term_id']
     );

    // Go through student lists
    while ($result = $db->fetch_row($studentsResults)) {

        $student = [];
        $student[] = $result['student_id'];
        $resultID =  $result['result_id'];

        // If Alternative student identifier
        if ($displayalternativenum) {

            if ($settingData['exam_student_id_type'] == EXAM_NUMBER_TYPE) {

                $student[] = $result['student_exam_number'];

            } elseif ($settingData['exam_student_id_type'] == CANDIDATE_NUMBER_TYPE) {

                $student[] =  $result['candidate_number'];

            } else {

                $student[] = $studentID;

            }

        }

        // If Student Order
        if ($displayorder) {

            $student[] = $result['student_order'];

        }

        // If Student Names
        if ($displaystudentname) {

            $student[] = $result['surname'];
            $student[] = $result['forename'];

        }

        // If Student Nationality
        if ($displaynationality) {

            $student[] = $result['nationality'];

        }

        // If Group Names
        if ($displaygroups) {

            $student[] = $groupData[$result['group_id']]['group_name'];

        }

        // If Session
        if ($displaysession) {

            $sessionID = $groupData[$result['group_id']]['session_id'];
            $student[] = $sessionData[$sessionID]['dateDesc'];

        }

        // If Examiner ID
        if ($displayexaminers) {

            $student[] = $result['examiner_id'];

        }

        // If First Submission
        if ($displaytimestamp) {

            $student[] = date_format(date_create($result['created_at']), 'H:i:s');

        }

        // Station Total Score
        $student[] = (float)$result['score'];

        // Score Percentage
        $student[] = percent($result['score'], (float)$formData['total_value']);

        // Weighting title
        if ($formWeighting)
            $student[] = roundAndFormat($result['weighted_score'], 2);

        // Get Item Values
        $itemValues = $db->results->getItemScoresPerResult(
            $resultID, true, false, true
        );

        // Loop through station items
        foreach ($formSections as &$section) {

            foreach ($section['items'] as &$item) {

                $itemID = $item['item_id'];
                $itemValue = isset($itemValues[$itemID]['option_value']) ? $itemValues[$itemID]['option_value'] : '';

                $student[] = $itemValue;

                if ($formWeighting && in_array($item['type'], ['radio', 'text', 'slider'])) {
                    $student[] = roundAndFormat($itemValues[$itemID]['option_weighted_value'], 2);
                }

            }

        }

        // If we have a completed result then figure out if it is pass or fail
        if ($result['is_complete'] == 1) {

            $gradeData[$result['student_id']] = $result['passed'];

        }

        $data[] = $student;

    }


    (new Template(Template::findMatchingTemplate(__FILE__)))->render([
        'formWeighting' => $formWeighting,
        'formName' => $formData['form_name'],
        'formSections' => $formSections,
        'titles' => $titles,
        'data' => $data,
        'gradeData' => $gradeData,
        'emptyCellCount' => $emptyCellsCount
    ]);

 ?></div>
