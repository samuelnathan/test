<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

define('STATION_COMBINE_SCENARIO', 0);
define('STATION_COMBINE_DEFAULT', 1);
define('STATION_COMBINE_TAG', 2);
define('GENERATE_ALL_STATIONS', 0);
define('GENERATE_INDIVIDUAL_STATION', 1);
define('RETURN_RESULTS_AS_ARRAY', true);
define('RETURN_MYSQLI_RESULTS', false);

defined("STUDENT_ID_TYPE") or define("STUDENT_ID_TYPE", "S");
defined("EXAM_NUMBER_TYPE") or define("EXAM_NUMBER_TYPE", "E");
defined("CANDIDATE_NUMBER_TYPE") or define("CANDIDATE_NUMBER_TYPE", "C");


