<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

// Include templating class
use \OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

// Define required arrays
$selectedForms = [];
$withResults = [];
$withoutResults = [];

// Post variable for chosen scenario
$formIDPostParam = filter_input(INPUT_POST, 'choose_form', FILTER_SANITIZE_NUMBER_INT);

// Loop through unique station IDs and build up selection scenarios
foreach ($selectedFormIDs as $uniqueFormID) {
    $selectedForms[$uniqueFormID] = [
        'form_name' => $formsFiltersMap[$uniqueFormID]['form_name'],
        'has_result' => $formsFiltersMap[$uniqueFormID]['has_result']
    ];
}

// If there are no scenarios then exit
if (count($selectedForms) == 0) {
   return false;
}
 
// Group forms by results and no results
$groupedForms = get_From_Array($selectedForms, 'has_result', [1], '=');

// With results
if (isset($groupedForms[0]) && count($groupedForms[0]) > 0) {
    $withResults = $groupedForms[0];
}

// Without results
if (isset($groupedForms[1]) && count($groupedForms[1]) > 0) {
   $withoutResults = $groupedForms[1];
}

// Template data
$data = array(
    'forms_with_results' => $withResults,
    'forms_without_results' => $withoutResults,
    'form_id_chosen' => $formIDPostParam,
);

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($data);
