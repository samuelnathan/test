<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
    return false;

 // Loop through selected stations
 foreach ($stationSessionsMap as $filterValue => &$stationData) {
    
    // Loop through sessions selected
    foreach ($stationData['sessions'] as $sessionID => &$eachSession) {
       
        // Remove unused station sets
        foreach ($eachSession as $key => &$station) {
            
            $stationID = $station['station_id'];
            $formID = $station['form_id'];
            
            // Each session
            if (!in_array($formID, $stationData['unique_forms'])) {

                // Remove no longer required session
                unset($eachSession[$key]);
                $eachSession = array_values($eachSession);

            }
            
            // Build session stations map
            if (!isset($sessionStationsMap[$sessionID][$stationID]))
                $sessionStationsMap[$sessionID][$stationID] = $station;


            // Build station set information
            if (!isset($allStationsData[$stationID])) {

                $allStationsData[$stationID] = [
                    'form_id' => $formID,
                    'station_number' => $station['station_number'],
                    'station_code' => $station['station_code'],
                    'pass_value' => $station['pass_value'],
                    'weighting' => $station['weighting'],
                    'form_name' => $station['form_name'],
                    'total_value' => $station['total_value']
                ];

            }
            
            // Build scenarios to filters map
            if (!isset($formsFiltersMap[$formID])) {

                $formsFiltersMap[$formID] = [
                    'station_number' => $station['station_number'],
                    'station_code' => $station['station_code'],
                    'form_name' => $station['form_name'],
                    'total_value' => $station['total_value'],
                    'has_result' => false
                ];

            }

            // Build filters to scenarios map
            setA($filtersScenariosMap, $filterValue, []);
            if (!in_array($formID, $filtersScenariosMap[$filterValue])) {
                $filtersScenariosMap[$filterValue][$formID] = [
                    'form_name' => $station['form_name'],
                    'total_value' => $station['total_value']
                ];
            }
        }

    }
  
 }

 /**
  *  Compile pass setting fields
  */
  foreach ($sessionsData as $eachSessionID => $sessionRecord) {
  
     /**
      * What's the point in displaying session pass fields,
      * if no stations attached
      */ 
     if (!isset($sessionStationsMap[$eachSessionID]))
        continue;
      
     $tableData = [];
     $tableData['title'] = $sessionRecord['session_date']  
              . " " . $sessionRecord['session_description'];
     $tableData['data'] = $sessionStationsMap[$eachSessionID];
     $passData[$eachSessionID] = $tableData;

  }
 
  $db->examRules->checkCompetencyRules($examIDCurrent);
  $competencyRules = $db->examRules->getCompetencyRules($examIDCurrent);
  $divergenceRule = ($rules['divergence_threshold'] == null) ? $rules['divergence_threshold'] :
      roundAndFormat($rules['divergence_threshold'], 1);
  
  // Template data
  $templateData = [
    'passRules' => $passData,
    'competencyRules' => $competencyRules,  
    'individualRules' => [
        'maxGrsFail' => $rules['max_grs_fail'],
        'minStationsPass' => $rules['min_stations_to_pass'],
        'multiExaminerResultsAveraged' => $rules['multi_examiner_results_averaged'],
        'divergence_threshold' => $divergenceRule,
        'grade_weightings' => $rules['grade_weightings'],
        'results_published' => $rules['results_published'],
    ]
  ];

  $template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__));
  $template->render($templateData);
