<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
 */
 
 defined("_OMIS") or die('Restricted Access');
 
 use \OMIS\Template as Template;
 use \OMIS\Auth\Role as Role;
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!Role::loadID($_SESSION["user_role"])->canAccess()) 
     return false;
 
 require_once "extra/helper.php";
 require_once "extra/cronbach.php";
 require_once "Math/Stats.php";
 include_once "analysis_definitions.php";
 
 // Feature Enabled|Disabled
 list($frameworkEnabled, $candidateNumFeature, $examNumFeature, $selfAssessFeature) = 
 $db->features->enabled([
      'competency-framework', 'candidate-number',
      'exam-number', 'self-assessments'
      ], $_SESSION['user_role']
 );

 $sessionsData = $groupsDB = $allGroupIDs = $allStationsData = $uniqueStationIDs
 = $stationIdentifiers = $stationsUserSelected = $groupsUserSelected 
 = $sessionsSelected = $selectedFormIDs = $formsFiltersMap = $filtersScenariosMap 
 = $stationSessionsMap = $sessionStationsMap = $meansGraphData 
 = $passFailGraphData = $failedGraphData = $cronbachsGraphData = $feedbackData 
 = $studentCompetencies = $competencyRules = [];
 
 $databaseCombination = "station_number";
 $competencyResultsExist = false;
 $totalGroupCount = 0;
 
 // From All Results Icon Selection
 $fromAllResultsIcon = isset($_GET['o']) && $db->exams->existsById($_GET['o']);
 
 // From Results Icon Selection
 $fromResultsIcon = isset($_GET['s']) && $db->sessions->existsById($_GET['s']);
 
 // From Delete Pages
 $fromDeletePage = isset($_POST['results_deleted']);
 
 // Are there Groups
 $areGroups = isset($_POST['grps']);
 
 // Tab Persistence Value
 $tabPersist = isset($_REQUEST['tab_persist']) ? explode('-', $_REQUEST['tab_persist'])[1] : "raw";

 // Process Groups Selected
 if ($areGroups) {
 
   if ($fromDeletePage) {
 
      $groupsUserSelected = unserialize(base64_decode($_POST['grps']));
 
   } else {
 
      $groupsUserSelected = $_POST['grps'];
 
   }
   
 }
 
 // GET exam ID & the exam record
 if (isset($_POST['exam_id']) && $db->exams->existsById($_POST['exam_id'])) {
 
     // From Results Icon Selection
     $examIDCurrent = $_POST['exam_id'];
 
 } elseif ($fromResultsIcon) {
 
     $examIDCurrent = $db->sessions->getSessionExamID($_GET['s']);
 
 // From all results icon selection
 } elseif ($fromAllResultsIcon) { 
 
     $examIDCurrent = $_GET['o'];
 
 } else {
 
     $examIDCurrent = "";
 
 }
 
 $examDB = $db->exams->getExam($examIDCurrent);
 
 if (!is_array($examDB) || count($examDB) == 0) 
     errorExitandEnd(gettext('OSCE') . " ID specified was not found");
 
 // Get exam settings
 $settings = $db->examSettings->get($examIDCurrent);
 
 // Exam SESSION & the SESSION DETAILS
 $sessionsDB = $db->exams->getExamSessions($examIDCurrent);
 
 while ($eachSession = $db->fetch_row($sessionsDB)) {
  
   // Exam GROUPS & the GROUP DETAILS
   $groups = $db->groups->getSessionGroups($eachSession['session_id']);
   $eachGroupCount = mysqli_num_rows($groups);
   $selectedGroupCount = 0;
 
   while ($eachGroup = $db->fetch_row($groups)) {
 
      $totalGroupCount++;

      // Select Group
      $sessionGroupIcon = ($fromResultsIcon && $eachSession['session_id'] == $_GET['s']);
      $groupsChoosen = ($areGroups && in_array($eachGroup['group_id'], $groupsUserSelected, true));

      if ($fromAllResultsIcon || $sessionGroupIcon || $groupsChoosen) {

          setA($eachGroup, 'checked', true);
          $allGroupIDs[] = $eachGroup['group_id'];
          $selectedGroupCount++;

      } else {

          setA($eachGroup, 'checked', false);

      }

      $groupsDB[$eachSession['session_id']][$eachGroup['group_id']] = $eachGroup;

    }

    // Selected sessions
    $selectSession = ($selectedGroupCount == $eachGroupCount && $eachGroupCount != 0);

    if ($selectedGroupCount > 0) 
        $sessionsSelected[] = $eachSession['session_id'];

    $sessionsData[$eachSession['session_id']] = [
        'session_description' => $eachSession['session_description'],
        'session_date' => formatDate($eachSession['session_date']),
        'checked' => $selectSession
     ];
   
 }
 
 // Total # of sessions
 $sessionCount = count($sessionsData);
 
 if ($sessionCount == 0)
     errorExitandEnd("No day/circuit exist for this " . gettext('OSCE'));
 
 
 if (count($groupsDB) == 0) 
     errorExitandEnd("No groups exist for this " . gettext('OSCE'));
 
 // EXAM STUDENT LIST
 $studentList = $db->groups->getStudents($allGroupIDs, null, true, $examDB['term_id']);
 
 if (count($studentList) == 0)
     errorExitandEnd("No " . gettext('students') . " exist for groups selected");
 
 // UPDATE rules
 if (!is_null(filter_input(INPUT_POST, 'update_rules')))
     (new \OMIS\Analysis\RulesService)->update($examDB);
 
 // Get exam rules 
 $rules = $db->examRules->getRules($examIDCurrent);
 
 // EXAM STATIONS
 $examStationsDB = $db->stations->getStations(
         $sessionsSelected,
         RETURN_MYSQLI_RESULTS,
         $databaseCombination
 );
 
 // Station ALL Checkbox checked true/false
 $stationChoosen = (isset($_POST['stations_cb']) && $_POST['stations_cb'] == 1);
 $stationsCBChecked = ($stationChoosen || $fromAllResultsIcon || $fromResultsIcon) ? 1 : 0;
 
 if (isset($_POST['stations'])) {
 
     if (isset($_POST['results_deleted'])) {
 
         $stationsUserSelected = unserialize(base64_decode($_POST['stations']));
 
     } else {
 
         $stationsUserSelected = $_POST['stations'];
 
     }
     
 }

 // Self assessments
 $examSelfAssess = $db->selfAssessments->existsByExam($examDB['exam_id']);

 if (!$examStationsDB || mysqli_num_rows($examStationsDB) == 0)
    errorExitandEnd("No stations exist for day/circuits choosen");
 
 /* Option panel display settings
  */
 $postVariables = [
     "displaygrs", "displayfeedback", "displaystudentname", 
     "displaygroups", "displayorder", "displayweighted", "displaysession", 
     "displaynationality", "displaycronbach", "displayexaminers", 
     "displaytimestamp", "displayalternativenum", "displaystationsfailed", 
     "displaygrade", "displaypercent", "displayflags", "displaynotes", "displayalerts"
 ];
 
 foreach ($postVariables as $postVariable) {
 
     $value = filter_input(INPUT_POST, $postVariable, FILTER_SANITIZE_NUMBER_INT);
     /* Double dollar sign creates a variable named after the variable supplied,
      * e.g. if $a = 'name' and $b = 'value', then $$a = $b is equivalent to:
      * $name = 'value'. Nice!
      */
     $$postVariable = ($value != 1) ? 0 : 1;
 
 }

 /* 
  * [Results Analysis Data Sorting]
  */
 $sortType = iS($_POST, 'order_type');
 $sortIndex = iS($_POST, 'order_index');
 $previousIndex = iS($_POST, 'previous_index');
 
 if ($sortType == '' && $sortIndex == '') {
   
     $sortIndex = "overallscore";
     $sortType = "desc";
     
 }
 
 if ($previousIndex != $sortIndex) {
   
     $sortType = "desc";
     
 }
 
 $previousIndex = $sortIndex;
 
 /* Set defaults from config file for options (checkboxes, drop-downs etc) on the control panel
  * Only set these when a new instance of the analysis & results section is first launched
  */
 $studentOrderConfig = $config->analysis_defaults['student_order'];
 $combineByNumberConfig = $config->analysis_defaults['hide_combine_by_number'] ? false : $config->analysis_defaults['combine_by_number'];
 $displayAlertsConfig = $config->analysis_defaults['alerts_tab'];
 
 $candidateNum = ($settings['exam_student_id_type'] == CANDIDATE_NUMBER_TYPE && $candidateNumFeature);
 $examNum = ($settings['exam_student_id_type'] == EXAM_NUMBER_TYPE && $examNumFeature);
 $examHasGradeRules = ($examDB['grade_rule'] > 1);

 if ($fromResultsIcon || $fromAllResultsIcon) {

    $displayorder = $studentOrderConfig;
    $displayalerts = $displayAlertsConfig;
    $displayalternativenum = ($candidateNum || $examNum) ? 1 : 0;
    $displayweighted = $rules['grade_weightings'] ||
          $db->exams->hasAnyWeighting($examDB['exam_id']) ? 1 : 0;
    $displaygrade = $examHasGradeRules ? 1 : 0;

 }

 // come back to ?
 $filterCombination = "station_number";
 
 // Station Tag Combination
 if ((isset($_POST['cnm']) && trim($_POST['cnm']) == STATION_COMBINE_TAG)) {
 
     $cnm = STATION_COMBINE_TAG;
     $databaseCombination = "station_code";
     $filterCombination = "station_code";
 
 // Station Number Combination
 } elseif ((($fromResultsIcon || $fromAllResultsIcon) && $combineByNumberConfig) || 
             (isset($_POST['cnm']) && trim($_POST['cnm']) == STATION_COMBINE_DEFAULT)) {
 
     $cnm = STATION_COMBINE_DEFAULT;
     $databaseCombination = "station_number";
 
 // Station Scenario Combination
 } else {
 
     $cnm = STATION_COMBINE_SCENARIO;
     $databaseCombination = "form_id";
 
 }

 // Temp override (will be removed!!)
 $cnm = STATION_COMBINE_DEFAULT;
 $databaseCombination = "station_number";

 /**
  * Array containing the maximum number of multiple examiners per station
  * (pulled from student_results table)
  */
 $stationMultiExaminerCount = $db->results->getMaxMultiResultCounts(
         $sessionsSelected,
         $databaseCombination
 );
 
 // Session Results
 $sessionsHaveResults = $db->results->doSessionsHaveResult($sessionsSelected, 'bool');
 
 // Get Examiner Names etc
 if ($displayexaminers) {
   
     $examinerInfo = $db->exams->getExamExaminerNames($examIDCurrent);
     
 // If not reset $tabPersist if it exists
 } elseif ($tabPersist == 'examiners') {
   
     $tabPersist = "raw";
     
 }
 
 // Get Global Rating Scale Data
 $grsData = $db->results->getGlobalRatingScaleData($examIDCurrent);
 
 // Get Exam Feedback Data
 if ($displayfeedback)
    $feedbackData = $db->feedback->getExamFeedbackData($examIDCurrent);
 
 // Alerts Group Tab reset if not selected
 if (!$displayalerts && $tabPersist == 'alerts')
     $tabPersist = "raw";

 // Reset tab if self assessments no longer exists    
 if ($tabPersist == 'assessments' && !$examSelfAssess)
     $tabPersist = "raw";
   
 ?>
 <div id="mainadiv">
 <form name="analysis_form" id="analysis_form" method="post" 
       action="manage.php?page=out_analysis" onsubmit="return(ValidateForm(this, null))">    
 <div id="topresdiv">
 <?php 
 
   // Include Options for the Analysis Section
   include "options_analysis.php";
   
 ?></div>
 <div id="analysis-div"><?php

     (new Template('tabs.html.twig'))->render([
       'tabPersist' => $tabPersist,
       'showCompetencies' => $frameworkEnabled,
       'showExaminers' => $displayexaminers,
       'showAlerts' => $displayalerts,
       'showSelfAssessments' => 
       ($selfAssessFeature && $examSelfAssess)
     ]);

 ?><div id="wait-content">Loading Please Wait....</div>
 <div id="tabs-content">
 <div id="results-rules"<?php if ($tabPersist == 'rules') { ?> class="show-content"<?php } ?>><?php
   
    // Exam rules pane
    include "exam_rules.php";
    
  ?></div>
     
  <?php 
  
   /**
    * Self Assessments Tab
    */
   if ($selfAssessFeature && $examSelfAssess) {
        
      ?>
        <div id="results-assessments"<?php if ($tabPersist == 'assessments') { ?> class="show-content"<?php } ?>></div>
     <?php 
  
     }
     
  ?>
              
  <div id="results-summary"<?php if ($tabPersist == 'summary') { ?> class="show-content"<?php } ?>><?php
  
   //  Build Analysis Table
   include "build_rawdata.php";
  
   if ($areResults) {
     
      // Include Summary Table
     include "build_summary.php";
     
     // Include Output Summary
     include "output_summary.php";
     
   } else {
     
     ?><p>No results to display data</p><?php
     
   }
  ?></div>
  
   <div id="results-detailed"<?php if ($tabPersist == 'detailed') { ?> class="show-content"<?php } ?>><?php
   
   if($areResults) {
     
      // Include Options Stations 
      include "options_stations.php";
      
   } else {
     
     ?><p class="adjusted-p">No results to display data</p><?php
     
   }
  ?></div><?php 
 
   // [If Display Examiners]
   if($displayexaminers) {
     
    ?><div id="results-examiners"<?php 
    
        if ($tabPersist == 'examiners') { 
          
            ?> class="show-content"<?php
            
        } 
        
     ?>><?php
        
        // [If Results]
        if ($areResults) {
          
         // Include Output Examiners
 	 include "examiner_analysis.php";
         
        } else {
          
         ?><p>No results to display data</p><?php
         
       }
 	  
     ?></div><?php
     
    }
   
   ?><div id="results-graphs"<?php if ($tabPersist == 'graphs') { ?> class="show-content"<?php } ?>><?php
 
   // [If Results]
   if ($areSubmittedResults) {
     	 
     // Stations Means array to session
     $_SESSION['stations_means'] = $meansGraphData;
     ?><img id="graph-img1" alt="Stations Mean" src="./graphs/stations_means.php?sid=<?=date('sihymd')?>"/><?php 
 
     // Pass Fail array to session
     $_SESSION['stations_pf'] = $passFailGraphData;
     ?><img id="graph-img2" alt="Stations Pass Fail" src="./graphs/stations_pass_fail.php?sid=<?=date('sihymd')?>"/><?php
 
     // Competencies graph
     if ($frameworkEnabled) {
       
        $competencies = $db->competencies->getExamCompetencyResults($uniqueStationIDs);
        $competencyResultsExist = (count($competencies) > 0);
        
        if ($competencyResultsExist) {
          
             $_SESSION['station_competencies'] = $competencies;  
             ?><img id="graph-img5" alt="Competencies" src="./graphs/radar_competencies.php?sid=<?=date('sihymd')?>"/><?php
             
        }
        
     }
     
     // Station Student fail count to session
     if ($displaystationsfailed && isset($failedGraphData['failed_count'])) {
       
       $_SESSION['stations_students_failed'] = $failedGraphData;
       ?><img id="graph-img3" alt="Student Stations Failed Count" src="./graphs/stations_failed.php?sid=<?=date('sihymd')?>"/><?php 
       
     }
     
     // Stations cronbach values to session
     if ($displaycronbach) {
       
       $_SESSION['stations_crons'] = $cronbachsGraphData;  
       ?><img id="graph-img4" alt="Stations Cronbach" src="./graphs/stations_cronbach.php?sid=<?=date('sihymd')?>"/><?php
       
     }
    
   } else {
     
     ?><p>No submitted results in order to display charts</p><?php
     
   }
    
   ?></div>
    <div id="results-raw"<?php 
    
     if ($tabPersist == 'raw') {
       
         ?> class="show-content"<?php
         
     } 
     
     ?>><?php
     
    // Include Output Analysis 
    include "output_rawdata.php";
    
  ?></div>
  
  <?php
 
  // Competency framework tab
  if ($frameworkEnabled) {
  
    // Include Student Competencies
    include "student_competencies.php";     
  
  }
  
  /**
   * Alerts tab content
   */
  if ($displayalerts) {
    
     $notifications = $db->examNotifications->bySessions($sessionsSelected);

     (new Template('exam_notifications.html.twig'))->render([
       'notifications' => $notifications,
       'examID' => $examIDCurrent,
       'stickTab' => ($tabPersist == "alerts"),
       'combination' => $cnm,
       'anon' => (!$displaystudentname)
     ]);
     
  }
  
  ?><div id="results-export"<?php 
  
    if ($tabPersist == 'export') {
      
        ?> class="show-content"<?php 
        
    }
    
    ?>><?php
    
    // [If Results]
    if ($areResults) { 
      
      // Include Options Export
      include "options_export.php";
      
    } else {
      
      ?><p class="adjusted-p">No results to export</p><?php
      
    }
    
   ?></div>
 </div>
 </div>
 </form>
 <?php
  
 // Submit Form for Detailed Scenario Results Section
 if ($areResults) {
   
     // Add Titles to Raw Data
     array_unshift($rawResults, $rawTitles);
   
    ?><form target="detailed-iframe" name="detailed_form" id="detailed_form" method="post" action="manage.php">
       <input type="hidden" name="page" value="resdets"/>
       <input type="hidden" name="sel" id="form-selected" value=""/>
       <input type="hidden" name="grps" value="<?=implode(',', $allGroupIDs)?>"/>
       <input type="hidden" name="displaystudentname" value="<?=$displaystudentname?>"/>
       <input type="hidden" name="displaynationality" value="<?=$displaynationality?>"/>
       <input type="hidden" name="displayexaminers" value="<?=$displayexaminers?>"/>
       <input type="hidden" name="displaygroups" value="<?=$displaygroups?>"/>
       <input type="hidden" name="displayorder" value="<?=$displayorder?>"/>
       <input type="hidden" name="displaysession" value="<?=$displaysession?>"/>
       <input type="hidden" name="displaytimestamp" value="<?=$displaytimestamp?>"/>
       <input type="hidden" name="displayalternativenum" value="<?=$displayalternativenum?>"/>
       <input type="hidden" name="displayweighted" value="<?=$displayweighted?>"/>
    </form>

    <form name="analysis_export_form" id="analysis-export-form" method="post" 
          action="./export/export_analysis.php" onsubmit="return(ValidateForm(this))">
      <input type="hidden" name="export_overall" value="export"/>
      <input type="hidden" name="raw_export" id="raw-hidden" value="1"/>
      <input type="hidden" name="summary_export" id="summary-hidden" value="1"/>
      <input type="hidden" name="detailed_export" id="detailed-hidden" value="1"/>
      <input type="hidden" name="cdetailed_export" id="cdetailed-hidden" value="1"/>
      <input type="hidden" name="exam_id" value="<?=$examIDCurrent?>"/>
      <input type="hidden" name="nrofcols" value="<?=$numberOfColumns?>"/>
      <input type="hidden" name="additionalsubcols" value="<?=$additionalSubColumns?>"/>

      <input type="hidden" name="displaystudentname" value="<?=$displaystudentname?>"/>
      <input type="hidden" name="displaynationality" value="<?=$displaynationality?>"/>
      <input type="hidden" name="displayorder" value="<?=$displayorder?>"/>
      <input type="hidden" name="displaysession" value="<?=$displaysession?>"/>
      <input type="hidden" name="displaytimestamp" value="<?=$displaytimestamp?>"/>
      <input type="hidden" name="displayalternativenum" value="<?=$displayalternativenum?>"/>
      <input type="hidden" name="displaygrade" value="<?=$displaygrade?>"/>
      <input type="hidden" name="displaypercent" value="<?=$displaypercent?>"/>
      <input type="hidden" name="displayweighted" value="<?=$displayweighted?>"/>
      <input type="hidden" name="displaycronbach" value="<?=$displaycronbach?>"/>
      <input type="hidden" name="cnm" value="<?=$cnm?>"/>

      <input type="hidden" name="groupsdata" value="<?=pack_data($allGroupIDs)?>"/>
      <input type="hidden" name="rawdata" value="<?=pack_data($rawResults)?>"/>
      <input type="hidden" name="summarydata" value="<?=pack_data($summaryTable)?>"/>
      <input type="hidden" name="scenariodata" value="<?=pack_data($formsFiltersMap)?>"/>
      <input type="hidden" name="stationdata" value="<?=pack_data($filtersScenariosMap)?>"/>
      <input type="hidden" name="multi_examiner_counts" value="<?=pack_data($stationMultiExaminerCount)?>"/>
     <?php

      // Examiner Export
    if ($displayexaminers) {

        ?><input type="hidden" name="examiners_export" id="examiners-hidden" value="1"/>
          <input type="hidden" name="examinerdata" value="<?=pack_data($examinerDataExport)?>"/><?php

    }

    // Student Competency Results Export
    if ($competencyResultsExist) {

        ?><input type="hidden" name="competencies_export" id="competencies-hidden" value="1"/>
          <input type="hidden" name="competenciesdata" value="<?=pack_data($studentCompetencies)?>"/><?php

    }

    ?><input type="hidden" name="cexp" id="cexp" value="<?=date('sihymd')?>"/>
    </form><?php 
 
 }
 
 ?></div>
 