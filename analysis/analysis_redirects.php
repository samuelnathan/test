<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 /* Defined to tell any included files that they have been included rather than being 
  * invoked directly from the browser. Need to declare it anywhere that a file is directly
  * invoked by the user (via AJAX).
  */
 define('_OMIS', 1);
 
 // Additional definitions
 define('MARK_ALL_ABSENT', 0);
 define('DELETE_ALL', 1);
 
 use \OMIS\Session\FlashList as FlashList;
 use \OMIS\Session\FlashMessage as FlashMessage;
 use \OMIS\Auth\RoleCategory as RoleCategory;
 
 $redirectOnSessionTimeOut = true;
 include __DIR__ . '/../extra/essentials.php';
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
 
    $template = new \OMIS\Template("noaccess.html.twig");
    $data = [
        'logged_in' => true,
        'root_url' => $config->base_path_url,
        'username' => $_SESSION['user_identifier'],
        'page' => \OMIS\Utilities\Request::getPageID()
    ];
    
    $template->render($data);
    return;
 
 }
 
 $flashlist = new FlashList();
 
 $stationResultsAction = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_STRING);
 $examResultsAction = filter_input(INPUT_POST, 'action_exam_results', FILTER_SANITIZE_NUMBER_INT);

 /**
  *  Examinee (Student) station result actions [Delete result, Mark absent]
  */
 if (!is_null($stationResultsAction)) {
    
    $resultsJsonID = filter_input(INPUT_POST, 'results_json_id', FILTER_SANITIZE_STRING);
    $resultsSelected = filter_input(INPUT_POST, 'multi_results', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);   
    $stationID = filter_input(INPUT_POST, "station_id", FILTER_SANITIZE_NUMBER_INT);
    $student = filter_input(INPUT_POST, "student", FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $success = TRUE;
    $absentOrDelete = FALSE;

    // first check the password
    if (empty($password) || !$db->users->checkPassword($_SESSION['user_identifier'], $password)) {
        
        // Password is wrong. Complain.
        $flashlist->add(
            "Supplied password was incorrect",
            FlashMessage::SESSION_FLASH_WARNING
        );

        error_log(__FILE__ . ": Action $stationResultsAction, incorrect password supplied.");
        
        $UL = '../manage.php?page=studentresult&results_id=' . $resultsJsonID;
        header('Location: ' . $UL);
        return;        
        
    }

    // Get station info (for logging)
    $stationRecord = $db->stations->getStation($stationID);

    // Deal with the specific action
    switch (trim(strtolower($stationResultsAction))) {

        case 'delete':
                
            // First drop notification (alerts), not critical as backup copy in log files
            $db->examNotifications->dropByResultIDs($resultsSelected, $student);
                
            // Delete specified result(s).
            $deleteCount = 0; 
            foreach ($resultsSelected as $resultID) {

                // Remove any self assessment ownership
                $db->selfAssessments->removeOwnershipByResult($resultID);

                $deleteSuccess = $db->results->deleteResult($resultID, false, true);
                
                if ($deleteSuccess) {
                  
                    $deleteCount++;

                }

                $success &= $deleteSuccess;

            }

            \Analog::info($_SESSION['user_identifier'] . " " 
                . ($deleteCount == 0 ? "failed to delete any" : "deleted $deleteCount") 
                . " result(s) for student $student in " 
                . " station " . $stationRecord['station_number']
                . " : " . $stationRecord['form_name']
                . " from the analysis screen"
            );

            if ($success) {

                $flashlist->add(
                    $deleteCount . " " . gettext('student') . " results deleted.",
                    FlashMessage::SESSION_FLASH_INFO
                );
               
                $absentOrDelete = TRUE;

            } else {

                $flashlist->add(
                    "Encountered errors deleting " . gettext('student') . " results.",
                    FlashMessage::SESSION_FLASH_ERROR
                );

            }
                    
            break;

        case 'absent':
 
            // Mark result(s) as absent
            if (count($resultsSelected) == 0) {
                
                $flashlist->add(
                    FlashMessage::SESSION_FLASH_INFO,
                    "No " . gettext('student') . "s selected to mark as absent"
                );
                
            }

            $absentCount = 0;
            foreach ($resultsSelected as $resultID) {
 
                $absentSuccess = true;
                $absentSuccess &= $db->results->deleteResult($resultID, true, true);
                $absentSuccess &= $db->results->setAbsentScoreValue($resultID, 1, 0);
                
                if ($absentSuccess) {
                  
                   $db->selfAssessments->removeOwnershipByResult($resultID);

                   $absentCount++;

                }

                $success &= $absentSuccess;
                
            }
 
            \Analog::info($_SESSION['user_identifier'] . " "
                . ($absentCount == 0 ? "failed to mark any" : "marked $absentCount") 
                . " result(s) as absent for student $student in " 
                . " station " . $stationRecord['station_number']
                . " : " . $stationRecord['form_name']
                . " from the analysis screen"
            );

            if ($success) {
 
                $flashlist->add(
                    $absentCount . " " . gettext('student') . "(s) marked as absent",
                    FlashMessage::SESSION_FLASH_INFO
                );

                $absentOrDelete = TRUE;
 
            } else {
 
                $flashlist->add(
                    "Encountered errors marking " . gettext('student') . "(s) as absent.",
                    FlashMessage::SESSION_FLASH_WARNING
                );
 
            }

            break;
        default:

            // We don't know what the hell the user asked us to do.
            $flashlist->add(
                "Unknown command $stationResultsAction",
                FlashMessage::SESSION_FLASH_ERROR
            );
 
            error_log(__FILE__ . ": Unexpected/unknown action command $stationResultsAction specified");
            $success = FALSE;
            break;
    }
    
    if (!$success) {
 
        error_log(__FILE__ . ": Requested action $stationResultsAction partially/completely failed");
 
    }
 
    /* 
     Absent or Delete status URL parameter to be returned to main dialog window.
     If the dialog detects this parameter then it will refresh the parent results and analysis
     popup window to reflect any changes (deletions or absents).
    */
    $absentOrDeleteUrl = "";

    if ($absentOrDelete) {

      $sessionID = $db->sessions->getSessionIDByStationID($stationID);
      $examID = $db->sessions->getSessionExamID($sessionID);
      $absentOrDeleteUrl = "&success=yes";

      (new \OMIS\Outcomes\OutcomesService($db))->setUpToDate($student, $examID, false);
      $db->gradeRules->calc($sessionID, $student);

    }
    
    $UL = '../manage.php?page=studentresult&results_id=' . $resultsJsonID . $absentOrDeleteUrl;
    header('Location: ' . $UL);
    return;
    
  
 /**
  * Mark exam results as absent / Delete exam results
  */
 } else if (!is_null($examResultsAction)) {
    
    // Is admin role
    $adminRoles = new RoleCategory(RoleCategory::ROLE_GROUP_ADMINISTRATORS);
    $userIsAdmin = $adminRoles->containsRole($_SESSION['user_role']);
    $password = filter_input(INPUT_POST, "confirm_password", FILTER_SANITIZE_STRING);
    $student = filter_input(INPUT_POST, 'student', FILTER_SANITIZE_STRING);
    $exam = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
    $session = filter_input(INPUT_POST, 'session', FILTER_SANITIZE_NUMBER_INT);
    $success = false;
   
    // Use does not have permissions
    if (!$userIsAdmin) {
 
        $flashlist->add(
            "You do not have permissions to perform this action, please contact your administrator",
            FlashMessage::SESSION_FLASH_WARNING
        );
 
    // Check password
    } else if (!$db->users->checkPassword($_SESSION['user_identifier'], $password)) {
 
        // Password is wrong. Complain.
        $flashlist->add(
            "Supplied password was incorrect",
            FlashMessage::SESSION_FLASH_WARNING
        );
 
    }

    // Mark exam results as absent
    else if (!is_null($examResultsAction) && $examResultsAction == MARK_ALL_ABSENT) {
      
       // Mark student absent for all stations in the session
       if ($db->results->markStudentAbsentSession($session, $student)) {
         
           // Remove any (if exists) self assessment ownership data
           $db->selfAssessments->removeOwnershipByExamStudent($exam, $student);

           // Re-run outcomes
           (new \OMIS\Outcomes\OutcomesService($db))->setUpToDate($student, $exam, false);

           // Recalculate grade
           $db->gradeRules->calc($session, $student);
         
           // Marked student as absent for all stations 
           $flashlist->add(
              "Marked " . gettext('student') . " absent for all stations in this day/circuit",
              FlashMessage::SESSION_FLASH_INFO
           );
           $success = true;
           
       } else {
         
           // This failed, let the user know
           $flashlist->add(
              "Failed to mark " . gettext('student') . " as absent in this day/circuit",
              FlashMessage::SESSION_FLASH_ERROR
           );         
           
       }
      
     // Delete exam results
     } else if (!is_null($examResultsAction) && $examResultsAction == DELETE_ALL) {
       
        if ($db->results->deleteStudentSessionResults($session, $student)) {
          
          // Drop notifications
          $db->examNotifications->dropBySessionStudent($session, $student);
         
          // Drop student exam data (notes)
          $db->examdata->dropByExamStudent($exam, $student);
          
          // Remove any (if exists) self assessment ownership data
          $db->selfAssessments->removeOwnershipByExamStudent($exam, $student);
  
          $flashlist->add(
              gettext('Exam') . " results deleted for this " . gettext('student'),
              FlashMessage::SESSION_FLASH_INFO
          );
          $success = true;
            
        } else {
            
          $flashlist->add(
              "Failed to delete " . gettext('exam') . " results, please try again",
              FlashMessage::SESSION_FLASH_ERROR
          );
          
        }
        
     }

     $UL = '../manage.php?page=studentexam'
        . '&student=' . $student 
        . '&exam=' . $exam
        . '&success=' . (int)$success;
     header('Location: ' . $UL);
     return;

 }
 
 //If none of the above return to index page
 header('Location: ../');
 