<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 use OMIS\Database\Results as Results;
 use OMIS\Template as Template;
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
     return false;
 }
 
 // For Export
 $examinerDataExport = [];
 
 $data = [
     "columns" => [
         gettext('Examiner') . " ID",
         "Surname",
         "Forenames",
         "Station " . (($cnm == STATION_COMBINE_TAG) ? "Tag" : "Number"),
         "Station Name",
         "Station Mean" 
     ]
 ];
 
 // Loop through examiners marking averages
 foreach ($examinerMarks as $exMark) {
 
     // Final Score
     $final_score = percent($exMark['score'], $exMark['possible']);
 
     // Passed or Failed
     $passed = Results::determinePassOrFail(
         $exMark['score'],
         ($exMark['pass'] / $exMark['count']),
         $exMark['possible']
     );
     
     // [If Combining By Station Tag/Number]
     if (in_array($cnm, [STATION_COMBINE_TAG, STATION_COMBINE_DEFAULT])) {
         $scenario_group = $exMark['combineID'];
         $station_number = '';
 
         // Concatenate Station Name
         foreach ($filtersScenariosMap[$exMark['combineID']] as $stationName) {
             $station_number .= $stationName['form_name'] . '<br/>';
         }
 
     } else { // [If By Stations]
         $scenario_group = $formsFiltersMap[$exMark['combineID']]['station_number'];
         $station_number = $formsFiltersMap[$exMark['combineID']]['form_name'];
     }
     
     // For Export (.xls)
     $examinerDataExport[] = [
         $exMark['examinerID'],
         @$examinerInfo[$exMark['examinerID']]['surname'],
         @$examinerInfo[$exMark['examinerID']]['forename'],
         $scenario_group,
         $station_number,
         $final_score
     ];
 
     // Append another examiner to twig template data
     $data['examiners'][] = [
         "id"            => $exMark['examinerID'],
         "surname"       => @$examinerInfo[$exMark['examinerID']]['surname'],
         "firstname"     => @$examinerInfo[$exMark['examinerID']]['forename'],
         "scenarios"     => $scenario_group,
         "station_name"  => $station_number,
         "score_colour"  => ($passed ? 'pass' : 'fail'),
         "final_score"   => $final_score
     ];
     
 }
 
 // Generate template
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($data);
 
 // Tidy up...
 unset(
   $passed, $examinerMarks, $data, $scenario_group, 
   $station_number, $final_score, $examinerInfo
 );
 