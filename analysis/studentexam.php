<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

 use \OMIS\Template as Template;
 use \OMIS\Session\FlashMessage as FlashMessage;
 
 // Analysis definitions
 include_once "analysis_definitions.php";
 
 // Additional definitions
 define("MARK_ALL_ABSENT", 0);
 define("DELETE_ALL", 1);
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Exam score & possible
 $examScore = 0;
 $examPossible = 0;
 $numOfCompetencies = 0;
 
 // Page access check / can user access this section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) {

    return false;

 }

 $flashList = new \OMIS\Session\FlashList();
 
 // Candidate number feature
 $candidateFeatureEnabled = $db->features->enabled(
         "candidate-number", 
         $_SESSION["user_role"]
 );
 
 // Student ID
 $studentID = filter_input(INPUT_GET, "student", FILTER_SANITIZE_STRING);
 if (empty($studentID)) {

    error_log(__FILE__ . ": 'student' parameter not set or empty");
    die("No " . gettext('student') . " specified");

 } else {

    $studentID = trim($studentID);

 }

 // Exam ID
 $examID = filter_input(INPUT_GET, "exam", FILTER_SANITIZE_NUMBER_INT);
 if (empty($examID) || ($examID < 0)) {

    error_log(__FILE__ . ": 'exam' parameter not set, empty, or negative");

 }
 
 // Request Combine By Number/Tag
 $cnm = filter_input(INPUT_GET, "cnm", FILTER_SANITIZE_NUMBER_INT);
 if (empty($cnm)) {

    $cnm = STATION_COMBINE_SCENARIO;

 }

 // back to previous page?
 $back = (int) filter_input(INPUT_GET, "back", FILTER_SANITIZE_NUMBER_INT);
 
 // Success (results marked absent or deleted)
 $actionSuccessParam = (boolean) filter_input(INPUT_GET, "success", FILTER_SANITIZE_NUMBER_INT);
 
 // Get exam record
 $examDB = $db->exams->getExam($examID);
 $examSettings = $db->examSettings->get($examID);
 $rules = $db->examRules->getRules($examID);
 $candidatesEnabled = ($candidateFeatureEnabled && $examSettings["exam_student_id_type"] == CANDIDATE_NUMBER_TYPE);
 $termID = $candidatesEnabled ? $examDB["term_id"] : null;
 $studentDB = $db->students->getStudent($studentID, $termID);
 
 $eachSession = $db->fetch_row($db->sessions->getStudentSession($studentID, $examID));
 $sessionID = $eachSession["session_id"];
 $studentResultsDB = $db->results->bySession(
        $studentID, 
        $sessionID,
        RETURN_RESULTS_AS_ARRAY,
        "station_id"
 );

 // Number of results
 $numResults = count($studentResultsDB);
 $nameSeparated = strlen($studentDB['surname']) > 0 && strlen($studentDB['forename']) > 0 ? ", " : "";
 $studentName = $studentDB['surname'] . $nameSeparated . $studentDB['forename'];
 $examName = $examDB["exam_name"];
 $nationality = $studentDB["nationality"];
 $gender = $studentDB["gender"];
 $candidateNumber = empty($studentDB["candidate_number"]) ? "" : $studentDB["candidate_number"];
 $stationIDs = [];
 
 // Session data
 $sessionExists = !is_null($eachSession);

 if ($sessionExists) {

    $sessionDate = formatDate($eachSession["session_date"]);
    $sessionDescription = $sessionDate . " - ";

    if (trim($eachSession["session_description"]) !== "") {

        $sessionDescription .= " " . $eachSession["session_description"];

    }
 
 } else {
 
    $sessionDate = "";
    $sessionDescription = "";

 }
 
 // Register student exam record (in if non-existant)
 $db->examdata->registerStudentData(
    [$studentID], 
    $examID
 );

 $studentExamData = $db->fetch_row(
   $db->examdata->byStudentExam(
      $studentID, 
      $examID
   )
 );
 
 // Radar Graph Url
 $radarGraphURL = "graphs/radar_student_stations.php"
                . "?student=" . $studentID
                . "&amp;exam=" . $examID
                . "&amp;session=" . $sessionID
                . "&amp;cnm=" . $cnm 
                . "&amp;sid=" . date("sihymd");

 // Competency graph Url
 $competencyGraphURL = "graphs/radar_student_competency.php"
                     . "?url_request=true"
                     . "&amp;student=" . $studentID
                     . "&amp;session=" . $sessionID 
                     . "&amp;sid=" . date("sihymd");
 
 // Student result history url 
 $studentHistoryUrl = "manage.php?page=studenthistory"
                    . "&amp;student=". $studentID
                    . "&amp;back=1"
                    . (isset($_REQUEST['anon']) ? '&amp;anon' : '');
 
 // Is view student history feature enabled
 $viewHistoryAllowed = $db->features->enabled(
        "student-history",
        $_SESSION["user_role"]
 );

 // Student has results history?
 $hasResultHistory = $db->results->doesStudentHaveResult(
        $studentID, null, null, null, null, true, 1
 );

 $showResultHistory = ($viewHistoryAllowed && $hasResultHistory);
 
 // Flash message count
 $flashMessageCount = $flashList->count();
 
 // Flash message to inform user
 if ($flashMessageCount == 0 && $numResults == 0) {
 
   $flashList->add(
      "No results exist for this " . gettext('student') . " in this " . gettext('exam'),
      FlashMessage::SESSION_FLASH_INFO
   );
   
   $flashList->display();
 
 } else if ($flashMessageCount > 0) {
 
   $flashList->display();
 
 }
 
 // Combine pre-select
 $combineDefaultSelect = ($cnm == STATION_COMBINE_DEFAULT);
 $combineTagSelect = ($cnm == STATION_COMBINE_TAG);
 
 $studentResults = [];
 $examSummary = [];
 $scoreFound = false;
 
 foreach ($studentResultsDB as $stationID => $stationResults) {
      
      // Station number
      $studentResults[$stationID]["station_number"] = $stationResults[0]["station_number"];       
       
      // Result Count
      $resultCount = count($stationResults);
      
      // Multi Examiner Results Count
      $studentResults[$stationID]["result_count"] = $resultCount;      
     
      // Absent values
      $absentValues = array_column($stationResults, "absent");     
     
      // Absent found in station results, display record as absent
      $absentFound = (array_search(1, $absentValues) !== false);
      $studentResults[$stationID]["station_absent"] = $absentFound;
      $scoreFound = (!$absentFound);
      
      // Weighted score is required based on rules setting
      if ($rules['multi_examiner_results_averaged']) {
          
           $stationPossible = $stationResults[0]["total_value"];
           $stationScore = $db->results->getMeanScoreForStudentStation(
              $studentID,
              $stationID
           );              
           
      // Aggregate score is required based on rules setting     
      } else {

           $stationPossible = $db->results->getAggregatePossibleForStudentStation(
              $examID,
              $studentID,
              $stationID
           );

           $stationScore = $db->results->getAggregateScoreForStudentStation(
              $studentID,
              $stationID
           );

      }
      
      $studentResults[$stationID]["station_possible"] = (float)$stationPossible;
      $studentResults[$stationID]["station_mean"] = round($stationScore, 1);
     
      // Percentage Station Score... round to one decimal place.
      $percentStationScore = sprintf("%.1f", ($stationScore * 100 / $stationPossible));
      $studentResults[$stationID]["station_percent"] = $percentStationScore;     
     
      // Accumulate values for overall
      $examScore += $stationScore;
 
      if ($scoreFound) {

        $examPossible += $stationPossible;
        
      }     
      
      // Encode student ID and Station ID into Json
      $resultsJsonID = base64_encode(json_encode([
         "station_id" => $stationID,
         "student_id" => $studentID,
      ]));
 
      if (!in_array($stationID, $stationIDs, true)) {
         array_push($stationIDs, $stationID);
      }
 
      // Result url 
      $resultUrl = "manage.php?page=studentresult&amp;results_id=" 
                   . $resultsJsonID . "&amp;back=1"
                   . (isset($_REQUEST['anon']) ? "&amp;anon=" : '');
 
      // Station link
      $studentResults[$stationID]["station_link"] = $resultUrl;
 
      // Station tag
      $studentResults[$stationID]["station_code"] = $stationResults[0]["station_code"];
 
      // Form name
      $studentResults[$stationID]["form_name"] = $stationResults[0]["form_name"];
    
 }
    
 /**
  *  We have results, then set exam summary values and get competency count
  */
 if ($numResults > 0 && $scoreFound) {
    
    // Exam Percentage Score
    $examSummary["exam_score"] = round($examScore, 1);
    $examSummary["exam_possible"] = $examPossible;
    $examSummary["exam_percent"] = percent($examScore, $examPossible);
  
    // Competency results per student and station IDs
    $competencyResults = $db->competencies->getExamCompetencyResults(
           $stationIDs, 
           $studentID
    );

    $numOfCompetencies = count($competencyResults);
    
 }
 
 // Template data
 $templateData = [
    "view_history" => $showResultHistory,
    "student_id" => $studentID,
    "student_name" => $studentName,
    "student_anon" => isset($_REQUEST['anon']),
    "gender" => $gender,
    "nationality" => $nationality,
    "candidate_number" => $candidateNumber, 
    "exam_id" => $examID,
    "session_id" => $sessionID,
    "exam_name" => $examName,
    "session_description" => $sessionDescription,
    "competency_result_count" => $numOfCompetencies,
    "student_history_url" => $studentHistoryUrl,
    "competency_graph_url" => $competencyGraphURL,
    "radar_graph_url" => $radarGraphURL,
    "student_results" => $studentResults,
    "student_exam_data" => $studentExamData,
    "grade_rule" => $db->gradeRules->getById($examDB['grade_rule']),
    "exam_summary" => $examSummary,
    "flash_message_count" => $flashMessageCount,
    "back_button" => $back,
    "action_success" => (!is_null($actionSuccessParam) && $actionSuccessParam),
    "score_found" => $scoreFound,
    "generate_all_stations" => GENERATE_ALL_STATIONS,
    "mark_all_absent" => MARK_ALL_ABSENT,
    "delete_all" => DELETE_ALL,
    "station_combine_scenario" => STATION_COMBINE_SCENARIO,
    "station_combine_default" => STATION_COMBINE_DEFAULT,
    "station_combine_tag" => STATION_COMBINE_TAG,
    "combine_default_select" => $combineDefaultSelect,
    "combine_tag_select" => $combineTagSelect,
    "enable_tag_option" => (count($db->stations->getStationGroups($examID)) > 2)
 ];
 
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($templateData);
 
 // Is view student history feature enabled
 if ($viewHistoryAllowed) {
 
  // Is view student history feature enabled
  if ($numResults > 0) {
     
    // Loop through all results forms
    foreach ($studentResultsDB as $stationID => $studentStationResults) {
        
        // Absent values
        $absentValues = array_column($studentStationResults, "absent");        
        
        // Absent found in station results, abort display
        $absentFound = (array_search(1, $absentValues) !== false);

        if($absentFound) {

            continue;

        }
        
        // Get examiner weightings
        $accExaminerWeightings = $db->exams->getExaminerWeightings($stationID);
 
        // Result record count
        $resultRecordCount = count($studentStationResults);
 
        // Put together station info to be used in 'each_station.php' script
        $stationRecord = [
            "form_id" => $studentStationResults[0]["form_id"],
            "form_name" => $studentStationResults[0]["form_name"],
            "station_number" => $studentStationResults[0]["station_number"],
            "pass_value" => $studentStationResults[0]["pass_value"]
        ];

        include "each_station.php";
    }
    
   }

 }

 ?>
</div>
