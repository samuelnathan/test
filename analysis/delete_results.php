<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */
 
 $session = new OMIS\Session;
 $session->check();
 global $page;
 
 if (!isset($page)) {
 
    // This is "dresults" in the menu list
    $page = "dresults";
 
 }
 
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess($page)) {
 
    return false;
 
 }

 ob_end_flush();
 ?><div id="mainadiv2"><input type="hidden" id="delete_act" value="1"/><?php
 
 $groupsData = [];
 $sessionData = [];
 $stationsData = [];
 $sessionGroupMapping = [];
 $selectedStations = [];
 $selectedGroups = [];
 
 $displayVariables = [
    "displaygrs" => 0,
    "displayfeedback" => 0,
    "displaystudentname" => 0, 
    "displaygroups" => 0,
    "displayorder" => 0,
    "displayweighted" => 0,
    "displaysession" => 0, 
    "displaynationality" => 0,
    "displaycronbach" => 0,
    "displayexaminers" => 0, 
    "displaytimestamp" => 0,
    "displayalternativenum" => 0,
    "displaystationsfailed" => 0, 
    "displaygrade" => 0,
    "displaypercent" => 0,
    "displayflags" => 0,
    "displaynotes" => 0,
    "displayalerts" => 0
 ];
 
 foreach ($displayVariables as $key => &$value) {
  
    $paramValue = filter_input(INPUT_POST, $key, FILTER_SANITIZE_NUMBER_INT);
    $value = $paramValue != 1 ? 0 : 1;
  
 }

 // Combine by number
 $cnm = iS($_POST, 'cnm'); 
 $examID = iS($_POST, 'exam_id');
 $examCB = iS($_POST, 'exam_cb');
 $stationsCB = iS($_POST, 'stations_cb');
 
 // User Delete Action
 if (isset($_POST['del_action']) || isset($_POST['password'])) {

    $password = trim($_POST['password']);
 
    // Get Stations
   if (isset($_POST['stations'])) {
 
        $selectedStations = unserialize(base64_decode($_POST['stations']));
 
    }
 
    // Get Groups
    if (isset($_POST['grps'])) {
 
        $selectedGroups = unserialize(base64_decode($_POST['grps'])); 
 
    }
    
 // Coming from Results Page    
 } elseif (isset($_POST['delete_results'])) {
    
    // Get Stations
    if (isset($_POST['stations'])) {
 
        $selectedStations = $_POST['stations'];
 
    }
 
    // Get Groups
    if (isset($_POST['grps'])) {
 
        $selectedGroups = $_POST['grps'];
 
    }
 
 }
 
 // Does Exam Exist
 if (!$db->exams->doesExamExist($examID)) {
 
    errorExitandEnd(
       gettext('OSCE') .
       'does not exist in system or it has been recently deleted, could not continue'
    );
 
 } else {
 
    $examRecord = $db->exams->getExam($examID, true);
 
 }
 
 // Process Groups and Session Information
 foreach ($selectedGroups as $group) {
 
    $groupRecord = $db->exams->getGroup($group);
    if (!is_null($groupRecord) && is_array($groupRecord)) {
 
        $groupsData[$group] = [
            'session_id' => $groupRecord['session_id'],
            'group_name' => $groupRecord['group_name'],
            'group_order' => $groupRecord['group_order']
        ];
 
        $sessionGroupMapping[$groupRecord['session_id']][] = $group;
        $sessionRecord = $db->exams->getSession($groupRecord['session_id']);
 
        $sessionData[$groupRecord['session_id']] = [
            'session_date' => formatDate($sessionRecord['session_date']),
            'session_description' => ucfirst($sessionRecord['session_description']),
            'exam_id' => $sessionRecord['exam_id'],
            'exam_name' => $sessionRecord['exam_name']
        ];
    }
 
 }
 
 // Do we have at least 1 group that exists
 if (sizeof($groupsData) == 0) {
 
    errorExitandEnd(
       'No groups were selected or no groups exist for this ' 
       . gettext('OSCE')
    );
 
 }
 
 // Process all Stations Information
 foreach ($selectedStations as $stationKey) {
 
    list($stationNumber, $formID) = explode(':', $stationKey);
    $formRecord = $db->forms->getForm($formID);
 
     // Does Station Exist
    if (!is_null($formRecord) && is_array($formRecord)) {
 
        $stationsData[$stationKey] = [
            'form_name' => $formRecord['form_name'],
            'station_number' => $stationNumber
        ];
 
    }
 
 }
 
 // Do we have at least 1 station that exists
 if (sizeof($stationsData) == 0) {
 
    errorExitandEnd(
      'No stations were selected or no stations exist for this ' 
      . gettext('OSCE')
    );
 
 }
 
 // Does Exam Exist
 if (!$db->exams->doesExamExist($examID)) {
 
    errorExitandEnd(
        gettext('OSCE') . ' does not exist in system or it ' .
        'has been recently deleted, could not continue'
    );
 
 } else {
 
    $examRecord = $db->exams->getExam($examID, true);
 
 }
 
 // Process Groups and Session Information
 foreach ($selectedGroups as $group) {
 
    $groupRecord = $db->exams->getGroup($group);
    if (!is_null($groupRecord) && is_array($groupRecord)) {
 
        $groupsData[$group] = [
            'session_id' => $groupRecord['session_id'],
            'group_name' => $groupRecord['group_name'],
            'group_order' => $groupRecord['group_order']
        ];
 
        $sessionGroupMapping[$groupRecord['session_id']][] = $group;
        $sessionRecord = $db->exams->getSession($groupRecord['session_id']);
 
        $sessionData[$groupRecord['session_id']] = [
            'session_date' => formatDate($sessionRecord['session_date']),
            'session_description' => ucfirst($sessionRecord['session_description']),
            'exam_id' => $sessionRecord['exam_id'],
            'exam_name' => $sessionRecord['exam_name']
        ];
 
    }
 
 }
 
 // Do we have at least 1 group that exists
 if (sizeof($groupsData) == 0) {
 
    errorExitandEnd(
      'No groups were selected or no groups exist for this ' . 
       gettext('OSCE')
    );
 
 }
 
 // Process all Stations Information
 foreach ($selectedStations as $stationKey) {
 
    list($stationNumber, $formID) = explode(':', $stationKey);
    $formRecord = $db->forms->getForm($formID);
 
    // Does Station Exist
    if (!is_null($formRecord) && is_array($formRecord)) {
 
        $stationsData[$stationKey] = [
            'form_name' => $formRecord['form_name'],
            'station_number' => $stationNumber
        ];
 
    }
 }
 
 // Do we have at least 1 station that exists
 if (sizeof($stationsData) == 0) {
 
    errorExitandEnd(
       'No stations were selected or no stations exist for this ' . 
       gettext('OSCE')
    );
 
 }
 
 /***************************** A Few Checks ***********************************/
 if (isset($_POST['del_action']) || isset($_POST['delete_results']) || isset($_POST['password'])) {
 
  ?><div class="content2">
<div class="card d-inline-block align-top mr-2 mb-4 todel">
  <div class="card-header">
    <div class="card-title h6 mb-0">
      <div class="row no-gutters align-items-center">
        <img src="assets/images/manage.svg" alt="card-title-icon" class="card-title-icon">
        <span class="card-title-text">
          Delete Results
        </span>
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="card-text">
      <div id="todel-div1">
     <span class="bold"><?=gettext('OSCE')?></span>&nbsp;<?=$examRecord['exam_name']?><br/>
     <span class="bold"><?=gettext('Department')?></span>&nbsp;<?=$examRecord['dept_name']?> [<span class="red"><?=$examRecord['dept_id']?></span>]<br/>
     <span class="bold">Day/Circuit<?php if(sizeof($sessionData) > 1) { ?>s<?php } ?> &amp; Group<?php if(sizeof($selectedGroups) > 1) { ?>s<?php } ?></span>
 
         <ul id="sess-del-list"><?php
 
          // Loop through sessions
          foreach ($sessionData as $sessonKey => $eachSession) {
 
            //Session
            ?><li class="sess-desc"><label><?=$eachSession['session_date'].' '.$eachSession['session_description']?></label><?php
                 //Groups
                  ?><ul><?php

                     //Loop through groups
                     foreach ($sessionGroupMapping[$sessonKey] as $eachGroup) {
 
                        ?><li><?=$groupsData[$eachGroup]['group_name']?></li><?php
 
                     }
                   ?></ul>
                  </li><?php
            }
          ?></ul>
         </div>
 
         <div class="fl">
          <span class="bold">In Station<?php if(sizeof($stationsData) > 1) { ?>s<?php } ?></span>
          <ul class="station-list"><?php
 
       foreach ($stationsData as $station) {
 
          ?><li><?=$station['station_number'].' - '.$station['form_name']?></li><?php
 
       }
 
           ?></ul>
         </div><br class="clearv2"/>

    </div>   
  </div> 
</div> 
   </div><?php
 
  }
 
 // Is Password Correct 
 if (isset($_POST['del_action']) || isset($_POST['password'])) {
 
    $passfail = true;
    $password = $_POST['password'];
 
    if ($db->users->checkPassword($_SESSION['user_identifier'], $password)) {
 
        $result = $db->users->getUser($_SESSION['user_identifier']);
 
    } else {
 
        $result = 0;
 
    }
 
    if ($result && mysqli_num_rows($result) == 1 && $password != '') {
 
        $passfail = false;
 
        \Analog::info($_SESSION['user_identifier'] . " deleted exam results for '"
            . $examRecord['exam_name'] . "' in department " . $examRecord['dept_name']
            . " from " . $config->getName());
 
        // Begin deletions
        foreach ($groupsData as $groupID => $groupInfo) {
          
            // Remove self assessment ownership records (if any)
            $db->selfAssessments->removeOwnershipByExamGroup($examID, $groupID);

            foreach ($stationsData as $stationKey => $station) {
 
                $stationID = $db->stations->getStationsBySessionID($stationKey, $groupInfo['session_id']);
                $db->results->deleteSessionResults($groupID, $stationID);
                $db->examNotifications->dropByGroupStation($groupID, $stationID);
                $db->blankStudents->deleteBlanksSubmitted($groupID, $station['station_number']);
 
            }

            // Recalculate grade/appointability for updated groups
            (new \OMIS\Outcomes\OutcomesService($db))->setUpToDateByExam($examID,false);
            $db->gradeRules->calcByGroup($groupID, $groupInfo['session_id']);

        }

 ?><div class="content3"><br/>
   <div class="infodiv">
       All results deleted for the above days/circuits, groups and stations
    </div><br/><br/><br/>
 <?php 
        $formType = "deleted"; 
        include "delete_results_form.php";

    ?></div>
 <?php
    }
 }
 
 // Deleting results or Incorrect Password
 if (filter_has_var(INPUT_POST, 'delete_results') || (isset($passfail) && $passfail)) {
     
    ?><div class="content3"><br/>
     <span class="lh130">
 	  <span class="f15 bold">Are you sure you want to continue with deletion?<br/>
      <span class="red"><?php
       
        // Incorrect Password
        if (isset($passfail) && $passfail) { 
            ?>Incorrect Password. Please enter the correct password in the field provided below to continue with this action<?php 
 
        //Correct Password
        } else { 
            ?>You must enter your login password in the field provided below to continue with this action<?php 
        }
 
 	   ?></span>
 	   <br/><br/>
 	 </span>
 	</span><?php 
 	 
 	  $formType = "cancel_delete";
 	  include "delete_results_form.php"; 
 	 
 	  $formType = "to_delete";
 	  include "delete_results_form.php";
 	
     ?></div><?php
 }
 
 ?></div>
 