<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

use OMIS\Template as Template;

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

/* @TODO: Wrap the remaining code in this script within a function declaration
 * and call it.
 */

#Form Field Values
switch ($formType) {

    case 'cancel_delete':
        $buttonID = 'no_confirm';
        $buttonName = '';
        $buttonValue = 'No, Go Back';
        $returnPage = 'out_analysis';
        break;
    case 'deleted':
        $buttonID = 'backr';
        $buttonName = '';
        $buttonValue = 'Ok, Back to Results Section';
        $returnPage = 'out_analysis';
        break;
    case 'to_delete':
        $buttonID = 'yes_confirm';
        $buttonName = 'del_action';
        $buttonValue = 'Yes, Continue';
        $returnPage = 'dresults';
        break;
    default:
        error_log(__FILE__ . "Unknown form type '$formType' specified.");
        return;
        
}

$data = [
    "return_page" => $returnPage,
    "form_type" => $formType,
    "exam_id" => $examID,
    "exam_cb" => $examCB,
    "stations_cb" => $stationsCB,
    "groups" => base64_encode(serialize($selectedGroups)),
    "stations" => base64_encode(serialize($selectedStations)),
    "cnm" => $cnm,
    "buttonID" => $buttonID,
    "buttonName" => $buttonName,
    "buttonValue" => $buttonValue,
    "displayVariables" => $displayVariables
];

$template = new Template(Template::findMatchingTemplate(__FILE__));
$template->render($data);
