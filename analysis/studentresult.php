<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

  use \OMIS\Database\Results as Results; 
 
 // Analysis definitions
 include_once "analysis_definitions.php";
  
 // Critical Session Check
 $session = new OMIS\Session();
 $session->check();
 
 // Page access check / Can user access this section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess()) 
     return false;
 
 // Flash display any error messages
 ?><span class="flash-span"><?php
 (new \OMIS\Session\FlashList())->display();
 ?></span><?php
 
 // Result ID param
 $resultIDParam = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
 $resultsJsonIDRedirect = filter_input(INPUT_GET, "results_id", FILTER_SANITIZE_STRING);
 
 // If coming from analysis raw data section with single result ID
 if (!is_null($resultIDParam)) {
    
    // Result ID
    $resultID = trim($resultIDParam);
 
    // Get Single Result
    $resultDB = $db->results->getResultByID($resultID);
    $stationID = $resultDB["station_id"];
    $studentID = $resultDB["student_id"];
 
    // Encode student ID and Station ID into Json
    $resultsJsonID = base64_encode(
        json_encode(["station_id" => $stationID, "student_id" => $studentID])
    );
 
    // Does Single Result Exist
    if (!$db->results->doesResultExist($resultID)) {
 
        die("Result does not exist");
 
    }
    
 } elseif (!is_null($resultsJsonIDRedirect)) {
    
    // Result ID
    $resultsJsonID = trim($resultsJsonIDRedirect);
    
    // If called from 'analysis_redirects' script after 'absent' and 'delete' actions
    list($stationID, $studentID) = array_values(
        json_decode(base64_decode($resultsJsonID), true)
    );
 
 } else {
 
    die("Result ID not specified");

 }
 
 // Does Student Exist?
 if (!$db->students->doesStudentExist($studentID)) {
 
    die(gettext('Student') . " does not exist");

 } 
 
 // Does Station Exist?
 $stationRecord = $db->exams->getStation($stationID);
 
 if (empty($stationRecord)) {
 
    die("Station not found in system");
 
 }
 
 $formName = $stationRecord["form_name"];
 $stationNum = $stationRecord["station_number"];
 $totalPossibles = [];
 
 // Back to Previous Page
 $back = (filter_input(INPUT_GET, "back", FILTER_SANITIZE_NUMBER_INT));
 
 // There are scores
 $areScores = false;
 
 // Weighted score
 $finalStationScore = "";
 
 // Percent score
 $percentScore = "";
 
 // Score count
 $scoreCount = 0;
 
 // Get All (Multi) Results
 $studentStationResults = $db->results->getResults($studentID, $stationID, null, true);
 
 // Calculate the number of non-absent scores
 if (sizeof($studentStationResults) > 0) {
 
   $scoreCount = count(
      array_filter(
        array_column($studentStationResults, "absent"), function ($value) {
           return ($value == 0);
   }));
   
 }
 
 // Result record count with absents
 $resultCountWithAbsents = count($studentStationResults);
 
 // Session Details
 $sessionID = $db->exams->getSessionIDByStationID($stationID);
 $sessionRecord = $db->exams->getSession($sessionID);
 $sessionDate = formatDate($sessionRecord["session_date"]);
 $examID = $sessionRecord["exam_id"];

 // Self assessment
 $formSelfAssess = $db->selfAssessments->scoresheetHasSelfAssessment($stationRecord['form_id']);
 $ownerID = $formSelfAssess ? $db->selfAssessments->getOwnerByStudentExam($studentID, $examID) : "";
 
 // Exam Details
 $examRecord = $db->exams->getExam($examID);
 $examName = $examRecord["exam_name"];
 $settings = $db->examSettings->get($examID);
 $rules = $db->examRules->getRules($examID);
 $candidateFeature = $db->features->enabled("candidate-number", $_SESSION["user_role"]);
 $candidatesEnabled = ($candidateFeature && $settings["exam_student_id_type"] == CANDIDATE_NUMBER_TYPE);
 $termID = $candidatesEnabled ? $examRecord["term_id"] : null;
 
 // Student Details
 $studentRecord = $db->students->getStudent($studentID, $termID);
 $nameSeparated = strlen($studentRecord['surname']) > 0 && strlen($studentRecord['forename']) > 0 ? ", " : "";
 $studentName = $studentRecord["surname"] . $nameSeparated . $studentRecord["forename"];
 $candidateNumber = isset($studentRecord["candidate_number"]) ? $studentRecord["candidate_number"] : null;
 
 // Session Info
 $sessionDescription = $sessionDate . " - ";
 if (!empty($sessionRecord["session_description"])) {
 
    $sessionDescription .= " " . $sessionRecord["session_description"];
 
 }
 
 // Examiners Attached to Stations
 $stationExaminers = $db->exams->getExaminersForStation($stationID, true);
 
 // Get the list of names for the various weightings.
 $examinerWeightings = $db->exams->getWeightingList();
 
 // All results Url
 $allResultsUrl = "manage.php?page=studentexam"
               . "&amp;student=" . $studentID 
               . "&amp;exam=" . $examID 
               . "&amp;back=1"
               . (isset($_REQUEST['anon']) ? "&amp;anon=" : '');
 
 ?><div id="password-form" name="password-form" title="Confirm Request">
  <p>Please enter your password to confirm that you really want to take this action.</p>
  <form>
   <label for="password">Password</label>
   <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all"/>
   <!-- Allow form submission with the keyboard without duplicating the dialog button -->
   <input type="submit" tabindex="-1" style="position:absolute; top:-1000px"/>
  </form>
 </div>
 
 <div class="topmain">
 <div>
 <form name="eresults_form" method="post" action="manage.php">
 <table id="examinee_sresult_table" class="title-table table-border">
   <tr>
     <th><?=gettext('Student')?> Identifier</th>
     <td><?=$studentID?></td>
     <th><?=ucwords(gettext('examination'))?></th>
     <td class="nbr"><?=$examName?></td>
   </tr>
   
   <?php 
   // Candidate number
   if (!empty($candidateNumber)) { 

        ?>
        <tr>
        <th>Candidate Number</th>
        <td><?=$candidateNumber?></td>
        <th>&nbsp;</th>
        <td class="nbr">&nbsp;</td>   
        </tr>
        <?php

    }
   ?>
    
   <tr>
    <th><?=gettext('Student')?> Name</th>
    <td><?=isset($_REQUEST['anon']) ? "-" : $studentName?>
       <input type="hidden" id="surname" 
       value="<?=(isset($_REQUEST['anon']) || empty($studentName) ? $studentID : $studentName)?>"
       />
       <input type="hidden" id="station" value="<?=$formName?>"/>
       <input type="hidden" name="page" value="pdfresults"/>
       <input type="hidden" name="action" value="<?=GENERATE_INDIVIDUAL_STATION?>"/>
       <input type="hidden" name="exam" value="<?=$examID?>"/>
       <input type="hidden" name="student" value="<?=$studentID?>"/>
       <input type="hidden" name="station_id" value="<?=$stationID?>"/>
       <input type="hidden" name="stations[]" value="<?=$stationNum?>"/>

       <?php if (isset($_REQUEST['anon'])) { ?>
         <input type="hidden" id="anon" name="anon" value=""/>
       <?php } ?>
    </td>
    <th>Day/Circuit</th>
    <td class="nbr"><?=$sessionDescription?></td>
   </tr>
        
   <tr>
    <th><?=gettext('Student')?> Gender</th>
    <td><?=$studentRecord["gender"]?></td>
    <th>Station Number</th>
    <td class="nbr"><?=$stationNum?></td>
   </tr>
        
   <tr>
    <th class="innerbb"><?=gettext('Student')?> Nationality</th>
    <td><?=$studentRecord["nationality"]?></td>
    <th class="innerbb">Station Name</th>
    <td class="nbr"><?=$formName?></td>
   </tr>
   
   <tr>
    <td class="nbb nbr tac" colspan="4">
       <a class="print-hide" href="<?=$allResultsUrl?>">View All <?=gettext('Exam')?> Results</a> 
    </td>
   </tr>
 </table>
     
 <?php if ($scoreCount > 0) { ?>
   <div class="delbdiv">
    <?php if ($back) { ?>
      <input type="button" id="back-button2" class="btn btn-secondary btn-sm" value="Back"/>
    <?php } ?>
     <input type="submit" name="pdf_results" class="btn btn-info btn-sm" value="PDF Forms"/>
     <input type="button" id="print-sres" class="btn btn-success btn-sm" value="Print"/>
   </div>
  <?php } ?>
 </form>
 </div>
 <br class="clearv2"/>     
     
 <?php
 
 // Do we have results
 if ($resultCountWithAbsents > 0) {
 
 ?><form name="results_form" id="results_form" method="post" action="analysis/analysis_redirects.php">
  <table id="multiple-results-table" class="title-table table-border">
    <tr id="results-row">
        <th>&nbsp;
            <input type="hidden" name="results_json_id" value="<?=$resultsJsonID?>"/>
            <input type="hidden" name="station_id" value="<?=$stationID?>"/>
        </th>
        <th><?=gettext('Examiner')?> #</th>
        <th><?=gettext('Examiner')?> ID</th>
        <th><?=gettext('Examiner')?> Name</th>
        <th><?=gettext('Examiner')?> Weighting</th>
        <th>First Submission</th>
        <th>Last Updated</th>
        <th>Score</th>
        <th class="nbr">Score (%)</th>
    </tr><?php
    
 // Score Accumulator
 $scoresWeightings = [];
 
 // Loop through all result records
 for ($i=0; $i<$resultCountWithAbsents; $i++) {
 
    // Student Result Record
    $studentResult = $studentStationResults[$i];
    $examinerID = $studentResult["examiner_id"];
 
    // Examiner Details
    $examinerDB = $db->users->getUser($examinerID, "user_id", true);
 
    // Scoring Details
    $absent = $studentResult["absent"];
 
    // Examiner Scoring Weight
    if (in_array($examinerID, array_keys($stationExaminers))) {
 
        /* The examiner is "known" to the system, so we should be able to
         * get that examiner's pre-defined weighting.
         */
        $examinerScoringWeight = (int) $stationExaminers[$examinerID]["scoring_weight"];
        
    } else {
 
        // If we don't know what weighting to assign to this examiner for any 
        // reason, then go with a default weighting.
        $examinerScoringWeight = \OMIS\Database\Exams::EXAMINER_WEIGHTING_DEFAULT;
 
    }
     
    // Observer highlight class
    $examinerClass = $examinerScoringWeight == 0 ? "observer-rw" : "";
 
    // Accumulate Examiner weighting for each_station.php script
    $accExaminerWeightings[$examinerID] = (int) $examinerScoringWeight;
 
    // If not absent do the score calculations
    if ($absent == 0) {
        
        $totalPossible = ($formSelfAssess && $ownerID != $examinerID) ? 
        $db->selfAssessments->getNonOwnerPossible($stationRecord["form_id"]) 
        : $stationRecord["total_value"];

        $scoresWeightings[] = [
            "score" => (float) $studentResult["score"],
            "weighting" => (int) $examinerScoringWeight,
        ];
        
        $scorePercent = percent(
              $studentResult["score"],  
              $totalPossible
        );
        
       
        /**
         * Exclude observer scores
         */
        if ($examinerScoringWeight > 0) {

            $totalPossibles[] = $totalPossible;

        }
        
        $areScores = true;
        
    }
 
 // Result summary row
 ?><tr class="<?=$examinerClass?>">
  <td><input type="checkbox" name="multi_results[]" class="print-hide multi-results" value="<?=$studentResult["result_id"]?>"/></td>
  <td class="tac"><?=$i + 1; ?></td>
  <td><?=$examinerID?></td>
  <td><?=$examinerDB["surname"] . ", " . $examinerDB["forename"]?></td><?php 
  
 if (empty($stationExaminers)) {
  
    ?><td class="weighting-td">(NA)</td><?php
 
 } else { 
 
    ?><td class="weighting-td"><?php 
       echo $examinerWeightings[(int) $examinerScoringWeight]?> 
       (<?php echo $examinerScoringWeight?>)
       <?php echo $examinerScoringWeight == 0 ?
         " - scores<br/>are excluded from final score" : ""?>
    </td><?php 
 
 } 
  ?><td><?php 
  
    echo date_format(
        date_create($studentResult['created_at']),
       "d/m/Y H:i:s"
    );

   ?></td>
    <td><?php
    
    echo date_format(
       date_create($studentResult['updated_at']),
       "d/m/Y H:i:s"
    );
    
    ?></td><?php 
 
  if ($absent) {
 
    ?><td class="absent" colspan="2">ABSENT</td><?php 
 
  } else {
 
    ?><td class="score-data tac"><?=round($studentResult['score'], 1)?></td>
      <td class="score-data tac nbr"><?=$scorePercent?></td><?php
 
  }
 
 ?></tr><?php
 
 }
    
 // Remove absent results and reset result count
 $studentStationResults = array_filter(
     $studentStationResults, create_function('$value', 'return $value["absent"] == 0;')
 );
 
 $overallLabel = "";

 // Summary Row
 if ($areScores) {
   
    list($finalStationScore, $possibleScore) = Results::determineScoreAndPossible(
        $rules['multi_examiner_results_averaged'],
        $scoresWeightings,
        $totalPossibles
    );

    if (count($scoresWeightings) > 0) {

        if ($rules['multi_examiner_results_averaged']) {
            
            $overallLabel = "Weighted Score";
            
        } else {
                  
            $overallLabel = "Aggregate Score";      
            
        }

    }
    
    $percentScore = percent(
        $finalStationScore, 
        $possibleScore
    );
  
 }

 if ($resultCountWithAbsents > 1) {
   
    ?><tr>
    <td colspan="7" class="tar bold nbb"><?=$overallLabel?></td>
    <td class="tac"><?php echo round($finalStationScore, 1)?></td>
    <td class="tac nbr"><?=$percentScore?></td>
    </tr><?php
 
 }

 ?><tr>
    <td class="nbr nbb" colspan="9">
     <div id="controls-div" class="print-hide<?php if ($resultCountWithAbsents == 1) { ?> controls-div-adj<?php } ?>">
      
      <select name="todo" id="todo" disabled="disabled" class="custom-select custom-select-sm mr-1">
        <option value="" disabled="disabled" selected="selected">Choose action</option>
        <option value="absent">absent</option>
        <option value="delete">delete</option>
      </select>
        <input type="hidden" name="password" id="confirm-password" value=""/>
        <input type="hidden" name="student" value="<?=$studentID?>"/>
        <input type="button" value="go" class="btn btn-outline-default btn-sm" name="submit_button" id="submit_button" disabled="disabled"/>
      </div>
    </td>
   </tr>
  </table>
 </form>
 </div>
 <?php
 
  if ($areScores) {

     include "each_station.php";

  }
  
} else { ?>
  <div id="no-results">
      No results exist for this <?=gettext('applicant')?> at this station<br/>
  </div>
</div><?php

}
