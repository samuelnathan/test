<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 use \OMIS\Database\Results as Results;
 use \OMIS\Template as Template;
 use \OMIS\Database\CoreDB as CoreDB;
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
   
    return false;
    
 }
 
 // Prepare twig template for the competency tab
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 
 // Competency pass values
 $competencyPassValues = CoreDB::group(
    $competencyRules,
    ['competency_id'],
    true,
    true
 );
 
 // If we don't have any data then we need to abort with the appropriate message
 if (!isset($competencies) || empty($competencies) || sizeof($competencyPassValues) == 0) {
   
    $template->render([
         "showContent" => ($tabPersist == "competencies"),
    ]);
    
    return false;
    
 }
 
 /**
  *  Build title row
  */
 $competencyTitles = ["Identifier"];
 $compTitleColours = array_fill(0, 4, '');

 if ($displaystudentname) {
  
   $competencyTitles = array_merge($competencyTitles, ["Surname", "Forenames"]);
   $compTitleColours = array_merge($competencyTitles, ["", ""]);
   
 }
 
 $competencyTitles = array_merge($competencyTitles, ["Grade", "Overall", "Overall %"]); 

 /**
  *  Build our competency data per student
  */
 foreach ($competencies as $competency) {
   
    $competencyTitles[] = $competency['name'];
    $compTitleColours[] = $competency['level_colour_code'];
    
 }
 
 foreach ($studentList as $studentRecord) {
  
    $studentCompetency = [];
    $studentCompetency['id'] = $studentRecord['student_id'];
    
    if ($displaystudentname) {
      
       $studentCompetency['surname'] = $studentRecord['surname'];
       $studentCompetency['forenames'] = $studentRecord['forename'];
        
    }
    
    $studentCompetency['grade'] = "NA";
    $studentCompetency['overall'] = "NA";
    $studentCompetency['overallPercent'] = "NA";

    /**
     * TO DO (DC): Work out a more efficient way to produce the competency results
     * per students. At least, less calls to the database
     */    
    $studentCompetencyResults = $db->competencies->getExamCompetencyResults(
            $uniqueStationIDs, 
            $studentRecord['student_id']
    );

    // Each student competency results
    $compTotalScore = 0;
    $compTotalPossible = 0;
    $compPassPossible = 0;
    $compResultCount = 0;
    
    foreach ($competencies as $competency) {
      
      $competencyID = $competency['id'];
      $compKey = 'competency_' . $competencyID;
      $studentCompetency[$compKey] = "NA";

      foreach ($studentCompetencyResults as $compResult) {
        
         if ($competencyID == $compResult['id']) {
           
            $studentCompetency[$compKey] = roundAndFormat($compResult['score']);
            $compTotalScore += $compResult['score'];
            $compTotalPossible += $compResult['possible'];
            $compPassPossible += $competencyPassValues[$competencyID]['pass'];
            $compResultCount++;
            
            break;
            
         }
         
      }
      
    }

    // Totals and final grade
    if ($compPassPossible > 0) {

       // Determine grade        
       $compPassPercentage = ($compPassPossible / $compResultCount);
       if (Results::determinePassOrFail($compTotalScore, $compPassPercentage, $compTotalPossible)) {
         
           $studentCompetency['grade'] = "pass";
           
       } else {
         
           $studentCompetency['grade'] = "fail";
           
       } 

       $studentCompetency['overall'] = roundAndFormat($compTotalScore);
       $studentCompetency['overallPercent'] = percent($compTotalScore, $compTotalPossible);
       
    }

    $studentCompetencies[] = $studentCompetency;
    
 }
 
 // Sort data by column 'overallPercent' and add title row
 orderArrayColumn($studentCompetencies, 'overallPercent', 'grouped', 'ASC');
 array_unshift($studentCompetencies, $competencyTitles);
 
 // Render template
 $template->render([
     "showContent" => ($tabPersist == "competencies"),
     "titleColours" => $compTitleColours,
     "data" => $studentCompetencies
 ]); 
 
 ?>