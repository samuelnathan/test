<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use \OMIS\Template as Template;
 
 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();
 
 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess())
     return false;
 
 // Features Enabled/Disabled (Note: Grs stands for Global Rating Scale)
 $deleteAllowed = $db->features->enabled('delete-results', $_SESSION['user_role']);
 $grsColumnAllowed = $db->features->enabled('grs-column', $_SESSION['user_role']);
 $feedbackAllowed = $db->features->enabled('feedback-column', $_SESSION['user_role']);
 $studentExamNumber = $db->features->enabled('exam-number', $_SESSION['user_role']);
 $studentCandidateNumber = $db->features->enabled('candidate-number', $_SESSION['user_role']);
 
 // Feature data for template
 $features = [
     'grs_column' => $grsColumnAllowed,
     'feedback_column' => $feedbackAllowed,
     'feature_disabled_user_info' => $config->feature_disabled_message,
     'student_exam_num' => $studentExamNumber,
     'student_candidate_num' => $studentCandidateNumber
 ];
 
 // Disable delete button
 $disableButton = (!$sessionsHaveResults || !$deleteAllowed);
 \Analog::info($_SESSION['user_identifier'] . " viewed results of " . $examDB['exam_name']);
 
 // Sessions selected string to be passed to hidden variable in the template
 $sessionsSelectedString = implode(',', $sessionsSelected);
 
 // Exam checkbox checked
 $examCBValue = filter_input(INPUT_POST, 'exam_cb', FILTER_SANITIZE_NUMBER_INT);
 $examCBChecked = ($fromAllResultsIcon || (strlen($examCBValue) > 0) || $sessionCount == 1);
  
 // Gather data for station template, unique station identifiers and station session map
 $multipleExaminers = false;
 $stationsData = [];
 
 while ($examStation = $db->fetch_row($examStationsDB)) {
 
     // Filter value
     $filterValue = ($cnm == STATION_COMBINE_TAG) ? $examStation['station_code'] : $examStation['station_number'];
     
     // Station Key
     $stationKey = $filterValue . ":" . $examStation['form_id'];
     
     // Build up selected station information
     if ($stationsCBChecked || in_array($stationKey, $stationsUserSelected)) {
 
         // If combining stations by numbers / Tag / if showing stations by station id
         if (in_array($cnm, [STATION_COMBINE_DEFAULT, STATION_COMBINE_TAG])) {

             if (!in_array($filterValue, $stationIdentifiers))
                 $stationIdentifiers[] = $filterValue;


         } else if (!in_array($examStation['form_id'], $stationIdentifiers)) {
             $stationIdentifiers[] = $examStation['form_id'];
         }
 
         // Add Station Filter Values (Station Number / Tag)
         if (!isset($stationSessionsMap[$filterValue]))
             $stationSessionsMap[$filterValue] = ['unique_forms' => []];
 
         // Unique Station IDs
         if (!in_array($examStation['form_id'], $stationSessionsMap[$filterValue]['unique_forms']))
             $stationSessionsMap[$filterValue]['unique_forms'][] = $examStation['form_id'];
 
         // Overall Unique Station IDs
         if (!in_array($examStation['form_id'], $selectedFormIDs))
             $selectedFormIDs[] = $examStation['form_id'];
 
         // Station Sessions Map
         if (!isset($stationSessionsMap[$filterValue]['sessions'])) {
             $stationSessionsMap[$filterValue]['sessions'] = 
             $db->stations->getStationsByFilter(
                 $sessionsSelected, 
                 $filterCombination, 
                 $filterValue
             );
         }
 
         $stationChecked = true;

     } else {

         $stationChecked = false;

     }
 
     // Check for multiple examiners
     if ($examStation['examiners_required'] > 1)
         $multipleExaminers = true;
 
     // If Scenario / Station Tag / Station Number
     if ($cnm == STATION_COMBINE_DEFAULT) {
         $stationText = $examStation['station_number'] . " - " . $examStation['form_name'];
     } else if ($cnm == STATION_COMBINE_TAG) {
         $stationText = $examStation['station_code'] . " - " . $examStation['form_name'];
     } else {
         $stationText = $examStation['form_name'];
     }
   
     // Station specific template data
     $stationsData[] = [
         'key' => $stationKey,
         'checked' => $stationChecked,
         'text' => $stationText
     ];  
     
 }
 
 // Settings fields template data
 $settingsFields = [
     'timestamp' => $displaytimestamp,
     'examiners' => $displayexaminers,
     'stations_failed' => $displaystationsfailed,
     'sessions' => $displaysession,
     'groups' => $displaygroups,
     'cronbach_analysis' => $displaycronbach,
     'global_rating_scales' => $displaygrs,
     'student_order' => $displayorder,
     'student_alternative_num' => $displayalternativenum,
     'student_nationality' => $displaynationality,
     'student_names' => $displaystudentname,
     'feedback' => $displayfeedback,
     'grade' => $displaygrade,
     'percent' => $displaypercent,
     'weighted' => $displayweighted,
     'alerts' => $displayalerts,
     'flags' => $displayflags,
     'notes' => $displaynotes,
     'combination_option' => $cnm,
     'hide_combine_num' => $config->analysis_defaults['hide_combine_by_number']
 ];
 
 
 // Template data
 $templateData = [
     'delete_disabled' => $disableButton,
     'tab_selected' => $tabPersist,
     'current_exam_id' => $examIDCurrent,
     'exam_info' => $examDB,
     'order_index' => $sortIndex,
     'previous_index' => $previousIndex,
     'order_type' => $sortType,
     'sessions_selected_string' => $sessionsSelectedString,
     'exam_cb_checked' => $examCBChecked,
     'sessions' => $sessionsData,
     'session_groups' => $groupsDB,
     'group_count' => $totalGroupCount,
     'stations_cb_checked' => $stationsCBChecked,
     'multiple_examiners' => $multipleExaminers,
     'stations' => $stationsData,
     'settings' => $settingsFields,
     'features' => $features,
     'examSettings' => $settings
 ];
 
 $template = new Template(Template::findMatchingTemplate(__FILE__));
 $template->render($templateData);
 