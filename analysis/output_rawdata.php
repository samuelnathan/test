<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */
 
 // Critical Session Check
 $session = new OMIS\Session();
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) return false;

 // Columns Selected
 $columnsSelected = [
     $displaystudentname,
     $displaystudentname,
     $displaynationality,
     $displaygroups,
     $displayorder,
     $displaysession,
     $displaysession,
     $displaynotes,
     $displaygrade,
     ($examHasGradeRules && $displaygrade),
     $displaypercent,
     $displayweighted,
     $displayweighted,
     $displaystationsfailed,
     $displayalternativenum
 ];

 // Number of Columns
 $numberOfColumns = count(array_filter($columnsSelected)) + 2;

 // Number of Sub Columns Selected
 $subColumnsSelected = [
     $displaygrs,
     $displayflags,
     $displayexaminers,
     $displayfeedback,
     $displaytimestamp
 ];

 // Column Colours
 $columnColours = [
    "#CCCCFF", // Pale blue
    "#FFDBFF", // Pale magenta
    "#CCFFCC", // Lime green
    "#FFF6E6", // Light orange
    "#FFCCCC", // Light red
    "#FFFFFF"  // White
 ];

 // Number of Sub Columns
 $numberOfSubColumns = count(array_filter($subColumnsSelected));
 $addWeightingColumn = $displayweighted ? 1 : 0;
 $additionalSubColumns = $numberOfSubColumns;

 // Raw Data Link Template
 $rawDataLinkTemplate = new \OMIS\Template('rawdata_link.html.twig');

 ?><table id="analysis_table" class="table-border2">

    <?php if ($statusCounts['incomplete'] > 0) { ?>
        <div class="incomplete-warn"><?=$statusCounts['incomplete'] . " ";
            echo "assessment" . ($statusCounts['incomplete'] > 1 ? "s" : "");
            ?> incomplete (or in progress), please review flashing <?php
            echo "score" . ($statusCounts['incomplete'] > 1 ? "s" : ""); ?> below
        </div>
    <?php } ?>

    <colgroup>
     <col span="<?=$numberOfColumns?>"/><?php

     // Loop through station Identifiers
     $columnIndex = 0;
     foreach ($stationIdentifiers as $stationIdentifier) {

         // This will cycle through the colours in sequence.
         $columnColour = $columnColours[$columnIndex % count($columnColours)];

         // Maximum number of multiple examiners per station
         $multiMax = $stationMultiExaminerCount[$stationIdentifier];
         $scoreSubColumn = ($multiMax > 1) ? 1 : 0;
         $rowSpan = ($multiMax * ($numberOfSubColumns + 1)) + $scoreSubColumn + $addWeightingColumn;

         ?>
         <col span="<?=$rowSpan?>" style="background-color: <?=$columnColour?> !important;"/>
         <?php

         // Increment column index
         $columnIndex++;

     }

    ?>
    </colgroup>

     <tr id="top-titlerow">
     <th id="left-columns" colspan="<?=$numberOfColumns?>">
       <div id="colour-codes" title="Result Colour Codes">
         <span id="fail-colour-code">fail (<?=$statusCounts['failed']?>)</span>
         <span id="incomplete-colour-code" <?
            echo $statusCounts['incomplete'] > 0 ? 'class="incomplete"' : '';
         ?>>incomplete (<?=$statusCounts['incomplete']?>)</span>
         <span id="pass-colour-code">pass (<?=$statusCounts['passed']?>)</span>
       </div>
     </th>

     <?php

     // Loop through station Identifiers
     foreach ($stationIdentifiers as $stationIdentifier) {

         // Maximum number of multiple examiners per station
         $multiMax = $stationMultiExaminerCount[$stationIdentifier];
         $scoreSubColumn = ($multiMax > 1) ? 1 : 0;
         $rowSpan = ($multiMax * ($numberOfSubColumns + 1)) + $scoreSubColumn + $addWeightingColumn;

         // Loop through multi results
          ?><th class="titletop3" colspan="<?=$rowSpan?>"><?php

          if ($cnm == STATION_COMBINE_SCENARIO) {

            echo $formsFiltersMap[$stationIdentifier]['form_name'];

          } else {

            echo "Station " . $stationIdentifier;

          }

          ?></th><?php

     }

    ?></tr>

    <tr id="order-titlerow"><?php

      // Student Identifier
      ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Identifier",
             $sortIndex,
             "studentid",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext('Student') . " Identifier",
             true
         );

         $rawTitles['student_id'] = "Identifier";

     ?></th><?php


     // Student exam number
     if ($displayalternativenum) {

       ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Number",
             $sortIndex,
             "altnumber",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext('Student') . "Exam Number",
             true
         );

         $rawTitles['altnumber'] = "Alt. Identifier";

      ?></th><?php

     }

     // Student Title
     if ($displaystudentname) {

     ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Surname",
             $sortIndex,
             "surname",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::Surname",
             true
         );

         $rawTitles['surname'] = "Surname";

     ?></th>

     <th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Forenames",
             $sortIndex,
             "forename",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::Forenames",
             true
         );

         $rawTitles['forename'] = "Forenames";

     ?></th><?php

     }

     // Nationality Title
     if ($displaynationality) {

     ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Nationality",
             $sortIndex,
             "nationality",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::Nationality ISO Codes",
             true
         );

         $rawTitles['nationality'] = "Nationality";

     ?></th><?php

     }

     //Groups Title
     if ($displaygroups) {

     ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Group",
             $sortIndex,
             "group",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext('Student') . " Group",
             true
         );

         $rawTitles['grp'] = "Group";

     ?></th><?php

     }

     // Student Order Title
     if ($displayorder) {

     ?><th class="titletop3 f12"><?php

         makeColumnOrderable(
             "Order",
             $sortIndex,
             "order",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext('Student') . " Order",
             true
         );

         $rawTitles['order'] = "Order";

     ?></th><?php

     }

     // Date / Session Title
     if ($displaysession) {

     ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Day",
             $sortIndex,
             "date",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::Days",
             true
         );

         $rawTitles['date'] = "Day";

     ?></th>
     <th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Circuit",
             $sortIndex,
             "session",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::Circuit",
             true
         );

         $rawTitles['session'] = "Circuit";

     ?></th><?php

     }

     // Student notes/issues
     if ($displaynotes) {

       ?><th class="titletop3 f12"><?php

          makeColumnOrderable(
             "Issues/Notes",
             $sortIndex,
             "notes",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext('Student') . " Notes",
             true
          );

          $rawTitles['notes'] = "Issues/Notes";

        ?></th><?php

     }

     // Overall Result Title
     if ($displaygrade) { ?>

     <th class="titletop3 f12"><?php
         makeColumnOrderable(
             gettext("Grade"),
             $sortIndex,
             "overallgrade",
             null,
             $sortType,
             $rawResults,
             "",
             "standard",
             "Click To Sort::" . gettext("Overall Grade"),
             true
         );

     ?></th><?php

     }

     $rawTitles['overallgrade'] = gettext("Overall Grade");


     if (($examHasGradeRules && $displaygrade) || $displayweighted) {

         ?><th class="titletop3 f12"><?php
             makeColumnOrderable(
                 "Source/Trigger",
                 $sortIndex,
                 "trigger",
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "standard",
                 "Click To Sort::Source/Trigger",
                 true
             );

         ?></th><?php

           $rawTitles['trigger'] = "Source/Trigger";

      }


     // Total Title
     ?><th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Total Raw Score",
             $sortIndex,
             "overallscore",
             null,
             $sortType,
             $rawResults,
             "",
             "grouped",
             "Click To Sort::Overall Raw Score Marks",
             true
         );

         $rawTitles['overallscore'] = "Total Raw Score";

     ?></th><?php

     // Weighted Overall Score
     if ($displayweighted) { ?>

         <th class="titletop3 f12"><?php
           makeColumnOrderable(
              "Total " . gettext('Weighted') . " Score",
              $sortIndex,
              "overallweighted",
              null,
              $sortType,
              $rawResults,
              "",
              "grouped",
              "Click To Sort::" . gettext('Weighted') . " Overall",
              true
          );
          ?></th><?php

          $rawTitles['overallweighted'] = "Total " . gettext('Weighted') . " Score";

     }

     // Total Title %
     if ($displaypercent) { ?>

       <th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Total % Score",
             $sortIndex,
             "overallpercent",
             null,
             $sortType,
             $rawResults,
             "",
             "grouped",
             "Click To Sort::Overall Score % Marks",
             true
         );
       ?></th><?php

       $rawTitles['overallpercent'] = "Total % Score";

     }

      // Failed Title
     if ($displaystationsfailed) { ?>

     <th class="titletop3 f12"><?php
         makeColumnOrderable(
             "Failed",
             $sortIndex,
             "failed",
             null,
             $sortType,
             $rawResults,
             "",
             "grouped",
             "Click To Sort::Station Failed Count",
             true
         );

         $rawTitles['failed'] = "Failed";

     ?></th><?php

     }

     // Render station titles
     foreach ($stationIdentifiers as $stationIdentifier) {

         // Maximum number of multiple examiners per station
         $multiMax = $stationMultiExaminerCount[$stationIdentifier];

         // Station Index
         $stationIndex = '_' . $stationIdentifier;

         // Export Titles for station 'number', 'scenario' and 'station group'
         $rawTitles['number' . $stationIndex] = "Station Number";
         $rawTitles['tag' . $stationIndex] = gettext("Station Group");
         $rawTitles['scenario' . $stationIndex] = "Scenario";
         $rawTitles['stationvalue' . $stationIndex] = "Score";

         if ($displayweighted) $rawTitles['stationweighting' . $stationIndex] = gettext('Weighted');

         if ($displaypercent) $rawTitles['stationpercent' . $stationIndex] = "Score %";

         if ($displaygrade) $rawTitles['stationpassed' . $stationIndex] = "Grade";

         //If combining by station number or tag
         if (in_array($cnm, [STATION_COMBINE_DEFAULT, STATION_COMBINE_TAG])) {

             //Station names / hover message
             $hoverMessage = implode(
                 "<br/>",
                 array_column($filtersScenariosMap[$stationIdentifier], "form_name")
             );

             //If by forms
         } else {

             $hoverMessage = $formsFiltersMap[$stationIdentifier]['form_name'];

         }

         // Station Final Score
         // If Exam contains multiple examiner results output default station score column
         ?><th class="titletop3 f12 italic"><?php
             makeColumnOrderable(
                 "Score",
                 $sortIndex,
                 "stationpercent" . $stationIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "grouped",
                 "Click To Sort Station Score::" . $hoverMessage,
                 true
             );
         ?></th><?php


         if ($displayweighted) {

             ?><th class="titletop3 f12 italic"><?php
             makeColumnOrderable(
                 gettext('Weighted'),
                 $sortIndex,
                 "stationweighting" . $stationIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "grouped",
                 "Click To Sort " . gettext('Weighting') . "::" . $hoverMessage,
                 true
             );

             ?></th><?php

         }

         // Loop through multi results
         for ($multiCount=1; $multiCount<=$multiMax; $multiCount++) {

             // Multi Result Index
             $multiIndex = '_' . $stationIdentifier . '_' . $multiCount;

             // Title Number
             $titleNum = ($multiMax > 1) ? $multiCount : "";

             if ($multiMax > 1) {

               // Display Examiner Score & Percentage
               ?><th class="titletop3"><?php

                 $examinerScoreTitle = "result " . $titleNum;
                 $examinerPercentTitle = "result " . $titleNum . " %";
                 $examinerGradeTitle = "grade " . $titleNum;

                 makeColumnOrderable(
                    $examinerScoreTitle,
                    $sortIndex,
                    "examinerpercent".$multiIndex,
                    null,
                    $sortType,
                    $rawResults,
                    "",
                    "grouped",
                    "Click To Sort::".$hoverMessage,
                    true
                 );

                 $rawTitles['examinerscore' . $multiIndex] = ucfirst($examinerScoreTitle);

                 if ($displaypercent) $rawTitles['examinerpercent' . $multiIndex] = ucfirst($examinerPercentTitle);

                 if ($displaygrade) $rawTitles['examinergrade' . $multiIndex] = ucfirst($examinerGradeTitle);

              ?></th><?php

         }

         // Display Examiner Title
         if ($displayexaminers) {

          ?><th class="titletop3"><?php

             $examinerTitle = gettext('examiner') . " " .$titleNum;

             makeColumnOrderable(
                 $examinerTitle,
                 $sortIndex,
                 "examinerid".$multiIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "standard",
                 "Click To Sort " . ucwords(gettext('examiners')) . "::".$hoverMessage,
                 true
             );

             $rawTitles['examiner'.$multiIndex] = ucfirst($examinerTitle);

          ?></th><?php

         }

         // Display Global Rating Scale Title
         if ($displaygrs) {

          ?><th class="titletop3"><?php

             $grsTitle = "grs " . $titleNum;

             makeColumnOrderable(
                 $grsTitle,
                 $sortIndex,
                 "grs".$multiIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "standard",
                 "Click To Sort Global Rating Scale::".$hoverMessage,
                 true
             );

             $rawTitles['grs'.$multiIndex] = strtoupper($grsTitle);

         ?></th><?php

         }

         // Display flags count
         if ($displayflags) {

          ?><th class="titletop3"><?php

             $flagsTitle = "flags " . $titleNum;

             makeColumnOrderable(
                 $flagsTitle,
                 $sortIndex,
                 "flags".$multiIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "grouped",
                 "Click To Sort Flags Count::".$hoverMessage,
                 true
             );

             $rawTitles['flags'.$multiIndex] = ucfirst($flagsTitle);

         ?></th><?php

         }

         // Display Feedback Title
         if ($displayfeedback) {

          ?><th class="titletop3"><?php

             $feedbackTitle = "feedback " . $titleNum;

             makeColumnOrderable(
                 $feedbackTitle,
                 $sortIndex,
                 "feedback".$multiIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "standard",
                 "Click To Sort Feedback::".$hoverMessage,
                 true
             );

             $rawTitles['feedback'.$multiIndex] = ucfirst($feedbackTitle);

         ?></th><?php

         }

         // Display First Submission Title
         if ($displaytimestamp) {

          ?><th class="titletop3"><?php
             $submissionTitle = "submitted " . $titleNum;

             makeColumnOrderable(
                 $submissionTitle,
                 $sortIndex,
                 "time".$multiIndex,
                 null,
                 $sortType,
                 $rawResults,
                 "",
                 "standard",
                 "Click To Sort submission time::".$hoverMessage,
                 true
             );

             $rawTitles['time'.$multiIndex] = ucfirst($submissionTitle);

          ?></th><?php

         }

       }

     }
 ?></tr><?php

 // Render each of the student's results
 foreach ($rawResults as &$eachResult) {

   ?><tr class="each-data-rw"><?php

     foreach ($eachResult as $key => $outputValue) {

         $keyParts = explode('_', $key);
         $columnPrefix = $keyParts[0];
         $columnSuffix = strstr($key, "_");
         $columnID = isset($keyParts[1]) ? $keyParts[1] : "";

         $multiResult = (isset($stationMultiExaminerCount[$columnID]) && $stationMultiExaminerCount[$columnID] > 1);
         $diverge = ($multiResult && $eachResult["stationdivergence_$columnID"]) ? 'style="background-color: #ffbf00"' : '';

         // Global rating scale
         if (preg_match("/^grs/i", $key) && $displaygrs) {

             $grsCss = $eachResult['failedgrs' . $columnSuffix] == 1 ? "fail" : "grs";

             $grsCss = preg_match("/red flag/i", $outputValue) ? "flag-cell" : $grsCss;

             ?><td class="<?=$grsCss?>" <?=$diverge?>><?=$outputValue;

               if (preg_match("/red flag/i", $outputValue)) {

                    ?>
                     <img src="storage/custom/images/flag-small.png" title="flag" alt="flag"/>
                    <?php

               }

             ?></td><?php

         // Flags column
         } elseif (preg_match("/flags/i", $key) && $displayflags) {

             ?><td <?php if ($outputValue > 0) { ?>class="flag-cell"<?php } echo $diverge?>><?php

                 echo $outputValue;

                 if ($outputValue > 0) {
                   ?> x <img src="storage/custom/images/flag-small.png" title="flag" alt="flag"/><?php
                 }

             ?></td><?php

         // Examiners Column
         } elseif (preg_match("/examinerid/i", $key) && $displayexaminers) {

             ?><td <?=$diverge?>><?=$outputValue?></td><?php

         // Feedback column
         } elseif (preg_match("/feedback/i", $key) && $displayfeedback) {

             ?><td class="feedback" <?=$diverge?>><?=$outputValue?></td><?php

         // Timestamp column
         } elseif (preg_match("/time/i", $key) && $displaytimestamp) {

             //First Submission Column
             ?><td class="submission" <?=$diverge?>><?=$outputValue?></td><?php

         // Station Weighting column
         } elseif (preg_match("/(stationweighting)/i", $key) && $displayweighted) {

             ?><td class="c" <?=$diverge?>><?=$outputValue?></td><?php

         // Examiner Score / Station Scores Column
         } elseif (preg_match("/(examinerscore|stationscore)/i", $key)) {

             $linkID = $cssClass = "";

             // Display Link (default true)
             $displayLink = true;

             // Link Title
             $title = "Click to view result for " . gettext('student') . ": " . $eachResult['studentid'];

                 // Examiner Score Link
                 if ($multiResult && $columnPrefix == "examinerscore") {

                     // If we have an 'NA' - No Result
                     if ($outputValue == "NA") {

                         $displayLink = false;

                     } else {

                         // Link ID for examiner
                         $linkID = $eachResult['stationresultid' . $columnSuffix];

                         // If we have an Absent
                         if ($outputValue == "ABSENT") {

                             $cssClass = "fail";

                         } else {
                             // Examiner percent and class
                             $outputValue .= $displaypercent ? " (" . $eachResult['examinerpercent' . $columnSuffix] . "%)" : "";

                              // Assessment form is completed (1) or incomplete (0)

                             /* CSS colour class to use:
                              * If we have a complete result use the 'pass' or 'fail' class
                              * If we have an incomplete result use the 'incomplete' class
                              */
                             if ($eachResult['completed' . $columnSuffix]) {

                               $cssClass = $eachResult['examinerpassed' . $columnSuffix];

                             } else {

                               $cssClass = 'incomplete';

                             }

                         }
                     }

                     // Render Score Link Template
                     $templateData = [
                         'displayLink' => $displayLink,
                         'cssClass' => $cssClass,
                         'style' => $diverge,
                         'title' => $title,
                         'outputValue' => $outputValue,
                         'linkID' => $linkID,
                         'scoreLinkType' => false
                     ];

                     $rawDataLinkTemplate->render($templateData);

                 // Station Score Link (if multiple examiners)
                 } elseif ($columnPrefix == "stationscore") {

                     // If we have an 'NA' - No Result
                     if ($outputValue == "NA") {

                         $displayLink = false;

                     } else {

                         // Encode student ID and Station ID into Json
                         $linkID = base64_encode(
                             json_encode(
                                 [
                                     "station_id" => $eachResult['stationid' . $columnSuffix],
                                     "student_id" => $eachResult['studentid']
                                 ]
                             )
                         );

                         // If we have an Absent
                         if ($outputValue == "ABSENT") {

                             $cssClass = "fail";

                         } else {

                             // Single result but not been completed
                             if (!$multiResult && !$eachResult['completed' . $columnSuffix . "_1"]) {

                                $cssClass = "incomplete";

                             // Passed or failed final score
                             } else {

                                $cssClass = $eachResult['stationpassed' . $columnSuffix];

                             }

                             // Station percent and class
                             $outputValue .= $displaypercent ? " (" . $eachResult['stationpercent' . $columnSuffix] . "%)" : "";

                         }

                     }

                     // Render Score Link Template
                     $templateData = [
                         'displayLink' => $displayLink,
                         'cssClass' => $cssClass,
                         'style' => $diverge,
                         'title' => $title,
                         'outputValue' => $outputValue,
                         'linkID' => $linkID,
                         'scoreLinkType' => true
                     ];
                     $rawDataLinkTemplate->render($templateData);

                 }

         // Overall grade column, raw score total column and overall column %
         } elseif ($key == "overallscore" || ($displayweighted && $key == "overallweighted") ||
             ($displaypercent && $key == "overallpercent") || ($displaygrade && $key == "overallgrade")) {

             ?><td class="c bglg2"><?php
               if ($outputValue == "ABSENT") {

                  ?><span class="fail"><?=$outputValue?></span><?php

               } elseif ($outputValue == "NA") {

                  echo "NA";

               } else {

                  ?><span class="<?=str_replace(' ', '-', $eachResult['overallgrade'])?>">
                   <?=$eachResult[$key]?>
                   </span><?php

               }

             ?></td><?php

             // Overall grade trigger
         } elseif (preg_match("/trigger/i", $key) && ($displaygrade || $displayweighted)) {

             ?><td class="notes <?=str_replace(' ', '-', $eachResult['overallgrade'])?>">
                <?=$outputValue?>
             </td><?php

         // Student notes/issues
         } elseif (preg_match("/notes/i", $key) && $displaynotes) {

             ?><td class="bglg2 notes"><?=$outputValue?></td><?php

         // Failed Column
         } elseif ($key == "failed") {

             ?><td class="c bglg2"><?php
                   ?><span <?php if ($outputValue > 0) {
                      ?>class="fail"<?php
                 } ?>><?=$outputValue?></span><?php
             ?></td><?php

         // Student ID Column
         } elseif ($key == "studentid") {

          ?><td class="bglg2"><?php
              $title = "Click to view all results for " . gettext('student') . ": ".$outputValue;
              ?><a href="javascript:void(0)" class="examineel" title="<?=$title?>"><?=$outputValue?></a>
            </td><?php

         } elseif (in_array($key, ["nationality", "order"])) {

          // Nationality or Order Column
          ?><td class="bglg2 c"><?=checkout($outputValue)?></td><?php

         // Everything Else
         } else {

             $ignoreColumns = [
                 "stationresultid", "complete", "overallgrade", "overallpercent",
                 "overallweighted", "stationweighting", "examinerpercent",
                 "examinerpassed", "scorepercent", "stationcode", "stationnumber",
                 "stationpercent", "stationid", "stationpassed", "stationform",
                 "stationgrsfail", "failedgrs", "stationdivergence"
             ];

             if (!preg_match("/(" . implode("|", $ignoreColumns). ")/i", $key)) {

                 // Ignore these columns
                 ?><td class="bglg2"><?=checkout($outputValue)?></td><?php

             }

         }

         /**
          * Remove data unrequired in export
          *
          */
         if (!$multiResult && preg_match("/(examinerscore|examinerpassed|examinerpercent)/i", $key))
             unset($eachResult[$key]);

     }

     ?></tr><?php

 }

 ?></table>
