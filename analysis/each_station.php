<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 use \OMIS\Database\Results as Results;
 use \OMIS\Database\Exams as Exams;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

    return false;

 }
                    
 $formSections = $db->forms->getFormSections([$stationRecord['form_id']]);
 $formPossible = $overallFormPossible = 0;
 $accumulations = $sectionFinalScores = [];

 // Self assessment
 $formSelfAssess = $db->selfAssessments->scoresheetHasSelfAssessment($stationRecord['form_id']);
 $ownerID = $formSelfAssess ? $db->selfAssessments->getOwnerByStudentExam($studentID, $examID) : "";

 ?><table class="exam-details-table table-border">
    <caption class="station-desc">
        Station <?=$stationRecord['station_number']?>: <?=$stationRecord['form_name']?>
    </caption>
 <?php

 // Loop through form sections
 while ($formSection = $db->fetch_row($formSections)) {

    $sectionTotal = $sectionPossible = 0;
    $titleBarStyles = "";
    $sectionItemCount = 1;
    $isScoreItem = false;
   
    // Get form items
    $items = $db->forms->getSectionItems([$formSection['section_id']]);
    
    // Prepare array for each form section
    $sectionAccumulatedScores = array_fill(0, count($studentStationResults), 0);

    // Prepare Section Title
    $sectionTitle_parts = [$formSection['competence_desc'], $formSection['section_text']];
    $sectionTitle = implode(" : ", array_filter($sectionTitle_parts));

    // Scale class for scale bar
    if ($formSection['competence_type'] == "scale") {

        $titleBarStyles .= " scalebar";
        $weightedTitle = "";
   
    } else {

        $weightedTitle = (($rules['multi_examiner_results_averaged']) ? "Weighted" : "Aggregate") . " Score";

    }
   
    // Self assessment bar
    if ($formSection['self_assessment']) {
      
       $titleBarStyles .= " selfassessment-bar";
        
    }
    

  /** 
   * Assessment Form Section Title Bar
   */  
  ?>
 <tr>
    <th class="title-section<?=$titleBarStyles?>"><?php 

      if ($formSection['self_assessment']) {
                    
          ?><a id="self-nav" name="self"></a><?php

      }

      echo $sectionTitle;

    ?></th>
 <?php

    $examiners = array_column($studentStationResults, 'examiner_id');
    $multipleExaminers = (count($examiners) > 1);
    $examinerIndex = 1;

    // Print the examiners index numbers and user IDs as column labels
    foreach ($examiners as $examiner) {

        $isObserver = (isset($accExaminerWeightings[$examiner]) &&
            $accExaminerWeightings[$examiner] == Exams::EXAMINER_WEIGHTING_OBSERVER);

        ?><th class="title-section tac<?=$titleBarStyles?>"><?php

            echo $multipleExaminers ? gettext('Examiner') . " #" .$examinerIndex : "";
            echo $isObserver ? " <span class='red normal'>Observer (0)</span>" : "";

        ?></th><?php

        $examinerIndex++;

    }
    
   /**
    * Weighted scores only when we have multiple examiners
    */    
    if ($multipleExaminers) {

       ?><th class="title-section tac<?=$titleBarStyles?>"><?=$weightedTitle?></th><?php

    }

  ?>
 </tr>
 <?php

 // Loop through items
 while ($item = $db->fetch_row($items)) {

    $itemPossible = max($item['lowest_value'], $item['highest_value']);
    $sectionPossible += $itemPossible;
    $itemScoresWeightings = [];
    $itemCommentsHtml = "";
    $resultCounter = 0;

    ?><tr class="<?=($sectionItemCount%2 == 0 ? "lgrey" : "")?>">
        <td class="items"><?php

           // If self assessment section
           if ($formSection['self_assessment']) {

                $question = $db->selfAssessments->getQuestionByOrderAndStudent(
                    $examID,
                    $item['item_order'],
                    $studentID
                );

                if (empty($question)) {

                    echo adjustHTML($item['text']);

                } else {

                    ?><div class="selfassessment-question"><?=$question['question']?></div>
                    <div class="selfassessment-response"><?=gettext('Student')?> Response: <span>"<?=$question['response']?>"</span></div>
                    <div class="selfassessment-score"><?=gettext('Student')?> Score: <span><?=$question['score']?></span></div><?php

                }

           } else {

                echo adjustHTML($item['text']);

           }

        ?></td>
    <?php

      foreach ($studentStationResults as $resultRecord) {

         $isObserver = (isset($accExaminerWeightings[$resultRecord['examiner_id']]) &&
         $accExaminerWeightings[$resultRecord['examiner_id']] == Exams::EXAMINER_WEIGHTING_OBSERVER);

         $notSelfAssessOwner = ($formSection['self_assessment'] && $ownerID != $resultRecord['examiner_id']);
         $overallFormPossible += ($isObserver || $notSelfAssessOwner) ? 0 : $itemPossible;

         ?><td class="<?php

           echo $isObserver ? "observer-cell " : ""?>options<?php
           echo $multipleExaminers ? "" : " nbr"?>">

        <?php

          // Result ID
          $resultID = $resultRecord['result_id'];

          // Default item score
          $eachItemScore = 0;

          // If text box or slider
          if (in_array($item['type'], ["text", "slider"])) {

                $itemScoreData = $db->results->getItemScore($item['item_id'], $resultID);
                $isScoreItem = true;

                if (is_array($itemScoreData) && isset($itemScoreData['option_value'])) {

                    $eachItemScore = $itemScoreData['option_value'];
                    $sectionTotal += $itemScoreData['option_value'];
                    $sectionAccumulatedScores[$resultCounter] += $itemScoreData['option_value'];
                    $textvalue = $itemScoreData['option_value'];

                } else {

                    $textvalue = "NA";

                }

                ?><div class="optxtdesc">Item Score</div>
                <div class="divval"><?=$textvalue?></div><?php

            } elseif (in_array($item['type'], ["radio", "radiotext"])) {

                // If Radio or Radiotext
                $itemScores = $db->fetch_row(
                     $db->results->getItemScoreData($item['item_id'], $resultID)
                );

                if ($item['type'] == "radio") {

                    $sectionTotal += $itemScores['option_value'];
                    $eachItemScore = $itemScores['option_value'];
                    $sectionAccumulatedScores[$resultCounter] += $itemScores['option_value'];
                    $isScoreItem = true;

                } else {

                    $isScoreItem = false;
                    $sectionTotal += 0;

                }

                if ($itemScores['option_value'] == "false") {

                    $radiovalue = "NA";

                }

            ?><div class="optxtdesc"><?php

                 if (is_numeric($itemScores['descriptor'])) {

                     ?><span class="describe-numeric">Descriptor: </span><?php

                 }

                 echo ucfirst($itemScores['descriptor']);

              ?></div><?php

                if ($item['grs'] == 0) {

                    ?><div class="divval"><?=$itemScores['option_value']?></div><?php

                }

                if ($itemScores['flag'] > 0) {

                  ?><img title="This is a flag item" height="36" width="36" src="storage/custom/images/flag-large.png"/><?php

                }

            // Checkbox item
            } else if ($item['type'] == "checkbox") {

                $isScoreItem = false;
                $itemScoreData = $db->results->getItemScore($item['item_id'], $resultID);

                if (is_array($itemScoreData) && isset($itemScoreData['option_value']) && strlen($itemScoreData['option_value']) > 0) {

                    ?><div class="optxtdesc green">Checked</div>
                    <?php

                }

           }

          // Examiner weighting for each item
          $examinerID = $resultRecord['examiner_id'];

          // If an examiner is not known to the station, assign them a "default" weighting.
          if (!isset($accExaminerWeightings[$examinerID])) {

             $accExaminerWeightings[$examinerID] = Exams::EXAMINER_WEIGHTING_DEFAULT;

          }

          $itemScoresWeightings[] = [
             'score' => $eachItemScore,
             'weighting' => $accExaminerWeightings[$examinerID]
          ];

         ?></td><?php

         $resultCounter++;

         // Examiner item feedback
         $itemFeedback = $db->feedback->getItemFeedback(
              $item['item_id'],
              $resultID
         );

         if (strlen($itemFeedback) > 0) {

             $itemCommentsHtml .= "<p><span class=\"examiner-comment\">"
                 . gettext('examiner') . " "
                 . (($multipleExaminers) ? "#" . $resultCounter : "")
                 . " feedback &rarr;</span> "
                 . $itemFeedback . "</p>";

         }

   }

   /**
    * Weighted scores only when we have multiple examiners
    */
   if ($multipleExaminers) {

        ?><td class="options nbr"><?php

            // If item is gradable then render final item score
            if ($isScoreItem && $item['grs'] == 0) {

                list($finalItemScore,) = Results::determineScoreAndPossible(
                    $rules['multi_examiner_results_averaged'],
                    $itemScoresWeightings
                );

                // Add weighted score to acc weighted score array  
                setA($sectionFinalScores, $formSection['section_id'], 0);
                $sectionFinalScores[$formSection['section_id']] += $finalItemScore;

                ?>
                <div class="optxtdesc">&nbsp;</div>
                <div class="divval"><?=sprintf("%.3g", $finalItemScore)?></div>
                <?php

            }

        ?></td>
    <?php } ?>
    </tr><?php

      // Item score
      if ($isScoreItem)
        $accumulations[$formSection['section_id']] = $sectionAccumulatedScores;

      // Render item comments complete
      if (strlen($itemCommentsHtml) > 0) {

        ?><tr>
            <td class="comment" colspan="<?=(1 + $resultCounter + (int)($multipleExaminers))?>">
                <?=$itemCommentsHtml?>
            </td>
        </tr><?php

      }

      $sectionItemCount++;

    }

    // Sum section possibles
    $formPossible += $sectionPossible;

    /**
     *  Gather section examiner feedback/comments and render
     */
    $examinerCount = 0;
    $commentsHtml = "";
    
    foreach ($studentStationResults as $resultRecord) {
    
        $examinerCount++;

        $examinerFeedback = $db->feedback->getSectionFeedback(
            $formSection['section_id'], 
            $resultRecord['result_id']
        );
    
        if (strlen($examinerFeedback) > 0) {

            $commentsHtml .= "<p><span class=\"examiner-comment\">"
                        . gettext('Examiner') . " "
                        . (($multipleExaminers) ? "#" . $examinerCount : "")
                        . " Feedback &rarr;</span> "
                        . $examinerFeedback . "</p>";

        }
        
    }
      
    // Examiner comments
    if (strlen($commentsHtml) > 0) {

        $examinerCount = ($multipleExaminers) ? $examinerCount + 1 : $examinerCount;
        ?><tr>
        <td class="comment" colspan="<?=(1+$examinerCount)?>">
            <?=$commentsHtml?>
        </td>
        </tr><?php
        
    }
  
 }

  // Bottom Total Result Section
  ?><tr>
     <td class="tdscorebuttons">&nbsp;</td><?php

     // Output multi examiner totals
     $resultIndex = 0;
     foreach ($studentStationResults as $resultRecord) {

       $formExaminerTotal = array_sum(array_column($accumulations, $resultIndex));
       $formExaminerPossible = ($formSelfAssess && $ownerID != $resultRecord['examiner_id'])
       ? $db->selfAssessments->getNonOwnerPossible($stationRecord['form_id']) : $formPossible;

       $formExaminerPercent = percent(
           $formExaminerTotal,
           $formExaminerPossible
       );
       
       $exPassDetermined = Results::determinePassOrFail(
           $formExaminerTotal,
           $stationRecord['pass_value'],
           $formExaminerPossible,
           1
       );

       $isObserver = (isset($accExaminerWeightings[$resultRecord['examiner_id']]) &&
             $accExaminerWeightings[$resultRecord['examiner_id']] == Exams::EXAMINER_WEIGHTING_OBSERVER);

       ?><td class="tdscorebuttons<?php
            echo $multipleExaminers ? "" : " nbr";
            echo $isObserver ? " observer-cell" : "";
            ?>">
         <div class="totalscore">
           <div class="scorevalue f16<?php

              echo $isObserver ? " strike" : "";

           ?>">
              <div class="innervaldiv">
                <span class="<?php
                  echo $exPassDetermined ? "green" : "red";
                 ?>"><?php
                  echo $formExaminerPercent;
                ?>%</span>&nbsp;&nbsp;<?php
                 echo round($formExaminerTotal, 1);
              ?> </div> / <?php
                 echo round($formExaminerPossible, 1);
               ?></div>
         </div>
       </td><?php

       $resultIndex++;

    }

    // Output overall formulated/averaged score
    $overallFinalScore = array_sum($sectionFinalScores);

    $finalFormPossible = $rules['multi_examiner_results_averaged'] ? $formPossible :
         $overallFormPossible;

    $overallPercent = percent(
         $overallFinalScore,
         $finalFormPossible
    );

    $passDetermined = Results::determinePassOrFail(
            $overallFinalScore,
            $stationRecord['pass_value'],
            $finalFormPossible,
            1
    );

    $resultSpanClass = $passDetermined ? "green" : "red";

    /** 
     * Weighting Column Total:
     * display only when we have multiple examiners
     */
     if ($multipleExaminers) {

        ?><td class="tdscorebuttons nbr">
            <div class="totalscore">
            <div class="scorevalue f16">
                <div class="innervaldiv">
                    <span class="<?=$resultSpanClass?>"><?php
                    echo $overallPercent;
                    ?>%</span>&nbsp;&nbsp;<?php
                    echo round($overallFinalScore, 1);
                ?> </div> / <?php
                    echo $finalFormPossible;
                ?></div>
            </div>
            </td>
        <?php 
    
      }

    ?>
    </tr>
 </table>