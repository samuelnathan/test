<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
 define('_OMIS', 1);
 header('Content-type: application/json');
 
 include_once __DIR__ . '/../vendor/autoload.php';
 
 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
 use \OMIS\Utilities\JSON as JSON;

 $exam = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);

 if (!empty($exam)) {
 
    include __DIR__ . '/../extra/essentials.php';
    
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
        exit();
    
    }
 
 } else {
 
    http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);   
    exit();
 
 }
  
 // Get list of questions and applicant responses and restructure
 $db->set_Convert_Html_Entities(false);
 $selfAssessmentlist = $db->selfAssessments->listByExam($exam);
 
 $data = $returnQuestions = $returnResponses = [];
 $applicantData = count($selfAssessmentlist['applicants']) > 0 ? $selfAssessmentlist['applicants'] : [];
 
 foreach ($applicantData as $id => $applicant) {
   
    foreach ($applicant as $question) {

         $questionID = $question['question']['question_id'];

         // Add to unique set of questions
         if (!in_array($question['question']['text'], array_column($returnQuestions, 'text'))) {

             unset($question['question']['question_id']);
             $returnQuestions["$questionID"] = $question['question'];

         }

         // Add Applicant responses
         $returnResponses["$id"]["$questionID"] = [
            'text' => $question['response']['text'],
            'score' => $question['response']['score']
         ];

    }
 
 }

 // If we have incomplete set of data, then return empty
 if (!empty($returnQuestions) && !empty($returnResponses)) {
 
    $data = [
      'questions' => $returnQuestions, 
      'responses' => $returnResponses,
      'studentsWithout' => $db->selfAssessments->studentsWithout($exam)
    ];

}
  
 /**
  * Encode JSON and handle any errors
  */
 try {
   
    $json = JSON::encode($data);
    
 } catch (Exception $ex) {
   
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($data, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
    
 }
 
 echo $json;
 