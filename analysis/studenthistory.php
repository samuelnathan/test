<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use OMIS\Database\CoreDB as CoreDB;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION["user_role"])->canAccess())
     return false;

 // Prepare some variables
 $history = [];
 $studentID = iS($_GET, "student");
 $back = (INT) iS($_GET, "back");
 $studentDB = $db->students->getStudent($studentID);
 $nameSeparated = strlen($studentDB['surname']) > 0 && strlen($studentDB['forename']) > 0 ? ", " : "";
 $studentName = $studentDB["surname"] . $nameSeparated . $studentDB["forename"];

 // Check if Student Exists
 if ($studentDB != NULL && count($studentDB) > 0) {

     // Get Student Exams
     $exams = $db->results->getAllStudentResults($studentID);

     // Put together Student results data
     while ($exam = $db->fetch_row($exams)) {

         // Exam rules
         $rules = $db->examRules->getRules($exam['exam_id']);

         // Initialize variables
         $tempStationsArray = [];
         $dateDescription = formatDate($exam["session_date"]) .
                            ((strlen($exam["session_description"]) > 0) ?
                            " - " . $exam["session_description"] : $exam["session_description"]);

         // Possibles
         $examPossible = 0;
         $examScore = 0;

         // Load the Exam results from the database
         $examResultsDB = CoreDB::group(
             CoreDB::into_array(
                 $db->results->getResultsByExam($studentID, $exam['exam_id'])
             ), ['station_id'], true
         );

         // Loop through results
         foreach($examResultsDB as $stationID => $stationResults) {

             // Station score
             $stationScore = 0;

             // Multi Station Count
             $multiStationCount = count($stationResults);

             // Weighted score is required based on rules setting
             if ($rules['multi_examiner_results_averaged']) {

                  $stationPossible = $stationResults[0]["total_value"];
                  $finalStationScore = $db->results->getMeanScoreForStudentStation(
                      $studentID,
                      $stationID
                  );

             // Aggregate score is required based on rules setting
             } else {

                 $stationPossible = $db->results->getAggregatePossibleForStudentStation(
                     $exam['exam_id'],
                     $studentID,
                     $stationID
                 );

                  $finalStationScore = $db->results->getAggregateScoreForStudentStation(
                     $studentID,
                     $stationID
                  );

             }

             // Accumulators
             $examPossible += $stationPossible;
             $examScore += $finalStationScore;

             // Station Data
             $tempStationsArray[$stationID] = [
                 "station_number" => $stationResults[0]["station_number"],
                 "station_code" => $stationResults[0]["station_code"],
                 "form_name" => $stationResults[0]["form_name"],
                 "examiner_count" => $multiStationCount,
                 "score" => (float)$finalStationScore,
                 "station_possible" => (float)$stationPossible
             ];
         }

         // Pass Exam Results & Station Data to "history" array
         $history[$exam["exam_id"]]["stations"] = $tempStationsArray;

         $history[$exam["exam_id"]]["exam"] = [
             "exam_name" => $exam["exam_name"],
             "term_id" => $exam["term_id"],
             "session" => $dateDescription,
             "dept_id" => $exam["dept_id"],
             "exam_possible" => (float)$examPossible,
             "exam_score" => (float)$examScore
         ];
     }
 ?>
     <div class="topmain">
     <table id="examinee-summary-history" class="table-border">
      <tr>
         <th class="title"><?=gettext('Student')?> Identifier</th>
         <td class="rtd"><?=$studentID?></td>
      </tr>
      <tr>
         <th class="title"><?=gettext('Student')?> Name</th>
         <td class="rtd"><?=isset($_REQUEST['anon']) ? "-" : $studentName?>
        </td>
      </tr>
      <tr>
         <th class="title"><?=gettext('Student')?> Gender</th>
         <td class="rtd"><?=$studentDB["gender"]?></td>
      </tr>
      <tr>
         <th class="title"><?=gettext('Student')?> Nationality</th>
         <td class="rtd nbb"><?=$studentDB["nationality"]?></td>
      </tr>
      </table>
      <div class="print-div"><?php

      if($back) {
         ?><input type="button" id="back-button" value="Back" class="btn btn-secondary btn-sm"/><?php
      }
 		?><input type="button" id="print-sres" value="Print" class="btn btn-success btn-sm"/>
           <input type="hidden" id="surname" value="<?=(isset($_REQUEST['anon']) || empty($studentName) ? $studentID : $studentName)?>"/>

      </div>
 	 <br class="clearv2"/>
   </div>

  <div class="contentdiv">
   <div class="results-container"><?php

   // Check Result Count
   if (count($exams) > 0) {

     ?>
     <div class="h6">Results History</div>
     <table class="tsr" id="student-history-results-table">
        <tr class="titlerow">
              <th><?=gettext('Exam')?> Name</th>
              <th><?=ucwords(gettext('academic term'))?></th>
              <th>Day/Circuit</th>
              <th><?=gettext('Department')?></th>
              <th>Raw Score</th>
              <th>Score %</th>
        </tr><?php

         // Loop Through All Exams
         foreach ($history as $examID => $data) {

          // Exam Data
          $exam = $data["exam"];

          // Student Exam Url
          $studentExamUrl = "manage.php?page=studentexam&amp;student=" 
            . $studentID . "&amp;exam=" . $examID . "&amp;back=1"
            . (isset($_REQUEST['anon']) ? "&amp;anon=" : '');

          // Exam Percentage Score
          $examPercentScore = percent($exam["exam_score"], $exam["exam_possible"]);

           // Titles
           ?><tr>
             <td class="nbl"><a href="<?=$studentExamUrl?>"><?=$exam["exam_name"]?></a></td>
             <td class="c"><?=$exam["term_id"]?></td>
             <td><?=$exam["session"]?></td>
             <td class="c"><?=$exam["dept_id"]?></td>
             <td class="c"><?=$exam["exam_score"]?> / <?=$exam["exam_possible"]?></td>
             <td class="c"><?=$examPercentScore?> %</td>
            </tr><?php

         }
         ?></table><?php

    // Loop through all exams
    foreach ($history as $examID => $data) {

       $exam = $data["exam"];
       $stations = $data["stations"];

       // Titles
      ?><table class="tsr">
       <caption class="f13"><?= $exam["exam_name"]?> : <?=$exam["term_id"]?></caption>
         <tr class="titlerow">
           <th>Station</th>
           <th>Station Group</th>
           <th><?=gettext('Scenario')?></th>
           <th><?=gettext('Examiner')?> Count</th>
           <th class="c">Raw Score</th>
           <th class="score-cell">Score %</th>
         </tr><?php

         // Loop through all stations
         foreach ($stations as $stationID => $stationResult) {

              // Encode student ID and Station ID into Json
              $resultsJsonID = base64_encode(json_encode([
                     "station_id" => $stationID,
                     "student_id" => $studentID,
              ]));

              // Station Url
              $stationUrl = "manage.php?page=studentresult&amp;results_id=" . $resultsJsonID
                 . "&amp;back=1" . (isset($_REQUEST['anon']) ? "&amp;anon=" : '');

              // Station Percentage Score
              $stationPercentScore = percent($stationResult["score"], $stationResult["station_possible"]);

             ?><tr>
                 <td class="station-nr"><a href="<?=$stationUrl?>">#<?=$stationResult["station_number"]?></a></td>
                 <td class="c"><?=$stationResult["station_code"]?></td>
                 <td><?=$stationResult["form_name"]?></td>
                 <td class="c"><?=$stationResult["examiner_count"]?></td>
                 <td class="c"><?=$stationResult["score"]?> / <?=$stationResult["station_possible"]?></td>
                 <td class="c"><?=$stationPercentScore?> %</td>
             </tr><?php

         }

         ?></table><?php

    }
   } else {
      ?><div class="bold orange">No results available for this <?=gettext('student')?></div><?php
   }
     ?></div>
     </div><?php

 } else {

    ?><div class="errordiv"><?=gettext('Student')?> with identifier <?=$studentID?> does not exist</div><?php

 }
