<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
     return false;
 }

 // [Maths Stats and Cronbach]
 $dataSet = new Math_Stats();
 $cronbach = new cronbach();

 // [Initial Variables]
 $summaryTable = [];
 $numOfStations = count($stationIdentifiers);
 $totalNumColumns = 13;
 $counter = 0;
 $totalWithResults = 0;
 $totalSum = 0;
 $possibleSum = 0;
 $overallFailed = 0;
 $overallPassed = 0;
 $overallGRSFails = 0;
 $cronbachsTotal = 0;
 $formCount = 0;

 // [Summary Titles]
 $summaryTable[0] = [
     '',
     "Nr. of Results",
     "Stations Passed",
     "Stations Failed",
     "GRS Clear Fails",
     "Mean Result",
     "Mean Result (%)",
     "SD",
     "SD (%)",
     "Min",
     "Max",
     "Range",
     "Mid Range"
 ];

 if ($displaycronbach) {
     $summaryTable[0][] = "Station Items<br/>Cronbach Alpha (&alpha;)";
     $totalNumColumns++;
 }

 // [Additional Cells]
 $secondLastRow = $numOfStations + 1;
 $lastRow = $numOfStations + 2;
 $preFill = array_fill(0, $totalNumColumns, '');
 $summaryTable[$secondLastRow] = $preFill;
 $summaryTable[$secondLastRow][0] = gettext('Exam') . " Totals &amp; Mean %";
 $summaryTable[$secondLastRow][1] = 'N/A';
 $summaryTable[$secondLastRow][2] = 'N/A';
 $summaryTable[$secondLastRow][3] = 'N/A';
 $summaryTable[$secondLastRow][4] = 'N/A';
 $summaryTable[$secondLastRow][5] = '';
 $summaryTable[$secondLastRow][6] = 'N/A';

 // [Student Pass|Fail row]
 $summaryTable[$lastRow] = $preFill;
 $summaryTable[$lastRow][0] = "Total " . gettext('Students') . " Passed | Failed";
 $summaryTable[$lastRow][2] = 'N/A';
 $summaryTable[$lastRow][3] = 'N/A';

 // [Cycle through stations]
 foreach ($stationIdentifiers as $id) {

     // [Initial Station Variables]
     $counter++;
     $totalStationPercent = 'N/A';
     $scenarioGroup = 'N/A';
     $stationTitle = '';
     $totalPossible = 0;
     $numPassed = 0;
     $numFailed = 0;
     $clearGRSFails = 0;
     $scoresColumnData = [];

     $summaryTable[$counter] = [
         '', 'NO RESULT', 'N/A', 'N/A',
         'N/A', 'N/A', 'N/A', 'N/A', 'N/A',
         'N/A', 'N/A', 'N/A', 'N/A'
     ];

     // Station Cronbach value to be passed
     $cronbachValue = 0;

     // [If Display Cronbach Alpha]
     if ($displaycronbach) {
         $summaryTable[$counter][13] = 'N/A';
     }

     // Get Scores from Scores Column
     $stationScoresColumn = array_filter(
             array_column(
                 $rawResults,
                 "stationscore_" . $id
          ),
          "is_numeric"
     );

     $resultColNum = $stationMultiExaminerCount[$id];

     // Discard incomplete results
     foreach ($stationScoresColumn as $key => $stationScore) {

         // Include score value for station
         $scoresColumnData[] = $stationScore;

         // Accumulate pass/fail values for complete scores
         $passFailIndex = 'stationpassed_' . $id;

         if ($rawResults[$key][$passFailIndex] == "pass") {

             $numPassed++;

         } else if ($rawResults[$key][$passFailIndex] == "fail") {

             $numFailed++;

         }

         // Accumulate GRS fail values
         $GRSFailsIndex = 'stationgrsfail_' . $id;

         if ($rawResults[$key][$GRSFailsIndex] == 1) {

            $clearGRSFails++;
            $overallGRSFails++;

         }

     }

     // [If Combining by station number / tag]
     if (in_array($cnm, [STATION_COMBINE_DEFAULT, STATION_COMBINE_TAG])) {

         // Scenario Group
         $scenarioGroup = $id;

         // Stations with this Scenario Group loop
         foreach ($filtersScenariosMap[$id] as $eachStationID => $stationData) {

             // Acumulate Station Name
             $stationTitle .= $id . ' - ' . $stationData['form_name'] . '<br/>';

             // Maximum Total Possible
             if ($stationData['total_value'] > $totalPossible) {
                 $totalPossible = $stationData['total_value'];
             }

             // Acumulate Cronbach
             if ($displaycronbach) {
                 $cronbachValue += $cronbach->calc_Alpha($db->results->getItemScoresMatrix(
                         $eachStationID,
                         $sessionsSelected,
                         $groupsUserSelected,
                         true
                 ));
             }
         }

         // Scenario Group & Names
         $summaryTable[$counter][0] = $stationTitle;

         // Cronbach Alpha
         if ($displaycronbach && $scoresColumnData != false) {

 	      // Number of stations with Scenario Group
               $numStations = count($filtersScenariosMap[$id]);

 	      // 13. Add Cronbach Alpha Value to Array (Average Cronbach)
               $meanCronbachsAlpha = $cronbachValue/$numStations;
               $cronbachsTotal += $meanCronbachsAlpha;
 	      $cronbachValue = roundAndFormat($meanCronbachsAlpha, 3);
 	      $summaryTable[$counter][13] = $cronbachValue;

         }
     } else { // If by Station Forms (Scoresheets)

         // Scenario Group
         $scenarioGroup = $formsFiltersMap[$id]['station_number'];

         // Scenario Group & Name
         $summaryTable[$counter][0] = $scenarioGroup.' - '.$formsFiltersMap[$id]['form_name'];

         // Aggregate or Weighting of total possibles
         if ($rules['multi_examiner_results_averaged']) {

             $totalPossible = $formsFiltersMap[$id]['total_value'];

         } else {

             $totalPossible = ($formsFiltersMap[$id]['total_value'] * $resultColNum);

         }

         // 13. Add Cronbach Alpha Value to Array
             if ($displaycronbach && $scoresColumnData != false) {
                $cronbachValue = $cronbach->calc_Alpha($db->results->getItemScoresMatrix(
                        $id,
                        $sessionsSelected,
                        $groupsUserSelected,
                        true
                ));
                $cronbachsTotal += $cronbachValue;
                $summaryTable[$counter][13] = $cronbachValue;
             }

     }

     // Station has result
     if ($scoresColumnData != false) {

         // Set Data
         $dataSet->setData($scoresColumnData);

         // Student Count
         $studentResultCount = sizeof($scoresColumnData);
         $formCount += $studentResultCount;

         // Calculate Possible Sum for each station
         $possibleSum += ($totalPossible * $dataSet->count());

         // 1. Student with result count
         $summaryTable[$counter][1] = $dataSet->count();

         // 2. Student Nr. passed
         $overallPassed += $numPassed;
         $summaryTable[$counter][2] = $numPassed;

         // 3. Student Nr. failed
         $overallFailed += $numFailed;
         $summaryTable[$counter][3] = $numFailed;

         // 4. GRS Clear Fails
         $summaryTable[$counter][4] = $clearGRSFails;

         // 5. Raw Average Result out of total possible
         $summaryTable[$counter][5] = roundAndFormat($dataSet->mean()).' / '. (float)$totalPossible;

         // 6. Percentage Average Result
         if ($totalPossible > 0) {
             $totalStationPercent = percent($dataSet->mean(), $totalPossible);
             $summaryTable[$counter][6] = $totalStationPercent . '%';
             $totalWithResults++;
             $totalSum += $dataSet->sum();
         }

         // 7. Raw stDevWithMean
         $standardDeviationWithMean = ($studentResultCount > 1) ? $dataSet->stDevWithMean($dataSet->mean()) : 0;
         $summaryTable[$counter][7] = roundAndFormat($standardDeviationWithMean);

         // 8. Calculate stDevWithMean as %
         $summaryTable[$counter][8] = percent($standardDeviationWithMean, $totalPossible) . '%';

         // 9. Min
         $summaryTable[$counter][9] = roundAndFormat($dataSet->min());

         // 10. Max
         $summaryTable[$counter][10] = roundAndFormat($dataSet->max());

         // 11. Calculate range
         $summaryTable[$counter][11] = roundAndFormat(($dataSet->max() - $dataSet->min()));

         // 12. Calculate mid range
         $summaryTable[$counter][12] = roundAndFormat($dataSet->midrange());

     }

     // [Graph Data Arrays]
     if ($scenarioGroup != 'N/A' && $totalStationPercent != 'N/A') {

         // Add graph array values [values will be passed to the graphs/stations_means.php file]
         $meansGraphData['stations'][] = [
             'scenario_group' => $scenarioGroup,
             'average' => $totalStationPercent
         ];

         // Add graph pass/fail array values [values will be passed to the graphs/stations_pass_fail.php file]
         $passFailGraphData['stations'][] = [
             'scenario_group' => $scenarioGroup,
             'fail_count' => $numFailed,
             'pass_count' => $numPassed
         ];

         // Add graph cronbachs array values [values will be passed to the graphs/stations_cronbach.php file]
         if ($displaycronbach) {
             $cronbachsGraphData['values'][] = [
                 'scenario_group' => $scenarioGroup,
                 'cronbach' => $cronbachValue
             ];
         }
     }
 }

 // [Overall Fail Pass & Average]
 if ($totalWithResults > 0) {
     $totalPercent = percent($totalSum, $possibleSum);
     $summaryTable[$secondLastRow][1] = $formCount;
     $summaryTable[$secondLastRow][2] = $overallPassed;
     $summaryTable[$secondLastRow][3] = $overallFailed;
     $summaryTable[$secondLastRow][4] = $overallGRSFails;
     $summaryTable[$secondLastRow][6] = $totalPercent . '%';

     // Add Mean Cronbachs Alpha
     if ($displaycronbach) {
       $summaryTable[$secondLastRow][13] = roundAndFormat(($cronbachsTotal/$numOfStations), 3);
     }

     // Overall Pass and Fail Count
     $overallPassArray = get_From_Array($rawResults, 'overallgrade', ['fail', 'pass'], '=');

     $passCount = count($overallPassArray[1]);
     $failCount = count($overallPassArray[0]);
     $totalCount = $passCount + $failCount;
     $passPercent = percent($passCount, $totalCount);
     $failPercent = percent($failCount, $totalCount);

     // Add pass percentage to summary table
     $summaryTable[$lastRow][2] = $passCount . ' (' . $passPercent . '%)';

     // Add fail percentage to summary table
     $summaryTable[$lastRow][3] = $failCount . ' (' . $failPercent . '%)';

     // Add overall average value to graph array [values will be passed to the graphs/stations_means.php file]
     $meansGraphData['overall_avg'] = $totalPercent;
     $meansGraphData['cnm'] = $cnm; // Combination/Filter Value

     // Add overall average value to graph array [values will be passed to the graphs/stations_pass_fail.php file]
     $passFailGraphData['total_students'] = $totalCount;
     $passFailGraphData['cnm'] = $cnm; // Combination/Filter Value

     // Add overall station count to graph array [values will be passed to the graphs/stations_failed.php file]
     $failedGraphData['station_count'] = $numOfStations;

     // Add Combination/Filter value to Cronbachs Graph Array [values will be passed to the graphs/stations_cronbach.php file]
     $cronbachsGraphData['cnm'] = $cnm;
 }
