<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

$data = [
    'displayexaminers' => $displayexaminers,
    'disabled_message' => $config->feature_disabled_message,
    'competency_results_exist' => $competencyResultsExist
];

$template = new OMIS\Template(OMIS\Template::findMatchingTemplate(__FILE__));
$template->render($data);
