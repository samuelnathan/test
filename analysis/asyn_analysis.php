 <?php
 /**
  * Analysis section Ajax & JSON
  *
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
 */

 define('_OMIS', 1);
 ob_start();
 include_once __DIR__ . '/../vendor/autoload.php';
 
 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
 use \OMIS\Auth\Role as Role;
 
 // Need to tell the server end that we want JSON back...
 header('Content-type: application/json');

 /* Settings for FILTER_VALIDATE_INT that will ensure that it doesn't let
  * negative numbers through...
  */
 $positiveInt = ['options' => ['min' => 1]];

 // Instantiate the configuration object if it's not already set.
 global $config;
 if (!isset($config)) {

    $config = new OMIS\Config();

 }

 // Do a session check to see if the user's logged in or not.
 global $session;
 if (!isset($session)) {

    $session = new OMIS\Session();
    $session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);

 }

 use OMIS\Database\CoreDB as CoreDB;
 use OMIS\Utilities\JSON as JSON;

 $isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

 if (!empty($isfor)) {

    include __DIR__ . '/../extra/essentials.php';

    if (!Role::loadID($_SESSION['user_role'])->canAccess($page)) {
        return false;
    }

 } else {

    include __DIR__ . '/../extra/noaccess.php';

 }

 // Instantiate the database access "class"...
 global $db;
 if (!isset($db)) {
    $db = CoreDB::getInstance(
        $config->db
    );
 }

 /* Iterate through the POST var checks for simplicity and to shrink the size of
  the upcoming if condition */
 $db->set_Convert_Html_Entities(false);
 $returnData = [];

 // Process Request
 switch ($isfor) {
   case 'stations': 
     
      $hasPost = \TRUE;
      foreach (['sessions', 'exam', 'cnm'] as $postVar) {
        
         $hasPost &= filter_has_var(INPUT_POST, $postVar);
         
      }
      
      if (!$hasPost) {
        
         http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
         exit();
         
      }
     
      $cnm = filter_input(INPUT_POST, 'cnm', FILTER_VALIDATE_INT, $positiveInt);
      $databaseCombination = ($cnm == 2) ? 'station_code' : 'station_number';
      $examID = filter_input(INPUT_POST, 'exam', FILTER_VALIDATE_INT, $positiveInt);
      
      if (!empty($examID) && $db->exams->doesExamExist($examID)) {
        
         $sessionsSelected = explode(',', $_POST['sessions']);
         $stationsDB = $db->exams->getStations($sessionsSelected, false, $databaseCombination);
         while ($station = $db->fetch_row($stationsDB)) {
           
             $returnData[] = [
                 'formid' => $station['form_id'],
                 'snum' => $station['station_number'],
                 'snam' => $station['form_name'],
                 'stag' => $station['station_code']
             ];
             
         }
         
      }
     
      break;
   case 'notifications':
      // Check that session identifiers are numeric
      $sessionIDs = explode(',', $_POST['sessions']);
      $sessions = array_filter($sessionIDs, function($sessionID) {
        return is_numeric($sessionID);
      });
      
      // Get notfications for exam
      $notifications = $db->examNotifications->bySessions($sessions, 'ASC');
      foreach ($notifications as $notification) {
         $returnData[] = [
            'id' => $notification['id'],
            'station_number' => $notification['station_number'],
            'student_id' => $notification['student_id'],
            'timestamp' => format_DateTime($notification['updated_at'], '/', false),
            'flag_type' => $notification['flag_type'],
            'flag_count' => $notification['flag_count'],
            'divergence_type' => $notification['divergence_type'],
            'divergence_threshold' => round($notification['divergence_threshold'], 2)
         ];
      }
            
      break;

   case 'update_student_notes':
        
        $student = filter_input(INPUT_POST, 'student', FILTER_SANITIZE_STRING);
        $exam = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_STRING);
        $notes = filter_input(INPUT_POST, 'notes', FILTER_SANITIZE_STRING);
        $author = $_SESSION['user_identifier'];

        // Update student notes
        if ($db->examdata->updateStudentData($student, $exam, $notes, $author)) {
    
            $returnData = $db->fetch_row(
                $db->examdata->byStudentExam($student, $exam)
            );

            $returnData['notes_updated'] = date_format(
                date_create($returnData['notes_updated']),
               "d/m/Y H:i:s"
            );

            $returnData['error'] = false;

        } else {

            $returnData['error'] = true;

        }
      
      break;
   case 'change_grade':

         $student = filter_input(INPUT_POST, 'student', FILTER_SANITIZE_STRING);
         $exam = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);
         $grade = filter_input(INPUT_POST, 'grade', FILTER_SANITIZE_STRING);
         $oldGrade = filter_input(INPUT_POST, 'last_updated_grade', FILTER_SANITIZE_STRING);
         $oldTS = filter_input(INPUT_POST, 'last_updated_timestamp', FILTER_SANITIZE_STRING);

         // Get current student data
         $examData = $db->fetch_row(
               $db->examdata->byStudentExam(
                   $student,
                   $exam
               )
         );

         // Has the grade changed from what is displayed
         if ($oldTS != $examData['grade_updated'] || strtolower($oldGrade) != strtolower($examData['grade'])) {

             $returnData['error'] = true;
             http_response_code(HttpStatusCode::HTTP_CONFLICT);
             exit();

         }

         // Valid grade check (hard coded for now)
         $valid = in_array(strtolower($grade), [
             "panel to decide",
             "appointable",
             "not appointable"
         ]);

         $permission = (strtolower($examData['grade']) == "panel to decide" || strlen($examData['grade_author']) > 0);

         // Update student grade when valid and with permission
         if ($valid && $permission && $db->gradeRules->updateGrade($exam, $student, $grade, false)) {

             $returnData['error'] = false;

         } else {

             error_log("We are here!");
             $returnData['error'] = true;

         }

         break;

   default:
      http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
      exit();
      break;
 }


 /**
  * Encode JSON and handle any errors
  */
 try {
    $json = JSON::encode($returnData);
 } catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnData, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
 }

 echo $json;
