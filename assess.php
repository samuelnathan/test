<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

define('_OMIS', 1);

$loader = require 'vendor/autoload.php';
$config = new OMIS\Config;
$session = new OMIS\Session;

/* OMIS could throw a wobbly if someone jumped to assess.php without having a
 * designated page (i.e. ?p=<something>) then PHP will throw (at the very least)
 * a notice and possibly worse.
 */
if (isset($_REQUEST['p']) && (file_exists("assess/" . $_REQUEST['p'] . ".php"))) {

    $pageFilter = filter_var($_REQUEST['p'], FILTER_SANITIZE_STRING);
    $page = $pageFilter === false ? "" : $pageFilter;

} else {

    // If no page is specified, fall back on the examination configuration page.
    $page = 'exam_configuration';

}

// Start output buffering for those pages that need it.
$bufferedPages = ['exam_configuration', 'exam_assessment', 'next', 'student_selection', 'examiners'];
if (in_array($page, $bufferedPages)) {
    ob_start();
}

// This is the check to see if user is not logged in.
$session->check(\OMIS\Session::SESSION_EXIT_REDIRECT);
require_once 'extra/header_assess.php';
require_once 'assess/' . $page . '.php';
?>
</body>
</html>
