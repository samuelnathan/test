<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 define('_OMIS', 1);
 include __DIR__ . '/../extra/essentials.php';

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 $errorList = [];
 $term = filter_input(INPUT_POST, 'term');
 $dept = filter_input(INPUT_POST, 'dept');

 if (empty($term) || empty($dept) || !$db->academicterms->existsById($term) || !$db->departments->existsById($dept)) {

     exit;

 }

 if (!is_dir('../storage/app/tmp/extract_to_dir/share')) {

     mkdir('../storage/app/tmp/extract_to_dir/share', 0777, true);

 }

 if (!is_dir('../storage/app/tmp/uploads')) {

     mkdir('../storage/app/tmp/uploads', 0777, true);

 }

 $allowedExts = ['zip'];
 $extension = end(explode('.', $_FILES['file']['name']));

 if (in_array($extension, $allowedExts)) {

     if ($_FILES['file']['error'] > 0) {

         $errorList[] = "Error: " . $_FILES['file']['error'];

     } else {

         $targetPath = '../storage/app/tmp/uploads/';
         $targetPath = $targetPath . basename($_FILES['file']['name']);

         if (!move_uploaded_file($_FILES['file']['tmp_name'], $targetPath)) {

             $errorList[] = "There was an error uploading the file, please try again!";

         }

     }

 } else {

     $errorList[] = "Invalid file";

 }

 $zip = new ZipArchive;
 $res = $zip->open('../storage/app/tmp/uploads/' . basename($_FILES['file']['name']));

 if ($res === true) {

     $zip->extractTo('../storage/app/tmp/extract_to_dir/');
     $zip->close();

 } else {

     $errorList[] = 'Extract failed';

 }

 $importedForms = [];
 $directory = '../storage/app/tmp/extract_to_dir/';
 $xmlFiles = glob($directory . '*.xml');

 if (count($xmlFiles) > 0) {

     $formNamesDB = $db->forms->getFormNamesByDeptTerm($dept, $term);

     // Loop through station files
     foreach ($xmlFiles as $xmlFile) {

         // Loads file and changes into array
         $xmlStr = file_get_contents($xmlFile);
         $xmlObj = simplexml_load_string($xmlStr);
         $xmlArray = objectsIntoArray($xmlObj);

         // Read and insert station / meta
         $xmlTitle = $xmlArray['meta']['title'];
         $xmlAuthor = $xmlArray['meta']['author'];
         $xmlFormDesc = $xmlArray['meta']['form_description'];

         $author = ($db->users->doesUserExist($xmlAuthor)) ? $xmlAuthor : $_SESSION['user_identifier'];

         // Look for duplicate station names
         $xmlTemp = $xmlTitle;
         $versionNum = 1;
         while (in_array($xmlTemp, $formNamesDB)) {

             $xmlTemp = "$xmlTitle ($versionNum)";
             $versionNum++;

         }

         $xmlFormDescription = (empty($xmlFormDesc)) ? '' : $xmlFormDesc;

          // Add form imported to import report
         $formInserted = $db->forms->insertForm(
             $xmlTemp,
             $author,
             $dept,
             $term,
             $xmlFormDescription
         );

         // Form was imported true/false
         $formWasInserted = (boolean) $formInserted[0];

         // Inserted form ID
         $insertedFormID = $formInserted[1];

         $importedForms[] = [
           'form_id' =>  $insertedFormID,
           'form_name' => $xmlTitle
         ];

         // Check xmlarray depth
         if (isset($xmlArray['competences']['competence']['competence_id'])) {

             $numOfSections = 1;

         } else {

             $numOfSections = count($xmlArray['competences']['competence']);

         }

         // Form was inserted
         if ($formWasInserted) {

             // Competencies Loop
             for ($sectionNum = 0; $sectionNum < $numOfSections; $sectionNum++) {

                 $section = $xmlArray['competences']['competence'];

                 if ($numOfSections == 1) {

                     $xmlCompetenceID = $section['competence_id'];
                     $xmlAdditionalInfo = $section['section_text'];
                     $xmlSectionFeedback = $section['section_feedback'];
                     $xmlItemFeedback = $section['item_feedback'];
                     $xmlFeedbackRequired = $section['feedback_required'];
                     $xmlFeedbackInternal = $section['internal_feedback_only'];

                     $sectionID = $db->forms->insertFormSection(
                         $insertedFormID,
                         [
                           "order" => ($sectionNum + 1),
                           "reSortOrder" => false,
                           "title" => $xmlAdditionalInfo,
                           "competence" => $xmlCompetenceID,
                           "sectionFeedback" => $xmlSectionFeedback,
                           "itemFeedback" => $xmlItemFeedback,
                           "feedbackRequired" => $xmlFeedbackRequired,
                           "feedbackInternal" => $xmlFeedbackInternal,
                           "selfAssessment" => 0
                         ]
                     );

                     $item = $section['items']['item'];
                     if (isset($section['items']['item']['text'])) {

                         $numOfItems = 1;

                     } else {

                         $numOfItems = count($section['items']['item']);

                     }

                 } else {

                     $xmlCompetenceID = $section[$sectionNum]['competence_id'];
                     $xmlAdditionalInfo = $section[$sectionNum]['section_text'];
                     $xmlSectionFeedback = $section[$sectionNum]['section_feedback'];
                     $xmlItemFeedback = $section[$sectionNum]['item_feedback'];
                     $xmlFeedbackRequired = $section[$sectionNum]['feedback_required'];
                     $xmlFeedbackInternal = $section[$sectionNum]['internal_feedback_only'];

                     $sectionID = $db->forms->insertFormSection(
                       $insertedFormID,
                       [
                          "order" => ($sectionNum + 1),
                          "reSortOrder" => false,
                          "title" => $xmlAdditionalInfo,
                          "competence" => $xmlCompetenceID,
                          "sectionFeedback" => $xmlSectionFeedback,
                          "itemFeedback" => $xmlItemFeedback,
                          "feedbackRequired" => $xmlFeedbackRequired,
                          "feedbackInternal" => $xmlFeedbackInternal,
                          "selfAssessment" => 0
                       ]
                     );

                     $item = isset($section[$sectionNum]['items']) ? $section[$sectionNum]['items']['item'] : '';

                     if (isset($section[$sectionNum]['items']['item']['text'])) {

                         $numOfItems = 1;

                     } else {

                         $numOfItems = ($item == '') ? 0 : count($section[$sectionNum]['items']['item']);

                     }

                 }

                 // Items Loop
                 for ($itemNum = 0; $itemNum < $numOfItems; $itemNum++) {

                     if ($numOfItems == 1) {

                         $xmlItemDesc = $item['text'];
                         $xmlAnswerType = $item['type'];
                         $xmlScale = $item['grs'];
                         $xmlCritical = $item['critical'];
                         $xmlHighestItemValue = $item['highest_value'];
                         $xmlLowestItemValue = $item['lowest_value'];
                         if (isset($item['options']['option']['value'])) {

                             $numOfOptions = 1;

                         } else {

                             $numOfOptions = count($item['options']['option']);

                         }

                         $option = $item['options']['option'];

                     } else {

                         $xmlItemDesc = $item[$itemNum]['text'];
                         $xmlAnswerType = $item[$itemNum]['type'];
                         $xmlScale = $item[$itemNum]['grs'];
                         $xmlCritical = $item[$itemNum]['critical'];
                         $xmlHighestItemValue = $item[$itemNum]['highest_value'];
                         $xmlLowestItemValue = $item[$itemNum]['lowest_value'];

                         if (isset($item[$itemNum]['options']['option']['value'])) {

                             $numOfOptions = 1;

                         } else {

                             $numOfOptions = count($item[$itemNum]['options']['option']);

                         }

                         $option = $item[$itemNum]['options']['option'];

                     }

                     $itemID = $db->forms->insertItem(
                             $sectionID,
                             $xmlItemDesc,
                             $xmlAnswerType,
                             $xmlHighestItemValue,
                             $xmlLowestItemValue,
                             $itemNum + 1,
                             $xmlScale,
                             $xmlCritical
                     );

                     $xmlValue = $xmlText = $xmlFlag = 0;

                     // Options Loop
                     for ($optionNum = 0; $optionNum < $numOfOptions; $optionNum++) {

                         // Option Value
                         if (isset($option[$optionNum]['value'])) {

                             $xmlValue = $option[$optionNum]['value'];

                         }

                         // Option Descriptor
                         if (isset($option[$optionNum]['text'])) {

                             $xmlText = $option[$optionNum]['text'];

                         }

                         // Option Flag
                         if (isset($option[$optionNum]['flag'])) {

                             $xmlFlag = $option[$optionNum]['flag'];

                         }

                         // Insert Option
                         if ($xmlAnswerType === 'text') {

                             $db->forms->insertOption(
                                 $itemID,
                                 $optionNum+1,
                                 $xmlText
                             );

                         } else {

                             $db->forms->insertOption(
                                 $itemID,
                                 $optionNum+1,
                                 $xmlText,
                                 $xmlValue,
                                 $xmlFlag
                             );

                         }

                     }

                 }

                 $db->forms->reCalcFormTotal($insertedFormID);

             }

          /**
           *  Add any content tags found
           */
          if (!isset($xmlArray['tags']['tag'])) {

             continue;

          }

          // Single tag
          if (is_array($xmlArray['tags']['tag'])) {

            $tagsToAdd = $xmlArray['tags']['tag'];

           // Multiple tags
          } else {

             $tagsToAdd = [$xmlArray['tags']['tag']];

          }

          //  Tags to add
          if (count($tagsToAdd) > 0) {

             $db->forms->linkFormTags($insertedFormID, $tagsToAdd);

          }

        }

     }

 } else {

     $errorList[] = 'No files detected';

 }

 // Copying Images
 $source = '../storage/app/tmp/extract_to_dir/share';
 $destination = '../storage/app/share';
 recursiveCopy($source, $destination);

 $output = [
     'imported_forms' => $importedForms,
     'error_list' => $errorList
 ];

 $URL = '../manage.php?page=station_tool&tab=import';
 $_SESSION['import_report'] = $output;

 header('Location: ' . $URL);

 ######## cleaning up folders ########
 foreach (glob('../storage/app/tmp/extract_to_dir/*.xml') as $file) {

     unlink($file);

 }

 $dir = '../storage/app/tmp/extract_to_dir/share';
 rrmdir($dir);

 function rrmdir($dir)
 {

     foreach (glob($dir . '/*') as $file) {

         if (is_dir($file)) {

             rrmdir($file);

         } else {

             unlink($file);

         }

     }

 }

 foreach (glob('../storage/app/tmp/uploads/*') as $file) {

     unlink($file);

 }

 // Function to copy folders and files
 function recursiveCopy($source, $destination)
 {

     $dir = opendir($source);
     @mkdir($destination);

     while (false !== ( $file = readdir($dir))) {

         if (( $file != '.' ) && ( $file != '..' )) {

             if (is_dir($source . '/' . $file)) {

                 recursiveCopy($source . '/' . $file, $destination . '/' . $file);

             } else {

                 copy($source . '/' . $file, $destination . '/' . $file);

             }

         }

     }

     closedir($dir);

 }


 function objectsIntoArray($arrObjData)
 {

     $arrData = [];

     if (is_object($arrObjData)) {

         $arrObjData = get_object_vars($arrObjData);

     }

     if (is_array($arrObjData)) {

         foreach ($arrObjData as $index => $value) {

             if ($index === 'text') {

                 $value = stripCdata($value);

             }

             if (is_object($value) || is_array($value)) {

                 $value = objectsIntoArray($value);

             }

             $arrData[$index] = $value;

         }

     }

     return $arrData;

 }
 ########### (http://php.net/manual/en/function.strip-tags.php)
 # hongong at webafrica dot org dot za 26-Mar-2009 05:52##

 function stripCdata($string)
 {

     preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $string, $matches);
     return str_replace($matches[0], $matches[1], $string);

 }
