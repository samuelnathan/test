<?php
  /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  */

 use OMIS\Template as Template;
 defined('_OMIS') or die('Restricted Access');

 /**
  * @ToDo (DC) The menu selector itself needs to be templated at some stage
  */
 include 'layout/menuSelector.php';
 global $page;
 /**
  * If user is logged on then display their name
  * and a logout option, if not, display date
  * 
  */
 if (isset($_SESSION['instance_identifier'])) {
      
   $userLoginData = [
       'id' => $_SESSION['user_identifier'],
       'name' => $_SESSION['user_forename'] . " " . $_SESSION['user_surname'],
       'roleSuperAdmin' => (isset($_SESSION['user_role']) && $_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SUPER_ADMIN),
       'roleName' => \OMIS\Auth\Role::loadID($_SESSION['user_role'])->name
   ];

 } else {
    
   $userLoginData = [
       'id' => '',
       'name' => '',  
       'roleSuperAdmin' => false,
       'roleName' => ''
   ];
     
 }
   
 $release = \OMIS\Release::getInstance();
 $template = new Template(Template::findMatchingTemplate(__FILE__));

 $template->render([
    'skin' => $config->localization['skin'],
    'domain' => $config->localization['domain'],

    'enabled' => (isset($config->systemNotifications['enabled']) &&
                       $config->systemNotifications['enabled'] == 1), 
    'clientCode' => isset($config->systemNotifications['client_code']) ?
                       $config->systemNotifications['client_code'] : '', 
    'name' => isset($config->systemNotifications['client_name']) ?
                       $config->systemNotifications['client_name'] : '', 
    'mode' => isset($config->systemNotifications['mode']) ?
                       $config->systemNotifications['mode'] : '',

    'systemName' => [
        'short' => $config->getName(),
    ],
    'release' => [
        'version' => $release::$version,
        'date' => $release::$date
    ],
    'clientName' => $config->getClientName(),
    'clientNameColour' => $config->localization['client'],
    'currentDate' => date('M j, Y, g:i a'),
    'user' => $userLoginData,
    'page' => $page
 ]);


 ?>