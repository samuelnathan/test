<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 $userMenuMap = [
    "Manage Administrators"  => "manage.php?page=man_admin",
    "Manage Assistants" => "manage.php?page=assist"
 ];

 $examTeamAllowed = $db->features->enabled('exam-team', $_SESSION['user_role']);

 if ($examTeamAllowed) {
   $userMenuMap["Manage " . gettext('Exam-Team-Placeholder') . gettext(' Team')] = "manage.php?page=manage_examiners";
   $userMenuMap["Import " . gettext('Exam-Team-Placeholder') . gettext(' Team')] = "manage.php?page=importingStep1&amp;dataType=4";
 }
 
 $success = printMenuItems($userMenuMap);
 if (!$success) {
    die("Failed to render menu. Session may have expired unexpectedly");
 }
