<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 use \OMIS\Auth\RoleCategory as RoleCategory;
 use \OMIS\Auth\Role as Role;

 require_once 'layoutFunctions.php';

 // Roles that can login
 $rolesWithAccess = new RoleCategory(null, $include_superadmin = true, $login_only = true);

 // Session and instance check
 //if (\OMIS\Session::correctInstance()) {
 if (isset($_SESSION['instance_identifier']) && $_SESSION['instance_identifier'] == $config->installation_id) {  
    $loginSession = (isset($_SESSION['user_role']) && $rolesWithAccess->containsRole($_SESSION['user_role']));
 } else {
    $loginSession = false;
 }

 // Defines which roles can access which menus.
 // @TODO Build an efficient mechanism to build the role maps from the database.
 $menuMap = [
    "Main" => [
        "require_file" => "layout/dropdownMain.php",
        "roles" => [
            Role::USER_ROLE_ADMIN,
            Role::USER_ROLE_SCHOOL_ADMIN,
            Role::USER_ROLE_EXAMINER,
            Role::USER_ROLE_EXAM_ADMIN,
            Role::USER_ROLE_STATION_MANAGER,
            Role::USER_ROLE_SUPER_ADMIN
        ]
    ],
    ucwords(gettext('schools')) =>  [
        "require_file" => "layout/dropdownSchools.php",
        "roles" => [
            Role::USER_ROLE_ADMIN,
            Role::USER_ROLE_SCHOOL_ADMIN,
            Role::USER_ROLE_SUPER_ADMIN
        ]
    ],
    "Users" => [
        "require_file" => "layout/dropdownUsers.php",
        "roles" => [
            Role::USER_ROLE_ADMIN,
            Role::USER_ROLE_SCHOOL_ADMIN,
            Role::USER_ROLE_EXAM_ADMIN,
            Role::USER_ROLE_SUPER_ADMIN,
            Role::USER_ROLE_STATION_MANAGER
        ]
    ],
    ucfirst(gettext('students')) => [
        "require_file" => "layout/dropdownStudents.php",
        "roles" => [
            Role::USER_ROLE_ADMIN,
            Role::USER_ROLE_SCHOOL_ADMIN,
            Role::USER_ROLE_EXAM_ADMIN,
            Role::USER_ROLE_SUPER_ADMIN
        ]
    ],
    ucfirst(gettext('osces')) => [
        "require_file" => "layout/dropdownExams.php",
        "roles" => [
            Role::USER_ROLE_ADMIN,
            Role::USER_ROLE_SCHOOL_ADMIN,
            Role::USER_ROLE_EXAMINER,
            Role::USER_ROLE_EXAM_ADMIN,
            Role::USER_ROLE_STATION_MANAGER,
            Role::USER_ROLE_SUPER_ADMIN
        ]
    ],
 ];
 
 ?>
 <div class="topimagediv">
 <div class="innerimgdiv row no-gutters justify-content-between align-items-center">

 <div class="header-left">
 <nav class="navbar navbar-top-menu">

 <ul class="navbar-nav align-items-center navbar-expand navbar-default" id="navigation">
      <li class="navbar-brand">
        <img id="q-icon-top-left" src="assets/images/<?=$config->localization['skin']['icon_filename']?>" 
          alt="<?=$config->getName(\TRUE)?>" 
          title="<?=$config->getName(\TRUE)?>"/><a class="logo-recruit"><?=$config->localization['skin']['app_name']?></a>
      </li>
      <?php
 if (!$loginSession) {
    ?>
    <li class="nav-item">
        <a class="nav-link" href="index.php">Go to login</a>
    </li>
    <?php
 } else {
      
    list($studentImagesAllowed, $studentsManageAllowed, $coursesManageAllowed) 
    = $db->features->enabled([
        'student-images', 'student-manage', 'student-courses'
      ], $_SESSION['user_role']
    );

    if (!$studentImagesAllowed && !$studentsManageAllowed && !$coursesManageAllowed) {
      unset($menuMap[ucfirst(gettext('students'))]);
    }

    $menuTitles = array_keys($menuMap);
    $lastMenuItem = array_pop($menuTitles);
    foreach ($menuMap as $title => $menu):
        // Do not render the menu if it's not used.
        if (!in_array($_SESSION['user_role'], $menu["roles"])):
            continue;
        endif;
        
        // Specify the link class.
        $url = $menu["require_file"];
        $linkClass = ($url) ? ' dropdown dropdown-default ' : '';
        ?>
    <li class="nav-item <?=$linkClass?>">
        <a href="#" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$title?></a>
        <ul class="dropdown-menu">
            <?php require_once "$url"; ?>
        </ul>
    </li>
        <?php
    endforeach;
    ?>
    <?php
 }
 ?>

    </ul>
  </nav>
