<?php
use \OMIS\Auth\Role as Role;
use \OMIS\Auth\Page as Page;

/**
 * Outputs a bulleted list of menu items for a given menu map (associative array
 * in the form of $menu_item_title => $url). Parses out any page references that
 * might need to through the role-based authentication system and only prints
 * those menu items that don't need such authentication or those that do and are
 * approved for that role.
 * 
 * @global boolean $loginSession Is there a user logged in?
 * @global boolean $db        Global database object
 * @param string[] $map       The title -> url associations for the menu, in order.
 * @return boolean TRUE on success, FALSE on failure.
 */
function printMenuItems($map)
{
    global $loginSession;
    global $db;

    /* If the session variable has disappeared, then there's no point in trying
     * to do anything else here.
     */
    if (!in_array('user_role', array_keys($_SESSION))) {
        return false;
    }
    
    /* Try to cast the role ID as an integer. If this fails for whatever reason
     * we have a problem, so return false.
     */
    try {
        $role_id = (int) $_SESSION['user_role'];
    } catch (Exception $e) {
        return false;
    }
    
    if (!isset($loginSession) || !$loginSession) {
        return false;
    }
    
    // If a key order isn't specified, just use what's there...
    $key_order = array_keys($map);
    
    foreach ($key_order as $title) {
        $url_or_attributes = $map[$title];
        
        // If the data for this menu item is an array, assume it contains at least
        // an item called
        if (is_array($url_or_attributes)) {
            if (!in_array('href', array_map("strtolower", array_keys($url_or_attributes)))) {
                // There's no link?! Don't render this item.
                continue;
            }
            $url = $url_or_attributes['href'];
            unset($url_or_attributes['href']);
            $link_attributes = $url_or_attributes;
        } else {
            $url = $url_or_attributes;
            $link_attributes = array();
        }
        
        // If there are other parameters, then use them.
        $other_attributes = "";
        foreach ($link_attributes as $key => $value) {
            $other_attributes .= sprintf(' %s="%s"', $key, $value);
        }
        
        // If there's a query string, get it.
        if (strpos($url, "?") !== false) {
            list($base_page, $query_string) = explode("?", $url);
            $params = array();
            parse_str($query_string, $params);
            
            /* If the page is "manage.php", then the param is "page". If the page
             * is "assess.php", then the param is "p"
             */
            if (($base_page == 'manage.php') && (isset($params['page'])) && trim($params['page']) !== '') {
                $page_id = $params['page'];
            } elseif (($base_page == 'assess.php') && (isset($params['p'])) && trim($params['p']) !== '') {
                $page_id = $params['p'];
            } else {
                $page_id = null;
            }
            
        } else {
            $page_id = null;
        }
        
        // Only render pages the user's role is allowed to see.
        if (!is_null($page_id)) {
            $page_protected = Page::isProtected($page_id);
            if ($page_protected) {
                #Page Access Check / Can User Access this Section?
                $page_role_ok = Role::loadID($_SESSION['user_role'])->canAccess($page_id, true);
            }
        }
        
        $page_allowed = (is_null($page_id) || !($page_protected) || ($page_protected && $page_role_ok));
        if ($page_allowed) {
            printf("<li><a href=\"%s\"%s>%s</a></li>\n", $url, $other_attributes, $title);
        }
    }
    
    return true;
}
