<?php
 /**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

 $studentLabel = ucwords(gettext('student'));
 $studentsLabel = ucwords(gettext('students'));
 $coursesLabel = ucwords(gettext('courses'));
 $modulesLabel = ucwords(gettext('modules'));
 $menuItems = [];

 // Student images feature enabled for the current user role ? If so then add menu option
 if ($studentsManageAllowed) {
   $menuItems["Manage $studentsLabel"] = "manage.php?page=manage_examinees";
   $menuItems["Import $studentsLabel"] = "manage.php?page=importingStep1&amp;dataType=3";
 }
 
 if ($studentImagesAllowed) {
   $menuKey = "Import $studentLabel Images";
   $menuItems[$menuKey] = "manage.php?page=examinees_uploadimages";
 }

 if ($coursesManageAllowed) {
   $menuItems["Manage $studentLabel $coursesLabel &amp; $modulesLabel"] = "manage.php?page=manage_courses";
   $menuItems["Import $studentLabel $coursesLabel &amp; $modulesLabel"] = "manage.php?page=importingStep1&amp;dataType=2";
 }

 if ($studentImagesAllowed || $studentsManageAllowed || $coursesManageAllowed) {
   printMenuItems($menuItems);
 }
