<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2016, Qpercom Limited
 */
#Brendan Devane Edit 15/08/2013: Changed Menu items to Title-Case.
/* DW 2013-10-15: Ripped out category-based menu permission system, replaced 
 * with page_level version checked against the database.
 */
$menu_map = array(
    "Logout" => "extra/initial.php?logout=lt",
    "Home Page" => "manage.php?page=main",
    "Manage Account" => "manage.php?page=account",
    "Manage Features" => "manage.php?page=manage_features",
    "Manage " . ucwords(gettext('terms')) => "manage.php?page=manage_terms",
    "Admin Tools" => "manage.php?page=admin_tool",
    "Import Data" => "manage.php?page=importingStep1"
);

$success = printMenuItems($menu_map);
if (!$success) {
    die("Failed to render menu. Session may have expired unexpectedly");
}
