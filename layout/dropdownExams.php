<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

 // Feature Enabled|Disabled
 list($matrixFeature, $bankFeature, $FeedbackFeature, $selfAssessFeature, $analysisFeature) =
 $db->features->enabled([
      'exam-matrix', 'assess-content', 'feedback-system', 
      'self-assessments', 'qpercom-analyse'
      ], $_SESSION['user_role']
 );

 $menu_map = [
    "Management &amp; Analysis" => "manage.php?page=manage_osce"
 ];

 if ($bankFeature) {

    $menu_map = array_merge($menu_map, [
       ucwords(gettext('form')) . " Bank" => "manage.php?page=forms_osce"
    ]);

 }

 if ($matrixFeature) {

    $menu_map = array_merge($menu_map, [
       "Import Matrix" => "manage.php?page=importingStep1&amp;dataType=5"
    ]);

 }

 if ($selfAssessFeature) {

  $menu_map = array_merge($menu_map, [
     "Import Self Assessments" => "manage.php?page=importingStep1&amp;dataType=6"
  ]);

 }

 if ($FeedbackFeature) {

    $menu_map = array_merge($menu_map, [
       "Send Feedback" => "manage.php?page=feedback_tool"
    ]);

 }

 if ($analysisFeature) {

   $menu_map = array_merge($menu_map,[
      "Advanced Analysis " => $config->localization['client']['analyse_url']
   ]);

 }

 $menu_map = array_merge($menu_map, [
   "Perform " . ucwords(gettext('exam')) => "assess.php?p=exam_configuration&amp;frmtop=1",
   "Upload media" => "manage.php?page=upload&term_id=".$_SESSION['cterm']."&dept_id=".$_SESSION['selected_dept'],
 ]);

 $success = printMenuItems($menu_map);

 if (!$success) {

    die("Failed to render menu. Session may have expired unexpectedly");

 }
