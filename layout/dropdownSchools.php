<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

#Brendan Devane : Edit 15/08/2013 : Changed Menu items to Title-Case.
/* DW 2013-10-15: Ripped out category-based menu permission system, replaced 
 * with page_level version checked against the database.
 */

$menu_map = [
    "Manage " . ucwords(gettext('schools')) => "manage.php?page=manage_schools",
    "Manage " . gettext('Departments') => "manage.php?page=manage_departments"
];

$success = printMenuItems($menu_map);
if (!$success) {
    die("Failed to render menu. Session may have expired unexpectedly");
}

