<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2020, Qpercom Limited
 * No logic thought out yet (this is for outsourcing partners)
 */

 use \OMIS\Database\CoreDB as CoreDB;

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 /*if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
 }*/

 $multimedia = $db->multimedia->getByStation($id);

 // logic goes here and add what more files required in parent directory

 
 $template = new \OMIS\Template(\OMIS\Template::findMatchingTemplate(__FILE__));
 $template->render([
    'multimedia' => $multimedia
 ]);

