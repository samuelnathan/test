<?php
/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

define('_OMIS', 1);

use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;
use \OMIS\Utilities\JSON as JSON;

if (isset($_POST['isfor'])) {
  
    include __DIR__ . '/../extra/essentials.php';
     //Page Access Check / Can User Access this Section?
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
        return false;
        
    }
    
} else {
  
    include __DIR__ . '/../extra/noaccess.php';
    
}

// Clean 'isfor' variable
$isfor = filter_input(INPUT_POST, 'isfor', FILTER_SANITIZE_STRIPPED);

// Encode data to JSON
$encodeJSON = false;

// The return string
$returnVariable = "";

// Add Global Rating Scale value
if ($isfor == "add_grs_value") {
  
  // Clean POST Variables
  $scaleTypeID = filter_input(INPUT_POST, 'scale_type_id');
  $scaleValue = filter_input(INPUT_POST, 'scale_value');
  $valueDescription = filter_input(INPUT_POST, 'scale_value_description');
  $valueBorderline = filter_input(INPUT_POST, 'value_borderline', FILTER_VALIDATE_BOOLEAN);
  $valueFail = filter_input(INPUT_POST, 'value_fail', FILTER_VALIDATE_BOOLEAN);  
    
  $success = $db->forms->insertRatingScaleValue(
          $scaleTypeID,
          $scaleValue,
          $valueDescription,
          $valueBorderline,
          $valueFail
  );
  $returnVariable = (($success) ? 'RECORD_SAVED' : 'RECORD_NOT_SAVED');
}

// Update Global Rating Scale value
else if ($isfor == "update_grs_value") {
  
  // Clean POST Variables
  $recordKey = filter_input(INPUT_POST, 'record_key');
  $scaleValue = filter_input(INPUT_POST, 'scale_value');
  $valueDescription = filter_input(INPUT_POST, 'scale_value_description');
  $valueBorderline = filter_input(INPUT_POST, 'value_borderline', FILTER_VALIDATE_BOOLEAN);
  $valueFail = filter_input(INPUT_POST, 'value_fail', FILTER_VALIDATE_BOOLEAN);
    
  $success = $db->forms->updateRatingScaleValue(
          $recordKey,
          $scaleValue,
          $valueDescription,
          $valueBorderline,
          $valueFail
  );
  $returnVariable = (($success)? 'RECORD_UPDATED' : 'RECORD_NOT_UPDATED');
}

// Delete Global Rating Scale values 
else if ($isfor == "delete_grs_values") {
  
  // Clean POST Variables
  $scaleValues = filter_input(INPUT_POST, 'records_to_delete', FILTER_SANITIZE_STRIPPED, FILTER_REQUIRE_ARRAY);
 
  // Delete Rating Scale Values
  $success = $db->forms->deleteRatingScaleValues($scaleValues);
  $returnVariable = (($success)? 'RECORDS_DELETED' : 'RECORDS_NOT_DELETED');
  
}
// Get All Global Rating Scale Values
else if ($isfor == "get_all_grs_values") {
  
  // Encoding to JSON
  $encodeJSON = true;
  
  // json_encode takes care of Html Entities
  $db->set_Convert_Html_Entities(false);
  
   // Clean POST Variables
  $scaleTypeID = filter_input(INPUT_POST, 'scale_type_id', FILTER_SANITIZE_STRIPPED); 
  
  // Get Rating Scale Values
  $scaleValues = $db->forms->getRatingScaleValues($scaleTypeID, NULL, false);
  $returnVariable = $scaleValues;
  
   
}
// Get Global Rating Scale Value 
else if ($isfor == "get_grs_value") {
  
  // Encoding to JSON
  $encodeJSON = true;
 
  // Record key
  $recordKey = filter_input(INPUT_POST, 'record_key', FILTER_SANITIZE_STRIPPED);
  
  // json_encode takes care of Html Entities
  $db->set_Convert_Html_Entities(false);
  
  // Get Rating Scale Value
  $scaleValue = $db->forms->getRatingScaleValues(NULL, $recordKey, false, NULL);

  $returnVariable = $scaleValue;
   
}

else {
  $returnVariable = "REQUEST_NOT_PROCESSED";
}

// Return data, raw or encoded in JSON
if ($encodeJSON) {
  try {
    echo JSON::encode($returnVariable);
  } catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnArray, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
  }
} else {
   echo $returnVariable;
}