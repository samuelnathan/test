<?php

session_start();

use \OMIS\Auth\RoleCategory as RoleCategory;
use \OMIS\Auth\Role as Role;

define('_OMIS', 1) or define("_OMIS", 1);

require __DIR__ . '/../vendor/autoload.php';


// New config object if not created
global $config;

if (!isset($config)) {
   $config = new \OMIS\Config;
}     

$ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING);

// If SSO is not enabled, redirect to index
if (!$config->isSSOEnabled()) {
  header('Location: ../index.php');
  return;
}

if (!isset($_POST['SAMLResponse'])) {
   throw new Exception('No SAMLResponse in request');
}

// Set the error message to show if the user is not registered in the system
$notRegisteredError = '<p class="loginfo">User not found</p>';

// Get the SAML Settings
$samlSettings = new OneLogin_Saml2_Settings($config->getSamlSettings());
// Get the SAML Response
$samlResponse = new OneLogin_Saml2_Response($samlSettings, $_POST['SAMLResponse']);

// Check that the response is valid
if (!$samlResponse->isValid()) {
    echo 'Invalid SAML Response';
    return;
}

// Get the user email from the SAML response
$userEmail = $samlResponse->getNameId();
\Analog::info("SAML IdP response with NameID: " . $userEmail); 

// Find the user with that email in the database
$db = \OMIS\Database\CoreDB::getInstance();
$resultSet = $db->users->getUser($userEmail, 'email');

// Check that the user is in the database
if (mysqli_num_rows($resultSet) == 0) {
    $_SESSION['loginerror'] = $notRegisteredError;
    header('Location: ../index.php');
    return;
}

$userRecord = $db->fetch_row($resultSet);
$userID = $userRecord['user_id'];

// Check that the user account is active
if (!$db->users->doesUserAccountExist($userID)) {
    $_SESSION['loginerror'] = $notRegisteredError;
    header('Location: ../index.php');
    return;
}

// Set the user in session

$_SESSION['userInputId'] = $userID;
$_SESSION['user_identifier'] = $userID;
$_SESSION['instance_identifier'] = $config->installation_id;
$_SESSION['user_forename'] = $userRecord['forename'];
$_SESSION['user_surname'] = $userRecord['surname'];
$_SESSION['user_email'] = $userRecord['email'];
$_SESSION['user_role'] = $userRecord['user_role'];
$_SESSION['selected_dept'] = '';
$_SESSION['selected_school'] = '';
$_SESSION['selected_course'] = '';
$_SESSION['selected_year'] = '';
$_SESSION['selected_module'] = '';
$_SESSION['loginerror'] = '';

// Keep in the session the fact that the user is logged via SSO
$_SESSION['sso_login'] = true;

$termData = $db->academicterms->getDefaultTerm();
$_SESSION['cterm'] = (!is_null($termData)) ? $termData['term_id'] : '';

$dates = $db->users->setTimeStampLogin($userID);
$_SESSION['lastlogin'] = $dates[1];

$presets = $db->userPresets->get();

// Prepare Department Selection Prefix
if (!empty($presets['filter_department']) && $db->departments->existsById($presets['filter_department'])) {
$_SESSION['selected_dept'] = $presets['filter_department'];

// If Admin or Super Admin, get last active department
} elseif (in_array($userRecord['user_role'], [Role::USER_ROLE_ADMIN, Role::USER_ROLE_SUPER_ADMIN])) {
$_SESSION['selected_dept'] = $db->departments->getLastActiveDepartment();

// If Examiner or Exam Admin
} elseif (in_array($userRecord['user_role'], [Role::USER_ROLE_EXAMINER, Role::USER_ROLE_EXAM_ADMIN])) {
$_SESSION['selected_dept'] = $db->departments->getExaminerDepartmentIDs($userID)[0];
}

// Save department prefix for next time
$db->userPresets->update(['filter_department' => $_SESSION['selected_dept']]);

// Prepare School Selection Prefix
if (!empty($presets['filter_school']) && $db->schools->doesSchoolExist($presets['filter_school'])) {
$_SESSION['selected_school'] = $presets['filter_school'];
}

// Prepare Course Selection Prefix
if (!empty($presets['filter_course']) && $db->courses->doesCourseExist($presets['filter_course'])) {
$_SESSION['selected_course'] = $presets['filter_course'];
}

// Prepare Year Selection Prefix
if (!empty($presets['filter_year']) && $db->courses->doesCourseYearExist($presets['filter_year'])) {
$_SESSION['selected_year'] = $presets['filter_year'];
}

// Prepare Module Selection Prefix
if (!empty($presets['filter_module']) && $db->courses->doesModuleExist($presets['filter_module'])) {
$_SESSION['selected_module'] = $presets['filter_module'];
}

// Etc..
$db->users->clearLoginAttempts($ipAddress, $userID);
\Analog::info($userID . " logged in successfully via SSO");

?>
<html>
  <head>
    <script>
      window.onload = function() {
        document.forms['redirect-initial'].submit();
      };
    </script>
  </head>

  <body>
    <form method="post" action="../extra/initial.php" name="redirect-initial">
      <input type="hidden" name="continue" value="continue" />
    </form>
  <body>
</html>
