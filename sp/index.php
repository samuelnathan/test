<?php
session_start();

define('_OMIS', 1) or define("_OMIS", 1);

require __DIR__ . '/../vendor/autoload.php';

// New config object if not created
global $config;

if (!isset($config)) {
   $config = new \OMIS\Config;
}     

// If SSO is not enabled, redirect to index
if (!$config->isSSOEnabled()) {
  header('Location: ../index.php');
  return;
}

// Get the first action from the request parameter and set it in the session
$_SESSION['todo'] = filter_input(INPUT_POST, 'todo', FILTER_SANITIZE_NUMBER_INT);

// Get the SAML Settings
$settings = new OneLogin_Saml2_Settings($config->getSAMLSettings());
// Create the SAML Request using the configuration in from settings 
$authRequest = new OneLogin_Saml2_AuthnRequest($settings);
$samlRequest = $authRequest->getRequest();

// Set the GET parameters to redirect the user with
$parameters = array('SAMLRequest' => $samlRequest);
$parameters['RelayState'] = OneLogin_Saml2_Utils::getSelfURLNoQuery();

// Get the IdP url to redirect
$idpData = $settings->getIdPData();
$ssoUrl = $idpData['singleSignOnService']['url'];

// Redirect the user to the IdP with the SAML Request
$url = OneLogin_Saml2_Utils::redirect($ssoUrl, $parameters, true);
header("Location: $url");

