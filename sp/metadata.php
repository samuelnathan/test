<?php

define('_OMIS', 1) or define("_OMIS", 1);

require __DIR__ . '/../vendor/autoload.php';

// New config object if not created
global $config;

if (!isset($config)) {
   $config = new \OMIS\Config;
}     

// If SSO is not enabled, redirect to index
if (!$config->isSSOEnabled()) {
  header('Location: ../index.php');
  return;
}

header('Content-Type: text/xml');

$samlSettings = new OneLogin_Saml2_Settings($config->getSAMLSettings());
$sp = $samlSettings->getSPData();

$samlMetadata = OneLogin_Saml2_Metadata::builder($sp);
echo $samlMetadata;
