<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 // Critical Session Check
 $session = new OMIS\Session;
 $session->check();

 // Page Access Check / Can User Access this Section?
 if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {

     return false;

 }

 require_once __DIR__ . '/../extra/helper.php';
 require_once __DIR__ . '/../osce/osce_functions.php';

 $schoolIDs = [];
 $importReport = [];

 // School admins, only retrieve linked schools
 if ($_SESSION['user_role'] == \OMIS\Auth\Role::USER_ROLE_SCHOOL_ADMIN) {
   $schoolIDs = array_unique(
       array_column(
           $db->schools->getAdminSchools($_SESSION['user_identifier']),
           'school_id'
       )
   );
 }

 $schoolsRecords = $db->departments->getDepartments(null, false, $schoolIDs);
 $schools = \OMIS\Database\CoreDB::group(
        \OMIS\Database\CoreDB::into_array($schoolsRecords),
        ['school_description'], true
 );

 // If tab selected
 $tab = filter_input(INPUT_GET, 'tab', FILTER_SANITIZE_STRING);
 if (!is_null($tab) && $tab == 'import') {

   // Import Session
   if (isset($_SESSION['import_report']) && is_array($_SESSION['import_report'])) {
      $importReport = $_SESSION['import_report'];
   }

   $mode = "import";

 } else {

   $mode = "export";

 }

 // Template data
 $templateData = [
     'mode' => $mode,
     'schools' => $schools,
     'importReport' => $importReport,
     'terms' => $db->academicterms->getAllTerms(),
     'currentTerm' => $_SESSION['cterm']
 ];

 // Load and render the template.
 $template = new OMIS\Template("station_tool.html.twig");
 $template->render($templateData);

