<?php
/* Original Author: Brendan Devane + Cormac Mc Swiney + David Cunningham
 * For Qpercom Ltd
 * Date: 20/06/2016
 * © 2016 Qpercom Limited. All rights reserved.
 * @Manage Stations Import & Export
 */

// Critical Session Check
$session = new OMIS\Session;
$session->check();

// Page Access Check / Can User Access this Section?
if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
    return false;
}

require_once __DIR__ . '/../extra/helper.php';
require_once __DIR__ . '/../osce/osce_functions.php';

use OMIS\Template as Template;

// Setup and get log file
if (!isset($config)) {
    $config = new OMIS\Config;
}

/**
 * Changed way to obtain log files in order to sort them by modification date
 */
//$logFiles = scandir('storage/logs');

// Get the files in the log folder
$d = new DirectoryIterator('storage/logs');
$logFiles = [];
foreach ($d as $file) {
    if (!$file->isFile() || !$file->valid()) continue;

    $logFiles[] = [
        'file' => $file->getFilename(),
        'timestamp' => $file->getMTime()
    ];
}

// Sort them by modification time descending
usort($logFiles, function (array $f1, array $f2) {
   if ($f1['timestamp'] != $f2['timestamp'])
       return $f2['timestamp'] - $f1['timestamp'];

   return strcmp($f1['file'], $f2['file']);
});

// Extract the filename
$logFiles = array_map(function (array $file) {
    return $file['file'];
}, $logFiles);


$logFileName = basename($config->analog_log_file);
$logFilesFiltered = array_values(
    array_filter($logFiles, function($logFile) use ($logFileName) { 
          return (strpos($logFile, $logFileName) !== false);
    }
));

$selectFileParam = filter_input(INPUT_GET, 'file', FILTER_VALIDATE_INT);
$selectedLogFileIndex = (!is_null($selectFileParam) && isset($logFilesFiltered[$selectFileParam])) ? $selectFileParam : 0;

// Get parent directory for log files
$directory = dirname($config->analog_log_file);

// Current file handle
$handle = fopen($directory . '/' . $logFilesFiltered[$selectedLogFileIndex], 'r');

if ($handle) {
    $data = ['records' => []];
    while (($buffer = fgets($handle, 16384)) !== false) {
        
        $fields = preg_split("/(?<!\\\\)\ - /", $buffer);
        
        $ip = array_shift($fields);
        $date = array_shift($fields);
        $level = array_shift($fields);

        if ($config->auditTrail['userAsField']) {
            $user = array_shift($fields);
            $message = implode(" - ", $fields);
        } else {
            $message = implode(" - ", $fields);
            $user = explode(" ", trim($message))[0];
        }
        
        $data['records'][] = [
            "ip" => $ip,
            "date" => $date,
            "level" => $level,
            "user" => $user,
            "message" => $message
        ];
     
    }
    
    // Add log file list
    $data['log_files'] = $logFilesFiltered;
    
    // Add selected log file
    $data['select_logfile'] = $selectedLogFileIndex;
    
    // Close the file, hopefully successfully.
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail";
    }
    fclose($handle);

    // Render the log entries using a template.
    $template = new Template(Template::findMatchingTemplate(__FILE__));
    $template->render($data);
}
