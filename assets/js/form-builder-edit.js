function addEditEvent(e) {
    var t = jQuery(e).parents("tr:first"),
        i = t.clone(!0, !0),
        a = t.find("input[id^=quest_cb]").val(),
        r = jQuery("<tr>", {
            id: "edit_qr"
        }),
        n = jQuery("<td>", {
            id: "edit_qc",
            colspan: "7",
            class: "waittdw"
        });
    n.html("&nbsp;"), r.prepend(n), t.replaceWith(r), r.prev("tr") && r.prev("tr").addClass("baseborder");
    jQuery("#assessform_sections_form").find("input, select, img[class=arrow], img[class=editb], img[class=comment-img], a").css("visibility", "hidden");
    var s = Math.random();
    jQuery.ajax({
        url: "forms/ajx_edit_item.php?",
        type: "post",
        data: {
            sid: s,
            item_id: a,
            session_id: jQuery("#sessionid_top").length ? jQuery("#sessionid_top").val() : ""
        }
    }).done(function(e) {
        if (n.html(e), jQuery("#item_text-" + a).length) {
            var t = jQuery("#item_text-" + a).attr("id");
            tinymce.execCommand("mceAddEditor", !1, t)
        }
        loadCompetenciesDialog(), n.attr("class", "edit"), setupEditButtons(i, r, a), setupEditFlipEvent(a), previewItemTypes(), flagsToggle(), jQuery("html, body").animate({
            scrollTop: jQuery("#edit_qr").offset().top - 20
        }, 800)
    }).fail(function(e) {
        r.replaceWith(i), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), i.find("a[id^=editq]").on("click", function(e) {
            e.preventDefault(), addEditEvent(this)
        }), alert("Request failed, please try again")
    })
}

function addSectionEditEvent(e) {
    var t = jQuery(e).parents("th:first").find("input[id^=section-ids]").val(),
        i = jQuery("#editsectiondiv_" + t),
        a = i.html();
    jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "hidden");
    var r = Math.random();
    i.empty().attr("class", "edit-section-wait"), jQuery.ajax({
        url: "forms/ajx_edit_section_details.php?",
        type: "post",
        data: {
            mode: "edit",
            sid: r,
            id: t,
            session_id: jQuery("#sessionid_top").length ? jQuery("#sessionid_top").val() : ""
        }
    }).done(function(e) {
        i.attr("class", "redborderv3"), i.html(e), sectionCancelEvent(jQuery("#editsectioncancel_" + t), jQuery("#editsectionupdate_" + t), i, a), sectionUpdateEvent(jQuery("#editsectionupdate_" + t), t), jQuery("html, body").animate({
            scrollTop: i.offset().top - 20
        }, 800)
    }).fail(function(e) {
        i.attr("class", "fl"), i.html(a), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), alert("The request failed, Please try again")
    })
}

function addSectionDeleteEvent(e) {
    var t = jQuery(e).parents("th:first").find("input[id^=section-ids]").val();
    if (confirm("Are you sure you want to continue with deletion?")) {
        var i = Math.random();
        jQuery.ajax({
            url: "forms/ajx_edit_section_details.php?",
            type: "post",
            data: {
                mode: "delete",
                sid: i,
                id: t
            }
        }).done(function() {
            refreshPage(!0)
        }).fail(function(e) {
            alert("The request failed, Please try again")
        })
    }
}

function sectionUpdateEvent(e, t) {
    jQuery(e).on("click", function() {
        if (0 !== jQuery("#compselectedit_" + t).val().length)
            if (0 !== jQuery("#sectionorderedit_" + t).val().length) {
                jQuery("#section-button-div").find("input[id^=editsectioncancel], input[id^=editsectionupdate]").css("display", "none"), jQuery("#section-button-div").attr("class", "button-div-wait");
                var e = +(jQuery("#selfassessment_" + t).length && jQuery("#selfassessment_" + t).is(":checked")),
                    i = +jQuery("#sectionfeedback_" + t).is(":checked"),
                    a = +jQuery("#itemfeedback_" + t).is(":checked"),
                    r = +jQuery("#feedbackinternal_" + t).is(":checked");
                jQuery.ajax({
                    url: "forms/ajx_edit_section_details.php?",
                    type: "post",
                    data: {
                        mode: "update",
                        sid: Math.random(),
                        id: t,
                        order: jQuery("#sectionorderedit_" + t).val(),
                        competence_selected: jQuery("#compselectedit_" + t).val(),
                        title_text: jQuery("#sectiondescedit_" + t).val(),
                        section_feedback: i,
                        item_feedback: a,
                        feedback_required: jQuery("#feedbackrequired_" + t).val(),
                        feedback_internal: r,
                        self_assessment: e
                    }
                }).done(function(e) {
                    refreshPage(!0)
                }).fail(function(e) {
                    jQuery("#section-button-div").find("input[id^=editsectioncancel], input[id^=editsectionupdate]").css("display", ""), jQuery("#section-button-div").attr("class", "group3-section-settings"), alert("The request failed, Please try again")
                })
            } else alert("Please choose order");
        else alert("Please choose a type")
    })
}

function sectionCancelEvent(e, t, i, a) {
    jQuery(e).on("click", function() {
        jQuery(e).remove(), jQuery(t).remove(), jQuery(i).attr("class", "fl"), jQuery(i).html(a), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible")
    })
}

function addItemToSection(e) {
    var t, i = jQuery(e).attr("id").split("-"),
        a = i[1],
        r = jQuery("#itemaddtd-" + a);
    if (jQuery("#assessform_sections_form").find("input, select, img[class=arrow], img[class=editb], img[class=comment-img], a").css("visibility", "hidden"), "clitem" === i[0]) {
        var n = jQuery("#clone_item-" + a).val();
        t = "yes"
    } else t = "no";
    var s = Math.random();
    r.html("&nbsp;").attr("class", "waittdw2"), r.parents("tr:first").prev("tr") && r.parents("tr:first").prev("tr").addClass("baseborder"), jQuery.ajax({
        url: "forms/ajx_add_item.php?",
        type: "post",
        data: {
            sid: s,
            section_id: a,
            clone_item: n,
            clone_item_yesno: t,
            session_id: jQuery("#sessionid_top").length ? jQuery("#sessionid_top").val() : ""
        }
    }).done(function(e) {
        if (r.attr("class", "section-buttons-edit"), r.html(e), previewItemTypes(), setupAddFlipEvent(a), jQuery("#item_text-" + a).length) {
            var t = jQuery("#item_text-" + a).attr("id");
            tinymce.execCommand("mceAddEditor", !1, t), tinymce.execCommand("mceFocus", !1, t)
        }
        loadCompetenciesDialog(), flagsToggle(), jQuery("html, body").animate({
            scrollTop: r.offset().top - 20
        }, 800)
    }).fail(function(e) {
        jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), alert("The request failed, please try again")
    })
}

function newScaleAddEvent() {
    jQuery("#create-scale-item").length && jQuery("#create-scale-item").on("click", function() {
        var e = jQuery(this).parents("table:first").parents("tr:first").prev("tr").find("input[id^=section-ids]").val(),
            t = jQuery("#sectiontd-" + e).find("td[id='itemaddtd']");
        jQuery("#assessform_sections_form").find("input, select, img[class=arrow], img[class=editb], img[class=comment-img], a").css("visibility", "hidden");
        var i = Math.random();
        t.html("&nbsp;").attr("class", "waittdw2"), jQuery.ajax({
            url: "forms/ajx_add_scale_item.php?",
            type: "post",
            data: {
                sid: i,
                section_id: e
            }
        }).done(function(e) {
            t.attr("class", "section-buttons-edit"), t.html(e), tinymce.execCommand("mceAddEditor", !1, jQuery("#item_text").attr("id")), scaleFlipEvent(), newScaleCancelEvent(), newScaleSubmitEvent(), changeScaleType(), jQuery("html, body").animate({
                scrollTop: t.offset().top - 20
            }, 800)
        }).fail(function(e) {
            jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), alert("The request failed, please try again")
        })
    })
}

function newScaleCancelEvent() {
    jQuery("#cancel_new_scale").length && jQuery("#cancel_new_scale").on("click", function() {
        var e = jQuery(this).parents("table:first").parents("tr:first").prev("tr").find("input[id^=section-ids]").val(),
            t = jQuery("#itemaddtd"),
            i = jQuery("#item_text").attr("id");
        tinymce.execCommand("mceRemoveEditor", !1, i);
        var a = Math.random();
        t.html("&nbsp;").attr("class", "waittdw2"), jQuery.ajax({
            url: "forms/ajx_scale_buttons.php?",
            type: "post",
            data: {
                sid: a,
                section_id: e
            }
        }).done(function(e) {
            jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), t.attr("class", "section-buttons-td"), t.html(e), newScaleAddEvent()
        }).fail(function(e) {
            alert("The request failed, please try refresh page [F5 key] and try again")
        })
    })
}

function newScaleSubmitEvent() {
    jQuery("#submit_new_scale").length && jQuery("#submit_new_scale").on("click", function() {
        jQuery(this).blur();
        var e = jQuery(this).parents("table:first").parents("tr:first").prev("tr").find("input[id^=section-ids]").val();
        if (jQuery("#itemaddtd").length && 0 != tinymce.activeEditor.getContent().length) {
            tinymce.triggerSave();
            var t = tinymce.activeEditor.getContent(),
                i = !1,
                a = jQuery("#radiobuttonsdiv").find("input[id^=radio-]"),
                r = jQuery("#radiobuttonsdiv").find("textarea[id^=radiotext-]"),
                n = jQuery("#scale-option-types").find("input[class=scale-types-radio]:checked");
            if (0 !== a.length || 0 !== r.length)
                if (0 !== n.length) {
                    var s = 0,
                        l = [];
                    jQuery.each(a, function(e, t) {
                        0 === jQuery(t).val().length && (i = !0), l[s] = jQuery(t).val(), s++
                    });
                    var o = 0,
                        d = [];
                    if (jQuery.each(r, function(e, t) {
                            0 === jQuery(t).val().length && (i = !0), d[o] = jQuery(t).val(), o++
                        }), !0 === i) alert("Incomplete fields, please review");
                    else {
                        jQuery("#buttonpanel").find("input").css("display", "none"), jQuery("#buttonpanel").attr("class", "bpanwait");
                        var c = Math.random();
                        jQuery.ajax({
                            url: "forms/submit_new_item_scale.php?",
                            type: "post",
                            data: {
                                sid: c,
                                section_id: e,
                                radiofar: l,
                                radiofdescar: d,
                                item_text: t,
                                scale: n.eq(0).val()
                            }
                        }).done(function(e) {
                            refreshPage(!0)
                        }).fail(function(e) {
                            jQuery("#buttonpanel").attr("class", "bpan"), jQuery("#buttonpanel").find("input").css("display", ""), alert("The request failed, please try again")
                        })
                    }
                } else alert("Please select a Scale Type");
            else alert("Incomplete fields, please review")
        } else alert("Incomplete fields, please review")
    })
}

function currentScaleUpdateItem(e) {
    if (jQuery("#update_scale_item").blur(), 0 != tinymce.activeEditor.getContent().length) {
        tinymce.triggerSave();
        var t = tinymce.activeEditor.getContent(),
            i = !1,
            a = jQuery("#radiobuttonsdiv").find("input[id^=radio]"),
            r = jQuery("#radiobuttonsdiv").find("textarea[id^=radiotext]"),
            n = jQuery("#scale-option-types").find("input[class=scale-types-radio]:checked");
        if (0 !== a.length && 0 !== r.length)
            if (0 !== n.length) {
                var s = 0,
                    l = [];
                jQuery.each(a, function(e, t) {
                    0 === jQuery(t).val().length && (i = !0), l[s] = jQuery(t).val(), s++
                });
                var o = 0,
                    d = [];
                if (jQuery.each(r, function(e) {
                        0 === jQuery(e).val().length && (i = !0), d[o] = jQuery(e).val(), o++
                    }), !0 === i) alert("Some fields in this section are incomplete");
                else {
                    var c = Math.random();
                    jQuery("#buttonpanel").find("input").css("display", "none"), jQuery("#buttonpanel").attr("class", "bpanwait"), jQuery.ajax({
                        url: "forms/update_scale_item.php?",
                        type: "post",
                        data: {
                            sid: c,
                            item_id: e,
                            item_text: t,
                            radfvals: l,
                            radfdesc: d,
                            scale: n.eq(0).val()
                        }
                    }).done(function() {
                        refreshPage(!0)
                    }).fail(function(e) {
                        jQuery("#buttonpanel").attr("class", "bpan"), jQuery("#buttonpanel").find("input").css("display", ""), alert("The request failed, please try again")
                    })
                }
            } else alert("Please select a Scale Type");
        else alert("Incomplete fields, please review")
    } else alert("Incomplete fields, please review")
}

function currentScaleEditEvent(e) {
    var t = jQuery(e).parents("tr:first"),
        i = t.clone(!0, !0),
        a = t.find("input[id^=quest_cb]").val(),
        r = jQuery("<tr>", {
            id: "edit_qr"
        }),
        n = jQuery("<td>", {
            id: "edit_qc",
            colspan: "7",
            class: "waittdw"
        });
    n.html("&nbsp;"), r.prepend(n), t.replaceWith(r);
    jQuery("#assessform_sections_form").find("input, select, img[class=arrow], img[class=editb], img[class=comment-img], a").css("visibility", "hidden");
    var s = Math.random();
    jQuery.ajax({
        url: "forms/ajx_edit_scale_item.php?",
        type: "post",
        data: {
            sid: s,
            item_id: a
        }
    }).done(function(e) {
        n.html(e), tinymce.execCommand("mceAddEditor", !1, jQuery("#item_text").attr("id")), n.attr("class", "edit"), scaletuttonEvents(i, r, a), scaleFlipEvent(), previewItemTypes(), changeScaleTypeEdit(), jQuery("html, body").animate({
            scrollTop: jQuery("#itemeditdiv").offset().top - 20
        }, 800)
    }).fail(function(e) {
        r.replaceWith(i), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), i.find("a[id^=editq]").on("click", function(e) {
            e.preventDefault(), currentScaleEditEvent(this)
        }), alert("Request failed, please try again")
    })
}

function changeScaleType() {
    jQuery("#scale-option-types").on("click", "input.scale-types-radio", function() {
        var e = jQuery(this).parents("table:first").parents("tr:first").prev("tr").find("input[id^=section-ids]").val(),
            t = jQuery(this).val(),
            i = Math.random();
        jQuery("#qansoptions").empty().attr("class", "ansoptcdivwait"), jQuery.ajax({
            url: "forms/scale_type.php?",
            type: "post",
            data: {
                sid: i,
                isajaxcall: "yes",
                section_id: e,
                type: t
            }
        }).done(function(e) {
            jQuery("#qansoptions").attr("class", "ansoptioncreatediv"), jQuery("#qansoptions").html(e), scaleFlipEvent()
        }).fail(function(e) {
            jQuery("#qansoptions").attr("class", "ansoptioncreatediv"), alert("The request failed, please try again")
        })
    })
}

function changeScaleTypeEdit() {
    jQuery("#scale-option-types").on("click", "input.scale-types-radio", function() {
        var e = jQuery(this).val(),
            t = jQuery("#scale-item-id").val(),
            i = jQuery("#qansoptions"),
            a = Math.random();
        i.empty().attr("class", "ansoptcdivwait"), jQuery.ajax({
            url: "forms/edit_scale_type.php?",
            type: "post",
            data: {
                sid: a,
                isajaxcall: "yes",
                item_id: t,
                type: e
            }
        }).done(function(e) {
            i.attr("class", "ansoptioncreatediv"), i.html(e), scaleFlipEvent()
        }).fail(function(e) {
            i.attr("class", "ansoptioncreatediv"), alert("The request failed, please try again")
        })
    })
}

function canceladdItemToSection(e) {
    var t = jQuery(e).attr("id").split("-"),
        i = jQuery("#itemaddtd-" + t[1]);
    if (jQuery("#item_text-" + t[1]).length) {
        var a = jQuery("#item_text-" + t[1]).attr("id");
        tinymce.execCommand("mceRemoveEditor", !1, a)
    }
    removeCompetenciesDialog();
    var r = Math.random();
    i.html("&nbsp;").attr("class", "waittdw2"), jQuery.ajax({
        url: "forms/ajx_section_buttons.php?",
        type: "post",
        data: {
            sid: r,
            section_id: t[1]
        }
    }).done(function(e) {
        jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), i.attr("class", "section-buttons-td"), i.html(e), i.parents("tr:first").prev("tr").length && i.parents("tr:first").prev("tr").removeClass("baseborder")
    }).fail(function(e) {
        alert("The request failed, please try refresh page [F5 key] and try again")
    })
}

function changeItemType(e) {
    var t, i = jQuery(e).attr("id").split("-"),
        a = i[1],
        r = i[0],
        n = jQuery("#qansoptions-" + a),
        s = Math.random();
    switch (r) {
        case "item_type_radio":
            t = "forms/add_radio_options.php?";
            break;
        case "item_type_text":
            t = "forms/add_textbox_option.php?";
            break;
        case "item_type_slider":
            t = "forms/add_slider_option.php?";
            break;
        case "item_type_checkbox":
            t = "forms/checkbox_option_add.php?"
    }
    n.empty().attr("class", "ansoptcdivwait"), jQuery.ajax({
        url: t,
        type: "post",
        data: {
            sid: s,
            isajaxcall: "yes",
            section_id: a
        }
    }).done(function(e) {
        n.attr("class", "ansoptioncreatediv"), n.html(e), setupAddFlipEvent(a), flagsToggle()
    }).fail(function(e) {
        n.attr("class", "ansoptioncreatediv"), alert("The request failed, please try again")
    })
}

function changeItemTypeEdit(e) {
    var t, i = jQuery(e).attr("id").split("-"),
        a = i[1],
        r = i[0],
        n = Math.random(),
        s = jQuery("#qansoptions-" + a);
    switch (s.empty().attr("class", "ansoptcdivwait"), r) {
        case "item_type_radio":
            t = "forms/edit_radio_options.php?";
            break;
        case "item_type_text":
            t = "forms/edit_textbox_option.php?";
            break;
        case "item_type_slider":
            t = "forms/edit_slider_option.php?";
            break;
        case "item_type_checkbox":
            t = "forms/checkbox_option_edit.php?"
    }
    jQuery.ajax({
        url: t,
        type: "post",
        data: {
            sid: n,
            isajaxcall: "yes",
            item_id: a
        }
    }).done(function(e) {
        s.attr("class", "ansoptioncreatediv"), s.html(e), setupEditFlipEvent(a), flagsToggle()
    }).fail(function(e) {
        s.attr("class", "ansoptioncreatediv"), alert("The request failed, please try again")
    })
}

function renderRadioOptions(e) {
    var t = jQuery(e).attr("id");
    idArray = t.split("-");
    var i = idArray[1],
        a = jQuery("#radiobuttonsdiv-" + i);
    if (isNaN(jQuery(e).val())) jQuery(e).val(""), a.html("Non-Numeric Value entered.<br/><br/>"), a.addClass("boldred");
    else {
        a.hasClass("boldred") && a.removeClass("boldred");
        var r = Math.random();
        a.empty().attr("class", "radcontwait"), jQuery.ajax({
            url: "forms/ajx_add_radio_fields.php?",
            type: "post",
            data: {
                sid: r,
                isajaxcall: "yes",
                quantity: jQuery(e).val(),
                section_id: i
            }
        }).done(function(e) {
            a.attr("class", "radcont").html(e), flagsToggle()
        }).fail(function(e) {
            a.attr("class", "radcont"), alert("The request failed, please try again")
        })
    }
}

function renderRadioOptionsEdit(e) {
    var t = jQuery(e).attr("id");
    idArray = t.split("-");
    var i = idArray[1],
        a = jQuery("#radiobuttonsdiv-" + i);
    if (isNaN(jQuery(e).val())) jQuery(e).val(""), a.html("Non-Numeric Value entered.<br/><br/>"), a.addClass("boldred");
    else {
        a.hasClass("boldred") && a.removeClass("boldred");
        var r = Math.random();
        a.empty().attr("class", "radcontwait"), jQuery.ajax({
            url: "forms/ajx_edit_radio_fields.php?",
            type: "post",
            data: {
                sid: r,
                isajaxcall: "yes",
                item_id: i,
                quantity: jQuery(e).val()
            }
        }).done(function(e) {
            a.attr("class", "radcont").html(e), flagsToggle()
        }).fail(function(e) {
            a.attr("class", "radcont"), alert("The request failed, please try again")
        })
    }
}

function deleteSectionItems(e) {
    var t = jQuery(e).attr("id").split("-")[1],
        i = jQuery("#sectiontd-" + t).find("input[id^=quest_cb-" + t + "]"),
        a = 0,
        r = Math.random(),
        n = [];
    jQuery.each(i, function(e, t) {
        jQuery(t).prop("checked") && (n[a] = jQuery(t).val(), a++)
    }), a > 0 ? confirm("Are you sure you want to delete the selected item(s)?") && jQuery.ajax({
        url: "forms/delete_items.php?",
        type: "post",
        data: {
            sid: r,
            section_id: t,
            to_del: n
        }
    }).done(function(e) {
        refreshPage(!0)
    }).fail(function(e) {
        alert("The request failed, please try again")
    }) : alert("You have not selected an Item")
}

function hasNumber(e) {
    var t = e.which ? e.which : e.keyCode;
    return !(t > 31 && 46 != t && (t < 48 || t > 57))
}

function weightVal() {
    var e = jQuery("#weightcb"),
        t = jQuery("#weighValLbl"),
        i = jQuery("#itemWeightVal");
    1 == e.prop("checked") ? (i.show(), t.show()) : (t.hide(), i.hide())
}

function validateItemWeight() {
    var e = jQuery("#itemWeightVal"),
        t = parseFloat(e.val()),
        i = parseFloat(e.attr("min")),
        a = parseFloat(e.attr("max"));
    return t > a || t < i ? (alert("Incorrect Weightings set. Value must be within " + i + " and " + a + " range. Please review. Default will be set to 1."), e.val("1.00"), null) : t
}

function submitNewItem(e) {
    jQuery(e).blur();
    var t, i, a, r, n = jQuery(e).attr("id").split("-")[1],
        s = Math.random(),
        l = !jQuery("#item_order-" + n).length || 0 === jQuery("#item_order-" + n).val().length;
    if (jQuery("#self-assess-" + n).length ? (i = jQuery("#self-assess-" + n).text().length > 0, a = !0) : (i = tinymce.activeEditor.getContent().length > 0, a = !1), !l && i) {
        a ? r = "<p>No self assessment question data</p>" : (tinymce.triggerSave(), r = tinymce.activeEditor.getContent());
        var o = [],
            d = 0;
        if (jQuery(".selected-competencies").each(function() {
                var e = jQuery(this);
                o[d] = e.val(), d++
            }), jQuery("#item_type_text-" + n).prop("checked") || jQuery("#item_type_slider-" + n).prop("checked")) {
            if (null !== (E = validateItemWeight())) {
                if (!(jQuery("#descriptor-" + n).length && jQuery("#text_rangeone-" + n) && jQuery("#text_rangetwo-" + n))) return void alert("Incomplete fields, please review");
                var c = jQuery("#descriptor-" + n),
                    u = jQuery("#text_rangeone-" + n),
                    p = jQuery("#text_rangetwo-" + n);
                0 === u.val().length || 0 === p.val().length ? alert("Incomplete fields, please review") : (t = jQuery("#item_type_text-" + n).prop("checked") ? "score_entry" : "slider_range", jQuery("#buttonpanel-" + n).find("input").css("display", "none"), jQuery("#buttonpanel-" + n).attr("class", "bpanwait"), jQuery.ajax({
                    url: "forms/submit_new_item.php",
                    type: "post",
                    data: {
                        sid: s,
                        section_id: n,
                        item_type: t,
                        item_text: r,
                        item_order: jQuery("#item_order-" + n).val(),
                        descriptor: c.val(),
                        text_rangeone: u.val(),
                        text_rangetwo: p.val(),
                        weighted_value: E,
                        competencies: o
                    }
                }).done(function(e) {
                    refreshPage(!0)
                }).fail(function(e) {
                    jQuery("#buttonpanel-" + n).attr("class", "bpan"), jQuery("#buttonpanel-" + n).find("input").css("display", ""), alert("The request failed, please try again")
                }))
            }
        } else if (jQuery("#item_type_checkbox-" + n).prop("checked")) {
            if (!jQuery("#checkbox-unchecked-" + n).length || !jQuery("#checkbox-checked-" + n)) return void alert("Incomplete fields, please review");
            c = jQuery("#checkbox-descriptor-" + n);
            var y = jQuery("#checkbox-unchecked-" + n),
                f = jQuery("#checkbox-checked-" + n);
            0 === y.val().length || 0 === f.val().length ? alert("Incomplete fields, please review") : (jQuery("#buttonpanel-" + n).find("input").css("display", "none"), jQuery("#buttonpanel-" + n).attr("class", "bpanwait"), jQuery.ajax({
                url: "forms/checkbox_option_save.php",
                type: "post",
                data: {
                    id: n,
                    sid: s,
                    text: r,
                    order: jQuery("#item_order-" + n).val(),
                    descriptor: c.val(),
                    unchecked: y.val(),
                    checked: f.val(),
                    competencies: o,
                    mode: "add"
                }
            }).done(function(e) {
                refreshPage(!0)
            }).fail(function(e) {
                jQuery("#buttonpanel-" + n).attr("class", "bpan"), jQuery("#buttonpanel-" + n).find("input").css("display", ""), alert("The request failed, please try again")
            }))
        } else if (jQuery("#item_type_radio-" + n).prop("checked")) {
            var m = !1;
            if (!(jQuery("#radiobuttonsdiv-" + n).length && jQuery("#numradiobuttons-" + n) && jQuery("#numradiobuttons-" + n).val().length > 0 && jQuery("#numradiobuttons-" + n).val() > 0)) return void alert("Incomplete fields, please review");
            var v = jQuery("#radiobuttonsdiv-" + n),
                j = v.find("input[id^=radio-" + n + "]"),
                h = v.find("textarea[id^=radiotext-" + n + "]"),
                Q = v.find("input[id^=flag-" + n + "]");
            if (0 === j.length && 0 === h.length) return void alert("Incomplete fields, please review");
            var _ = jQuery("#isnumval-" + n).prop("checked"),
                g = !1;
            "forms/submit_new_item_radio.php?";
            var b = 0,
                x = [];
            if (jQuery.each(j, function(e, t) {
                    0 === jQuery(t).val().length && (m = !0), _ && !isFinite(jQuery(t).val()) && (g = !0), x[b] = jQuery(t).val(), b++
                }), g) return void alert("Numeric (Score) Values Only");
            _ = !0 === _ ? 1 : 0;
            var w = 0,
                k = [];
            jQuery.each(h, function(e, t) {
                0 === jQuery(t).val().length && (m = !0), k[w] = jQuery(t).val(), w++
            });
            var E, q = 0,
                T = [];
            jQuery.each(Q, function(e, t) {
                T[q] = jQuery(t).val(), q++
            }), null !== (E = validateItemWeight()) && (m ? alert("Incomplete fields, please review") : (jQuery("#buttonpanel-" + n).find("input").css("display", "none"), jQuery("#buttonpanel-" + n).attr("class", "bpanwait"), jQuery.ajax({
                url: "forms/submit_new_item_radio.php?",
                type: "post",
                data: {
                    sid: s,
                    section_id: n,
                    isnumval: _,
                    radiofar: x,
                    radiofdescar: k,
                    radio_flags: T,
                    item_order: jQuery("#item_order-" + n).val(),
                    item_text: r,
                    weighted_value: E,
                    competencies: o
                }
            }).done(function(e) {
                refreshPage(!0)
            }).fail(function(e) {
                jQuery("#buttonpanel-" + n).attr("class", "bpan"), jQuery("#buttonpanel-" + n).find("input").css("display", ""), alert("The request failed, please try again")
            })))
        }
    } else alert("Incomplete fields, please review")
}

function previewItemTypes() {
    jQuery(".hoverimage").tooltip({
        placement: "left",
        track: !1,
        classes: {
            "ui-tooltip": "imagetips"
        }
    })
}

function setupEditButtons(e, t, i) {
    jQuery("#cancel_edit_item").length && (jQuery("#cancel_edit_item").on("click", function() {
        if (jQuery("#item_text-" + i).length) {
            var a = jQuery("#item_text-" + i).attr("id");
            tinymce.execCommand("mceFocus", !1, a), tinymce.execCommand("mceRemoveEditor", !1, a)
        }
        removeCompetenciesDialog(), t.prev("tr") && t.prev("tr").removeClass("baseborder"), t.replaceWith(e), t.remove(), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), e.find("a[class=standard]").on("click", function(e) {
            e.preventDefault(), addEditEvent(this)
        })
    }), jQuery("#update_item").on("click", function(e) {
        e.preventDefault(), updateItem(i)
    }))
}

function scaleEditButtonEvents(e, t, i) {
    jQuery("#cancel_update_scale_item").on("click", function() {
        var i = jQuery("#item_text").attr("id");
        tinymce.execCommand("mceFocus", !1, i), tinymce.execCommand("mceRemoveEditor", !1, i), t.replaceWith(e), t.remove(), jQuery("#assessform_sections_form").find("input, select, img, a").css("visibility", "visible"), e.find("a[class=scale]").on("click", function(e) {
            e.preventDefault(), currentScaleEditEvent(this)
        })
    }), jQuery("#update_scale_item").on("click", function(e) {
        e.preventDefault(), currentScaleUpdateItem(i)
    })
}

function setupEditFlipEvent(e) {
    var t;
    jQuery("#flip-" + e).length && jQuery("#flip-" + e).on("click", function() {
        var i = jQuery("#radiobuttonsdiv-" + e).find("input[id^=radio-" + e + "]"),
            a = [],
            r = [],
            n = i.length,
            s = n;
        if (n > 0)
            for (jQuery.each(i, function(t, i) {
                    var n = i.attr("id").split("-");
                    a[s] = i.val(), r[s] = jQuery("#radiotext-" + e + "-" + n[2]).val(), s--
                }), t = 1; t <= n; t++) jQuery("#radio-" + e + "-" + t).val(a[t]), jQuery("#radiotext-" + e + "-" + t).val(r[t])
    })
}

function setupAddFlipEvent(e) {
    var t;
    jQuery("#flip-" + e).length && jQuery("#flip-" + e).on("click", function() {
        var i = jQuery("#radiobuttonsdiv-" + e).find("input[id^=radio-" + e + "]"),
            a = [],
            r = [],
            n = i.length,
            s = n;
        if (n > 0)
            for (jQuery.each(i, function(t, i) {
                    var n = jQuery(i).attr("id").split("-");
                    a[s] = jQuery(i).val(), r[s] = jQuery("#radiotext-" + e + "-" + n[2]).val(), s--
                }), t = 1; t <= n; t++) jQuery("#radio-" + e + "-" + t).val(a[t]), jQuery("#radiotext-" + e + "-" + t).val(r[t])
    })
}

function scaleFlipEvent() {
    jQuery("#flip").length && jQuery("#flip").on("click", function() {
        var e = jQuery("#radiobuttonsdiv").find("input[id^=radio-]"),
            t = [],
            i = [],
            a = e.length,
            r = a;
        if (a > 0) {
            jQuery.each(e, function(e, a) {
                var n = a.attr("id").split("-")[1];
                t[r] = a.val(), i[r] = jQuery("#radiotext-" + n).val(), r--
            });
            for (var n = 1; n <= a; n++) jQuery("#radio-" + n).val(t[n]), jQuery("#radiotext-" + n).val(i[n])
        }
    })
}

function updateItem(e) {
    jQuery("#update_item").blur();
    var t, i, a, r, n = Math.random(),
        s = !jQuery("#item_order-" + e).length || 0 === jQuery("#item_order-" + e).val().length;
    if (jQuery("#self-assess-" + e).length ? (a = jQuery("#self-assess-" + e).text().length > 0, i = !0) : (a = tinymce.activeEditor.getContent().length > 0, i = !1), !s && a) {
        i ? r = "<p>No self assessment question data</p>" : (tinymce.triggerSave(), r = tinymce.activeEditor.getContent());
        var l = jQuery("#item_order-" + e),
            o = [],
            d = 0;
        if (jQuery(".selected-competencies").each(function() {
                var e = jQuery(this);
                o[d] = e.val(), d++
            }), jQuery("#item_type_text-" + e).prop("checked") || jQuery("#item_type_slider-" + e).prop("checked")) {
            if (null !== (E = validateItemWeight())) {
                if (!(jQuery("#descriptor-" + e).length && jQuery("#text_rangeone-" + e) && jQuery("#text_rangetwo-" + e))) return void alert("Incomplete fields, please review");
                var c = jQuery("#descriptor-" + e),
                    u = jQuery("#text_rangeone-" + e),
                    p = jQuery("#text_rangetwo-" + e);
                0 === u.val().length || 0 === p.val().length ? alert("Some fields in this section are incomplete") : (t = jQuery("#item_type_slider-" + e).prop("checked") ? "slider_range" : "value_input", jQuery("#buttonpanel-" + e).find("input").css("display", "none"), jQuery("#buttonpanel-" + e).attr("class", "bpanwait"), jQuery.ajax({
                    url: "forms/update_item.php",
                    type: "post",
                    data: {
                        sid: n,
                        item_id: e,
                        item_type: t,
                        item_order: l.val(),
                        item_text: r,
                        descriptor: c.val(),
                        text_rangeone: u.val(),
                        text_rangetwo: p.val(),
                        weighted_value: E,
                        competencies: o
                    }
                }).done(function(e) {
                    refreshPage(!0)
                }).fail(function(t) {
                    jQuery("#buttonpanel-" + e).attr("class", "bpan"), jQuery("#buttonpanel-" + e).find("input").css("display", ""), alert("The request failed, please try again")
                }))
            }
        } else if (jQuery("#item_type_checkbox-" + e).prop("checked")) {
            if (!jQuery("#checkbox-unchecked-" + e).length || !jQuery("#checkbox-checked-" + e)) return void alert("Incomplete fields, please review");
            c = jQuery("#checkbox-descriptor-" + e);
            var y = jQuery("#checkbox-unchecked-" + e),
                f = jQuery("#checkbox-checked-" + e);
            0 === y.val().length || 0 === f.val().length ? alert("Incomplete fields, please review") : (jQuery("#buttonpanel-" + e).find("input").css("display", "none"), jQuery("#buttonpanel-" + e).attr("class", "bpanwait"), jQuery.ajax({
                url: "forms/checkbox_option_save.php",
                type: "post",
                data: {
                    id: e,
                    sid: n,
                    text: r,
                    order: l.val(),
                    descriptor: c.val(),
                    unchecked: y.val(),
                    checked: f.val(),
                    competencies: o,
                    mode: "update"
                }
            }).done(function(e) {
                refreshPage(!0)
            }).fail(function(t) {
                jQuery("#buttonpanel-" + e).attr("class", "bpan"), jQuery("#buttonpanel-" + e).find("input").css("display", ""), alert("The request failed, please try again")
            }))
        } else if (jQuery("#item_type_radio-" + e).prop("checked")) {
            var m = !1;
            if (!(jQuery("#radiobuttonsdiv-" + e).length && jQuery("#numradiobuttons-" + e) && jQuery("#numradiobuttons-" + e).val() > 0 && jQuery("#isnumval-" + e))) return void alert("Incomplete fields, please review");
            var v = jQuery("#radiobuttonsdiv-" + e),
                j = v.find("input[id^=radio-" + e + "]"),
                h = v.find("textarea[id^=radiotext-" + e + "]"),
                Q = v.find("input[id^=flag-" + e + "]"),
                _ = jQuery("#isnumval-" + e).prop("checked");
            if (0 === j.length || 0 === h.length) return void alert("Incomplete fields, please review");
            var g = !1,
                b = 0,
                x = [];
            if (jQuery.each(j, function(e, t) {
                    0 === jQuery(t).val().length && (m = !0), !0 !== _ || isFinite(jQuery(t).val()) || (g = !0), x[b] = jQuery(t).val(), b++
                }), !0 === g) return void alert("Numeric (Score) Values Only");
            _ = !0 === _ ? 1 : 0;
            var w = 0,
                k = [];
            jQuery.each(h, function(e, t) {
                0 === jQuery(t).val().length && (m = !0), k[w] = jQuery(t).val(), w++
            });
            var E, q = 0,
                T = [];
            jQuery.each(Q, function(e, t) {
                T[q] = jQuery(t).val(), q++
            }), null !== (E = validateItemWeight()) && (!0 === m ? alert("Incomplete fields, please review") : ("forms/update_radio_item.php", jQuery("#buttonpanel-" + e).find("input").css("display", "none"), jQuery("#buttonpanel-" + e).attr("class", "bpanwait"), jQuery.ajax({
                url: "forms/update_radio_item.php",
                type: "post",
                data: {
                    sid: n,
                    item_id: e,
                    isnumval: _,
                    item_order: l.val(),
                    item_text: r,
                    radfvals: x,
                    radfdesc: k,
                    radio_flags: T,
                    weighted_value: E,
                    competencies: o
                }
            }).done(function(e) {
                refreshPage(!0)
            }).fail(function(t) {
                jQuery("#buttonpanel-" + e).attr("class", "bpan"), jQuery("#buttonpanel-" + e).find("input").css("display", ""), alert("The request failed, please try again")
            })))
        }
    } else alert("Incomplete fields, please review")
}


