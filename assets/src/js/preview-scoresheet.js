/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2018 Qpercom Limited.  All rights reserved.
 */
jQuery(document).ready(function() {

	if (jQuery('#exam_assessment_table').length) {

        jQuery('#status').remove();
        jQuery('#settings-td').addClass('nbb');
        jQuery('#exam_assessment_table').css('display', ((detectIE()) ? 'block' : 'table'));

        /**
         * Checkbox row highlighting
         */
        function cbRowHighlight(element) {

            var cbParentTr = element.parents('td:first').parents('tr:first');

            if (element.is(':checked')) {

                cbParentTr.addClass('checked');

            } else {

                cbParentTr.removeClass('checked');

            }

        }

        // Event for checkbox item highlighting
        jQuery('table#exam_assessment_table').on('click', 'input.checkbox-control', function() {

            cbRowHighlight(jQuery(this));

        });

        // Comment Fields Code
        jQuery.each(jQuery('.comment'), function(i, td) {

            var h = jQuery(td).get(0).clientHeight;
            var ta = jQuery(td).find('textarea');

            jQuery(td)
                .on('focus', function() {
                    jQuery(this).css('color', '#000000');
                })
                .on('blur', function() {
                    if (jQuery(this).val().length === 0) {

                        if (jQuery(this).attr('class').indexOf('crequired') > -1) {
                            jQuery(this).addClass('tahighlight');
                        }

                        jQuery(this).css('color', '#bfbfbf');

                    }
                })
                .on('keyup', function() {
                    if (jQuery(this).hasClass('tahighlight')) {

                        jQuery(this).removeClass('tahighlight');

                    }
                })

            if (h < 50) {
                h = 60;
            }

            ta.css('height', h);

        });

        //Prevent text selection/copy of item answers sections [IE only]
        if (detectIE()) {

            jQuery('.options, .items, .title-section, .title-sectionv2, .descriptors')
                .on('selectstart', function(){
                    return false;
                })
                .on('mousedown', function(){
                    return false;
                });

        }

        //hover over value
        jQuery( '.hmv' ).tooltip({
            placement: "left",
            track: false,
            classes: {
                "ui-tooltip": "tipsval"
              }
          });

        // Preview Options Events
        jQuery('#settings-td').on('change', 'select[class=preview-opts]', function(e) {

            var opt_url = "manage.php?page=previewform_osce&form_id=" + jQuery('#form_id').val() + "&marks=" + jQuery('#mar').val() + "&totals=" + jQuery('#tot').val();
            if (jQuery('#notes-pop').length) {

                opt_url += "&notes=" + jQuery('#notes-pop').val();

            }
            parent.location = opt_url;

        });

        //prepare sliders
        var sliders = jQuery('#exam_assessment_table').find("div[id^=slider-]");
        var domReadyLoad = 1;

        if (sliders.length > 0) {

            jQuery.each(sliders, function(i, trigger) {
                var knob = jQuery(trigger).find('.ui-slider-handle');
                var answerID = jQuery(knob).attr('id').split('-')[1];
                var answer = jQuery('#ansoption-' + answerID);
                var answerValue = (answer.val() * 1);
                var start = (jQuery('#start-value' + answerID).val() * 1);
                var end = (jQuery('#end-value' + answerID).val() * 1);

                jQuery(trigger).slider({
                        animate: "fast",
                        min: start,
                        max: end,
                        value: answerValue,
                        slide: function(e, ui) {
                            if (jQuery('#view_marks').val() == 1) {
                                knob.text((ui.value * 1));
                            } else {
                                var max = Math.max(start, end);
                                var percentCalculated = getPercent(ui.value, max);
                                knob.text(percentCalculated);
                            }
                        },
                        stop: function(e, ui) {
                            answer.val(ui.value * 1);
                            if(!domReadyLoad) {
                              CalculateTotal(trigger);
                            }
                        }
                    });
            });
        }
        domReadyLoad = 0;
    }

    // Disable directional keys when using radio button groups
    jQuery('input[type="radio"]').keydown(function(e) {

        var arrowKeys = [37, 38, 39, 40];

        if (arrowKeys.indexOf(e.which) !== -1) {
            jQuery(this).blur();
            return false;
        }

    });

    //prepare clock
    if (jQuery('#clock').length) {

        var tlimit_default = 300000;
        var value = jQuery('#time_limit').val();
        var reg = /^([0-9]\\d):?([0-5]\\d):?([0-5]\\d)$/;
        if (jQuery('#time_limit').length && reg.test(value)) {
            var tlimitdb = jQuery('#time_limit').val().split(':');
            tlimit_default = (tlimitdb[0] * 60 * 60 * 1000) + (tlimitdb[1] * 60 * 1000) + (tlimitdb[2] * 1000);
        }

        initializeClock(
            new Date(new Date().getTime() + tlimit_default),
            function(counter) {
                var text = '';
                text += (counter.hours > 9 ? '' : '0') + counter.hours + ':';
                text += (counter.minutes > 9 ? '' : '0') + counter.minutes + ':';
                text += (counter.seconds > 9 ? '' : '0') + counter.seconds;

                jQuery('#clock').text(text);
            },
            function() {
                jQuery('#clock').attr('class', 'clock-off');
            })

    }

    //prepare station notes event
    if (jQuery('#station-notes-outer').length) {

        // Mask window document object
        // var window_mask = new Mask({maskMargins: true});

        // add position
        var left = (jQuery(window).width() / 2) - (jQuery('#station-notes-outer').width() / 2);
        var top = (jQuery(window).height() / 2) - (jQuery('#station-notes-outer').height() / 2);
        jQuery('#station-notes-outer').css({
            position: 'absolute',
            left: left,
            top: top
        });

        if (jQuery('#notes-pop').length && jQuery('#notes-pop').val() == 1) {
            // window_mask.show();
            jQuery('#station-notes-outer').css('display', 'block');
        }

        if (jQuery('#show-notes').length) {
            jQuery('#show-notes').on('click', function(e) {
                e.preventDefault();
                // window_mask.show();
                jQuery('#station-notes-outer').css('display', 'block');
            });
        }

        jQuery('#close-notes').on('click', function(e) {
            e.preventDefault();
            jQuery('#station-notes-outer').css('display', 'none');
            // window_mask.hide();
        });
    }

    //Document Title
    document.title = "Preview | " + jQuery('#form_name').val() + " - " + jQuery('#dept').val();
});

// Calculate Total
function CalculateTotal(trigger) {
    var parentTr = jQuery('#'+ trigger.id).parents('tr:first');
    var parentTrID = parentTr.attr('id');
    var parentTrIDArray = parentTrID.split('-');
    var sectionID = parentTrIDArray[1];
    var isScoring = false;
    var totalScore = 0;
    var sectionTotal = 0;
    var sectionRows = jQuery("#exam_assessment_table").find("tr[id^=section-" + sectionID + "-qrow]");

    // Turn on marked indication for item
    parentTr.addClass('done');

    /**
     * Compulsory feedback option function
     */

    // Feedback Text Area Object
    var feedbackField = jQuery('#comment-' + sectionID);

    // Remove Highlighting
    feedbackField.removeClass('tahighlight');

    // Comments are required in any case OR Fail option ticked OR Borderline option ticked
    var failOption = trigger.hasClass('fail');
    var blOption = trigger.hasClass('borderline');
    var flagOption = trigger.hasClass('flag');

    if (feedbackField.hasClass('crequired')
        || (feedbackField.hasClass('crequired-fail') && failOption)
        || (feedbackField.hasClass('crequired-bl') && blOption)
        || (feedbackField.hasClass('crequired-fb') && (failOption || blOption))
        || (feedbackField.hasClass('crequired-flag') && flagOption)) {

         // Highlight Text Area
         if (feedbackField.val().length == 0) {

             feedbackField.addClass('tahighlight');

         }

    }

    // Total up the scores
    jQuery.each(sectionRows, function(i, row) {
        var tds = jQuery(row).find("td[class^=options]");
        var firstCell = jQuery(tds[0]);

        // If for slider type and slider has already been marked
        if (firstCell.find("div[id^=slider]") !== null) {
          if(firstCell.parents('tr:first').hasClass('done')) {
            isScoring = true;
            var sliderElement = firstCell.find("input[id^=ansoption-]");
                sectionTotal += (sliderElement.val() * 1);
            }
        // Everything else
        } else {
            var firstElement = firstCell.find('input:first');
            var propertyType = firstElement.attr('type');

            // Radio type
            if (propertyType === 'radio') {
                var elements = row.find("input[class*=radoption]:checked");
                if (elements.length > 0) {
                    var optionID = jQuery(elements[0]).attr('value');
                    var actualValue = jQuery("#ansoptval:" + optionID).attr('value');

                    if (isFinite(actualValue)) {
                        sectionTotal += (actualValue * 1);
                        isScoring = true;
                    }
                }

            // Text type
            } else if (propertyType === 'text') {
                isScoring = true;

                if (firstElement.val().length > 0) {

                    // Format decimal values e.g. '.5' => '0.5', '8.' => '8' etc
                    firstElement.val(parseFloat(firstElement.val()));

                    var textValue = (firstElement.val() * 1);
                    var trID = firstElement.parents('tr:first').attr('id');
                    var trIDArray = trID.split('-');
                    var itemIDNum = trIDArray[3];
                    var textValueLimits = jQuery("#lowhigh" + itemIDNum).val();
                    var limitsArray = textValueLimits.split(',');
                    var lowestLimit = limitsArray[0];
                    var highestLimit = limitsArray[1];

                    // Validate field
                    if (!jQuery.isNumeric(textValue) || (textValue < lowestLimit || textValue > highestLimit)) {

                        row.removeClass('done');
                        alert("Number you entered must be in range (" + lowestLimit + " to " + highestLimit + ")");
                        firstElement.val('');

                        /*
                         * Re-focus on the textbox
                         * 'focus()' doesn't work right away in Chrome so we need to set a timer
                         */
                        setTimeout(function() {
                          firstElement.focus();
                        }, 0);

                    } else {

                        sectionTotal += textValue;

                    }

               } else {

                   row.removeClass('done done-darker updated-score submitted-score');

               }

            }
        }
    });

    // If we have scoring items within the section then total them up
    if (isScoring) {
        if (jQuery('#totalsection-' + sectionID).length) {
            jQuery('#totalsection-' + sectionID).val(sectionTotal);
        }

        //Section count
        var sectioncount = jQuery('#sectioncount').val();

        //Sum up the section totals to get the overall total
        for (var j=1; j<=sectioncount; j++) {
            if (jQuery('#totalsection-' + j).length) {
                totalScore += (jQuery('#totalsection-' + j).val() * 1);
            }
        }
        if (jQuery('#scoreval').length) {
            jQuery('#scoreval').text(totalScore);
        }
    }
}
