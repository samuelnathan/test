/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Regular expression to parse and validate times in 24-hour format.
var validateTimeRegex = /^([0-9]|[0-1][0-9]|2[0-3]):[0-5][0-9]$/;
var called = 0;

jQuery(document).ready(function() {

    /*************** For PDF Print Export ***************/
    if (jQuery('#pdform').length) {
        if (jQuery('#pdframe-bulk').length) {
            jQuery('#pdframe-bulk').on('load', function(){
                removeWait();git
            });
        }
        if (jQuery('#sessioninfo').length) {
            document.title = "Marking Grids PDF Print | " + jQuery('#sessioninfo').val();
        }

        // Display or hide the 'Number of lines' input depending on whether the user selected to show
        // or hide the feedback area
        var feedbackAreaSelect = jQuery('#feedback-area');
        feedbackAreaSelect.change(function() {
            if (feedbackAreaSelect.val() === "0") {
                jQuery("#feedback-height-fieldset").css('display', 'none');
            } else {
                jQuery("#feedback-height-fieldset").css('display', 'inline');
            }
        });
    }

    if (jQuery('#return').length) {
        jQuery('#return').on('click', function() {
            window.location.href = 'manage.php?page=manage_osce#X' + jQuery('#exam_id').val()
        });
    }

    /* Filter sessions */
    jQuery('#filter-sessions').change(function() {
        parent.location = 'manage.php?page=sessions_osce'
                        + '&exam_id=' + jQuery('#exam_id').val()
                        + '&filter=' + jQuery('#filter-sessions').val();
    });

    if (jQuery('#sessions_list_form').length) {
        if (jQuery('#check-all').length) {
            addCheckboxEvents('sessions');
        }

        if (jQuery('#add').length) {
            addSessionEvent();
        }

        editSessionEvents();
    }

    showMenu();
    performScroll();

    // Check for results updates for all displayed sessions (Every 30 seconds)
    setInterval(function() {

      var listedSessions = [];
      jQuery('.session-records').each(function(sessionIndex) {
          listedSessions[sessionIndex] = jQuery(this).val();
      });

      // Skip if not found
      if (listedSessions.length == 0) {
         return true;
      }

      jQuery.ajax({
         url: 'osce/json_sessions.php',
         type: 'post',
         dataType: 'json',
         cache: false,
         data: {
            request: 'session_results_status',
            sessions: listedSessions
         }
        })
        .done(function(sessions) {

            if (Object.keys(sessions).length > 0) {

                jQuery('.session-records').each(function() {
                    var sessionRecordID = jQuery(this).val();

                    if (sessions[sessionRecordID] == true) {
                       jQuery('#session-results-link-'+sessionRecordID).removeClass('invisible');
                    } else {
                       jQuery('#session-results-link-'+sessionRecordID).addClass('invisible');
                    }

                });

            }

        })
        .fail(function(jqXHR) {

            console.log(
                httpStatusCodeHandler(
                jqXHR.status,
                "Could not retrieve session results status.\n"
            ));

        });

    }, 20000);

});

/******* For PDF Export *******/
function removeWait() {

    if (jQuery('#pdfwait').length && called === 0) {
        jQuery('#pdfwait').remove();
        called = 1;
    }

}

// add session event
function addSessionEvent() {

    var lastrow;
    jQuery('#add').on('click', function(e) {
        e.preventDefault();

        var elements = "select, button, input, a:not(.linkin), img:not(.multiexaminer-icon)";
        jQuery('#sessions_table').find(elements).css('display', 'none');

        if (jQuery('#first_sessions_row').length) {

            var fsr = jQuery("#first_sessions_row").remove();
            lastrow = jQuery("#title-row");

        } else {

            if (jQuery('#count').length) {

                lastrow = jQuery('#sessions_row_' + jQuery('#count').val());
            }

        }

        var new_addrow = jQuery('<tr>', {'id': 'add-row'});
        lastrow.after(new_addrow);

        var colspan = jQuery('#base-tr').find('td:first').attr('colspan');
        colspan--;

        setWaitStatusRow(jQuery(new_addrow), colspan);

        var url = "osce/ajx_session_view.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'add',
                exam: jQuery('#exam_id').val()
            }
         })
         .done(function(html){
            new_addrow.empty().attr('class', 'addrow').html(html);

            //Setup calendar
            setUpCalendar();

            //Load color selector
            loadColorSelector();

            //Time Picker - Session Schedule
            jQuery('#start-time').timepicker();
            jQuery('#end-time').timepicker();

            addButtonsRow();
            sessionTipEvents(jQuery('#add_desc'), "e.g. Morning, Afternoon");
            submitSessionEvent();
            addSessionCancelEvent(new_addrow, fsr);

            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            new_addrow.remove();
            jQuery('#sessions_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });

}

// Submit sesssion
function submitSessionEvent() {

    jQuery('#add-save').click(function() {

        // @TODO (DW) The value here looks too specific to be safe long-term?
        if (jQuery('#add_desc').val().length === 0 || jQuery('#add_desc').val() === "e.g. Morning, Afternoon") {
            alert("Please specify description");
            jQuery('#add_desc').addClass('red-border').focus();
            jQuery('#add_desc').off('keyup').on('keyup', function() {
                jQuery(this).removeClass('red-border');
            });

            // @TODO (DC) Time field validation needs to be improved
        } else if (areDateFieldsComplete(jQuery('#add-row'))) {

            var tdate = jQuery('#add-row').find('input[id=thedate]').val();
            var date = changeDateStringSeparator(tdate, '/', '-');
            var startTime = jQuery('#start-time').val().trim();
            var endTime = jQuery('#end-time').val().trim();

            // Validate start and end time
            if (validateTimeRegex.test(startTime) && validateTimeRegex.test(endTime) &&
                    (Date.parse(date + " " + startTime) < Date.parse(date + " " + endTime))) {

                jQuery(this).css('display', 'none');
                jQuery('#add-cancel').css('display', 'none');
                jQuery('#base-button-td').attr('class', 'waittd3');

                jQuery.ajax({
                       url: "osce/json_session_update.php",
                       type: "post",
                       data: {
                           sid: Math.random(),
                           mode: 'submit',
                           access: Number(jQuery('#add_acc').is(':checked')),
                           date: date,
                           description: jQuery('#add_desc').val(),
                           start_time: startTime,
                           end_time: endTime,
                           colour: (jQuery('#color').length ? jQuery('#color').val() : ''),
                           exam_id: jQuery('#exam_id').val(),
                           clone_stations: jQuery('#clone-stations').val(),
                           clone_groups: jQuery('#clone-groups').val(),
                           clone_assistants: jQuery('#clone-assistants').val()
                       },
                       dataType: "json"
                    })
                    .done(function() {

                       prepareScroll('scrolltobase');
                       parent.location = 'manage.php?page=sessions_osce&exam_id=' + jQuery('#exam_id').val();

                     })
                    .fail(function(jqXHR) {

                        if (jqXHR.status == 0) {
                            failAdd();
                            alert(
                              "The request failed, Please check your " +
                              "internet connection and try again"
                            );
                            return;
                        }

                        var message = httpStatusCodeHandler(
                          jqXHR.status,
                          "Failed to update record.\n"
                        );

                        alert(message);
                        refreshPage(false);

                });

            } else {
                alert("Schedule fields are invalid and/or end time comes before start time.");
            }
        } else {
            alert("Please complete all fields.");
        }

    });

}

// Add cancel event
function addSessionCancelEvent(row, fsr) {

    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();
        if (jQuery('#count').attr('value') == 0) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#sessions_table').find("select, img, input, a, button").css('display', '');
        removeNewRecordControls();
    });

}

//Edit Events
function editSessionEvents() {

    if (jQuery('#count').attr('value') > 0) {
        var buttons = jQuery('#sessions_list_form').find('a[id^="edit_"]');

        jQuery.each(buttons, function(i, b) {
            editSessionEvent(b);
        });
    }

}

function editSessionEvent(b) {

    jQuery(b).off('click', editSessionEventHandler).on('click', editSessionEventHandler);

}

function editSessionEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var session_id = row.find('input[id^=sessions-check]').val();
    var fsr = row.clone(true, true);
    var elements = "select, button, input, a:not(.linkin), img:not(.multiexaminer-icon)";
    jQuery('#sessions_table').find(elements).css('display', 'none');

    var colspan = jQuery('#base-tr').find('td:first').attr('colspan');
    colspan--;
    setWaitStatusRow(jQuery(row), colspan);
    var url = "osce/ajx_session_view.php";
    var random = Math.random();
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random, 
            isfor: "edit",
            session_id: session_id
        }
     })
     .done(function(html){
        if (html === "SESSION_NON_EXIST") {
            alert(
              "Record you are trying to edit was not found in the system, " +
              "click 'OK' to refresh List"
            );
            refreshPage(false);
        } else {
            row.empty().attr('class', "editrw")
            .attr('id', "edit_row").get(0).innerHTML = html;

            //Setup calendar
            setUpCalendar();

            //Load color selector
            loadColorSelector();

            // If both the Start and End fields are enabled then initiate time picker
            if (jQuery('#start-time').length && jQuery('#end-time').length) {
              // Time Picker - Session Schedule
              jQuery('#start-time').timepicker();
              jQuery('#end-time').timepicker();
            }

            editButtonsRow(jQuery(row), colspan);

            cancelUpdateSessionEvent(jQuery('#edit-cancel'), fsr, row);
            updateSessionEvent();
        }
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#sessions_table').find("input, select, a, img").css('display', '');
        editSessionEvent(fsr.find('a[id^=edit_]'));
        addCheckboxEvent(fsr.find('input[id^=sessions-check]'), "sessions");
        alert('The request failed, Please try again');
     });

}

//update event
function updateSessionEvent() {

    jQuery('#edit-update').click(function() {

       var tdate = jQuery('#edit_row').find('input[id=thedate]').val();
       var date = changeDateStringSeparator(tdate, '/', '-');

       if (areDateFieldsComplete(jQuery('#edit_row')) && jQuery('#edit_desc').val().length > 0) {

          // Start and End time values
          startTime = jQuery('#start-time').val().trim();
          endTime = jQuery('#end-time').val().trim();

          // Check that the times are correct
          timesValid = (validateTimeRegex.test(startTime) && validateTimeRegex.test(endTime)
                       && (Date.parse(date + " " + startTime) < Date.parse(date + " " + endTime)));

        // If time is valid continue
        if (timesValid) {

                jQuery('#edit-update').css('display', 'none');
                jQuery('#edit-cancel').css('display', 'none');
                jQuery('#b_edit_td').attr('class', 'waittd2');

                jQuery.ajax({
                       url: "osce/json_session_update.php",
                       type: "post",
                       data: {
                          sid: Math.random(),
                          mode: "update",
                          id: jQuery('#edit_id').val(),
                          date: date,
                          description: jQuery('#edit_desc').val(),
                          start_time: startTime,
                          end_time: endTime,
                          colour: (jQuery('#color').length ? jQuery('#color').val() : ''),
                          access: Number(jQuery('#edit_acc').is(':checked'))
                       },
                       dataType: "json"
                    })
                    .done(function() {

                        refreshPage(false);

                     })
                    .fail(function(jqXHR){

                        if (jqXHR.status == 0) {
                            failEdit();
                            alert(
                              "The request failed, Please check your " +
                              "internet connection and try again"
                            );
                            return;
                        }

                        var message = httpStatusCodeHandler(
                          jqXHR.status,
                          "Failed to update record.\n"
                        );

                        alert(message);
                        refreshPage(false);

                });

            } else {
                alert("Schedule fields are invalid or end time is not after start time.");
            }

        } else {
            alert("Please complete all fields.");
        }

    });

}

//cancel update event
function cancelUpdateSessionEvent(button, org, oldrow) {

    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#sessions_table').find("select, img, input, a, button").css('display', '');
        editSessionEvent(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=sessions-check]'), "sessions");
    })

}

//Add Session Field Tip Event
function sessionTipEvents(field, message) {

    jQuery(field)
        .on('focus', function(){
            if (jQuery(this).val() === message) {
                jQuery(this).val('');
                jQuery(this).removeClass('color-grey');
            }
        })
        .on('blur', function(){
            if (jQuery(this).val().length === 0) {
                jQuery(this).val(message);
                jQuery(this).addClass('color-grey');
            }
        });

}

/**
 * Load Color Selector - For Add and Update Session
 */
function loadColorSelector() {

    // Do the required fields exist ?
    if (jQuery('#color').length && jQuery('#remove-color'.length)) {
    jQuery('#color').colorPicker({pickerDefault: "",
        colors: ["ffffff", "FF0000", "0000FF", "FFFF00", "008000", "FFA500", "800080", "FFC0CB", "008080", "FFD700", "C0C0C0"],
        transparency: true,
        showHexField: false,
        onColorChange: function() {
            jQuery("#remove-color").css("visibility", "visible");
        }});

    // Clear/Remove color selected event
    jQuery("#remove-color").click(function() {
        jQuery("#color").val("");
        jQuery(".colorPicker-picker").attr("style", "");
        jQuery(this).css("visibility", "hidden");
    });
   }

}

/**
 * Validate form
 * @param object form object
 * @returns true|false
 */
function ValidateForm(form) {

    // Sessions Management
    if (jQuery(form).attr('name') === 'sessions_list_form' && (jQuery('#todo').val() === "delete")) {
        return confirm("Are you sure you would like to delete these Sessions");
    }

    return true;

}
