/* Original Author: David Cunningham
   For Qpercom Ltd
   Password field functions (jQuery)
   © 2018 Qpercom Limited.  All rights reserved
*/
    
var requiredVerdict = "strong"; 
var requiredVerdictNum = 3;
var nonMatchMessage = "";
var insufficientStrengthMessage = "";
    
/*
 * Prepare password validation variables
 * 
 */
function preparePasswordValidation() {
   
// Password verdicts required
requiredVerdict = passwordSettings['minStrength'];
requiredVerdictNum = jQuery.inArray(requiredVerdict, verdictList);

// Password field related messages
nonMatchMessage = "The passwords you entered do not match\n\n" +
                  "Please make sure to enter the same password " +
                  "in the corresponding confirmation field";

insufficientStrengthMessage = "The password(s) are not of sufficient strength\n\n" +
                              "Minimum Requirement: " + requiredVerdict + "\n\n" +
                              "To increase the strength of the password(s) consider using:\n" +
                              " - Non-alphanumeric characters\n" +
                              " - Upper and lower case letters\n" +
                              " - Numbers";
                                  
}


/* Password field events
 * @parentElement {object} parent element for password fields
 * @enableMeterByDefault {bool} enable strength meter by default
 * @returns void
 */
function passwordFieldsEvents(parentElement, enableMeterByDefault) {
   
  // Enable Strength Meter by default (Without checkbox)
  if (enableMeterByDefault) {
    jQuery('.first-password-field').each(function() {
       applyStrengthMeter(jQuery(this));
    });

  } else {
    // Checkbox click
    parentElement.delegate('.change-password', 'change', function() {
        
      var checkBox = jQuery(this);
      var parentTD = checkBox.parents('td[class=password-fields-td]');
      var passwordFields = parentTD.find('input[type=password]');
      
      // Loop through password fields
      passwordFields.each(function() {
                
        var passwordField = jQuery(this);
        
        // Fields enabled
        if (checkBox.prop('checked')) {
            
          // Add password strength meter to first password field
          if (passwordField.hasClass('first-password-field')) {
            applyStrengthMeter(passwordField);
          }  
          
          // Enable field
          passwordField.prop('disabled', false).attr('value', '');
            
       } else {
           
          // Remove strength meter from first password field
          if (passwordField.hasClass('first-password-field')) {
            removeStrengthMeter(passwordField);
          }
          
          // Disable field
          passwordField.prop('disabled', true).attr('value', '*******');
       }
       
      });
      
    });
    
  }
}

/* Apply strength meter (Bootstrap2 PwStrength)
 * @passwordField {object} password field to apply meter to
 */
function applyStrengthMeter(passwordField) {

    // Option array
    var options = [];

   /* Apply settings from the system config file
    * Plugin: pwstrength bootstrap Jquery
    */
    if (typeof passwordSettings !== 'undefined') {
       options = passwordSettings;
    }

    options.ui = {
        bootstrap2: true,
        showVerdictsInsideProgressBar: true,
        viewports: {
            progress: ".pwstrength-viewport-progress"
        }
    };

    options.common = {
        debug: false
    };
    passwordField.pwstrength(options);
}

/* Remove strength meter (Bootstrap2 PwStrength)
 * @passwordField {object} password field to remove meter from
 */
function removeStrengthMeter(passwordField) {
  passwordField.pwstrength('destroy');
}


/* Validate single field strength (Bootstrap2 PwStrength)
 * @field object (type input)
 * @returns bool true|false
 */
function sufficientPasswordStrength(field) {

    // Password verdicts
    var passwordVerdict = field.parents('td:first').find('.password-verdict').text().toLowerCase();
    var passwordVerdictNum = jQuery.inArray(passwordVerdict, verdictList);

    var sufficientStrength = !(passwordVerdictNum < requiredVerdictNum);

    // If the password is of insufficient strength then inform user
    if (!sufficientStrength) {

        alert(insufficientStrengthMessage);
        return false;

    } else {

        return true;

    }
}

/* Validate password fields (Bootstrap2 PwStrength)
 * @mode string field add|edit mode
 * @returns bool true|false
 */
function passwordFieldsValid(mode) {
    
    // Password verdicts values
    var fieldsDontMatch = false;
    var insufficientStrength = false;

   // Loop through password fields
   jQuery('.password-fields-td').each(function() {
     
     // Container cell
     var containerCell = jQuery(this);
     var checkBox = containerCell.find('.change-password');
     
     // Checkbox checked ?
     if (mode === 'add' || checkBox.prop('checked')) {
     
       // Password fields
       var firstField = containerCell.find('.first-password-field');
       var secondField = containerCell.find('.second-password-field'); 
     
       // Password verdicts
       var passwordVerdict = containerCell.find('.password-verdict').text().toLowerCase();
       var passwordVerdictNum = jQuery.inArray(passwordVerdict, verdictList);

       // If both password fields don't match then break         
       if (firstField.val() !== secondField.val()) {
         fieldsDontMatch = true;
         return false;
       // Invalid password then break
       } else if (passwordVerdictNum < requiredVerdictNum) {
         insufficientStrength = true;
         return false;
       }
       
     }
     
    });
    
    // If fields don't match then inform user
    if (fieldsDontMatch) {
       alert(nonMatchMessage);
       return false;
    }
  
    // If the password is of insufficient strength then inform user
    if (insufficientStrength) {
       alert(insufficientStrengthMessage);
       return false;
    }
    
   return true;
}
