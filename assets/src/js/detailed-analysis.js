/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2019 Qpercom Limited.  All rights reserved.
 */

jQuery(document).ready(function() {
 
	if(jQuery('#resdetdv').length) {

	  if (jQuery('#result_details_tables').length ){

		jQuery("#result_details_tables").easyTable({
			sortable:true
		});

	  }
	  
   }

	window.parent.removeIframeWait();

	jQuery("#results-detailed-table").tableExport({
		formats: ["xlsx"],
		position: "top",
        filename: jQuery('#form-name').val(),
        sheetname: "data",
		bootstrap: true,
        trimWhitespace: true
	});	

});


