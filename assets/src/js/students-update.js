/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Add checkbox events for the update table
function addExamineesCheckboxEvents() {
    var checkboxes = jQuery("#update-table").find("input[class=ecb]");
    jQuery(checkboxes).on("click", function(e) {
        var rows = jQuery("#update-table").find("tr[class=editrw2]");
        var checkedCount = 0;
        jQuery.each(rows, function(i, row) {
            if (jQuery(row).find('*:first').find('input:first').prop('checked') === true) {
                checkedCount++;
            }
        });
        
        disableGoButton("update", (checkedCount <= 0));
    });
}

// Basic Url
function basicUrl() {
    var url = 
        "&c=" + jQuery("#course").val() +
        "&y=" + jQuery("#year").val() +
        "&m=" + jQuery("#module").val() +
        "&p=" + jQuery("#pagenr").val() +
        "&s=" + jQuery("#search").val() +
        "&i=" + jQuery("#order-index").val() +
        "&o=" + jQuery("#order-type").val();
    return url;
}

// Validate Form
function ValidateForm() {
    
    var rows = jQuery("#update-table").find("tr[class=editrw2]");
    var generalError = false;
    var repeatedError = false;
    var validationPassed = true;
    var recordIDsValid = true;
    var dateError = false;
    var emailError = false;
    var action = jQuery("#action").val();
    var examineeIDs = [];
    var repeated = [];
    var candidateNumbers = [];
    var candidateRecords = [];

    // Iterate through examinee rows
    jQuery.each(rows, function(i, row) {

       /*
        * Validate examinee ID
        */
       var newID = jQuery(row).find("input[class^=examinee-id]").val().trim();
       var currentID = jQuery(row).find("input.ecb").val().trim();
       if (newID.length == 0) {
          generalError = true;
      } else if (!isRecordIDValid(newID, true)) {
          recordIDsValid = false;
       } else if (examineeIDs.indexOf(newID) > -1) {
          repeated.push(newID);
          repeated = jQuery.unique(repeated);
          repeatedError = true;
       } else {
          if (action == "add" || (action == "update" &&  currentID != newID)) {
             examineeIDs.push(newID);
             examineeIDs = jQuery.unique(examineeIDs);
          }
       }

       /*
        * Validate candidate number, if any
        */
       var candidateNumberEle = jQuery(row).find("input[class^=candidate-number]");
       var candidateNumber = (candidateNumberEle) ? candidateNumberEle.val().trim() : "";
       if (candidateNumber.length > 0) {
          if (!isRecordIDValid(candidateNumber, true)) {
            recordIDsValid = false;
          } else if (candidateNumbers.indexOf(candidateNumber) > -1) {
            repeated.push(candidateNumber);
            repeated = jQuery.unique(repeated); 
            repeatedError = true;
          } else {
            candidateNumbers.push(candidateNumber);  
            candidateNumbers = jQuery.unique(candidateNumbers);  
            candidateRecords.push({
                currentID: currentID,
                newID: newID,
                candidateNumber: candidateNumber
            });
          }
       }

       /* 
        * Basic date validation if fields are not empty 
        */
       var day = jQuery(row).find("input[class^=day]").val();
       var month = jQuery(row).find("input[class^=month]").val();
       var year = jQuery(row).find("input[class^=year]").val();

       /*
        * If at least one of the fields (day, month and year) has been filled then
        * we know there's been an attempt to complete them so we must validate
        */
       if (day.length > 0 || month.length > 0 || year.length > 0) {

         // Validate field values
         var invalidLength = (day.length > 2 || month.length > 2 || year.length != 4);
         var nonNumeric = (isNaN(day) || isNaN(month) || isNaN(year));
         if (invalidLength || nonNumeric) {
            dateError = true;
         } else {
            // Now validate the date itself
            var date = new Date(year, month-1, day);  
            var validDate = (date.getDate() == day && date.getMonth() == month-1 && date.getFullYear() == year);                    
            if (!validDate) {
              dateError = true;  
            }

         }

       }

       /* 
        * Validate gender, nationality and email
        * Only if there's a value entered
        */
       var gender = jQuery(row).find("select[class^=examinee-gender]").val();
       if (gender > 0 && gender.length != 1) {
           generalError = true;
       }

       var nationality = jQuery(row).find("select[class^=examinee-nationality]").val();
       if (nationality > 0 && nationality.length != 2) {
           generalError = true;
       }

       var emailValue = jQuery(row).find("input[class^=examinee-email]").val().trim();
       if (emailValue.length > 0 && !isEmailValid(emailValue)) {
           emailError = true;
       }

    });

    // Validate years and modules
    var moduleRows = jQuery("#update-table").find("tr[class=modulerw]");
    jQuery.each(moduleRows, function(i, moduleRow) {

      // Check Modules
      var moduleList = jQuery(moduleRow).find("ul[class=modules-ul]");
      var checkboxes = moduleList.find("input[type=checkbox]:checked");
      if (checkboxes.length <= 0) {
          generalError = true;
      }

    });

    // Error messages
    if (generalError) {
       alert("Please complete all fields");
       return false;
    } else if (!recordIDsValid) {
        /**
         * Output appropriate error message
         * Pass invalid dummy ID '^' to output error
         */
        isRecordIDValid("^");
        return false;
    } else if (dateError) {
       alert("Invalid date fields detected");
       return false; 
    } else if (emailError) {
       alert("Email Address is not valid, leave blank if Email Address is not known");
       return false;
    } else if (repeatedError) {
       var repeatMessage = "You have repeated the following identifiers: \n";
       jQuery.each(repeated, function(i, value) {
           repeatMessage += "> " + value + "\n";
       });
       alert(repeatMessage);
       return false;

    // Check identifiers
    } else {
        
         jQuery.ajax({
                url: "examinees/json_examinees.php",
                type: "post",
                dataType: "json",
                async: false,
                data: {
                  isfor: "examinees_exist",
                  ids: examineeIDs
             },
             dataType: "json"
           })
          .done(function(idsError) {
             if (idsError !== "null" && idsError.length > 0) {
                var errorMessage = "The following identifiers are already in use within the system:\n";
                jQuery.each(idsError, function(index, each) {
                   var examineeID = each.examinee_id.trim();
                     errorMessage += "> " + examineeID + "\n";
                });
                validationPassed = false;
                alert(errorMessage);
             }
         })
         .fail(function(jqXHR) {

           var message = "Failed to confirm if records exists.\n";
           switch (jqXHR.status) {
                case 404:
                case 500:
                    message += "An internal error occurred";
                    break;
                case 400:
                    message += "Update request was invalid.";
                    break;
                case 403:
                    message += "Sorry, You may not be logged in or your session may have expired";
                    break;
                default:
                    message += "An unspecified error has occurred. Contact your system administrator.";
                    message += "\n(HTTP Code: " + jqXHR.status + ")";
           }

            alert(message);

         });        
       
       // Make sure candidate numbers are unique, if they exist
       if (validationPassed && candidateNumbers.length > 0) {

         jQuery.ajax({
                url: "examinees/json_examinees.php",
                type: "post",
                dataType: "json",
                async: false,
                data: {
                 isfor: "candidate_numbers_taken",
                 numbers: candidateRecords,
                 term: jQuery("#term").val()
             },
             dataType: "json"
           })
          .done(function(data) {

           if (data.length > 0) {
                var errorMessage = "The candidate numbers for the following are " +
                                   "already in use within the system:\n";
               jQuery.each(data, function(index, student) {
                 errorMessage += "> " + student.newID + " (candidate number: " 
                              + student.candidateNumber + ")\n";
               });
               validationPassed = false;
               alert(errorMessage);
           }
          
         })
         .fail(function(jqXHR) {

           var message = "Failed to check candidate number uniqueness.\n";
           switch (jqXHR.status) {
                case 404:
                case 500:
                    message += "An internal error occurred";
                    break;
                case 400:
                    message += "Update request was invalid.";
                    break;
                case 403:
                    message += "Sorry, You may not be logged in or your session may have expired";
                    break;
                default:
                    message += "An unspecified error has occurred. Contact your system administrator.";
                    message += "\n(HTTP Code: " + jqXHR.status + ")";
           }

            alert(message);

         });
       }

  }
  return validationPassed;
}

jQuery(document).ready(function() {
   
    /**
     * Add Examinees Section
     */
    if (jQuery("#update-table").length) {
      
      // Add the checkbox events
      addExamineesCheckboxEvents();

      /*
       * Go button event 
       * (Remove new examinee rows event)
       */ 
      if (jQuery("#go-update").length) {
          
         jQuery("#go-update").on("click", function() {

           var checkboxes = jQuery("#update-table").find("input[class=ecb]");
           var beforeCount = checkboxes.length;
           var removeCount = 0;

           // Remove the rows
           jQuery.each(checkboxes, function(i, checkbox) {
             if (jQuery(checkbox).prop('checked')) {
                removeCount++;
                if (removeCount < beforeCount) {
                  var parent = jQuery(checkbox).parents("tr:first");
                  var next = parent.next("tr");
                      next.remove();
                      parent.remove();
                }
            }
           });

           // Re-number rows
           if (removeCount < beforeCount) {
               jQuery("#examinee-count").text(beforeCount - removeCount);
               var newNumber = 0;
               var rowNumFields = jQuery("#update-table").find("td[class=row-number]");
               jQuery.each(rowNumFields, function(i, numField) {
                  newNumber++; 
                  numField.text(newNumber);
               });
           } else if (removeCount == beforeCount) {
               jQuery("#examinee-count").text(1);
               $(".row-number").text(1);
               var warning = "A minimum of 1 record is required. If you need to cancel click the 'return' button below";
               alert(warning);
           }
           disableGoButton("update", true);
         });
        }

        /* 
         * Examinees course year checkbox events
         */
        jQuery("#update-table").on("click", '.year-check', function() {
            var parentRow = jQuery(this).parents("tr:first");
            var splitID = parentRow.attr('id').split('-');
            var modulesUL = parentRow.find("ul[class=modules-ul]");
            var examineeID = parentRow.prev("tr").find("input.ecb").val();
            var checkedCbs = parentRow.find("input.year-check:checked");
            var checkedYears = jQuery.map(checkedCbs, function(element) {
                return jQuery(element).val();
            });
            
            // Empty the main 'ul' element
            modulesUL.empty();

            // User has selected years
            if (checkedYears.length > 0) {
              // Add loading status
              modulesUL.append(jQuery("<li>", {"class": "mloading"}).text("loading....."));

              jQuery.ajax({
                url: "examinees/json_examinees.php",
                type: "post",
                dataType: "json",
                cache: false,
                data: {
                    isfor: "get_modules_by_years",
                    term: jQuery("#term").val(),
                    years: checkedYears,
                    course: jQuery("#course").val(),
                    examinee: examineeID,
                    action: jQuery("#action").val()
                }
             })
             .done(function(years){
                modulesUL.empty();
                        
                    var yearCount = 0;
                    
                    // Loop though years
                    jQuery.each(years, function(i, year) {
                      var moduleCount = 0;
                      yearCount++;
                      
                      // Create new list element
                      modulesUL.append(jQuery("<li>", {"class": "each-title"}).text(year.description));
                      
                      // Loop through modules linked to the year and render list elements
                      jQuery.each(year.modules, function(ind, module) {
                         moduleCount++;
                         var checkboxID = "module-cb" + splitID[1] + yearCount + moduleCount;
                         var li = jQuery("<li>", {"class": "mod-element"});
                         var nameAttrValue = "examinee[" + examineeID + "][course_data][" + year.id + "][]";
                         var input = jQuery(
                                         "<input>", 
                                         {"type": "checkbox",
                                          "value": module.module_id,
                                          "name": nameAttrValue,
                                          "id": checkboxID,
                                          "checked": module.examinee_has}
                         );

                         var span = jQuery("<span>", {
                             "class": "boldgreen",
                             "text": module.module_id
                            })
                                         
                         var label = jQuery("<label>", {
                             "for": checkboxID,
                             "text": module.module_name + " ["
                            })
                            .append(span)
                            .append(document.createTextNode("]"));

                         modulesUL.append(li.append([input, label]));
                       });
                    });   
                     // Add empty 'li' element to space
                     modulesUL.append(jQuery("<li>", {"class": "li-buffer"}));
             })
             .fail(function(jqXHR) {
                var message = "Failed to get records for ID specified.\n";
                switch (jqXHR.status) {
                    case 404:
                    case 500:
                        message += "An internal error occurred";
                        break;
                    case 400:
                        message += "Update request was invalid.";
                        break;
                    case 403:
                        message += "Sorry, You may not be logged in or your session may have expired";
                        break;
                    default:
                        message += "An unspecified error has occurred. Contact your system administrator.";
                        message += "\n(HTTP Code: " + jqXHR.status + ")";
                }

                alert(message);
             });
             
            } else {
                var message = "Please complete all fields"; 
                var pElement = jQuery("<p>", {"html": message});
                modulesUL.append(pElement);
            }
        });
    }
    
    // Return button
    if (jQuery("#return").length) {
        jQuery("#return").on("click", function() {
            parent.location = "manage.php?page=manage_examinees" + basicUrl();
        });
    }
       
    showMenu();
    
    // Focus first cell (Add mode)
    if (jQuery('.examinee-id').first().val().length == 0) {

        jQuery('.examinee-id').first().focus();
        
    }
   
});