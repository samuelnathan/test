/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2018 Qpercom Limited.  All rights reserved.
 */

/**
 * Map of the type: (Exam ID) => (Whether the exam with that ID has any scoresheet assigned)
 */
var examHasScoresheets = {};

jQuery(document).ready(function() {
    if (jQuery('#dataTypeSel').length) {
        //import data type selection on data_import_step1 **page**
        jQuery('#dataTypeSel').on('change', function() {
            parent.location = 'manage.php?page=importingStep1&dataType=' + jQuery('#dataTypeSel').val();
        });
    }

    if (jQuery('#backstep1').length) {
        jQuery('#backstep1').on('click', function() {
            parent.location = 'manage.php?page=importingStep1&dataType=' + jQuery('#dataType').val();
        });
    }

    if (jQuery('#backstep2').length) {
        jQuery('#backstep2').on('click', function() {
            parent.location = 'manage.php?page=importingStep2';
        });
    }

    /* File Encoding Selection Event */
    if (jQuery('#file_enc').length) {
        jQuery('#file_enc').on('change', function() {
            var encodingSelected = jQuery(this).val();
            var orginalValues = jQuery('#mappingTable').find('span[class=encoded-org]');
            var count = 0;

            jQuery.each(orginalValues, function(i, eobj) {
                var referenceID = 'converted-' + count + '-' + encodingSelected;

                if (jQuery(referenceID).length) {
                    jQuery(eobj).text(jQuery(referenceID).val());
                }

                count++;
            });
        });
    }

    showMenu();

    /* (DW) If there's only one option to select, then select it. This would be
     * much more efficient if I could do it from PHP, but I can't as the headers
     * are already sent :(
     */
    if (jQuery('#dataTypeSel').length) {
        var this_uri = parseQueryString(window.location.search);

        if (this_uri['dataType'] === undefined) {
            var type_select = jQuery('#dataTypeSel');

            if (type_select.find('option').length === 1) {
                type_select.trigger('change');
            }
        }
    }

   /**
    * File format type selection event (jQuery)
    */
   jQuery('.filetype-option').click(function() {

     var random = Math.random();
     var source = jQuery(this).val();

     jQuery.ajax({
          type: "POST",
          url: "import/ajx_data_import_step1.php",
          data: {
             source: source,
             sid: random
          }
      }).done(function(html) {
          jQuery('#field-qualified-row').remove();

          // excel option does not require field qualified option
          if (source !== "excel") {
          var fieldQualifiedRow = jQuery('<tr>', {'id': 'field-qualified-row'});
              jQuery('#' + source + '-row').after(fieldQualifiedRow);
              fieldQualifiedRow.empty().html(html);
          }
      }).fail(function() {
           alert('The request failed, please make selection again');
      });

    });

   /**
    * Generate exams based on department selected (jQuery)
    */
    jQuery('#department-matrix').change(function() {

        // Loading dropdown wait...
        loadingDropdownImport(jQuery('#exam-matrix'));

        jQuery.ajax({
             type: 'POST',
             url: "import/json_department_exams.php",
             cache: false,
             data: {
                department: jQuery('#department-matrix').val()
             },
             dataType: 'json'
         }).done(function(exams) {

            jQuery('#exam-matrix').empty();
            jQuery('#exam-matrix').append(jQuery('<option>', {'value': '', 'text': '--'}));
            jQuery('#startImport').prop('disabled', true);

            // Do we have records to process ?
            if (exams.length > 0) {

              // Loop through returned records
              jQuery.each(exams, function(index, exam) {
                  jQuery('#exam-matrix').append(jQuery('<option>', {'value': exam.id, 'text': exam.name}));
              });

            }

         }).fail(function() {
               jQuery('#exam-matrix')
                   .empty()
                   .append(jQuery('<option>', {'value': '', 'text': '--'}));
               jQuery('#startImport').prop('disabled', true);
               alert("Records could not be retrieved, please selection again");
         });

   });

   /**
    * Matrix exam selection event (jQuery)
    */
    jQuery('#exam-matrix').change(function() {

       var enabled = (jQuery(this).val().length > 0);
       jQuery('#startImport').prop('disabled', !enabled);

    });

    /**
     * Generate exams based on department selected (jQuery)
     */
    jQuery('#department-selfassessment').change(departmentChange(
        "import/json_department_exams_selfassessment.php",
        "#department-selfassessment",
        "#exam-selfassessment"
    ));

    jQuery('#exam-selfassessment').change(function() {
       var examId = jQuery(this).val();
       var enabled = examId.length > 0;

       var showDisplay = {
           display: enabled ? 'table-row' : 'none'
       };

        jQuery("#has-scoresheets-section").css('display', enabled && examHasScoresheets[examId] ?
            'block' : 'none');

       jQuery('#title-selfassessment-section').css(showDisplay);
       jQuery('#maxscore-selfassessment-section').css(showDisplay);
       jQuery('#startImport').prop('disabled', !enabled);
    });

});

function departmentChange(endpoint, departmentSelctor, examSelector) {
    return function() {
        // Loading dropdown wait...
        loadingDropdownImport(jQuery(examSelector));

        jQuery.ajax({
            type: 'POST',
            url: endpoint,
            cache: false,
            data: {
                department: jQuery(departmentSelctor).val()
            },
            dataType: 'json'
        }).done(function(exams) {

            jQuery(examSelector).empty();
            jQuery(examSelector).append(jQuery('<option>', {'value': '', 'text': '--'}));
            jQuery('#startImport').prop('disabled', true);

            // Do we have records to process ?
            if (exams.length > 0) {

                // Loop through returned records
                jQuery.each(exams, function(index, exam) {
                    jQuery(examSelector).append(jQuery('<option>', {'value': exam.id, 'text': exam.name}));
                    examHasScoresheets[exam.id] = exam.hasScoresheets;
                });

            }

        }).fail(function() {
            jQuery(examSelector)
                .empty()
                .append(jQuery('<option>', {'value': '', 'text': '--'}));
            jQuery('#startImport').prop('disabled', true);
            alert("Records could not be retrieved, please selection again");
        });
    }
}

function submitImport() {
    if (jQuery('#data_file').val().length < 1) {
        jQuery('#error_student_list').html("&#xab; Select");
        return false;
    }

    return true;
}

/**
 * Validate Html form
 * @param object form
 * @returns boolean
 */
function ValidateForm(form) {

    // Step 1 of the import tool
    if (jQuery(form).attr('id')=== 'importSource') {

        if (jQuery('#' + jQuery(form).attr('id')).find('input:checked').val() === 'custom') {

            var valueTrimmed = jQuery('#customDelimiter').val().trim();

            if (valueTrimmed.length === 0) {

                jQuery('#customDelimiter').val('');
                alert("Please specify the delimiter used in the source");
                return false;

            }

        }

        return true;

    // Step 3 of the import tool or a drop-down has been triggered in step 3
    } else if (jQuery(form).attr('id') === 'importStep3' || jQuery(form).attr('id').split('_')[0] === "dataField") {

        var validForm = true;
        var error_message = "";
        var fieldsValues = {};
        var dupFieldValues = [];
        var dupCount = 0;

        $fields = jQuery('#mappingTable').find('select');
        jQuery.each($fields, function(index, item) {

            if (typeof fieldsValues[jQuery(item).val()] == 'undefined') {

                fieldsValues[jQuery(item).val()] = index;

            } else if (jQuery(item).val() !== "none") {

                dupFieldValues[dupCount] = 'row_' + index;
                dupCount++;
                dupFieldValues[dupCount] = 'row_' + fieldsValues[jQuery(item).val()];
                dupCount++;

            }

        });

        tableRows = jQuery('#mappingTable').find('tr');
        tableRows.each(function(index, item) {

            if (dupFieldValues.indexOf(jQuery(item).attr('id')) > -1) {

                jQuery(item).attr('class', 'errorBgColor');

            } else {

                jQuery(item).removeClass('errorBgColor');

            }

        });

        if (dupFieldValues.length !== 0) {

            validForm = false;
            error_message = "Please make sure that the database<br/>fields above are only mapped once.";

        }

        // Form at step 3 submitted
        if (jQuery(form).attr('id') === "importStep3" && !jQuery('#matrix-import-table').length) {

            var reqFields = jQuery('#reqFields').val().split('/');
            var reqFieldsTranslated = jQuery('#reqFieldsTranslated').val().split('/');

            if (validForm) {

                error_message = "<span class = 'black'>Please map the following fields:<br/> </span> ";
                jQuery.each(reqFields, function(index, item) {

                    if (typeof fieldsValues[item] == 'undefined') {

                        error_message = error_message + reqFieldsTranslated[index] + "<br/>";
                        validForm = false;

                    }

                });

            }

        }

        if (jQuery("#mappingTable").find('#error_div').length) {

            jQuery("#mappingTable").find('#error_div').remove();

        }

        // The form is not valid, display the appropriate error message
        if (!validForm) {

            error_div = jQuery('<div>', {id: 'error_div'});
            error_div.attr('class', 'errordiv');
            jQuery("#mappingTable").find('#import-buttons-div').before(error_div);
            error_div.html(error_message);

        // We are are step 3 of the import tool and the form is valid
        } else if (jQuery(form).attr('id') === 'importStep3' && validForm) {

          // Data type is 'students'
          if (jQuery('#data-type').val() === 'students') {

           // Check for student name fields 'forename', 'surname' selected
           var nameFieldsFound = false;
           jQuery.each($fields, function(i, dropdown) {

              var selectedValue = jQuery(dropdown).val();

              if (['forename',  'surname'].indexOf(selectedValue) > -1) {

                 nameFieldsFound = true;

              }

           });

           /**
            * If none of these fields are chosen then confirm with user if they would
            * like to continue without these fields
            */
           if (!nameFieldsFound) {

             // Show continue confirmation fields
             jQuery('#continue-div').removeClass('hide');

             // Add checkbox event for confirmation
             jQuery('#continue-without-names').on('click', function() {

               jQuery('#startImport').prop('disabled', !jQuery(this).prop('checked'));

             });

             /**
              * If we need to pause importing then disable import button and return false
              */
             var pauseImport = !jQuery('#continue-without-names').prop('checked');
             if (pauseImport) {
              jQuery('#startImport').prop('disabled', true);
              return false;
             }

           }

          }

          // Remove base fields and show wait indicator while importing
          jQuery('#continue-div').remove();
          jQuery('#import-buttons-div').remove();
          jQuery('#optionstr').remove();
          jQuery('#import-wait-div').removeClass('hide');

        }

        return validForm;
    }
}

function clearError() {
    jQuery('#error_student_list').html("");
}

/**
 * Loading dropdown
 */
function loadingDropdownImport(element) {
    element.empty().append(jQuery('<option>', {'value': '', 'text': 'loading....'}))
    return element;
}
