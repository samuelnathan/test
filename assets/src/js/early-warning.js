/**
 * Encapsulation of the logic and state of the early warning system client
 *
 * @param dialogSelector String Selector of the element that contains the dialog that displays the warnings
 * @param notificationBubbleSelector String Selector of the notification that shows there are warnings
 * @param examIconClass String Name of the class assigned to the icons that show an exam has warnings
 * @constructor
 */
function EarlyWarningClient(dialogSelector, notificationBubbleSelector, examIconClass) {
    var that = this;

    /**
     * ID of the exam whose warnings are being displayed
     */
    this.currentExamWarning = null;

    /**
     * Flag that specifies if there have been found any warnings at all
     * @type {boolean}
     */
    this.anyWarnings = false;

    /**
     * Map that associates an exam ID with the warnings found for its exam
     * @type {{}}
     */
    this.examWarnings = {};

    /**
     *  Shows the current warnings in the dialog
     */
    var showWarningsInDialog = function() {
        var warningList = jQuery('.ui-dialog-content >  #warnings-list');

        warningList.empty();

        that.examWarnings[that.currentExamWarning].forEach(function (warning) {
            jQuery('<li/>', {
                text: warning
            }).appendTo(warningList);
        });
    };

    /**
     * For the exam whose warnings are being currently shown, sends a request to the server to ignore them
     * and hides the warning alert
     */
    var ignoreWarnings = function() {
        var dialog = jQuery(this);
        // Send the request to ignore warnings for the exam that's being shown
        jQuery.ajax('osce/ajx_hide_warning.php', {
            data: { examId: that.currentExamWarning },
            method: 'POST',
            // After the request is done, remove the warning icon and close the dialog
            success: function() {
                dialog.dialog("close");
                var warningButton = Array.prototype.find.call(document.getElementsByClassName(examIconClass), function(element) {
                    return element.dataset.examId === that.currentExamWarning;
                });
                if (warningButton !== null)
                    jQuery(warningButton).hide();
            }
        });
    };

    this.warningsDialog = jQuery(dialogSelector).dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        show: { duration: 500 },
        open: showWarningsInDialog,
        buttons: [
            {
                text: "Don't show again",
                icon: "ui-icon-cancel",
                click: ignoreWarnings
            },
            {
                text: "Close",
                click: function() {
                    jQuery(this).dialog("close");
                }
            }
        ]
    });

    this.checkWarnings = function() {
        var examsToCheck = document.getElementsByClassName(examIconClass);
        Array.prototype.forEach.call(examsToCheck, function (element) {
            var examId = element.dataset.examId;

            // Show the warnings when the warning button is clicked
            element.onclick = function () {
                if (that.examWarnings[examId])
                    showWarningsDialog(examId);
            };

            jQuery.ajax('osce/ajx_verify_exam.php', {
                data: {examId: examId},
                method: 'GET',
                success: function (response) {
                    // If no warnings have been found, finish
                    if (!response.length) return;

                    jQuery(element).css('display', 'block');
                    that.examWarnings[examId] = response;

                    if (that.anyWarnings) return;

                    that.anyWarnings = true;
                    showNotificationWarning();
                }
            })
        });
    };

    /**
     * Opens the warnings dialog
     */
    var showWarningsDialog = function(examId) {
        that.currentExamWarning = examId;
        that.warningsDialog.dialog("open");
    };

    /**
     * Shows the notification that there are warnings for some of the exams
     */
    var showNotificationWarning = function() {
        var notificationBubble = jQuery(notificationBubbleSelector);
        notificationBubble.click(function() { notificationBubble.fadeOut() });

        notificationBubble.fadeIn(400, function() {
           setTimeout(function() { notificationBubble.fadeOut() }, 3000);
        });
    };

}

window.onload = function() {
    var earlyWarningSystem = new EarlyWarningClient('#warnings-dialog', '#warning-notification', 'exam-early-warning');
    earlyWarningSystem.checkWarnings();
};