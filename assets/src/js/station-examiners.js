/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

jQuery(document).ready(function() {

    /**
     * Return button click
     */
    jQuery('.return').click(function() {
       parent.location = "manage.php"
                       + "?page=stations_osce" 
                       + "&session_id=" + jQuery('#session').val()
                       + "&show_session=" + jQuery('#show_session').val();
    });
    
    /**
     * Reset changes action
     */ 
    jQuery('#reset-slots').click(function() {
        location.reload();
    });
    
    /**
     * Cancel edit slots
     */ 
    jQuery('#cancel-slots').click(function() {
        
        // Restore previous examiners and weightings selected
        jQuery('.slot-examiner-div').each(function() {
            
           // Slot/examiner name
           var slotName = jQuery(this).find('.slot-name');
           if (jQuery(this).find('.user-original').length) {
               
               var examinerOriginal = jQuery(this).find('.user-original').val();
               var examinerName = jQuery(this).find('.user-name').val();
               slotName.text(examinerName + ' - ' + examinerOriginal);
               
           } else {
               var anyExaminerLabel = jQuery('#examiner-list-template').find('option').text();
               slotName.html(anyExaminerLabel);
           }
           
           // Slot weighting
           var slotWeighting = jQuery(this).find('.slot-weighting');
           if (jQuery(this).find('.weighting-original').length) {
               
               var weightingName = jQuery(this).find('.weighting-name').val();
               var weightingOriginal = jQuery(this).find('.weighting-original').val();
               slotWeighting.html(weightingName + " (" + weightingOriginal + ")");
               
           } else {
               slotWeighting.html('no weighting set');
           }
           
        });
        
        // Restore previous observer slots
        jQuery('.slot-observer-div').each(function() {

           // Slot/observer
           var slotName = jQuery(this).find('.slot-name');
           if (jQuery(this).find('.user-original').length) {
               
               var observerOriginal = jQuery(this).find('.user-original').val();
               var observerName = jQuery(this).find('.user-name').val();
               slotName.text(observerName + ' - ' + observerOriginal);
              
           // No previous observer found, then remove slot 
           } else {
               
               // Remove slot
               jQuery(this).remove();
                
           }
           
           // Display or hide Observer group title
           toggleObserverTitle();            

        });
        
        // Show reset and edit button
        jQuery('#reset-slots').show();
        jQuery('#edit-slots').show();
            
        // Hide cancel button
        jQuery('#cancel-slots').hide();
        
    });
    
    
    /**
     * Edit slots action, get list of examiner from server
     * via ajax request. Returns list as json
     */ 
    jQuery('#edit-slots').click(function() {
         
      // Save required number of examiners
         jQuery.ajax({
           url: 'osce/ajx_station_examiners.php',
           type: 'post',
         data: {
           isfor: 'get_examiners',
           station: jQuery('#station_id').val(),
           random: Math.random()   
         },
           dataType: 'json'
       })
       .done(function(examiners) {
          
        // Render examiner and weighting lists for each examiner slot
        jQuery('.slot-examiner-div, .slot-observer-div').each(function(index, slotDiv) {
            
            // Slot type (examiner or observer)
            var slotType = (jQuery(slotDiv).hasClass('slot-examiner-div')) ? 'examiner' : 'observer';
            
            // Add new examiner list
            if (slotType == "examiner") {
                var newExaminerList = jQuery('#examiner-list-template')
                                  .clone()
                                  .removeAttr('id');
                          
            // Add new observer list
            } else {
                var newExaminerList = jQuery('#observer-list-template')
                                  .clone()
                                  .removeAttr('id');
            }
            
            jQuery(this).find('.slot-name').html(newExaminerList);
            
            // Add option, each option is an examiner
            jQuery.each(examiners, function(i, examiner) {
              var examinerID = examiner.user_id;
              var forenames = examiner.forename;
              var surname = examiner.surname;
              var examinerValue = forenames + " " + surname + " - " + examinerID;
              var option = jQuery('<option>', {'value': examinerID}).html(examinerValue);
              newExaminerList.append(option);
            });
            
            // Pre-select examiner from the list
            if (jQuery(this).find('.user-selected').length) {     
                var selectedExaminer = jQuery(this).find('.user-selected').val();
                jQuery(this).find('.examiner').val(selectedExaminer);
            }
            
            /**
             * Make examiner dropdown list searchable using the chosen plugin 
             * and add update event to pass the selected value to a hidden
             * variable 'user-selected'. This hidden variable is passed
             * to server side when the form is submitted
             */ 
            jQuery(this).find('.examiner')
                        .chosen()
                        .on('change', examinerSelectEvent);
                        
            // Add weighting list
            var newWeightingList = jQuery('#weighting-list-template')
                                  .clone()
                                  .removeAttr('id');
                          
            jQuery(this).find('.slot-weighting').html(newWeightingList);
            
            // Examiner slot type
            if (slotType === 'examiner') {
                
              // Pre-select weighting from the list
              if (jQuery(this).find('.weighting-selected').length) {
                  var selectedWeighting = jQuery(this).find('.weighting-selected').val();
                 jQuery(this).find('.weighting').val(selectedWeighting);
              }
              
            }
                        
            /**
             * Make weighting dropdown list fancy using the chosen plugin 
             * and add update event to pass the selected value to a hidden
             * variable 'weighting-selected'. This hidden variable is passed
             * to server side when the form is submitted
             */ 
            jQuery(this).find('.weighting')
                        .chosen({ 'disable_search': true })
                        .on('change', weightingSelectEvent);          
            
            // Enable reset and update buttons
            updateButtonPanel(false);
            
            // Hide reset and edit button
            jQuery('#reset-slots').hide();
            jQuery('#edit-slots').hide();
            
            // Show cancel and update button
            jQuery('#cancel-slots').show();
                       
        });
        

       })
       .fail(function(jqXHR) {
          
          var error = "";
          switch (jqXHR.status) {
             case 404:
             case 500:
                error = "An internal server error has occurred";
                break;
             case 501:
                error = "Service unavailable, please try again later";
                break;
             case 400:
                error = "1 or more of the required parameters are missing "
                      + "or are invalid";
                break;
             case 403:
                error = "Sorry, your session may have expired "
                      + "or you do not have access to this feature";
                break;
             default:
                error = "An unspecified error has occurred. "
                      + "Please contact your system administrator";
                error += "\n [HTTP Code: " + jqXHR.status + "]";
          }
          alert(error);
            
       });

    });
    
    
    /**
     * Update slots
     * Changes are submitted to the server
     */ 
    jQuery('#form-slots').submit(function() {
      jQuery('#num-slots-examiners').attr('disabled', false);
      jQuery('#num-slots-observers').attr('disabled', false);
      
      // Some validation, check for duplicate examiners selected
      var selectedExaminers = jQuery(this).find('.user-selected').map(function() {
          var value = jQuery(this).val();
          return (value.length > 0) ? value : null;
      }).get();
            
      // Get unique list of examiners
      var uniqueExaminers = selectedExaminers.filter(function(element, position) {
         return selectedExaminers.indexOf(element) === position;
      });
      
      // If duplicates found then output appropriate message to user
      if (selectedExaminers.length !== uniqueExaminers.length) {
         alert("Duplicate user records selected, please review your options");
         return false;
      }
      
      /*
       * Check for an examiner selected without a weighting
       * and vice versa in all slots
       */ 
      var incomplete = false;
      var invalid = false;
      var weightingValue = '';
      var examinersSelectedCount = 0;
      jQuery('.slot-examiner-div').each(function() {

          var examinerSelected = (jQuery(this).find('.user-selected').val().length > 0);
              weightingValue = jQuery(this).find('.weighting-selected').val();
          var weightingSelected = (weightingValue.length > 0);
          
          if (examinerSelected) {
             examinersSelectedCount++; 
          }
          
          if (examinerSelected && !weightingSelected) {
              incomplete = true;
              return false;  
          } else if ((!examinerSelected && weightingValue != 2)) {
              invalid = true;
              return false; 
          }
        
      });
      
      // Incomplete form, then output appropriate error message to user
      if (incomplete) {
        alert("If you choose a user, you must choose a weighting, please review all slots");
        return false;
        
      // Unassigned slots can only have a Regular Examiner Weighting (2)
      } else if (invalid) { 
        alert("Unassigned slots can only have a regular weighting (2), please review all slots");
        return false;
        
      // Single examiner must have a Regular Examiner Weighting (2)
      } else if (examinersSelectedCount == 1 && weightingValue != 2) {
        alert("Single user assignment must use a regular weighting (2), please review slot");
        return false;
        
      }
      
      // Validate observer slots
      var observersNotSelected = false;
      jQuery('.slot-observer-div').each(function() {

          var observerSelected = (jQuery(this).find('.user-selected').val().length > 0);
          if (!observerSelected) {
              observersNotSelected = true;
              return false;
          }
          
      });
      
      if (observersNotSelected) {
        alert("You must select an observer from the dropdown list(s)");
        return false;
      }
      
      return true;
    });
    

    /**
     * Change event for the multi examiners required feature (jQuery)
     */ 
    jQuery('#num-slots-examiners').change(function() {
   
      var slotsRequired = jQuery(this).val();
      var currentNumSlots = jQuery('.slot-examiner-div').length;
      
      /* 
       * Remove unrequired slots
       */
      if (slotsRequired < currentNumSlots) {
        var slotCount = 0;
        jQuery('.slot-examiner-div').each(function() {
          slotCount++;

          // Remove unrequired slots
          if (slotCount > slotsRequired) {
              jQuery(this).remove();
          }

        });
        
      /* 
       * Add new slots if required
       */
      } else if (slotsRequired > currentNumSlots) {

         var newNumSlots = (slotsRequired - currentNumSlots); 
          
         for (var num=1; num<=newNumSlots; num++) {
            var clonedSlot = jQuery('.slot-examiner-div:first').clone();
                clonedSlot.appendTo('#examiner-slots-container');
                
                // Reset selections
                clonedSlot.find('.user-selected').val('');
                clonedSlot.find('.weighting-selected').val(2);
                clonedSlot.find('.user-name').val('');
                clonedSlot.find('.weighting-original').val(2);
                clonedSlot.find('.weighting-name').val('Regular Weighting');
                clonedSlot.find('.user-original').remove();

                // Dropdowns exist
                if (clonedSlot.find('.chosen-container').length > 0) {
                  
                  // Remove chosen container
                  clonedSlot.find('.chosen-container').remove();
                
                  // Reattach chosen object to the examiners list
                  clonedSlot.find('.examiner')
                            .css('display', '')
                            .chosen()
                            .on('change', examinerSelectEvent);
                
                  // Reattach chosen object to the weightings list
                  clonedSlot.find('.weighting')
                            .css('display', '')
                            .chosen({ 'disable_search': true })
                            .on('change', weightingSelectEvent);                 
              } else {
                  var anyExaminerLabel = jQuery('#examiner-list-template').find('option').text();
                  clonedSlot.find('.slot-name').html(anyExaminerLabel);
                  clonedSlot.find('.slot-weighting').html('Regular Weighting (2)');
              }
         }

      }
     
      // Update button access and renumber slots  
      updateButtonPanel(false);

   });
   
   /* Number of slots of observers */
   jQuery('#num-slots-observers').change(function() {
      var slotsRequired = jQuery(this).val();
      var currentNumSlots = jQuery('.slot-observer-div').length;
      
      /* 
       * Remove unrequired slots
       */
      if (slotsRequired < currentNumSlots) {
          
        var slotCount = 0;
        jQuery('.slot-observer-div').each(function() {
          slotCount++;

          // Remove unrequired slots
          if (slotCount > slotsRequired) {
              jQuery(this).remove();
          }

        });
        
      /* 
       * Add new slots if required
       */
      } else if (slotsRequired > currentNumSlots) {
         
         var newNumSlots = (slotsRequired - currentNumSlots); 
          
         for (var num=1; num<=newNumSlots; num++) {
            var clonedSlot = jQuery('.slot-examiner-div:first').clone();
            
                // Make an observer slot
                clonedSlot.appendTo('#observer-slots-container');
                clonedSlot.removeClass('slot-examiner-div').addClass('slot-observer-div');
                
                // Reset selections
                clonedSlot.find('.user-selected').val('').attr('name', 'observers[]');
                clonedSlot.find('.user-name').val('');
                clonedSlot.find('.weighting-original').remove();
                clonedSlot.find('.weighting-selected').remove();
                clonedSlot.find('.weighting-name').remove();
                clonedSlot.find('.user-original').remove();
                
                // Dropdowns exist
                if (clonedSlot.find('.chosen-container').length > 0) {
                  
                 // Remove chosen container
                 clonedSlot.find('.chosen-container').remove();
                 
                }
                
                jQuery('#edit-slots').trigger('click'); 
                    
                // Remove weighting cell slot-weighting    
                clonedSlot.find('.slot-weighting').remove();
                clonedSlot.find('.slot-name').css('border-right', '0px');
    
         }
        
      }
      
      // Display or hide Observer group title
      toggleObserverTitle();
      
   });
   
   /**
    * Delegate remove slot events to container box (examiners)
    */
   jQuery('#examiner-slots-container').delegate('.remove-slot-img', 'click', function() {
      
      // Get current number of slots
      var currentNumSlots = jQuery('.slot-examiner-div').length;
       
      /**
       * Abort if the number of slots is 1.
       * We need at least 1 examiers slot
       */ 
      if (currentNumSlots === 1) {
        alert("At least one slot is required");
        return false;  
      }
      
      // Remove slot
      jQuery(this).closest('.slot-examiner-div').remove();
      updateButtonPanel(false);

      // Update number of slots dropdown selection
      jQuery('#num-slots-examiners').val(currentNumSlots-1);

   });
   
   /**
    * Delegate remove slot events to container box (observers)
    */
   jQuery('#observer-slots-container').delegate('.remove-slot-img', 'click', function() {
      
      // Get current number of slots
      var currentNumSlots = jQuery('.slot-observer-div').length;
       
      // Remove slot
      jQuery(this).closest('.slot-observer-div').remove();
      updateButtonPanel(false);

      // Update number of slots dropdown selection
      jQuery('#num-slots-observers').val(currentNumSlots-1);

      // Display or hide Observer group title
      toggleObserverTitle();
      
   });
         
   showMenu();
   
});

/**
 * Update button panel
 * 
 * @param boolean disabled
 */
function updateButtonPanel(disabled) {
    
   // Update set of buttons
   jQuery('#reset-slots').attr('disabled', disabled);
   jQuery('#update-slots').attr('disabled', disabled);
      
}

/**
 * Examiner selectbox event
 * 
 */
function examinerSelectEvent() {
    var parentSlot = jQuery(this).closest('.slot-div');
    parentSlot.find('.user-selected').val(jQuery(this).val());
}

/**
 * Weighting selectbox event
 * 
 */
function weightingSelectEvent() {
    var parentSlot = jQuery(this).closest('.slot-examiner-div');
    parentSlot.find('.weighting-selected').val(jQuery(this).val());
}

/**
 * Toggle observer title
 * 
 */
function toggleObserverTitle() {
    
    // Get count of observer slots
    var totalNumSlots = jQuery('.slot-observer-div').length;

    if (totalNumSlots > 0) {
          jQuery('#observer-title').removeClass('hide');
    } else {
          jQuery('#observer-title').addClass('hide');
    }
    
}
