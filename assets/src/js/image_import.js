/* Original Author: Domhnall Walsh
 * For Qpercom Ltd
 * Date: 05/02/2015
 * © 2010 Qpercom Limited.  All rights reserved.
 */

/*
function hasVerticalScrollvar(node) {
    if (node === undefined) {
        if (window.innerHeight) {
            return document.body.offsetHeight > innerHeight;
        }
        else
            return  document.documentElement.scrollHeight >
                    document.documentElement.offsetHeight ||
                    document.body.scrollHeight > document.body.offsetHeight;
    }
    else
        return node.scrollHeight > node.offsetHeight;
}

function scrollDown() {
    var html = document.documentElement;
    var height = Math.max(
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
    );
    
    if (hasVerticalScrollvar(html)) {
        jQuery('html, body').scrollTop( height );
    }
    
    if (document.readyState !== 'complete') {
        window.setTimeout(scrollDown, 50);
    }
}
scrollDown();
*/

function confirmUpload(form) {
    var max_upload = parseInt(jQuery('#MAX_FILE_SIZE').val(), 10);
    var max_upload_image = 204800;

    var mimetype_limits = {
        'image/jpeg': max_upload_image,
        'image/png': max_upload_image,
        'image/gif': max_upload_image,
        'application/zip': max_upload
    };
    
    var mimetypes = [];
    jQuery.each(mimetype_limits, function(i, mt) {
        mimetypes.push(mt);
    });
    
    var input = jQuery('#data_file').get(0);
    if (typeof input != 'undefined' && input.files && (input.files.length === 1)) {
        if (input.files[0].size > max_upload) {
            alert("This file is larger than the maximum permissible upload.");
            return false;
        } else if (input.files[0].size > 1048576) {
            var result = confirm("This file is large, and will take a considerable time to upload and process.\n Are you sure you wish to upload this file?");
            if ((result) && jQuery('#progress').length) {
                if (jQuery('#uploaded_filename').get(0)) {
                    jQuery('#uploaded_filename').get(0).innerHTML = input.files[0].name;
                }
                jQuery('#progress').toggle();
            }
            return result;
        }
    }
}

