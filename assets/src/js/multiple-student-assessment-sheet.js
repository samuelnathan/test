/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
/* (DW) Changing behaviour of warning generation for window.onbeforeunload so that it
 * only issues a warning if data has actually changed or not. If a user hasn't bothered
 * clicking on anything, the "you can use data" warning can scare people.
 */
var givewarning = false;

// ID of timeout used for recurring Auto Save function.
var timeoutIDAutoSave;

// Skip first auto save message
var skipFirstAutoSaveMessage = true;

// Elapsed time for the Auto Save deature. Default: 1000 milliseconds (1 second)
var elapsedTimeAutoSave = 2000;

// self assessment button status
var takeOwnershipLabel = 'Take Ownership';
var cancelOwnershipLabel = 'Cancel Ownership';
var notOwnershipLabel = 'Not Owner';
var ownerLabel = 'Owner';

$(document).ready(function() {

    // Cancel button code
    $('.cancel-exam-button').click(function() {

        var confirmMessage = "Do you wish to leave this assessment?\nAll changes made during this edit session will be discarded!";

        // Suspend the autosave while the dialog is being displayed
        window.clearTimeout(timeoutIDAutoSave);

        // Short circuit trick. If givewarning is false, the confirm never runs.
        if ((givewarning === false) || confirm(confirmMessage)) {
            // User has stated that yes, they want to leave the assessment.
            givewarning = false;

            if ($('#assessment-state').val() === "unsubmitted") {
                parent.location = "assess/discard.php";
            } else {
                parent.location = "assess.php?p=student_selection";
            }
        } else {
            // User is not cancelling, so we need to restart the autosave timer.
            // Default to the standard interval for the first iteration, and the
            // autosave program will correct the timings for second and subsequent
            // iterations. But only if this is a new record, not for updates.
            if ($('#assessment-state').val() !== 'submitted') {
                interval = parseInt($('#autosave-interval').val());
                timeoutIDAutoSave = window.setTimeout(autoSave, interval);
            }
        }

    });

    // Disable directional keys when using radio button groups
    $('input[type="radio"]').keydown(function(e) {

        var arrowKeys = [37, 38, 39, 40];

        if (arrowKeys.indexOf(e.which) !== -1) {
            $(this).blur();
            return false;
        }

    });

    //Code for count down clock
    if ($('#clock').length) {

        var timeLimit = 300000;
        if ($('#time-limit').length && $('#time-limit').val().match("^([0-9]\\d):?([0-5]\\d):?([0-5]\\d)$")) {

            // if alarm 1 or 2 is the start time then flash clock
            if ($('#time-limit').val() == $('#alarm1').val()) {
                $('#clock').addClass('clock-warn1');
            }

            // When alarm 2 starts
            if ($('#time-limit').val() == $('#alarm2').val()) {
                $('#clock').addClass('clock-warn2');
            }

            var timeLimitSplit = $('#time-limit').val().split(':');
            timeLimit = (timeLimitSplit[0] * 60 * 60 * 1000) + (timeLimitSplit[1] * 60 * 1000) + (timeLimitSplit[2] * 1000);
        }

        /* Future Time */
        var targetTime = new Date(new Date().getTime() + timeLimit);

        setInterval(function() {
            var timeNow = new Date();
            var timeDiff = Math.floor((targetTime.getTime() - timeNow.getTime()) / 1000);

            var secs = fixIntegerValue(timeDiff % 60);
            timeDiff = Math.floor(timeDiff / 60);

            var mins = fixIntegerValue(timeDiff % 60);
            timeDiff = Math.floor(timeDiff / 60);

            var hrs = fixIntegerValue(timeDiff % 24);

            var timeText = hrs + ':' + mins + ':' + secs;
            $('#clock').text(timeText);

            // When alarm 1 starts
            if (timeText == $('#alarm1').val()) {
                $('#clock').addClass('clock-warn1');
            }

            // When alarm 2 starts
            if (timeText == $('#alarm2').val()) {
                $('#clock').addClass('clock-warn2');
            }

            // When alarm 1 stops
            if (timeText == $('#alarm1-stop').val()) {
                $('#clock').removeClass('clock-warn1');
            }

            // When alarm 2 stops
            if (timeText == $('#alarm2-stop').val()) {
                $('#clock').removeClass('clock-warn2');
            }

            // Turn off flashing when count down reached
            if (timeText === '00:00:00') {
                $('#clock').removeClass('clock-warn1 clock-warn2').addClass('clock-off');
            }

        }, 200);
    }



    // jQuery code for form
    if ($('.exam-assessment-table').length) {

        //Display form
        if (navigator.appName === "Microsoft Internet Explorer") {

            $('.exam-assessment-table').css('display', 'block');

        } else {

            $('.exam-assessment-table').css('display', 'table');

        }

        // Comment fields code
        $('.comment, .comment-right').each(function() {

            var h = $(this).height();
            var ta = $(this).find('textarea');

            if (h < 50) {
                h = 60;
            }

            ta.css('height', h);


            ta.on({
                focus: function() {

                    $(this).addClass('with-text');

                },
                blur: function() {

                    /*
                     * If checkbox (borderline, fail, flag) ticked, then highlight
                     * comments box onblur, if comments field is empty
                     */
                    if ($(this).val().length === 0) {

                        var commentsBoxID = $(this).attr('id');
                        var studentID = commentsBoxID.split("-")[0];
                        var sectionNum = commentsBoxID.split("-")[2];
                        var sectionSelector = "tr[id^='" + studentID + "-section-" + sectionNum +"']";
                        var checkTypes = '.borderline, .fail, .flag';
                        var anyCB = ($(this).hasClass('crequired') && $(sectionSelector).find('input:checked'));
                        var specificCB = ($(this).attr('class').indexOf('crequired') >= 0 && $(sectionSelector).find('input:checked').is(checkTypes));

                        if (anyCB || specificCB) {

                            $(this).addClass('tahighlight');

                        }

                        $(this).removeClass('with-text');

                    }

                    /*
                     * Make sure that the onbeforeunload method fires a warning message
                     * if someone updates a comment/feedback box
                     */
                    givewarning = true;

                },
                keyup: function() {

                    if ($(this).hasClass('tahighlight')) {

                        $(this).removeClass('tahighlight');

                    }

                }
            });
        });


        //Prevent text selection/copy of item options sections [IE only]
        if (navigator.appName === "Microsoft Internet Explorer") {
            $(".options, .items, .title-section, .title-sectionv2, .descriptors, #top-table").on({
                selectstart: function() {
                    return false;
                },
                mousedown: function() {
                    return false;
                }
            });
        }

        //Sliders code
        var sliders = $('.exam-assessment-table').find("div.slider");

        if (sliders.length > 0) {

            /* Loop through sliders div's and create & configure objects */
            sliders.each(function() {
                var thisSlider = $(this);
                var knob = thisSlider.find('.ui-slider-handle');
                var studentID = knob.attr('id').split('-')[0];
                var ansID = knob.attr('id').split('-')[2];
                var ansElement = $('#'+ studentID +'-ansoption-' + ansID);
                var initialValue = (ansElement.val() * 1);
                var end = ($('#' + studentID + '-end-value' + ansID).val() * 1);
                var start = ($('#' + studentID + '-start-value' + ansID).val() * 1);

                var max = Math.max(start, end);
                var min = Math.min(start, end);

                // If the start value is greater than the end value then flip the slider from right to left
                var sliderRTL = false;
                if (start > end) {
                    sliderRTL = true;
                }

                thisSlider.slider({
                    animate: "fast",
                    min: min,
                    max: max,
                    orientation: "horizontal",
                    isRTL: sliderRTL, // RTL
                    value: initialValue,
                    slide: function(e, ui) {
                        if ($('#view-marks').val() == 1) {
                            knob.text((ui.value * 1));
                        } else {
                            var percentCalculated = getPercent(ui.value, max);
                            knob.text(percentCalculated);
                        }
                    },
                    stop: function(e, ui) {
                        ansElement.val((ui.value * 1));
                        calculateTotal(thisSlider.attr('id'));
                    }
                });

                // Attempt to forcibly set the initial value in the slider's
                // "knob"...
                if ($('#view-marks').val() == 1) {
                    knob.text(initialValue);
                } else {
                    var percentCalculated = getPercent(initialValue, max);
                    knob.text(percentCalculated);
                }
            });
        }

        /**
         * Checkbox row highlighting
         */
        function cbRowHighlight(element) {

            var cbParentTr = element.parent('td').parent('tr');

            if (element.is(':checked')) {

                cbParentTr.addClass('checked');

                if (cbParentTr.hasClass('lgrey')) {

                    cbParentTr.addClass('checked-darker');

                }

            } else {

                cbParentTr.removeClass('checked checked-darker');

            }

        }

        // Event for checkbox item highlighting
        jQuery('.exam-assessment-table').on('click', 'input.checkbox-control', function() {

            cbRowHighlight(jQuery(this));

        });

    }

    /* If the page starts out as absent, then automatically uncheck the "absent" button
     * if the user starts inputting results. Need to do this in two stages:
     * 1. For all inputs that are related to inputting data (i.e. not the absent, station
     *    notes, or submit or cancel, and
     * 2. The sliders, as they're not technically inputs.
     */
    function makePresent() {
        absent_cb = $('#absent');
        if (absent_cb.prop('checked')) {
            var dialogMessage = "Since you are entering results for this person, they "
                + "are no longer being marked 'absent'.\n\nIf you wish to mark the person "
                + "as absent, please check the 'Absent' checkbox before you click 'Update'.";
            alert(dialogMessage);
            absent_cb.trigger('click');
        }
    }

    /* Use jQuery selectors to get a list of all of the relevant inputs on the
     * page - both conventional inputs like radio buttons and the likes and
     * jQuery sliders.
     */
    inputs = $('input').not('#absent,#submit-exam,#submit-next,.cancel-exam');

    sliders = $('div').filter(function() {return this.id.match(/^slider-/); });

    // Attach makePresent() to the appropriate events for each control type.
    inputs.on("click", makePresent);
    sliders.on("slidechange", makePresent);

    // Submit button code
    $('#submit-exam').click(function() {

        if (validateExam() == true) {

            var sbButton = $(this)
            sbButton.prop('disabled', true);

            var promises = [];
            $('form').each(function(index, element){
                // Prepare auto save data
                var savaData = $(element).serializeArray();
                savaData.push({name: 'submit_action', value: 'submit'});

                // Actual post to server
                var promise = submitFormData("assess/submit_results.php", savaData);
                promises.push(promise);
            });

            Promise.all(promises)
                .then(function (response){
                    window.location = response[0].url
                })
                .catch(function () {
                    alert("something went wrong please try again");
                    $('#submit-exam').prop('disabled', false);
                })
        }

        jQuery(this).data('jstclickd', false);

    });

    // Submit next button code
    $('#submit-next').click(function() {
        $('#submit-next').data('jstclickd', true);
    });

    function submitFormData(url ,data) {
        return new Promise(function(resolve, reject) {
            return $.post(url, data, function(){}, 'json')
                .done(resolve)
                .fail(reject);
        });
    }

    /* Auto Save Feature Introduced Omis Version 1.9
     */
    function autoSave() {

        // If form has not been updated then skip the Auto Save AJAX POST to server
        // but keep the auto save feature running
        if ($("#form-updated").val() === "0") {
            setAutoSaveTimeout(elapsedTimeAutoSave);

            // Only 'POST' results when form has been updated
        } else {

            // Set auto save status
            $('#auto-save-status').css({'color': '#000000'}).text("saving form...");

            var promises = [];
            $('form').each(function(index, element){
                // Prepare auto save data
                var autoSaveData = $(element).serializeArray();
                autoSaveData.push({name: 'submit_action', value: 'autosave'});

                // Actual post to server
                promises.push(submitFormData("assess/submit_results.php", autoSaveData));

            });

            // Capture the time at which the AJAX POST was submitted.
            var postTimeAutoSave = new Date().getTime();

            Promise.all(promises)
                .then(function() {

                    // Calc auto save time for status message
                    var timeNow = new Date();
                    var theHours = timeNow.getHours();
                    var theMinutes = timeNow.getMinutes();
                    var theSeconds = timeNow.getSeconds();
                    var amPM = "am";

                    if (theHours > 12) {
                        theHours = theHours - 12;
                        amPM = "pm";
                    } else if (theHours < 10) {
                        theHours = "0" + theHours;
                    }

                    if (theMinutes < 10) {
                        theMinutes = "0" + theMinutes;
                    }

                    if (theSeconds < 10) {
                        theSeconds = "0" + theSeconds;
                    }

                    // Set auto save time for status message
                    if (!skipFirstAutoSaveMessage) {
                        $('#auto-save-status')
                            .css({'display': 'block'})
                            .html("saved<br/>" + theHours + ":" + theMinutes + ":" + theSeconds + " " + amPM);
                    } else {
                        skipFirstAutoSaveMessage = false;
                    }

                    // Reset form updated hidden variable to '0'
                    $("#form-updated").val("0");

                })
                .catch(function(response) {

                    // Not a forbidden status, only occurs when result is already complete (delayed autosave)
                    if (response.status != 403) {
                        $('#auto-save-status').css({'display': 'block', 'color': '#FF0000', 'font-weight': 'bold'}).text("save failed");
                    }


                })
                .finally(function() {
                    /* (DW) The autosave function was repeatedly attempting to save even
                     * though it was possible that the previous attempt to save hadn't
                     * actually finished yet, which has the potential to cause problems.
                     * Instead, we re-trigger the autosave after a computed interval each
                     * time it completes (successfully or not) so that it keeps trying
                     * but doesn't stumble over previous instances of itself in the
                     * process, tying up one or more of the relatively scarce presistent
                     * HTTP connections (per server) available to the browser.
                     *
                     * This way,each client should only be trying to save one record at
                     * a time, and by timing each submission and setting the interval
                     * for the difference between that and the desired interval, we
                     * should get a roughly accurate cycle time. Sub-second accuracy at
                     * any rate, which is more than good enough for this sort of thing.
                     * The fact that it reduces server load when it's most beneficial
                     * to do so is no harm either :)
                     */

                    // Figure out how long the just-completed AJAX POST took, and from
                    // there how long we should wait before autosaving again.
                    elapsedTimeAutoSave = new Date().getTime() - postTimeAutoSave;
                    setAutoSaveTimeout(elapsedTimeAutoSave);
                })
        }
    }


    /**
     * Set Auto Save Timeout
     *
     * @param number   newElapsedTime    New elapsed time between current time and last POST time in milliseconds
     */
    function setAutoSaveTimeout(newElapsedTime) {

        // Figure out how long the just-completed AJAX POST took, and from
        // there how long we should wait before autosaving again.
        interval = parseInt($('#autosave-interval').val());

        // Make sure we wait at least a second no matter what.
        delayTime = Math.max(interval - newElapsedTime, 1000);

        // Light the fuse on the next pass.
        timeoutIDAutoSave = window.setTimeout(autoSave, delayTime);
    }

    /* Only enable auto-saving if the form hasn't been previously submitted -
     * that is, if it's being entered for the first time by an examiner.
     */
    if ($('#assessment-state').val() === 'unsubmitted') {
        autoSave();
    }

    //prepare station notes event
    if ($('#station-notes-outer').length) {
        var autoOpen = ($('#notes-pop').length && $('#notes-pop').val() == 1);

        var window_mask = $('#station-notes-outer').dialog({
            resizable: false,
            title: "Notes",
            modal: false,
            height: 368,
            width: 480,
            autoOpen: autoOpen,
            buttons: {'continue': function() {
                $(this).dialog('close');
            }}
        });

        if ($('#show-notes').length) {
            $('#show-notes').click(function() {
                $('#station-notes-outer').dialog('open');
            });
        }
    }

    /**
     * Event for click area (table cell: TD) that surrounds
     * each of the radio/checkbox buttons
     */
    $('.standard-check-td, .scale-check-td').click(function() {

        var radioObj = $(this).find('input[type=radio]');
        radioObj.prop('checked', true);
        calculateTotal(radioObj.attr('id'));

    });

    $('.checkbox-td').click(function(event) {

        if (!$(event.target).is('input')) {

            var cbObj = $(this).find('input[type=checkbox]');
            cbObj.prop('checked', !cbObj.is(':checked'));
            cbRowHighlight(cbObj);

        }

    });

    /**
     * Fix for assessment form cancel button (with back arrow) on safari. Button does not display correctly
     * unless we set the background color to grey (weird)
     */
    if (navigator.userAgent.indexOf("Safari") != -1) {

        jQuery(".cancel-top").css("background", "#ccc");

    }


    $(window).on('beforeunload', confirmExit);

    $(".bt-ownership-self-assessment").on('click',
        function(e){
        console.log("bt-ownership-self-assessment");
            var currentLabelValue = $(this).val();
            var selfAssessmentItemElem = $(".self-assessment-item");
            var selfAssessmentID = $(".self-assessment-content:first").attr('data-self-assessment');
            var self = $(this);

            if (takeOwnershipLabel === currentLabelValue) {
                takeOwnership(1).then(
                    function(){
                        selfAssessmentItemElem.removeClass('hidden').addClass('display');
                        self.val(cancelOwnershipLabel);
                    },
                    function(error){
                        alert(error.responseText);
                    }
                );
            }
            else if (cancelOwnershipLabel === currentLabelValue && confirmCancellation() == true) {
                takeOwnership(0).then(
                    function() {
                        selfAssessmentItemElem.removeClass('display').addClass('hidden');
                        selfAssessmentItemElem.removeClass('done');
                        selfAssessmentItemElem.find("input[type=text], textarea").val("");
                        selfAssessmentItemElem.find('input[type=checkbox], input[type=radio]').prop('checked', false);
                        self.val(takeOwnershipLabel);
                    },
                    function(error) {
                        alert(error.responseText);
                    }
                )
            }

            function confirmCancellation() {
                return confirm('if you cancel ownership the markings for self assessment ' +
                    'items will be lost, do you want to proceed?');
            }

            function takeOwnership(take) {
                var takeOwnershipData = {
                    take: take,
                    selfAssessmentID: selfAssessmentID,
                    action: 'add_remove'
                }
                return $.post("assess/ajx_self_assessment_ownership.php", takeOwnershipData);
            }
        }
    );


    function startCheckingForOwnershipTaken() {
        var ownershipTimeout = setTimeout(
            function() {
                checkOwnershipTaken().then(
                    function() {
                        clearTimeout(ownershipTimeout);
                        startCheckingForOwnershipTaken();
                    }
                )
            },
            5000
        );

        function checkOwnershipTaken() {
            if ($(".bt-ownership-self-assessment").val() === cancelOwnershipLabel) {
                return Promise.resolve();
            }


            var selfAssessmentID = $(".self-assessment-content:first").attr('data-self-assessment');
            var takeOwnershipData = {
                take: '',
                selfAssessmentID: selfAssessmentID,
                action: 'check'
            }

            return $.post("assess/ajx_self_assessment_ownership.php", takeOwnershipData)
                .then(
                    function(data){
                        if (data.isTaken == true){
                            if ($(".bt-ownership-self-assessment").val() == takeOwnershipLabel){
                                $(".bt-ownership-self-assessment").val(notOwnershipLabel);
                            }
                        }
                        else {
                            $(".bt-ownership-self-assessment").val(takeOwnershipLabel);
                        }
                    }
                );
        }
    }

    startCheckingForOwnershipTaken();

});


// Validate Assessment Form
function validateExam() {

    // If absent
    if ($('#absent').is(':checked') === true) {

        // If student is marked absent...
        var confirmmessage = "Do you want to proceed with marking this person as absent?";
        if (confirm(confirmmessage) === false) {

            $('#absent').prop('checked', false);
            $('#absent').parent('div').removeClass('bg-red');
            return false;

        }

        // If not absent
    } else {

        var error = false;

        // First up, scoring options

        $('.exam-assessment-table').find("tr.sections").each(function() {

            var row = $(this);
            var tds = row.find("td[class^=options]");

            if (tds.length == 0) {

                return true;

            }

            if (row.hasClass('self-assessment-item') && row.hasClass('hidden')){
                return true;
            }

            var firstelement = tds.first().find("input:first");
            var propertyType = firstelement.attr("type");

            // Radio Button Type
            if (propertyType === 'radio') {

                var elements = row.find("input[class*=radoption]:checked");
                if (elements.length === 0) {

                    error = true;

                }

                // Text Box Type
            } else if (propertyType === 'text') {

                if (firstelement.val().length === 0 || isNaN(firstelement.val())) {

                    error = true;

                }

                // Slider Type and others
            } else if (!(row.hasClass('submitted-score') || row.hasClass('done'))) {

                error = true;

            }

            // Abort, we have an error
            if (error) {
                // var studentID = row.closest("form").attr("id")[0];
                // $("#" + studentID + "-nav-bar-item").addClass('invalid');
                return false;

            }

        });

        $("table.exam-assessment-table").each(function(index, element){
            var trs = $(element).find("tr.sections");
            var id = $(element).closest('form').attr("id").split("-")[0];
            var tas = $(element).find("textarea.tahighlight");

            if (trs.not(".done").length > 0 && trs.not(".checked").length > 0) {
                $("#" + id + "-nav-bar-item").removeClass('valid');
                $("#" + id + "-nav-bar-item").addClass('invalid');
            }

            if (trs.not(".done").length == 0 || trs.not(".checked").length == 0){
                $("#" + id + "-nav-bar-item").removeClass('invalid');
                $("#" + id + "-nav-bar-item").addClass('valid');
            }

            if (tas.length > 0){
                $("#" + id + "-nav-bar-item").removeClass('valid');
                $("#" + id + "-nav-bar-item").addClass('invalid');
            }

        })

        // Item scoring incomplete
        if (error) {
            alert("\nYou must complete all scoring items before continuing\n(items not yet highlighted)\n\n\n");
            return false;

        }

        // Validate feedback fields
        var studentsID = [];
        $('.score-sheet-form').each(function(){
            var id = $(this).attr('id').split("-")[1];
            studentsID.push(id);
        });

        studentsID.forEach(function (studentID){

            var sectionCount = $('#section-count').val();
            for (var j=1; j<=sectionCount; j++) {

                // Feedback fields incomplete
                var element = $('#' + studentID + '-comment-' + j)
                if (element.length && element.attr('class').indexOf('crequired') >= 0 && element.val().length === 0) {

                    // Any standalone (without items) sections
                    if (element.hasClass('crequired')) {

                        element.addClass('tahighlight');
                        error = true;

                        // All other sections (with items) with feedback field highlighted triggered by items
                    } else if (element.hasClass('tahighlight')) {

                        error = true;

                    }

                }

            }

        });

        if (error) {

            alert("\nPlease complete compulsory feedback fields highlighted in red\n\n\n");
            return false;

        }

    }

    // If submit next is clicked
    if ($('#submit-next').length && $('submit-next').data('jstclickd')) {

        if ($('#subselstud').length) {

            if ($('#subselstud').val().length === 0) {

                alert("Please select ONE person from the list.");
                return false;

            }

        }

    }

    // Offline status
    if (Offline.state == "down") {

        alert("Internet/Network connection is down, please try again.\n\n" +
            "If unsuccessful please move to paper backup forms and if possible inform support team.");
        return false;

    }

    /* Kill keep alive request if running */
    window.clearTimeout(timeoutIDAutoSave);
    //
    // if ($('.self-assessment-item').length > 0){
    //     if ($("#bt-ownership-self-assessment").val() === takeOwnershipLabel){
    //         alert('This is a self assessment and the ownership has not been taken,' +
    //             ' in order to submit the scoresheet please take ownership first.');
    //         return false;
    //     }
    // }

    //No warning and return true
    givewarning = false;
    return true;

}

// Fix integer value for count down
function fixIntegerValue(integer) {
    if (integer < 0)
        integer = 0;
    if (integer < 10)
        return "0" + integer;
    return "" + integer;
}

//confirm exit code
function confirmExit() {
    // Code adapted from http://stackoverflow.com/a/4047879
    dialogMessage = "You are attempting to leave this page.\n"
        + "If you have made any changes to fields without clicking the Save/Submit button, your changes will be lost.\n"
        + "Are you sure you want to exit this page?\n\n[IN FUTURE PLEASE USE THE CANCEL BUTTON]";

    if (givewarning === true)
    {
        if (window.event)
        {
            e = window.event;
            //e.cancelBubble is supported by IE - this will kill the bubbling process.
            e.cancelBubble = true;
            e.returnValue = dialogMessage;
            //e.stopPropagation works in Firefox.
            if (e.stopPropagation)
            {
                e.stopPropagation();
                e.preventDefault();
            }
        }

        //return works for Chrome and Safari
        return dialogMessage;
    }
}

//Calculate total accumulated on assessment form
function calculateTotal(triggerID) {

    /* If someone has clicked on an item that causes a recalculation, then we can
     * assume that the form is "dirty" and that we need to issue some sort of message
     * onbeforeunload.
     */
    givewarning = true;

    var trigger = $("#" + triggerID);
    var parentTr = trigger.parents('tr');
    var trID = parentTr.attr('id');
    var trIDArray = trID.split('-');
    var studentID = trIDArray[0];
    var sectionIDNum = trIDArray[2];
    var isScoring = false;
    var overallTotal = 0;
    var sectionTotal = 0;
    var rows = $(".exam-assessment-table").find("tr[id^="+ studentID +"-section-" + sectionIDNum + "-qrow]");

    // Turn on marked indicator for item
    if ($('#assessment-mode').val() == "assessed") {

        parentTr.addClass('updated-score');

    } else {

        parentTr.addClass('done');

        /**
         * Light grey rows hightlighted become 'darker'
         * tea green background colour
         */
        if (parentTr.hasClass('lgrey')) {

            parentTr.addClass('done-darker');

        }

    }

    // Form has been updated. Update hidden variable 'form-updated' set to '1'.
    // The autosave feature will only run if this value is set to '1'.
    $("#form-updated").val(1);

    // Feedback Text Area Object
    var feedbackField = $('#' + studentID + '-comment-' + sectionIDNum);

    // Hightlight fields
    var highlightFields = [];

    // Feedback required default false
    var feedbackRequired = false;

    // A list of all the feedback field types that might need to be checked.
    var allFieldClasses = ['crequired', 'crequired-fail', 'crequired-bl', 'crequired-fb', 'crequired-flag'];

    /**
     * Comments are required in any case OR Fail option ticked
     * OR Borderline option ticked OR Flag Option
     */
    var failOption = rows.find('.radoption:checked').hasClass('fail');
    var borderlineOption = rows.find('.radoption:checked').hasClass('borderline');
    var flagOption = rows.find('.radoption:checked').hasClass('flag');

    /* Decide if feedback is required based on which GRS entry was selected and
     * which of those entries require feedback to be entered on this station.
     * ('Borderline' = 'Borderline', 'Borderline Pass', 'Borderline Fail'...)
     */
    if (feedbackField.hasClass('crequired')) {
        // Feedback is required for all GRS items.
        feedbackRequired = true;
        highlightFields = allFieldClasses;
    } else if (feedbackField.hasClass('crequired-fb')) {
        // Feedback is required for GRS Fail or GRS Borderline
        feedbackRequired = failOption || borderlineOption;
        highlightFields = ['crequired-fail', 'crequired-bl', 'crequired-fb'];
    } else if (feedbackField.hasClass('crequired-fail')) {
        // Feedback is required for GRS Fail only
        feedbackRequired = failOption;
        highlightFields = ['crequired-fail', 'crequired-fb'];
    } else if (feedbackField.hasClass('crequired-bl')) {
        // Feedback is required for GRS Borderline options only
        feedbackRequired = borderlineOption;
        highlightFields = ['crequired-bl', 'crequired-fb'];
    } else if (feedbackField.hasClass('crequired-flag')) {
        // Feedback is required for Flag Option
        feedbackRequired = flagOption;
        highlightFields = ['crequired-flag', 'crequired-fb'];
    }

    /* Iterate through each feedback Field to see which ones need highlighting
     * applied to them.
     */
    $("textarea[id="+ studentID +"-comment-" + sectionIDNum + "]").each(
        function(taIndex, feedbackField) {
            var feedbackFieldID = feedbackField.id;

            // No hightlighting by default
            var doHighlight = false;

            // Feedback required? then highlight comments fields if empty
            if (feedbackRequired) {
                /* Need to do feedback. If the comment field has any of the
                 * required classes, then mark it for highlighting. Could do
                 * this more neatly with Array.reduce()...
                 */
                $.each(highlightFields, function(hfIndex, className) {
                    /* If feedbackField has any of the classes in highlightFields
                     * then doHighlight will be true.
                     */
                    doHighlight |= $('#' + feedbackFieldID).hasClass(className);
                });
            }


            // If it isn't highlighted and it should be, highlight it.
            if (doHighlight && !($('#' + feedbackFieldID).hasClass('tahighlight'))) {

                var feedbackText = $('#' + feedbackFieldID).val();
                if (feedbackText.length == 0) {

                    $('#' + feedbackFieldID).addClass('tahighlight');

                }

            }

            // If it is highlighted and it shouldn't be, remove the highlight.
            if (!doHighlight && ($('#' + feedbackFieldID).hasClass('tahighlight'))) {

                $('#' + feedbackFieldID).removeClass('tahighlight');

            }

        }
    );

    rows.each(function() {
        var row = $(this);
        var tds = row.find("td[class^=options]");

        /* First TD Element */
        var firstTD = tds.first();

        /* Grab Slider Score */
        if (firstTD.find("div[class^=slider]").length === 1) {

            /* Only if slider has been marked */
            if(firstTD.parents('tr').hasClass('done') || firstTD.parents('tr').hasClass('updated-score') ||
                firstTD.parents('tr').hasClass('submitted-score')) {
                isScoring = true;
                var sliderele = firstTD.find("input[id^="+ studentID +"-ansoption-]");
                sectionTotal += (sliderele.val() * 1);
            }
            /* Everything else */
        } else {
            var firstElement = firstTD.find('input:first');
            var propertyType = firstElement.attr('type');

            /* Grab Radio Score */
            if (propertyType === 'radio') {
                var elements = row.find("input[class*=radoption]:checked");

                if (elements.length > 0) {
                    var optionID = elements.first().val();

                    var actualValue = $("#" + studentID + "-ansoptval-" + optionID).val();

                    if (isFinite(actualValue)) {
                        sectionTotal += (actualValue * 1);
                        isScoring = true;
                    }
                }
            } else if (propertyType === 'text') {

                /* Grab Textbox Score */
                isScoring = true;

                if (firstElement.val().length > 0) {

                    var textValue = (firstElement.val() * 1);
                    var trID = firstElement.parents('tr').attr('id');
                    var trIDArray = trID.split('-');
                    var itemIDNum = trIDArray[4];
                    var textValueLimits = $("#" + studentID + "-lowhigh-" + itemIDNum).val();
                    var limitsArray = textValueLimits.split(',');
                    var lowestLimit = limitsArray[0];
                    var highestLimit = limitsArray[1];

                    // Format decimal values e.g. '.5' => '0.5', '8.' => '8' etc
                    firstElement.val(parseFloat(textValue));

                    // Now validate the text field
                    if (!jQuery.isNumeric(textValue) || (textValue < lowestLimit || textValue > highestLimit)) {

                        row.removeClass('done done-darker updated-score submitted-score');
                        alert("Number you entered must be in range (" + lowestLimit + " to " + highestLimit + ")");
                        firstElement.val("");

                        /*
                         * Re-focus on the textbox
                         * 'focus()' doesn't work right away in Chrome so we need to set a timer
                         */
                        setTimeout(function() {
                            firstElement.focus();
                        }, 0);

                    } else {

                        sectionTotal += textValue;

                    }

                } else {

                    row.removeClass('done done-darker updated-score submitted-score');

                }
            }
        }
    });

    if (isScoring === true) {

        if ($("#" + studentID + "-sectiontotal-" + sectionIDNum).length) {
            $("#" + studentID + "-sectiontotal-" + sectionIDNum).val(sectionTotal);
        }

        var sectionCount = $('#section-count').val();
        for (var j = 1; j <= sectionCount; j++) {
            if ($("#" + studentID + "-sectiontotal-" + j).length) {
                overallTotal += ($("#" + studentID + "-sectiontotal-" + j).val() * 1);
            }
        }

        if ($('#'+ studentID + '-scoreval').length) {
            $('#'+ studentID + '-scoreval').text(overallTotal);
        }
        if ($('#'+ studentID + '-totalresult').length) {
            $('#'+ studentID + '-totalresult').val(overallTotal);
        }
    }
}