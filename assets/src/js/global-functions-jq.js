/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2017 Qpercom Limited.  All rights reserved
 * JQuery Overall Functions
 */

/*
 *  Add record event
 */
function addRecordEvent() {

  if(jQuery('#add').length) {
     jQuery('#add').click(function() {
        addRecordAction();
     });
  }

}

/*
 *  'Save' record event
 */
function saveRecordEvent() {
   if(jQuery('#add-save').length) {
       
      jQuery('#add-save').click(function() {
          saveRecordAction();
      });
  
  }
}

/*
 *  Add 'Edit' records events
 */
function editRecordEvent() {
   // Assign the 'list-form' the task of delegating the events for the edit buttons
   if (jQuery('#list-form').length) {
       jQuery('#list-form').on('click', '.edit-record', function() {
           editRecordAction(jQuery(this));
       });
   }
   
   // If edit buttons are greyed out remove css class
   jQuery('.editb').removeClass('grey-out-image');
}

/*
 *  Remove 'Edit' records events
 */
function removeEditRecordEvent() {

   if (jQuery('#list-form').length) {
       jQuery('#list-form').off("click");
   }
  
   // Grey out edit buttons
   jQuery('.editb').addClass('grey-out-image');
   
}

/*
 *  'Update' record event (Edit mode)
 */
function updateRecordEvent() {
   if(jQuery('#edit-update').length) {
       
      jQuery('#edit-update').click(function() {
          updateRecordAction();
      });
  
  }
}

/*
 *  Go button event
 */
function goButtonEvent() {

  // 'Go' button exists
  if (jQuery('#go').length) {
      
    // Add click event for the 'Go' button 
    jQuery('#go').click(function() {
       goButtonAction();
    });
    
  }
  
 }
 
 /*
 *  Return button event
 */
function returnButtonEvent(url) {

  // 'Return' button exists
  if (jQuery('.return').length) {
      
    // Add click event for the 'Return' button 
    jQuery('.return').click(function() {
       parent.location = url;
    });
    
  }
  
 }
 
 
 

/*
 *  Add Checkbox Events for records list
 */
function chooseRecordEvent() {
    
   // Assign the 'list-table' the task of delegating the events for the checkboxes
   jQuery('#list-table').on('click', '#check-all, .list-check', function() {
    
     // Checkbox clicked
     var cbElement = jQuery(this);
    
     // If the 'Check All' checkbox is checked
      if (cbElement.attr('id') === "check-all") {
        
        if (cbElement.prop('checked')) {
            
           jQuery('.list-check').prop('checked', true);
           jQuery('.datarow, .datarowsel').prop('class', 'datarowsel');
           toggleGoFields(false);
           
        } else {
            
           jQuery('.list-check').prop('checked', false);
           jQuery('.datarow, .datarowsel').prop('class', 'datarow');
           toggleGoFields(true);
           
        }
          
       // If any other checkbox element is checked
       } else {
       
           var parentEle = cbElement.closest('tr');
           parentEle.toggleClass('datarow');
           parentEle.toggleClass('datarowsel');
        
           var numOfCheckboxes = jQuery('.list-check').length;
           var numChecked = jQuery('.list-check:checked').length;
           jQuery('#check-all').prop('checked', (numOfCheckboxes === numChecked));
           toggleGoFields(!(numChecked > 0)); 
  
      }
    });

}

/*
 *   Dim data cells
 */
function dimDataCells() {
   jQuery('.data-cell').addClass('grey-out-cell');
}


/*
 *  Un-dim data cells
 */
function unDimDataCells() {
   jQuery('.data-cell').removeClass('grey-out-cell');
}


/*
 *  Enable / Disable 'Go' button, 'Actions' drop down list
 */
function toggleGoFields(disable) {
   if (jQuery('#actions').length && jQuery('#go').length) {
       jQuery('#actions').attr('disabled', disable);
       jQuery('#go').attr('disabled', disable);
   }
}

/*
 *  Enable / Disable 'check-all' check box
 */
function toggleCheckAllBox(disable) {
    jQuery('#check-all').prop('checked', false).attr('disabled', disable);
}

/*
 *  Enable / Disable 'add' button
 */
function toggleAddButton(disable) {
    jQuery('#add').attr('disabled', disable);
}

/*
 *  Show / Hide base row
 */
function toggleBaseRow(show) {
    jQuery('#base-row').toggle(show);
}



/*
 * Toggle (Activate/deactivate) controls for edit or add mode
 * @param boolean disableAddButton
 * @param boolean disableGoFields
 * @param boolean disableCheckboxes
 * @param boolean showAddButton
 * @param boolean showGoFields
 */
function toggleListControls(disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields) {
   
   // Enable or Disable add button
   jQuery('#add').attr('disabled', disableAddButton);

   // Enable or Disable go fields
   jQuery('#go').attr('disabled', disableGoFields);
   jQuery('#actions').attr('disabled', disableGoFields);
   
   // Enable or Disable checkboxes
   jQuery('#list-table').find('input[type=checkbox]').attr('disabled', disableCheckboxes);
   
   // Show or Hide add button
   jQuery('#add').toggle(showAddButton);
    
   // Show or Hide go/action fields
   jQuery('#action-fields').toggle(showGoFields);
  
}

/*
 * Add 'Submit' buttons row
 */
function addSaveFields() {
   
    // Add save buttons
    var cancelLink = jQuery('<button>', {'id': 'add-cancel', 'type': 'button', 'text': 'cancel', 'title': 'cancel addition of new record', class: 'btn btn-danger btn-sm'});
    var saveLink = jQuery('<button>', {'id': 'add-save', 'type': 'button', 'text': 'save', 'title': 'save new record', class: 'btn btn-success btn-sm'});
    
    saveLink.insertBefore(jQuery('#add'));
    cancelLink.insertBefore(saveLink);
    //jQuery('tr#base-row td').addClass('grey-border-top');

}

/*
 *  Cancel event for record being added or edited
 */
function cancelRecordEvent(cancelEventFor) {
 
   // Cancel event when adding a new record
   if (cancelEventFor === 'add') {
       
     if (jQuery('#add-cancel').length) {
     
        jQuery('#add-cancel').click(function() {
            
        // Remove grey border used for record highlighter    
        jQuery('tr#base-row td').removeClass('grey-border-top');    
      
        // Show no records row	
        showNoRecordsRow();
        
        // Remove new record row
        removeNewRecordRow();
        
        // Remove save fields
        removeSaveFields();
        
        /* Reactivate form controls
         * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
         */
        toggleListControls(false, true, false, true, true);
        
        // Re-enable edit events
        editRecordEvent();
        
        // Un-dim data cells
        unDimDataCells();
        
     });
    
    }
 
  // Cancel event when editing a record
  } else if (cancelEventFor === 'edit') {
      
    if (jQuery('#edit-cancel').length) {
     
       jQuery('#edit-cancel').click(function() {
         
         // Remove editing rows
         removeEditingRows();
          
         // Show old row
         jQuery('#original-row-before-edit').show();
          
         jQuery('#original-row-before-edit').removeAttr('id');
          
         /* Reactivate form controls
          * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
          */
         toggleListControls(false, true, false, true, true);
          
         // Show base row if hidden
         toggleBaseRow(true);
          
         // Re-enable edit events
         editRecordEvent();
          
         // Un-dim data cells
         unDimDataCells();
          
        });
    
    }
 }
 
}

/*
 *  Render new record row
 */
function renderNewRecordRow() {
  
  // Get second last row in record list
  var secondLastRow = jQuery('#list-table').find("tr:nth-last-child(2)");

  // Create add row
  var addRow = jQuery('<tr>', {'id': 'add-row', 'class': 'addrow'});
  
  // Add first and last cells
  var firstCell = jQuery('<td>', {'id': 'first-add-cell', 'class': 'checkb'}).html("&nbsp;");
  var lastCell = jQuery('<td>', {'id': 'last-add-cell', 'class': 'titleedit2'}).html("&nbsp;");

      addRow.append(firstCell, lastCell);
      addRow.insertAfter(secondLastRow);
      
}

/*
 *  Render edit record row
 */
function renderEditRecordRow() {
  
  // Create add row
  var editRow = jQuery('<tr>', {'id': 'edit-row', 'class': 'editrw'});
  
  // Add first and last cells
  var firstCell = jQuery('<td>', {'id': 'first-edit-cell', 'class': 'checkb'}).html("&nbsp;");
  var lastCell = jQuery('<td>', {'id': 'last-edit-cell', 'class': 'titleedit2'}).html("&nbsp;");

      editRow.append(firstCell, lastCell);
      editRow.insertAfter(jQuery('#original-row-before-edit'));
      
}

/*
 *  Remove new record row
 */
function removeNewRecordRow() {
    // Remove add row	
    if (jQuery('#add-row').length) {
        jQuery('#add-row').remove();
    }
}

/*
 *  Render empty records row
 */
function renderNoRecordsRow() {
  
  // Create row
  var emptyRow = jQuery('<tr>', {'id': 'no-records-row'});
  
  // Get num of cells  
  var numOfCells = jQuery("#num-of-fields").val() + 1; 
  
  // Inner div and cell
  var emptyListMessage = jQuery('#list-empty-message').val();
  var innerDiv = jQuery('<div>', {'class': 'tablestatus3'}).html(emptyListMessage);
  var innerCell = jQuery('<td>', {'class': 'mess', 'colspan': numOfCells}).append(innerDiv);
      emptyRow.append(innerCell);
      emptyRow.insertAfter('#title-row');
}

/*
 *  Show no records message row
 */
function showNoRecordsRow() {
  if (jQuery('#no-records-row').length) {
      jQuery('#no-records-row').show();
  }
}


/*
 *  Hide no records message row
 */
function hideNoRecordsRow() {
  if (jQuery('#no-records-row').length) {
      jQuery('#no-records-row').hide();
  }
}

/*
 *  Delete no records message row
 */
function deleteNoRecordsRow() {
  if (jQuery('#no-records-row').length) {
      jQuery('#no-records-row').remove();
  }
}


/*
 *  Render list wait row
 */
function renderListWaitRow() {
 
  // Create row
  var emptyRow = jQuery('<tr>', {'id': 'wait-row'});
  
  // Get num of cells  
  var numOfCells = jQuery('#num-of-fields').val() + 1;
  
  // Total height of all data rows
  var tableHeight = jQuery('#list-table').height();
  var tableWidth  = jQuery('#list-table').width();
  var titleRowHeight = jQuery('#title-row').height();
  var lastRowHeight = jQuery('#list-table').find("tr:last").height();
  var dataRowsHeight = (tableHeight - titleRowHeight - lastRowHeight);
  
  jQuery('.datarow, .datarowsel').remove();
  
  // Inner div and cell
  var innerDiv = jQuery('<div>').html("Updating List...........");
  var innerCell = jQuery('<td>', {'id': 'list-wait-cell', 'colspan': numOfCells}).append(innerDiv);
      innerCell.css('height', dataRowsHeight);
      innerCell.css('width', tableWidth);
      emptyRow.append(innerCell);
      emptyRow.insertAfter('#title-row');  
}

/*
 *  Remove list wait row
 */
function removeListWaitRow() {
    // Remove list wait row	
    if (jQuery('#wait-row').length) {
        jQuery('#wait-row').remove();
    }
}


/*
 *  Add edit wait row
 */
function renderEditWaitRow() {
    
    // Wait row
    var waitRow = jQuery('<tr>', {'id': 'edit-wait-row'});
  
    // First and last cells
    var firstCell = jQuery('<td>', {'class': 'zero-padding'}).html("&nbsp;");
    var lastCell = jQuery('<td>', {'class': 'nbr'}).html("&nbsp;");
    
    // Wait cell 
    var rowWidth = jQuery('#original-row-before-edit').width();
    var rowHeight = jQuery('#original-row-before-edit').height();
    var numOfCells = jQuery('#num-of-fields').val();
    var waitCell = jQuery('<td>', {'class': 'waittd4', 'colspan': numOfCells}).html("&nbsp;");
        waitCell.css({'width' : rowWidth + 'px', 'height' : rowHeight + 'px'});
        
    // Put row together
    waitRow.append(firstCell, waitCell, lastCell);
    
    // Insert wait row at same location as hidden row being edited
    waitRow.insertAfter('#original-row-before-edit');
    
}

/*
 *  Remove edit wait row
 */
function removeEditWaitRow() {
    // Remove list wait row	
    if (jQuery('#edit-wait-row').length) {
        jQuery('#edit-wait-row').remove();
    }
}


/*
 *  Remove editing rows
 */
function removeEditingRows() {
    
    // Remove update fields row	
    if (jQuery('#update-fields-row').length) {
        jQuery('#update-fields-row').remove();
    }
    
    // Remove edit row
    if (jQuery('#edit-row').length) {
        jQuery('#edit-row').remove();
    }
        
}


/*
 *  Add save wait
 */
function addSaveWait() {
    jQuery('#add-cancel, #add-save').css('display', 'none');
    jQuery('#base-button-td').prop('class', 'waittd3');
}


/*
 *  Remove save wait
 */
function removeSaveWait() {
    jQuery('#base-button-td').prop('class', 'base-button-options');
}

/*
 *  Show save fields
 */
function showSaveFields() {
    jQuery('#add-cancel, #add-save').css('display', '');
}

/*
 *  Remove save fields
 */
function removeSaveFields() {
    // Remove cancel and submit/save buttons	
    if (jQuery('#add-cancel').length && jQuery('#add-save').length) {
        jQuery("#add-cancel, #add-save").remove();
    }
}

/*
 *  Add update wait
 */
function addUpdateWait() {
    jQuery('#edit-cancel, #edit-update').css('display', 'none');
    jQuery('#update-fields-cell').prop('class', 'waittd2');
}

/*
 *  Remove update wait
 */
function removeUpdateWait() {
    jQuery('#update-fields-cell').prop('class', 'editbts');
}

/*
 *  Show update fields
 */
function showUpdateFields() {
    jQuery('#edit-cancel, #edit-update').css('display', '');
}


/*
 *  Render update fields row
 */
function renderUpdateFieldsRow() {
    
    // Create update fields row
    var updateFieldsRow = jQuery('<tr>', {'id': 'update-fields-row'});
  
    // First cell
    var firstCell = jQuery('<td>', {'class': 'editbtsl'}).html("&nbsp;");
       
    // Button fields cell [Middle]
    var numOfCells = jQuery('#num-of-fields').val();
    var fieldsCell = jQuery('<td>', {'id': 'update-fields-cell', 'class': 'editbts', 'colspan': numOfCells});
    
    // Last cell
    var lastCell = jQuery('<td>', {'class': 'editbtsr'}).html("&nbsp;");
    
    // Cancel and Update link
    var cancelLink = jQuery('<button>', {'type': 'button', 'id': 'edit-cancel', 'title': 'Cancel Record Update', 'text': 'cancel', class: 'btn btn-danger btn-sm'});
    var updateLink = jQuery('<button>', {'type': 'button', 'id': 'edit-update', 'title': 'Update Record', 'text': 'update', class: 'btn btn-success btn-sm'});
    
    fieldsCell.append(cancelLink, updateLink);
    
    // Put row together
    updateFieldsRow.append(firstCell, fieldsCell, lastCell);
    
    /* If the update fields row happens to sit above the base row the hide the base row
     * and remove the bottom border from the edit row
     */
    if (jQuery('#edit-row').next().attr('id') === "base-row") {
        toggleBaseRow(false);
        updateFieldsRow.addClass('nbbtr');
    }
   
    // Insert update fields row after edit row
    updateFieldsRow.insertAfter('#edit-row');
}


/*
 *  Append new record to record list
 *  @param int recordID
 *  @param array|mixed recordData
 *  @param boolean includeLink to sub page/section
 */
function appendRecordToList(recordID, recordData, includeLink) {
  
   // Get second last row in record list
   var secondLastRow = jQuery('#list-table').find("tr:nth-last-child(2)");

   // Create new row to append
   var addRow = jQuery('<tr>', {'class': 'datarow'});
   var tooltipMessage = jQuery('#edit-tooltip-message').val();
   var anchor = jQuery('<a>', {'class': 'edit-record', 'href': 'javascript:void(0)'});
   var image = jQuery('<img>', {'class': 'editb', 'title': tooltipMessage, 'alt': tooltipMessage, 'src': 'assets/images/edit.svg'});
   var checkbox = jQuery('<input>', {'type': 'checkbox', 'class': 'list-check', 'value': recordID});
   var firstCell = jQuery('<td>', {'class': 'checkb'});
   var lastCell = jQuery('<td>', {'class': 'titleedit2'});
   
   firstCell.append(checkbox);
   anchor.append(image);
   lastCell.append(anchor);
   addRow.append(firstCell);
   
   // Append data cells
   var recordInc = 1;
   jQuery.each(recordData, function(key, value) {
       
       // Include link to sub section [Need to think of a solution for multiple sub pages]
       if (recordInc === 1 && includeLink) {
          var linkUrl = "manage.php?page=" + jQuery('#field1-subpage').val() + "&id=" + recordID;
          var innerElement =  jQuery('<a>', {'class': 'field-link', 'href': linkUrl}).html(value);
       } else {
          var innerElement =  jQuery('<span>').html(value); 
       }
       
       var dataCell = jQuery('<td>').append(innerElement);
       
       addRow.append(dataCell);
       
       recordInc++;
   });
   
   addRow.append(lastCell);
   addRow.insertAfter(secondLastRow);
    
}

/*
 * Scroll to bottom of page
 * @param string scrollType
 */
function scrollBottomOfPage(scrollType) {
     
     // If with no animation i.e. instant scroll
     if (scrollType === 'no_animation') {
       jQuery('html, body').scrollTop( jQuery(document).height());
     } else {
       jQuery('html, body').animate({scrollTop:jQuery(document).height()}, 'slow');  
     }
     
}

/**
 * Loading dropdown
 */
function loadingDropdown(element) {
    element.empty().append(jQuery('<option>', {'value': '', 'text': 'Loading....', 'class': 'select-load'}));
    element.addClass('select-load');
    return element;
}

function getResponseHeaders(jqXHR){
    jqXHR.responseHeaders = {};
    var headers = jqXHR.getAllResponseHeaders();
    headers = headers.split("\n");
    headers.forEach(function (header) {
      header = header.split(": ");
      var key = header.shift();
      if (key.length == 0) return
      // chrome60+ force lowercase, other browsers can be different
      key = key.toLowerCase(); 
      jqXHR.responseHeaders[key] = header.join(": ");
    });
  }