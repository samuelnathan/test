/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * JS Code for Global Rating Scale Values
 */

// Server page URL
var requestUrl = "tools/ajax_grs_values.php";

jQuery(document).ready(function() {
    
   // Add events
   chooseRecordEvent();
   addRecordEvent();
   editRecordEvent();
   
   goButtonEvent();
   returnButtonEvent("manage.php?page=grs_types_tool");
   
   // Show menu
   showMenu();
});


/*
 *  Global Rating Scale value 'edit' event action
 */
function editRecordAction(trigger) {
    
    var parentRow = trigger.closest('tr');
    var recordKey = parentRow.find('input[class=list-check]').val(); 
    parentRow.attr('id', 'original-row-before-edit');
    parentRow.hide();
        
    /* Deactivate form controls
     * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
     */
    toggleListControls(true, true, true, true, true);
    
    // Deactivate edit buttons
    removeEditRecordEvent();
    
    // Dim data cells
    dimDataCells();

    // Add wait row
    renderEditWaitRow();
    
    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'get_grs_value',
                record_key: recordKey},
         dataType: 'json'
     }).done(function(record) {
       
       // Remove edit wait row
       removeEditWaitRow();
       renderEditRecordRow();
          
       // Add additional fields and values
       var valueField = jQuery('<input>', {'type': 'text',
                                          'id': 'scale-value',
                                          'maxlength': '2',
                                          'class': 'tf1',
                                          'value': record.scale_value});
                                      
       var descriptionField = jQuery('<input>', {'type': 'text',
                                            'id': 'value-description',
                                            'class': 'tf2',
                                            'value': record.scale_value_description});
                                        
       var borderlineField = jQuery('<input>', {
                           'type': 'checkbox',
                           'id': 'value-borderline',
                           'checked': (record.scale_value_borderline == 1),
                           'value': record.scale_value_borderline
       });      
       
       var failField = jQuery('<input>', {
                           'type': 'checkbox',
                           'id': 'value-fail',
                           'checked': (record.scale_value_fail == 1),
                           'value': record.scale_value_fail
       });         
                                                                        
       var valueCell = jQuery('<td>').append(valueField);
       var descriptionCell = jQuery('<td>').append(descriptionField);
       var borderlineCell = jQuery('<td>').append(borderlineField);
       var failCell = jQuery('<td>').append(failField);
       jQuery('#first-edit-cell').after(valueCell, descriptionCell, borderlineCell, failCell);
       
       // Show update fields row
       renderUpdateFieldsRow();
       
       // Add cancel edit event
       cancelRecordEvent('edit');
       
       // Add update record event
       updateRecordEvent();
       
     }).fail(function() {
         removeEditWaitRow();
         parentRow.show();
         alert("Record could not be retrieved, please refresh page [F5]");
     });
    
}


/*
 *  Global Rating Scale value 'update' event action
 */
function updateRecordAction() {
    
   // Get values to pass
   var scaleValue = jQuery('#scale-value').val();
   var valueDescription = jQuery('#value-description').val();
   
   // [DC 29/10/2014] Need to improve validation here as it's quite basic
   if(!jQuery.isNumeric(scaleValue) || scaleValue.length === 0 || valueDescription.length === 0) {
      alert("Please complete all fields\nNote: The 'GRS value' field only accepts numeric values");
      return;
   }

   // Hide update fields and show wait indicator
   addUpdateWait();
   
   // Get record key from row being edited
   var recordKey = jQuery('#original-row-before-edit').find('input[class=list-check]').val(); 

   // Post data to server
   jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {
             isfor: 'update_grs_value',
             record_key: recordKey,
             scale_value: scaleValue,
             scale_value_description: valueDescription,
             value_borderline: jQuery('#value-borderline').prop('checked'),
             value_fail: jQuery('#value-fail').prop('checked')
            }
   }).done(function(response) {

      removeUpdateWait();
      if (response === "RECORD_NOT_UPDATED") {
          showUpdateFields();
          alert("Record could not be updated, please try again");
      } else {
          removeEditingRows();
          
          /* Reactivate form controls
           * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
           */
          toggleListControls(false, true, false, true, true);
          
          // Show base row
          toggleBaseRow(true);
          
          // Reload list
          reloadRecordList(true);
          
          // Re-enable edit events
          editRecordEvent();
       }

    }).fail(function() {
         removeUpdateWait();
         showUpdateFields();
         alert("Record could not be updated, please try again");
    });
  
}


/*
 *  Global Rating Scale value 'Add' event action
 */
function addRecordAction() {

    /* Deactivate form controls
     * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
     */
    toggleListControls(true, true, true, false, false);

    // Hide no records message row if it exists
    hideNoRecordsRow();

    // Render new record row
    renderNewRecordRow();

    // Remove edit events
    removeEditRecordEvent();
    
    // Dim data cells
    dimDataCells();

    // Add additional fields
    var valueField = jQuery('<input>', {'type': 'text', 'id': 'scale-value', 'maxlength': '2', 'class': 'tf1'});
    var descriptionField = jQuery('<input>', {'type': 'text', 'id': 'value-description', 'class': 'tf2'});
    var borderlineField = jQuery('<input>', {'type': 'checkbox', 'id': 'value-borderline', 'value': '0'});
    var failField = jQuery('<input>', {'type': 'checkbox', 'id': 'value-fail', 'value': '0'});
    var valueCell = jQuery('<td>').append(valueField);
    var descriptionCell = jQuery('<td>').append(descriptionField);
    var borderlineCell = jQuery('<td>').append(borderlineField);
    var failCell = jQuery('<td>').append(failField);
    jQuery('#first-add-cell').after(valueCell, descriptionCell, borderlineCell, failCell);

    // Add buttons for new record
    addSaveFields();

    // Add cancel event for new record 
    cancelRecordEvent('add');

    // Add save event for new record 
    saveRecordEvent();
    
    // Focus first field
    jQuery('#scale-value').focus();     

    // Scroll to bottom of page
    scrollBottomOfPage('slow');

}

/*
 *  Global Rating Scale value 'Save' event action
 */
function saveRecordAction() {
  
    // Get values to pass
    var scaleValue = jQuery('#scale-value').val();
    var valueDescription = jQuery('#value-description').val();

    // [DC 29/10/2014] Need to improve validation here as it's quite basic
   if(!jQuery.isNumeric(scaleValue) || scaleValue.length === 0 || valueDescription.length === 0) {
      alert("Please complete all fields\nNote: The 'GRS value' field only accepts numeric values");
      return;
   }

    // Hide save fields
    addSaveWait();

    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {
             isfor: 'add_grs_value',
             scale_type_id: jQuery('#scale_type_id').val(),
             scale_value: scaleValue,
             scale_value_description: valueDescription,
             value_borderline: jQuery('#value-borderline').prop('checked'),
             value_fail: jQuery('#value-fail').prop('checked')             
            }
     }).done(function(response) {
         
         removeSaveWait();
         if (response === "RECORD_NOT_SAVED") {
            showSaveFields();
            alert("Record could not be saved, please try again");
         } else {
            removeNewRecordRow();
            removeSaveFields();

            /* Reactivate form controls
             * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
             */
            toggleListControls(false, true, false, true, true);

            reloadRecordList(true);
            
            // Re-enable edit events
            editRecordEvent();
            
         }

     }).fail(function() {
         removeSaveWait();
         showSaveFields();
         alert("Record could not be saved, please try again");
     });
}

/*
 *  Global Rating Values List 'Action' event action
 */
function goButtonAction() {
 
  // What is the action ?
  var action = jQuery('#actions').val(); 
  if (action === "delete") {

    var recordsToDelete = jQuery(".list-check:checked").map(function() {
        return jQuery(this).val();
    }).get();

    // Nothing to delete then get the hell out of here
    if (recordsToDelete.length === 0) {
        toggleGoFields(true);
        return;
    }

    // Confirm with user
    var yes = confirm("Are you sure you would like to delete the selected record(s)");
      if (!yes) {
        return;
      }

      // Post data to server
      jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'delete_grs_values',
                records_to_delete: recordsToDelete}
         }).done(function() {

           reloadRecordList(false);

         }).fail(function() {
            alert("Selected Record(s) could not be deleted, please try again");
         });

      }
}

/*
 * Reload record list
 * @param Boolean true|false scrollToBottom
 */
function reloadRecordList(scrollToBottom) {
    
    // Remove old records and wait for new records
    toggleGoFields(true);
    toggleAddButton(true);
    toggleCheckAllBox(true);
    renderListWaitRow();

    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {
          isfor: 'get_all_grs_values',
          scale_type_id: jQuery('#scale_type_id').val()
         },
          dataType: 'json'
     }).done(function(recordsJSON) {
       
       // Remove list wait message
       removeListWaitRow();
             
       // Do we have records to process ? 
       if (recordsJSON.length > 0) {
        
         // Loop through returned records
         jQuery.each(recordsJSON, function(index, eachRecord) {
             var recordID = eachRecord.scale_value_id;
             
             var checkIcon = jQuery('<img>', {
                 'class': 'check-icon',
                 'title': 'enabled',
                 'alt': 'enabled',
                 'src': 'assets/images/green-check.png'
             });
             
             var borderline = (eachRecord.scale_value_borderline == 1) ? checkIcon : '';
             var fail = (eachRecord.scale_value_fail == 1) ? checkIcon.clone() : '';
             
             var recordData = [
                 eachRecord.scale_value, 
                 eachRecord.scale_value_description,
                 borderline,
                 fail
                ];
                appendRecordToList(recordID, recordData, false);
         });
     
         // Scroll to bottom of page
         if (scrollToBottom) { 
           scrollBottomOfPage('no_animation');
         }
         
         deleteNoRecordsRow();
         toggleCheckAllBox(false);
       } else {
         renderNoRecordsRow();
       }
       toggleAddButton(false);
       
     }).fail(function() {
         alert("Records could not be retrieved, please refresh page [F5]"); 
     });
     
   
}