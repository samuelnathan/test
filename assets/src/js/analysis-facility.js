/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2019 Qpercom Limited.  All rights reserved.
 */

checkWeightings = function() {
var weightingsValid = true;

            jQuery(".sub-rules-table").each(function() {
                var weightings = jQuery(this).find("input[class^='weight-field']");
                var percentTotal = 0;

                weightings.each(function() {
                    percentTotal += parseFloat(jQuery(this).val());
                });

                if (percentTotal != 0 && percentTotal != 100) {

                    weightingsValid = false;
                    jQuery('.rules-normal').css({
                       'display':'none'
                    });

                    jQuery('.rules-highlight').css({
                    'cssText': 'color: #ff0000 !important;',
                    'font-weight': 'bold'

                    });

                weightings.addClass('incomplete-pass');
                    return false;
                }
                return weightingsValid;
           });
       };
jQuery(document).ready(function() {
    checkWeightings();
    /* Main Analysis Section  */
    if (jQuery('#res_options').length) {
        /* Set default session values */
        var sessionValues = jQuery('#default-sess-values').val();

        /* Delete Button */
        if (jQuery('#delete-results').length) {
            jQuery('#delete-results').on('click', function(e) {
                e.preventDefault();
                jQuery('#analysis_form').attr('action', 'manage.php?page=dresults');
                if (jQuery('#analysis_form').attr('action') === 'manage.php?page=dresults') {
                    submitUpdateForm(true); /* pass in true for delete mode */
                }
            });
        }

        /* Checkbox Delete Button Unlock */
        if (jQuery('#cb-del').length) {
            jQuery('#cb-del').on('click', function(e) {
                jQuery('#delete-box').css('display', 'none');
                jQuery('#delete-results').css('display', 'inline');
            });
        }

        /* Sessions and Groups Checkboxes */

        /* Add Station Status Function */
        var addStationStatus = function(message) {
            jQuery('#station-dets').append(jQuery('<li>', {'id': 'station-status'}).text(message));
        };

        /* Add Station Wait Function */
        var addStationWait = function(message) {
            jQuery('#station-dets').append(jQuery('<li>', {'id': 'station-wait'}).text(message));
        };

        /* Remove Station Status Function */
        var removeStationStatus = function() {
            if (jQuery('#station-status').length) {
                jQuery('#station-status').remove();
            }
        };

        /* Remove Station Wait Function */
        var removeStationWait = function() {
            if (jQuery('#station-wait').length) {
                jQuery('#station-wait').remove();
            }
        };

        /* Toggle Enable/Disable Delete Button Function */
        var toggleDeleteButton = function(disabled) {
            if (jQuery('#cb-del').length) {
                jQuery('#cb-del').prop('checked', false).prop('disabled', disabled);
            }
            if (jQuery('#delete-box').length) {
                jQuery('#delete-box').css('display', 'block');
            }
            if (jQuery('#delete-results').length) {
                jQuery('#delete-results').css('display', 'none');
            }
        };

        /* Toggle Enable/Disable Delete Button Function */
        var toggleUpdateButton = function(disabled) {
            jQuery('#updateanaly').prop('disabled', disabled);
        };

        /* Adopt Station Element Function */
        var adoptStationElement = function(stationObject, stationIndex) {
            var stationKey = ((combinationValue === 2) ? stationObject.stag : stationObject.snum) + ':' + stationObject.formid;
            var stationLi = jQuery('<li>', {'class': 'station-li'});
            var stationCB = jQuery('<input>', {'type': 'checkbox', 'id': 'es' + stationIndex, 'name': 'stations[]', 'class': 'each-station', 'value': stationKey}).prop('checked', true);

            var combinationValue = jQuery('#cnm').val();
            var stationText;
            switch (combinationValue) {
                case 1:
                    stationText = stationObject.snum + ' - ' + stationObject.snam;
                    break;
                case 2:
                    stationText = stationObject.stag + ' - ' + stationObject.snam;
                    break;
                default:
                    stationText = stationObject.snam;
            }

            var stationLabel = jQuery('<label>', {'for': 'es' + stationIndex}).text(stationText);
            jQuery('#station-dets').append(stationLi.append(stationCB, stationLabel));
        };

        /* Combine By Drop Down Event */
        if (jQuery('#cnm').length) {
            jQuery('#cnm').on('change', function(e) {
                refreshStationList(sessionValues);
            });
        }

        /* Refresh Station List Function */
        var refreshStationList = function(sessionValues) {
            jQuery('.station-li').remove();
            jQuery('#all-stations-li').addClass('hide');

            removeStationStatus();
            removeStationWait();
            toggleDeleteButton(true);
            toggleUpdateButton(true);

            var groupsCbs = jQuery('.studentgroups:checked');

            if (groupsCbs.length > 0) {
                
                addStationWait('Please wait.......');
                
                AjaxQueue({
                    url: 'analysis/asyn_analysis.php',
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    data: {
                        isfor: 'stations',
                        exam: jQuery('#exam-id').val(),
                        sessions: sessionValues,
                        cnm: jQuery('#cnm').val()
                    },
                    timeout: 4000,
                    success: function(stations){
                        removeStationStatus();
                        removeStationWait();
                        if (stations.length > 0) {
                            jQuery('.station-li').remove();
                            jQuery('#all-stations-li').removeClass('hide');
                            jQuery('#stations-cb').prop('checked', true);
                            var stationsIndex = 0;
                            jQuery.each(stations, function(i, station) {
                                adoptStationElement(station, ++stationsIndex);
                            });
                            stationCheckboxesEvents();
                            toggleDeleteButton(false);
                            toggleUpdateButton(false);
                        } else {
                            addStationStatus('No stations found for days/circuits selected');
                        }
                    },
                    error: function (jqXHR){
                        var message = "Failed to get list of stations for selected days/circuits.\n";
                        switch (jqXHR.status) {
                            case 404:
                            case 500:
                                message += "An internal error occurred";
                                break;
                            case 400:
                                message += "Update request was invalid.";
                                break;
                            case 403:
                                message += "Sorry, You may not be logged in or your session may have expired";
                                break;
                            default:
                                message += "An unspecified error has occurred. Contact your system administrator.";
                                message += "\n(HTTP Code: " + jqXHR.status + ")";
                        }

                        alert(message);
                    }
                })
            } else {
                addStationStatus('Please select at least 1 day/circuit or group');
            }
        };

        /* Update Session IDs Function */
        var updateSessionIDs = function() {
            var tempValues = [];
            var sessionList = jQuery('.tdli');
            jQuery.each(sessionList, function(index, li) {
                if (jQuery(li).find('ul').find('input.studentgroups:checked').length !== 0) {
                  var tdchksVal = jQuery(li).find('input.tdchks').val();
                  if (jQuery.inArray( tdchksVal, tempValues ) == -1) {
                    tempValues.push(jQuery(li).find('input.tdchks').val());
                  }
                }
            });

            if (tempValues.toString() !== sessionValues) {
                sessionValues = tempValues.toString();
                refreshStationList(sessionValues);
            }
        };

        /* All Checkbox Events Window 1 */
        jQuery(document).on('click', '#sessions-grps input[type="checkbox"]', function(e) {
            var $self = jQuery(this);
            if ($self.attr('id') === 'exam-cb') { /* Exam Checked */
                var cbs = jQuery('#sessions-grps').find('input.studentgroups, input.tdchks');
                jQuery.each(cbs, function(index, cb) {
                    jQuery(cb).prop('checked', $self.prop('checked'));
                })

                updateSessionIDs();
            }
            /* SESSION Checked */
            else if ($self.hasClass('tdchks')) {
                var inputState = jQuery('.tdchks:checked').length === jQuery('#session-cnt').val();
                jQuery('.exam-cb').prop('checked', inputState);


                var groupsCbs = $self.nextAll('ul').find('input.studentgroups');
                jQuery.each(groupsCbs, function(index, cb) {
                    jQuery(cb).prop('checked', $self.prop('checked') );
                });

                updateSessionIDs();
            } else if ($self.hasClass('studentgroups')) { /* GROUPS Checked */
                var parentList = $self.parents('li[class^=tdli]:first');
                var sessionCBs = parentList.find('input.tdchks');
                var groupsNotChecked = parentList.find('ul').find('input').not('.studentgroups:checked').length;
                sessionCBs.prop('checked', (groupsNotChecked > 0) ? false : true);

                var inputState = (jQuery('.studentgroups:checked').length === jQuery('#total-grp-cnt').val()) ? true : false;

                jQuery('#exam-cb').prop('checked', inputState);

                updateSessionIDs();
            }
        });

        /* Station Checkbox Events */
        jQuery('#stations-cb').on('click', function() {
            var checked = jQuery(this).prop('checked')
            jQuery('.each-station').each(function(i, cb) {
                jQuery(cb).prop('checked', checked);
            });
        });

        /* Add each station click event */
        var stationCheckboxesEvents = function() {
            jQuery('#station-dets').off('click', '.each-station').on('click', '.each-station', function(e) {
                var checked = (jQuery('#station-dets').find('input:not([class=each-station]:checked)').length - 1 > 0) ? false : true
                jQuery('#stations-cb').prop('checked', checked);
            });
        };

        stationCheckboxesEvents();

        /* Station Setup Section (Edit Pass Values Event) */
        jQuery(".edit-rules").click(function() {

            // table background color
            jQuery(".sub-rules-table, #rules-table").css("background-color", "#f7f7f7");

            // hide edit button
            jQuery(".edit-rules").hide();
            jQuery(".pass-cancel").show();
            jQuery(".pass-update").show();

            // cancel and add buttons
            var passInputs = jQuery(".sub-rules-table").find("input[class^='pass-field']");
            var weightInputs = jQuery(".sub-rules-table").find("input[class^='weight-field']");
            var iphidden = jQuery("<input>", {"type": "hidden", "id": "rules-edit-mode", "name": "update_rules", "value": "update"});
                iphidden.insertBefore(jQuery(".pass-cancel"));

            jQuery(".individual-rule").removeClass("hide");
            jQuery(".individual-rule-label").addClass("hide");

            // edit pass fields
            jQuery.merge(passInputs, weightInputs).each(function() {
                jQuery(this).prop('readonly', false).removeClass('readonly').addClass('edit-mode');
            });
        });

        // CANCEL RULES UPDATE button event
        jQuery(".pass-cancel").click(function() {
            jQuery(".individual-rule").each(function(index, element) {
                jQuery(element).val(
                   jQuery("#original-" + jQuery(element).attr('id')).val()
                );
            });

            jQuery(".pass-cancel").hide();
            jQuery(".pass-update").hide();
            jQuery(".edit-rules").show();
            jQuery(".sub-rules-table, #rules-table").css("background-color", "");

            // make pass fields non-edit
            var passInputs = jQuery(".sub-rules-table").find("input[class^='pass-field']");
            passInputs.each(function() {
                jQuery(this).prop("readonly", true)
                            .removeClass("edit-mode")
                            .removeClass("incomplete-pass")
                            .addClass("readonly");
                jQuery(this).val(jQuery(this).prev('input.original-pass').val());
            });

            var weightInputs = jQuery(".sub-rules-table").find("input[class^='weight-field']");
            weightInputs.each(function() {
                jQuery(this).prop("readonly", true)
                            .removeClass("edit-mode")
                            .removeClass("incomplete-pass")
                            .addClass("readonly");
                jQuery(this).val(jQuery(this).prev('input.original-weight').val());
            });

            jQuery(".individual-rule").addClass("hide");
            jQuery(".individual-rule-label").removeClass("hide");
            jQuery("#rules-edit-mode").remove();

        });

        // UPDATE RULES button event
        jQuery(".pass-update").click(function() {

            // Divergence threshold
            var divergenceValue = jQuery('#divergence-threshold').val();
            if (divergenceValue.length > 0 && !jQuery.isNumeric(divergenceValue)) {
               alert("Divergence threshold value must either be numeric or empty (disabled)");
               return;
            }

            // Weighting checks
            var weightingsValid = true;

            jQuery(".sub-rules-table").each(function() {

                var weightings = jQuery(this).find("input[class^='weight-field']");
                var percentTotal = 0;

                weightings.each(function() {

                    percentTotal += parseFloat(jQuery(this).val());

                });

                if (percentTotal != 0 && percentTotal != 100) {

                    weightingsValid = false;
                    alert(
                     "The total weighting value (%) for stations in each circuit must equal to 0% or 100%, " +
                     "Please review. \n\n 0% = Weightings disabled\n100% = Weightings enabled");

                     weightings.addClass('incomplete-pass');

                    return false;

                }

                return weightingsValid;

           });

           if (weightingsValid == false) {
               return;
           }

           submitUpdateForm(null);
        });


        /* Tab selection events */
        if (jQuery('#tabs').length) {
            jQuery('#tabs').on('click', 'a', function(e) {
                e.preventDefault();
                var parentList = jQuery(this).parents('li:first');
                var tabs = jQuery('#tabs').find('li');

                jQuery.each(tabs, function(i, tab) {
                    jQuery(tab).removeAttr('id');
                    var selector = '#' + jQuery(tab).attr('class');
                    if (jQuery(selector).length) {
                        jQuery(selector).removeClass('show-content');
                    }
                });

                var parentListClass = '#' + parentList.attr('class');
                jQuery(parentListClass).addClass('show-content');

                // Tab Selection & Hidden Variable
                parentList.attr('id', 'tab-selected');
                jQuery('#tab-persist').val(parentListClass);
            });
        }

        // Any alerts/notifications then highlight tab
        // use this to highlight rules tab possibly
        if (jQuery('.divergence-alert, .flag-alert').length > 0) {
            jQuery('.alerts-tab').css({
                'cssText': 'color: #ff0000 !important;',
                'font-weight': 'bold'
            });
        }

        /* Exam notifications and alerts check */
        if (jQuery('#displayalerts').is(':checked')) {

            setInterval(function() {

               jQuery.ajax({
                 url: "analysis/asyn_analysis.php",
                 type: "post",
                 dataType: "json",
                 cache: false,
                 data: {
                   isfor: 'notifications',
                   sessions: sessionValues
                 }
              })
              .done(function(notifications) {

                jQuery.each(notifications, function() {
                   var alert = jQuery(this)[0];

                    if (jQuery('#alert' + alert.id).length == 0) {

                        jQuery('.alerts-tab').css({
                           'cssText': 'color: #ff0000 !important;',
                           'font-weight': 'bold'
                        });

                        // Remove no-notfications message if none found
                        if (jQuery('#no-notifications-message').length) {
                            jQuery('#no-notifications-message').remove();
                        }

                        var newAlert = jQuery('#template-alert').clone();
                        var alertType = ((alert.flag_type == 1 ?  'flag' : 'divergence') + '-alert');
                        var existingUrl = newAlert.find('a').attr('href');
                        var additionalUrl = alert.student_id + "&exam=" + jQuery('#exam-id').val()
                                          + "&cnm=" + jQuery('#cnm').val();

                        if (alert.flag_type == 1) {
                          var alertMessage = alert.flag_count + " flag"
                                           + (alert.flag_count == 1 ?  '' : 's')
                                           + " registered";
                        } else {
                          var alertMessage = " divergence threshold of "
                                           + alert.divergence_threshold + " exceeded";
                        }

                        newAlert.attr('id', "alert" + alert.id);
                        newAlert.attr('class', alertType);
                        newAlert.find('span.timestamp').text(alert.timestamp);
                        newAlert.find('span.message-part-1').text(alertMessage);
                        newAlert.find('span.message-part-2').text(alert.station_number);
                        newAlert.find('a').text(alert.student_id);
                        newAlert.find('a').attr('href', existingUrl + additionalUrl);
                        jQuery('#notifications-container').prepend(newAlert);

                    }

                 });

                 if (notifications.length == 0) {
                       jQuery('.alerts-tab').css({
                           'cssText': '', 'font-weight': 'normal'
                       });
                       jQuery('#notifications-container').empty();
                 }

              })
              .fail(function(jqXHR) {

                console.log(
                  httpStatusCodeHandler(
                     jqXHR.status,
                     "Failed to retrieve exam notifications.\n"
                ));

              });

            }, 20000);
        }

        if (jQuery('#analysis_table').length) {
           /* Ordering Title Events */
           if (jQuery('#order-titlerow').length) {
            jQuery('#order-titlerow').on('click', 'a[class*="orderlink"]', function(e) {
               e.preventDefault();

               // If raw analysis form
               if (jQuery('#analysis_form').attr('action') === 'manage.php?page=out_analysis') {
                 jQuery('#order-index').val(jQuery(this).nextAll('input.sort-index').val());
                 jQuery('#order-type').val(jQuery(this).nextAll('input.next-sort-type').val());
                 submitUpdateForm(null);
               }

              });
            }

            /* View Each Station Result for Person */
            jQuery('#analysis_table').find('a.student-result-link').click(function() {

                // Hightlight row
                jQuery(this).parents('td:first').addClass('highlight');

                // Get hidden value containing result ID
                var idHidden = jQuery(this).nextAll('input.student-result-id');

                // Score link i.e. muli-results
                var scoreType = idHidden.hasClass('score');

                // Result ID value
                var id = idHidden.val();

                var linkType = (scoreType) ? 'results_id' : 'id';

                var winPop = window.open(
                    'manage.php?page=studentresult'
                    + '&' + linkType + '=' + id
                    + (!jQuery('#displaystudentname').is(':checked') ? "&anon=" : ""),
                    'StudentStationAnalysis' + id
                );
                winPop.focus();

            });

            // View Overall Results for Person - Link on left column of raw resuls with person icon
            jQuery('#tabs-content').on('click', 'a.examineel', function() {

                if (!jQuery(this).hasClass('self-section')) {

                  jQuery(this).parents('td:first').parents('tr:first').addClass('highlight');

                }

                var winpop = open('manage.php?page=studentexam'
                           + '&student=' + jQuery(this).text()
                           + '&exam=' + jQuery('#exam-id').val()
                           + '&cnm=' + jQuery('#cnm').val()
                           + (!jQuery('#displaystudentname').is(':checked') ? "&anon=" : "")
                           + (jQuery(this).hasClass('self-section') ? "#self" : ""),
                        'StudentExamAnalysis' + jQuery(this).text());
                winpop.focus();
            });

            /* Hover Over Message */
            jQuery( '.hovermessage, .title_ad, .title_au' ).tooltip({
                placement: "left",
                track: true,
                html:true,
                classes: {
                    "ui-tooltip": "tips4"
                  }
              });


            /* Detailed Station/Scenario Results : Detailed Tab */
            if (jQuery('#choose-form').length) {
                // Click Button
                jQuery('#choose-form').on('change', function(e) {
                    e.preventDefault();

                    jQuery('#form-selected').val(jQuery('#choose-form').val());

                    // Empty Iframe
                    var ifdoc = jQuery('#detailed-iframe').get(0).contentDocument || jQuery('#detailed-iframe').get(0).contentWindow.document;
                    ifdoc.removeChild(ifdoc.documentElement);

                    // Show iframe wait
                    if (jQuery(this).val().length > 0) {
                        jQuery('#iframe-load').removeClass('hide');
                    }



                    // Submit form
                    jQuery('#detailed_form').submit();


                });
            }

            jQuery('#detailed-iframe').on('load resize', function() {

                var container = jQuery(this).get(0).contentDocument.getElementById('resdetdv') || jQuery(this).get(0).contentWindow.document.getElementById('resdetdv');
                if (container) {

                    jQuery(this).css({
                        width: jQuery(container).width() + 20,
                        height: jQuery(container).height() +20
                    })

                }

            })

            /* Export Checkbox Events : Export Tab */
            if (jQuery('#export-fs').length) {
                jQuery('#export-fs').on('click', 'input[type=checkbox]', function() {
                    // Get Checkboxes
                    var exportCbs = jQuery('#export-fs').find('input[type=checkbox]:checked');

                    // Number of Checkboxes Checked
                    if (exportCbs.length === 0) {
                        jQuery('#export-analysis').prop('disabled', true);
                    } else {
                        jQuery('#export-analysis').prop('disabled', false);
                    }
                });
            }

            /* Export All/Complete Analysis Button */
            if (jQuery('#export-analysis').length) {
                jQuery('#export-analysis').on('click', function(e) {
                    e.preventDefault();

                    // Transfer checkbox values to form hidden variables
                    var expcbs = jQuery('#export-fs').find('input[type=checkbox]');

                    jQuery.each(expcbs, function(i, cb) {
                        var cbid = jQuery(cb).attr('id').split('-');

                        if (jQuery(cb).prop('checked')) {
                            jQuery('#' +cbid[0] + '-hidden').val(1);
                        } else {
                            jQuery('#' + cbid[0] + '-hidden').val(0);
                        }
                    });

                    // Show wait and disable export button
                    jQuery('#exporting-wait').css('display', 'block');
                    jQuery(this).prop('disabled', true);

                    // Check to see when export is finished [received 'cexp'
                    // cookie] and then clear wait and enable export button
                    var exportingTimer = setInterval(function() {
                        if(getCookie('cexp') == jQuery('#cexp').val()){
                            deleteCookie('cexp');
                            jQuery('#exporting-wait').css('display', '');
                            jQuery('#export-analysis').prop('disabled', false);
                            clearTimer();
                        }
                    }, 200);

                    var clearTimer = function() {
                        clearInterval(exportingTimer);
                    };

                    // Submit
                    jQuery('#analysis-export-form').submit();
                });
            }

            /* Examiner Marking Average Table */
            if (jQuery('#marking_average').length) {
                jQuery("#marking_average").easyTable({
                    sortIndex: 3,
                    sortable:true
                });
            }
            
            // Popup Window Title
            var windowTitle = jQuery('#exam-name').val() + " | ";
            var sessionsChecked = jQuery('#res_options').find('input[class=tdchks]:checked');
            jQuery.each(sessionsChecked, function(i, td) {
                windowTitle += jQuery('#sess-date' + td.value).val() + " " + jQuery('#sess-name' + td.value).val() + " | ";
            });
            document.title = windowTitle.slice(0, windowTitle.length - 2);

        }

        // show groups
        jQuery('.grpul').css('display', '');

        // Initiate treemenu
        jQuery("#tddets").treemenu();

        // Remove wait
        if (jQuery('#wait-content').length && jQuery('#tabs-content').length) {
            jQuery('#wait-content').remove();
            jQuery('#tabs-content').css('display', 'block');
        }

        // Read Tab Session Storage - Scroll Position and Tree State
        if (sessionStorage.getItem('tree_state') !== null && sessionStorage.getItem('tree_state').length > 0) {
            var stateArray = JSON.parse(sessionStorage.getItem('tree_state'));
            // Loop through tree states
            jQuery.each(stateArray[1], function(index, el) {
              jQuery('#tddets > li').eq(el).addClass('tree-opened active').removeClass('tree-closed').find('.treemenu').show()
            });

            // Clear Tab Session Storage
            sessionStorage.setItem('tree_state', '');

            // Scroll to positions of window
            jQuery(window).scrollTop(stateArray[0])
        }

        /* Scenario Data load on page load */
        if (jQuery('#choose-form').length && jQuery('#choose-form').val().length > 0) {
            jQuery('#form-selected').val(jQuery('#choose-form').val());
            jQuery('#iframe-load').removeClass('hide');
            jQuery('#detailed_form').submit();
            
        }

    /* Delete Results Section */
    } else if (jQuery('#delete_act').length) {

        // Delete Results Title
        document.title = "Delete Action OSCE Results";

        // Tree Expansion for Sessions & Groups list
        jQuery('#sess-del-list').treemenu()
    }


});

/* Validate Form */
function ValidateForm(form, deleting) {
    var i;
    // Analysis List Form
    if (jQuery(form).attr('name') === 'analysis_form') {
        if (!jQuery('#exam-id').length) {
            return false;
        } else if (jQuery('#exam-id').val().length === 0) {
            alert('No OSCE Specified');
            return false;
        }

        var theStations = jQuery('#station-dets').find('input[class=each-station]:checked');
        var theGroups = jQuery('#res_options').find('input[class=studentgroups]:checked');

        if (theStations.length === 0) {
            alert('You must select at least one Station');
            return false;
        } else if (theGroups.length === 0) {
            alert('You must select at least one Group');
            return false;
        } else {
            // Pass Edit Mode Only - See Station Tab
            if (jQuery('#rules-edit-mode').length) {
                var fields_passed = true;

                jQuery('.pass-field, .weight-field').each(function(i, ip) {
                    var fieldValue = jQuery(ip).val();

                    if (fieldValue.length === 0 || !(fieldValue >= 0 && fieldValue <= 100)) {
                        fields_passed = false;
                        jQuery(ip).addClass('incomplete-pass').off('keyup').on('keyup', function() {
                            jQuery(this).removeClass('incomplete-pass');
                        });
                    }
                });

                if (!fields_passed) {
                    alert('Station value must be between 0 and 100%');
                    return false;
                }
            }

            /* Delete results yes/no */
            if (deleting && !confirm("Are you sure you would like to delete results for the groups and stations selected?")) {
                jQuery('#cb-del').prop('checked', false);
                jQuery('#delete-box').css('display', '');
                jQuery('#delete-results').css('display', 'none');
                jQuery('#analysis_form').attr('action', 'manage.php?page=out_analysis');
                return false;
            }

            // Store Tree State
            var treeState = [];
            var treeElements = jQuery('#tddets .tree-opened');
            jQuery.each(treeElements, function(index, el) {
              treeState.push(jQuery(el).index())
            });

            // Window Scroll Position and tree State Array
            sessionStorage.setItem(
                'tree_state',
                JSON.stringify([jQuery(window).scrollTop(), treeState])
            );
            return true;
        }
  }

}

// Submit update form, called from detailed form results page
function submitUpdateForm(deleting) {
    if (ValidateForm(jQuery('#analysis_form'), deleting)) {
        jQuery('#analysis_form').submit();
    }
}

// Remove Iframe Wait
function removeIframeWait() {
    if (jQuery('#iframe-load').length) {
        jQuery('#iframe-load').addClass('hide');
    }
};
