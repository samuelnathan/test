/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 28/06/2019
 * © 2019 Qpercom Limited. All rights reserved.
 * @Manage Email Template Section
 */

jQuery(document).ready(function() {

  // If exists and is in edit mode
  if (jQuery('#main-content').length && jQuery('.temp-container').length && typeof (tinymce) !== "undefined") { 
        
     tinymce.init({
        mode: "exact",
        selector: "textarea#main-content",
        theme: "modern",
        width: "698",
        height: "220",
        indentation : '8pt',
        entity_encoding: "raw",
        contextmenu_never_use_native: true,
        elementpath: false,
        paste_as_text: true,
        menubar: false,
        default_link_target: "_blank",
        moxiemanager_image_settings: {
            moxiemanager_title: 'Image Folder'
          },
          external_plugins: {
           'moxiemanager': '../../../vendor/moxiemanager/moxiemanager/plugin.min.js'
          },
        plugins: [
           "advlist autolink lists link image charmap preview anchor pagebreak",
           "searchreplace visualblocks visualchars fullscreen",
           "insertdatetime nonbreaking save table contextmenu directionality",
           "paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "undo redo | styleselect | bold italic | " +
                  "alignleft aligncenter alignright | " +
                  "removeformat | bullist numlist outdent indent | " +
                  "link image | forecolor backcolor | table | preview",
        contextmenu: "copy paste removeformat | link image inserttable | cell row column deletetable"

    });
    
    tinymce.init({
        mode: "exact",
        selector: "textarea#instructions-content",
        theme: "modern",
        width: "698",
        height: "220",
        indentation : '8pt',
        entity_encoding: "raw",
        contextmenu_never_use_native: true,
        elementpath: false,
        paste_as_text: true,
        menubar: false,
        default_link_target: "_blank",
        moxiemanager_image_settings: {
            moxiemanager_title: 'Image Folder'
          },
          external_plugins: {
           'moxiemanager': '../../../vendor/moxiemanager/moxiemanager/plugin.min.js'
          },        
        plugins: [
           "advlist autolink lists link image charmap preview anchor pagebreak",
           "searchreplace visualblocks visualchars fullscreen",
           "insertdatetime nonbreaking save table contextmenu directionality",
           "paste textcolor colorpicker textpattern imagetools hr"
        ],
        toolbar1: "undo redo | styleselect | bold italic | " +
                  "alignleft aligncenter alignright | " +
                  "removeformat | bullist numlist outdent indent | " +
                  "link image | forecolor backcolor | table | preview | hr",
        contextmenu: "copy paste removeformat | link image inserttable | cell row column deletetable"

    });   
        
  }

    // Cancel template update event
    jQuery('#temp-cancel').on('click', function (e) {
        e.preventDefault();
        window.parent.getDialogObject().dialog('close');
    });

    // Instructions options group (hide instructions content if not needed)
    jQuery('#instructions-state input').on('change', function() {

        jQuery('#instruction-container').toggle(!(jQuery(this).val() == "no"));

    });
 
});

// Validate tool forms
function ValidateForm(form) {

    if (jQuery(form).attr('name') === 'temp_update_form') {

        // Update action
        if (jQuery('#temp-update').length) {

            tinymce.triggerSave();
            var itemContent = tinymce.activeEditor.getContent();
            var fieldsInvalid = (jQuery('#tempname').val().length === 0
                || jQuery('#subject').val().length === 0 || jQuery('#hello').val().length === 0);
             
            if (fieldsInvalid || itemContent.length === 0) {

                alert('Please complete all empty fields');

                return false;

            }

            var ccValid = (jQuery('#cc').val().length === 0 || isEmailValid((jQuery('#cc').val())));

            // Validate email address (optional field)
            if (!ccValid) {
               alert("The optional field: email cc can be left blank or contain a valid email address");
               return false; 
            }
            
        }

        return true;

    }

}