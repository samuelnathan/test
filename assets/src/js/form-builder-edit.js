/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 // edit buttons standard items
 function addEditEvent(button) {

   var parentTr = jQuery(button).parents('tr:first');
   var originalTr = parentTr.clone(true, true);
   var itemID = parentTr.find('input[id^=quest_cb]').val();
   var newTr = jQuery('<tr>', {'id': 'edit_qr'});
   var newTd = jQuery('<td>', {'id': 'edit_qc', 'colspan': '7', 'class': 'waittdw'});

   newTd.html('&nbsp;');
   newTr.prepend(newTd);

   parentTr.replaceWith(newTr);
   if (newTr.prev('tr')) {
     newTr.prev('tr').addClass('baseborder');
   }

   var selectors = "input, select, img[class=arrow], " +
     "img[class=editb], img[class=comment-img], a";
   jQuery('#assessform_sections_form')
    .find(selectors)
    .css('visibility', 'hidden');

     var url = "forms/ajx_edit_item.php?";
     var random = Math.random();

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            item_id: itemID,
            session_id: (jQuery('#sessionid_top').length ?
            jQuery('#sessionid_top').val() : "")
        }
     })
     .done(function(html){
        newTd.html(html);

        // Initiate tinymce for item description (if exists)
            if (jQuery('#item_text-' + itemID).length) {

            var textAreaID = jQuery('#item_text-' + itemID).attr('id');
            tinymce.execCommand('mceAddEditor', false, textAreaID);

        }

        // Initiate item competencies dialog
        loadCompetenciesDialog();

        newTd.attr('class', 'edit');

        // Include some required events
        setupEditButtons(originalTr, newTr, itemID);
        setupEditFlipEvent(itemID);
        previewItemTypes();
        flagsToggle();

        // Scroll to element
        jQuery('html, body').animate({
            scrollTop: (jQuery("#edit_qr").offset().top - 20)
        }, 800);
    })
     .fail(function(jqXHR) {
        newTr.replaceWith(originalTr);
        jQuery('#assessform_sections_form').find('input, select, img, a').css('visibility', 'visible');
        originalTr.find('a[id^=editq]').on('click', function(event) {
            event.preventDefault();
            addEditEvent(this);
        });
        alert('Request failed, please try again');
      });

 }

 // Edit section button
 function addSectionEditEvent(button) {

     var buttonParent = jQuery(button).parents('th:first');
     var sectionID = buttonParent.find('input[id^=section-ids]').val();
     var editdiv = jQuery('#editsectiondiv_' + sectionID);
     var orgHtml = editdiv.html();

     jQuery('#assessform_sections_form')
      .find('input, select, img, a')
      .css('visibility', 'hidden');

     var url = "forms/ajx_edit_section_details.php?";
     var random = Math.random();
     editdiv.empty().attr('class', 'edit-section-wait');

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            mode: 'edit',
            sid: random,
            id: sectionID,
            session_id: (jQuery('#sessionid_top').length ?
            jQuery('#sessionid_top').val() : "")
        }
     })
     .done(function(html){
        editdiv.attr('class', 'redborderv3');
        editdiv.html(html);

        sectionCancelEvent(
             jQuery('#editsectioncancel_'+sectionID),
             jQuery('#editsectionupdate_'+sectionID),
             editdiv,
             orgHtml
        );

        sectionUpdateEvent(
             jQuery('#editsectionupdate_'+sectionID),
             sectionID
        );

         // Scroll to element
         jQuery('html, body').animate({
             scrollTop: (editdiv.offset().top - 20)
         }, 800);

     })
     .fail(function(jqXHR) {

        editdiv.attr('class', 'fl');
        editdiv.html(orgHtml);
        jQuery('#assessform_sections_form')
          .find('input, select, img, a')
          .css('visibility', 'visible');
        alert('The request failed, Please try again');

      });

 }

 // Delete section
 function addSectionDeleteEvent(button) {

     var buttonParent = jQuery(button).parents('th:first');
     var sectionID = buttonParent.find('input[id^=section-ids]').val();

     var yesno = confirm("Are you sure you want to continue with deletion?");
     if (yesno) {

         var url = "forms/ajx_edit_section_details.php?";
         var random = Math.random();
         jQuery.ajax({
            url: url,
            type: "post",
            data: {
                mode: 'delete',
                sid: random,
                id: sectionID
            }
         })
         .done(function(){
            refreshPage(true);
         })
         .fail(function(jqXHR) {
            alert('The request failed, Please try again');
         });
     }

 }

 // Section Update Event
 function sectionUpdateEvent(button, sectionID) {

    jQuery(button).on('click', function() {

         if (jQuery('#compselectedit_' + sectionID).val().length === 0) {
             alert("Please choose a type");
             return;
         } else if (jQuery('#sectionorderedit_' + sectionID).val().length === 0) {
             alert("Please choose order");
             return;
         }

         jQuery('#section-button-div')
           .find('input[id^=editsectioncancel], input[id^=editsectionupdate]')
           .css('display', 'none');

         jQuery('#section-button-div')
           .attr('class', 'button-div-wait');

         var selfAssessment = +(jQuery('#selfassessment_' + sectionID).length
                   && jQuery('#selfassessment_' + sectionID).is(':checked'));
         var sectionFeedback = +jQuery('#sectionfeedback_' + sectionID).is(':checked');
         var itemFeedback = +jQuery('#itemfeedback_' + sectionID).is(':checked');
         var feedbackInternal = +jQuery('#feedbackinternal_' + sectionID).is(':checked');

         jQuery.ajax({
            url: "forms/ajx_edit_section_details.php?",
            type: "post",
            data: {
                mode: 'update',
                sid: Math.random(),
                id: sectionID,
                order: jQuery('#sectionorderedit_' + sectionID).val(),
                competence_selected: jQuery('#compselectedit_' + sectionID).val(),
                title_text: jQuery('#sectiondescedit_' + sectionID).val(),
                section_feedback: sectionFeedback,
                item_feedback: itemFeedback,
                feedback_required: jQuery('#feedbackrequired_' + sectionID).val(),
                feedback_internal: feedbackInternal,
                self_assessment: selfAssessment
            }
         })
         .done(function(html){
            refreshPage(true);
         })
         .fail(function(jqXHR) {
             jQuery('#section-button-div')
                  .find('input[id^=editsectioncancel], input[id^=editsectionupdate]')
                  .css('display', '');
            jQuery('#section-button-div').attr('class', 'group3-section-settings');
                  alert('The request failed, Please try again');
        });

     });

 }

 // Add section cancel event
 function sectionCancelEvent(cbu, ubu, editdiv, orgHtml) {
     jQuery(cbu).on('click', function() {
         jQuery(cbu).remove();
         jQuery(ubu).remove();
         jQuery(editdiv).attr('class', 'fl');
         jQuery(editdiv).html(orgHtml);
         jQuery('#assessform_sections_form')
                 .find('input, select, img, a')
                 .css('visibility', 'visible');
     });
 }

 /* function add Item To Section */
 function addItemToSection(button) {

     var id = jQuery(button).attr('id');
     var idArray = id.split('-');
     var sectionID = idArray[1];
     var itemTD = jQuery("#itemaddtd-" + sectionID);
     var cloneItem;
     var selectors = "input, select, img[class=arrow], img[class=editb], img[class=comment-img], a";

     jQuery('#assessform_sections_form').find(selectors).css('visibility', 'hidden');

     // If at least one Item to clone
     if (idArray[0] === 'clitem') {

       // Get clone details
       var itemToClone = jQuery("#clone_item-" + sectionID).val();
       cloneItem = 'yes';

     } else {

       cloneItem = 'no';

     }

     var url = "forms/ajx_add_item.php?";
     var random = Math.random();
     itemTD.html('&nbsp;').attr('class', 'waittdw2');
     if (itemTD.parents('tr:first').prev('tr')) {

         itemTD.parents('tr:first').prev('tr').addClass('baseborder');

     }

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            section_id: sectionID,
            clone_item: itemToClone,
            clone_item_yesno: cloneItem,
            session_id: (jQuery('#sessionid_top').length ?
               jQuery('#sessionid_top').val() : "")
        }
     })
     .done(function(html){
        itemTD.attr('class', 'section-buttons-edit');
        itemTD.html(html);

        previewItemTypes();
        setupAddFlipEvent(sectionID);

        // Initiate tinymce for item description (if exists)
        if (jQuery('#item_text-' + sectionID).length) {

            var textAreaID = jQuery('#item_text-' + sectionID).attr('id');
            tinymce.execCommand('mceAddEditor', false, textAreaID);
            tinymce.execCommand('mceFocus', false, textAreaID);

        }

        // Initiate item competencies dialog
        loadCompetenciesDialog();

        // Initiate flaf toggle
        flagsToggle();

        // Scroll to element
        jQuery('html, body').animate({
            scrollTop: (itemTD.offset().top - 20)
        }, 800);
     })
     .fail(function(jqXHR) {

        jQuery('#assessform_sections_form')
            .find('input, select, img, a')
            .css('visibility', 'visible');
        alert("The request failed, please try again");

      });
 }

 /**
  * Add Scale Item to Competence
  */
 function newScaleAddEvent() {

   // Add submit scale event
   if (jQuery('#create-scale-item').length) {
       jQuery('#create-scale-item').on('click', function() {

       // Get Section ID
       var sectionID = jQuery(this).parents('table:first')
                           .parents('tr:first')
                           .prev('tr')
                           .find('input[id^=section-ids]').val();

       var itemAddTD = jQuery("#sectiontd-" + sectionID).find("td[id='itemaddtd']");

       // Hide fields for editing
       jQuery('#assessform_sections_form').find("input, select, img[class=arrow], img[class=editb], img[class=comment-img], a")
                                   .css('visibility', 'hidden');
       // Send Request
       var url = "forms/ajx_add_scale_item.php?";
       var random = Math.random();
       itemAddTD.html('&nbsp;').attr('class', 'waittdw2');

       jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                section_id: sectionID
            }
        })
        .done(function(html){
            itemAddTD.attr('class', 'section-buttons-edit');
            itemAddTD.html(html);

            tinymce.execCommand('mceAddEditor', false, jQuery('#item_text').attr('id'));

            // Scale Flip Event
            scaleFlipEvent();

            // Add new scale Cancel event
            newScaleCancelEvent();

            // Add new scale Submit event
            newScaleSubmitEvent();

            // Scale Type Change
            changeScaleType();

            // Scroll to Element
            jQuery('html, body').animate({
                scrollTop: (itemAddTD.offset().top - 20)
            }, 800);

        })
        .fail(function(jqXHR) {

            jQuery('#assessform_sections_form').find('input, select, img, a')
                                           .css('visibility', 'visible');
               alert("The request failed, please try again");

        });
       });
    }

 }

   /**
  * Cancel event for adding a new scale Item to a Competence
  */
 function newScaleCancelEvent() {

 // Add submit scale event
 if (jQuery('#cancel_new_scale').length) {
     jQuery('#cancel_new_scale').on('click', function() {

     // Get Section ID
     var sectionID = jQuery(this).parents('table:first')
                         .parents('tr:first')
                         .prev('tr')
                         .find('input[id^=section-ids]').val();

     var itemAddTD = jQuery('#itemaddtd');
     var itemTextBoxID = jQuery('#item_text').attr('id');
         tinymce.execCommand('mceRemoveEditor', false, itemTextBoxID);

     var url = "forms/ajx_scale_buttons.php?";
     var random = Math.random();
         itemAddTD.html('&nbsp;')
                 .attr('class', 'waittdw2');

                 jQuery.ajax({
                    url: url,
                    type: "post",
                    data: {
                        sid: random,
                        section_id: sectionID
                    }
                 })
                 .done(function(html){
                    jQuery('#assessform_sections_form').find('input, select, img, a').css('visibility', 'visible');
                    itemAddTD.attr('class', 'section-buttons-td');
                    itemAddTD.html(html);
                    newScaleAddEvent();
                 })
                 .fail(function(jqXHR) {

                    alert('The request failed, please try refresh page [F5 key] and try again');

                  });

     });
 }
 }

 /*
 * Submit New Scale Item
 */
 function newScaleSubmitEvent() {

 // Add submit scale event
 if (jQuery('#submit_new_scale').length) {
     jQuery('#submit_new_scale').on('click', function() {

     // Blur Button
     jQuery(this).blur();

     // Get Section ID
     var sectionID = jQuery(this).parents('table:first')
                         .parents('tr:first')
                         .prev('tr')
                         .find('input[id^=section-ids]').val();

     if (!jQuery('#itemaddtd').length|| tinymce.activeEditor.getContent().length == 0) {
         alert("Incomplete fields, please review");
         return;
     }

     tinymce.triggerSave();
     var itemContent = tinymce.activeEditor.getContent();
     var error = false;

     var fieldValues = jQuery('#radiobuttonsdiv').find("input[id^=radio-]");
     var fieldDescriptions = jQuery('#radiobuttonsdiv').find("textarea[id^=radiotext-]");
     var scaleTypesChecked = jQuery('#scale-option-types').find("input[class=scale-types-radio]:checked");

     if (fieldValues.length === 0 && fieldDescriptions.length === 0) {
         alert("Incomplete fields, please review");
         return;
     }

     // No option selected then quit
     if (scaleTypesChecked.length === 0) {
         alert("Please select a Scale Type");
         return;
     }

     // Scale Values Validation
     var valueCount = 0;
     var scaleValues = [];
     jQuery.each(fieldValues, function(i, eachValue) {
         if (jQuery(eachValue).val().length === 0) {
             error = true;
         }
         scaleValues[valueCount] = jQuery(eachValue).val();
         valueCount++;
     });

     // Scale Description Validation
     var descCount = 0;
     var scaleDescriptions = [];
     jQuery.each(fieldDescriptions, function(i, eachDesc) {
         if (jQuery(eachDesc).val().length === 0) {
             error = true;
         }
         scaleDescriptions[descCount] = jQuery(eachDesc).val();
         descCount++;
     });

     if (error === true) {
         alert("Incomplete fields, please review");
     } else {
         jQuery('#buttonpanel').find('input').css('display', 'none');
         jQuery('#buttonpanel').attr('class', 'bpanwait');

         var random = Math.random();
         var url = "forms/submit_new_item_scale.php?";
         jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                section_id: sectionID,
                radiofar: scaleValues,
                radiofdescar: scaleDescriptions,
                item_text: itemContent,
                scale: scaleTypesChecked.eq(0).val()
            }
         })
         .done(function(html){
            refreshPage(true);
         })
         .fail(function(jqXHR) {

            jQuery('#buttonpanel').attr('class', 'bpan');
            jQuery('#buttonpanel').find('input').css('display', '');
            alert('The request failed, please try again');

          });

     }
 });
 }
 }

 /*
 * Update Scale Event
 * @param int itemID
 */
 function currentScaleUpdateItem(itemID) {

     jQuery('#update_scale_item').blur();

     if (tinymce.activeEditor.getContent().length == 0) {
         alert("Incomplete fields, please review");
         return;
     }

     tinymce.triggerSave();
     var itemContent = tinymce.activeEditor.getContent();
     var error = false;
     var fieldValues = jQuery('#radiobuttonsdiv').find("input[id^=radio]");
     var fieldDescriptions = jQuery('#radiobuttonsdiv').find("textarea[id^=radiotext]");
     var scaleTypesChecked = jQuery('#scale-option-types').find("input[class=scale-types-radio]:checked");

     if (fieldValues.length === 0 || fieldDescriptions.length === 0) {
         alert("Incomplete fields, please review");
         return;
     }

     // No option selected then quit
     if (scaleTypesChecked.length === 0) {
         alert("Please select a Scale Type");
         return;
     }

     // Scale Values Validation
     var valueCount = 0;
     var scaleValues = [];
     jQuery.each(fieldValues, function(i, eachValue) {
         if (jQuery(eachValue).val().length === 0) {
             error = true;
         }
         scaleValues[valueCount] = jQuery(eachValue).val();
         valueCount++;
     });

     // Scale Description Validation
     var descCount = 0;
     var scaleDescriptions = [];
     jQuery.each(fieldDescriptions, function(eachDesc) {
         if (jQuery(eachDesc).val().length === 0) {
             error = true;
         }
         scaleDescriptions[descCount] = jQuery(eachDesc).val();
         descCount++;
     });


     if (error === true) {
         alert("Some fields in this section are incomplete");
     } else {
         var random = Math.random();
         var url = "forms/update_scale_item.php?";
         jQuery('#buttonpanel').find('input').css('display', 'none');
         jQuery('#buttonpanel').attr('class', 'bpanwait');

         jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                item_id: itemID,
                item_text: itemContent,
                radfvals: scaleValues,
                radfdesc: scaleDescriptions,
                scale: scaleTypesChecked.eq(0).val()
            }
         })
         .done(function(){
            refreshPage(true);
         })
         .fail(function(jqXHR) {

            jQuery('#buttonpanel').attr('class', 'bpan');
            jQuery('#buttonpanel').find('input').css('display', '');
            alert('The request failed, please try again');

          });
     }
 }

 /*
 * Edit buttons scale items
 */
 function currentScaleEditEvent(editButton) {

     var parentRow = jQuery(editButton).parents('tr:first');
     var originalRow = parentRow.clone(true, true);
     var itemID = parentRow.find('input[id^=quest_cb]').val();

     // Edit Row
     var editRow = jQuery('<tr>', {'id': 'edit_qr'});

     // Edit Cell
     var editCell = jQuery('<td>', {'id': 'edit_qc', 'colspan': '7', 'class': 'waittdw'});

     // Replace Parent row with Edit row
     editCell.html('&nbsp;');
     editRow.prepend(editCell);
     parentRow.replaceWith(editRow);

     var selectors = "input, select, img[class=arrow], img[class=editb], img[class=comment-img], a";
     jQuery('#assessform_sections_form').find(selectors)
                                 .css('visibility', 'hidden');

     var url = "forms/ajx_edit_scale_item.php?";
     var random = Math.random();

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            item_id: itemID
        }
     })
     .done(function(html){

             // Add Scale Content to Edit
             editCell.html(html);
             tinymce.execCommand('mceAddEditor', false, jQuery('#item_text').attr('id'));
             editCell.attr('class', 'edit');

             // Setup Scale Edit Buttons
             scaleEditButtonEvents(originalRow, editRow, itemID);

             // Scale Flip Event
             scaleFlipEvent();

             // Preview Item Types Events
             previewItemTypes();

             // Scale Type Change
             changeScaleTypeEdit();

             // Scroll to Element
             jQuery('html, body').animate({
                scrollTop: (jQuery("#itemeditdiv").offset().top - 20)
            }, 800);
     })
     .fail(function(jqXHR) {
            editRow.replaceWith(originalRow);
             jQuery('#assessform_sections_form').find('input, select, img, a').css('visibility', 'visible');
             originalRow.find('a[id^=editq]').on('click', function(e) {
                 e.preventDefault();
                 currentScaleEditEvent(this);
             });
             alert('Request failed, please try again');

      });
 }

 /*
 * Change scale type event - editing scale item section
 */
 function changeScaleType() {

     // Relay change events from parent element 'itemadddiv'
     jQuery('#scale-option-types').on('click', 'input.scale-types-radio', function() {

     // Get Section ID
     var sectionID = jQuery(this).parents('table:first')
                         .parents('tr:first')
                         .prev('tr')
                         .find('input[id^=section-ids]').val();


     var scaleType = jQuery(this).val();
     var random = Math.random();
     var url = "forms/scale_type.php?";

     jQuery('#qansoptions').empty().attr('class', 'ansoptcdivwait');

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isajaxcall: 'yes',
            section_id: sectionID,
            type: scaleType
        }
     })
     .done(function(html){
        jQuery('#qansoptions').attr('class', 'ansoptioncreatediv');
             jQuery('#qansoptions').html(html);

             // Scale Flip Event
             scaleFlipEvent();
     })
     .fail(function(jqXHR) {

        jQuery('#qansoptions').attr('class', 'ansoptioncreatediv');
        alert('The request failed, please try again');

      });

     });
 }

 /*
 * Change scale type event - editing scale item section
 */
 function changeScaleTypeEdit() {

     // Relay change events from parent element 'itemadddiv'
     jQuery('#scale-option-types').on('click', 'input.scale-types-radio', function() {

     var scaleType = jQuery(this).val();
     var itemID = jQuery('#scale-item-id').val();
     var qansoptions = jQuery('#qansoptions');
     var random = Math.random();
     var url = "forms/edit_scale_type.php?";

     qansoptions.empty().attr('class', 'ansoptcdivwait');
     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isajaxcall: 'yes',
            item_id: itemID,
            type: scaleType
        }
     })
     .done(function(html){
        qansoptions.attr('class', 'ansoptioncreatediv');
             qansoptions.html(html);

             // Scale Flip Event
             scaleFlipEvent();
     })
     .fail(function(jqXHR) {

        qansoptions.attr('class', 'ansoptioncreatediv');
             alert('The request failed, please try again');

      });

     });

 }

 // function to cancel add Item to Competence
 function canceladdItemToSection(cancelbutton) {
     var id = jQuery(cancelbutton).attr('id');
     var idArray = id.split('-');
     var itemaddtd = jQuery("#itemaddtd-" + idArray[1]);

     if (jQuery('#item_text-' + idArray[1]).length) {

         var textAreaID = jQuery('#item_text-' + idArray[1]).attr('id');
         tinymce.execCommand('mceRemoveEditor', false, textAreaID);

     }

     removeCompetenciesDialog();

     var url = "forms/ajx_section_buttons.php?";
     var random = Math.random();
     itemaddtd.html('&nbsp;').attr('class', 'waittdw2');

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            section_id: idArray[1]
        }
     })
     .done(function(html){
        jQuery('#assessform_sections_form').find('input, select, img, a').css('visibility', 'visible');
        itemaddtd.attr('class', 'section-buttons-td');
        itemaddtd.html(html);

        if (itemaddtd.parents('tr:first').prev('tr').length) {
            itemaddtd.parents('tr:first').prev('tr').removeClass('baseborder');
        }
     })
     .fail(function(jqXHR) {

        alert('The request failed, please try refresh page [F5 key] and try again');

      });
 }

 // Change Item Type
 function changeItemType(input) {
     var id = jQuery(input).attr('id');
     var idArray = id.split('-');
     var sectionID = idArray[1];
     var type = idArray[0];
     var qansoptions = jQuery('#qansoptions-' + sectionID);
     var random = Math.random();

     var url;
     switch (type) {
         case 'item_type_radio':
             url = 'forms/add_radio_options.php?';
             break;
         case 'item_type_text':
             url = 'forms/add_textbox_option.php?';
             break;
         case 'item_type_slider':
             url = 'forms/add_slider_option.php?';
             break;
         case 'item_type_checkbox':
             url = 'forms/checkbox_option_add.php?';
             break;
     }

     qansoptions.empty().attr('class', 'ansoptcdivwait');

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isajaxcall: 'yes',
            section_id: sectionID
        }
     })
     .done(function(html){
        qansoptions.attr('class', 'ansoptioncreatediv');
             qansoptions.html(html);
             setupAddFlipEvent(sectionID);
             flagsToggle();
     })
     .fail(function(jqXHR) {

        qansoptions.attr('class', 'ansoptioncreatediv');
        alert('The request failed, please try again');

      });
 }

 // Change Item Type Edit
 function changeItemTypeEdit(input) {
     var id = jQuery(input).attr('id');
     var idArray = id.split('-');
     var item_id = idArray[1];
     var type = idArray[0];
     var random = Math.random();
     var qansoptions = jQuery('#qansoptions-' + item_id);
     var url;

     qansoptions.empty().attr('class', 'ansoptcdivwait');

     switch (type) {
         case 'item_type_radio':
             url = 'forms/edit_radio_options.php?';
             break;
         case 'item_type_text':
             url = 'forms/edit_textbox_option.php?';
             break;
         case 'item_type_slider':
             url = 'forms/edit_slider_option.php?';
             break;
         case 'item_type_checkbox':
             url = 'forms/checkbox_option_edit.php?';
             break;
     }

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isajaxcall: 'yes',
            item_id: item_id
        }
     })
     .done(function(html){
        qansoptions.attr('class', 'ansoptioncreatediv');
        qansoptions.html(html);
        setupEditFlipEvent(item_id);
        flagsToggle();
     })
     .fail(function(jqXHR) {

        qansoptions.attr('class', 'ansoptioncreatediv');
        alert('The request failed, please try again');

      });
 }

 // generate radio buttons in osce item creation
 function renderRadioOptions(textfieldobject) {
     var id = jQuery(textfieldobject).attr('id');
     idArray = id.split('-');
     var sectionID = idArray[1];
     var radiobuttonsdiv = jQuery('#radiobuttonsdiv-' + sectionID);

     if (isNaN(jQuery(textfieldobject).val())) {
         jQuery(textfieldobject).val('');
         radiobuttonsdiv.html('Non-Numeric Value entered.<br/><br/>');
         radiobuttonsdiv.addClass('boldred');
     } else {
         if (radiobuttonsdiv.hasClass('boldred')) {
             radiobuttonsdiv.removeClass('boldred');
         }

         var url = "forms/ajx_add_radio_fields.php?";
         var random = Math.random();
         radiobuttonsdiv.empty().attr('class', 'radcontwait');

         jQuery.ajax({
            url: url,
            type: "post",
            data: {
             sid: random,
             isajaxcall: 'yes',
             quantity: jQuery(textfieldobject).val(),
             section_id: sectionID
            }
         })
         .done(function(html){
            radiobuttonsdiv.attr('class', 'radcont').html(html);
            flagsToggle();
         })
         .fail(function(jqXHR) {
            radiobuttonsdiv.attr('class', 'radcont');
            alert('The request failed, please try again');
          });
     }
 }

 // Generate Radio Option in the editing section
 function renderRadioOptionsEdit(tf) {
     var id = jQuery(tf).attr('id');
     idArray = id.split('-');
     var item_id = idArray[1];
     var radiobuttonsdiv = jQuery('#radiobuttonsdiv-' + item_id);

     if (isNaN(jQuery(tf).val())) {
         jQuery(tf).val('');
         radiobuttonsdiv.html('Non-Numeric Value entered.<br/><br/>');
         radiobuttonsdiv.addClass('boldred');
     } else {
         if (radiobuttonsdiv.hasClass('boldred')) {
             radiobuttonsdiv.removeClass('boldred');
         }

         var url = "forms/ajx_edit_radio_fields.php?";
         var random = Math.random();
         radiobuttonsdiv.empty().attr('class', 'radcontwait');

         jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isajaxcall: 'yes',
                item_id: item_id,
                quantity: jQuery(tf).val()
            }
         })
         .done(function(html){
            radiobuttonsdiv.attr('class', 'radcont').html(html);
                 flagsToggle();
         })
         .fail(function(jqXHR) {
            radiobuttonsdiv.attr('class', 'radcont');
            alert('The request failed, please try again');
        });
     }
 }

 // Delete Section Items
 function deleteSectionItems(button) {
     var id = jQuery(button).attr('id');
     var idArray = id.split('-');
     var sectionID = idArray[1];
     var sectiontd = jQuery('#sectiontd-' + sectionID);
     var cbs = sectiontd.find("input[id^=quest_cb-" + sectionID + "]");

     var itemcount = 0;
     var url = "forms/delete_items.php?";
     var random = Math.random();

     var to_del = [];
     jQuery.each(cbs, function(i, cb) {
         if (jQuery(cb).prop('checked')) {
             to_del[itemcount] = jQuery(cb).val();
             itemcount++;
         }
     });

     if (itemcount > 0) {
         if (confirm("Are you sure you want to delete the selected item(s)?")) {
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    section_id: sectionID,
                    to_del: to_del
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                alert('The request failed, please try again');
             });
         }
     }
     else {
         alert("You have not selected an Item");
     }
 }
//validate number entry for applying item weightings
 function hasNumber(event) {
    var charCode = (event.which) ? event.which : event.keyCode
	    if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))){
       return false;
	    }
    return true;
 }
 //dynamically show or hide input for weightings
 function weightVal() {
     var checkBox = jQuery('#weightcb');
     var valLbl = jQuery('#weighValLbl');
     var weighVal = jQuery('#itemWeightVal');
     // If the checkbox is checked, display the weighting input box
     if(checkBox.prop('checked') == true){
	 weighVal.show();
	 valLbl.show();
     }else {
	 valLbl.hide();
	 weighVal.hide();
     }

   }

/**
 * Validate that the specified item weighting is within correct boundaries. Gets the boundaries from
 * the attributes of the input and returns the parsed value if it is valid. If the value is
 * not valid, returns null
 *
 * @returns {number|null}
 */
function validateItemWeight() {
    var weightedValueTxt = jQuery('#itemWeightVal');
    var weighted_value = parseFloat(weightedValueTxt.val());

    var weightMin = parseFloat(weightedValueTxt.attr("min"));
    var weightMax = parseFloat(weightedValueTxt.attr("max"));

    if (weighted_value > weightMax || weighted_value < weightMin) {
        alert("Incorrect Weightings set. Value must be within " + weightMin + " and " + weightMax + " range. Please review. Default will be set to 1.");
        weightedValueTxt.val("1.00");
        return null;
    }

    return weighted_value;
}

 // Submit new item
 function submitNewItem(button) {

     jQuery(button).blur();
     var id = jQuery(button).attr('id');
     var idArray = id.split('-');
     var sectionID = idArray[1];
     var random = Math.random();
     var url, type, contentYes, selfAssessment, itemContent;
     var orderInvalid = (!jQuery('#item_order-' + sectionID).length || jQuery('#item_order-' + sectionID).val().length === 0);

     // Self assessment item
     if (jQuery('#self-assess-' + sectionID).length) {

         contentYes = (jQuery('#self-assess-' + sectionID).text().length > 0);
         selfAssessment = true;

     // Standard item
     } else {

         contentYes = (tinymce.activeEditor.getContent().length > 0);
         selfAssessment = false;

     }

     if (orderInvalid || !contentYes) {

         alert("Incomplete fields, please review");
         return;

     }

     // Self Assessment content
     if (selfAssessment) {

         itemContent = "<p>No self assessment question data</p>";

     // Get tinymce content (standard item)
     } else {

         tinymce.triggerSave();
         itemContent = tinymce.activeEditor.getContent();

     }

     // Get item competencies if any selected
     var itemCompetencies = [];
     var competencyCount = 0;
     jQuery('.selected-competencies').each(function() {
         var competency = jQuery(this);
         itemCompetencies[competencyCount] = competency.val();
         competencyCount++;
     });


     /**
      * Range Slider or Enter Number (textbox) type
      */
     if (jQuery('#item_type_text-' + sectionID).prop('checked') || jQuery('#item_type_slider-' + sectionID).prop('checked')) {
         var weighted_value = validateItemWeight();
         if (weighted_value !== null) {
	 if (jQuery('#descriptor-' + sectionID).length && jQuery('#text_rangeone-' + sectionID) && jQuery('#text_rangetwo-' + sectionID)) {
             var descriptor = jQuery('#descriptor-' + sectionID);
             var text_rangeone = jQuery('#text_rangeone-' + sectionID);
             var text_rangetwo = jQuery('#text_rangetwo-' + sectionID);

         } else {
             alert("Incomplete fields, please review");
             return;
         }

         if (text_rangeone.val().length === 0 || text_rangetwo.val().length === 0) {
             alert("Incomplete fields, please review");
         } else {
             if (jQuery('#item_type_text-' + sectionID).prop('checked')) {
                 type = "score_entry";
             } else {
                 type = "slider_range";
             }
             jQuery('#buttonpanel-' + sectionID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + sectionID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: 'forms/submit_new_item.php',
                type: "post",
                data: {
                    sid: random,
                    section_id: sectionID,
                    item_type: type,
                    item_text: itemContent,
                    item_order: jQuery('#item_order-' + sectionID).val(),
                    descriptor: descriptor.val(),
                    text_rangeone: text_rangeone.val(),
                    text_rangetwo: text_rangetwo.val(),
                    weighted_value: weighted_value,
                    competencies: itemCompetencies
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + sectionID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + sectionID).find('input').css('display', '');
                alert('The request failed, please try again');
             });
         }
         }
     /**
      * Question (checkbox option)
      */
     } else if (jQuery('#item_type_checkbox-' + sectionID).prop('checked')) {

         if (jQuery('#checkbox-unchecked-' + sectionID).length && jQuery('#checkbox-checked-' + sectionID)) {

             var descriptor = jQuery('#checkbox-descriptor-' + sectionID);
             var unchecked = jQuery('#checkbox-unchecked-' + sectionID);
             var checked = jQuery('#checkbox-checked-' + sectionID);

         } else {
             alert("Incomplete fields, please review");
             return;
         }

         if (unchecked.val().length === 0 || checked.val().length === 0) {

             alert("Incomplete fields, please review");

         } else {

             jQuery('#buttonpanel-' + sectionID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + sectionID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: 'forms/checkbox_option_save.php',
                type: "post",
                data: {
                    id: sectionID,
                    sid: random,
                    text: itemContent,
                    order: jQuery('#item_order-' + sectionID).val(),
                    descriptor: descriptor.val(),
                    unchecked: unchecked.val(),
                    checked: checked.val(),
                    competencies: itemCompetencies,
                    mode: "add"
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + sectionID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + sectionID).find('input').css('display', '');
                alert('The request failed, please try again');
             });

         }

     /**
      * Choose from List item type (radio buttons)
      */
     } else if (jQuery('#item_type_radio-' + sectionID).prop('checked')) {
         var error = false;
         if (jQuery('#radiobuttonsdiv-' + sectionID).length && jQuery('#numradiobuttons-' + sectionID)
             && jQuery('#numradiobuttons-' + sectionID).val().length > 0 && jQuery('#numradiobuttons-' + sectionID).val() > 0) {
             var radiobuttonsdiv = jQuery('#radiobuttonsdiv-' + sectionID);
         } else {
             alert("Incomplete fields, please review");
             return;
         }

         var radiofieldvals = radiobuttonsdiv.find("input[id^=radio-" + sectionID + "]");
         var radiofielddescs = radiobuttonsdiv.find("textarea[id^=radiotext-" + sectionID + "]");
         var radiofieldflags = radiobuttonsdiv.find("input[id^=flag-" + sectionID + "]");

         if (radiofieldvals.length === 0 && radiofielddescs.length === 0) {
             alert("Incomplete fields, please review");
             return;
         }

         var isNumber = jQuery('#isnumval-' + sectionID).prop('checked');
         var containschar = false;
         url = "forms/submit_new_item_radio.php?";

         // Radio Values
         var radiofieldcount = 0;
         var radiofar = [];
         jQuery.each(radiofieldvals, function(i, el) {
             if (jQuery(el).val().length === 0) {
                 error = true;
             }
             if (isNumber && !isFinite(jQuery(el).val())) {
                 containschar = true;
             }
             radiofar[radiofieldcount] = jQuery(el).val();
             radiofieldcount++;
         });

         if (containschar) {
             alert("Numeric (Score) Values Only");
             return;
         } else {
             isNumber = (isNumber === true) ? 1 : 0;
         }

         // Radio Descriptors
         var descfieldcount = 0;
         var radiofdescar = [];
         jQuery.each(radiofielddescs, function(i, ele) {
             if (jQuery(ele).val().length === 0) {
                 error = true;
             }
             radiofdescar[descfieldcount] = jQuery(ele).val();
             descfieldcount++;
         });

         // Radio Flags
         var radioFlagCount = 0;
         var radioFlags = [];
         jQuery.each(radiofieldflags, function(i, flagField) {
         radioFlags[radioFlagCount] = jQuery(flagField).val();
         radioFlagCount++;
         });

       //item weightings
         var weighted_value = validateItemWeight();
         if (weighted_value !== null) {
         if (error) {
             alert("Incomplete fields, please review");
         } else {
             jQuery('#buttonpanel-' + sectionID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + sectionID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    section_id: sectionID,
                    isnumval: isNumber,
                    radiofar: radiofar,
                    radiofdescar: radiofdescar,
                    radio_flags: radioFlags,
                    item_order: jQuery('#item_order-' + sectionID).val(),
                    item_text: itemContent,
                    weighted_value: weighted_value,
                    competencies: itemCompetencies
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + sectionID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + sectionID).find('input').css('display', '');
                alert('The request failed, please try again');
             });
         }
     }
     }
 }


 /*
 * Preview Item Types - Hover Image Tip
 */
 function previewItemTypes() {

    jQuery( '.hoverimage' ).tooltip({
        placement: "left",
        track: false,
        classes: {
            "ui-tooltip": "imagetips"
          }
      });

 }

 // Setup Edit Buttons
 function setupEditButtons(or, etr, item_id) {

     if (jQuery('#cancel_edit_item').length) {

         jQuery('#cancel_edit_item').on('click', function() {

             // Initiate tinymce for item description (if exists)
             if (jQuery('#item_text-' + item_id).length) {

                 var textAreaID = jQuery('#item_text-' + item_id).attr('id');
                 tinymce.execCommand('mceFocus', false, textAreaID);
                 tinymce.execCommand('mceRemoveEditor', false, textAreaID);

             }

             removeCompetenciesDialog();

             if (etr.prev('tr')) {

                 etr.prev('tr').removeClass('baseborder');

             }
             etr.replaceWith(or);
             etr.remove();

             // display all buttons
             jQuery('#assessform_sections_form').find('input, select, img, a').css('visibility', 'visible');

             //reset edit event
             or.find('a[class=standard]').on('click', function(e) {

                 e.preventDefault();
                 addEditEvent(this);

             });
         });

         jQuery('#update_item').on('click', function(e) {

             e.preventDefault();
             updateItem(item_id);

         });
     }
 }

 /*
 * Scale Editing Buttons
 * @param html Object originalRow
 * @param html Object editingRow
 * @param int itemID
 */
 function scaleEditButtonEvents(originalRow, editingRow, itemID) {

     // Cancel update scale item event
     jQuery('#cancel_update_scale_item').on('click', function() {

     var textboxID = jQuery('#item_text').attr('id');
     tinymce.execCommand('mceFocus', false, textboxID);
     tinymce.execCommand('mceRemoveEditor', false, textboxID);
     editingRow.replaceWith(originalRow);
     editingRow.remove();

     // Display all buttons
     jQuery('#assessform_sections_form').find('input, select, img, a')
                                     .css('visibility', 'visible');

     // Reset edit event
     originalRow.find('a[class=scale]').on('click', function(e) {
         e.preventDefault();
         currentScaleEditEvent(this);
     });

     });

     // Update scale item event
     jQuery('#update_scale_item').on('click', function(e) {
     e.preventDefault();
     currentScaleUpdateItem(itemID);
     });

 }

 // Flip Item Options 'Edit' Event
 function setupEditFlipEvent(item_id) {

     var i;
     if (jQuery("#flip-" + item_id).length) {

         jQuery("#flip-" + item_id).on('click', function() {
             var rfdvals = jQuery("#radiobuttonsdiv-" + item_id).find("input[id^=radio-" + item_id + "]");
             var newrdvals = [];
             var newrdescs = [];
             var vcnt = rfdvals.length;
             var orgvcnt = vcnt;

             if (vcnt > 0) {
                 jQuery.each(rfdvals, function(i, ele) {
                     var splar = ele.attr('id').split('-');
                     newrdvals[orgvcnt] = ele.val();
                     newrdescs[orgvcnt] = jQuery('#radiotext-' + item_id + "-" + splar[2]).val();
                     orgvcnt--;
                 });

                 for (i = 1; i <= vcnt; i++) {
                     jQuery('#radio-' + item_id + "-" + i).val(newrdvals[i]);
                     jQuery('#radiotext-' + item_id + "-" + i).val(newrdescs[i]);
                 }
             }
         });

     }

 }

 // Flip Item Options 'Add' Event
 function setupAddFlipEvent(sectionID) {

     var i;

     if (jQuery("#flip-" + sectionID).length) {

         jQuery("#flip-" + sectionID).on('click', function() {
             var rfdvals = jQuery("#radiobuttonsdiv-" + sectionID).find("input[id^=radio-" + sectionID + "]");
             var newrdvals = [];
             var newrdescs = [];
             var vcnt = rfdvals.length;
             var orgvcnt = vcnt;

             if (vcnt > 0) {
                 jQuery.each(rfdvals, function(i, ele) {
                     var splar = jQuery(ele).attr('id').split('-');
                     newrdvals[orgvcnt] = jQuery(ele).val();
                     newrdescs[orgvcnt] = jQuery('#radiotext-' + sectionID + "-" + splar[2]).val();
                     orgvcnt--;
                 });

                 for (i = 1; i <= vcnt; i++) {
                     jQuery('#radio-' + sectionID + "-" + i).val(newrdvals[i]);
                     jQuery('#radiotext-' + sectionID + "-" + i).val(newrdescs[i]);
                 }
             }
         });

     }

 }

 /**
  * Flip Event for Editing and Adding Scale Items
  */
 function scaleFlipEvent() {

   if (jQuery('#flip').length) {

        jQuery('#flip').on('click', function() {

        var radioFields = jQuery('#radiobuttonsdiv').find("input[id^=radio-]");
        var newValues = [];
        var newDescriptions = [];
        var fieldCount = radioFields.length;
        var fieldIndex = fieldCount;

        if (fieldCount > 0) {

            // Grab old values and descriptions
            jQuery.each(radioFields, function(i, radioField) {
            var splitID = radioField.attr('id').split('-');
            var radioTextID = splitID[1];
                newValues[fieldIndex] = radioField.val();
                newDescriptions[fieldIndex] = jQuery('#radiotext-' + radioTextID).val();
                fieldIndex--;
            });

            // Set new values and descriptions
            for (var i=1; i<=fieldCount; i++) {
                jQuery('#radio-'+i).val(newValues[i]);
                jQuery('#radiotext-'+i).val(newDescriptions[i]);
            }

        }

        });

   }

 }
 // Update section item
 function updateItem(itemID) {

     jQuery('#update_item').blur();
     var random = Math.random();
     var url, type, selfAssessment, contentYes, itemContent;
     var orderInvalid = (!jQuery('#item_order-' + itemID).length || jQuery('#item_order-' + itemID).val().length === 0);

     // Self assessment item
     if (jQuery('#self-assess-' + itemID).length) {

         contentYes = (jQuery('#self-assess-' + itemID).text().length > 0);
         selfAssessment = true;

     // Standard item
     } else {

         contentYes = (tinymce.activeEditor.getContent().length > 0);
         selfAssessment = false;

     }

     if (orderInvalid || !contentYes) {

         alert("Incomplete fields, please review");
         return;

     }

     // Self Assessment content
     if (selfAssessment) {

         itemContent = "<p>No self assessment question data</p>";

     // Get tinymce content (standard item)
     } else {

         tinymce.triggerSave();
         itemContent = tinymce.activeEditor.getContent();

     }

     var item_order = jQuery('#item_order-' + itemID);

     /**
      * Get item competencies (if any selected)
      */
     var itemCompetencies = [];
     var competencyCount = 0;
     jQuery('.selected-competencies').each(function() {
         var competency = jQuery(this);
         itemCompetencies[competencyCount] = competency.val();
         competencyCount++;
     });

     // Value entry or Range slider
     if (jQuery('#item_type_text-' + itemID).prop('checked') || jQuery('#item_type_slider-' + itemID).prop('checked')) {
         var weighted_value = validateItemWeight();
         if (weighted_value !== null) {
         if (jQuery('#descriptor-' + itemID).length && jQuery('#text_rangeone-' + itemID) && jQuery('#text_rangetwo-' + itemID)) {
             var descriptor = jQuery('#descriptor-' + itemID);
             var text_rangeone = jQuery('#text_rangeone-' + itemID);
             var text_rangetwo = jQuery('#text_rangetwo-' + itemID);


         } else {
             alert("Incomplete fields, please review");
             return;
         }

         if (text_rangeone.val().length === 0 || text_rangetwo.val().length === 0) {
             alert("Some fields in this section are incomplete");
         } else {
             if (jQuery('#item_type_slider-' + itemID).prop('checked')) {
                 type = "slider_range";
             }
             else {
                 type = "value_input";
             }

             jQuery('#buttonpanel-' + itemID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + itemID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: 'forms/update_item.php',
                type: "post",
                data: {
                    sid: random,
                    item_id: itemID,
                    item_type: type,
                    item_order: item_order.val(),
                    item_text: itemContent,
                    descriptor: descriptor.val(),
                    text_rangeone: text_rangeone.val(),
                    text_rangetwo: text_rangetwo.val(),
                    weighted_value: weighted_value,
                    competencies: itemCompetencies
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + itemID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + itemID).find('input').css('display', '');
                alert('The request failed, please try again');
             });
         }
         }
     /**
      * Question (checkbox option)
      */
     } else if (jQuery('#item_type_checkbox-' + itemID).prop('checked')) {

         if (jQuery('#checkbox-unchecked-' + itemID).length && jQuery('#checkbox-checked-' + itemID)) {

             var descriptor = jQuery('#checkbox-descriptor-' + itemID);
             var unchecked = jQuery('#checkbox-unchecked-' + itemID);
             var checked = jQuery('#checkbox-checked-' + itemID);

         } else {
             alert("Incomplete fields, please review");
             return;
         }

         if (unchecked.val().length === 0 || checked.val().length === 0) {

             alert("Incomplete fields, please review");

         } else {

             jQuery('#buttonpanel-' + itemID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + itemID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: "forms/checkbox_option_save.php",
                type: "post",
                data: {
                    id: itemID,
                    sid: random,
                    text: itemContent,
                    order: item_order.val(),
                    descriptor: descriptor.val(),
                    unchecked: unchecked.val(),
                    checked: checked.val(),
                    competencies: itemCompetencies,
                    mode: "update"
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + itemID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + itemID).find('input').css('display', '');
                alert('The request failed, please try again');
             });

         }

     // Choose from list (radio) option
     } else if (jQuery('#item_type_radio-' + itemID).prop('checked')) {
         var error = false;
         //few pre checks
         if (jQuery('#radiobuttonsdiv-' + itemID).length && jQuery('#numradiobuttons-' + itemID) && jQuery('#numradiobuttons-' + itemID).val() > 0 && jQuery('#isnumval-' + itemID)) {
             var radiobuttonsdiv = jQuery('#radiobuttonsdiv-' + itemID);
             var radiofieldvals = radiobuttonsdiv.find("input[id^=radio-" + itemID + "]");
             var radiofielddescs = radiobuttonsdiv.find("textarea[id^=radiotext-" + itemID + "]");
             var radiofieldflags = radiobuttonsdiv.find("input[id^=flag-" + itemID + "]");

             var isNumber = jQuery('#isnumval-' + itemID).prop('checked');
             if (radiofieldvals.length === 0 || radiofielddescs.length === 0) {
                 alert("Incomplete fields, please review");
                 return;
             }
         } else {
             alert("Incomplete fields, please review");
             return;
         }

         var containschar = false;

         // Radio values
         var radiofieldcount = 0;
         var radiofar = [];
         jQuery.each(radiofieldvals, function(i, el) {
             if (jQuery(el).val().length === 0) {
                 error = true;
             }

             if (isNumber === true && !isFinite(jQuery(el).val())) {
                 containschar = true;
             }

             radiofar[radiofieldcount] = jQuery(el).val();
             radiofieldcount++;
         });

         if (containschar === true) {
             alert("Numeric (Score) Values Only");
             return;
         } else {
             isNumber = (isNumber === true) ? 1 : 0;
         }

         // Description values
         var descfieldcount = 0;
         var radiofdescar = [];
         jQuery.each(radiofielddescs, function(i, ele) {
             if (jQuery(ele).val().length === 0) {
                 error = true;
             }
             radiofdescar[descfieldcount] = jQuery(ele).val();
             descfieldcount++;
         });

         // Radio Flags
         var radioFlagCount = 0;
         var radioFlags = [];
         jQuery.each(radiofieldflags, function(i, flagField) {
            radioFlags[radioFlagCount] = jQuery(flagField).val();
            radioFlagCount++;
         });
         //item weightings
         var weighted_value = validateItemWeight();
         if (weighted_value !== null) {

         if (error === true) {
             alert("Incomplete fields, please review");

         } else {
             url = "forms/update_radio_item.php";
             jQuery('#buttonpanel-' + itemID).find('input').css('display', 'none');
             jQuery('#buttonpanel-' + itemID).attr('class', 'bpanwait');

             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    item_id: itemID,
                    isnumval: isNumber,
                    item_order: item_order.val(),
                    item_text: itemContent,
                    radfvals: radiofar,
                    radfdesc: radiofdescar,
                    radio_flags: radioFlags,
                    weighted_value: weighted_value,
                    competencies: itemCompetencies
                }
             })
             .done(function(html){
                refreshPage(true);
             })
             .fail(function(jqXHR) {
                jQuery('#buttonpanel-' + itemID).attr('class', 'bpan');
                jQuery('#buttonpanel-' + itemID).find('input').css('display', '');
                alert('The request failed, please try again');
             });
         }
     }
         }
}


