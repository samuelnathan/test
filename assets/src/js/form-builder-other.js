/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */
  
 /**
  * Flag toggle
 */
 function flagsToggle() {
     jQuery('.flag').click(function() {
     var flagOption = jQuery(this).nextAll('.option-flag');
     if (jQuery(this).hasClass('flag-off')) {
        jQuery(this).removeClass('flag-off');
         flagOption.val(1);
     } else {
        jQuery(this).addClass('flag-off');
         flagOption.val(0);
     }
     });
 }
 
 /**
  * Save competencies
  */
 function saveCompetencies() {
     
     // Get competency container
     var competencyContainer = jQuery('#container-competencies');
     
     // Clear current competencies
     competencyContainer.empty();
     
     // Add new ones
     jQuery('input.level-one-check:checked').each(function() {
     var checkedCB = jQuery(this);
     var labelContent = checkedCB.next('label').text();
     
     // Background colour for each role
     var colorHex = checkedCB.next().next("input[class='colour-code']").val();
             
     // Competency span
     var levelOneSpan = jQuery('<span>', {
             'text': labelContent,
             'class': 'item-competency',
             'style': 'background-color: ' + colorHex
     });
     
     // Remove competency option
     var levelOneRemoveImg = jQuery('img#remove-img-template').clone();
         levelOneSpan.prepend(levelOneRemoveImg); 
         levelOneRemoveImg.prop('id', null);
         
     // Append the hidden fields, linked competencies
     var splitClass = checkedCB.attr('class').split(' ');
     var classGroup = splitClass[1];
     
     jQuery('input.'+classGroup + ':checked').each(function() {
         
         var eachCheckBox = jQuery(this);
         var competencyID = eachCheckBox.val();
         var hiddenInput = jQuery('<input>', {
             'type': 'hidden',
             'class': 'selected-competencies',
             'value': competencyID
         });
         levelOneSpan.append(hiddenInput);
         
     });
     
     competencyContainer.append(levelOneSpan);
     
     });
     competencyDialog.dialog('close');
 }
 
 
 /**
  * Initiate item competencies dialog 
  * 
  */
 function loadCompetenciesDialog() {
     
     competencyDialog = jQuery('#competency-selector').dialog({
         autoOpen: false,
         height: 500,
         width: 650,
         modal: true,
         buttons: [
            {
                text: 'Cancel',
                class: 'btn btn-sm btn-danger btn-danger-i',
                click: function() {
                    competencyDialog.dialog('close');
                }
            },
            {
                text: 'Save',
                class: 'btn btn-sm btn-success-i',
                click: function() {
                    saveCompetencies()
                }
            }
         ]
     });
 
     competencyDialog.find('form').on('submit', function(event) {
         event.preventDefault();
     });
 
     jQuery('#edit-competencies').on('click', function() {
     competencyDialog.dialog('open');
     });
 
     // Level one checkbox events
     jQuery('div#competency-selector').unbind('change').on('change', 'input[type=checkbox]', function() {
         var checkBox = jQuery(this);
         var checked = checkBox.prop('checked');
         var splitClass = checkBox.attr('class').split(' ');
         var classLevel = splitClass[0];
         var classGroup = splitClass[1];
         if (classLevel === 'level-one-check') {
         jQuery('input.'+classGroup).prop('checked', checked);
         } else if (classLevel === 'level-two-check') {
         /**
          * If any of the sub competence are checked, then we must check the top level competence
          */ 
         var oneChecked = false;
         jQuery('input.'+classGroup).each(function() {
             var eachCheckBox = jQuery(this);
             if (!eachCheckBox.hasClass('level-one-check') && eachCheckBox.prop('checked')) {
                 oneChecked = true;
                 return false;
             }
         });
 
         // Check level one competency if required
         checkBox.parents('div.level-one-container')
                 .find('input.level-one-check')
                 .prop('checked', oneChecked);
 
         }
         
     });
     
     // Toggle sub competencies
     jQuery('a.toggle-sub-competencies').unbind('click').on('click', function() {
         
         // Toggle option text
         var toggleButton = jQuery(this);
         if(toggleButton.hasClass('show-sublevels')) {
         toggleButton.text('more..')
                     .switchClass('show-sublevels', 'hide-sublevels');
         } else {
         toggleButton.text('less..')
                     .switchClass('hide-sublevels', 'show-sublevels');
         }
         
         // Show or hide the actual sub competencies
         var splitClass = toggleButton.attr('class').split(' ');
         var classGroup = splitClass[1];
             jQuery('div.'+classGroup).toggle();
             
     });
     
     // Add remove competency event
     jQuery('#container-competencies').on('click', 'img.remove-competency', function() {
         jQuery(this).parent('span').remove();
     });
     
 }
 
 // Remove competencies dialog
 function removeCompetenciesDialog() {
 competencyDialog.dialog('destroy').remove();  
 }
 
 
 // Validate numeric field value (Used for text type)
 function ValidateNumeric(field) {
     
     // Format decimal values e.g. '.5' => '0.5', '8.' => '8' etc          
     jQuery(field).val(parseFloat(jQuery(field).val()));
     if (isNaN(jQuery(field).val())) {
        jQuery(field).val('');
         
         /* 
         * Re-focus on the textbox
         * 'focus()' doesn't work right away in Chrome so we need to set a timer 
         */
         setTimeout(function() {
            jQuery(field).focus();
         }, 0);
         
     }
 }
 
 // Validate Numeric V2 (Used for slider type)
 function ValidateNumericV2(field) {
     if (jQuery(field).val() !== "-" && !isFinite(jQuery(field).val())) {
        jQuery(field).val('');
     }
 }
 
 // Validate Radio Values
 function ValRadioVals(tb) {
     var isnumobj = jQuery(tb).parents("div[id^=qansoptions]:first").find("input[id^=isnumval]");
 
     if (isnumobj.prop('checked') === true) {
         if (jQuery(tb).val() !== "-" && !isFinite(jQuery(tb).val())) {
             alert("Numeric (Score) Values Only");
             jQuery(tb).val('');
         }
     }
 }
 
 
 //Validate forms for the form management section
 function ValidateForm(form) {
     var yesno;
     if (jQuery(form).attr('name') === 'comp_add_form') {
         if (jQuery('#competence-type').val().length === 0) {
             alert("Please select a type");
             return false;
         } else if (jQuery('#section_order').val().length === 0 || isNaN(jQuery('#section_order').val())) {
             alert("Please choose an order number");
             return false;
         }
         return true;
     } else if (jQuery(form).attr('name') === 'add_previous_form') {
         if (jQuery('#previous-form-id').val().length === 0 || jQuery('#previous-form-id').val() === "--") {
             alert("Please select an object from the bank");
             return false;
         } else if (jQuery('#form-section-list').html().indexOf('No Competencies') > -1) {
             alert("No objects found in bank");
             return false;
         } else {
             var cbchecked = false;
             var celes = jQuery('#form-section-list').find('input[id^=prevsection]');
             jQuery.each(celes, function(i, cb) {
                 if (jQuery(cb).prop('checked')) {
                     cbchecked = true;
                 }
             });
             if (!cbchecked) {
                 alert("Please select at least one item from the checkbox list");
             }
             return cbchecked;
         }
     } else if (jQuery(form).attr('name') === 'additional_options_form') {
         if (clearFormatting) {
             var message = "Are you sure you want to clear the formatting for all items?" +
                         "\n\nClick 'OK' for Yes\nClick 'Cancel' for No";
             yesno = confirm(message);
             if (yesno) {
                jQuery('#cformat').css('color', '#D80000');
                jQuery('#cformat').attr('value', 'Updating');
             } else {
                 clearFormatting = false;
                 return false;
             }
         }
         return true;
     }
 }