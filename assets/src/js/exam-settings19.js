/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2017 Qpercom Limited.  All rights reserved.
 * Javascript (JQuery, Mootools) for the Manage Exam Settings section
 */

jQuery(document).ready(function() {

  returnButtonEvent("manage.php?page=manage_osce");
    
  // Orginal selected state of dropdowns
  var originalState = getCurrentSelectionState();
        
  // Dropdown change events
  jQuery(".advanced-settings-ul").delegate("select", "change", function() {
     var currentState = getCurrentSelectionState();
     
     // Enable or disable update button based on selection station
     var disabled = (currentState === originalState);
     jQuery("#update-settings").prop("disabled", disabled);
     jQuery("#update-settings").prop("value", "Update Settings");
     
     if (disabled) {
        jQuery("#update-settings").css("color", "#000000"); 
     } else {
        jQuery("#update-settings").css("color", "#ff0000"); 
     }
     
  });
    
  // Update button event
  jQuery("#update-settings").click(function() {
           
    var settings = [];      
        
    // Get drop down values
    var dropdowns = jQuery(".advanced-settings-ul").find("select");
    // Loop through all dropdowns and form state string
    jQuery.each(dropdowns, function() {
      var dropdown = jQuery(this);
      
      // Replace '-' with '_' to form database field name
      var field = dropdown.attr('id').replace(/-/g, "_");
      var value = dropdown.val();
      settings.push({field: field, value: value});
    });
    
    var genFail = "Settings could not be updated, please try again";  
      
    // Post data to server
    jQuery.ajax({
      type: 'POST',
      url: 'osce/ajx_settings.php',
      cache: false,
      data: {isfor: 'update',
             exam: jQuery('#exam-id').val(),
             settings: settings}
      }).done(function(response) {
        
        // Take the appropriate action based on response message
        if (response === "EXAM_NON_EXIST") {
          alert("Exam does not exist, please return to exam list");
          jQuery("#update-settings").prop("disabled", true);
          
        } else if (response === "SETTINGS_UPDATE_FAIL") {
          alert(genFail);
          
        } else {
          jQuery("#update-settings").prop("value", "Settings Updated");
          jQuery("#update-settings").prop("disabled", true);
          jQuery("#update-settings").css("color", "#458B00");
          originalState = getCurrentSelectionState();
        }
      }).fail(function() {
        alert(genFail);
      });  
  });
    
  showMenu();
  performScroll();
});


/**
 * Get current selection state
 * 
 * @return string selectState
 */
function getCurrentSelectionState() {
  var selectedState = ""; 
  /* Detect Drop-down changes and enable/disable update button */
  var dropdowns = jQuery(".advanced-settings-ul").find("select");
  
  // Loop through all dropdowns and form state string
  jQuery.each(dropdowns, function() {
     selectedState += "" + jQuery(this).val();
  });
 
  return selectedState;
}
