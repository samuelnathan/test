/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Clear Lists Of Students
function clearList() {
    if (!jQuery("#select_examinee_row").length) {
        
        // Prevent Browser Jump When Hidding Rows
        var theight = (jQuery("#exam_assessment_table").outerHeight() - 71);

        jQuery("#check-all").prop("disabled", true);
        jQuery(".datarow").remove();
        if (jQuery("#base-tr").length) {
            jQuery("#base-tr").remove();
        }

        jQuery("#examinee_table").css("margin-bottom", theight + "px");

        var columnCount = jQuery("#column-count").val();
        var atr = jQuery("<tr>", {"id": "select_examinee_row"});
        var atd = jQuery("<td>", {"colspan": columnCount, "class": "mess2"});
        var adiv = jQuery("<div>", {"id": "examinee_row_div", "class": "tablestatus1"});
        var userInfo = "Please complete filter criteria above and then click the 'view list' button";
        adiv.text(userInfo);

        atd.append(adiv);
        atr.append(atd);
        jQuery("#title-row").after(atr);
    }
}

/**
 * Disable options on the manage list page
 */ 
function disableOptions() {

    if (jQuery("#examinees-caption").length) {
        
        jQuery("#examinees-caption").find("input, select").each(function(i, element) {
            jQuery(element).prop("disabled", true);
        });

        if (jQuery("#filtered-info-cell").length) {
            jQuery("#filtered-info-cell").remove();
        }
        
        if (jQuery("#print-list").length) {
            jQuery("#print-list").remove();
        }

        if (jQuery("#page-count").length) {
            jQuery("#page-count").text("1");
        }

        if (jQuery("#pagenr").length) {
            jQuery("#pagenr").get(0).selectedIndex = 0;
        }

    }

}

// Disable title row links
function disableTitleRowLinks() {
    if (jQuery("#title-row").length) {
        jQuery("#title-row").find("a").each(function(i, a) {
            jQuery(a).parents("th:first").html(jQuery(a).attr("text"));
        });
    }
}

// Basic Url
function basicUrl() {
    var url = 
        "&c=" + jQuery("#course").val() +
        "&y=" + jQuery("#year").val() +
        "&m=" + jQuery("#module").val() +
        "&p=" + jQuery("#pagenr").val()+
        "&s=" + jQuery("#search").val() +
        "&i=" + jQuery("#order-index").val() +
        "&o=" + jQuery("#order-type").val();
    return url;
}

// Validate Form
function ValidateForm(form) {
    
    // Examinee list form
    if (jQuery(form).attr('name') === "examinees_form") {
      if (jQuery("#todo").val() === "delete") {
        var checkboxes = jQuery("#examinees-form").find("input[id^=examinee-check]:checked");

        if (checkboxes.length > 0) {
                
            var message = "** WARNING PLEASE READ **\n\n "
                        + "Are you sure you want to delete the selected records from the system?\n "
                        + "Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n"
            var yesNo = confirm(message);
            return yesNo;
        }
      }
   }
}

/*
 * Event handler for term change
 */
function handlerTermChange() {
    
    emptyDropdown(jQuery("#year").removeClass("select-load"), "", "--");
    emptyDropdown(jQuery("#module").removeClass("select-load"), "", "--");
    emptyDropdown(jQuery("#course").removeClass("select-load"), "", "--");
    disableTitleRowLinks();
    clearList();
    disableOptions();
    
    jQuery("#list-examinees").attr("disabled", true);
   
    if (jQuery("#term").val() == "--" || jQuery("#term").val().length == 0) {
        return;
    }
    
    emptyDropdown(jQuery("#course").addClass("select-load"), "", "Loading....");
    
    jQuery.ajax({
           url: "examinees/json_examinees.php",
           type: "post",
           dataType: 'json',
           data: {
             isfor: "get_term_courses",
             term: jQuery("#term").val()
           },
        dataType: "json"
      })
     .done(function(response) {

        emptyDropdown(jQuery("#course").removeClass("select-load"), "", "--");

        // courses !== "null" && courses.data.length > 0
        if (Object.keys(response).length > 0) {

          if (response.groupBySchool == true) {

             jQuery.each(response.data, function(school, courses) {

                var optGroup = jQuery("<optgroup>", {"label": school});

                jQuery.each(courses, function(school, course) {

                    optGroup.append(jQuery("<option>", {"value": course.id})
                    .text(course.name + " [" + course.id + "]"));

                });

                jQuery("#course").append(optGroup);

             });

          } else {

             jQuery.each(response.data, function(index, each) {
                
                jQuery("#course").append(jQuery("<option>", {"value": each.id})
                .text(each.name + " [" + each.id + "]"));
                
             });       

          }
         
        }
     
    })
    .fail(function(jqXHR) {
      emptyDropdown(jQuery("#course").removeClass("select-load"), "", "--");
      var message = "Failed to retrieve records.\n";
      switch (jqXHR.status) {
           case 404:
           case 500:
               message += "An internal error occurred";
               break;
           case 400:
               message += "Update request was invalid.";
               break;
           case 403:
               message += "Sorry, You may not be logged in or your session may have expired";
               break;
           default:
               message += "An unspecified error has occurred. Contact your system administrator.";
               message += "\n(HTTP Code: " + jqXHR.status + ")";
      }

       alert(message);

    });
    
}

/* 
 * Event handler for course change
 */
function handlerCourseChange()
{
    jQuery("#list-examinees").prop("disabled", true);
    disableTitleRowLinks();
    clearList();
    disableOptions();

    if (jQuery("#course").val().length > 0) {
        emptyDropdown(jQuery("#year").addClass("select-load"), "", "Loading....");
        emptyDropdown(jQuery("#module").addClass("select-load"), "", "Loading....");

        jQuery.ajax({
            url: "examinees/json_examinees.php",
            type: "post",
            dataType: 'json',
            data: {
                isfor: "get_years_and_modules",
                course: jQuery("#course").val()
            }
         })
         .done(function(yrs_mods){
            jQuery("#list-examinees").prop("disabled", false);
            emptyDropdown(jQuery("#year").removeClass("select-load"), "", "ALL");
            emptyDropdown(jQuery("#module").removeClass("select-load"), "", "ALL");

            /* Adopt Years */
            jQuery.each(yrs_mods[0], function(i, year) {
                var yearID = year.year_id.trim();
                var yearValue = year.year_name.trim();

                var option = jQuery("<option>", {"value": yearID}).text(yearValue);
                jQuery("#year").append(option);
            });

            /* Adopt Modules */
            jQuery.each(yrs_mods[1], function(i,module) {
                var moduleID = module.module_id.trim();
                var moduleName = module.module_name.trim();

                var option = jQuery("<option>", {"value": moduleID, "title": moduleName})
                                 .text(moduleName + " [" + moduleID + "]");
                jQuery("#module").append(option);
            });
         })
         .fail(function(jqXHR) {
            var message = "Failed to retrieve records for ID specified\n";
            switch (jqXHR.status) {
                case 404:
                case 500:
                    message += "An internal error occurred";
                    break;
                case 400:
                    message += "Update request was invalid.";
                    break;
                case 403:
                    message += "Sorry, You may not be logged in or your session may have expired";
                    break;
                default:
                    message += "An unspecified error has occurred. Contact your system administrator.";
                    message += "\n(HTTP Code: " + jqXHR.status + ")";
            }

            alert(message);
         });
    } else {
        jQuery("#year").empty();
        jQuery("#module").empty();
    }
}

jQuery(document).ready(function() {
   /**
    * Printing Examinee List Popup Window
    */
    if (jQuery("#examinee-print-list").length || jQuery("#pdframe").length) {
        document.title = "List | " + jQuery("#course-name").val() + " | " + jQuery("#year-value").val();
    }

   /**
    * Examinee List Section
    */
    if (jQuery("#examinees-form").length) {
       
        /* Term Selection Event */
        jQuery("#term").change(handlerTermChange);
          
        /* Course Selection Event */
        if (jQuery("#course").length) {
            jQuery("#course").on("change", handlerCourseChange);
        }

        /* Term and Module Selection Event */
        if (jQuery("#module").length) {
            jQuery("#module").on("change", function() {
                disableTitleRowLinks();
                clearList();
                disableOptions();
            });
        }

        /* Course Year Selection Event */
        if (jQuery("#year").length) {
            jQuery("#year").on("change", function() {
                disableTitleRowLinks();
                jQuery("list-examinees").prop("disabled", true);
                emptyDropdown(jQuery("#module").addClass("select-load"), "", "Loading....");
                clearList();
                disableOptions();

                var course_year = jQuery(this).val();
                jQuery.ajax({
                    url: "examinees/json_examinees.php",
                    type: "post",
                    dataType: 'json',
                    cache: false,
                    data: {
                        isfor: "get_modules",
                        year: course_year,
                        course: jQuery("#course").val()
                    }
                 })
                 .done(function(modules){
                    jQuery("#list-examinees").prop("disabled", false);
                    emptyDropdown(jQuery("#module").removeClass("select-load"), "", "ALL");

                    jQuery.each(modules, function(i, module) {

                        var moduleID = module.module_id.trim();
                        var moduleName = module.module_name.trim();

                        var option = jQuery("<option>", {"value": moduleID, "title": moduleName})
                                         .text(moduleName + " [" + moduleID + "]");
                        jQuery("#module").append(option);
                    });
                 })
                 .fail(function(jqXHR) {
                    var message = "Failed to get records for ID specified.\n";
                    switch (jqXHR.status) {
                        case 404:
                        case 500:
                            message += "An internal error occurred";
                            break;
                        case 400:
                            message += "Update request was invalid.";
                            break;
                        case 403:
                            message += "Sorry, You may not be logged in or your session may have expired";
                            break;
                        default:
                            message += "An unspecified error has occurred. Contact your system administrator.";
                            message += "\n(HTTP Code: " + jqXHR.status + ")";
                    }

                    alert(message);
                 });
            });
        }

        /**
         * List students
         */
        if (jQuery("#list-examinees").length) {
            jQuery("#list-examinees").on("click", function() {
                if (jQuery("#examinee_table").length) {
                    jQuery("#examinee_table").css("visibility", "hidden");
                }
                parent.location = "manage.php?page=manage_examinees"
                                + "&t=" + jQuery("#term").val() 
                                + basicUrl();
            });
        }

        /**
         * Page selection
         */
        if (jQuery("#pagenr").length) {
            jQuery("#pagenr").on("change", function() {
                parent.location = "manage.php?page=manage_examinees"
                                + basicUrl();
            });
        }

        /**
         * Print examinee list
         */
        jQuery("#print-list").click(function() {
 
            var url = "manage.php?page=preview_examinees" + basicUrl();
            var name = "printexaminees" 
                        + jQuery("#course").val()
                        + jQuery("#year").val()
                        + jQuery("#module").val();     
            var popup = window.open(url, name);
            popup.focus();
            
        });


        /*
         * Add new students
         */
        if (jQuery("#add-examinees").length) {
            jQuery("#add-examinees").on("click", function() {
                var number = jQuery("#number").val().trim();
                if (!isNaN(number) && number.length > 0 && number > 0) {
                    parent.location = "manage.php?page=update_examinees&a=add" +
                            "&n=" + jQuery("#number").val() + basicUrl();
                } else {
                    jQuery("#number").val(1);
                    alert("Please enter a numeric value greater than 0");
                }
            });
        }

        /*
         * Student result history events
         */
        jQuery('table#examinee_table').on('click', '.examinee-result', function() {
            var examineeID = jQuery(this).closest('tr').find('.examinee-checkbox').val();
            var popup = open(
               "manage.php?page=studenthistory&student=" + examineeID,
               "_examineehistory" + examineeID
            );
            popup.focus();
        });
    }
    
     /* Check Box Event & Edit Event */
    if (jQuery("#examinee_table").length) {
        addCheckboxEvents("examinee");

        // hover 'linked to' modules message
        jQuery( '.hover-modules' ).tooltip({
            placement: "left",
            track: false,
            classes: {
                "ui-tooltip": "tips3"
              }
          });
    }   
    
    // Is There a Menu
    if (!jQuery("#pdframe").length && !jQuery("#print-action").length) {
        showMenu();
    }
});
