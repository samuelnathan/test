/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 20/06/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 * @Manage Tools Section
 */

jQuery(document).ready(function() {

    // Login Management button: Cormac Mc Swiney
    if (jQuery('#clear-login-button').length) {
        jQuery('#clear-login-button').on('click', function () {
            var message = 'Access has been restored to all users blocked as a result of entering wrong passwords.\n'
                        + 'If users still do not know their password, passwords can be reset in '
                        + 'the manage examiners and manage administrators sections.'
            alert(message);
        });
    }
    
    // Log file selector
    jQuery('#logfile').change(function() {
        parent.location = "manage.php?page=userlog_tool"
                        + "&file=" + jQuery(this).val();
    });

    showMenu();
});
