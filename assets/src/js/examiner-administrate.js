/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2018 Qpercom Limited.  All rights reserved.
*/

// Sms Dialog iFrame object default
var smsDialog;

jQuery(document).ready(function() {

   // Filter events
   if (jQuery('#filter-list').length) {
     jQuery('#filter-list').on('click', function() {
          filterList();
     });

   }

   // Filter search input field key press (ENTER Key)
   if (jQuery('#search').length) {
     jQuery('#search').keypress(function (event) {
       if (event.which === 13) {
         filterList();
       }
     });
   }

   // Prepare password validation (jquery-password-fields.js)
   if(typeof preparePasswordValidation != 'undefined'){
       preparePasswordValidation();
   }

   // Show menu and scroll
   showMenu();

   // Scroll to bottom
   performScroll();

   // Examiner List Section Events
   if (jQuery('#examiner_list_form').length) {
        if (jQuery('#add').length) {
            addExaminerEvent();
        }

        if (jQuery('#count').length && jQuery('#count').val() > 0) {
            if (jQuery('#check-all').length) {
                addCheckboxEvents('examiner');
            }
            editExaminersEvents();
        }
  // Examiner Edit Section Events
    } else if (jQuery('#examredit_list_form').length) {
        passwordFieldsEvents(jQuery('#examredit_table'), false);
        commonPasswordEvent();
        cancelEditExaminersEvent();
    }
});

/*
 * Add examiner event (When add button is clicked)
 *
 */
function addExaminerEvent() {
    jQuery('#add').on('click', function(e) {
        e.preventDefault();

        // Hide some fields before displaying add record row
        jQuery('#examiner_table').find('input, select, img').css('display', 'none');

        // Hide last login column as it's unrequired for the add/edit mode
        toggleLastLoginColumn();

        var lastRow;
        if (jQuery('#first_examiner_row').length) {
            var informationRow = jQuery('#first_examiner_row').remove();
            lastRow = jQuery('#title-row');
        } else {
            if (jQuery('#count').length) {
                lastRow = jQuery('#examiner_row_' + jQuery('#count').val());
            }
        }
        var newAddRow = jQuery('<tr>', {'id': 'add-row'});
            lastRow.after(newAddRow);

        setWaitStatusRow(jQuery(newAddRow), (jQuery('#filtered-info-cell').attr('colspan') - 2));

        var url = "examiners/ajx_examiners.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'add_examiner',
                fordept: jQuery('#dept_sel').val(),
                role_filtered: jQuery('#role').val()
            }
         })
         .done(function(html){
            newAddRow.empty().attr('class', 'addrow').html(html);
            jQuery('#add_exmnr_id').focus();
            addButtonsRow();
            cancelAddExaminerEvent(newAddRow, informationRow);
            submitExaminerEvent();
            passwordFieldsEvents(jQuery('.password-fields-td'), true);
            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            jQuery('#examiner_table').find('input, select, a, img').css('display', '');
            newAddRow.remove();
            alert('The request failed, Please try again');
         });
    });
}

// Add cancel event Examiners
function cancelAddExaminerEvent(row, informationRow) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();
        if (!jQuery('#count').length) {
            row.replaceWith(informationRow);
        } else {
            row.remove();
        }

        jQuery('#examiner_table').find('select, img, input, a').css('display', '');
        removeNewRecordControls();

        // Show last login column
        toggleLastLoginColumn();

    });
}

/*
 * Submit new examiner, when user click the submit button for a new examiner record
 */
function submitExaminerEvent() {

    jQuery('#add-save').on('click', function (e) {
        e.preventDefault();

        // Field Validations
        if (areEditFieldsComplete(jQuery('#add-row')) && jQuery('#last-known-term').val().length > 0) {
            if (!isEmailValid(jQuery('#add_exmnr_em').val())) {

                alert("Email Address is not valid");
                return false;

            } else {

                // Validate record ID field
                if (!isRecordIDValid(jQuery('#add_exmnr_id').val(), false)) {

                    return false;

                }

                var url = "examiners/ajx_examiners.php";
                var random = Math.random();
                var error = false;
                var deptCount = 0;
                var departments = [];

                var deptsChecked = jQuery('#add-dpt').find('input[class=depts-cbs]:checked');

                jQuery.each(deptsChecked, function (i, el) {
                    error |= (jQuery(el).val().length === 0);
                    departments[deptCount] = jQuery(el).val();
                    deptCount++;
                });

                if (error) {

                    alert('Invalid Department List');
                    return false;

                }

                /* Password fields check
                 * Code: jQuery
                 */

                // If password fields are not valid then abort
                if (!passwordFieldsValid('add')) {

                    return false;

                }

                // Send request to server
                jQuery(this).css('display', 'none');
                jQuery('#add-cancel').css('display', 'none');
                jQuery('#base-button-td').attr('class', 'waittd3');
                jQuery.ajax({
                    url: url,
                    type: "post",
                    data: {
                        sid: random,
                        isfor: 'submit_examiner',
                        exmnr_id: jQuery('#add_exmnr_id').val(),
                        exmnr_fn: jQuery('#add_exmnr_fn').val(),
                        exmnr_sn: jQuery('#add_exmnr_sn').val(),
                        exmnr_em: jQuery('#add_exmnr_em').val(),
                        exmnr_no: jQuery('#add_exmnr_no').val(),
                        exmnr_pass: jQuery('.first-password-field').val(),
                        exmnr_lvl: jQuery('#add_exmnr_lvl').val(),
                        depts: departments,
                        exmnr_train: (jQuery('#add_exmnr_train').length ? jQuery('#add_exmnr_train').val() : 0),
                        exmnr_ac: jQuery('#add_exmnr_ac').val(),
                        exmnr_term: jQuery('#choose-term').val()
                    }
                 })
                 .done(function(html){
                    // Term doesn't exist
                    if (html.indexOf('TERM_NON_EXIST') > -1) {

                        failAdd();
                        alert('Term/year selected does not exist in system, please choose another');

                    } else if (html.indexOf('USER_ALREADY_USED') > -1) {

                        // User doesn't exist...
                        failAdd();
                        alert('Examiner ID is already in use');

                    } else if (html.trim() !== 'true') {

                        failAdd();
                        alert("Examiner ID '" + html + "' is already in use");

                    } else {

                        // New examiner created, filter list + pagination
                        filterList(true, true);

                    }
                 })
                 .fail(function(jqXHR) {
                    failAdd();
                    alert('The request failed, Please try again');
                 });

            }
        } else {

            alert("Please complete all fields.");

        }
    });
}

// Add Fields Complete
function areEditFieldsComplete(row) {
    var allComplete = true;

    // Check text/password fields
    var inputFields = jQuery(row).find('input[type=text], input[type=password]');
    jQuery.each(inputFields, function(i, el) {
        // Check all fields apart from 'exmnr_no'
        if ((!jQuery(el).attr('id') || !jQuery(el).attr('id').indexOf('exmnr_no') > -1) && jQuery(el).val().length === 0) {
            allComplete = false;
        }
    });

    // Check Drop Downs
    var selectFields = jQuery(row).find('select');
    jQuery.each(selectFields, function(i, el) {
        if (jQuery(el).length === 0) {
            allComplete = false;
        }
    });

    // Check depatment fields
    var selectedDepartments = jQuery(row).find('input[class=depts-cbs]:checked');
    if (selectedDepartments.length === 0) {
        allComplete = false;
    }

    return allComplete;
}

// Edit Events Examiners
function editExaminersEvents() {
    if (jQuery('#count').length) {
        var buttons = jQuery('#examiner_list_form').find('a[id^=edit_]');

        jQuery.each(buttons, function(i, b) {
            editExaminerEvent(b);
        });
    }
}

// Edit examiner event
function editExaminerEvent(b) {
    jQuery(b).off('click', editExaminerEventHandler).on('click', editExaminerEventHandler);
}
function editExaminerEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');

    // Hide last login column as it's unrequired for the add/edit mode
    toggleLastLoginColumn();

    var rowCloned = row.clone(true, true);
    var examinerID = row.find('input[id^=examiner-check]').val();

    var url = "examiners/ajx_examiners.php";
    var random = Math.random();

    jQuery('#examiner_table').find('input').css('display', 'none');
    jQuery('#examiner_table').find('select, img').css('display', 'none');

    setWaitStatusRow(jQuery(row), (jQuery('#filtered-info-cell').attr('colspan') - 2));
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: 'edit_examiner',
            examiner: examinerID
        }
     })
     .done(function(html){
        row.empty().html(html);
        row.attr('class', 'editrw');
        row.attr('id', 'edit_row');

        passwordFieldsEvents(jQuery('.password-fields-td'), false);

        editButtonsRow(jQuery(row), (jQuery('#filtered-info-cell').attr('colspan') - 2));
        cancelUpdateExaminerEvent(jQuery('#edit-cancel'), rowCloned, row);
        updateExaminerEvent(jQuery('#edit-update'));
     })
     .fail(function(jqXHR) {
        row.replaceWith(rowCloned);
        jQuery('#examiner_table').find('input, select, a, img').css('display', '');
        editExaminerEvent(rowCloned.find('a[id^=edit_]'));
        addCheckboxEvent(rowCloned.find('input[id^=examiner-check]'), 'examiner');
        alert('The request failed, Please try again');
     });
}
/*
 * Update examiner event, runs when a user clicks the update button
 * to update an examiner record
 */
function updateExaminerEvent(button) {
    jQuery(button).on('click', function (e) {
        e.preventDefault();

        // Validate fields
        if (areEditFieldsComplete(jQuery('#edit_row'))) {
            if (!isEmailValid(jQuery('#edit_exmnr_em').val())) {
                alert("Email Address is not valid");
                return false;
            } else {

                // Validate record ID field
                if (!isRecordIDValid(jQuery('#edit_exmnr_id').val(), false)) {
                    return false;
                }

                var url = "examiners/ajx_examiners.php";
                var random = Math.random();

                var error = false;
                var deptCount = 0;
                var departments = [];
                var deptsChecked = jQuery('#edit-dpt').find('input[class=depts-cbs]:checked');

                jQuery.each(deptsChecked, function (i, el) {
                    if (jQuery(el).val().length === 0) {
                        error = true;
                    }
                    departments[deptCount] = jQuery(el).val();
                    deptCount++;
                });

                if (error) {
                    alert('Invalid Department List');
                    return false;
                }

                /*
                 * Change password action required?
                 * Code: jQuery
                 */
                var changePasswordChecked = jQuery('.change-password').prop('checked');
                if (changePasswordChecked) {

                    // If password fields are not valid then abort
                    if (!passwordFieldsValid('edit')) {
                        return false;
                    }

                }

                jQuery('#edit-update').css('display', 'none');
                jQuery('#edit-cancel').css('display', 'none');
                jQuery('#b_edit_td').attr('class', 'waittd2');
                jQuery.ajax({
                    url: url,
                    type: "post",
                    data: {
                        sid: random,
                        isfor: 'update_examiner',
                        exmnr_id: jQuery('#edit_exmnr_id').val(),
                        exmnr_fn: jQuery('#edit_exmnr_fn').val(),
                        exmnr_sn: jQuery('#edit_exmnr_sn').val(),
                        exmnr_em: jQuery('#edit_exmnr_em').val(),
                        exmnr_no: jQuery('#edit_exmnr_no').val(),
                        exmnr_pass: jQuery('.first-password-field').val(),
                        exmnr_passc: (changePasswordChecked ? 1 : 0),
                        exmnr_lvl: jQuery('#edit_exmnr_lvl').val(),
                        depts: departments,
                        org_exmnr_id: jQuery('#examiner_nr').val(),
                        exmnr_train: jQuery('#edit_exmnr_train').val(),
                        exmnr_ac: jQuery('#edit_exmnr_ac').val()
                    }
                 })
                 .done(function(html){
                    // User does not exist
                    if (html.indexOf('USER_NON_EXIST') > -1) {
                        alert("Examiner you are trying to update is not found in the System, please refresh list [F5]");
                        failEdit();
                        // Term does not exist
                    } else if (html.indexOf('TERM_NON_EXIST') > -1) {
                        alert('Term selected does not exist in system, refreshing web page');
                        refreshPage(false);
                    } else if (html.indexOf('USER_ALREADY_USED') > -1) {
                        failEdit();
                        alert("Examiner ID specified is already in use");
                    } else {
                        refreshPage(false);
                    }
                 })
                 .fail(function(jqXHR) {
                    failEdit();
                    alert('The request failed, Please try again');
                 });
            }
        }
        else {
            alert("Please complete all fields.");
        }
    });
}

// Cancel Update Event
function cancelUpdateExaminerEvent(button, clonedRow, row) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        row.replaceWith(clonedRow);
        jQuery('#examiner_table').find('input, a, img, select').css('display', '');
        var editOption = clonedRow.find('a[id^="edit_"]');
        var rowCheckbox = clonedRow.find('input[id^="examiner-check"]');
            rowCheckbox.css('display', '');

        editExaminerEvent(editOption);
        addCheckboxEvent(rowCheckbox, 'examiner');

        // Show last login column
        toggleLastLoginColumn();

    });
}

/******************[Edit Update Section]************************/

/* Common password event (jQuery)
 */
function commonPasswordEvent() {

    jQuery('#common-password-button').click(function() {

       // Get Common Password
       var commonPassword = jQuery('#common-password-field').val();
       var changeCheckboxes = jQuery('.change-password');

       // Loop through all password checkbox fields
       changeCheckboxes.each(function() {

          // check the checkbox
          var checkBox = jQuery(this);
          var alreadyChecked = checkBox.prop('checked');
          checkBox.prop('checked', true);

          // Trigger checkbox event if not checked
          if (!alreadyChecked) {
            checkBox.trigger('change');
          }

          // Get parent TD
          var parentTD = checkBox.parents('td[class=password-fields-td]');

          // Get password fields
          var firstPasswordField = parentTD.find('input[class=first-password-field]');
          var secondPasswordField = parentTD.find('input[class=second-password-field]');

          // Set password field with common password
          firstPasswordField.val(commonPassword).trigger('keyup');
          secondPasswordField.val(commonPassword).trigger('keyup');

       });
    });
}

// Cancel button for edit Examiners event [Added 03/05/2013]
function cancelEditExaminersEvent() {
    if (jQuery('#cancel-edit-exmrs').length) {
        jQuery('#cancel-edit-exmrs').on('click', function(e) {
            e.preventDefault();
            parent.location = "manage.php?page=manage_examiners";
        });
    }
}
jQuery(document).ready(function() {  
    var dialog = jQuery('<div>', {hidden: '', id: 'dialog-iframe'}).append(jQuery('<iframe>', {name: 'ifdialog', src:'manage.php'}))
    jQuery('body').append(dialog)
});
// Validate submit form
function ValidateForm(form) {
    //examiner list form
    if (jQuery(form).attr('name') === 'examiner_list_form') {
        var theCheckboxes = jQuery('#examiner_list_form').find('input[id^=examiner-check]:checked');
        if (theCheckboxes.length > 0) {
            //Delete Examiners
            var confirmMessage = "** Are you sure you want to DELETE the selected users permanently from the system **";
            if (jQuery('#todo').val() === "delete" && !confirm(confirmMessage)) {
                return false;
                //Send SMS Examiners
            } else if (jQuery('#todo').val() === 'sms') {
                //Change form properties

                jQuery('#examiner_list_form').attr({target: 'ifdialog', action: 'manage.php'});

                //Show hidden variables
                jQuery('#sms-mode, #sms-page').prop('disabled', false);
                jQuery('#examiners-data').prop('disabled', true);

                
                jQuery('#dialog-iframe').removeAttr('hidden');
                smsDialog = jQuery('#dialog-iframe').dialog({
                    title: 'Send SMS to Users', 
                    modal: true,
                    width: 700,
                    closeOnEscape: false,
                    dialogClass: "smsDialog",
                    close: function() {
                        //Change form properties & Show hidden variables on the Close Event
                        jQuery('#dialog-iframe').attr('hidden', '');
                        jQuery('#examiner_list_form').attr({target: '', action: 'examiners/examiners_redirect.php'});
                        jQuery('#sms-mode, #sms-page').prop('disabled', true);
                        jQuery('#examiners-data').prop('disabled', false).removeAttr('disabled');
                    }
                });
                
            } else {
                return true;
            }
        } else {
            return false;
        }

    // For Edit Examiner Form
    } else if (jQuery(form).attr('name') === 'examredit_list_form') {
        var recordsComplete = true;
        var recordIDsValid = true;
        var emailValid = true;
        var duplicateIDs = false;
        var listOfIDs = [];

        var editRows = jQuery('#examredit_table').find('tr[class=edit-exmr-rows]');

        // Loop through rows of fields
        jQuery.each(editRows, function(i, editRowItem) {
            var editRow = jQuery(editRowItem);

            // Check for incomplete fields
            if (!areEditFieldsComplete(editRow)) {
                recordsComplete = false;

            // Validate email fields
            } else if (!isEmailValid(editRow.find('input[class$=email]').val())) {
                emailValid = false;
            }

            // Examiner ID
            var examinerID = editRow.find('input[class*="newID"]').val();
                examinerID = examinerID.trim();

            // Validate record ID field
            if (!isRecordIDValid(examinerID, true)) {
                recordIDsValid = false;
            }

            // Not in list of IDs
            if (!listOfIDs.indexOf(examinerID) > -1) {
                listOfIDs.push(examinerID);
                listOfIDs = jQuery.unique(listOfIDs);
            } else {
                duplicateIDs = true;
            }

        });

        /**
         * Some fields are incomplete,
         * output appropriate error message
         *
         */
        if (!recordsComplete) {
            alert('Please complete all fields before continuing');
            return false;
        /**
         * One or more email addresses are invalid
         */
        } else if (!emailValid) {
            alert("One or more email addresses are not valid");
            return false;
        /**
         * One or more record IDs are invalid
         */
        } else if (!recordIDsValid) {
            /**
             * Output appropriate error message
             * Pass invalid dummy ID '^' to output error
             */
            isRecordIDValid('^');
            return false;
        /**
         * Duplicate records IDs found, inform the user
         */
        } else if (duplicateIDs) {
            var message = "You cannot assign multiple users with the same Identifier. " +
                          "Please double check the Identifier fields"
            alert(message);
            return false;
        } else {
            // If password fields are not valid then abort
            return passwordFieldsValid('edit');
        }
    }
}

/*
 * Hide last login column for add/edit records mode to make more space.
 * Unrequired column in add/edit mode
 */
function toggleLastLoginColumn() {

  // Get current colspan values for the top and bottom rows
  var topColspan = jQuery('#filtered-info-cell').attr('colspan');
  var baseColspan = jQuery('#base-button-td').attr('colspan');

  // Incrementing or decrementing colspan values
  if (jQuery('.last-login').is(':visible')) {
    topColspan--;
    baseColspan--;
  } else {
    topColspan++;
    baseColspan++;
  }

  // Toggle colspan values for top and bottom rows
  jQuery('#filtered-info-cell').attr('colspan', topColspan);
  jQuery('#base-button-td').attr('colspan', baseColspan);

  // Toggle hide / show
  jQuery('.last-login').toggle();

}

/**
 * Filters examiner list
 * @created boolean  new user added to exam team list
 * @pagination persist pagination
 */
function filterList(created, pagination) {

    var url = "manage.php?page=manage_examiners"
            + "&dept=" + jQuery('#dept').val()
            + "&term=" + jQuery('#term').val()
            + "&role=" + jQuery('#role').val()
            + "&account=" + jQuery('#account').val()
            + "&forenames=" + jQuery('#forenames').val()
            + "&surnames=" + jQuery('#surnames').val()
            + "&search=" + jQuery('#search').val();

    // New user created parameter
    if (typeof created !== 'undefined' && created) {
        url += "&created=yes";
    }

    // Persist with pagination set to "all"
    if (typeof pagination !== 'undefined' && pagination) {
        url += "&num=" + ((jQuery('#page-number').val()
        && jQuery('#page-number').val() == 0) ? "0" : "1");
    }

    parent.location = url;

}

// Get Dialog Object
function getDialogObject() {
    return smsDialog;
}
