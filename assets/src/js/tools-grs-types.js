/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 * JS Code for Global Rating Scale Types
 */

// Server page URL
var requestUrl = "tools/ajax_grs_types.php";

jQuery(document).ready(function() {
    
   // Add events
   chooseRecordEvent();
   addRecordEvent();
   goButtonEvent();
   editRecordEvent();
   
   // Show menu
   showMenu();
});


/*
 *  Global Rating Scale type 'edit' event action
 */
function editRecordAction(trigger) {
    
    var parentRow = trigger.closest('tr');
    var recordKey = parentRow.find('input[class=list-check]').val(); 
    parentRow.attr('id', 'original-row-before-edit');
    parentRow.hide();
        
    /* Deactivate form controls
     * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
     */
    toggleListControls(true, true, true, true, true);
    
    // Deactivate edit buttons
    removeEditRecordEvent();
    
    // Dim data cells
    dimDataCells();

    // Add wait row
    renderEditWaitRow();
    
    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'get_grs_type',
                record_key: recordKey},
         dataType: 'json'
     }).done(function(record) {
       
       // Remove edit wait row
       removeEditWaitRow();
       renderEditRecordRow();
         
       // Add additional fields and values
       var idField = jQuery('<input>', {'type': 'text',
                                        'id': 'scale-type-id',
                                        'maxlength': '2',
                                        'class': 'tf7',
                                        'value': record.scale_type_id});
          
       var nameField = jQuery('<input>', {'type': 'text',
                                          'id': 'scale-type-name',
                                          'maxlength': '10',
                                          'class': 'tf2',
                                          'value': record.scale_type_name});
                                      
       var colourField = jQuery('<input>', {'type': 'text',
                                            'id': 'scale-type-colour',
                                            'class': 'tf2',
                                            'value': record.scale_type_colour});
                                        
       var idCell = jQuery('<td>').append(idField);
       var nameCell = jQuery('<td>').append(nameField);
       var colourCell = jQuery('<td>').append(colourField);
       jQuery('#first-edit-cell').after(idCell, nameCell, colourCell);
       
       // Show update fields row
       renderUpdateFieldsRow();
       
       // Add cancel edit event
       cancelRecordEvent('edit');
       
       // Add update record event
       updateRecordEvent();
       
     }).fail(function() {
         removeEditWaitRow();
         parentRow.show();
         alert("Record could not be retrieved, please refresh page [F5]");
     });
    
}


/*
 *  Global Rating Scale type 'update' event action
 */
function updateRecordAction() {
    
   // Get values to pass
   var scaleTypeID = jQuery('#scale-type-id').val();
   var scaleTypeName = jQuery('#scale-type-name').val();
   var scaleTypeColour = jQuery('#scale-type-colour').val();

   // [DC 31/10/2014] Need to improve validation here as it's quite basic
   if(scaleTypeID.length === 0 || !jQuery.isNumeric(scaleTypeID) || scaleTypeName.length === 0 || scaleTypeColour.length === 0) {
      alert("Please complete all fields\nNote: The 'Scale Type ID' field only accepts numeric values");
      return;
   }

   // Hide update fields and show wait indicator
   addUpdateWait();
   
   // Get record key from row being edited
   var recordKey = jQuery('#original-row-before-edit').find('input[class=list-check]').val(); 

   // Post data to server
   jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'update_grs_type',
                record_key: recordKey,
                scale_type_id: scaleTypeID,
                scale_type_name: scaleTypeName,
                scale_type_colour: scaleTypeColour}
   }).done(function(response) {

      removeUpdateWait();
      if (response === "RECORD_NOT_UPDATED") {
          showUpdateFields();
          alert("Record could not be updated, please make sure that the record ID is not already in use");
      } else {
          removeEditingRows();
          
          /* Reactivate form controls
           * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
           */
          toggleListControls(false, true, false, true, true);
          
          // Show base row
          toggleBaseRow(true);
          
          // Reload list
          reloadRecordList(true);
          
          // Re-enable edit events
          editRecordEvent();
       }

    }).fail(function() {
         removeUpdateWait();
         showUpdateFields();
         alert("Record could not be updated, please try again");
    });
  
}


/*
 *  Global Rating Scale type 'Add' event action
 */
function addRecordAction() {

    /* Deactivate form controls
     * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
     */
    toggleListControls(true, true, true, false, false);

    // Hide no records message row if it exists
    hideNoRecordsRow();

    // Render new record row
    renderNewRecordRow();

    // Remove edit events
    removeEditRecordEvent();
    
    // Dim data cells
    dimDataCells();

    // Add additional fields
    var idField = jQuery('<input>', {'type': 'text', 'id': 'scale-type-id', 'maxlength': '2', 'class': 'tf7'});
    var nameField = jQuery('<input>', {'type': 'text', 'id': 'scale-type-name', 'maxlength': '10', 'class': 'tf2'});
    var colourField = jQuery('<input>', {'type': 'text', 'id': 'scale-type-colour', 'class': 'tf2'});
    var idCell = jQuery('<td>').append(idField);
    var nameCell = jQuery('<td>').append(nameField);
    var colourCell = jQuery('<td>').append(colourField);
    jQuery('#first-add-cell').after(idCell, nameCell, colourCell);

    // Add buttons for new record
    addSaveFields();

    // Add cancel event for new record 
    cancelRecordEvent('add');

    // Add save event for new record 
    saveRecordEvent();
    
    // Focus first field
    jQuery('#scale-type-id').focus();    

    // Scroll to bottom of page
    scrollBottomOfPage('slow');

}

/*
 *  Global Rating Scale type 'Save' event action
 */
function saveRecordAction() {
  
    // Get values to pass
    var scaleTypeID = jQuery('#scale-type-id').val();
    var scaleTypeName = jQuery('#scale-type-name').val();
    var scaleTypeColour = jQuery('#scale-type-colour').val();

   // [DC 31/10/2014] Need to improve validation here as it's quite basic
   if(scaleTypeID.length === 0 || !jQuery.isNumeric(scaleTypeID) || scaleTypeName.length === 0 || scaleTypeColour.length === 0) {
      alert("Please complete all fields\nNote: The 'GRS value' field only accepts numeric values");
      return;
   }

    // Hide save fields
    addSaveWait();

    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'add_grs_type',
                scale_type_id: scaleTypeID,
                scale_type_name: scaleTypeName,
                scale_type_colour: scaleTypeColour}
     }).done(function(response) {
         
         removeSaveWait();
         if (response === "RECORD_NOT_SAVED") {
            showSaveFields();
            alert("Record could not be updated, please make sure that the record ID is not already in use");
         } else {
            removeNewRecordRow();
            removeSaveFields();

            /* Reactivate form controls
             * Params: disableAddButton, disableGoFields, disableCheckboxes, showAddButton, showGoFields
             */
            toggleListControls(false, true, false, true, true);

            reloadRecordList(true);
            
            // Re-enable edit events
            editRecordEvent();
            
         }

     }).fail(function() {
         removeSaveWait();
         showSaveFields();
         alert("Record could not be saved, please try again");
     });
}

/*
 *  Global Rating Scale List 'Action' event action
 */
function goButtonAction() {
 
  // What is the action ?
  var action = jQuery('#actions').val(); 
  if (action === "delete") {

    var recordsToDelete = jQuery(".list-check:checked").map(function() {
        return jQuery(this).val();
    }).get();

    // Nothing to delete then get the hell out of here
    if (recordsToDelete.length === 0) {
        toggleGoFields(true);
        return;
    }

    // Confirm with user
    var yes = confirm("Are you sure you would like to delete the selected record(s)");
      if (!yes) {
        return;
      }

      // Post data to server
      jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'delete_grs_types',
                records_to_delete: recordsToDelete}
         }).done(function() {

           reloadRecordList(false);

         }).fail(function() {
            alert("Selected Record(s) could not be deleted, please try again");
         });

      }
}

/*
 * Reload record list
 * @param Boolean true|false scrollToBottom
 */
function reloadRecordList(scrollToBottom) {
    
    // Remove old records and wait for new records
    toggleGoFields(true);
    toggleAddButton(true);
    toggleCheckAllBox(true);
    renderListWaitRow();

    // Post data to server
    jQuery.ajax({
         type: 'POST',
         url: requestUrl,
         cache: false,
         data: {isfor: 'get_all_grs_types'},
         dataType: 'json'
     }).done(function(recordsJSON) {
       
       // Remove list wait message
       removeListWaitRow();
             
       // Do we have records to process ? 
       if (recordsJSON.length > 0) {
        
         // Loop through returned records
         jQuery.each(recordsJSON, function(index, eachRecord) {
             var recordID = eachRecord.scale_type_id;
             var recordData = [eachRecord.scale_type_id, eachRecord.scale_type_name, eachRecord.scale_type_colour];
                 appendRecordToList(recordID, recordData, true);
         });
     
         // Scroll to bottom of page
         if (scrollToBottom) { 
           scrollBottomOfPage('no_animation');
         }
         deleteNoRecordsRow();
         toggleCheckAllBox(false);
       } else {
         renderNoRecordsRow();
       }
       toggleAddButton(false);
       
     }).fail(function() {
         alert("Records could not be retrieved, please refresh page [F5]"); 
     });
     
   
}
