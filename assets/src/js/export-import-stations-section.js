 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @ Manage Stations Import & Export
  */

 jQuery(document).ready(function() {

     // Tabs selection events
     if (jQuery('#tabs').length) {

         jQuery('#tabs').on('click', 'a', function(e) {

             e.preventDefault();
             var pli = jQuery(this).parents('li:first');
             var tabs = jQuery('#tabs').find('li');

             jQuery.each(tabs, function(i, tab) {

                 jQuery(tab).removeAttr('id');
                 jQuery("#" + jQuery(tab).attr('class')).removeClass('show-content');

             });

             var parentLiClass = pli.attr('class');
             jQuery("#" + parentLiClass).addClass('show-content');
             pli.attr('id', 'tab-selected');

         });

     }

     // Department and tern dropdowns
     if (jQuery('#dept-ex').length && jQuery('#term-ex').length) {

         jQuery('#dept-ex, #term-ex').on('change', function() {

             // Disable go button
             jQuery('#export-button').prop('disabled', true);

             // Populate records
             if (jQuery(this).val().trim().length > 0) {

                 jQuery('#form-list').empty().append(jQuery('<li>', {'text': 'loading.....'}));

             }

            // POST/Request to the server
             jQuery.ajax({
                url: 'tools/json_tools.php',
                type: "post",
                dataType: 'json',
                data: {
                    isfor: 'forms',
                    dept: jQuery('#dept-ex').val(),
                    term: jQuery('#term-ex').val()
                }
             })
             .done(function(forms){
                // Empty station list
                jQuery('#form-list').empty();

                // Loop through stations
                var formCount = forms.length;

                if (formCount > 0) {

                    // Add form checkboxes
                    jQuery.each(forms, function(i, form) {

                    var li = jQuery('<li>');
                    li.append(jQuery('<input>', {
                        'class': 'include-forms',
                        'type': 'checkbox', 'checked': false,
                        'id': 'stat' + form.form_id,
                        'name': 'stations[]',
                        'value': form.form_id
                    }));

                    li.append(jQuery('<label>', {
                        'for': 'stat' + form.form_id,
                        'text': form.form_name
                    }));

                    jQuery('#form-list').append(li);

                    });
                } else {

                    jQuery('#form-list')
                    .empty()
                    .append(jQuery('<li>', {
                        'class': 'red',
                        'text': 'No records found'
                    }));

                }

                // Add events
                addFormsEvents();
             })
             .fail(function(jqXHR) {
                alert('Could not get requested data, please try again or refresh page');
             });

         });

     }

     // Importing department dropdown and file selection
     if (jQuery('#term-im').length && jQuery('#dept-im').length && jQuery('#file').length) {

         jQuery('#term-im, #dept-im, #file').on('change', function() {

             var ready = (jQuery('#term-im').val().length > 0 && jQuery('#dept-im').val().length > 0 && jQuery('#file').val().length > 0);
             jQuery('#import-button').prop('disabled', !ready);

         });

         // Preview station events
         if (jQuery('#tbi').length) {

          var previewIcons = jQuery('#tbi').find('a[class=previewa]');

          previewIcons.each(function(b) {

             previewEventForm(b);

          });

        }

     }

     // Import submit wait event
     jQuery('#import-button').click(function() {

         jQuery('#import-button').css('color', '#ff0000').val('importing..');

     });

     showMenu();

 });

 // Preview form event
 function previewEventForm(button) {

     jQuery(button).on('click', function(event) {

         event.preventDefault();
         var elementID = jQuery(this).attr('id');
         var elementArr = elementID.split('-');
         var formID = elementArr[1];
         winpop = open(
             'manage.php?page=previewform_osce&form_id=' + formID,
             '_form' + formID);
         winpop.focus();

     });

 }

 // Add forms check events
 function addFormsEvents() {

     jQuery('#form-list').on('click', '.include-forms', function(e) {

         var isDisabled = (jQuery('#form-list').find('input[class=include-forms]:checked').length === 0);
         jQuery('#export-button').prop('disabled', isDisabled);

     });

 }
