function ResultsCount(dateFromSelector, dateToSelector, resultSelector) {

    var getDateValue = function(selector) {
        var dateTxt = jQuery(selector).val().trim();

        if (dateTxt === '') return null;

        var date = moment(dateTxt, "DD/MM/YYYY");

        if (!date.isValid())
            throw "Invalid value for date \"" + dateTxt + "\"";

        return date.unix();
    };

    this.updateResultsCount = function() {
        try {
            var dateFromMs = getDateValue(dateFromSelector);
            var dateToMs = getDateValue(dateToSelector);
        } catch (err) {
            alert(err);
            return;
        }

        jQuery.get('admins/ajx_admin_data.php', {
            q: 'results_count',
            dateFrom: dateFromMs,
            dateTo: dateToMs
        }, function(response) {
            jQuery(resultSelector).text(JSON.parse(response).count);
        });
    };

    // Create date pickers
    var dateFormat = "dd/mm/yy";
    var from = jQuery(dateFromSelector).datepicker({
        defaultDate: "-1d",
        changeMonth: true,
        dateFormat: dateFormat
    }).on("change", function() {
        to.datepicker("option", "minDate", getDate(this));
    });

    var to = jQuery(dateToSelector).datepicker({
        defaultDate: "+0d",
        changeMonth: true,
        dateFormat: dateFormat
    }).on("change", function() {
        from.datepicker("option", "maxDate", getDate(this));
    });

    var getDate = function(element) {
        var date;
        try {
            date = jQuery.datepicker.parseDate(dateFormat, element.value);
        } catch(error) {
            date = null;
        }

        return date;
    };
}

function superAdminDataMain() {
    var resultsCount = new ResultsCount('#txt-date-from', '#txt-date-to', '#submitted-results-count');

    // Add event to update scoresheet count
    jQuery('#btn-update-scoresheets-count').click(resultsCount.updateResultsCount.bind(resultsCount));

    // Get the initial value
    resultsCount.updateResultsCount();
}

jQuery(superAdminDataMain);