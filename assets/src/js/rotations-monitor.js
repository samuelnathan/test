/**
 * Utility object that handles the logic of assigning a colour to a examiner
 */
var ColourAssigner = function() {
    var that = this;

    this.availableColours = [
        "#42a5f5",
        "#ef5350",
        "#ab47bc",
        "#00bcd4",
        "#e0cd3e",
        "#7cb342"
    ];

    this.examinerColours = {};

    var colourIndex = 0;

    var nextColour = function() {
        if (colourIndex === that.availableColours.length)
            colourIndex = 0;

        var colour = that.availableColours[colourIndex];

        colourIndex++;

        return colour;
    };

    this.getColourFor = function(examiner) {
        if (!that.examinerColours.hasOwnProperty(examiner))
            that.examinerColours[examiner] = nextColour();

        return that.examinerColours[examiner];
    };
};

var rotationsMonitor = {
    /**
     * Flag that is set to true when the user receives the logout event
     */
    loggedOut: false,

    /**
     * WebSocket server endpoint
     */
    wsEndpoint: undefined,

    /**
     * Sessions the exam has
     */
    examSessions: [],

    /**
     * Key in the student object to select the value to display as Student ID
     */
    studentIdKey: 'student_id',

    /**
     * Reference to the Vue app
     */
    app: undefined
};

/**
 * Scrollspy to show the session that is currently being watched
 */
document.addEventListener("scroll", function() {
    // The app hasn't loaded yet. Do nothing
    if (!rotationsMonitor.app)
        return;

    // Get the session title of each loaded session
    var titles = document.getElementsByClassName("session-title");
    // Current scroll position
    var scrollPosition = window.scrollY;

    // Store the closest title to the scroll position
    var distance = Number.POSITIVE_INFINITY;
    // Session selected as current one
    var selectedSession = null;

    // For each title, check if it's closer than the current selected one. If it is, update it
    for (var i = 0; i < titles.length; i++) {
        var title = titles[i];
        var titleTop = jQuery(title).offset().top;

        // If the title is beyond the current scroll position, ignore this one
        if (titleTop > scrollPosition)
            continue;

        // Check the distance and update the selected one if it's closer
        if (scrollPosition - titleTop < distance) {
            distance = scrollPosition - titleTop;
            selectedSession = title;
        }
    }

    if (selectedSession !== null)
        rotationsMonitor.app.currentSessionId = selectedSession.getAttribute("data-session-id");
});

document.addEventListener("DOMContentLoaded", function() {
    // Get the WS endpoint
    rotationsMonitor.wsEndpoint = document.getElementById("ws-endpoint").value;
    // Get the data of the sessions available
    rotationsMonitor.examSessions = JSON.parse(document.getElementById("exam-sessions").value).map(function (s) {
        s.session_description = he.decode(s.session_description);
        return s;
    });

    rotationsMonitor.studentIdKey = document.getElementById('student-id-key').value;

    // Initialize the Vue app
    rotationsMonitor.app = new Vue({
        el: "#rotations-monitor",

        data: {
            /**
             * List of monitored sessions
             */
            sessions: [],

            /**
             * List of sessions available to monitor that aren't being currently monitored
             */
            availableSessions: rotationsMonitor.examSessions,

            /**
             * ID of the session that is currently being watched
             */
            currentSessionId: undefined,

            /**
             * Selected session to add in the monitoring list
             */
            selectedSession: "",

            /**
             * Whether the student IDs are displayed on the monitor or not
             */
            displayStudentIDs: true,

            /**
             * State of the detail view of a student in a station
             */
            detail: {
                visible: false,     // Whether the detail is visible or not
                data: {},           // Data of the student and station to show in the detail
                logic: null         // Object that handles the logic of the session
            },

            colourAssigner: new ColourAssigner()
        },

        computed: {
            /**
             * Of the sessions being monitored and have an ongoing rotation, select the rotation data
             * @returns {Array}
             */
            ongoingRotations: function() {
                return this.sessions
                    // Filter out the ones that have no ongoing data
                    .filter(function (session) { return session.rotationData.ongoing !== null; })
                    // Select the rotation data
                    .map(function (session) {
                        var rotationsLogic = SessionRotationsPool.sessionRotations(session.rotationData, session.sessionID);
                        var info = {};

                        // Get the data of the ongoing station
                        rotationsLogic.getStation(
                            session.rotationData.ongoing.student_id
                            , session.rotationData.ongoing.station_id
                            , false
                            , info);

                        var rotations = info.previousRotation ? [{
                            rotation: info.previousRotation,
                            rotationNumber: info.rotationNumber - 1,
                            isOngoing: false
                        }] : [];

                        rotations.push({
                            rotation: info.rotation,
                            rotationNumber: info.rotationNumber,
                            isOngoing: true
                        });

                        // Return the data of the rotation needed to show in the monitor table
                        return {
                            rotationsLogic: rotationsLogic,
                            rotations: rotations,
                            group: info.group.group,
                            rotationNumber: info.rotationNumber,
                            stations: session.rotationData.stations,
                            sessionDescription: session.sessionDescription,
                            circuitColour: session.circuitColour
                        };
                    });
            },

            /**
             * Of the ongoing rotations being monitored, get the maximum number of stations that the session they
             * belong to have
             * @returns {*}
             */
            maxNumberOfOngoingStations: function() {
                return this.maxNumberOfStations(this.ongoingRotations.map(function (o) {
                    return o.stations;
                }));
            }
        },

        methods: {
            /**
             * Add a given session to the list of monitored sessions
             * @param session
             * @param callback Function Function to call when the session has been added
             */
            addSession: function(session, callback) {
                var that = this;

                var pushSession = function(result) {
                    // Add the session to the list of displayed sessions
                    that.sessions.push({
                        rotationData: result,
                        sessionID: session.session_id,
                        sessionDescription: session.session_description,
                        sessionDate: new Date(session.session_date),
                        circuitColour: session.circuit_colour || null,
                        hasOngoing: false
                    });

                    // Sort the monitored sessions
                    that.sessions.sort(function (s1, s2) {
                        return s1.sessionID - s2.sessionID;
                    });

                    // Remove it from the list of available sessions
                    that.availableSessions = that.availableSessions.filter(function (s) {
                        return s.session_id !== session.session_id;
                    });

                    // If a callback has been passed, call it
                    if (callback) callback();
                };

                // Try to find it in the cache
                var cached = SessionRotationsPool.getBySessionId(session.session_id);
                if (cached !== null) {
                    pushSession(cached.rotationsData);
                    return;
                }

                // Get the session data
                jQuery.ajax('ajx_rotations.php', {
                    method: 'get',
                    data: {
                        session_id: session.session_id
                    },
                    success: function(result) {
                        // Save the data in the pool to be quickly retrieved later if needed
                        SessionRotationsPool.sessionRotations(result, session.session_id);
                        pushSession(result);
                    }
                });
            },

            /**
             * When a session is selected in the select of available sessions, add it to the list of
             * monitored sessions
             */
            onSelectedSession: function() {
                // Get the selected session and set the selected value as the default one
                var session = this.selectedSession;
                this.selectedSession = "";

                // If the selected options has a value, add the session
                if (session !== null)
                    this.addSession(session);
                // Otherwise, the user selected the "show all" option: add all the remaining sessions
                else
                    this.addAllSessions();
            },

            /**
             * Add all available sessions to the list of monitored sessions
             */
            addAllSessions: function() {
                if (this.availableSessions.length > 0)
                    this.addSession(this.availableSessions[0], this.addAllSessions);
            },

            /**
             * Remove a session from the monitored sessions list
             * @param sessionId
             */
            removeSession: function(sessionId) {
                // Remove it from the list
                this.sessions = this.sessions.filter(function(session) {
                    return session.sessionID !== sessionId;
                });

                // Add it to the available session list
                this.availableSessions.push(rotationsMonitor.examSessions.find(function (session) {
                    return session.session_id === sessionId;
                }));

                // Sort the available list
                this.availableSessions.sort(function(s1, s2) {
                    return s1.session_id - s2.session_id;
                });
            },

            /**
             * When the user selects a monitored session in the left menu, set that session as the currently watched
             * one
             * @param event {MouseEvent}
             */
            onClickSessionMenu: function(event) {
                setTimeout(function() {
                    this.currentSessionId = event.target.getAttribute("data-session-id")
                }.bind(this), 10);
            },

            changeOngoingFor: function(session) {
                this.$set(session, 'hasOngoing', true);
            },

            /**
             * Show the details for a given student-station
             * @param {Object} studentStation
             */
            detailStudentStation: function(studentStation) {
                this.detail.data = studentStation.student;
                this.detail.logic = studentStation.rotationsLogic;
                this.detail.studentIdDisplay = studentStation.studentIdDisplay;

                var showModal = function() {
                    $('#detail-student-modal').modal();
                };

                // If the modal was already rendered, just show it
                if (this.detail.visible) {
                    showModal();
                    return;
                }

                // Otherwise, render it and show it when the element has been created in the DOM
                this.detail.visible = true;
                this.$nextTick(showModal);
            },

            /**
             * Update the position of the details dialog
             * @param event {MouseEvent}
             */
            updateDetailPosition: function(event) {
                // Calculate the distance between the new cursor point and the previous position of the detail
                // dialog
                var distance = Math.sqrt(
                    Math.pow(event.clientX - this.detail.positionX, 2) +
                    Math.pow(event.clientY - this.detail.positionY, 2)
                );

                // If the distance is too small, don't update the position
                if (distance < 0.5)
                    return;

                // Otherwise, update the position of the dialog
                this.detail.positionX = event.clientX;
                this.detail.positionY = event.clientY;
            },

            hideStudentStation: function() {
                this.detail.visible = false;
            },

            maxNumberOfStations: function(sessionsStations) {
                return sessionsStations.reduce(function(max, stations) {
                    return max >= stations.length ? max : stations.length;
                }, 0);
            },

            formatDate: function(sessionDate) {
                return moment(sessionDate).format('DD/MM/YYYY');
            }
        }
    });
});