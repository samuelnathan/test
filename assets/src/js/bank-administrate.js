/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Override :contains selector to make it case insensitive
jQuery.expr[':'].contains = function(a, i, m) {
 return jQuery(a).text().toUpperCase()
     .indexOf(m[3].toUpperCase()) >= 0;
};

jQuery(document).ready(function() {
    jQuery( '.orderlink[data-original-title]' ).tooltip({
        placement: "left",
        track: true,
        html:true,
        classes: {
            "ui-tooltip": "tips4"
          }
      });
      
   // Department and Term elements exist
   if (jQuery('#select-department').length && jQuery('#select-term').length) {
       
     // Add event for Department and Term filtering
     jQuery('#select-department, #select-term').change(function() {
        parent.location = "manage.php?page=forms_osce&dept=" + jQuery('#select-department').val() + 
                          "&term=" + jQuery('#select-term').val();
     });

     // Add checkbox events
     if (jQuery('#check-all').length) {
        addCheckboxEvents('forms');
     }

     // Add Form event
     if (jQuery('#add').length) {
        newFormAddEvent();
     }

     // Add Edit and Preview events
     if (jQuery('#forms_list_form').length) {
        editFormsEvents();
     }
    
     // Search records
     jQuery('#search-records').keyup(function() {
         filterRecords();
         saveFilterCriteria();
     });
   
   }
   
  // Tag management, events auto-suggest
  jQuery('table#forms_table').on('keydown blur', 'input#new-tag-input', function(e) {
      
      var stringValue = jQuery.trim(jQuery(this).val());
      allowedEvent = (e.type == "focusout" || e.which == 13);
      if (stringValue.length > 0 && allowedEvent) {
       
        jQuery('#tags-existing').remove();
        jQuery('#forms-tags').append(
          jQuery('<div>', {
            'class': 'form-tag-linked',
            'text': stringValue
                   .trim()
                   .toLowerCase()
                   .replace(/[_\s]+/g, '-')
                   .replace(/[^-a-z0-9\s]/g, '')
          })
       );
       jQuery(this).val('');
       
     }
     
  });
    
  // Remove tag event
  jQuery('table#forms_table').on('click', 'div.form-tag-linked', function() {
      jQuery(this).remove();
  });
  
  // Tags filtering
  jQuery('ul#filter-tags').on('change', 'input.cb-tags', function() {
      filterRecords();
      saveFilterCriteria();
  });
    
  showMenu();
  performScroll();
  filterRecords();
});

/**
 * Filter assessment form records
 */
function filterRecords() {
    
      var filterCriteria = "";
      var searchString = "";
      if (jQuery('#search-records').length && jQuery('#search-records').val().length > 0) {
         searchString = jQuery('#search-records').val().toLowerCase();
      }
      
      jQuery('input.cb-tags:checked').each(function() {
          var checkedText = jQuery(this).parent('li').find('label').text();
          filterCriteria += "div.form-tag[data-tag='" + checkedText + "'],";
      });
      
      jQuery('table#forms_table tr').each(function() {
            
         var rowID = jQuery(this).attr('id');
         var ignore = ['title-row', 'base-tr', 'first_forms_row', 'no-forms-found'];
         if (jQuery.inArray(rowID, ignore) !== -1) {
            return true;
         }
         
         var noSearch = (searchString.length > 0 && jQuery(this).find("td:contains('" + searchString + "')").length == 0);
         var noFilter = (filterCriteria.length > 0 && jQuery(this).find(filterCriteria.slice(0,-1)).length == 0);
         if (noFilter || noSearch) {
           jQuery(this).hide();
         } else {
           jQuery(this).show();   
         }

     });

     if (jQuery('tr.datarow:visible, tr.datarowsel:visible').length == 0) {
       genSearchEmptyRow();
     } else {
       jQuery('tr#no-forms-found').remove();   
     }
      
}

/**
 * Save filter criteria
 */
function saveFilterCriteria() {
   
   var cbData = jQuery('input.cb-tags:checked').map(function() {
     return jQuery(this).val();
   }).get();
   
   var gotDropdowns = (jQuery('#select-department').val().length > 0 && jQuery('#select-term').val().length > 0);
   if (gotDropdowns) {
      jQuery.ajax({
        url: 'forms/ajx_forms.php',
        type: 'post',
        cache: false,
        data: {
           isfor: 'save_search_criteria',
           department: jQuery('#select-department').val(),
           search_term: jQuery('#search-records').val(),
           tags: cbData.join(',')
         }
       })
       .fail(function(jqXHR) {

         console.log(
            httpStatusCodeHandler(
            jqXHR.status,
             "Could save filter criteria.\n"
         ));

       });
   }
 
}

/**
 * Generate no assessment forms found table row
 * 
 */
function genSearchEmptyRow() {
   if (jQuery('tr#no-forms-found').length > 0 || jQuery('tr#first_forms_row').length > 0) {
     return false;
   }
   jQuery('<tr>', {'id':'no-forms-found'}).append(
       jQuery('<td>', {'colspan': (jQuery('tr#title-row td, tr#title-row th').length)}).append(
       jQuery('<div>', {'id': 'forms_row_div', 'class': 'tablestatus2', 'text': 'No assessment forms found for that search term'})
   )).insertAfter('tr#title-row');
}

// New Form Add Event
function newFormAddEvent() {
    jQuery(document).on('click', '#add',function(e) {
        e.preventDefault();
        e.stopPropagation();
        jQuery('#forms_table').find("input, select, img, a:not([class~=orderlink], [class~=editems])")
                        .css('display', 'none');

        var lastrow;
        if (jQuery('#first_forms_row').length) {
            var fsr = jQuery('#first_forms_row').clone(true, true);
                      jQuery('#first_forms_row').remove();
            lastrow = jQuery('#title-row');
        } else {
            if (jQuery('#count').length) {
                lastrow = jQuery('#forms_row_' + jQuery('#count').val());
            }
        }

        var new_addrow = jQuery('<tr/>', {'id': 'add-row'});
        lastrow.after(new_addrow);

        setWaitStatusRow(new_addrow, 7);
        var url = "forms/ajx_forms.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'add',
                sdept: jQuery('#select-department').val(),
                strm: jQuery('#select-term').val()
            }
         })
         .done(function(html){
            new_addrow.empty().attr('class', 'addrow').html(html);
            jQuery('#add_form_name').focus();
            addButtonsRow();
            newFormSubmitEvent();
            newFormCancelEvent(new_addrow, fsr);
            jQuery('#new-tag').combobox({
               select: function (event, ui) {
                 
                 setTimeout(function() {
                   jQuery('input#new-tag-input').trigger('blur');
                 }, 100);
                 
            }});

            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            new_addrow.remove();
            jQuery('#forms_table').find("input, select, a, img").css('display', '');
            alert('The request failed, please try again');
         });
    });
}

// Submit new form event
function newFormSubmitEvent() {

    jQuery('#add-save').click(function () {

        if (areFieldsCompleteV3(jQuery('#add-row')) && jQuery('#select-department').val().length > 0 && jQuery('#select-term').val().length > 0) {

            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');
            
            var tags = jQuery('div.form-tag-linked').map(function() {
             return jQuery(this).text();
            }).get();

            jQuery.ajax({
                url: "forms/ajx_json_forms.php",
                type: "post",
                data: {
                   mode: 'submit',
                   sn: jQuery('#add_form_name').val(),
                   sd: jQuery('#add_dept').val(),
                   tags: tags.length > 0 ? tags.join(',') : ''
                }
            }).done(function() {

                prepareScroll('scrolltobase');
                parent.location = 'manage.php?page=forms_osce';

            })
            .fail(function(jqXHR) {

                // Resource not found
                if (jqXHR.status == 404) {

                    alert("Certain filter criteria not found, please re-select filters again");
                    refreshPage(false);

                    // Form name already taken
                } else if (jqXHR.status == 410) {

                    var message = "\n\nThe name '" + jQuery('#add_form_name').val().trim()
                        + "' already in use and found in this list, please specify another\n\n"

                    failAdd(message);

                } else {

                    failAdd(
                        httpStatusCodeHandler(jqXHR.status, "Could not add record. ")
                    );

                }

            });

        } else {

            alert("Please complete all fields.");

        }

    });

}

// Edit Events Forms
function editFormsEvents() {
    if (jQuery('#count').length) {
        var buttons = jQuery('#forms_list_form').find('a[id^=edit_]');

        jQuery.each(buttons, function(i, b) {
            editFormEvent(b);
        });
    }
}

// Edit form event
function editFormEvent(button) {
    jQuery(button).off('click').on('click', editFormEventHandler);
}

function editFormEventHandler (event) {
    event.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var form_id = row.find('input[id^=forms-check]').val();
    var fsr = row.clone(true, true);
    jQuery('#forms_table')
            .find("input, img, a:not([class~=orderlink], [class~=editems])")
            .css({'visibility': 'hidden', 'width': '0px'});
    jQuery('#forms_table')
            .find("input[type='checkbox'], #select-div, #add-div")
            .css('display', 'none');
    setWaitStatusRow(jQuery(row), 7);
    var url = "forms/ajx_forms.php";
    var random = Math.random();
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: 'edit',
            station: form_id
        }
     })
     .done(function(html){
        if (html === "FORM_NON_EXIST") {
            alert("Object you are trying to edit was not found " +
                  "in the system, click 'OK' to refresh list");
            refreshPage(false);
        } else {
            row.empty().attr('class', 'editrw')
                       .attr('id', 'edit_row')
                       .html(html);
            editButtonsRow(jQuery(row), 7);
            updateFormCancelEvent(jQuery('#edit-cancel'), fsr, row);
            updateFormEvent();
            jQuery('#new-tag').combobox({
               select: function (event, ui) {

                 setTimeout(function() {
                   jQuery('input#new-tag-input').trigger('blur');
                 }, 100);

            }});
      }
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#forms_table').find("input, img, a").css({'visibility': 'visible', 'width': ''});
        jQuery('#forms_table').find("input[type='checkbox'], #select-div, #add-div").css('display', '');
        editFormEvent(fsr.find('a[id^=edit_]'));
        addCheckboxEvent(fsr.find('input[id^=forms-check]'), 'forms');
        alert('The request failed, please try again');
     });
}

// Update form event
function updateFormEvent() {
 jQuery('#edit-update').on('click', function (event) {
    event.preventDefault();
    if (areFieldsCompleteV3(jQuery('#edit_row'))) {

        jQuery('#edit-update').css('display', 'none');
        jQuery('#edit-cancel').css('display', 'none');
        jQuery('#b_edit_td').attr('class', 'waittd2');

        var tags = jQuery('div.form-tag-linked').map(function() {
         return jQuery(this).text();
        }).get();

        var url = "forms/ajx_forms.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'update',
                stat_id: jQuery('#edit_form_id').val(),
                stat_name: jQuery('#edit_form_name').val(),
                stat_dept: jQuery('#edit_dept').val(),
                stat_complete: Number(jQuery('#edit_complete').prop('checked')),
                tags: tags.length > 0 ? tags.join(',') : ''
            }
         })
         .done(function(html){
            switch (html)
            {
               case 'FORM_NON_EXIST':
                   jQuery('#b_edit_td').attr('class', 'editbts');
                   failEdit();
                   alert("Object you are trying to update was not " +
                           "found in the system, please refresh list [F5 Key]");
                   break;
               case 'DEPT_NON_EXIST':
                   jQuery('#b_edit_td').attr('class', 'editbts');
                   failEdit();
                   alert("The filter option or options you have chosen were not " +
                           "found in the system, please refresh page [F5 key]");
                   break;
               default:
                   refreshPage(false);
            }
         })
         .fail(function(jqXHR) {
            failEdit();
            alert('The request failed, Please try again');
         });
    } else {
        alert("Please complete all fields.");
    }
  });
}

// Cancel Update Form Event
function updateFormCancelEvent(button, org, oldrow) {

    button.on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        jQuery('#b_edit').remove();
        org.replaceAll(oldrow);
        jQuery('#forms_table').find("input, img, a").css({'visibility': 'visible', 'width': ''});
        jQuery('#forms_table').find("input[type='checkbox'], #select-div, #add-div").css('display', '');
        editFormEvent(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=forms-check]'), 'forms');
    });
}

// New Form Cancel Event
function newFormCancelEvent(row, fsr) {
    jQuery(document).on('click', '#add-cancel', function(e) {
        e.preventDefault();
        e.stopPropagation();

        if (!jQuery('#count').length) {
            fsr.replaceAll(row);
        } else {
            row.remove();
        }
        jQuery('#forms_table').find("select, img, input, a").css('display', '');
        removeNewRecordControls();
    });
}

// Validate Html Form
function ValidateForm() {

   // Only for deletion
   if (jQuery('#todo').val() === "delete") { 
      return confirm("Are you sure you want to delete these records");
   }
 
   return true;
   
}