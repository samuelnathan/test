/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

var goClicked = false;

jQuery(document).ready(function() {
        jQuery('#select-department, #select-term, #select-accessible').change(function() {
            parent.location = "manage.php?page=manage_osce"
                            + "&dept=" + jQuery('#select-department').val()
                            + "&term=" + jQuery('#select-term').val()
                            + "&access=" + jQuery('#select-accessible').val();
        });

    if (jQuery('#check-all').length) {
        addCheckboxEvents('exam');
    }

    if (jQuery('#add').length) {
        addExamEvent();
    }

    if (jQuery('#exam_list_form').length) {
        editEvents();
    }

    // handler for the go button click event called
    jQuery("#go-exam").click(function() {
       goClicked = true;
    });

    showMenu();
    performScroll();

    // Check for results updates for all displayed exams (Every 30 seconds)
    setInterval(function() {

      var listedExams = [];
      jQuery('.exam-records').each(function(examIndex) {
          listedExams[examIndex] = jQuery(this).val();
      });

      // Skip if not found
      if (listedExams.length == 0) {
         return true;
      }

      jQuery.ajax({
         url: 'osce/json_exams.php',
         type: 'post',
         dataType: 'json',
         cache: false,
         data: {
            request: 'exam_results_status',
            exams: listedExams
         }
        })
        .done(function(exams) {

            if (Object.keys(exams).length > 0) {

                jQuery('.exam-records').each(function() {
                    var examRecordID = jQuery(this).val();

                    if (exams[examRecordID] == true) {
                       jQuery('#exam-results-link-'+examRecordID).removeClass('invisible');
                    } else {
                       jQuery('#exam-results-link-'+examRecordID).addClass('invisible');
                    }

                });

            }

        })
        .fail(function(jqXHR) {

            console.log(
                httpStatusCodeHandler(
                jqXHR.status,
                "Could not retrieve exam results status.\n"
            ));

        });

    }, 20000);

});

// Add exam event
function addExamEvent() {
    jQuery('#add').on('click', function(e) {
        e.preventDefault();

        var elements = "select, input, a:not(.tdlink), img, button";
        jQuery('#exam_table').find(elements).css('display', 'none');

        if (jQuery('.exam-matrix').length) {
            jQuery('.exam-matrix').prop('disabled', true);
        }

        var lastrow = null;
        if (jQuery('#first_exam_row').length) {
            var fsr = jQuery('#first_exam_row').remove();
            lastrow = jQuery('#title-row');
        } else if (jQuery('#count').length) {
            lastrow = jQuery('#exam_row_' + jQuery('#count').val());
        }

        var new_addrow = jQuery('<tr>', {'id': 'add-row'});
        lastrow.after(new_addrow);

        setWaitStatusRow(jQuery(new_addrow), 5);

        var url = "osce/ajx_exams.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                mode: 'add'
            }
         })
         .done(function(html){
            new_addrow.empty().attr('class', 'addrow');
            new_addrow.get(0).innerHTML = html;
            addButtonsRow();
            submitExamEvent();
            cancelSubmitEvent(new_addrow, fsr);
            jQuery('#add_name').focus();
            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            new_addrow.remove();
            jQuery('#exam_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

// Submit exam event
function submitExamEvent() {

    jQuery('#add-save').on('click', function(e) {
        e.preventDefault();

        if (areFieldsCompleteV2(jQuery('#add-row')) &&
            jQuery('#select-department').val().length > 0 && jQuery('#select-term').val().length > 0) {

            var url = "osce/ajx_exams.php";
            var random = Math.random();

            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    mode: 'submit',
                    add_name: jQuery('#add_name').val(),
                    add_desc: jQuery('#add_desc').val(),
                    add_published: Number(jQuery('#add-published').prop('checked')),
                    add_viewresults: Number(jQuery('#add-viewresults').prop('checked')),
                    add_viewmarks: Number(jQuery('#add-viewmarks').prop('checked')),
                    add_viewtotals: Number(jQuery('#add-viewtotals').prop('checked')),
                    add_submitnext: Number(jQuery('#add-submitnext').prop('checked')),
                    add_countdown: Number(jQuery('#add-countdown').prop('checked')),
                    add_examinersdefault: jQuery('#add-examinersdefault').val(),
                    dept: jQuery('#select-department').val(),
                    grade: jQuery('#grade-rule').val()
                }
             })
             .done(function(html){
                if (html === "DEPT_NON_EXIST") {

                    failAdd();
                    alert('Selected filter criteria combination no longer found in the system, ' +
                        'please refresh page [F5 key] and update filter criteria');

                } else {

                    prepareScroll('scrolltobase');
                    refreshPage(false);

                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });

        } else {

            alert('Please complete all fields');

        }
    });
}

// Cancel submit event
function cancelSubmitEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();

        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#exam_table').find("select, img, input, a:not(.exam-early-warning), button").css('display', '');

        if (jQuery('.exam-matrix').length) {
            jQuery('.exam-matrix').prop('disabled', false);
        }

        removeNewRecordControls();
    });
}

// Edit Events
function editEvents() {
    if (jQuery('#count').length) {
        var buttons = jQuery('#exam_list_form').find('a[id^=edit_]');

        jQuery.each(buttons, function(i, b) {
            editEvent(b);
        });
    }
}

// Each edit event
function editEvent(b) {
    jQuery(b).off('click', editEventHandler).on('click', editEventHandler);
}

function editEventHandler(e) {

    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var exam_id = row.find('input[id^=exam-check]').val();
    var fsr = row.clone(true, true);
    var elements = "select, input, a:not(.tdlink), button, img";
    jQuery('#exam_table').find(elements).css('display', 'none');

    if (jQuery('.exam-matrix').length) {
        jQuery('.exam-matrix').prop('disabled', true);
    }

    setWaitStatusRow(jQuery(row), 5);
    var url = "osce/ajx_exams.php";
    var random = Math.random();
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            mode: 'edit',
            exam_id: exam_id
        }
     })
     .done(function(html){
        if (html === "EXAM_NON_EXIST") {

            alert("Record you are trying to edit was not found in the system, click 'OK' to refresh List");
            refreshPage(false);

        } else {

            row.empty().attr('class', 'editrw')
            .attr('id', 'edit_row').get(0).innerHTML = html;
            editButtonsRow(jQuery(row), 5);
            cancelUpdateEvent(jQuery('#edit-cancel'), fsr, row);
            updateEvent();

        }
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#exam_table').find("input, select, a, img").css('display', '');
        editEvent(fsr.find('a[id^=edit_]'));
        addCheckboxEvent(fsr.find('input[id^=exam-check]'), 'exam');
        alert('The request failed, please try again');
     });

}
// Update event
function updateEvent() {

    jQuery('#edit-update').on('click', function(e) {
        e.preventDefault();

        if (areFieldsCompleteV2(jQuery('#edit_row'))) {

            jQuery('#edit-update').css('display', 'none');
            jQuery('#edit-cancel').css('display', 'none');
            jQuery('#b_edit_td').attr('class', 'waittd2');

            var url = "osce/ajx_exams.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    mode: 'update',
                    edit_id: jQuery('#edit_id').val(),
                    edit_name: jQuery('#edit_name').val(),
                    edit_desc: jQuery('#edit_desc').val(),
                    edit_published: Number(jQuery('#edit-published').prop('checked')),
                    edit_viewresults: Number(jQuery('#edit-viewresults').prop('checked')),
                    edit_viewmarks: Number(jQuery('#edit-viewmarks').prop('checked')),
                    edit_viewtotals: Number(jQuery('#edit-viewtotals').prop('checked')),
                    edit_submitnext: Number(jQuery('#edit-submitnext').prop('checked')),
                    edit_countdown: Number(jQuery('#edit-countdown').prop('checked')),
                    edit_examinersdefault: jQuery('#edit-examinersdefault').val(),
                    grade: jQuery('#grade-rule').val()
                }
             })
             .done(function(html){
                if (html === "EXAM_NON_EXIST") {

                    jQuery('#b_edit_td').attr('class', 'editbts');
                    alert("Record you are updating was not found in the system, " +
                          "the list will now be refreshed");

                }

                refreshPage(false);
             })
             .fail(function(jqXHR) {
                failEdit();
                alert('The request failed, Please try again');
             });

        } else {

            alert("Please complete all fields.");

        }

    });

}

// Cancel update event
function cancelUpdateEvent(button, org, oldrow, br) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#exam_table').find("input, a:not(.exam-early-warning), img, select, button").css('display', '');

        if (jQuery('.exam-matrix').length) {
            jQuery('.exam-matrix').prop('disabled', false);
        }

        editEvent(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=exam-check]'), 'exam');
    });
}

// Validate Form
function ValidateForm(form)
{
    // Exams Management
    if (goClicked && jQuery(form).attr('name') === 'exam_list_form') {
        goClicked = false;
        return confirm("Are you sure you would like to delete these records");
    }

    goClicked = false;
}
