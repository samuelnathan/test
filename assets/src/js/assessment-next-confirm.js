/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */


$(document).ready(function() {
    
   $("#no-confirm").click(function() {
     parent.location = "assess/discard.php"; 
   });
      
   // Check to make sure we have an internet conenction before proceeding to form
   $("#next-form").submit(function() {
      if (isOfflineAlert()) {
           return false; 
      }       
   });
  
});

$(window).on('load', function() {
   
   // Make the "To Examine" and "Examined" fieldsets of equal width
   if ($(".toexam").length && $(".examined").length) {
       var toExamWidth = $(".toexam").width();
       var examinedWidth = $(".examined").width();
       var maxWidth = Math.max(toExamWidth, examinedWidth) + 15;
       
       $(".toexam").width(maxWidth);
       $(".examined").width(maxWidth);
       
   }   
    
});