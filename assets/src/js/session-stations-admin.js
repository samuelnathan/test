/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2018 Qpercom Limited.  All rights reserved.
 */

var columnCount = 9;
jQuery(document).ready(function() {
    if (jQuery('.return').length) {
        jQuery('.return').on('click', function() {
            var newHash = '';
            var exam_id = jQuery('#exam_id').val();
            if(!exam_id || exam_id == ''){
                exam_id = jQuery('#exam-id').val();
            }
            var newParams = {
                page: 'sessions_osce',
                exam_id: exam_id
            }
            if (jQuery('#show_session').length && jQuery('#show_session').val().length > 0) {
                newParams['show_session'] = jQuery('#show_session').val()
            } else {
                newHash = '#' + 'X' + jQuery('#session_id').val();
            }
            window.location.href = window.location.pathname + '?' + jQuery.param(newParams) + newHash;
        });
    }

    if (jQuery('#check-all').length) {
        addCheckboxEvents('stations');
    }

    if (jQuery('#add').length) {
        addStationEvent();
    }

    if (jQuery('#stations_list_form').length) {
        editStationsEvents();
    }

    // Adjust column span for base row in case that some columns are hidden (features disabled)
    columnCount = jQuery("#title-row th:not('.hide')").length;
    jQuery('#base-button-td').attr('colspan', columnCount+1);

    /**
     * Tooltip content for tagging explained
     */
    var tooltipContent = jQuery("#tagging-explained").text();
    jQuery("#tagging-info").prop('title', '<tooltiper>'+tooltipContent+'</tooltiper>');
    jQuery("#tagging-info, .tags-info").tooltip({
      placement: "left",
      html: true,
      track: true,
      delay: { show: 100, hide: 2000 },
      classes: {
        "ui-tooltip": "ui-corner-all"
      },
      show: {
        effect: "slideDown",
        delay: 250
      },
      content: function () {
        return jQuery(this).prop('title');
      }
    });

    showMenu();
    performScroll();

});

// Add AutoComplete
function addAutoComplete(type) {

    var exam_tags = jQuery('#exam_tags').val().split(',');
    var input = '#' + type + '_tag';
    jQuery(input).autocomplete({
        source: exam_tags,
        minLength: 0
      });

    var secondClick = false;
    jQuery(document).on('click', input, function(){
        if(secondClick){
            jQuery(input).autocomplete( "search", "" );
            secondClick = false;
        }else{
            secondClick = true;
        }
    });
}

// add button event
function addStationEvent() {
    var lastrow;

    jQuery('#add').on('click', function(e) {
        e.preventDefault();
        jQuery('#stations_table').find("input, select, img[class!=tags-info], a[class!=linkin]").css('display', 'none');

        if (jQuery('#first_stations_row').length) {
            var fsr = jQuery('#first_stations_row').remove();
            lastrow = jQuery('#title-row');
            lastrow.find('label').css('visibility', 'hidden');
        } else {
            if (jQuery('#count').length) {
                lastrow = jQuery('#stations_row_' + jQuery('#count').val());
            }
        }

        var new_addrow = jQuery('<tr>', {'id': 'add-row'});
        lastrow.after(new_addrow);
        setWaitStatusRow(jQuery(new_addrow), columnCount);

        var url = "osce/ajx_stations.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'add',
                session_id: jQuery('#session_id').val()
            }
         })
         .done(function(html){
            if (html !== "SESSION_NON_EXIST") {
                new_addrow.empty().attr('class', 'addrow').html(html);

                // Adjust colspan for options panel cell
                jQuery('#options-panel').attr('colspan', columnCount+2);

                addButtonsRow();
                submitStationEvent();
                prepareStationSourceEvents();
                examSelectEvent();
                sessionSelectEvent();
                addStationCancelEvent(new_addrow, fsr);
                jQuery('#add-row').prev().addClass("nbbtr");
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
            } else {
                var message = "The record thst you are trying to add a station to was not "
                            + "found in the system, please return to main list"
                alert(message);
                refreshPage(false);
            }

            if(!jQuery('#crclone').prop('checked')){
               addAutoComplete('add');
            }

            // Add Rest Station Checkbox Event
            if(jQuery('#crnew').prop('checked')) {
               addRestStationEvent();
            }
         })
         .fail(function(jqXHR) {
            new_addrow.remove();
            jQuery('#stations_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

 // Prepare station source radio button events
 function prepareStationSourceEvents() {

    if (jQuery('#add-row').length && jQuery('#station-add-panel').length) {

            jQuery("input[type=radio][name=createact]").change(function() {

            var url = "osce/";
            var optID = jQuery(this).attr('id');

            // New scoresheet
            if (optID === 'crnew') {

                url += "new_form.php";

            // Use form from assessment form bank
            } else if (optID === 'crold') {

                url += "form_bank.php";

            // Use single form from previous session
            } else if (optID === 'crprev') {

                url += "single_form_previous_session.php";

            // Copy stations from previous session
            } else if (optID === 'crclone') {

                url += "copy_stations_previous_session.php";

            }

            jQuery('#station-add-panel').attr('class', 'waitv6');
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: Math.random(),
                    session_id: jQuery('#session_id').val(),
                    isajax: 'isajax'
                }
             })
             .done(function(html){
                jQuery('#station-add-panel').removeClass('waitv6');
                jQuery('#station-add-panel').html(html);

                if (optID === 'crnew') {

                    addRestStationEvent();

                }

                if (optID === 'crclone' || optID === 'crprev') {

                    examSelectEvent();

                }

                if (optID === 'crprev') {

                    sessionSelectEvent();

                }

                if (optID !== 'crclone') {

                    addAutoComplete('add');

                }

                setTimeout(function() {
                    jQuery(".tags-info").tooltip({
                        placement: "left",
                        html: true,
                        track: true,
                        delay: { show: 100, hide: 2000 },
                        classes: {
                          "ui-tooltip": "ui-corner-all"
                        },
                        show: {
                          effect: "slideDown",
                          delay: 250
                        },
                        content: function () {
                          return jQuery(this).prop('title');
                        }
                      });
                }, 500);
             })
             .fail(function(jqXHR) {
                alert('The request failed, Please try again');
             });

        });

    }

 }

/* For Exam Change Event Add Stations Section */
function examSelectEvent() {

    if (jQuery('#add_exam').length) {

        jQuery('#add_exam').on('change', function() {

            emptyDropdown(jQuery('#clone-stations'), '', '--');

            // Only for previous station section
            emptyDropdown(jQuery('#add_stat'), '', '--');

            var cloneExam = jQuery(this).val();

            if (cloneExam.length > 0) {

                simpleWaitOn(jQuery('#clone-stations'), 'type1');

                jQuery.ajax({
                    url: "osce/json_stations.php",
                    type: "post",
                    dataType: "json",
                    data: {
                        isfor: 'get_sessions',
                        exam: cloneExam
                    }
                 })
                 .done(function(data) {

                     simpleWaitOff(jQuery('#clone-stations'));

                    jQuery.each(data, function(index, td) {

                        var stationcount_str = '&nbsp;&nbsp;&nbsp;[' + td.stationcount.trim() + ' Station';

                        if (td.stationcount.trim() > 1) {

                            stationcount_str += 's]';

                        } else {

                            stationcount_str += ']';

                        }

                        jQuery('#clone-stations')
                              .append(jQuery('<option>', {'value': td.sessionid.trim()})
                              .html(td.sessiondate.trim() + ' ' + td.sessiondesc.trim() + stationcount_str));

                    });

                })
                 .fail(function(jqXHR) {
                    var message = "Failed to get records list.\n";
                    switch (jqXHR.status) {
                        case 404:
                        case 500:
                            message += "An internal error occurred";
                            break;
                        case 400:
                            message += "Update request was invalid.";
                            break;
                        case 403:
                            message += "Sorry, You may not be logged in or your session may have expired";
                            break;
                        default:
                            message += "An unspecified error has occurred. Contact your system administrator.";
                            message += "\n(HTTP Code: " + jqXHR.status + ")";
                    }

                    alert(message);

                 });

            }

        });

    }

}

/* For Session Change Event Add Stations Section */
function sessionSelectEvent() {
    if (jQuery('#clone-stations').length) {
        jQuery('#clone-stations').on('change', function() {

            emptyDropdown(jQuery('#add_stat'), '', '--');
            var sessionSelected = jQuery(this).val();
            if (sessionSelected.length > 0) {
                simpleWaitOn(jQuery('#add_stat'), 'type1');
                jQuery.ajax({
                    url:  "osce/json_stations.php",
                    cache: false,
                    dataType: "json",
                    type: "post",
                    dataType: "json",
                    data: {
                        isfor: 'get_stations',
                        current_session_id: jQuery('#session_id').val(),
                        selected_session_id: sessionSelected
                    }
                 })
                 .done(function(sts) {

                    simpleWaitOff(jQuery('#add_stat'));

                    jQuery.each(sts, function(i, st) {

                        jQuery('#add_stat')
                        .append(jQuery('<option>', {'value': st.formid.trim()})
                        .text(st.stationnum.trim() + ' - ' + st.formname.trim()));

                    });

                 })
                 .fail(function(jqXHR) {
                    var message = "Failed to get records list.\n";
                    switch (jqXHR.status) {
                        case 404:
                        case 500:
                            message += "An internal error occurred";
                            break;
                        case 400:
                            message += "Update request was invalid.";
                            break;
                        case 403:
                            message += "Sorry, You may not be logged in or your session may have expired";
                            break;
                        default:
                            message += "An unspecified error has occurred. Contact your system administrator.";
                            message += "\n(HTTP Code: " + jqXHR.status + ")";
                    }

                    alert(message);
                 });
            }
        });
    }
}

// Submit station event
function submitStationEvent() {

    jQuery('#add-save').click(function() {

        if (validateStationRow('add')) {

            jQuery(this).css('display', 'none');
            jQuery("#add-cancel").css('display', 'none');
            jQuery("#base-button-td").attr('class', 'waittd3');

            var postData = {
                'mode': 'submit',
                'session_id': jQuery("#session_id").val()
            };

            // If cloning stations from other session
            if (jQuery("#selectiontype").val() === 'clone_stations_exam') {

                 jQuery.extend(postData, {'selectiontype': "clone_stations_exam"});
                 jQuery.extend(postData, {'clone_session_id': jQuery("#clone-stations").val()});

            } else {

                // If choosing the form found to have same name as one entered
                if (jQuery("#form-existing").length && jQuery("#form-existing").is(':checked')
                    && jQuery("#used-form-id").length && jQuery("#used-form-id").val().length > 0) {

                    jQuery.extend(postData, {'selectiontype': "bank_station"});
                    jQuery.extend(postData, {'add_stat': jQuery("#used-form-id").val()});

                } else {

                    jQuery.extend(postData, {'selectiontype': jQuery("#selectiontype").val()});
                    jQuery.extend(postData, {'add_stat': jQuery('#add_stat').val()});

                }

                // Rest Station checked
                var restStation = +(jQuery('#add_rest').length && jQuery('#add_rest').prop('checked'));

                // Clean tag
                var tagValue = (jQuery('#add_tag').val()).replace(/[^a-zA-Z0-9\/#-]+/g, '');

                jQuery.extend(postData, {'add_nr': jQuery('#add_nr').val()});
                jQuery.extend(postData, {'add_rest': restStation});
                jQuery.extend(postData, {'add_tag': tagValue});
                jQuery.extend(postData, {'add_pass': jQuery('#add_pass').val()});
                jQuery.extend(postData, {'add_time': jQuery('#add-hrs').val()
                    + ':' + jQuery('#add-mins').val()
                    + ':' + jQuery('#add-secs').val()});

                jQuery.extend(postData, {'add_alarm1': jQuery('#add-alarm1-hrs').val()
                    + ':' + jQuery('#add-alarm1-mins').val()
                    + ':' + jQuery('#add-alarm1-secs').val()});

                jQuery.extend(postData, {'add_alarm2': jQuery('#add-alarm2-hrs').val()
                    + ':' + jQuery('#add-alarm2-mins').val()
                    + ':' + jQuery('#add-alarm2-secs').val()});

                jQuery.extend(postData, {'add_pop': ((jQuery('#add_pop').length) ? jQuery('#add_pop').val() : 0)});
                jQuery.extend(postData, {'exclude_grade': ((jQuery('#exclude-grade').length) ? jQuery('#exclude-grade').val() : 0)});
                jQuery.extend(postData, {'weighting': jQuery('#weighting').val()});

            }

            jQuery.ajax({
                url: "osce/ajx_json_stations.php",
                type: "post",
                data: postData,
                dataType: "json"
            })
            .done(function() {

                prepareScroll('scrolltobase');
                refreshPage(false);

             })
            .fail(function(jqXHR) {

                // Resource not found
                if (jqXHR.status == 404) {

                    alert("The record that you are trying to add a station to " +
                        "was not found in the system, please return to main list");
                    refreshPage(false);

                // Form name already taken
                } else if (jqXHR.status == 400 || jqXHR.status == 410) {

                    failAdd();

                    if (jqXHR.status == 400) {

                        jQuery("#used-form-id").val(jqXHR.responseJSON.form_id);

                    }

                    nameTakenAction(jqXHR.status);

                } else {

                    failAdd();
                    alert(httpStatusCodeHandler(jqXHR.status, "Could not add record. "));

                }

            });

        }

    });

}

/**
 * Open name taken dialog
 *
 * @param integer Http status number
 */
function nameTakenAction(httpStatus) {

    // Grab the form name to display
    jQuery(".assess-form-name").text(
        jQuery("#add_stat").val().trim()
    );

    // Open dialog to inform user
    jQuery("#name-taken-confirm").dialog({
        open: function() {

            // Session form name taken
            if (httpStatus == 410) {

                jQuery("#exists-circuit").show();
                jQuery("#exists-dept-term").hide();
                jQuery("#form-existing-li").hide();
                jQuery("#form-new").prop("checked", true);
                jQuery("#new-form-name").prop('disabled', false)

            } else {

                jQuery("#form-existing").prop("checked", true);
                jQuery("#new-form-name").prop('disabled', true);
                jQuery("#exists-circuit").hide();
                jQuery("#exists-dept-term").show();
                jQuery("#form-existing-li").show();

            }

        },
        resizable: false,
        width: 600,
        height: 250,
        modal: true,
        buttons: {
            "Cancel": function() {

                jQuery(this).dialog("close");
                jQuery("#add_stat").focus();
                jQuery("#used-form-id").val("");
                jQuery('#form-name-warm').hide();

            },
            "Proceed": function() {

                if (jQuery("#form-new").is(':checked')) {

                    if (jQuery("#new-form-name").val().length == 0) {

                        jQuery('#form-name-warm').show();
                        jQuery("#new-form-name").focus();
                        return false;

                    }

                    jQuery("#add_stat").val(
                        jQuery("#new-form-name").val()
                    );

                }

                jQuery(this).dialog('close');
                jQuery('#add-save').trigger('click');

            }
        }
    });

    // Enable or disable form name entry field
    jQuery("input[name='specify_form']").change(function() {

        jQuery("#new-form-name").prop(
            'disabled',
            !jQuery("#form-new").is(':checked')
        ).val("");

        if (jQuery("#form-existing").is(':checked'))
            jQuery('#form-name-warm').hide();

    });

}

// Add Station Cancel Event
function addStationCancelEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();
        jQuery('#add-row').prev().removeClass('nbbtr');
        jQuery('#title-row').find('label').css('visibility', 'visible');
        if (jQuery('#count').val() == 0) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#stations_table').find("select, img, input, a").css('display', '');
        jQuery('#add-save').remove();
        jQuery('#add').prop('disabled', false);
        jQuery(this).remove();
    });
}

// Edit Stations Events
function editStationsEvents() {
    if (jQuery('#count').val() > 0) {
        var buttons = jQuery('#stations_list_form').find('a[id^=edit_]');

        jQuery.each(buttons, function(i, b) {
            editStationEvent(b);
        });
    }
}

// Edit Station Event
function editStationEvent(b) {

    jQuery(b).off('click', editStationEventHandler).on('click', editStationEventHandler);

}

function editStationEventHandler (e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var station_id = row.find('input[id^=stations-check]').val();
    var fsr = row.clone(true, true);

    jQuery('#stations_table').find("input, img[class!=tags-info], a[class!=linkin]").css({'visibility': 'hidden', 'width': '0px'});
    jQuery('#stations_table').find("input[type='checkbox'], #select-div, #add-div").css('display', 'none');

    setWaitStatusRow(jQuery(row), columnCount);
    jQuery.ajax({
        url: "osce/ajx_stations.php",
        type: "post",
        data: {
            sid: Math.random(),
            isfor: 'edit',
            station_id: station_id
        }
     })
     .done(function(html){
        if (html === "STATION_NON_EXIST") {
            alert(
             "The station you are trying to edit was not " +
             "found in the system, the list will now be refreshed"
            );
            refreshPage(false);
        } else {
            row.empty().attr('class', 'editrw').attr('id', 'edit_row').html(html);
            editButtonsRow(jQuery(row), columnCount);
            updateStationCancelEvent(jQuery('#edit-cancel'), fsr, row);
            updateStationEvent();
        }

        addAutoComplete('edit');
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#stations_table').find("input, img, a[class!=linkin]").css({'visibility': 'visible', 'width': ''});
        jQuery('#stations_table').find("input[type='checkbox'], #select-div, #add-div").css('display', '');
        editStationEvent(fsr.find('a[id^=edit_]'));
        addCheckboxEvent(fsr.find('input[id^=stations-check]'), 'stations');
        alert('The request failed, Please try again');
     });
}

// Update Station Event
function updateStationEvent() {
    jQuery('#edit-update').on('click', function(e) {

        e.preventDefault();
        if (validateStationRow('edit')) {
            jQuery('#edit-update').css('display', 'none');
            jQuery('#edit-cancel').css('display', 'none');
            jQuery('#b_edit_td').attr('class', 'waittd2');

            var tagValue = (jQuery('#edit_tag').val()).replace(/[^a-zA-Z0-9\/#-]+/g, '');
            jQuery.ajax({
                url: "osce/ajx_stations.php",
                type: "post",
                data: {
                    sid: Math.random(),
                    isfor: 'update',
                    edit_id: jQuery('#edit_id').val(),
                    edit_nr: jQuery('#edit_nr').val(),
                    edit_tag: tagValue,
                    edit_stat: jQuery('#edit_stat').val(),
                    edit_pass: jQuery('#edit_pass').val(),
                    edit_time: jQuery('#edit-hrs').val() + ':' + jQuery('#edit-mins').val() + ':' + jQuery('#edit-secs').val(),
                    edit_alarm1: jQuery('#edit-alarm1-hrs').val() + ':' + jQuery('#edit-alarm1-mins').val() + ':' + jQuery('#edit-alarm1-secs').val(),
                    edit_alarm2: jQuery('#edit-alarm2-hrs').val() + ':' + jQuery('#edit-alarm2-mins').val() + ':' + jQuery('#edit-alarm2-secs').val(),
                    autopopup: (jQuery('#edit_autopop').length ? Number(jQuery('#edit_autopop').prop('checked')) : 'NO_UPDATE'),
                    exclude_grade: (jQuery('#exclude-grading').length ? Number(jQuery('#exclude-grading').prop('checked')) : 'NO_UPDATE'),
                    weighting: jQuery('#weighting').val()
                }
             })
             .done(function(html){
                if (html === "STATION_NON_EXIST") {
                    failEdit();
                    var message = "The station you are trying to update was not found in the system, "
                                + "the record list will now be refreshed";
                    alert(message);
                    refreshPage(false);
                } else if (html === "STATB_NON_EXIST") {
                    failEdit();
                    var message = "The station type selected from the bank was not "
                                + "found in the system, please select a different station type";
                    alert(message);
                } else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failEdit();
                alert('The request failed, Please try again');
             });
        }
    });
}

// Update Station Cancel Event
function updateStationCancelEvent(button, org, oldrow) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#stations_table').find("input, img, a[class!=linkin]").css({'visibility': 'visible', 'width': ''});
        jQuery('#stations_table').find("input[type='checkbox'], #select-div, #add-div").css('display', '');
        editStationEvent(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=stations-check]'), 'stations');
    });
}

/**
 * Add rest station event for checkbox
 */
function addRestStationEvent() {
   jQuery("#add_rest").change(function() {
    // If checked disable fields not required
    if(jQuery(this).prop("checked")) {
       jQuery(".disable-element")
                   .attr("disabled", true)
                   .css("background-color", "#eee");
    // Else re-enable
    } else {
       jQuery(".disable-element")
                   .attr("disabled", false)
                   .css("background-color", "");

    }

   });
}

// Validate Station Row
function validateStationRow(type) {
    /* If Clone Session Stations */
    if (type === 'add' && jQuery('#selectiontype').val() === 'clone_stations_exam' && jQuery('#clone-stations').length && (jQuery('#clone-stations').val().length === 0 || jQuery('#clone-stations').val() === '--')) {
        alert('Please complete all fields');
        return false;
    } else {
        if (jQuery('#' + type + '_nr').length && jQuery('#' + type + '_nr').val().length === 0) {
            alert('Station Number field is not complete');
            return false;
        }
        // Rest station so no other field needs to be checked. Returns true.
        else if (jQuery('#' + type + '_rest').length && jQuery('#' + type + '_rest').prop("checked")) {
            return true;
        }
        else if (jQuery('#' + type + '_nr').length && isNaN(jQuery('#' + type + '_nr').val())) {
            alert('Station Number field value must be numeric');
            return false;
        } else if (jQuery('#' + type + '_stat').length && (jQuery('#' + type + '_stat').val().length === 0 || jQuery('#' + type + '_stat').val() === '--')) {
            // For Previous Station Section
            alert('Please completed all fields');
            return false;
        } else if (jQuery('#' + type + '_pass').length && (jQuery('#' + type + '_pass').val().length === 0 || (!(jQuery('#' + type + '_pass').val() >= 0 && jQuery('#' + type + '_pass').val() <= 100)))) {
            alert('Station pass value must be between 0 and 100%');
            return false;
        } else if (jQuery('#weighting').length && (jQuery('#weighting').val().length === 0 || (!(jQuery('#weighting').val() >= 0 && jQuery('#weighting').val() <= 100)))) {
        alert('Station weighting value must be between 0 and 100%');
        return false;
       }
    }
    return true;
}

// Validate Manage Stations Form
function ValidateForm(form) {

    //Stations Management
    if (jQuery(form).attr('name') === 'stations_list_form' && (jQuery('#todo').val() === "remove")) {
            return confirm("Are you sure you would like to remove the selected stations");
    }

}
