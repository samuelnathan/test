/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

jQuery(document).ready(function() {
    if (jQuery('#school_table').length) {
        addCheckboxEvents("school");
        schoolsEditEvents();
        schoolAddEvent();
    }
    showMenu();
    performScroll();
});

function schoolsEditEvents() {
    if (jQuery('#school_table').length && jQuery('#count').length) {
        var buttons = jQuery('#school_table').find('a[class=editba]');

        jQuery.each(buttons, function (i, b) {
            schoolEditEvent(b);
        });
    }
}

function schoolEditEvent(b) {
    jQuery(b).off('click', schoolEditEventHandler).on('click', schoolEditEventHandler);
}
function schoolEditEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var school_id = row.find('input[id^=school-check]').val();

    var url = "schools/ajx_schools.php";
    var random = Math.random();

    jQuery('#school_table').find("input, select, img").css('display', 'none');
    setWaitStatusRow(jQuery(row), 2);
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: "edit",
            school_id: school_id
        }
     })
     .done(function(html){
        row.empty().html(html);
        row.attr('class', "editrw");
        row.attr('id', "edit_row");

        editButtonsRow(jQuery(row), 2);
        cancelUpdateEvent(jQuery('#edit-cancel'), fsr, row);
        schoolUpdateEvent(jQuery('#edit-update'));
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        row.empty().remove();
        jQuery('#school_table').find("input, select, img").css('display', '');
        schoolEditEvent(fsr.find('a[class=editba]'));
        addCheckboxEvent(fsr.find('input[id^=school-check]'), "school");

        if (jqXHR.status === 404) {
            jsonResponse = JSON.parse(xhr.responseText);
            alert(jsonResponse.message);
        } else {
            alert('The request failed, Please try again');
        }
     });
}
// update event
function schoolUpdateEvent(b) {
    jQuery(b).on('click', function (e) {
        e.preventDefault();

        if (jQuery('#edit_schooldesc').val().length === 0) {
            alert("Please complete all fields.");
            return;
        }

        var url = "schools/ajx_schools.php";
        var random = Math.random();

        jQuery('#edit-update').css('display', 'none');
        jQuery('#edit-cancel').css('display', 'none');
        jQuery('#b_edit_td').attr('class', 'waittd2');
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "update",
                school_id: jQuery('#school_id').val(),
                school_desc: jQuery('#edit_schooldesc').val()
            }
         })
         .done(function(html){
            refreshPage(false);
         })
         .fail(function(jqXHR) {
            jQuery('#b_edit_td').attr('class', 'editbts');
            jQuery('#edit-update').css('display', '');
            jQuery('#edit-cancel').css('display', '');
            jQuery('#b_edit_td').removeClass('waittd2');
            switch (jqXHR.status) {
                case 403: // Forbidden, a.k.a. Invalid Argument
                case 404: // Not found.
                    jsonResponse = JSON.parse(xhr.responseText);
                    alert(jsonResponse.message);
                    break;
                default:
                    console.log("onFailure(): " + jqXHR.status);
                    alert('The request failed, Please try again');
            }
         });
    });
}

// add cancel update event
function cancelUpdateEvent(button, org, oldrow) {
    jQuery(button).on('click', function (e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        oldrow.empty().remove();
        jQuery('#school_table').find("input, img, select").css('display', '');
        var org_edit = org.find('a[class=editba]');
        var org_cb = org.find('input[id^=school-check]');
        org_cb.css('display', '');

        schoolEditEvent(org_edit);
        addCheckboxEvent(org_cb, "school");
    });
}

// add button event
function schoolAddEvent() {
    var lastrow;

    if (!jQuery('#add').length) {
        return;
    }

    jQuery('#add').on('click', function (e) {
        e.preventDefault();
        jQuery('#school_table').find("input, select, img").css('display', 'none');

        if (jQuery('#first_school_row').length) {
            var fsr = jQuery("#first_school_row").remove();
            lastrow = jQuery("#title-row");
        } else {
            if (jQuery('#count').length) {
                lastrow = jQuery("#school_row_" + jQuery("#count").val());
            }
        }

        var new_addrow = jQuery('<tr>', {'id': 'add-row'});
        lastrow.after(new_addrow);

        setWaitStatusRow(jQuery(new_addrow), 2);

        var url = "schools/ajx_schools.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "add"
            }
         })
         .done(function(html){
            new_addrow.empty().attr('class', 'addrow').html(html);
            addButtonsRow();
            schoolSubmitEvent();
            cancelSubmitEvent(new_addrow, fsr);
            jQuery('#add_schooldesc').focus();
            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            new_addrow.empty().remove();
            jQuery('#school_table').find("input, select, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

// add cancel event
function cancelSubmitEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function (e) {
        e.preventDefault();
        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        }
        row.empty().remove();

        jQuery('#school_table').find("select, img, input").css('display', '');
        removeNewRecordControls();
    });
}

// submit new record event
function schoolSubmitEvent() {
    jQuery('#add-save').on('click', function (e) {
        e.preventDefault();

        if (!areFieldsComplete(jQuery('#add-row'))) {
            alert("Please complete all Fields");
            return;
        }

        jQuery(this).css('display', 'none');
        jQuery('#add-cancel').css('display', 'none');
        jQuery('#base-button-td').attr('class', 'waittd3');

        var url = "schools/ajx_schools.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "submit",
                school_desc: jQuery('#add_schooldesc').val()
            }
         })
         .done(function(html){
            prepareScroll('scrolltobase');
            refreshPage(false);
         })
         .fail(function(jqXHR) {
            failAdd();

            switch (jqXHR.status) {
                case 403: // Forbidden, a.k.a. Invalid Argument
                case 404: // Not found.
                case 500: // Update failed on server.
                    jsonResponse = JSON.parse(xhr.responseText);
                    alert(jsonResponse.message);
                    break;
                default:
                    console.log("onFailure(): " + jqXHR.status);
                    alert('The request failed, Please try again');
            }
         });
    });
}

function ValidateForm(form) {
    if (jQuery(form).attr('name') !== 'school_list_form') return;

    var anyChecked = false;
    var checkBoxes = jQuery('#school_list_form').find('input[id^=school-check]');

    jQuery.each(checkBoxes, function (i, cb) {
        anyChecked |= cb.prop('checked');
    });

    if (anyChecked) {
        return confirm("** WARNING PLEASE READ **\n\n" +
            "Are you sure you want to delete the selected records?\n" +
            "All underlying records linked will be DELETED also!\n\n" +
            "Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n");
    } else {
        return false;
    }
}
