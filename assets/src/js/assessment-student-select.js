/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 */

$(document).ready(function() {
    
    includeManageEvent();
    includeSupportOption();

    // Enable boostrap tooltips
    $('[data-toggle="tooltip"]').tooltip(); 

//Events Forms: Students to Examine, Incomplete Results, Students Examined 
    $('#toassess-form, #incomplete-form, #assessed-form').bind("submit", function() {
         
        if (isOfflineAlert()) {
           return false; 
        }

        var currentID = $(this).attr('id');
        var currentListID = currentID.replace("-form", "");

        var anySelected = false;
        $("." + currentListID + "-input").each(function (_, input) {
            anySelected |= $(input).prop('checked');
        });

        if (!anySelected)
            alert("No candidate has been selected");

        return anySelected;
    });

    // When an item from a student list is selected, hide the buttons of the other lists
    $('.student-input').change(function (e) {
        var listType = $(this).data('listType');
        var allUnchecked = $('.' + listType + '-input:checked').length === 0;

        $('.student-list-buttons').each(function () {
            var buttonsListType = $(this).data('listType');
            var disable = (buttonsListType !== listType) || allUnchecked;

            $(this).css('visibility', disable ? 'hidden' : 'visible');

            $(this).find('input').each(function() {
                $(this).prop('disabled', disable);
            });
        });

        $('.student-input').each(function() {
            var inputsListType = $(this).data('listType');
            var uncheck = inputsListType !== listType;

            if (uncheck)
                $(this).prop('checked', false);
        });
    });

    /* Filter Drop-down Lists */
    $('#st').keyup(function() {
        /* disable Lists and buttons */
        $('.student-input').prop('checked', false);
        $('#dotest, #absentb, #discard, #complete, #retest').prop('disabled', true);

        var searchValue = $(this).val().toLowerCase();
        var regularEx = "(?:.*?)" + (searchValue+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1") + "(?:.*?)";

        /* Filter lists by search term */
        $('.student-selection-student').each(function() {
            var text = $(this).children('label').text().toLowerCase();

            // IE issues with hiding select options
            if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {

                if (!text.match(regularEx, "i")) {

                   $(this).wrap("<span>").hide();

                }

            } else { 

              if (text.match(regularEx, "i")) {

                  $(this).parent().show();

                } else { 

                  $(this).parent().hide();

              }

            }
        });

        // Check if any list is now empty. If it is, hide their button bar
        $('.student-list-buttons').each(function() {
            var listType = $(this).data('listType');
            // Select the list items that are not hidden. If there are 0 of them, the list is empty
            var empty = $('.' + listType + '-selection-list > li:not([style*="display: none"])').length === 0;
            var noneElement = $('.' + listType + '-selection-list > div');

            if (!empty) {
                noneElement.hide();
                return;
            }

            $(this).css('visibility', 'hidden');
            noneElement.show();
        });
    }).focus();

    $(".student-check").parent().parent().click(function () {
        var check = $(this).find('.student-check');
        var isChecked = check.prop('checked');
        check.prop('checked', !isChecked);
        check.change();
    });

    $(".student-radio").parent().parent().click(function () {
       var radio = $(this).find('.student-radio');
       radio.prop('checked', true);
       radio.change();
       $('.li-student').removeClass('student-highlight');
       $(this).toggleClass('student-highlight', true);
    });

    /* Station change */
    $('#station_id')
         .selectmenu({ change: function() {
         parent.location = "assess.php?p=student_selection&stat_sel=" + $(this).val();
    }});

    /* Group change */
    $('#group_id')
         .selectmenu({ change: function() {
         parent.location = "assess.php?p=student_selection&grp_sel=" + $(this).val();
    }});    
   
    /* Examiner change */
    if ($('#examiner-as').length) {
      $('#examiner-as')
           .selectmenu({ change: function() {
           parent.location = "assess.php?p=student_selection&examiner=" + $(this).val();
      }}); 
    }

    /* Absent Button Event - works but need to refactor */
    $('#absentb').click(function() {
        var studentCount = $('.toassess-input:checked').length;

        // One and only one option must be selected
        if (studentCount !== 1) {
            alert("Please select ONE student from the list");
            return;
        }

        if (isOfflineAlert()) {
           return false; 
        }
        
        // Get option value
        var selectedVal = $('.toassess-input:checked').val();

        // Selected option contains a value ?
        if (selectedVal.length > 0) {
            /* If a blank record, e.g. value of option starts with "blank::" */
            if (selectedVal.indexOf("blank::") >= 0) {
                $('#abs').val(selectedVal);
                if ($('#abs').val().length > 0) {
                    $('#abs_student').submit();
                }
                
            /* If a student record */    
            } else {
                var studNameArr = $(document.querySelector('.toassess-input:checked').labels[0]).text().split('-');
                var studentName = $.trim(studNameArr[0]);
                var confirmPart;
    
                // Exam Number mode, don't reveal student ID
                if ($('#student-id-mode').val() == "E") {
                  confirmPart = "ID -> " + studentName
                              + "\nName -> " + studentName;
                } else if ($('#student-id-mode').val() == "C") {
                  confirmPart = "\nName -> " + studentName;                       
                } else {
                  confirmPart = "ID -> " + selectedVal
                              + "\nName -> " + studentName;
                }
                
                var confirmMessage = confirmPart
                    + "\n\nAre you sure you want to mark this person as absent for this station?";

                if (confirm(confirmMessage)) {
                    $('#abs').val(selectedVal);
                    if ($('#abs').val().length > 0) {
                        $('#abs_student').submit();
                    }
                }
            }
            
        }
    });

    /* Discard button event, remove results from incomplete list */
    $('#discard').click(function() {
        var studentCount = $(".incomplete-input:checked").length;

        // One and only one option must be selected
        if (studentCount !== 1) {
            alert("Please select ONE student from the list");
            return;
        }
        
        if (isOfflineAlert()) {
           return false; 
        }        

        // Get option value
        var selectedVal = $('.incomplete-input:checked').val();

        // Selected option contains a value ?
        if (selectedVal.length > 0) {
            var studNameArr = $(document.querySelector('.incomplete-input:checked').labels[0]).text().split('-');
            var studentName = $.trim(studNameArr[0]);
            var confirmPart;
            
            // Exam Number mode, don't reveal student ID
            if ($('#student-id-mode').val() == "E") {
              confirmPart = "ID -> " + studentName
                          + "\nName -> " + studentName;      
            } else if ($('#student-id-mode').val() == "C") {
              confirmPart = "\nName -> " + studentName;               
            } else {
              confirmPart = "ID -> " + selectedVal
                          + "\nName -> " + studentName;
            }
            
            var confirmMessage = confirmPart
                + "\n\nAre you sure you would like to discard the selected incomplete result?";

            if (confirm(confirmMessage)) {
                $('#discard-result').val(selectedVal);
                if ($('#discard-result').val().length > 0) {
                    $('#discard-form').submit();
                }
            }
        }
    });


    /* Reset Form Button */
    $('#resetser').click(function() {
        parent.location = "assess.php?p=student_selection";
    });
    
    
   // jQuery ui look and feel for the search field
   $('#st').button()
   .css({
      'font' : 'inherit',
      'color' : 'inherit',
      'text-align' : 'left',
      'outline' : 'none',
      'cursor' : 'text'
    });    
    
});
