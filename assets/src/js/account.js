/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

jQuery(document).ready(function() {

     jQuery('#change_pass').click(function() {

            jQuery('#password_td').empty().append(jQuery('<input>', {
                'type': 'password',
                'placeholder': 'enter old password..',
                'class': 'acip form-control',
                'id': 'password_ip',
                'name': 'password_ip'
            }));

            jQuery("#password_td").addClass('nbb');

            jQuery('#password_ip').focus();
            
            // first row 
            var tr1 = jQuery('<tr>');

            var th1 = jQuery('<th>', {'class': 'title'});
            th1.text("New Password");
            tr1.append(th1);

            var td1 = jQuery('<td>', {
                'id': 'td-new-password',
                'class': 'td-password-fields nbb'
            });

            var secondPasswordField = jQuery('<input>', {
                'type': 'password',
                'placeholder': 'enter new password..',
                'class': 'acip form-control',
                'id': 'new_pass',
                'name': 'new_pass'
            });
            tr1.append(td1);
            td1.empty().append(secondPasswordField);

            var td1_info = jQuery('<td>', {
                'id': 'new_pass_err',
                'class': 'infotd'
            });

            tr1.append(td1_info);

            jQuery('#password_row').after(tr1);

            // second row
            var tr2 = jQuery('<tr>');

            var th2 = jQuery('<th>', {'class': 'title'});
            th2.text("Verify Password");
            tr2.append(th2);

            var td2 = jQuery('<td>', {'class': 'td-password-fields'});
            tr2.append(td2);
            td2.empty().append(jQuery('<input>', {
                'type': 'password',
                'placeholder': 'confirm new password..',
                'class': 'acip form-control',
                'id': 'retype_pass',
                'name': 'retype_pass'
            }));

            var td2_info = jQuery('<td>', {
                'id': 'retype_pass_err',
                'class': 'infotd'
            });

            tr2.append(td2_info);
            tr1.after(tr2);

            jQuery('#change_acc').css('display', 'none');
            jQuery(this).css('display', 'none');

            var update_pass = jQuery('<input>', {
                'type': 'button',
                'class': 'buttonv11 btn btn-success btn-sm',
                'id': 'update_pass',
                'name': 'update_pass',
                'value': 'change'
            });

            jQuery('.buttontd').append(update_pass);

            var cancel_pass = jQuery('<input>', {
                'type': 'button',
                'class': 'buttonv11 btn btn-danger btn-sm',
                'id': 'cancel_pass',
                'name': 'cancel_pass',
                'value': 'cancel'
            });

            update_pass.before(cancel_pass);

            //third row
            var tr3 = jQuery('<tr>');

            var th3 = jQuery('<th>', {'class': 'title'});
            th3.text("Email Password");
            tr3.append(th3);

            var td3 = jQuery('<td>');
            tr3.append(td3);
            var emailpass = jQuery('<input>', {
                'type': 'checkbox',
                'id': 'email_pass',
                'name': 'email_pass',
                'value': '1'
            });

            td3.empty().append(emailpass);

            var label = jQuery('<label>', {
                'for': 'email_pass',
                'class': 'red',
                'id': 'email_pass_label'
            });

            emailpass.after(label);
            label.text('Yes');

            var td3_info = jQuery('<td>', {
                'id': 'email_pass_err',
                'class': 'infotd'
            });

            tr3.append(td3_info);
            tr2.after(tr3);

            // Prepare password validation(jquery-password-fields.js)
            secondPasswordField.after(jQuery("<div>", {
                'class': 'pwstrength-viewport-progress'
            }));

            preparePasswordValidation();
            applyStrengthMeter(secondPasswordField);

            // cancel event
            cancel_pass.click(function() {

                jQuery('.infotd').html('');

                jQuery('#change_acc').css('display', 'inline');
                jQuery('#change_pass').css('display', 'inline');

                tr1.remove();
                tr2.remove();
                tr3.remove();

                jQuery('#password_td').empty().text("*********************");

                update_pass.remove();
                jQuery(this).remove();
                jQuery("#password_td").removeClass('nbb');

            });

            update_pass.click(function() {

                jQuery('.infotd').html('');

                if (jQuery('#password_ip').val().length === 0) {

                    jQuery('#password_err').html("&#xab; fill in");
                    return;

                }

                if (jQuery('#new_pass').val().length === 0) {

                    jQuery('#new_pass_err').html("&#xab; fill in");
                    return;

                }

                if (jQuery('#retype_pass').val().length === 0) {

                    jQuery('#retype_pass_err').html("&#xab; fill in");
                    return;

                }

                if (!sufficientPasswordStrength(jQuery('#new_pass'))) {

                    return;

                }

                if (jQuery('#retype_pass').val() !== jQuery('#new_pass').val()) {

                    alert("New password and confirmed password do not match");
                    return;

                }

                if (jQuery('#new_pass').val().indexOf(" ") > -1) {

                    alert("Sorry, spaces are not allowed.");
                    return;

                }

                jQuery('#account_submit').val("password_change");

                if (jQuery('#account_submit').val() === "password_change") {
                    
                   jQuery('#acc_form').off('submit').submit(function (event) {

                    event.preventDefault();
                    jQuery("#update_pass").attr('disabled', true).val("updating..");

                    jQuery.ajax({
                        url: jQuery(this).attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: jQuery(this).serialize()
                    })
                    .done(function (result) {
                        
                        if (result.success) {
                            alert("Password changed successfully.");
                            if ("message" in result) {
                               alert(result.message);
                            }
                           refreshPage(false);
                        } else {
                            if ("message" in result) {
                              alert(result.message);
                            } else {
                              var message = "Failed to change password. Please contact an administrator " +
                                            "or consult the activity logs.";
                              alert(message);
                            }
                        }
                     })
                    .fail(function (jqXHR) {
                        var error = "";
                        switch (jqXHR.status) {
                            case 404:
                                error = "Request target doesn't exist!";
                                break;
                            case 301:
                            case 302:
                                error = "Redirect";
                                break;
                            case 401:
                                error = "Sorry, you don't have permission to perform this action.";
                                break;
                            default:
                                error = "Could not send request, please try again";
                        }
                        alert(error);
                    });    
                  }).submit();



                }

            });

    });

    jQuery('#change_acc').click(function() {

            var forename = jQuery('#forename_td').text();
            jQuery('#forename_td').empty().append(jQuery('<input>', {
                'type': 'text',
                'class': 'acip form-control',
                'id': 'forename_ip',
                'name': 'forename_ip',
                'value': forename
            }));

            var surname = jQuery('#surname_td').text();
            jQuery('#surname_td').empty().append(jQuery('<input>', {
                'type': 'text',
                'class': 'acip form-control',
                'id': 'surname_ip',
                'name': 'surname_ip',
                'value': surname
            }));

            var email = jQuery('#email_td').text();
            jQuery('#email_td').empty().append(jQuery('<input>', {
                'type': 'text',
                'class': 'acip form-control',
                'id': 'email_ip',
                'name': 'email_ip',
                'value': email
            }));

            var password = jQuery('#password_td').text();
            jQuery('#password_td').empty().append(jQuery('<input>', {
                'type': 'password',
                'class': 'acip form-control',
                'id': 'password_ip',
                'name': 'password_ip',
                'placeholder': 'enter password..'
            }));

            jQuery(this).css('display', 'none');
            jQuery('#change_pass').css('display', 'none');

            var update = jQuery('<input>', {
                'type': 'button',
                'class': 'buttonv11 btn btn-success btn-sm',
                'id': 'update',
                'name': 'update',
                'value': 'update'
            });

            var cancel = jQuery('<input>', {
                'type': 'button',
                'class': 'buttonv11 btn btn-danger btn-sm',
                'id': 'cancel',
                'name': 'cancel',
                'value': 'cancel'
            });

            jQuery('.buttontd').append(cancel);

            cancel.after(update);

            cancel.click(function() {

                jQuery('.infotd').html("");

                jQuery('#change_acc').css('display', 'inline');
                jQuery('#change_pass').css('display', 'inline');
                jQuery('#update').remove();

                jQuery('#forename_ip').remove();
                jQuery('#surname_ip').remove();
                jQuery('#email_ip').remove();
                jQuery('#password_ip').remove();

                jQuery('#forename_td').text(forename);
                jQuery('#surname_td').text(surname);
                jQuery('#email_td').text(email);
                jQuery('#password_td').text(password);

                jQuery(this).remove();

            });

            update.click(function() {

                jQuery('.infotd').html("");

                if (jQuery('#forename_ip').val().length === 0) {

                    jQuery('#forename_err').html("&#xab; fill in");
                    return;

                }

                if (jQuery('#surname_ip').val().length === 0) {

                    jQuery('#surname_err').html("&#xab; fill in");
                    return;

                }

                if (jQuery('#email_ip').val().length === 0) {

                    jQuery('#email_err').html("&#xab; fill in");
                    return;

                }

                if (!isEmailValid(jQuery('#email_ip').val())) {

                    jQuery('#email_err').html("&#xab; Invalid Email");
                    return;

                }

                if (jQuery('#password_ip').val().length === 0) {

                    jQuery('#password_err').html("&#xab; password");
                    return;

                }

                jQuery('#account_submit').val("change");

                if (jQuery('#account_submit').val() === "change") {
                                    
                  jQuery('#acc_form').off('submit').submit(function (event) {
                    event.preventDefault();
                    jQuery.ajax({
                        url: jQuery(this).attr('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: jQuery(this).serialize()
                    })
                    .done(function (result) {

                        if (!result.success) {

                            alert("The Password does not match the Account Identifier");

                        } else {

                            refreshPage(false);

                        }
                    })
                    .fail(function (jqXHR) {
                        var error = "";
                        switch (jqXHR.status) {
                            case 404:
                                error = "Request target doesn't exist!";
                                break;
                            case 301:
                            case 302:
                                error = "Redirect";
                                break;
                            case 401:
                                error = "Sorry, you don't have permission to perform this action.";
                                break;
                            default:
                                error = "Could not send request, please try again";
                        }
                        alert(error);
                    });    
                  }).submit();
                
                }
            });

    });

    showMenu();

});
