var LastActiveUsers = function() {

    var that = this;

    this.offset = 0;
    this.length = 4;

    this.getLastActiveUsers = function() {
        jQuery.get('admins/ajx_admin_data.php', {
            q: 'last_active_users',
            offset: this.offset,
            length: this.length
        }, function (response) {
            var activeUsers = JSON.parse(response);
            var numberOfEntries = Object.keys(activeUsers).length;
            that.offset += numberOfEntries;

            var table = document.getElementById("last-active-users-table");
            {
                Object.keys(activeUsers).forEach(function (user) {
                    var row = document.createElement("tr");

                    var userTd = document.createElement("td");
                    var ipTd = document.createElement("td");
                    var dateTd = document.createElement("td");

                    userTd.appendChild(document.createTextNode(user));

                    // Ip link
                    var ipAnchor = document.createElement('a');
                    ipAnchor.setAttribute("target", "_blank");
                    ipAnchor.setAttribute("href", "http://whatismyipaddress.com/ip/" + activeUsers[user].ip);
                    ipAnchor.appendChild(document.createTextNode(
                        activeUsers[user].ip
                    ));
                    ipTd.appendChild(ipAnchor);

                    dateTd.appendChild(document.createTextNode(
                        moment.unix(activeUsers[user].date)
                            .format("DD/MM/YYYY HH:mm:ss")
                    ));

                    row.appendChild(userTd);
                    row.appendChild(ipTd);
                    row.appendChild(dateTd);

                    table.appendChild(row);
                });
            }

            if (numberOfEntries < that.length)
                jQuery('#btn-load-more').css('display', 'none');
        });
    };

    this.refreshLastActiveUsers = function() {
        this.offset = 0;
        jQuery('#btn-load-more').css('display', 'inline');
        document.getElementById('last-active-users-table').innerHTML = '';
        this.getLastActiveUsers();
    };

};

function adminDataMain() {
    var lastActiveUsers = new LastActiveUsers();

    jQuery('#btn-load-more').click(lastActiveUsers.getLastActiveUsers.bind(lastActiveUsers));
    jQuery('#btn-refresh-last-active').click(lastActiveUsers.refreshLastActiveUsers.bind(lastActiveUsers));

    lastActiveUsers.getLastActiveUsers();
}

jQuery(adminDataMain);