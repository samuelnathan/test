/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 30/05/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */
var called = 0;
jQuery(document).ready(function() {
    if (jQuery('#pdform').length) {
        if (jQuery('#pdframe-bulk').length) {
            jQuery('#pdframe-bulk').on('load', function(){
                removeWait();
            });
        }
        if (jQuery('#respdf-info').length) {
            document.title = jQuery('#respdf-info').val();
        }

        /* Back to all results page */
        if (jQuery('#back-all').length && jQuery('#student').length && jQuery('#exam').length) {

            jQuery('#back-all').on('click', function(e) {
                parent.location = "manage.php?page=studentexam"
                  + "&student=" + jQuery('#student').val() 
                  + "&exam=" + jQuery('#exam').val()
                  + (jQuery('#anon') ? "&anon=" : "");
            });
            
        }

        /* Back to single results page */
        if (jQuery('#back-single').length && jQuery('#resultsid').length) {
            jQuery('#back-single').on('click', function(e) {
                parent.location = "manage.php?page=studentresult&results_id=" + jQuery('#resultsid').val();
            });
        }
    }
});

/* Remove Wait Function */
function removeWait() {
    if (jQuery('#pdfwait').length && (called === 0)) {
        jQuery('#pdfwait').remove();
        called = 1;
    }
}
