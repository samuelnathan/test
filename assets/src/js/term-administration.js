/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

var urlTerm = "terms/ajx_terms.php";
jQuery(document).ready(function() {

    if (jQuery('#term_list_form').length) {
        if (jQuery('#add').length) {
            newTermEvent();
        }

        if (jQuery('#count').length && jQuery('#count').val() > 0) {
            if (jQuery('#check-all').length) {
                addCheckboxEvents('term');
            }
            editEvents();
        }
    }
    showMenu();
    performScroll();
});

// Edit events
function editEvents() {
    if (jQuery('#term_table').length && jQuery('#count').length) {
        var buttons = jQuery('#term_table').find('a[class=editba]');

        jQuery.each(buttons, function(i, button) {
            editEvent(button);
        });
    }
}

// Edit term event
function editEvent(b) {
    jQuery(b).off('click', editEventHandler).on('click', editEventHandler);
}

function editEventHandler (event) {
    event.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var termID = row.find('input[id^=term-check]').val();

    var random = Math.random();
    jQuery('#term_table').find("input, select, a:not([class~=orderlink]), img").css('display', 'none');
    setWaitStatusRow(jQuery(row), 5);
    jQuery.ajax({
        url: urlTerm,
        type: "post",
        data: {
            sid: random,
            mode: 'edit',
            id: termID
        }
     })
     .done(function(html){
        if (!html.indexOf('TERM_NON_EXIST') > -1) {
            row.empty().html(html);
            row.attr('class', "editrw");
            row.attr('id', "edit_row");

            editButtonsRow(jQuery(row), 5);
            addDatePicker();
            updateTermCancelEvent(jQuery('#edit-cancel'), fsr, row);
            updateTermEvent(jQuery('#edit-update'));
        }
        else {
            var message = "This record does not exist in the system anymore, "
                          "click 'OK' button to proceed with refreshing list";
            alert(message);
            refreshPage(false);
        }
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#term_table').find("input, select, a, img").css('display', '');
        editEvent(fsr.find('a[class=editba]'));
        addCheckboxEvent(fsr.find('input[id^=term-check]'), 'term');
        alert('The request failed, Please try again');
     });
}
// Update term event
function updateTermEvent(button) {
    jQuery(button).on('click', function (event) {
        event.preventDefault();

        // Are all fields complete
        if (areFieldsCompleteV2(jQuery('#edit_row'))) {
            var random = Math.random();

            // Validate date fields
            if (!validateTermDateFields()) {
                return false;
            }

            // Validate record ID field
            if (!isRecordIDValid(jQuery('#edit-id').val(), false)) {
                return false;
            }

            // Change dates format to '2015-11-19'
            var startDate = changeDateStringSeparator(jQuery('#start-date').val(), '/', '-');
            var endDate = changeDateStringSeparator(jQuery('#end-date').val(), '/', '-');

            // Add waiting status row to row we are updating
            jQuery('#edit-update').css('display', 'none');
            jQuery('#edit-cancel').css('display', 'none');
            jQuery('#b_edit_td').attr('class', 'waittd2');

            // Submit updated term record
            jQuery.ajax({
                url: urlTerm,
                type: "post",
                data: {
                    sid: random,
                    mode: 'update',
                    original_id: jQuery('#org-id').val(),
                    id: jQuery('#edit-id').val(),
                    start_date: startDate,
                    end_date: endDate,
                    description: jQuery('#description').val(),
                    default_term: jQuery('#default-term').val()
                }
             })
             .done(function(html){
                if (html.indexOf('TERM_NON_EXIST') > -1) {
                    failEdit();
                    var failMessage = "The record you are trying to update does not " +
                            "exist in the system, refreshing page"
                    alert(failMessage);
                    refreshPage(false);
                } else if (html.indexOf('TERM_ALREADY_USED') > -1) {
                    failEdit();
                    alert("The ID you specified is already in use, please try a different ID");
                } else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failEdit();
                alert('The request failed, Please try again');
             });
        } else {
            alert("Please complete all fields");
        }
    });
}

// Add cancel update event
function updateTermCancelEvent(button, org, oldrow) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#term_table').find("input, a, img, select").css('display', '');
        var org_edit = org.find('a[class=editba]');
        var org_cb = org.find('input[id^=term-check]');
        org_cb.css('display', '');

        editEvent(org_edit);
        addCheckboxEvent(org_cb, 'term');
    });
}

// New term event
function newTermEvent() {
    if (jQuery('#add').length) {
        jQuery('#add').on('click', function(e) {
            e.preventDefault();
            jQuery('#term_table').find("input, select, img, a:not([class~=orderlink])").css('display', 'none');

            var lastrow;
            if (jQuery('#first_term_row').length) {
                var fsr = jQuery('#first_term_row').remove();
                lastrow = jQuery('#title-row');
            }
            else {
                if (jQuery('#count').length) {
                    lastrow = jQuery('#term_row_' + jQuery('#count').val());
                }
            }
            var new_addrow = jQuery('<tr>', {'id': 'add-row'});
            lastrow.after(new_addrow);

            setWaitStatusRow(jQuery(new_addrow), 5);

            var random = Math.random();
            jQuery.ajax({
                url: urlTerm,
                type: "post",
                data: {
                    sid: random,
                    mode: "add"
                }
             })
             .done(function(html){
                new_addrow.empty().attr('class', 'addrow').html(html);
                addButtonsRow();
                submitTermEvent();
                submitTermCancelEvent(new_addrow, fsr);
                addDatePicker();
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
             })
             .fail(function(jqXHR) {
                new_addrow.remove();
                jQuery('#term_table').find("input, select, a, img").css('display', '');
                alert('The request failed, Please try again');
             });
        });
    }
}

// Add submit event
function submitTermEvent() {
    jQuery('#add-save').on('click', function(event) {
        event.preventDefault();

        // Validate fields to submit (check if data entered)
        if (areFieldsCompleteV2(jQuery('#add-row'))) {

            // Validate date fields
            if (!validateTermDateFields()) {
               return false;
            }

            // Validate record ID field
            if (!isRecordIDValid(jQuery('#id').val(), false)) {
                return false;
            }

            // Change dates format to '2015-11-19'
            var startDate = changeDateStringSeparator(jQuery('#start-date').val(), '/', '-');
            var endDate = changeDateStringSeparator(jQuery('#end-date').val(), '/', '-');

            // Add wait button bar
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            // Submit request
            var random = Math.random();
            jQuery.ajax({
                url: urlTerm,
                type: "post",
                data: {
                    sid: random,
                    mode: "submit",
                    id: jQuery('#id').val(),
                    description: jQuery('#description').val(),
                    start_date: startDate,
                    end_date: endDate,
                    default_term: jQuery('#default-term').val(),
                    clone_courses: jQuery('#clone-courses').val()
                }
             })
             .done(function(html){
                if (html.indexOf('TERM_EXISTS') > -1) {
                    failAdd();
                    var message = "ID specified already exists in the system, "
                                  "please choose a different ID";
                    alert(message);
                  } else if (html.indexOf('INVALID_ORDER_DATES') > -1) {
                    failAdd();
                    alert ("Start date must come before end date");

                  } else if (html.indexOf('INVALID_DATES') > -1) {
                    failAdd();
                    alert ("One or both of the date fields are invalid");
                  } else {
                    prepareScroll('scrolltobase');
                    parent.location = 'manage.php?page=manage_terms';
                  }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });

        } else {
            alert("Please complete all fields");
        }
    });
}

// aAdd cancel event
function submitTermCancelEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();
        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#term_table').find("select, img, input, a").css('display', '');
        removeNewRecordControls();
    });
}

 /**
  * Add date picker for both
  * start and end date fields
  *
  */
 function addDatePicker() {

    jQuery('#start-date, #end-date').datepicker({
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        showOn: 'both',
        buttonImage: 'assets/images/calendar.svg',
        buttonImageOnly: true,
        buttonText: 'Click to set/change date'
    });

 }


/**
 * Validate date term fields
 *
 * @return bool true|false returns valid yes|no
 */
function validateTermDateFields() {

    // Validate date fields (start and end)
    if (!(jQuery('#start-date').length && jQuery('#end-date').length)) {
        alert ("Date fields not found");
        return false;
    }

    var startDate = changeDateStringSeparator(jQuery('#start-date').val(), '/', '-');
    var endDate = changeDateStringSeparator(jQuery('#end-date').val(), '/', '-');
    var startDateTime = new Date(startDate).getTime();
    var endDateTime = new Date(endDate).getTime();

    if (isNaN(startDateTime) && isNaN(endDateTime)) {
       alert ("One or both of the date fields are invalid");
       return false;
    } else if (startDateTime >= endDateTime) {
       alert ("Start date must come before end date");
       return false;
    } else {
       return true;
    }

}

// Validate form
function ValidateForm(form) {
    if (jQuery(form).attr('name') === 'term_list_form') {
        var ischecked = false;
        var thecbs = jQuery('#term_list_form').find('input[id^=term-check]');

        jQuery.each(thecbs, function(i, cb) {
            if (jQuery(cb).prop('checked')) {
                ischecked = true;
            }
        });

        if (ischecked) {
            var warnConfirm = "** WARNING PLEASE READ **\n\n"
                            + "Are you sure you want to delete the selected Term(s)? \n\nAll Stations, "
                            + "Courses, Students, Examiners, OSCE's and OSCE Results\ntied to each term "
                            + "will also be deleted";
            var yesno = confirm(warnConfirm);

            if (yesno === true) {
                return true;
            }
        }
    }
    return false;
}
