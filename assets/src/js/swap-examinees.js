/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 07/06/2016
 * © 2016 Qpe5com Limited.  All rights reserved.
 * Swap students Main > Admin Tools > OSCE Student
 */

jQuery(document).ready(function() {
    var url = 'tools/json_tools.php';

    // Password field change
    jQuery('#swap-password').keyup(function() {
        selectTriggerAction(null);
    });

    /* Swap action EVENT */
    jQuery('#swap-action').change(function() {

       // Swap students in exam
       if (jQuery(this).val() == 1) {
           jQuery('.swap-from-exam').show();
           jQuery('.swap-from-course').hide();
           jQuery('#swap-station-li').hide();
           jQuery('#swap-out-legend').text('Swap this Student');
           jQuery('#swap-in-legend').text('With this Student');

           // Clone session options
           cloneSessionsOptions();

       // Swap student in from course/module
       } else if (jQuery(this).val() == 2) {
           jQuery('.swap-from-exam').hide();
           jQuery('.swap-from-course').show();
           jQuery('#swap-station-li').hide();
           jQuery('#swap-out-legend').text('Swap OUT this Student');
           jQuery('#swap-in-legend').text('Swap IN this Student');

           // Select first item
           var swapinCourse = jQuery('#swapin-course');
               swapinCourse[0].selectedIndex = 0;

           // Empty child fields (underneath)
           selectTriggerAction(jQuery('#swapin-course'));

       // Swap results in a station
       } else if (jQuery(this).val() == 3) {
           jQuery('#swapin-session-li').hide(1);
           jQuery('#swapin-group-li').hide(1);
           jQuery('.swap-from-course').hide(1);
           jQuery('#swap-station-li').show();

           jQuery('#swap-out-legend').text('Swap Station Result for this Student');
           jQuery('#swap-in-legend').text('With this Student');

           // Clone students dropdown
           cloneStudentsOptions();
       }

    });


    /* Exam EVENT */
    if (jQuery('#swapout-exam').length) {
        jQuery('#swapout-exam').on('change', function () {
            var examID = jQuery(this).val();
            selectTriggerAction(jQuery(this));
            selectTriggerAction(jQuery('#swapin-session'));
            emptyDropdown(jQuery('#swapin-session'), '', '--');

            if (examID.length > 0) {
                emptyDropdown(jQuery('#swapout-session')
                        .prop('disabled', false)
                        .addClass('select-load'), '', 'Loading....');

                        jQuery.ajax({
                            url: url,
                            type: "post",
                            dataType: "json",
                            cache: false,
                            data: {
                                isfor: 'swap_sessions',
                                exam: examID
                            }
                         })
                         .done(function(sessions){
                            emptyDropdown(jQuery('#swapout-session').removeClass('select-load'), '', '--');

                        jQuery.each(sessions, function (i, session) {
                            var optionElement = jQuery('<option>', {'value': session.session_id})
                                .html(session.session_date + ' ' + session.session_description);
                            jQuery('#swapout-session').append(optionElement);
                        });

                        // Clone session options from the first dropdown
                        var clonedOptions = jQuery('select#swapout-session').find('option').clone();
                            jQuery('#swapin-session').empty().append(clonedOptions);
                         })
            }
        });
    }

    /* SESSION EVENT */
    if (jQuery('#swapout-session').length && jQuery('#swapin-session').length) {
        jQuery('#swapout-session, #swapin-session').on('change', function () {
            var sessionID = jQuery(this).val();
            var splitArr = jQuery(this).attr('id').split('-');
            var type = splitArr[0];
            selectTriggerAction(jQuery(this));

            if (sessionID.length > 0) {

                emptyDropdown(jQuery('#' + type + '-group')
                        .prop('disabled', false)
                        .addClass('select-load'), '', 'Loading....');


                if (type === 'swapout') {
                  emptyDropdown(jQuery('#swapout-station')
                          .prop('disabled', false)
                          .addClass('select-load'), '', 'Loading....');
                }

                jQuery.ajax({
                    url: url,
                    type: "post",
                    dataType: "json",
                    cache: false,
                    data: {
                        isfor: 'swap_groups_stations',
                        session: sessionID
                    }
                 })
                 .done(function(data){
                    emptyDropdown(jQuery('#' + type + '-group')
                             .removeClass('select-load'), '', '--');

                        if (type === 'swapout') {
                          emptyDropdown(jQuery('#swapout-station')
                               .removeClass('select-load'), '', '--');
                          jQuery.each(data.stations, function (i, station) {
                            jQuery('#swapout-station')
                                .append(jQuery('<option>', {'value': station.id})
                                .html(station.number + " - " + station.name));
                          });
                        }

                         jQuery.each(data.groups, function (i, group) {
                            $(type + '-group')
                                .append(jQuery('<option>', {'value': group.id})
                                .html(group.name));
                         });
                 })

            }
        });
    }

    /* GROUP STUDENTS */
    if (jQuery('#swapout-group').length && jQuery('#swapin-group').length) {
        jQuery('#swapout-group, #swapin-group').on('change', function () {
            var groupID = jQuery(this).val();
            var splitArr = jQuery(this).attr('id').split('-');
            var type = splitArr[0];

            if (groupID.length > 0) {

                emptyDropdown(jQuery('#' + type + '-student')
                        .prop('disabled', false)
                        .addClass('select-load'), '', 'Loading....');

                        jQuery.ajax({
                            url: url,
                            type: "post",
                            dataType: "json",
                            cache: false,
                            data: {
                                isfor: 'swap_students',
                                group: groupID
                            }
                         })
                         .done(function(students){
                            emptyDropdown(jQuery('#' + type + '-student')
                            .removeClass('select-load'), '', '--');
                            jQuery.each(students, function (i, student) {
                                var optionEle = jQuery('<option>', {'value': student.student_id})
                                        .html(student.student_id + ' - ' + student.surname + ', ' + student.forename);
                                $(type + '-student').append(optionEle);
                            });

                            // Clone students dropdown
                            cloneStudentsOptions();
                         })
            }
        });
    }

    /* COURSE EVENT */
    if (jQuery('#swapin-course').length) {
        jQuery('#swapin-course').on('change', function () {
            var courseID = jQuery(this).val();
            selectTriggerAction(jQuery(this));

            if (courseID.length > 0) {
                emptyDropdown(jQuery('#swapin-year').prop('disabled', false).addClass('select-load'), '', 'Loading....');
                jQuery.ajax({
                    url: url,
                    type: "post",
                    dataType: "json",
                    cache: false,
                    data: {
                        isfor: 'swap_years', course_id: courseID
                    }
                 })
                 .done(function(courseYears){
                    emptyDropdown(jQuery('#swapin-year').removeClass('select-load'), '', '--');
                        jQuery.each(courseYears, function (i, courseYear) {
                            var optionEle = jQuery('<option>', {'value': courseYear.year_id}).html(courseYear.year_name);
                            jQuery('#swapin-year').append(optionEle);
                        });
                 })
            }
        });
    }

    /* COURSE YEAR EVENT */
    if (jQuery('#swapin-year').length) {
        jQuery('#swapin-year').on('change', function () {
            var courseYearID = jQuery(this).val();
            selectTriggerAction(jQuery(this));

            if (courseYearID.length > 0) {
                emptyDropdown(jQuery('#swapin-course-student').prop('disabled', false).addClass('select-load'), '', 'Loading....');

                jQuery.ajax({
                    url: url,
                    type: "post",
                    dataType: "json",
                    cache: false,
                    data: {
                        isfor: 'swap_course_students',
                        course_year_id: courseYearID
                    }
                 })
                 .done(function(students){
                    emptyDropdown(jQuery('#swapin-course-student').removeClass('select-load'), '', '--');
                        jQuery.each(students, function (i, student) {
                            var studentName = '';

                            if (student.surname.length > 0 && student.forename.length > 0) {
                                studentName = ' - ';
                            }

                            if (student.surname.length > 0) {
                                studentName += student.surname;
                            }

                            if (student.forename.length > 0) {
                                studentName += ', ' + student.forename;
                            }

                            var optionEle = jQuery('<option>', {'value': student.student_id})
                                    .html(student.student_id + studentName);
                            jQuery('#swapin-course-student').append(optionEle);
                        });
                 })
            }
        });
    }

    // Miscellaneous field events
    var fields = [
       "swapout-student", "swapin-student", "swapin-course-student", "swapout-station"
    ];
    fields = "#" + fields.join(", #");
    jQuery(fields).change(function() {
            selectTriggerAction(null);
    });

    showMenu();
});


// Clone station options
function cloneSessionsOptions() {
    // Clone session options from the first dropdown
    var clonedOptions = jQuery('select#swapout-session').find('option').clone();
    jQuery('#swapin-session').empty().append(clonedOptions);

    // Select first item
    var swapinSession = jQuery('#swapin-session');
        swapinSession[0].selectedIndex = 0;

    // Empty child fields (underneath)
    selectTriggerAction(jQuery('#swapin-session'));
    jQuery('.swap-from-exam').show();
    jQuery('.swap-from-course').hide();
}

// Clone students options
function cloneStudentsOptions() {

   // Swap station results option choosen
   if (jQuery("#swap-action").val() == 3) {

    // Show swapin list item
    jQuery('.swapin-student').show();

    // Clone session options from the first dropdown
    var clonedOptions = jQuery('select#swapout-student').find('option').clone();
    jQuery('select#swapin-student').empty().append(clonedOptions).attr('disabled', false);

   }
}

// Enable or Disable the swap button
function selectTriggerAction(currentElement) {
    if (currentElement !== null) {
        var parentLi = currentElement.parent('li');
        var liList = parentLi.nextAll("li[class^='" + parentLi.attr('class') + "']");

        jQuery.each(liList, function (index, li) {
            var selectElement = jQuery(li).find('select:first');
            emptyDropdown(selectElement, '', '--').prop('disabled', true);
        });
    }

    // Swap validaton
    var swapoutGroupSelected = (jQuery('#swapout-group').val().length > 0);
    var swapoutStudentSelected = (jQuery('#swapout-student').val().length > 0);
    var swapoutStationSelected = (jQuery('#swapout-station').val().length > 0);

    var swapinStudentSelected = (jQuery('#swapin-student').val().length > 0);
    var swapinCourseStudentSelected = (jQuery('#swapin-course-student').val().length > 0);
    var passwordFieldComplete = (jQuery('#swap-password').val().length > 0);

    var swapStationResultsRequired = (jQuery('#swap-action').val() == 3);

    var swapOutReady = (swapoutGroupSelected && swapoutStudentSelected);
    var swapInReady = (swapinCourseStudentSelected || swapinStudentSelected);
    var stationsReady = (!swapStationResultsRequired || swapoutStationSelected);

    // Ready to swap
    if (passwordFieldComplete && swapOutReady && swapInReady && stationsReady) {
        jQuery('#swap-button').prop('disabled', false)
                              .val('swap students');
    } else {
        jQuery('#swap-button').prop('disabled', true)
                              .val('swap students');
    }

}
