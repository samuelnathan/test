/**
 * Class that handles the business logic of the rotations for a given session
 * @param rotationsData Rotations data of a session
 * @constructor
 */
var SessionRotations = function (rotationsData) {

    var that = this;

    this.rotationsData = rotationsData;

    /**
     * Cache that saves the already found students in station
     * @type {{}}
     */
    var studentStationCache = {};

    /**
     * Tries to get a student in station from the cache. If it's not found, returns null
     * @param studentId
     * @param stationId
     * @param blankStudent {boolean?}
     * @param additionalInfo {Object?} Pointer to the object where the additional info will be stored
     * @returns {Object|null}
     */
    var getStationFromCache = function(studentId, stationId, blankStudent, additionalInfo) {
        blankStudent = blankStudent || false;

        // Get the data from cache
        var cached = studentStationCache[studentId + "@" + stationId + (blankStudent ? "[blank]" : "")];
        // If it's not there, return false
        if (!cached) return null;

        // If there's no additional info to get, return the data
        if (!additionalInfo)
            return cached.data;

        // Set the additional info
        additionalInfo.rotationNumber = cached.additionalInfo.rotationNumber;
        additionalInfo.group = cached.additionalInfo.group;
        additionalInfo.rotation = cached.additionalInfo.rotation;
        additionalInfo.previousRotation = cached.additionalInfo.previousRotation;

        // Return the data
        return cached.data;
    };

    /**
     * Puts a student/station in the cache
     * @param studentId
     * @param stationId
     * @param blankStudent
     * @param data
     * @param additionalInfo
     */
    var putStationInCache = function(studentId, stationId, blankStudent, data, additionalInfo) {
        studentStationCache[studentId + "@" + stationId + (blankStudent ? "[blank]" : "")] = {
            data: data,
            additionalInfo: additionalInfo
        };
    };

    /**
     * Find the object that contains the data of a specific student in a specific station
     * @param studentId String
     * @param stationId String
     * @param blankStudent Boolean? Optional value. true if the student is a blank student, false otherwise. The default value is false
     * @param additionalInfo {Object}? Object where additional info about the station is stored
     * @returns {Object|null}
     */
    this.getStation = function(studentId, stationId, blankStudent, additionalInfo) {
        blankStudent = blankStudent || false;

        // Try to get it from the cache. If it's found, return it. Otherwise, look for it in the table and
        // add it to the cache
        var cached = getStationFromCache(studentId, stationId, blankStudent, additionalInfo);
        if (cached !== null)
            return cached;

        var studentIdKey = blankStudent ? "blank_student_id" : "student_id";

        // Iterate through the groups
        for (var i = 0; i < rotationsData.groups.length; i++) {
            var group = rotationsData.groups[i];
            // Iterate through the rotations in the group
            for (var j = 0; j < group.rotations.length; j++) {
                var rotation = group.rotations[j];
                // Iterate through the stations in the rotation
                for (var k = 0; k < rotation.length; k++) {
                    var stations = rotation[k].station;
                    for (var l = 0; l < stations.length; l++) {
                        var station = stations[l];
                        // If it's not the one we're looking for, continue looping
                        if (!(station.station_id === stationId && rotation[k].student[studentIdKey] === studentId))
                            continue;

                        var previousRotation = j !== 0 ? group.rotations[j - 1] : null;

                        // If it is, add it to the cache and return it
                        putStationInCache(studentId, stationId, blankStudent, rotation[k], {
                            rotationNumber: (i * group.rotations.length) + j,
                            group: group,
                            rotation: rotation,
                            previousRotation: previousRotation
                        });

                        return getStationFromCache(studentId, stationId, blankStudent, additionalInfo);
                    }
                }
            }
        }

        // The station was not found, return null
        return null;
    };

    /**
     * Update the ongoing station
     * @param updateData {Object?} Latest updated station
     */
    this.updateOngoing = function(updateData) {
        // When a station is updated. Update the ongoing stations using it as a candidate. Select the more advanced
        // rotation of the currently ongoing one, the last completed when and the newly updated one

        var asCandidate = function(studentId, stationId) {
            return {
                number: that.getRotationNumber(studentId, stationId),
                station: {
                    student_id: studentId,
                    station_id: stationId
                }
            };
        };

        var candidates = [];

        // If there's a complete station, add the latest one as a candidate
        var lastCompleted = this.getLatestCompletedStation();
        if (lastCompleted)
            candidates.push(asCandidate(lastCompleted.student.student_id, lastCompleted.station[0].station_id));

        // If there's a currently ongoing station, add it as a candidate
        if (this.rotationsData.ongoing)
            candidates.push(asCandidate(
                this.rotationsData.ongoing.student_id,
                this.rotationsData.ongoing.station_id));

        // Add the newly updated station as a candidate
        if (updateData)
            candidates.push(asCandidate(updateData.student_id, updateData.station_id));

        if (candidates.length === 0)
            return;

        // Set as ongoing station the candidate with the greatest rotation number
        this.rotationsData.ongoing = candidates.reduce(function(ongoing, candidate) {
            return candidate.number > ongoing.number ? candidate : ongoing;
        }).station;
    };

    /**
     * Gets the rotation number in which a given student will be at a given station
     * @param studentId
     * @param stationId
     * @param blankStudent
     * @returns {number|*|null}
     */
    this.getRotationNumber = function(studentId, stationId, blankStudent) {
        var additionalInfo = {};
        this.getStation(studentId, stationId, blankStudent, additionalInfo);

        return additionalInfo.rotationNumber || null;
    };

    /**
     * Calculate the progress of the scoring of a student in a station as a percentage number
     * @param progress
     * @returns {number}
     */
    this.getProgressPercentage = function (progress) {
        // Sums the value of a given field in the progress array
        var accumulateField = function(fieldKey) {
            return progress.reduce(function(total, examinerProgress) {
                return total + parseInt(examinerProgress[fieldKey]);
            }, 0);
        };

        var total = accumulateField("total");           // Sum of all the items in the station
        var completed = accumulateField("completed");   // Sum of all the completed items in the station

        return (completed * 100) / total;
    };

    /**
     * Given a rotation, check if it is the ongoing one
     * @callback ongoingCallback
     * @param rotation {Object}
     * @returns {boolean}
     */
    this.isOngoingRotation = function(rotation) {
        // If there's no data of ongoing station return false
        if (!rotationsData.ongoing)
            return false;

        // Check if the ongoing station is in the rotation
        return rotation.some(function(studentInStation) {
            return studentInStation.student.student_id === rotationsData.ongoing.student_id &&
                studentInStation.station.some(function(station) {
                    return station.station_id === rotationsData.ongoing.station_id;
                });
        });
    };

    this.isCompleteStation = function(station) {
        return station.complete.length > 0 && station.complete.every(this.isCompleteExaminer);
    };

    this.isCompleteExaminer = function(completion) {
        return completion.is_complete === "1";
    };

    /**
     * Check if a given station is a rest station
     * @param studentStation
     * @returns {boolean}
     */
    this.isRestStation = function(studentStation) {
        return studentStation.every(function(station) { return station.rest_station === '1'; });
    };

    /**
     * Get the list of expected examiners for a given station
     * @param station_number
     * @returns Array
     */
    this.getExaminersForStation = function(station_number) {
        return rotationsData.stations.find(function(station) {
            return station.station_number === station_number;
        }).examiners;
    };

    this.selectGroupId =function(group) {
        return group.group.group_id;
    };

    /**
     * Given a student in a station. Check if the scoring should be complete by now
     * @param studentInStation
     * @returns {boolean}
     */
    this.shouldBeDone = function(studentInStation) {
        if (!studentInStation.student.hasOwnProperty("student_id") || !rotationsData.ongoing)
            return false;

        var rotationNumber = this.getRotationNumber(studentInStation.student.student_id, studentInStation.station[0].station_id);
        var ongoingRotationNumber = this.getRotationNumber(rotationsData.ongoing.student_id, rotationsData.ongoing.station_id);

        return rotationNumber < ongoingRotationNumber;
    };

    /**
     * Get the latest station that has been completed
     * @returns {*}
     */
    this.getLatestCompletedStation = function() {
        for (var i = rotationsData.groups.length - 1; i >= 0; i--) {
            var group = rotationsData.groups[i];

            for (var j = group.rotations.length - 1; j >= 0; j--) {
                var rotation = group.rotations[j];

                for (var k = rotation.length - 1; k >= 0; k--) {
                    var station = rotation[k];

                    if (this.isCompleteStation(station))
                        return station;
                }
            }
        }

        return null;
    };

};

/**
 * Cache that saves the already loaded SessionRotations instances
 */
var SessionRotationsPool = new (function() {

    /**
     * Storage of instances in the form of <session id> => <SessionRotations instance>
     * @type {{}}
     */
    var pool = {};

    /**
     * Gets a SessionRotations instance from the pool or creates a new one and adds it to the pool
     * @param rotationsData
     * @param sessionId
     * @returns {SessionRotations}
     */
    this.sessionRotations = function(rotationsData, sessionId) {
        if (!pool[sessionId])
            pool[sessionId] = new SessionRotations(rotationsData);

        return pool[sessionId];
    };

    /**
     * Gets a SessionRotations instance from the pool
     * @param sessionId
     * @returns {SessionRotations|null}
     */
    this.getBySessionId = function(sessionId) {
        return pool[sessionId] || null;
    };

})();