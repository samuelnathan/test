/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

jQuery(document).ready(function() {
    if (jQuery('#select').length) {
        jQuery('#select').on('change', function () {
            parent.location = "manage.php?page=manage_departments&select=" + jQuery('#select').val();
        });
    }

    if (jQuery('#dept_table').length) {
        addCheckboxEvents("dept");
        departmentsEditEvents();
        departmentAddEvent();
    }

    showMenu();
    performScroll();
});

function departmentsEditEvents() {
    if (jQuery('#dept_table').length && jQuery('#count').length) {
        var buttons = jQuery('#dept_table').find('a[class=editba]');

        jQuery.each(buttons, function (i, b) {
            departmentEditEvent(b);
        });
    }
}

function departmentEditEvent(b) {
    jQuery(b).off('click', departmentEditEventHandler).on('click', departmentEditEventHandler);
}
function departmentEditEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var dept_id = row.find('input[id^=dept-check]').val();

    var url = "schools/ajx_departments.php";
    var random = Math.random();

    jQuery('#dept_table').find("input, select, img").css('display', 'none');
    setWaitStatusRow(jQuery(row), 2);

    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: "edit",
            dept: dept_id
        }
     })
     .done(function(html){
        row.empty().html(html);
        row.attr('class', "editrw");
        row.attr('id', "edit_row");

        editButtonsRow(jQuery(row), 2);
        cancelUpdateEvent(jQuery('#edit-cancel'), fsr, row);
        departmentUpdateEvent(jQuery('#edit-update'));
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#dept_table').find("input, select, img").css('display', '');
        departmentEditEvent(fsr.find('a[class=editba]'));
        addCheckboxEvent(fsr.find('input[id^=dept-check]'), "dept");
        
        if (jqXHR.status === 403 || jqXHR.status === 404) {
            jsonResponse = JSON.parse(xhr.responseText);
            alert(jsonResponse.message);
        } else {
            alert('The request failed, Please try again');
        }
        
        refreshPage(false);
     });

}

// update 
function departmentUpdateEvent(b) {
    jQuery(b).on('click', function (e) {
        e.preventDefault();

        if (jQuery('#edit_dept_id').val().length === 0 || jQuery('#edit_deptname').val().length === 0) {
            alert("Please complete all fields.");
            return false;
        }
        
        // Validate record ID field
        if (!isRecordIDValid(jQuery('#edit_dept_id').val(), false)) {
            return false;
        }        
        
        var url = "schools/ajx_departments.php";
        var random = Math.random();

        jQuery('#edit-update').css('display', 'none');
        jQuery('#edit-cancel').css('display', 'none');
        jQuery('#b_edit_td').attr('class', 'waittd2');
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random, isfor: "update",
                org_dept_id: jQuery('#org_dept_id').val(),
                dept_id: jQuery('#edit_dept_id').val(),
                dept_name: jQuery('#edit_deptname').val()
            }
         })
         .done(function(html){
            refreshPage(false);
         })
         .fail(function(jqXHR) {
            jQuery('#b_edit_td').attr('class', 'editbts');
            jQuery('#edit-update').css('display', '');
            jQuery('#edit-cancel').css('display', '');
            jQuery('#b_edit_td').removeClass('waittd2');

            switch (jqXHR.status) {
                case 403: // Forbidden, a.k.a. Invalid Argument
                case 404: // Not found.
                    jsonResponse = JSON.parse(jqXHR.responseText) || 'The request failed, Please try again';
                    alert(jsonResponse.message);
                    break;
                default:
                    alert('The request failed, Please try again');
            }
         });
    });
}

//add cancel update event
function cancelUpdateEvent(button, org, oldrow) {
    jQuery(button).on('click', function (e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#dept_table').find("input, img, select").css('display', '');
        var org_edit = org.find('a[class=editba]');
        var org_cb = org.find('input[id^=dept-check]');
        org_cb.css('display', '');

        departmentEditEvent(org_edit);
        addCheckboxEvent(org_cb, "dept");
    });
}

// add button event 
function departmentAddEvent() {
    if (jQuery('#add').length) {
        var lastrow;
        
        jQuery('#add').on('click', function (e) {
            e.preventDefault();
            jQuery('#dept_table').find("input, select, img").css('display', 'none');

            if (jQuery('#first_dept_row').length) {
                var fsr = jQuery("#first_dept_row").remove();
                lastrow = jQuery("#title-row");
            } else {
                if (jQuery('#count').length) {
                    lastrow = jQuery('#dept_row_' + jQuery('#count').val());
                }
            }

            var new_addrow = jQuery('<tr>', {'id': 'add-row'});

            lastrow.after(new_addrow);
            setWaitStatusRow(jQuery(new_addrow), 2);

            var url = "schools/ajx_departments.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "add",
                    school_id: jQuery('#select').val()
                }
             })
             .done(function(html){
                new_addrow.empty().attr('class', 'addrow').html(html);
                addButtonsRow();
                departmentSubmitEvent();
                cancelSubmitEvent(new_addrow, fsr);
                jQuery('#add_id').focus();
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
             })
             .fail(function(jqXHR) {
                if (jqXHR.status === 403) {
                    alert("The parent record was not found in the system, please go back to main list");
                    refreshPage(false);
                }
                
                new_addrow.remove();
                jQuery('#dept_table').find("input, select, img").css('display', '');
                alert('The request failed, Please try again');
             });
        });
    }
}

// add cancel event
function cancelSubmitEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function (e) {
        e.preventDefault();

        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#dept_table').find("select, img, input").css('display', '');
        removeNewRecordControls();
    });
}

// submit
function departmentSubmitEvent() {
    jQuery('#add-save').on('click', function (e) {
        e.preventDefault();
        if (areFieldsComplete(jQuery('#add-row'))) {

            // Validate record ID field
            if (!isRecordIDValid(jQuery('#add_id').val(), false)) {
                return false;
            }          
            
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            var url = "schools/ajx_departments.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random, 
                    isfor: "submit",
                    school_id: jQuery('#select').val(),
                    add_id: jQuery('#add_id').val(),
                    add_deptname: jQuery('#add_deptname').val()
                }
             })
             .done(function(html){
                prepareScroll('scrolltobase');
                refreshPage(false);
             })
             .fail(function(jqXHR) {
                failAdd();
                    
                switch (jqXHR.status) {
                    case 403: // Forbidden, a.k.a. Invalid Argument
                    case 404: // Not found.
                    case 500: // Update failed on server.
                        if (getResponseHeaders(jqXHR)['Content-Type'] === 'application/json') {
                            alert((JSON.parse(jqXHR.responseText)).message);
                        } else {
                            alert('The request failed, Please try again');
                        }
                        break;
                    default:
                        alert('The request failed, Please try again');
                }
             });
        } else {
            alert("Please complete all Fields");
        }
    });
}

function ValidateForm(form) {
    if (jQuery(form).attr('name') !== 'dept_list_form') {
        return false;
    }

    var anyChecked = false;
    var thecbs = jQuery('#dept_list_form').find('input[id^=dept-check]');

    /* Iterate through each checkbox with id "dept-check-<index>". If *any* are
     * checked, set isChecked to true.
     */
    jQuery.each(thecbs, function (i, cb) {
        anyChecked |= cb.prop('checked');
    });

    if (anyChecked) {
        return confirm("** WARNING PLEASE READ **\n\n" +
            "Are you sure you want to delete the selected records?\n" +
            "All underlying records linked will be DELETED also!\n\n" +
            "Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n");
    }

    return false;
}
