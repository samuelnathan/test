 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */

 jQuery(document).ready(function() {

     // Return to Manage Courses Button
     jQuery('#returncy').click(function () {

         parent.location = 'manage.php?page=manage_courses' +
         (jQuery('#chosen-term').val().length > 0 ? "&t=" + jQuery('#chosen-term').val() : '');

     });

     // Return to Manage Years Button
     jQuery('#returncym').click(function () {

         parent.location = 'manage.php?page=manage_years&crs=' + jQuery('#crs').val() +
         (jQuery('#chosen-term').val().length > 0 ? "&t=" + jQuery('#chosen-term').val() : '');

     });

     // Manage Courses Event
     if (jQuery('#crs_list_form').length) {

         addCheckboxEvents("crs");
         editCourseEvents();
         addCourseEvent();

     }

     // Manage Years Event
     if (jQuery('#crsyr_list_form').length) {

         addCheckboxEvents("crsyr");
         editYearEvents();
         addYearEvent();

     }

     // Manage Modules Event
     if (jQuery('#cmod_list_form').length) {

         addCheckboxEvents("cmod");
         editModuleEvents();
         addModuleEvent();

     }

     // Switch term/year
     jQuery('#select-term').change(function() {

         parent.location = "manage.php?page=manage_courses"
             + "&t=" + jQuery(this).val()

     });

     showMenu();
     performScroll();

 });


 //Edit Course Events
 function editCourseEvents() {

     if (jQuery('#crs_table').length && jQuery('#count').length) {
         var buttons = jQuery('#crs_table').find('a.editba');
         jQuery.each(buttons, function (i, b) {
             editCourseEvent(b);
         });
     }
 }

 //Edit Course Events
 function editCourseEvent(b) {
     jQuery(b).off('click', editCourseEventHandler).on('click', editCourseEventHandler);
 }
 function editCourseEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var tid = row.find('input[id^="crs-check"]').val();

    var url = "schools/ajx_courses.php";
    var random = Math.random();

    jQuery('#crs_table').find("input, select, img").css('display', 'none');
    setWaitStatusRow(row, 3); 

    jQuery.ajax({
       url: url,
       type: "post",
       data: {
           sid: random,
           isfor: "edit",
           crs: tid
       }
    })
    .done(function(html){
       if (!html.indexOf('CRS_NON_EXIST') > -1) {
           row.empty().html(html);
           row.attr('class', "editrw");
           row.attr('id', "edit_row");

           editButtonsRow(row, 3);
           updateCourseCancelEvent(jQuery('#edit-cancel'), fsr, row);
           updateCourseEvent(jQuery('#edit-update'));
       }
       else {
           alert("This record does not exist in the system, click 'OK' button to proceed with refreshing list");
           refreshPage(false);
       }
    })
    .fail(function(jqXHR) {
       row.replaceWith(fsr);
       jQuery('#crs_table').find("input, select, img").css('display', '');
       editCourseEvent(fsr.find('a.editba'));
       addCheckboxEvent(fsr.find('input[id^=crs-check]'), "crs");
       alert('The request failed, Please try again');
    });

}

 //Edit year events
 function editYearEvents() {

     if (jQuery('#crsyr_table').length && jQuery('#count').length) {
         var buttons = jQuery('#crsyr_table').find('a.editba');
         jQuery.each(buttons, function (i,b) {
             editYearEvent(b);
         });
     }
 }

 //Edit year event
 function editYearEvent(b) {
     jQuery(b).off('click', editYearEventHandler).on('click', editYearEventHandler);
 }
 function editYearEventHandler(e) {
    e.preventDefault();

    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var tid = row.find('input[id^="crsyr-check"]').val();

    var url = "schools/ajx_years.php";
    var random = Math.random();

    jQuery('#crsyr_table').find("input, select, img").css('display', 'none');
    setWaitStatusRow(jQuery(row), 2);
    jQuery.ajax({
       url: url,
       type: "post",
       data: {
           sid: random,
           isfor: "edit",
           crsyr: tid
       }
    })
    .done(function(html){
       if (!html.indexOf('CRSYR_NON_EXIST') > -1) {
           row.empty().html(html);
           row.attr('class', "editrw");
           row.attr('id', "edit_row");

           editButtonsRow(row, 2);
           updateYearCancelEvent(jQuery('#edit-cancel'), fsr, row);
           updateYearEvent(jQuery('#edit-update'));
       }
       else {
           alert("This record does not exist in the system, click the 'OK' button to proceed with refreshing list");
           refreshPage(false);
       }
    })
    .fail(function(jqXHR) {
       row.replaceWith(fsr);
       jQuery('#crsyr_table').find("input, select, img").css('display', '');
       editYearEvent(fsr.find('a.editba'));
       addCheckboxEvent(fsr.find('input[id^=crsyr-check]'), "crsyr");
       alert('The request failed, Please try again');
    });

}
 //Edit module events
 function editModuleEvents() {

     if (jQuery('#cmod_table').length && jQuery('#count').length) {
         var buttons = jQuery('#cmod_table').find('a.editba');
         jQuery.each(buttons, function (i,b) {
             editModuleEvent(b);
         });
     }
 }

 //Edit module event
 function editModuleEvent(b) {
     jQuery(b).off('click', editModuleEventHandler).on('click', editModuleEventHandler);
 }
 function editModuleEventHandler (e) {
    e.preventDefault();

    var row = jQuery(this).parents('tr:first');
    var fsr = row.clone(true, true);
    var tid = row.find('input[id^=cmod-check]').val();

    var url = "schools/ajx_modules.php";
    var random = Math.random();

    jQuery('#cmod_table').find("input, select, img").css('display', 'none');
    setWaitStatusRow(jQuery(row), 2);

    jQuery.ajax({
       url: url,
       type: "post",
       data: {
           sid: random,
           isfor: "edit",
           editid: tid
       }
    })
    .done(function(html){
       if (!html.indexOf('MOD_NON_EXIST') > -1) {
           row.empty().html(html);
           row.attr('class', "editrw");
           row.attr('id', "edit_row");

           editButtonsRow(jQuery(row), 2);
           updateModuleCancelEvent(jQuery('#edit-cancel'), fsr, row);
           updateModuleEvent(jQuery('#edit-update'));
       }
       else {
           alert("This record does not exist in the system, click the 'OK' button to proceed with refreshing list");
           refreshPage(false);
       }
    })
    .fail(function(jqXHR) {
       row.replaceWith(fsr);
       jQuery('#cmod_table').find("input, select, img").css('display', '');
       editModuleEvent(fsr.find('a.editba'));
       addCheckboxEvent(fsr.find('input[id^=cmod-check]'), "cmod");
       alert('The request failed, Please try again');
    });

}

 //Update module cancel event
 function updateModuleCancelEvent(button, org, oldrow) {
     jQuery(button).on('click', function (e) {
         e.preventDefault();
         jQuery('#b_edit').remove();
         oldrow.replaceWith(org);
         jQuery('#cmod_table').find("input, img, select").css('display', '');
         var org_edit = org.find('a.editba');
         var org_cb = org.find('input[id^=cmod-check]');
         org_cb.css('display', '');

         editModuleEvent(org_edit);
         addCheckboxEvent(org_cb, "cmod");
     });
 }

 //Update Module Event
 function updateModuleEvent(b) {
     jQuery(b).on('click', function (e) {
         e.preventDefault();

         if (areFieldsComplete(jQuery('#edit_row'))) {

             // Validate record ID field
             if (!isRecordIDValid(jQuery('#editid').val(), false)) {
                 return false;
             }

             var url = "schools/ajx_modules.php";
             var random = Math.random();

             jQuery('#edit-update').css('display', 'none');
             jQuery('#edit-cancel').css('display', 'none');
             jQuery('#b_edit_td').attr('class', "waittd2");
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "update",
                    editid: jQuery('#editid').val(),
                    orgid: jQuery('#orgid').val(),
                    editname: jQuery('#editname').val()
                }
             })
             .done(function(html){
                if (html.indexOf('MOD_NON_EXIST') > -1) {
                    failEdit();
                    alert("Record ID does not exist, refreshing page");
                    refreshPage(false);
                }
                else if (html.indexOf('MOD_EXISTS_ALREADY') > -1) {
                    failEdit();
                    alert("ID specified already exists in the system");
                }
                else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failEdit();
                jQuery('#b_edit_td').removeClass('waittd2');
                alert('The request failed, Please try again');
             });
         }
         else {
             alert("Please complete all fields.");
         }

     });
 }

 //Update Year Event
 function updateYearEvent(b) {

     jQuery(b).on('click', function (e) {
         e.preventDefault();

         if (jQuery('#year_name').val().length > 0) {

             // Validate record ID field
             if (!isRecordIDValid(jQuery('#year_name').val(), false)) {

                 return false;

             }

             var url = "schools/ajx_years.php";
             var random = Math.random();

             jQuery('#edit-update').css('display', 'none');
             jQuery('#edit-cancel').css('display', 'none');
             jQuery('#b_edit_td').attr('class', "waittd2");

             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "update",
                    crsyrid: jQuery('#crsyrid').val(),
                    year_name: jQuery('#year_name').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRSYR_NON_EXIST') > -1) {
                    jQuery('#b_edit_td').attr('class', "editbts");
                    jQuery('#edit-update').css('display', '');
                    jQuery('#edit-cancel').css('display', '');
                    alert("ID does not exist, refreshing page");
                }
                refreshPage(false);
             })
             .fail(function(jqXHR) {
                jQuery('#b_edit_td').attr('class', "editbts");
                jQuery('#edit-update').css('display', '');
                jQuery('#edit-cancel').css('display', '');
                jQuery('#b_edit_td').removeClass('waittd2');

                alert('The request failed, Please try again');
             });

         }
         else {

             alert("Please complete all fields.");

         }

     });

 }

 //Cancel Event for Update Year
 function updateYearCancelEvent(button, org, oldrow) {
     jQuery(button).on('click', function (e) {
         e.preventDefault();
         jQuery('#b_edit').remove();
         oldrow.replaceWith(org);
         jQuery('#crsyr_table').find("input, img, select").css('display', '');
         var org_edit = org.find('a.editba');
         var org_cb = org.find('input[id^=crsyr-check]');
         org_cb.css('display', '');

         editYearEvent(org_edit);
         addCheckboxEvent(org_cb, "crsyr");
     });
 }


 //Update Course Event
 function updateCourseEvent(b) {
     jQuery(b).on('click', function (e) {
         e.preventDefault();

         if (jQuery('#edit_id').val().length > 0 && jQuery('#edit_name').val().length > 0) {

             // Validate record ID field
             if (!isRecordIDValid(jQuery('#edit_id').val(), false)) {
                 return false;
             }

             var url = "schools/ajx_courses.php";
             var random = Math.random();

             jQuery('#edit-update').css('display', 'none');
             jQuery('#edit-cancel').css('display', 'none');
             jQuery('#b_edit_td').attr('class', "waittd2");

             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random, isfor: "update",
                    org_id: jQuery('#org_id').val(),
                    edit_id: jQuery('#edit_id').val(),
                    edit_name: jQuery('#edit_name').val()
                }
             })
             .done(function(html){
                if (html != "true" && html != 1) {
                    jQuery('#b_edit_td').attr('class', "editbts");
                    jQuery('#edit-update').css('display', '');
                    jQuery('#edit-cancel').css('display', '');
                    alert("ID already in use, please specify another");
                }
                else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                jQuery('#b_edit_td').attr('class', "editbts");
                jQuery('#edit-update').css('display', '');
                jQuery('#edit-cancel').css('display', '');
                jQuery('#b_edit_td').removeClass('waittd2');

                alert('The request failed, Please try again');
             });
         }
         else {
             alert("Please complete all fields.");
         }

     });
 }

 //Update Course Cancel Event
 function updateCourseCancelEvent(button, org, oldrow) {
     jQuery(button).on('click', function (e) {
         e.preventDefault();
         jQuery('#b_edit').remove();
         oldrow.replaceWith(org);
         jQuery('#crs_table').find("input, img, select").css('display', '');
         var org_edit = org.find('a.editba');
         var org_cb = org.find('input[id^="crs-check"]');
         org_cb.css('display', '');

         editCourseEvent(org_edit);
         addCheckboxEvent(org_cb, "crs");
     });
 }


 //Add Course Event
 function addCourseEvent() {
     if (jQuery('#add').length) {
         jQuery('#add').on('click', function (e) {
             e.preventDefault();
             jQuery('#crs_table').find("input, select, img").css('display', 'none');

             if (jQuery('#first_crs_row').length) {
                 var fsr = jQuery("#first_crs_row").remove();
                 var lastRow = jQuery("#title-row");
             }
             else {
                 if (jQuery('#count').length) {
                     var lastRow = jQuery('#crs_row_' + jQuery('#count').val());
                 }
             }

             var addRow = jQuery('<tr>', {'id': 'add-row'});
             lastRow.after(addRow);

             setWaitStatusRow(jQuery(addRow), 3);

             var url = "schools/ajx_courses.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "add"
                }
             })
             .done(function(html){
                addRow.empty().attr('class', "addrow").html(html);
                     addButtonsRow();
                     submitCourseEvent();
                     submitCourseCancelEvent(addRow, fsr);
                     jQuery('#add_id').focus();
                     jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
             })
             .fail(function(jqXHR) {
                addRow.remove();
                jQuery('#crs_table').find("input, select, img").css('display', '');
                alert('The request failed, Please try again');
             });
         });
     }
 }

 //Add Year Event
 function addYearEvent() {
     if (jQuery('#add').length) {
         jQuery('#add').on('click', function (e) {
             e.preventDefault();
             jQuery('#crsyr_table').find("input, select, img").css('display', 'none');

             if (jQuery('#first_crsyr_row').length) {
                 var fsr = jQuery("#first_crsyr_row").remove();
                 var lastRow = jQuery("#title-row");
             }
             else {
                 if (jQuery('#count').length) {
                     var lastRow = jQuery('#crsyr_row_' + jQuery('#count').val());
                 }
             }

             var addRow = jQuery('<tr>', {'id': 'add-row'});
             lastRow.after(addRow);

             setWaitStatusRow(jQuery(addRow), 2);

             var url = "schools/ajx_years.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "add",
                    course_id: jQuery('#crs').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRS_NON_EXIST') > -1) {
                    alert("This record does not exist");
                    refreshPage(false);
                }

                addRow.empty().attr('class', "addrow").html(html);
                addButtonsRow();
                submitYearEvent();
                submitYearCancelEvent(addRow, fsr);
                jQuery('#add_yr').focus();
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
             })
             .fail(function(jqXHR) {
                addRow.remove();
                jQuery('#crsyr_table').find("input, select, img").css('display', '');
                alert('The request failed, Please try again');
             });
         });
     }
 }


 //Add Module Event
 function addModuleEvent() {
     if (jQuery('#add').length) {
         jQuery('#add').on('click', function (e) {
             e.preventDefault();
             jQuery('#cmod_table').find("input, select, img").css('display', 'none');

             if (jQuery('#first_cmod_row').length) {
                 var fsr = jQuery("#first_cmod_row").remove();
                 var lastRow = jQuery("#title-row");
             }
             else {
                 if (jQuery('#count').length) {
                     var lastRow = jQuery('#cmod_row_' + jQuery('#count').val());
                 }
             }

             var addRow = jQuery('<tr>', {'id': 'add-row'});
             lastRow.after(addRow);

             setWaitStatusRow(jQuery(addRow), 2);

             var url = "schools/ajx_modules.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "add",
                    crsyr: jQuery('#crsyr').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRSYR_NON_EXIST') > -1) {
                    alert("This record does not exist");
                    refreshPage(false);
                }

                addRow.empty().attr('class', "addrow").html(html);
                addButtonsRow();
                submitModuleEvent();
                submitModuleCancelEvent(addRow, fsr);
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");

                if (jQuery('#existing-module').length) {
                    jQuery('#existing-module').on('change', function (e) {

                        //If new module
                        if (jQuery(this).val() == "--") {

                            //Disable Module ID field
                            jQuery('#add-id').prop('disabled', false);

                            //Show module name text box
                            jQuery('#add-name').css('display', '');

                            //Empty existing module name container
                            jQuery('#existing-module-name').empty();

                            //If using existing module
                        } else {

                            //Enable Module ID field
                            jQuery('#add-id').prop('disabled', true);

                            //Hide module name text box
                            jQuery('#add-name').css('display', 'none');

                            //Display module name
                            jQuery('#existing-module-name').text(jQuery(this).find('option:selected').attr('title'));

                        }

                    });
                }

                jQuery('#add-id').focus();
             })
             .fail(function(jqXHR) {
                addRow.remove();
                jQuery('#cmod_table').find("input, select, img").css('display', '');
                alert('The request failed, Please try again');
             });
         });
     }
 }

 //Submit Module Event
 function submitModuleEvent() {
     jQuery('#add-save').on('click', function (e) {
         e.preventDefault();
         if (jQuery('#existing-module').val() !== "--" || areFieldsComplete(jQuery('#add-row'))) {

             // Validate record ID field
             if (jQuery('#existing-module').val() === "--" && !isRecordIDValid(jQuery('#add-id').val(), false)) {
                 return false;
             }

             jQuery(this).css('display', 'none');
             jQuery('#add-cancel').css('display', 'none');
             jQuery('#base-button-td').addClass('waittd3');

             var url = "schools/ajx_modules.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "submit",
                    add_id: jQuery('#add-id').val(),
                    add_name: jQuery('#add-name').val(),
                    existing_module: jQuery('#existing-module').val(),
                    crsyr: jQuery('#crsyr').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRSYR_NON_EXIST') > -1) {

                    failAdd();
                    alert("This record does not exist");
                    refreshPage(false);

                }
                else if (html.indexOf('MOD_EXISTS') > -1) {

                    failAdd();
                    alert("ID already in use, please specify another");

                } else {

                    prepareScroll('scrolltobase');
                    refreshPage(false);

                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });

         } else {
             alert("Please complete all Fields");
         }
     });
 }

 //Submit Module Cancel Event
 function submitModuleCancelEvent(row, fsr) {
     jQuery('#add-cancel').on('click', function (e) {
         e.preventDefault();
         if (!jQuery('#count').length) {
             row.replaceWith(fsr);
         } else {
             row.remove();
         }
         jQuery('#cmod_table').find("select, img, input").css('display', '');
         removeNewRecordControls();
     });
 }


 //Submit Year Cancel Event
 function submitYearCancelEvent(row, fsr) {
     jQuery('#add-cancel').on('click', function (e) {
         e.preventDefault();

         if (!jQuery('#count').length) {
             row.replaceWith(fsr);
         } else {
             row.remove();
         }
         jQuery('#crsyr_table').find("select, img, input").css('display', '');
         removeNewRecordControls();
     });
 }

 //Submit Course Cancel Event
 function submitCourseCancelEvent(row, fsr) {
     jQuery('#add-cancel').on('click', function (e) {
         e.preventDefault();

         if (!jQuery('#count').length) {
             row.replaceWith(fsr);
         } else {
             row.remove();
         }
         jQuery('#crs_table').find("select, img, input").css('display', '');
         removeNewRecordControls();
     });
 }


 // Submit Year Event
 function submitYearEvent() {

     jQuery('#add-save').on('click', function (e) {
         e.preventDefault();

         if (areFieldsComplete(jQuery('#add-row'))) {

             // Validate record ID field
             if (!isRecordIDValid(jQuery('#add_yr').val(), false)) {
                 return false;
             }

             jQuery(this).css('display', 'none');
             jQuery('#add-cancel').css('display', 'none');
             jQuery('#base-button-td').addClass('waittd3');

             var url = "schools/ajx_years.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "submit",
                    add_yr: jQuery('#add_yr').val(),
                    course_id: jQuery('#crs').val(),
                    term: jQuery('#chosen-term').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRS_NON_EXIST') > -1) {
                    failAdd();
                    alert("Record does not exist");
                }
                prepareScroll('scrolltobase');
                refreshPage(false);
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });

         } else {

             alert("Please complete all fields");

         }

     });

 }

 //Submit Course Event
 function submitCourseEvent() {
     jQuery('#add-save').on('click', function (e) {
         e.preventDefault();
         if (areFieldsComplete(jQuery('#add-row'))) {

             // Validate record ID field
             if (!isRecordIDValid(jQuery('#add_id').val(), false)) {
                 return false;
             }

             jQuery(this).css('display', 'none');
             jQuery('#add-cancel').css('display', 'none');
             jQuery('#base-button-td').addClass('waittd3');

             var url = "schools/ajx_courses.php";
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "submit",
                    add_id: jQuery('#add_id').val(),
                    add_name: jQuery('#add_crsname').val()
                }
             })
             .done(function(html){
                if (html.indexOf('CRS_EXIST') > -1) {
                    failAdd();
                    alert("ID specified aleady in use, Please specify a different ID");
                } else {
                    prepareScroll('scrolltobase');
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });
         } else {
             alert("Please complete all Fields");
         }
     });
 }

 //Validate forms
 function ValidateForm(form) {

     //Validate Courses Form
     if (jQuery(form).attr('name') == 'crs_list_form') {
         var ischecked = false;
         var thecbs = jQuery('#crs_list_form').find('input[id^=crs-check]');

         jQuery.each(thecbs, function (i, cb) {
             if (jQuery(cb).prop('checked')) {
                 ischecked = true;
             }
         });

         if (ischecked) {
             var yesno = confirm("** WARNING PLEASE READ **\n\n Are you sure you would like to proceed with deleting the selected records?\n\n"
                     + " Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n");

             if (yesno == true) {
                 return true;
             }
         }
     }

     //Validate Years Form
     else if (jQuery(form).attr('name') == 'crsyr_list_form') {
         var ischecked = false;
         var thecbs = jQuery('#crsyr_list_form').find('input[id^=crsyr-check]');

         jQuery.each(thecbs, function (i,cb) {
             if (jQuery(cb).prop('checked')) {
                 ischecked = true;
             }
         });

         if (ischecked) {
             var yesno = confirm("** WARNING PLEASE READ **\n\n Are you sure you want to proceed with the deletion of the selected records?\n\n"
                     + " Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n");

             if (yesno == true) {
                 return true;
             }
         }
     }

     //Validate Modules Form
     else if (jQuery(form).attr('name') == 'cmod_list_form') {
         var ischecked = false;
         var thecbs = jQuery('#cmod_list_form').find('input[id^=cmod-check]');

         jQuery.each(thecbs, function (i, cb) {
            if (jQuery(cb).prop('checked')) {
                ischecked = true;
            }
        });

         if (ischecked) {
             var yesno = confirm("** WARNING PLEASE READ **\n\n Are you sure you want to proceed with removal of selected records?\n\n"
                     + " Click 'OK' button if Yes, Click 'Cancel' button if No.\n\n");

             if (yesno == true) {
                 return true;
             }
         }
     }
     return false;
 }
