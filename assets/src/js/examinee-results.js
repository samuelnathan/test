/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 27/06/2016
 * © 2016 Qpercom Limited.  All rights reserved.
 */

jQuery(document).ready(function() {
    // Back Button
    if (jQuery('#back-button2').length) {
        jQuery('#back-button2').on('click', function(e) {
            e.preventDefault();
            history.go(-1);
            return false;
        });
    }

    // Window Title
    if (jQuery('#examinee_sresult_table').length) {
        document.title = jQuery('#surname').val() + " | " + jQuery('#station').val();
    }

    // Delete Results Checkbox Events
    if (jQuery('.multi-results').length > 0) {
        jQuery('.multi-results').on('change', function(e) {
            var enabledDisabled = true;

            if (jQuery('.multi-results:checked').length > 0) {
                enabledDisabled = false;
            }

            jQuery('#todo').prop('disabled', enabledDisabled);
            jQuery('#submit_button').prop('disabled', enabledDisabled);
        });
    }

    /* Results Parent Page Reload 
       'status' value returned from analysis_redirects.php script 
    */
   
    var parentUriString = parseQueryString(window.opener.location.search);
    var currentUriString = document.URL;
    var data = parseQueryString(currentUriString);
    var deleteOrAbsent = false;
    
    if(data['success'] && data['success'] === 'yes') {
       deleteOrAbsent = true;
    }
    
    if (deleteOrAbsent && window.opener && window.opener.open && !window.opener.closed && parentUriString['page'] === 'out_analysis') {
        window.opener.submitUpdateForm(null);
    }
    
    // Print button action
    if (jQuery('#print-sres').length) {
        jQuery('#print-sres').on('click', function(e) {
            e.preventDefault();
            window.print();
        });
    }    
    
});

// --- Begin jQuery code ---
jQuery(document).ready(function() {
    var confirm_dialog, dialog_form;
    var password = jQuery('#password');

    confirm_dialog = jQuery('#password-form').dialog({
        autoOpen: false,
        height: 190,
        width: 350,
        modal: true,
        buttons: {
            'OK': submitPassword,
            Cancel: function() {
                confirm_dialog.dialog("close");
            }
        },
        close: function() {
            dialog_form[0].reset();
            password.removeClass('ui-state-error');
        }
    });

    dialog_form = confirm_dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        submitPassword();
    });

    /* Get the entered password from the dialog, load it into the form, and 
     * submit that.
     */
    function submitPassword() {
        jQuery('input#confirm-password').val(password.val());
        confirm_dialog.dialog("close");
        jQuery("form[name='results_form']").submit();
    }
    
    // Hang an event handler off the submit button so that we can intercept the
    // submit if we need to get a password.
    jQuery('#submit_button').on('click', function () {
        
        /* If we're looking to do anything other than delete a record, then go
         * ahead and do it:
         */
        var actionToTake = jQuery('#todo').val();
        
        // No action found, then abort form submission
        if(actionToTake.length === 0) {
          return false;  
         
        // Mark results as absent
        } else if (actionToTake === "absent") {
           confirm_dialog.dialog('option', 'title', 'Mark Results as ABSENT'); 
        } else if (actionToTake === "delete") {
           confirm_dialog.dialog('option', 'title', 'DELETE Results'); 
        }
        
        /* If the user is looking to delete the data, then stop the submit from
         * going through until we've thrown a dialog at them...
         */
        confirm_dialog.dialog('open');
    });
});

