/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

jQuery(document).ready(function() {
    if (jQuery('.return').length) {
        jQuery('.return').on('click', function() {
            var newHash = '';
            var newParams = {
                page: 'sessions_osce',
                exam_id: jQuery('#exam-id').val()
            }
            if (jQuery('#show_session').length && jQuery('#show_session').val().length > 0) {
                newParams['show_session'] = jQuery('#show_session').val()
            } else {
                newHash = '#' + 'X' + jQuery('#session-id').val();
            }
            window.location.href = window.location.pathname + '?' + jQuery.param(newParams) + newHash;
        });
    }

    if (jQuery('#check-all').length) {
        addCheckboxEvents('sessassist');
    }

    if (jQuery('#add-new-assistant').length && jQuery('#add-existing-assistant').length) {
        newAssistantEvent();
        existingAssistantEvent();
    }

    if (jQuery('#sessassist_list_form').length) {
        editEvents();
    }
    showMenu();
    performScroll();
});

// Add New Assistant Event 
function newAssistantEvent() {
    var lastRow;
        
    jQuery('#add-new-assistant').on('click', function(e) {
        e.preventDefault();
        jQuery('#sessassist_table').find("input, select, img, a").css('display', 'none');

        if (jQuery('#first_sessassist_row').length) {
            var fsr = jQuery('#first_sessassist_row').remove();
            lastRow = jQuery('#title-row');
        }
        else {
            if (jQuery('#count').length) {
                lastRow = jQuery('#sessassist_row_' + jQuery('#count').val());
            }
            // @TODO: (DW) lastRow may not always be defined. Is this okay?
        }

        var newAddRow = jQuery('<tr>', {'id': 'add-row'});
        lastRow.after(newAddRow);

        setWaitStatusRow(jQuery(newAddRow), 5);

        var url = "osce/ajx_osce_assistants.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "add_new_assistant",
                session_id: jQuery('#session-id').val()
            }
         })
         .done(function(html){
            if (html !== "SESSION_NON_EXIST") {
                newAddRow.empty().attr('class', 'addrow').html(html);
                addButtonsRow('add-new-assistant');
                newAssistantSubmitEvent();
                cancelEvent(newAddRow, fsr);
                jQuery('#add-id').focus();
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
            } else {
                alert("The Session you are trying to attach an Assistant to was not found in the System, please return to Session list");
                refreshPage(false);
            }
         })
         .fail(function(jqXHR) {
            newAddRow.remove();
            jQuery('#sessassist_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

//Add Submit Event
function newAssistantSubmitEvent() {
    jQuery('#add-save').on('click', function(e) {
        e.preventDefault();
        if (validateAssistant('add')) {
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            var url = "osce/ajx_osce_assistants.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "submit_new_assistant",
                    session_id: jQuery('#session-id').val(),
                    add_id: jQuery('#add-id').val(),
                    add_fn: jQuery('#add-fn').val(),
                    add_sn: jQuery('#add-sn').val(),
                    add_email: jQuery('#add-email').val(),
                    add_mobile: jQuery('#add-mobile').val()
                }
             })
             .done(function(html){
                if (html === "SESSION_NON_EXIST") {
                    alert('The Session you are trying to attach an Assistant to was not found in the System, please return to Session list');
                    refreshPage(false);
                }
                else if (html === "ASSISTANT_ID_EXISTS") {
                    failAdd();
                    alert('User ID exists in the System already, please specify a different User ID');
                }
                else {
                    prepareScroll('scrolltobase');
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });
        }
    });
}

// Add Existing Assistant Event 
function existingAssistantEvent() {
    var lastRow;
    
    jQuery('#add-existing-assistant').on('click', function(e) {
        e.preventDefault();
        jQuery('#sessassist_table').find("input, select, img, a").css('display', 'none');

        if (jQuery('#first_sessassist_row').length) {
            var fsr = jQuery("#first_sessassist_row").remove();
            lastRow = jQuery("#title-row");
        }
        else {
            if (jQuery('#count').length) {
                lastRow = jQuery('#sessassist_row_' + jQuery('#count').val());
            }
            // @TODO (DW) lastRow may not always be defined. Is this okay?
        }

        var newAddRow = jQuery('<tr>', {'id': 'add-row'});
        lastRow.after(newAddRow);

        setWaitStatusRow(jQuery(newAddRow), 5);

        var url = "osce/ajx_osce_assistants.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "add_existing_assistant",
                session_id: jQuery('#session-id').val()
            }
         })
         .done(function(html){
            if (html !== "SESSION_NON_EXIST") {
                newAddRow.empty().attr('class', 'addrow').html(html);
                addButtonsRow('add-existing-assistant');
                assistantChangeEvent();
                existingAssistantSubmitEvent();
                cancelEvent(newAddRow, fsr);
            } else {
                alert("The Session you are trying to attach an Assistant to was not found in the System, please return to Session list");
                refreshPage(false);
            }
         })
         .fail(function(jqXHR) {
            newAddRow.remove();
            jQuery('#sessassist_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

//Add Assistant Change Event
function assistantChangeEvent() {

    jQuery('#change-existing-assistant').on('change', function(e) {
        e.preventDefault();

        jQuery('#add-mobile-td').find('span, input').css('display', 'none');
        jQuery('#add-email-td').find('span, input').css('display', 'none');

        jQuery('#add-mobile-td').addClass('waittd');
        jQuery('#add-email-td').addClass('waittd');

        var selected = jQuery(this).val();
        var url = "osce/ajx_osce_assistants.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "assistant_selection",
                user_id: selected
            }
         })
         .done(function(html){
            if (html !== "ASSISTANT_NON_EXIST") {
                var sarr = html.split("[*&&*]");

                jQuery('#add-mobile-td').removeClass('waittd');
                jQuery('#add-email-td').removeClass('waittd');

                jQuery('#add-mobile-td').find('span, input').css('display', '');
                jQuery('#add-email-td').find('span, input').css('display', '');
                
                // email & mobile
                jQuery('#add-email-existing').val(sarr[0].trim());
                jQuery('#add-mobile-existing').val(sarr[1].trim());

            } else {
                jQuery('#add-mobile-td').removeClass('waittd');
                jQuery('#add-email-td').removeClass('waittd');
                alert("The Assistant you selected was not found in the System or has been recently deleted, please select another Assistant from the list");
            }
         })
         .fail(function(jqXHR) {
            alert('The request failed, Please try again');
         });
    });

}

//Existing Assistant Submit Event
function existingAssistantSubmitEvent() {
    jQuery('#add-save').on('click', function(e) {
        e.preventDefault();
        if (validateExistingAssistant()) {
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            var url = "osce/ajx_osce_assistants.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random, 
                    isfor: "submit_existing_assistant",
                    session_id: jQuery('#session-id').val(),
                    user_id: jQuery('#change-existing-assistant').val(),
                    add_email: jQuery('#add-email-existing').val(),
                    add_mobile: jQuery('#add-mobile-existing').val()
                }
             })
             .done(function(html){
                if (html === "SESSION_NON_EXIST") {
                    alert('The Session you are trying to attach an Assistant to was not found in the System, please return to Session list');
                    refreshPage(false);
                }
                else if (html === "ASSISTANT_NON_EXIST") {
                    failAdd();
                    alert('The Assistant you selected was not found in the System or has recently been deleted, please select a different Assistant from the list');
                }
                else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });
        }
    });
}

//Validate New Assistant Row
function validateAssistant(record) {
    if (jQuery('#'+record + '-id').val().length === 0) {
        alert("please specify an ID for the Assistant");
        return false;
    }
    
    // Validate record ID field
    if (!isRecordIDValid(jQuery('#'+record + '-id').val(), false)) {
        return false;
    }
    
    if (jQuery('#'+record + '-fn').val().length === 0) {
        alert("please specify a Forename for the Assistant");
        return false;
    }
    
    if (jQuery('#'+record + '-sn').val().length === 0) {
        alert("please specify a Surname for the Assistant");
        return false;
    }
    
    if (jQuery('#'+record + '-email').val().length > 0 && !isEmailValid(jQuery('#'+record + '-email').val())) {
        alert("Email Address specified is invalid, please review");
        return false;
    }
    
    if (jQuery('#'+record + '-mobile').val().length > 0 && isNaN(jQuery('#'+record + '-mobile').val())) {
        alert("The Mobile Number must be numeric");
        return false;
    }
    
    // If we get to here all of the tests check out, so...
    return true;
}

//Validate row for existing assistant
function validateExistingAssistant() {
    if (!jQuery('#add-existing-assistant').length || jQuery('#add-existing-assistant').val().length === 0 || jQuery('#add-existing-assistant').val() === "*--*") {
        alert("No existing Assistant selected");
        return false;
    }

    if (!jQuery('#add-email-existing').length || (jQuery('#add-email-existing').val().length > 0 && !isEmailValid(jQuery('#add-email-existing').val()))) {
        alert("Email Address '" + jQuery('#add-email-existing').val() + "' specified is not Valid");
        return false;
    }
    
    if (!jQuery('#add-mobile-existing').length || (jQuery('#add-mobile-existing').val().length > 0 && isNaN(jQuery('#add-mobile-existing').val()))) {
        alert("mobile Number specified is not Valid");
        return false;
    }
    
    return true;
}

// Add Cancel Event
function cancelEvent(row, fsr) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();

        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#sessassist_table').find("select, img, input, a").css('display', '');
        jQuery('#add-new-assistant').prop('disabled', false);
        removeNewRecordControls();
        
    });
}

//Edit Events
function editEvents() {
    if (jQuery('#count').length) {
        var buttons = jQuery('#sessassist_list_form').find('a[class^=edit_]');

        jQuery.each(buttons, function(i, b) {
            editEvent(b);
        });
    }
}

//Each edit event
function editEvent(b) {
    jQuery(b).off('click', editEventHandler).on('click', editEventHandler);
}
function editEventHandler(e) {
    e.preventDefault();
    var row = jQuery(this).parents('tr:first');
    var user_id = row.find('input[id^=sessassist-check]').val();
    var fsr = row.clone(true, true);

    jQuery('#sessassist_table').find("input").css('display', 'none');
    jQuery('#sessassist_table').find("select, img, a").css('display', 'none');
    setWaitStatusRow(jQuery(row), 5);

    var url = "osce/ajx_osce_assistants.php";
    var random = Math.random();
    jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: "edit_assistant",
            org_user_id: user_id,
            session_id: jQuery('#session-id').val()
        }
     })
     .done(function(html){
        switch (html) {
            case 'SESSION_NON_EXIST':
                alert('The Session was not found in the System or has been recently deleted, please return to Session list');
                refreshPage(false);
                break;
            case 'ASSISTANT_NON_EXIST':
                alert('The Assistant you were trying to update was not found in the System or has recently been deleted, the Session Assistants list will now be refreshed');
                refreshPage(false);
                break;
            default:
                row.empty().attr('class', "editrw").attr('id', "edit_row").html(html);
                editButtonsRow(jQuery(row), 5);
                cancelUpdateEvent(jQuery('#edit-cancel'), fsr, row);
                updateEvent();
        }
     })
     .fail(function(jqXHR) {
        row.replaceWith(fsr);
        jQuery('#sessassist_table').find("input, select, a, img").css('display', '');
        editEvent(fsr.find('a[id^=edit_]'));
        addCheckboxEvent(fsr.find('input[id^=sessassist-check]'), "sessassist");
        alert('The request failed, Please try again');
     });

}
//Update Event
function updateEvent() {
    jQuery('#edit-update').on('click', function(e) {
        e.preventDefault();
        if (validateAssistant('edit')) {
            jQuery('#edit-update').css('display', 'none');
            jQuery('#edit-cancel').css('display', 'none');
            jQuery('#b_edit_td').attr('class', 'waittd2');

            var url = "osce/ajx_osce_assistants.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "update_assistant",
                    org_user_id: jQuery('#org_user_id').val(),
                    edit_id: jQuery('#edit-id').val(),
                    edit_fn: jQuery('#edit-fn').val(),
                    edit_sn: jQuery('#edit-sn').val(),
                    edit_email: jQuery('#edit-email').val(), edit_mobile: jQuery('#edit-mobile').val(),
                    session_id: jQuery('#session-id').val()
                }
             })
             .done(function(html){
                if (html === "SESSION_NON_EXIST") {
                    jQuery('#b_edit_td').attr('class', 'editbts');
                    alert('The Session was not found in the System or has been recently deleted, please return to Session list');
                    refreshPage(false);
                } else if (html === "ASSISTANT_NON_EXIST") {
                    jQuery('#b_edit_td').attr('class', 'editbts');
                    alert('The Assistant you were trying to update was not found in the System or has recently been deleted, the Session Assistants list will now be refreshed');
                    refreshPage(false);
                } else if (html === "ASSISTANTID_DOES_EXIST") {
                    failEdit();
                    alert('The Assistant ID you specified is already in use in the System, please specify a different ID');
                } else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failEdit();
                alert('The request failed, Please try again');
             });
        }
    });
}

//Cancel Update Event
function cancelUpdateEvent(button, org, oldrow) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#sessassist_table').find("input, a, img, select").css('display', '');
        editEvent(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=sessassist-check]'), "sessassist");
    });
}

//Validate Submit Form
function ValidateForm(form) {
    if (jQuery(form).attr('name') === 'sessassist_list_form') {
        var ischecked = false;
        var thecbs = jQuery('#sessassist_list_form').find('input[id^=sessassist-check]');

        jQuery.each(thecbs, function(i, cb) {
            ischecked |= jQuery(cb).prop('checked');
        });

        // @TODO This function will not always return a value; this needs to be fixed.
        if (ischecked) {
            return confirm("Are you sure you want to remove the selected Assistant(s) from the Assistant List");
        }
    }
}