/**
 * Vue.js component that displays the information of a student in a station in the rotations monitor
 */
Vue.component('station-monitor', {
   template: '#station-monitor-template',

   props: [
       'studentStation',
       'rotationsLogic'
   ],

   methods: {
       /**
        * Get the classes that a cell representing a specific student in a specific station may have
        * @param studentStation
        * @returns {Object}
        */
       studentStationClass: function(studentStation) {
           return {
               // Is not a blank student
               'station': studentStation.student.hasOwnProperty("student_id"),
               // Is complete or a discarded blank student
               'success': (this.rotationsLogic.isCompleteStation(studentStation) && !studentStation.absent && this.rotationsLogic.getProgressPercentage(studentStation.progress) === 100) ||
               (studentStation.student.hasOwnProperty("discarded") && studentStation.student.discarded),
               // The examiner opened the scoresheet but hasn't started scoring yet
               'open-scoresheet': studentStation.opened && studentStation.progress.length === 0,
               // Is marked as complete but not all examiners have finished
               'warning': (studentStation.complete.some(this.rotationsLogic.isCompleteExaminer) &&
                   this.rotationsLogic.getProgressPercentage(studentStation.progress) < 100) ||
               (!this.rotationsLogic.isCompleteStation(studentStation) && this.rotationsLogic.shouldBeDone(studentStation) && !(this.rotationsLogic.isRestStation(studentStation.station))),
               // The student is marked as absent
               'danger': studentStation.absent,
               // Is a non discarded blank student
               'blank': (studentStation.student.hasOwnProperty('blank_student_id') && !studentStation.student.discarded),
               // Is a rest station
               'rest-station': this.rotationsLogic.isRestStation(studentStation.station),

               'blank-discarded': (studentStation.student.hasOwnProperty("discarded") && studentStation.student.discarded)
           };
       },

       displayStudentId: function() {
           if (!this.$root.displayStudentIDs)
               return "-";
           return this.studentStation.student[rotationsMonitor.studentIdKey];
       },

       detailStudentStation: function(studentStation) {
           if (studentStation.student.hasOwnProperty("student_id"))
               this.$emit('detailed-student', {
                   student: studentStation,
                   studentIdDisplay: this.displayStudentId(),
                   rotationsLogic: this.rotationsLogic
               });
       }

   }

});