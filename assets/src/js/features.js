/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 05/04/2015
   © 2015 Qpercom Limited.  All rights reserved.
   Manage Features Section
*/

jQuery(document).ready(function() {
 
 // Delegate role checkbox click events
 if (jQuery('#features-dl').length && jQuery('#update-features').length) {
     jQuery('#features-dl').on('click', 'input[type=checkbox]', function() {
        jQuery('#update-features').prop('disabled', false).prop('value', 'update');
     });
     
    // Show group of features filtered
    var selectedGroup = jQuery('#feature-groups-select').val();
    jQuery('.group-' + selectedGroup).removeClass('hide');
 }
 
 // Groups drop-down event
 if (jQuery('#feature-groups-select').length) {
     jQuery('#feature-groups-select').change(function() {
       var groupID = jQuery(this).val();
       jQuery('#features-dl').find('dd, dt').addClass('hide');
       jQuery('.group-' + groupID).removeClass('hide');
    });
 }
 
showMenu(); 
});