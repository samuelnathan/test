/* Original Author: David Cunningham
 * For Qpercom Ltd
 * Date: 13/04/2013
 * © 2013 Qpercom Limited. All rights reserved.
 * @Examiners SMS Section
 */

jQuery(document).ready(function() {
    /* Cancel Button */
    if (jQuery('#sms-cancel').length) {
        jQuery('#sms-cancel').on('click', function(e) {
            e.preventDefault();
            window.parent.getDialogObject().dialog('close');
        });
    }

    /* Sms Examiners Delete Event */
    jQuery('#sms-examiners').on('click', 'img', function(e) {
        e.preventDefault();
        jQuery(this).parents('div:first').remove();
    });
});

//Validate SMS Form
function ValidateForm() {
    //Form Checks
    if (jQuery('#sms-message').length && jQuery('#sms-message').val().length === 0) {
        alert("Please complete 'SMS Message' field");
        return false;
    } else if (jQuery('.examiner-id').length === 0) {
        alert("No 'Recipients' to send to. Please close popup window");
        return false;
    } else {
        return true;
    }
}