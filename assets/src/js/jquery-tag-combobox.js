/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

jQuery.widget("custom.combobox", {
  _create: function() {
    this.wrapper = jQuery("<span>")
      .addClass("custom-combobox")
      .insertAfter(this.element);

    this.element.hide();
    this._createAutocomplete();
    this._createShowAllButton();
  },

  _createAutocomplete: function() {
    var selected = this.element.children(":selected"),
      value = selected.val() ? selected.text() : "";

    this.input = jQuery("<input>")
      .appendTo(this.wrapper)
      .val(value)
      .attr("title", "Alphanumeric and hyphen characters only")
      .attr("placeholder", "+ tags (optional)")
      .attr('id', this.element.attr('id') + '-input')
      .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left ignore")
      .autocomplete({
        delay: 0,
        minLength: 0,
        source: jQuery.proxy(this, "_source")
      });

    this._on(this.input, {
      autocompleteselect: function(event, ui) {
        ui.item.option.selected = true;
        this._trigger("select", event, {
          item: ui.item.option
        });
      }
    });
  },
  
  _createShowAllButton: function() {
    var input = this.input,
      wasOpen = false;

    jQuery("<a>")
      .attr("tabIndex", -1)
      .appendTo(this.wrapper)
      .button({
        icons: {
          primary: "ui-icon-triangle-1-s"
        },
        text: false
      })
      .removeClass("ui-corner-all")
      .addClass("custom-combobox-toggle ui-corner-right")
      .on("mousedown", function() {
        wasOpen = input.autocomplete("widget").is(":visible");
      })
      .on("click", function() {
        input.trigger("focus");

        // Close if already visible
        if (wasOpen) {
          return;
        }

        // Pass empty string as value to search for, displaying all results
        input.autocomplete("search", "");
      });
  },

  _source: function(request, response) {
    var matcher = new RegExp(jQuery.ui.autocomplete.escapeRegex(request.term), "i");
    response(this.element.children("option").map(function() {
      var text = jQuery(this).text();
      if (this.value && (!request.term || matcher.test(text)))
        return {
          label: text,
          value: text,
          option: this
        };
    }));
  },

  _destroy: function() {
    this.wrapper.remove();
    this.element.show();
  }
});