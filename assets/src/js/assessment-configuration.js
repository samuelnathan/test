/* 
 * Original author Kelvin Nunn
 * For Qpercom
 * Date 12/09/17
 *
 * Javascript logic for the assessment form configuration
 *
 */

/**
 * Setup the assessment configuration dialog
 */
function setupDialog() {
    var configDialog = jQuery('#assessment-config-dialog').dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
        resizable: false
    });

    jQuery(document).on('click', '.ui-widget-overlay', function () {
        configDialog.dialog('close');
    });

    jQuery('#assessment-config-btn').click(function() {
        configDialog.parent().css({position:"fixed"}).end().dialog('open');
    });
}

/**
 * Use cookie to get text scaling and current value is default
 */
function setupTextScaling() {
    //get cookie for assessment scale store value in variable
    var cookie = jQuery.cookie('assess-scale-accessibility');

    var textScaleSelect = jQuery('#textselect');
    textScaleSelect.val(cookie);

    // Adjust text scale, icon size, and cookie on selection
    textScaleSelect.change(function() {
        var selectedSize = this.value;

        jQuery('#assessment-body').parents('html').css('font-size', selectedSize + 'px');
        jQuery.cookie('assess-scale-accessibility', selectedSize);
    });
}

jQuery(document).ready(function() {
    setupDialog();
    setupTextScaling();
});



