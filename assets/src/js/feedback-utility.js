/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

// Global Scope Variables
var tempDialog;

// Add Stations Events
function addStationsEvents() {
    /* If there's only one unchecked item in the list that's not of the class 
     * assigned to individual checkboxes, then everything bar the "all" checkbox
     * is checked. Logically, it should be too.
     */
    jQuery('#station-list').on('click', '.include-stations', function () {
        var filter = 'input:not([class=include-stations]:checked)';
        var isChecked = (jQuery('#station-list').find(filter).length === 1);
        jQuery('#include-all-stations').prop('checked', isChecked);
    });
}

// Add Students Events
function addStudentsEvents() {
    /* If there's only one unchecked item in the list that's not of the class 
     * assigned to individual checkboxes, then everything bar the "all" checkbox
     * is checked. Logically, it should be too.
     */
    jQuery('#student-list').on('click', '.include-students', function () {
        var filter = 'input:not([class=include-students]:checked)';
        var isChecked = (jQuery('#student-list').find(filter).length === 1)
        jQuery('#include-all-students').prop('checked', isChecked);
    });
}

// Toggle Options
function toggleOptions(feedbackReady) {
    
    if (feedbackReady) {
        jQuery('#feedback-button').removeClass('btn-success').addClass('btn-outline-success');
    }

    if (!feedbackReady) {
        jQuery('#feedback-button').removeClass('btn-outline-success').addClass('btn-success');
    }

    jQuery('#feedback-button').prop('disabled', feedbackReady);
    jQuery('#edit-template-button, #clone-template-button').prop('disabled', feedbackReady);
    jQuery('#exam').prop('disabled', feedbackReady);
    jQuery('#template').prop('disabled', feedbackReady);
    jQuery('#session').prop('disabled', feedbackReady);
    jQuery('#term').prop('disabled', feedbackReady);

}

// Add Feedback Link
function addFeedbackLinkEvents() {

    jQuery('#feedback').on('click', '.previewlink', function () {
        var id = jQuery(this).attr('id');
        var windowID = id.replace(/[^a-z0-9\s]/gi, '');
        var winpop = window.open(
           'manage.php?page=prevfb_tool&n=' + id, 
           windowID
        );
        winpop.focus();
    });
    
}

// Add Feedback Delete Events
function addFeedbackDeleteEvents() {
    jQuery('#feedback-body').off('click', '.deletelink').on('click', '.deletelink', function (e) {
        e.preventDefault();

        var parentElement = jQuery(this).parents('tr:first');
        var linkText = parentElement.find('a').text();

        // Remove the file extension '.pdf' from the end of filename
        var studentID = linkText.substring(0, linkText.length - 4);

        if (confirm("Are you sure you want to delete this feedback record? \n\n'OK' = Yes \n'Cancel' = No")) {
            jQuery.ajax({
                url: "tools/email_feedback.php",
                type: "post",
                dataType: "json",
                cache: false,           
                data: {
                    isfor: 'delete_record', 
                    exam: jQuery('#exam').val(), 
                    student: studentID
                }})
                .done(function(deleted) {
                    if (Boolean(deleted)) {
                        parentElement.remove();
                    }
                });
            }
    });
}

// Add Email Link Event
function addEmailLinkEvent() {
    
    var pollFeedbackCall;
    
    jQuery('#send-link').off('click').click(function () {
 
        var currentExamID = jQuery('#exam').val();
        var messageToSender = "Are you sure you want to email feedback to " 
                + jQuery("#student-translation3").text() 
                + "? \n\n'OK' = Yes \n'Cancel' = No";
        
        if (confirm(messageToSender)) {
            
            var statusCell = jQuery('<td>', {'colspan': '10'});
            var spinnerImage = jQuery('<img>', {'id': 'generating-img', 'src': 'assets/images/wait2.gif', 'alt': 'Sending...'});
            var statusSpan = jQuery('<span>', {'id': 'generating-span', 'text': 'Sending feedback to '
                           + jQuery('#student-translation3').text() +'....'});
            
            toggleOptions(true);
            
            if (jQuery('#send-link').length) {
               jQuery('#send-link').hide();
            }

            if (jQuery('#download-link').length) {
                jQuery('#download-link').hide();
            }
            
            // Add status row
            jQuery('#feedback-body').prepend(
              jQuery('<tr>', {id: 'status-row'}).prepend(statusCell) 
            );            
            jQuery('.left-cells').html('&nbsp;');
            
            statusCell.append(statusSpan);
            
            // Remove hyper link for PDF previews, left with raw text
            jQuery('.pdf-preview').each(function() {
               var rawText = jQuery(jQuery(this).html()).text();
               jQuery(this).text(rawText);
            });
            
            // Add wait icon to each record
            jQuery('.status-ready').empty().append(spinnerImage);
          
            jQuery.ajax({
                url: "tools/email_feedback.php",
                type: "post",
                dataType: "json",
                cache: false,           
                data: {
                  exam: currentExamID
                },
                beforeSend: function() {
                                   
                  pollFeedbackCall = setInterval(function() {
                  
                    jQuery.getJSON('tools/poll_feedback.php', 
                      { exam: currentExamID }, 
                      function (sendStatus) {
                          updateFeedbackRowStatus(sendStatus);
                      });
                   
                  }, 6000);
                  
               }
             })
             .done(function(feedbackSent) {
                clearInterval(pollFeedbackCall);
                updateFeedbackRowStatus(feedbackSent);
                jQuery('#status-row').remove();
                toggleOptions(false);
                    
             })
             .fail(function(jqXHR) {
                 
                clearInterval(pollFeedbackCall); 
                toggleOptions(false);
                alert(
                  httpStatusCodeHandler(
                     jqXHR.status,
                     "Failed to process feedback.\n"
                ));
                
             });
                 
           }
           
        });
        
}

/**
 * Update feedback record
 */
function updateFeedbackRowStatus(records) {


      if (records.length == 0) {
         return false;
      }
     
      jQuery.each(records, function(i, record) {
      
         var studentRow = jQuery(document.getElementById("student-" + record.id));
         var status = (record.is_sent === '1') ? 'sent' : 'failed';
         studentRow.find('.status-ready').attr('class', 'status-' + status).text(status.capitalize());

         // Email sent, then display timestamp sent
         var timestamp = (record.is_sent === '1') ? record.time_sent : " ---- ";
         studentRow.find('.time-sent-cell').html(timestamp);
     
      });
      
}

// Get Dialog Object
function getDialogObject() {
    return tempDialog;
}

// Dom ready (page ready) event 
jQuery(document).ready(function() {
    var includeStudents = [];
    var studentCount = 0;
    var stationCount = 0;

    if (jQuery('#feedback-button').length) {
        jQuery('#feedback-button').on('click', function (e) {
            e.preventDefault();
    
            var getValue = function (element) {
                return jQuery(element).val();
            };

            // Get excluding stations 
            var excludeStations = jQuery.map(jQuery('#station-list').find('input[class=include-stations]:not(:checked)'), getValue);

            // Get excluded students
            includeStudents = jQuery.map(jQuery('#student-list').find('input[class=include-students]:checked:enabled'), getValue);

            var excludeStationsCount = excludeStations.length;
            var includeStudentsCount = includeStudents.length;

            var totalStationCount = stationCount - excludeStationsCount;

            if (includeStudentsCount > 0 && totalStationCount > 0 && jQuery('#exam').val().length > 0 && jQuery('#template').val().length > 0) {

                toggleOptions(true);
                
                var tdEle = jQuery('<tr>').append(jQuery('<td>', {'id': 'gen-status', 'colspan': '10'}));
                jQuery('#feedback-body').empty().append(tdEle);

                var statusSpan = jQuery('<span>', {'id': 'generating-span', 'text': 'Generated 0/' + includeStudentsCount + ' Feedback Forms'});
                var spinnerImage = jQuery('<img>', {'id': 'generating-img', 'src': 'assets/images/wait2.gif', 'alt': 'Please Wait...'});
                jQuery('#gen-status').append(spinnerImage, statusSpan);

                // This is the execution limit for the process. If it takes the
                // server more than this number of milliseconds to show some
                // sort of progress (i.e. the generation of another PDF), then
                // something's wrong, there's no point in keeping asking the
                // server how it's getting on. Obviously, this value needs to be
                // tuned to suit server speed.
                var statusRequestRepeatDelay = 1000;
                var statusRequestRuntime = statusRequestRepeatDelay;
                var statusRequestLimit = 120000; // 2 minutes
                var statusRequestCompleted = 0;
                var genExamID = jQuery('#exam').val();
                
                var pollGenFeedback = function() {

                    jQuery.ajax({
                        url: 'tools/json_tools.php',
                        type: 'POST',
                        dataType: 'json',
                        cache: false,
                        data: {
                          isfor: 'feedback_count',
                          exam: genExamID
                        }
                        })
                        .done(function(status) {

                            if (statusRequestCompleted !== parseInt(status[0].complete)) {

                                statusRequestCompleted = parseInt(status[0].complete);
                                statusRequestRuntime = statusRequestRepeatDelay;

                            } else {

                                statusRequestRuntime += statusRequestRepeatDelay;

                            }
    
                            if (statusRequestRuntime >= statusRequestLimit) {

                                jQuery('#generating-img').src = 'assets/images/exclamation.png';
                                var errorMessage = "Server taking too long - no progress after " + statusRequestLimit / 1000 
                                                 + " seconds.\nPlease refresh the page via the browser and try again.";
                                jQuery('#generating-span').text(errorMessage);
                                alert(errorMessage);
                                clearInterval(intervalGenFeedback);
                                
                            } else {

                                // If we're still waiting to finish, report how many are done so far.
                                if(jQuery('#generating-span').length > 0) {

                                    jQuery('#generating-span').html('Generated '
                                        + statusRequestCompleted + '/' + includeStudentsCount
                                        + ' Feedback Forms');

                                }

                        }

                    });

                };

                var genSessionID = jQuery('#session').val();
                var intervalGenFeedback;
                var feedbackData = {
                    
                    // Feedback flag fields
                    content_score_text: (jQuery('#score-text').prop('checked')) ? 1 : 0,
                    content_score_descriptors: (jQuery('#score-descs').prop('checked')) ? 1 : 0,
                    content_score_values: (jQuery('#score-values').prop('checked')) ? 1 : 0,
                    content_nonscore_text: (jQuery('#non-score-text').prop('checked')) ? 1 : 0,
                    content_nonscore_descriptors: (jQuery('#non-score-descs').prop('checked')) ? 1 : 0,
                    content_nonscore_values: (jQuery('#non-score-values').prop('checked')) ? 1 : 0,
                    content_grs_text: (jQuery('#scale-text').prop('checked')) ? 1 : 0,
                    content_grs_descriptors: (jQuery('#scale-descs').prop('checked')) ? 1 : 0,
                    content_section_titles: (jQuery('#section-titles').prop('checked')) ? 1 : 0,
                    content_comments: (jQuery('#comments').prop('checked')) ? 1 : 0,
                    content_finalscore_raw: (jQuery('#final-scores-raw').prop('checked')) ? 1 : 0,
                    content_finalscore_percent: (jQuery('#final-scores').prop('checked')) ? 1 : 0,
                    content_finalscore_weighted: (jQuery('#final-scores-weighted').prop('checked')) ? 1 : 0,
                    content_finalscore_weighted_percent: (jQuery('#final-scores-weighted-percent').prop('checked')) ? 1 : 0,
                    content_multiexaminer_ratings: (jQuery('#multi-examiners').prop('checked')) ? 1 : 0,
                    content_observer_ratings: (jQuery('#include-observers').prop('checked')) ? 1 : 0,
                    content_station_notes: (jQuery('#station-notes').prop('checked')) ? 1 : 0,

                    radarplot_comparison: (jQuery('#radar-stations').prop('checked')) ? 1 : 0,
                    radarplot_competency: (jQuery('#radar-competencies').prop('checked')) ? 1 : 0,

                    summary: (jQuery('#exam-summary').prop('checked')) ? 1 : 0,
                    summary_scores: (jQuery('#summary-scores').prop('checked')) ? 1 : 0,
                    summary_grs: (jQuery('#summary-grs').prop('checked')) ? 1 : 0,
                    summary_outcomes: (jQuery('#summary-outcomes').prop('checked')) ? 1 : 0,
                    summary_overall_score: (jQuery('#summary-score-overall').prop('checked')) ? 1 : 0,
                    summary_overall_outcome: (jQuery('#summary-outcome-overall').prop('checked')) ? 1 : 0,
                    item_weighting: (jQuery('#item-weightings').prop('checked')) ? 1 : 0,
                    
                    // Other variables
                    var_notesTitle: jQuery('#notes-title').val(),
                    var_exam: jQuery('#exam').val(),
                    var_template: jQuery('#template').val(),                    
                    var_excludedStations: excludeStations,
                    var_includedStudents: includeStudents
                };

                jQuery.ajax({
                    url: 'pdfgen/pdf_feedback.php',
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    data: feedbackData,
                    beforeSend: function () {

                        if (jQuery('#send-link').length) {
                            jQuery('#send-link').hide();
                        }
                        
                        if (jQuery('#download-link').length) {
                            jQuery('#download-link').hide();
                        }
                         
                        intervalGenFeedback = setInterval(pollGenFeedback, 1000);

                    }
                  })
                  .done(function(students) {

                    clearInterval(intervalGenFeedback);
                    jQuery('#feedback-body').empty();
                        var totalNumOfStudents = students.length;

                        if (totalNumOfStudents > 0) {
                            var studentCounter = 0;
                            jQuery.each(students, function (i, student) {
                                studentCounter++;

                                var sentTR = jQuery('<tr>', {'id': 'student-'+student.student_id});
                                if (totalNumOfStudents === studentCounter) {
                                    sentTR.attr('class', 'nbbtr');
                                }

                                var deleteLink = jQuery('<img>', {'src': 'assets/images/deletetiny.svg', 'class': 'deletelink', 'alt': 'Delete Feedback', 'title': 'Delete Feedback'});
                                var previewLink = jQuery('<a>', {'href': 'javascript:void(0)', 'class': 'previewlink', 'id': student.file, 'title': 'Click to Preview'})
                                        .html(student.student_id + ".pdf");
                                var dataCell1 = jQuery('<td>', {'class': 'left-cells'}).append(deleteLink);
                                var dataCell2 = jQuery('<td>', {'class': 'c'}).text('#' + studentCounter);
                                var dataCell3 = jQuery('<td>', {'class': 'pdf-preview'}).append(previewLink);
                                var dataCell4 = jQuery('<td>').text(student.forename + ' ' + student.surname);
                                var dataCell5 = jQuery('<td>').text(student.email);
                                var dataCell6 = jQuery('<td>', {'class': 'c'}).text(student.template_name);
                                var highlight = student.receives_instructions ? "bold red" : "";
                                var dataCell7 = jQuery('<td>', {'class': 'c '+ highlight}).text(
                                    student.receives_instructions ? "Yes" : "No"
                                );
                                var dataCell8 = jQuery('<td>', {'class': 'time-created-cell'}).text(student.time_created);
                                var dataCell9 = jQuery('<td>', {'class': 'time-sent-cell'}).html("&nbsp;");
                                var dataCell10 = jQuery('<td>', {'class': 'status-ready'}).text('Ready');

                                jQuery('#feedback-body').append(
                                   sentTR.append(
                                       dataCell1, dataCell2, dataCell3, dataCell4, dataCell5, 
                                       dataCell6, dataCell7, dataCell8, dataCell9, dataCell10
                                    )
                                );

                                jQuery(".left-cells").show();
                                
                            });

                            if (jQuery('#generated-feedback-exam-id').length) {
                                jQuery('#generated-feedback-exam-id').val(genExamID);
                                jQuery('#generated-feedback-session-id').val(genSessionID);
                            }

                            if (jQuery('#send-link').length) {
                                jQuery('#send-link').show().removeClass('btn-outline-success').addClass('btn-success');
                            }

                            if (jQuery('#download-link').length) {
                                jQuery('#download-link').show().removeClass('btn-outline-info').addClass('btn-info');
                            }                           

                            addEmailLinkEvent();
                            addFeedbackDeleteEvents();
                            addFeedbackLinkEvents();

                        }
                        toggleOptions(false);
                        jQuery('#feedback-button').removeClass('btn-success')
                        .addClass('btn-outline-success');
                  })
                  .fail(function() {
                    clearInterval(intervalGenFeedback);  
                    alert('Could not get requested data, please try again or refresh page');
                    toggleOptions(false);
                  })
            } else {
                alert('Please complete all fields before continuing');
            }
        });
    }

    /* Edit|Clone Template */
    jQuery('#edit-template-button, #clone-template-button').on('click', function (e) {
        e.preventDefault();

        if (jQuery('#template').val().length === 0) {

            alert('Please choose an Email Template from dropdown list');

        } else {

            var templateUrl = 'manage.php?page=template_tool&mode=edit&template=' + jQuery('#template').val();
            templateUrl += (jQuery(this).attr('id') == "clone-template-button") ? "&clone=yes" : templateUrl;

            tempDialog = jQuery('<div>').html('<iframe id="ifdialog" src="'+templateUrl+'">').dialog({
                    title: 'Edit Feedback Email Template',
                    dialogClass: "templateDialog",
                    closeOnEscape: false,
                    draggable: false,
                    modal: true,
                    minWidth: 870,
                    open: function () {

                      jQuery("body").addClass("dialog-open");

                    },
                    close: function () {

                    // Get new template ID
                    var newTemplateID = jQuery('#ifdialog')
                    .contents().find('#template-id-edit').val();

                    // Update template list
                    jQuery.ajax({
                        url: "tools/json_tools.php",
                        type: "post",
                        dataType: "json",
                        cache: false,
                        data: {
                          isfor: 'templates'
                        }      
                     })       
                     .done(function(templates) {
                         
                        if (templates.length > 0) {

                          jQuery('#template').empty().append(
                             jQuery('<option>', {
                                'value': "", 
                                'text': "--" 
                             })
                          );

                          jQuery.each(templates, function() {

                            var template = jQuery(this)[0];

                            jQuery('#template').append(
                               jQuery('<option>', {
                                   'value': template.id, 
                                   'text': template.name 
                               })
                            );

                          });

                          // Select new template ID
                          jQuery('#template').val(newTemplateID);

                        }

                        tempDialog.dialog("destroy");

                        // Re-focus the generate feedback button
                        jQuery('#feedback-button').removeClass('btn-outline-success').addClass('btn-success');
                        jQuery('#download-link').removeClass('btn-info').addClass('btn-outline-info');
                        jQuery('#send-link').removeClass('btn-success').addClass('btn-outline-success');
                           
                     })
                     .fail(function(jqXHR) {
                             
                        alert(
                         httpStatusCodeHandler(
                            jqXHR.status,
                            "Could not update template list.\n"
                        ));

                      });
                      
                      jQuery("body").removeClass("dialog-open");
                      
                    }
                }
            );
    
        }
    });

    // Checkbox and select options change - buttons visual highlighting
    jQuery('#feedbackfs').on('change', "input[type='checkbox'], select", function() {
        
        // Feedback generate button
        var feedbackButtonEnabled = (jQuery('#template').val().length !== 0 && jQuery('#exam').val().length !== 0);
        if (feedbackButtonEnabled) {

            jQuery('#feedback-button').removeClass('btn-outline-success').addClass('btn-success');

        }

        // Send and download buttons
        if (jQuery('#download-link').is(':visible') || jQuery('#send-link').is(':visible')) {

            jQuery('#download-link').removeClass('btn-info').addClass('btn-outline-info');
            jQuery('#send-link').removeClass('btn-success').addClass('btn-outline-success');

        }

    });

    // Template change event
    jQuery('#template').change(function () {

        var feedbackButtonDisabled = (jQuery(this).val().length === 0 || jQuery('#exam').val().length === 0);
        jQuery('#feedback-button').prop('disabled', feedbackButtonDisabled);                              

        if (feedbackButtonDisabled) {
            jQuery('#feedback-button').removeClass('btn-success').addClass('btn-outline-success');
        }
    
        if (!feedbackButtonDisabled) {
            jQuery('#feedback-button').removeClass('btn-outline-success').addClass('btn-success');
        }  

        jQuery('#edit-template-button').prop('disabled', (jQuery(this).val().length === 0));
        jQuery('#clone-template-button').prop('disabled', (jQuery(this).val().length === 0));           

    });
    
    // Notes change event
    jQuery('#station-notes').change(function () {

        jQuery('#notes-title').prop('disabled', !jQuery(this).is(':checked'));

    });

   
    // Exam summary checkbox event
    jQuery('#exam-summary').change(function () {
        
        jQuery('#summary-scores').prop('checked', jQuery(this).is(':checked'));
        jQuery('#summary-outcomes').prop('checked', jQuery(this).is(':checked'));
        jQuery('#summary-grs').prop('checked', jQuery(this).is(':checked'));
        jQuery('#summary-score-overall').prop('checked', jQuery(this).is(':checked'));
        jQuery('#summary-outcome-overall').prop('checked', jQuery(this).is(':checked'));
        
    });

    var selectorString = "#summary-scores, #summary-outcomes, " + 
            "#summary-grs, #summary-score-overall, #summary-outcome-overall";
        
    // Sub options checkbox events
    jQuery(selectorString).change(function () {
       
        var mainChecked = (jQuery('#summary-scores').is(':checked') || 
        jQuery('#summary-outcomes').is(':checked') || jQuery('#summary-grs').is(':checked'));
        jQuery('#exam-summary').prop('checked', mainChecked);

        if (!jQuery('#summary-scores').is(':checked')) {
            jQuery('#summary-score-overall').prop('checked', false);
        }
       
        if (!jQuery('#summary-outcomes').is(':checked') && !jQuery('#summary-grs').is(':checked')) {
            jQuery('#summary-outcome-overall').prop('checked', false);
        }
        
    });

    // Term event
    jQuery('#term').change(function () {

        jQuery.ajax({
            url: "tools/json_tools.php",
            type: "post",
            data: {
                isfor: "exams",
                term: jQuery('#term').val()
            },
            dataType: "json"
        }).done(function(data) {

                jQuery('#exam').empty()
                    .append(jQuery("<option>", {'value': '--'}));

                if (data.length == 0) {
                    return false;
                }

                jQuery.each(data.schools, function(optSchool, optDepartments) {

                    var optGroup = jQuery("<optgroup>", {'class': 'school-label', 'label': optSchool + ':'});

                    jQuery.each(optDepartments, function(optDepartment, optExams) {

                        var innerOptGroup = jQuery(
                            "<optgroup>",
                            {'class': 'department-label', 'label': "~ " + optDepartment}
                        );

                        jQuery.each(optExams, function(exID, optExam) {

                            innerOptGroup.append(
                                jQuery("<option>", {'text': optExam.exam_name}).val(exID)
                            );

                        });

                        optGroup.append(innerOptGroup);

                    });


                    // Append to select box
                    jQuery('#exam').append(optGroup);
                    jQuery('#exam').html(jQuery('#exam').html());
                });

            })
            .fail(function(jqXHR) {

                alert(httpStatusCodeHandler(
                    jqXHR.status,
                    "Failed to retrieve data, please try/make selection again.\n"
                ));


            });

    });

    // OSCE (exam) and Session dropdown event
    if (jQuery('#exam').length && jQuery('#session').length) {
        
        jQuery('#exam, #session').on('change', function () {
            
            // Remove any warning messages for new selections
            jQuery('#feedback-warning').removeClass('show');

            // Element ID (exam or session dropdown)
            var elementID = jQuery(this).attr('id');
           
            // Disable feedback button by default
            jQuery('#feedback-button')
            .addClass('btn-outline-success')
            .removeClass('btn-success')
            .prop('disabled', true);

            // No exam value then abort
            if (elementID === "exam") {
                
              jQuery('#session').empty();
              
              if (jQuery(this).val().trim().length === 0) {
                  
                 // Reset session dropdown list
                 jQuery('#session').append(
                    jQuery('<option>', 
                   {'text': 'All Days/Circuits', 'class': 'bold', 'value': ''})
                 );
                 jQuery('#station-list').empty();
                 jQuery('#student-list').empty();
                 
                 return false;
                 
              // Examiner selected, then render exam sessions
              } else {

                // Apply loading status
                jQuery('#session').append(jQuery('<option>', {'text': 'loading..', 'value': ''}));
                jQuery.ajax({
                    url: 'tools/json_tools.php',
                    type: "post",
                    cache: false,
                    data: {
                        isfor: 'get_exam_sessions',
                        exam: jQuery('#exam').val()
                    },
                    dataType: "json"
                })
                .done(function(sessions) {
                    // append each session to session list
                    jQuery.each(sessions, function (i, session) {
                        jQuery('#session').append(jQuery('<option>', {'text': session.name, 'value': session.id}));
                    });                      

                    // Set default option text
                    jQuery('#session').find('option:first').text('All Days/Circuits');
                })
                .fail(function(jqXHR) {
                    alert('Could not get requested data, please make a or refresh page');
                })
                    
             }
             
           }
            
            // Include Students and Stations
            jQuery('#station-list').empty().append(jQuery('<li>', {'text': 'loading.....'}));
            jQuery('#student-list').empty().append(jQuery('<li>', {'text': 'loading.....'}));

            // Get list of stations and students from server in Json format
            jQuery.ajax({
                url: 'tools/json_tools.php',
                type: "post",
                cache: false,
                data: {
                    isfor: 'stations_students',
                    exam: jQuery('#exam').val(),
                    session: jQuery('#session').val()
                },
                dataType: "json"
            })
            .done(function(dataBack) {
                    jQuery('#student-list').empty();
                    jQuery('#station-list').empty();

                    stationCount = dataBack[0].length;
                    studentCount = dataBack[1].length;
                                    
                    // Station Processing
                    if (stationCount > 0) {
                                
                        // Create Check All Check Box, Stations
                        var allStationsLi = jQuery('<li>');
                        allStationsLi.append(jQuery('<input>', {'type': 'checkbox', 'id': 'include-all-stations'}).prop('checked', true));
                        allStationsLi.append(jQuery('<label>', {'for': 'include-all-stations', 'text': 'All Stations (' + stationCount + ')'}));
                        jQuery('#station-list').append(allStationsLi);

                        // Add Station Check Boxes
                        jQuery.each(dataBack[0], function (index, station) {
                            var stationLi = jQuery('<li>');
                            stationLi.append(jQuery('<input>', {'class': 'include-stations', 'type': 'checkbox', 'id': 'stat' + station.station_number, 'value': station.station_number}).prop('checked', true));
                            stationLi.append(jQuery('<label>', {'for': 'stat' + station.station_number, 'text': 'Station ' + station.station_number}));
                            jQuery('#station-list').append(stationLi);
                        });

                        // Add Check All Event
                        jQuery('#include-all-stations').on('click', function () {
                            var checked = jQuery(this).prop('checked');
                            jQuery.each(jQuery('.include-stations'), function (index, el) {
                                jQuery(el).prop('checked', checked);
                            });
                        });

                        addStationsEvents();
                    } else {
                        jQuery('#station-list').empty().append(jQuery('<li>', {'class': 'red', 'text': 'No stations found in exam'}));
                    }

                    // Student Processing
                    if (studentCount > 0) {
                        //Create Check All Check Box, Students
                        var allStudentsLi = jQuery('<li>', {'id': 'include-all-students-li', 'class': 'student'});
                        allStudentsLi.append(jQuery('<input>', {'type': 'checkbox', 'id': 'include-all-students'}).prop('checked', true));
                        allStudentsLi.append(jQuery('<label>', {'for': 'include-all-students', 'text': 'All ' + jQuery('#student-translation').text() +' (' + studentCount + ')'}));
                        jQuery('#student-list').append(allStudentsLi);

                        // Add Student Check Boxes
                        var stuCount = 0;
                        var groupID = null;
                        var sessionID = null;
                        var warnUser = false;
                        
                        jQuery.each(dataBack[1], function (i, student) {
                            stuCount++;
                           
                            // Add session information
                            if (jQuery('#session').val().trim().length === 0) {
                                if (sessionID !== student.session_id) {
                                    var sessionName = student.session_name + ' - ' + student.session_date;
                                    jQuery('#student-list').append(jQuery('<li/>', {'class': 'session-name'})
                                                     .text(sessionName));
                                    sessionID = student.session_id;
                                }
                            }
                            
                            // Add group information
                            if (groupID !== student.group_id) {
                              jQuery('#student-list').append(jQuery('<li>', {'class': 'group-name'})
                                               .text(student.group_name));                                       
                              groupID = student.group_id;
                            }
                            
                            var studentName = student.student_id + ' ' + student.forename + ' ' + student.surname;
                            
                            /**
                             * If we have invalid email addresses for the student
                             * then we need to disable the student option
                             * and warn the user with an appropriate message
                             */
                            var validEmail = (!jQuery('#validate-emails').length || isEmailValid(student.email));
                            var disableStudentCB = (validEmail);
                            var tickCB = (validEmail);
                            var disabledStyle = (validEmail) ? '' : 'color: #ff0000 !important';
                            var disabledTitle = (validEmail) ? '' : 'Student email address is invalid';
                            
                            
                            if (!validEmail) {
                               warnUser = true;
                            }
                            
                            var studentLi = jQuery('<li>', {
                                'class': 'student',
                                'title': disabledTitle,
                                'style': disabledStyle
                            })
            
                            studentLi.append(jQuery('<input>', {
                                'class': 'include-students',
                                'type': 'checkbox',
                                'id': 'stu' + stuCount,
                                'value': student.student_id
                            }).prop('disabled', !disableStudentCB).prop('checked', tickCB));
                            
                            studentLi.append(jQuery('<label>', {'for': 'stu' + stuCount, 'text': studentName}));
                            jQuery('#student-list').append(studentLi);
                        });
                        
                        // If we need to warn the user then apply message and show
                        if (warnUser) {
                           jQuery('#feedback-warning').addClass('show');
                           
                           var studentTranslation = jQuery("#student-translation").text();
                           var studentTranslation3 = jQuery("#student-translation3").text();
                           var warnMessage = "The system has detected invalid email addresses for the " + studentTranslation3 + " highlighted in red below<br/>"
                                           + "making it impossible to send feedback to them. Please rectify these in "+ studentTranslation +" > Manage "+ studentTranslation;
                           jQuery('#feedback-warning').html(warnMessage);
                        }
                       
                        // Add Check All Event
                        jQuery('#include-all-students').on('click', function () {
                            var checked = jQuery(this).prop('checked');
                            jQuery.each(jQuery('.include-students'), function (index, el) {
                                jQuery(el).prop('checked', checked);
                            });
                        });

                        addStudentsEvents();
                    } else {
                        jQuery('#student-list')
                             .empty()
                             .append(jQuery('<li>', {'class': 'red', 'text': 'No ' + jQuery('#student-translation2').text() + ' results found'}));
                    }

                    // Enable feedback and download buttons
                    if (stationCount > 0 && studentCount > 0 && jQuery('#template').val().length > 0) {
                        jQuery('#feedback-button')
                           .removeClass('btn-outline-success')
                           .addClass('btn-success')
                           .prop('disabled', false);
                    }
            })
            .fail(function(jqXHR) {
                alert('Could not get requested data, please make a or refresh page');
            })
            
        });
        
    }
    
    // Student preview window title
    if (jQuery('#student-reference').length && jQuery('#student-reference').val().length > 0) {
        document.title = "Student " + jQuery('#student-reference').val() + " - Feedback";
    }

    showMenu();
});
