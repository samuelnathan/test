/**
 * Class that manages the WebSocket connection and the logic to update the data from the received events
 * @param wsEndpoint
 * @param sessionID
 * @param component
 * @constructor
 */
var SessionMonitor = function(wsEndpoint, sessionID, component) {

    var that = this;

    this.component = component;

    var ws = null;

    var actions = {
        /**
         * The progress of the scoring of a student in a station has been updated
         * @param {Object} updateData
         */
        "progress": function(updateData) {
            // Find the student and station referenced
            var station = that.component
                .rotationsLogic
                .getStation(updateData.student_id, updateData.station_id);
            if (!station) return;

            // Remove the existing progress for that scoring
            station.progress = station.progress.filter(function (examinerProgress) {
                return !updateData.progress.some(function (newProgress) {
                    return examinerProgress.examiner_id === newProgress.examiner_id;
                });
            });

            // Add the new progress for the scoring
            updateData.progress.forEach(function (examinerProgress) {
                station.progress.push(examinerProgress);
            });

            // Set it as the ongoing one if the examiner has started filling the form
            if (updateData.progress.length !== 0)
                this._updateOngoing(updateData);
            // If not, flag it as an open form
            else
                station.opened = true;
        },

        /**
         * A examiner has submited the scoring of a student in a station
         * @param {Object} updateData
         */
        "completion": function(updateData) {
            // Find the student and station referenced
            var station = that.component
                .rotationsLogic
                .getStation(updateData.student_id, updateData.station_id);

            // Try to find the completion status for that scoring
            var completed = station.complete.find(function (completeStatus) {
                return completeStatus.examiner_id === updateData.examiner_id &&
                    completeStatus.station_id === updateData.station_id
            });

            // If it exists, update it
            if (completed)
                completed.is_complete = updateData.newValue ? '1' : '0';
            // Otherwise, add it
            else
                station.complete.push({
                    "examiner_id": updateData.examiner_id,
                    "station_id": updateData.station_id,
                    "is_complete": updateData.newValue ? '1' : '0'
                });

            // Find the progress for the given examiner
            var examinerProgress = station.progress.find(function(examinerProgress) {
                return examinerProgress.examiner_id === updateData.examiner_id;
            });

            // If it exists, update the completion value
            if (examinerProgress)
                examinerProgress.is_complete = updateData.newValue ? '1' : '0';
        },

        /**
         * A examiner has marked an absence in a station
         * @param {Object} updateData
         */
        "absence": function(updateData) {
            this._updateFlag("absent", updateData);
            this._updateOngoing(updateData);
        },

        /**
         * A blank student has been discarded from a station
         * @param {Object} updateData
         */
        "discardBlank": function(updateData) {
            // Find the station
            var station = that.component
                .rotationsLogic
                .getStation(updateData.blank_student_id, updateData.station_id, true);
            if (!station) return;

            var value = updateData.newValue;
            if (station.student.discarded !== value)
                station.student.discarded = value;
        },

        "discard": function(updateData) {
            // Find the station
            var station = that.component
                .rotationsLogic
                .getStation(updateData.student_id, updateData.station_id);

            if (!station) return;

            station.progress = [];
            station.opened = false;
        },

        /**
         * The user has logged out in another tab
         */
        "logout": function() {
            if (rotationsMonitor.loggedOut)
                return;

            rotationsMonitor.loggedOut = true;
            alert("You have logged out from the system. Redirecting to login page...");
            document.location = "/";
        },

        "_updateFlag": function(flagName, updateData) {
            // Find the student and station referenced
            var station = that.component
                .rotationsLogic
                .getStation(updateData.student_id, updateData.station_id);
            if (!station) return;

            // Update the value if it's different from the previous one
            var value = updateData.newValue;
            if (station[flagName] !== value)
                station[flagName] = value;
        },

        "_updateOngoing": function(updateData) {
            that.component.rotationsLogic.updateOngoing(updateData);
            that.component.$emit('changed-ongoing');
        }

    };

    var constructor = function() {
        // Subscribe to events
        login();
    };

    var login = function () {
        jQuery.ajax('ajx_rotations_whitelist.php', {
            method: 'post',
            success: function (body) {
                subscribe(JSON.parse(body).token);
            }
        });
    };

    var subscribe = function(token) {
        // Establish the WebSocket connection to receive updates
        ws = new WebSocket(wsEndpoint);
        ws.onopen = function(_) {
            // Subscribe to the events of the session
            ws.send(JSON.stringify({
                sessionId: sessionID,
                token: token
            }));

            ws.onmessage = function (event) {
                // Assert that the message received is the Authentication OK. If it's not, show an error message
                // and finish
                if (event.data !== 'OK') {
                    alert('Authentication error');
                    return;
                }

                // Listen to the events and select the action to perform
                ws.onmessage = function (event) {
                    var msg = JSON.parse(event.data);

                    var action = msg.action;                     // Get the action to perform
                    actions[action](msg.data);                   // Perform the action given the type of action received
                }
            };
        };
    };

    constructor();
};

/**
 * Global object that manages the connections to the WebSocket server for the different monitored sessions
 * @type {{getMonitorForSession}}
 */
var SessionMonitorPool = (function (){
    var sessionPool = {};

    return {
        /**
         * Create or retrieve a SessionMonitor instance for a given session
         * @param sessionId
         * @param component
         * @returns SessionMonitor
         */
        getMonitorForSession: function(sessionId, component) {
            if (!sessionPool[sessionId])
                sessionPool[sessionId] = new SessionMonitor(rotationsMonitor.wsEndpoint, sessionId, component);
            else
                sessionPool[sessionId].component = component;

            return sessionPool[sessionId];
        }
    };
})();