/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2017 Qpercom Limited.  All rights reserved
 */

/**
 * Take a string that's supposed to have a date in it, slice it up at each
 * fromSeparator, apply the trim() method to each item, then recombine them so
 * that the outputted string is separated by the toSeparator character.
 * Domhnall Walsh 06/03/2014
 *
 * @param str date
 * @param String fromSeparator
 * @param String toSeparator
 * @returns date reformatted with new separator.
 */
function changeDateStringSeparator(date, fromSeparator, toSeparator)
{
    return date.split(fromSeparator).map(function (x) { return x.trim(); }).reverse().join(toSeparator);
}

/* empty clone td stations td select [Added 23/12/11] */
function emptyDropdown(ele, default_val, default_txt) {
    if (ele.length) {
        ele.empty().append(jQuery('<option/>', {
            'value': default_val,
            'class': 'black',
            'text': default_txt
        }));
    }
    return ele;
}

function selectRow(row_id, page) { //highlight row
    var rows;
    var checked_cnt = 0;

    rows = jQuery('#' + page + '_table').find('tr[id^=' + page + '_row_]');

    if (row_id > 0) {
        setRowClass(jQuery('#' + page + "_row_" + row_id));
        jQuery.each(rows, function(index, row) {
            if (jQuery(row).find('*').first().find('input:first').prop('checked') === true) {
                checked_cnt++;
            }
        });

        jQuery('#check-all').prop('checked', (checked_cnt === rows.length));

        return (checked_cnt > 0);
    } else {
        var check_all = jQuery('#check-all').prop('checked');
        jQuery.each(rows, function(index, row) {
            var cbobj = jQuery(row).find('*').first().find('input:first');

            if (check_all) {
                if (!cbobj.prop('disabled')) {
                    checked_cnt++;
                    cbobj.prop('checked', true);
                    jQuery(row).attr('class', 'datarowsel');
                }
            } else {
                cbobj.prop('checked', false);
                jQuery(row).attr('class', 'datarow');
            }
        });
        return (checked_cnt > 0);
    }
}

function setRowClass(row) {
    row.toggleClass('datarow');
    row.toggleClass('datarowsel');
}

function setUpCalendar() { //set up calender
    jQuery('#thedate').datepicker({
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'dd/mm/yy',
        showOn: 'both',
        buttonImage: 'assets/images/calendar.svg',
        buttonImageOnly: true,
        buttonText: 'Click to set/change date'
    });
}

function isDefined(obj) {
    return (obj !== undefined);
}

function chk(obj) {
    return !!(obj || obj === 0);
}

function disableGoButton(page, yes_no) {
    jQuery('#todo').prop('disabled', yes_no);
    jQuery('#' + 'go-' + page).prop('disabled', yes_no);
}

//Are fields complete - Text & Password fields only
function areFieldsComplete(row) {
    var allComplete = true;

    var tbs = row.find('input[type=text], input[type=password]');
    jQuery.each(tbs, function(index, el) {
        allComplete &= (jQuery(el).val().length !== 0);
    });

    return allComplete;
}

//Are fields complete version 2 - Text & Textareas fields
function areFieldsCompleteV2(row) {
    var allComplete = true;

    var tbs = row.find('input[type=text], input[type=password]');
    jQuery.each(tbs, function(index, el) {
        allComplete &= (jQuery(el).val().length !== 0);
    });

    var tas = row.find('textarea');

    jQuery.each(tas, function(index, el) {
        allComplete &= (jQuery(el).val().length !== 0);
    });

    var sels = row.find('select');
    jQuery.each(sels, function(index, el) {
        allComplete &= (jQuery(el).find('option').length !== 0);
    });

    return allComplete;
}

//Are fields complete version 3 - Text & Dropdown fields
function areFieldsCompleteV3(row) {
    var allComplete = true;

    var tbs = row.find('input[type=text], input[type=password]');
    jQuery.each(tbs, function(index, el) {
        allComplete &= (jQuery(el).hasClass('ignore') || jQuery(el).val().length !== 0);
    });

    var sels = row.find('select');
    jQuery.each(sels, function(index, el) {
        allComplete &= (jQuery(el).find('option').length !== 0);
    });

    return allComplete;
}

//Are date fields complete
function areDateFieldsComplete(row) {
    var tdate = row.find('input[id=thedate]').val();
    dateparts = tdate.split("/");
    cday = dateparts[0];
    cmonth = dateparts[1];
    cyear = dateparts[2];
    return !(cday.length !== 2 || isNaN(cday) || cmonth.length !== 2 || isNaN(cmonth) || cyear.length !== 4 || isNaN(cyear));
}

//Set wait status row
function setWaitStatusRow(row, span) {
    var row_width = jQuery(row).parent('table').outerWidth();
    jQuery(row).empty().html("<td class = 'checkb'>&nbsp;</td><td colspan = '" + span + "' class = 'waittd' style = 'width: " + row_width + "px'>&nbsp;</td><td class = 'titleedit'>&nbsp;</td>");
}

function showMenu() {
    if (jQuery('#navigation').length && jQuery('#main').length) {
        jQuery('#main').css('display', 'block');
        jQuery('#loading').remove();
    }
}

//Edit row buttons

function editButtonsRow(row, span) {
    //bottom row and buttons
    var brw = jQuery('<tr/>', {'id': 'b_edit'});
    if (row.next('tr').attr('id') === "base-tr") {
        brw.attr('class', 'nbbtr');
    }

    var brtdl = jQuery('<td/>', {'class': 'editbtsl'});
    var brtd = jQuery('<td/>', {'id': 'b_edit_td', 'class': 'editbts', 'colspan': span});
    var brtdr = jQuery('<td/>', {'class': 'editbtsr'});

    brw.append(brtdl);
    brw.append(brtd);
    brw.append(brtdr);

    brtd.empty().html("&nbsp;<button type='button' title='Cancel Record Editing' id='edit-cancel' class='btn btn-danger btn-sm'>cancel</button>" +
            "<button type='button' title='Update Record' id='edit-update' class='btn btn-success btn-sm'>update</button>");

    row.after(brw);
}

/**
 * add buttons row
 */
function addButtonsRow(submitButtonID) {

    //Default parameter = 'add'
    submitButtonID = submitButtonID || "add";

    // add row buttons
    var button1 = jQuery('<button/>', {'id': 'add-cancel', 'text': 'cancel', 'type': 'button', 'title': 'Cancel new record', class: 'btn btn-danger btn-sm'});
    var button2 = jQuery('<button/>', {'id': 'add-save', 'text': 'save', 'type': 'button', 'title': 'Save new record', class: 'btn btn-success btn-sm'});

    jQuery('#' + submitButtonID).before(button1).before(button2);

    // Add grey border used for record highlighter
    jQuery('tr#base-tr td').addClass('grey-border-top');

}

/**
 * Remove new record buttons/controls
 */
function removeNewRecordControls() {

    if (jQuery('#add').length) {
       jQuery('#add').prop('disabled', false);
    }

    if (jQuery('#add-cancel').length) {
       jQuery('#add-cancel').remove();
    }

    if (jQuery('#add-save').length) {
       jQuery('#add-save').remove();
    }

    // Add grey border used for record highlighter
    jQuery('tr#base-tr td').removeClass('grey-border-top');

}

// Fail edit
function failEdit(message) {

    jQuery('#edit-update').css('display', '');
    jQuery('#edit-cancel').css('display', '');
    jQuery('#b_edit_td').attr('class', 'editbts');

    // Delay to allow all before to complete before displaying alert message
    setTimeout(function() {

        if (message !== undefined && message.length > 0) {

            alert(message);

        }

    }, 100);

}

// Fail add
function failAdd(message) {

    jQuery('#base-button-td').attr('class', 'base-button-options grey-border-top');
    jQuery('#add-save').css('display', '');
    jQuery('#add-cancel').css('display', '');

    // Delay to allow all before to complete before displaying alert message
    setTimeout(function() {

        if (message !== undefined && message.length > 0) {

            alert(message);

        }

    }, 100);

}

//Add checkbox events to list
function addCheckboxEvents(page) {

    if (jQuery('#check-all').length) {
        jQuery('#check-all').on('click', function() {
            disableGoButton(page, !(selectRow(-1, page)));
        });
    }

    var cbs = jQuery('#' + page + '_table').find('input[id^=' + page + '-check]');
    jQuery.each(cbs, function(index, cb) {
        addCheckboxEvent(cb, page);
    });
}

//Add checkbox event
function addCheckboxEvent(cb, page) {
    jQuery(cb).on('click', function(e) {
        var id = jQuery(this).attr('id').split('-');
        disableGoButton(page, !selectRow(id[2], page));
    });
}

// waiting on
function simpleWaitOn(rep, type) {
    var imgsrc;
    var wid = (rep.width() - 16) / 2;

    if (type === 'type1') {
        imgsrc = "assets/images/wait.gif";
    } else if (type === 'type2') {
        imgsrc = "assets/images/waitsmall.gif";
    }

    var wait = jQuery('<img/>', {id: 'litwait', src: imgsrc, alt: 'Please Wait...'}).css({margin: "0px " + wid + "px 0px " + wid + "px"});
    rep.after(wait);
    rep.css('display', 'none');
}

// waiting off
function simpleWaitOff(rep) {
    if (jQuery('#litwait').length) {
        jQuery('#litwait').remove();
        rep.css('display', '');
    }
}

// is email address valid
function isEmailValid(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return filter.test(email);
}

/**
 * Is record ID valid
 *
 * @param string id   id of record
 * @param string suppressError  suppress error message
 *
 * @returns boolean   record ID is valid yes|no
 */
function isRecordIDValid(id, suppressError) {
    var regEx = /^([\.\-\_0-9a-z]+)$/i;
    if (!regEx.test(id)) {

      if (!suppressError) {
      var message = "The record identifier(s) that you entered can only contain the following:\n\n"
                  + " - alpha numeric characters (0-9 and/or a-z)\n"
                  + " - special characters dot(.) dash(-) or underscore(_)";
          alert(message);
      }

      return false;
    }
    return true;
}

// Percent calculation
function getPercent(val1, val2) {
    if (val1 === 0 || val2 === 0) {
        return "0%";
    }
    return (Math.round((val1 / val2) * (100)) + "%");
}

function refreshPage(ie_scroll_y) {
    if (detectIE()) {
        if (ie_scroll_y) {
            prepareScroll(getScrollPosition());
        }
        history.go(0);
    } else {
        window.location.reload();
    }
}

//Scrolling Functions [Added 05/05/2012]
function performScroll() {
    if (window.name === 'scrolltobase') { //scroll to base
        window.name = '';
        jQuery('html, body').scrollTop( jQuery(document).height());
    } else if (window.name !== "" && !isNaN(window.name)) { //specific scroll position
        jQuery('html, body').scrollTop( Math.max(window.name, jQuery(document).height()) );
        window.name = '';
    }
}

//Prepare scroll position [Added 05/05/2012]
function prepareScroll(action) {
    window.name = action;
}

//Get current scroll position [Added 05/05/2012]
function getScrollPosition() {
    if (typeof pageYOffset !== 'undefined') { //most browsers
        return pageYOffset;
    } else {
        var b = document.body; //IE 'quirks'
        var d = document.documentElement; //IE with doctype
        d = (d.clientHeight) ? d : b;
        return d.scrollTop;
    }
}

// Really just for IE8 or earlier. Make String.trim() available.
if (!String.prototype.trim){
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g,'');
    };
}

/**
 *  Handle generic http status codes
 *
 * @param int status            status code number
 * @param string message        default message
 * @returns string message
 */
function httpStatusCodeHandler(status, message) {
   switch (status) {
       case 404:
       case 500:
           message += "An internal error occurred";
           break;
       case 400:
           message += "Request was invalid.";
           break;
       case 403:
           message += "Sorry, You may not be logged in or your session may have expired";
           break;
       default:
           message += "An unspecified error has occurred.<br/>Contact your system administrator.";
           message += "\n(HTTP Code: " + status + ")";
   }
   return message;
}

/**
 * Request Queue
 */
var AjaxQueue = (function(){
    var ajaxReqs = 0;
    var ajaxQueue = [];
    var ajaxActive = 0;
    var ajaxMaxConc = 4;
    function ajaxQue (obj) {
        ajaxReqs++;
        var oldSuccess = obj.success;
        var oldError = obj.error;
        var callback = function() {
            ajaxReqs--;
            if (ajaxActive === ajaxMaxConc) {
                return jQuery.ajax(ajaxQueue.shift());
            } else {
                ajaxActive--;
            }
        }
        obj.success = function(resp, xhr, status) {
            callback();
            if (oldSuccess) oldSuccess(resp, xhr, status);
        };
        obj.error = function(xhr, status, error) {
            callback();
            if (oldError) oldError(xhr, status, error);
        };
        if (ajaxActive === ajaxMaxConc) {
            ajaxQueue.push(obj);
        } else {
            ajaxActive++;
            return jQuery.ajax(obj);
        }
    }

    return ajaxQue;
})()

function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
	  "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
	setCookie(name, "", {
	  expires: -1
	})
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
  }

/**
 * Preloader
 * @param {jQuery selector} $el
 * @param {object} options
 */
function customSpinner($el, options) {
    var defaultOptions = {
        message: 'Exporting Data Please Wait....',
        class: 'spin-style',
        position: {
            paddingTop: 4,
            paddingLeft: 0
        }
    }
    this.options = jQuery.extend({}, defaultOptions, options);

    var spinner = jQuery('<div>', {
        class: this.options.class,
        text: this.options.message
    }).css(this.options.position);

    $el.find('.'+this.options.class).remove();
    $el.prepend(spinner);

    this.destroy = function (){
        spinner.remove();
    }
}

/**
 * Query string to object
 */
function parseQueryString(string) {
    if (string == "") {
      return {};
    }
    var _data = {};
    var decodeString = decodeURI(string.split('/').reverse()[0].split('?').reverse()[0]);
    var search = decodeString.replace( '?', '' ).split( '&' );
    var VRegExp = new RegExp(/(([A-Za-z0-9])+)+/g);
    jQuery.each( search, function( index, part ){
      if( part !== '' ){
        part = part.split( '=' );
        if (part[0].indexOf('[]') > -1) {
          var VResult = part[0].match(VRegExp);
          var _key = VResult[0];
          if (!_data[ _key ]) {
            _data[ _key ] = [];
          }
          _data[ _key ].push(part[ 1 ]);
        }else{
          if (part[0].indexOf('[') > -1) {
            var _key = part[0];
            var VResult = part[0].match(VRegExp);
            if (!_data[ VResult[0] ]) {
              _data[ VResult[0] ] = [];
            }
            if (!_data[ VResult[0] ][VResult[1]]) {
              _data[ VResult[0] ][VResult[1]] = [];
            }
            _data[ VResult[0] ][VResult[1]].push(part[ 1 ]);
          }else{
            _data[ part[ 0 ] ] = part[ 1 ];
          }
        }

      }
    });
    return _data;
  }

  function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}