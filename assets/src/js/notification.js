$(document).ready(function(){
    //Set Interval to check if there is any notifications for this user on the application
    checkForNotifications();
    setInterval(function(){
        checkForNotifications();
        loadNotifications();
    },3000);
    
    $('#notificationButton').on("show.bs.dropdown", function(event){
        $('#bell').text('');
    })
    //This function loads all the notifications that are stored on the localstroge and puts them on the notification tab/dropdown
    loadNotifications = function(){
        //Checks if there is any notifications in the local storage
        if(JSON.parse(this.window.localStorage.getItem("Not"))===null){
            $('#dropdown').html('<li><a>No Notifications</a></li>');
        }else{
            var data = JSON.parse(this.window.localStorage.getItem("Not"));
            $('#dropdown').html('');
            //Iterates through the client notifications and displays them on the drop down
            $.each(data.client, function(clientIndex, notification){
                if(notification.new==true){
                    $('#bell').text('.');
                    notification.new = false;
                }
                if(notification.closed == false)
                {
                    $('#dropdown').append('<tr id="client'+clientIndex+'">'+
                    '<td><a href="'+notification.link+'" target="_blank"><strong>'+notification.title+'</strong><br/><small><em>'+notification.notification_message+'</em></small></a></td>'+
                    '<td><a class="fas fa-times" id="'+clientIndex+'" onclick="closeClientNotification(this)" style="padding-left: 10px; text-align: right;" ><small></small></a></td>'+
                    '</tr>');
                }
            });
            //Iterates through the user notifications and displays them on the drop down
            $.each(data.user, function(userIndex, notification){
                if(notification.new==true){
                    $('#bell').text('.');
                    notification.new = false;
                }
                if(notification.closed == false)
                {
                    $('#dropdown').append('<tr id="user'+userIndex+'">'+
                    '<td><a href="'+notification.link+'" target="_blank"><strong>'+notification.title+'</strong><br/><small><em>'+notification.notification_message+'</em></small></a></td>'+
                    '<td><a class="fas fa-times" id="'+userIndex+'" onclick="closeUserNotification(this)" style="padding-left: 10px; text-align: right;"><small></small></a></td>'+
                    '</tr>');
                }
            });
            this.window.localStorage.setItem("Not", JSON.stringify(data));
        }
    };

    /** 
     * Sends a message to the notification system that checks if there is any notifications. 
     * Also, works as a regestration function if the client or product are not already regestered in the database
     */
    function checkForNotifications()
    {   
        var client = {
            clientCode: $('#clientCode').text(),
            clientName: $('#clientName').text(),
            app: $('#mode').text(),
            user: $('#user').text(),
            url: window.location.href,
            check: "123"
        };
        if(client.user!="")
        {
            var frame = document.getElementById('notificationFrame');
            frame.contentWindow.postMessage(JSON.stringify(client),'*');
        }
    };
    /**
     * When notifcation system sends the notifications they are stored on the local host to be loaded when they're needed
     */
    window.addEventListener('message',function(event){
        if(~event.origin.indexOf('http://localhost:8083'))
        {
            if(JSON.parse(event.data).client!=0 || JSON.parse(event.data).user!=0){
                //If the local storage is empty then add all notifications else check for what notifications are the new ones.
                if(JSON.parse(this.window.localStorage.getItem("Not"))===null){
                    this.console.log(JSON.parse(event.data));
                    var data = JSON.parse(event.data);
                    this.window.localStorage.setItem("Not", JSON.stringify(data));
                }else{
                    //Comparing the notifications from the local storage to the ones the notification system sent.
                    var check = JSON.parse(this.window.localStorage.getItem("Not"));
                    var data = JSON.parse(event.data);
                    var currentDate = new Date();
                    if(check.client!=null){
                        for(var i = 0; i<data.client.length; i++){
                            for(var z = 0; z<check.client.length;z++){
                                var exists = false;
                                if(check.client[z].expiry_date<currentDate){
                                    check.client.splice(i,1);
                                }
                                if(data.client[i].notification_id == check.client[z].notification_id){
                                    exists = true;
                                    break;
                                }
                                if(z == (check.client.length-1) && exists == false){
                                    check.client.push(data.client[i]);
                                }
                            }
                        }
                    }else{
                        check.client.push(data.client[0]);
                    }
                    if(check.user!=null){
                        for(var i = 0; i<data.user.length; i++){
                            for(var z = 0; z<check.user.length;z++){
                                var exists = false;
                                if(check.user[z].expiry_date<currentDate){
                                    check.user.splice(i,1);
                                }
                                if(data.user[i].user_notification_id == check.user[z].user_notification_id){
                                    exists = true;
                                    break;
                                }
                                if(z == (check.user.length-1) && exists == false){
                                    check.user.push(data.user[i]);
                                }
                            }
                        }
                    }else{
                        check.user.push(data.user[0]);
                    }
                    this.window.localStorage.clear();
                    this.window.localStorage.setItem("Not", JSON.stringify(check));
                }
            }else{
                this.window.localStorage.clear();
            }
        }
    });
});
//Closes a client notification
function closeClientNotification(row){
    var data = JSON.parse(this.window.localStorage.getItem("Not"));
    $('#client'+$(row).attr('id')).remove();
    $.each(data.client, function(clientIndex, notification){
        if(clientIndex == $(row).attr('id')){
            notification.closed = true;
        }
    });
    this.window.localStorage.clear();
    this.window.localStorage.setItem("Not", JSON.stringify(data));
}

//Closes a user notification
function closeUserNotification(row){
    var data = JSON.parse(this.window.localStorage.getItem("Not"));
    $('#user'+$(row).attr('id')).remove();
    $.each(data.user, function(clientIndex, notification){
        if(clientIndex == $(row).attr('id')){
            notification.closed = true;
        }
    });
    this.window.localStorage.clear();
    this.window.localStorage.setItem("Not", JSON.stringify(data));
}