/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

$(document).ready(function() {
    
    // Menu item link :: Manage Section
    includeManageEvent();
    includeSupportOption();
   
    // Display manual config options
    $('.manual-config').click(function () {
        $('#config-card').css('display', '');
        $('.assigned-list').hide();
        $(this).hide();

        // Expand all dropdowns again on config page
        $.each($('select'), function () {
            $(this).selectmenu({ width : $(this).width()})
        });

    });

    // Make the sessions dropdown nice and shiny
    $.widget("custom.sessionSelectMenu", $.ui.selectmenu, {
        
      _renderItem: function(ul, item) {
        var li = $( "<li>" ),
          wrapper = $( "<div>", { text: item.label } );

        // Circuit indicator is set ?
        var circuitIndicatorColour = item.element.attr('circuit-indicator');
 
        if (item.disabled) {
          li.addClass('ui-state-disabled');
        }
 
        // Add circuit colour if it exists
        if (typeof circuitIndicatorColour !== 'undefined') {
            
            // Set the title for the dropdown item
            var itemTitle = item.element.attr('title');
            li.attr('title', itemTitle).addClass('dropdown-item');
 
            // Add the div that will contain the colour
            $('<div>', {
              'class': 'circuit-indicator',
              'style': 'background-color: #' + circuitIndicatorColour,
              'title': itemTitle
            }).appendTo(wrapper);    
              
        }
        
        return li.append(wrapper).appendTo(ul);
      }
      
    });

    // Term selection
    $('#term-id').selectmenu({
        width: null,
        change: function() {
            generateDepartments(this.value);
        }});

    // Department selection
    $('#dept_id').selectmenu({ 
           width: null,
           change: function() {
           generateExams(this.value);
      }});

    // Exam selection
     $('#exam_id').selectmenu({ 
          width: null,
           change: function() {
           generateSessions(this.value);
      }});
  
    // Session selection
    $('#session_id').sessionSelectMenu({
          width: null,
          change: function() {
             generateStationsGroups(this.value);
      }});
  
    // Station selection
    $('#station_id').selectmenu({ 
        width: null, 
        change: function() {
           $(".titleerror").html('&nbsp;');
    }});

    // Group selection
    $('#group_id').selectmenu({ 
        width: null, 
        change: function() {
           $(".titleerror").html('&nbsp;');
    }});

    // Scale accessibility selection
    $('#scale-accessibility').selectmenu({
        width: null,  
        change: function() {
        $('#assessment-body').parents('html').css('font-size', $('#scale-accessibility').val() + 'px');
        $.cookie("assess-scale-accessibility", $('#scale-accessibility').val());
    }});

    // Form Event
    $("#stationselect").submit(function() {


        if ($("#term-id").val().length === 0 || $("#term-id").val() === "--") {
            $("#select_term_err").html("&#xab; Select");
            return false;
        }

        if ($("#dept_id").val().length === 0 || $("#dept_id").val() === "--") {
            $("#select_dept_err").html("&#xab; Select");
            return false;
        }

        if ($("#exam_id").val().length === 0 || $("#exam_id").val() === "--") {
            $("#select_exam_err").html("&#xab; Select");
            return false;
        } else if ($("#session_id").val().length === 0 || $("#session_id").val() === "--") {
            $("#select_session_err").html("&#xab; Select");
            return false;
        } else if ($("#station_id").length === 0 || $("#station_id").val() === "--" || $("#station_id").val() === "nostations") {
            $("#select_stations_err").html("&#xab; Select");
            return false;
        } else if ($("#group_id").length === 0 || $("#group_id").val() === "--" || $("#group_id").val() === "nogrps") {
            $("#select_groups_err").html("&#xab; Select");
            return false;
        } else {
            return true;
        }
        

    });

    //Generate Departments Drop Down
    function generateDepartments(term) {

        $(".titleerror").html('&nbsp;');
        $("#stationselect").find("div").html('&nbsp;');

        if (term.length < 1 || term === "--") {
            return;
        }

        var deptDiv = $("#dept-div");
        deptDiv.addClass("wait");

        $.ajax({
            type: "POST",
            url: "assess/ajx_depts.php?",
            data: {
                term: term,
                sid: Math.random()
            }
        }).done(function(html) {

            deptDiv.removeClass('wait').html(html);

            $('#dept_id')
                .selectmenu({ change: function() {
                        generateExams(this.value);
                    }});

        }).fail(function() {

            alert('The request failed, please make selection again');

        });

    }

    //Generate Exams Drop Down  
    function generateExams(dept) {
        $(".titleerror").html('&nbsp;');
        $("#stationselect").find("div:not(#dept-div)").html('&nbsp;');

        if (dept.length < 1 || dept === "--") {
            return;
        }

        var examdiv = $("#examdiv");
        examdiv.addClass("wait");

        var url = "assess/ajx_assessment.php?";
        var random = Math.random();

        $.ajax({
            type: "POST",
            url: url,
            data: {dept_id: dept, sid: random}
        }).done(function(html) {
            examdiv.removeClass('wait');
            examdiv.html(html);

        $('#exam_id')
         .selectmenu({ change: function() {
              generateSessions(this.value);
         }});
        }).fail(function() {
            alert('The request failed, please make selection again');
        });
    }

    //Generate Sessions
    function generateSessions(exam) {
        var tddiv = $("#tddiv");
        $(".titleerror").html('&nbsp;');
        tddiv.html('&nbsp;');
        $("#groups-div").html("&nbsp;");
        $("#stations-div").html("&nbsp;");

        if (exam.length < 1 || exam === "--") {
            return;
        }

        tddiv.addClass('wait');

        var url = "assess/ajx_sessions.php?";
        var random = Math.random();

        $.ajax({
            type: "POST",
            url: url,
            data: {exam_id: exam, sid: random}
        }).done(function(html) {
            tddiv.removeClass('wait');
            tddiv.html(html);
      
            // Session selection
            $('#session_id')
                   .sessionSelectMenu({ change: function() {
                   generateStationsGroups(this.value);
              }}); 
            
        }).fail(function() {
            alert('The request failed, please make selection again');
        });
    }

//Generate Selections, Groups and Stations
    function generateStationsGroups(session_id) {
        $(".titleerror").html("&nbsp;");
        $("#stations-div").html("&nbsp;");
        $("#groups-div").html("&nbsp;");

        if (session_id.length < 1 || session_id === "--") {
            return;
        }

        $("#stations-div").addClass('wait');
        $("#groups-div").addClass('wait');

        var url = "assess/ajx_selections.php?";
        var random = Math.random();

        $.ajax({
            type: "POST",
            url: url,
            data: {
              ajax_call: true,
              session_id: session_id,
              sid: random
            }
        }).done(function(html) {
            
            // Remove wait
            $("#stations-div").removeClass('wait');
            $("#groups-div").removeClass('wait');
            
            // Some variables
            var splitContent = html.split("[!*$*!]");

            // Fill with select boxes
            $("#stations-div").html(splitContent[0]);
            $("#groups-div").html(splitContent[1]);
            
            // Add jquery UI select menu look and feel to stations and groups dropdown
            $("#station_id").selectmenu({ change: function() {
                $(".titleerror").html('&nbsp;');
            }});
        
            $("#group_id").selectmenu({ change: function() {
                $(".titleerror").html('&nbsp;');
            }});
            
        }).fail(function() {
            alert('The request failed, please make selection again');
        });
    }

    // Make that preset options are of equal width
    var maxPresetWidth = 100; 
    $('.assigned-station').each(function() {
       maxPresetWidth = ($(this).width() > maxPresetWidth) ? $(this).width() : maxPresetWidth; 
    });
    $('.assigned-station').width(maxPresetWidth + 50);
    

});
