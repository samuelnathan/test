/* This file exists purely to make sure that when jQuery loads, that it doesn't
 * cause chaos with MooTools until MooTools is completely phased out.
 * @author Domhnall Walsh <domhnall.walsh@qpercom.ie>
 * I've put this in a file by itself that I can force header_html.php to load
 * before MooTools so that it doesn't have a seizure.
 * See http://davidwalsh.name/jquery-mootools for more.
 */
jQuery.noConflict();
