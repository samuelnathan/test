/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2017 Qpercom Limited.  All rights reserved.
 */
jQuery(document).ready(function() {

    /* Home page links */
    jQuery('#manual').on('click', 'a', function() {
       
        var url = jQuery('#' + jQuery(this).attr('id') + '_hidden').val();
        winpop = open(url, 'displayWindow', 'width=800, height=600, left=0, top=0, status=0, ' + 
                'toolbar=0, menubar=0, dependent=1, resizable=1, scrollbars=1');
        winpop.focus();
        
    });
   
    /* Login fields highlighting */
    if (jQuery('#id').length && jQuery('#id').attr('type') === 'text'
            && jQuery('#password').length && jQuery('#password').attr('type') === 'password') {
        
        jQuery('#id, #password').on('focus', function (){
            jQuery(this).addClass('login-hl');
        });
        jQuery('#id, #password').on('blur', function (){
            jQuery(this).removeClass('login-hl');
        });

        if (jQuery('#id').val().length === 0) {
            jQuery('#id').focus();
        } else {
            jQuery('#password').focus();
        }
        
    }
    
    
    /* Login validation jQuery
     * Keeping it simple: using a validation plugin here would be overkill
     */
    if (jQuery('#loginform').length && jQuery('#id').length && jQuery('#password').length) {
        
        // Bind submit event to login form
        jQuery('#loginform').submit(function(event) {
          
          // Trim field values 
          var idValue = jQuery('#id').val().trim();
          var passwordValue = jQuery('#password').val();
        
          // Set ID (username) field with trimmed value
          jQuery('#id').val(idValue);
         
          // If fields are not complete then abort submit and inform user
          if (idValue.length === 0 || passwordValue.length === 0) {
             
             // Message to user
             var message = "Please complete all login fields";
              
             // If the user info element already exists then set the message  
             if (jQuery('.loginfo').length) {
                 jQuery('.loginfo').text(message);
              // Or else create a new element
             } else {
                 jQuery('#baselogintd').prepend('<div class="form-group text-right"><p class="loginfo">' + message + '</p></div>'); 
             }
             event.preventDefault();
              
          }
       
       });
        
    }
   
    /* Request password reset */
    if (jQuery('#requestform').length) {
        jQuery('#request-button').on('click', function() {
            if (jQuery('#identifier').val().length === 0) {
                jQuery('#identifier-error').html("fill in").addClass('alert-danger alert mt-2');
            } else if (jQuery('#email').val().length === 0) {
                jQuery('#email-error').html("fill in").addClass('alert-danger alert mt-2');
            } else if (!isEmailValid(jQuery('#email').val())) {
                jQuery('#email-error').html("&#xab; invalid email address");
            } else {
                jQuery('.infotd').html("").removeClass('alert-danger alert mt-2');
                
                jQuery('#requestform').submit();
                jQuery('#identifier').prop('disabled', true);
                jQuery('#email').prop('disabled', true);
                jQuery('#request-button').css('color', '#ff0000').val('Please wait..');
            }
        });

        jQuery('#email').on('keyup', function() {
            jQuery('#email-error').html("");
        });

        jQuery('#identifier').on('keyup', function() {
            jQuery('#identifier-error').html("");
        });
    }
    
    showMenu();
    
    // Adjust twitter feed font once page is loaded
    window.setTimeout(function() {
      jQuery('#twitter-timeline-loading').hide();
    }, 300);
    
    window.setTimeout(function() {
      jQuery('.twitter-timeline').contents().find('.timeline-Tweet-text').css('font-size','13px');
      jQuery('.twitter-timeline').contents().find('.TweetAuthor-name').css('font-size','13px');
      jQuery('.twitter-timeline').show();       
    }, 1100);

});