/**
 * Script to handle MooTools and such functions on the role editor page(s).
 * @author Domhnall Walsh
 */

/*JSLint processor commands */
/*global jQuery */

/**
 * Function to store a definition of a field.
 *
 * @param {String} name      Name of field
 * @param {String} title     Title of column containing this field
 * @param {String} css_class CSS Class
 * @returns {FieldSpec}
 */
var FieldSpec = function (name, title, css_class) {
    this.name = name;
    this.title = title;
    this.css_class = css_class;
};

// Define
var cell_fields = [];
cell_fields.push(new FieldSpec('role_id', 'Role ID', ''));
cell_fields.push(new FieldSpec('role_name', 'Name', 'tf3'));
cell_fields.push(new FieldSpec('role_shortname', 'Short Name', 'tf3'));
cell_fields.push(new FieldSpec('role_description', 'Description', 'tf6'));
cell_fields.push(new FieldSpec('role_importexport_field', 'Import/Export Field Value', 'tf5'));

/* Function prototype to help simplify
 */
var TextField = function (field, value, hidden, prefix) {
    this.fieldspec = typeof field !== 'undefined' ? field : '';
    this.value = typeof value !== 'undefined' ? value : '';
    this.hidden = typeof hidden !== 'undefined' ? hidden : false;
    this.prefix = typeof prefix !== 'undefined' ? prefix : 'editrow';
    this.id = prefix + "." + this.fieldspec.name;
    this.name = this.id;
    
    this.toHTML = function () {
        // Generate a control based on the textfield's format.
        var identifier = this.prefix + "." + this.fieldspec.name;
        var type = (this.hidden ? 'hidden' : 'text');

        return '<input type="' + type + '" name="' + identifier + '" id="' + identifier + '" value="' + this.value + '" class="' + this.fieldspec.css_class + '" previous_value="' + this.value + '"/>';
    };
};

/**
 * Function to check/uncheck all roles. Each individual role's checkbox should
 * have an ID starting with "role_id"...
 *
 * @returns {undefined}
 */
var handleClickSelectAll = function () {
    var newStatus = (jQuery('#check-all').prop('checked'));

    jQuery('input[name^="role_id"]').each(function(i, checkbox) {
        jQuery(checkbox).prop('checked', newStatus);
    });
};

/**
 * Function to set the order of the view (which field, which direction, etc.)
 *
 * @param {String} field     Name of data field to sort by
 * @param {String} direction Sort direction ('asc' or 'desc')
 * @returns {undefined}
 */
function changeView(field, direction) {
    var url_parts = location.href.split('?');
    var page_url = url_parts[0];
    var params = parseQueryString(location.href);

    params.order_by = field;
    params.direction = direction;

    var param_strings = [];
    var keys = Object.keys(params);

    jQuery.each(keys, function (i, key) {
        param_strings.push(encodeURI(key) + "=" + encodeURI(params[key]));
    });

    location.href = page_url + "?" + param_strings.join('&');
}

/**
 * Function to enable/disable the "todo" select box and associated "go" button
 * depending on whether any checkboxes have been selected.
 * None selected = disabled
 * 1+ selected = enabled.
 *
 * @returns {undefined}
 */
var handleEnableTaskSelect = function () {
    var checked_boxes = jQuery('input[id^="role_id"]:checked');
    var todo = jQuery('#todo');
    var go_button = jQuery('#role_task');

    todo.prop('disabled', (checked_boxes.length === 0));
    go_button.prop('disabled', (checked_boxes.length === 0));
};

/**
 * Remove any existing table rows that contain edit controls.
 *
 * @param {HTMLTableElement} table
 *
 * @returns {int} number of rows deleted.
 */
function removeEditControls(table) {
    var edit_rows = jQuery(table).find('tr[id=edit_controls]');

    jQuery.each(edit_rows, function (i, row) {
        row.remove();
    });

    return edit_rows.length;
}

/**
 *
 * Injects a row into the table containing edit controls.
 *
 * @param {HTMLTableElement} table
 * @param {HTMLTableRowElement} afterRow
 * @returns {undefined}
 */
function injectEditControls(table, afterRow) {
    // In case the edit controls have been loaded for another row, drop 'em.
    removeEditControls(table);
    
    var role_id = jQuery(afterRow).attr('id').match(/[0-9]+?/g);
    
    // Get the index of the row we're interested in
    var row_insert_position = jQuery(table).find('#' + jQuery(afterRow).attr('id')).index();
    
    // Create the row.
    var row = jQuery('<tr>', {
        "id": "edit_controls"
    });
    
    // Create a single cell in that new row that spans the entire row.
    var cell = jQuery('<td>', {
        colSpan: 6,
        align: 'right'
    });
    
    // Build and inject the HTML that defines that row into it.
    var cell_html = '<button class="btn-danger btn-sm" onclick="editCancel(' + role_id + ');" type="button" title="Cancel Edit">Cancel</button>';
    cell_html += '&nbsp;';
    cell_html += '<button class="btn-success btn-sm" onclick="editCommit(' + role_id + ');" title="Save &amp Close">Save</button>';
    cell.html(cell_html);

    row.append(cell);

    jQuery(table).find('tr').eq(row_insert_position).after(row)
}

/**
 * Event Handler to display an editable row of controls for the selected row
 * when the edit button is clicked...
 * It also needs to hide the edit controls and the checkboxes.
 *
 * @returns {undefined}
 */
var editBegin = function () {
    var table = jQuery(this).parents('table:first');
    var row = jQuery(this).parents('tr:first');
    var role_id = row.find('input[type=checkbox]')[0].id.match(/[0-9]+?/g);
    var cell_index = 0;
    var cell_html = "";

    // Add a row to the table with edit/cancel controls.
    injectEditControls(table, row);

    // Add the red border around the row being edited
    row.attr('class', 'editrw');

    enableCheckboxes(table, false);
    enableTableBottomControls(false);
    enableEditButtons(table, false);
    enableSortableFieldHeaders(false);

    // Get all cells except the first (checkbox) and last (edit button).
    var cells = row.find('td').not(':first').not(':last');

    jQuery.each (cells, function (i, cell) {
        // Handle special case...
        if (cell_index === 0) {
            cell_html = (new TextField(cell_fields[0], role_id, true)).toHTML();
            cell_index++;
        } else {
            cell_html = "";
        }

        jQuery(cell).get(0).innerHTML = injectEditInput(cell, cell_fields, cell_index, cell_html);
        cell_index++;
    });

    /* Give the first input box focus so that the user can start typing straight
     * away. (Index 0 is the ID checkbox, so we need to jump to 1)
     */
    var first_input = jQuery('input[name="editrow.' + cell_fields[1].name + '"]').eq(0);
    first_input.focus();
    //first_input.setCaretPosition("end");
};

/**
 *
 * @param {type} cell
 * @param {type} prefix
 * @returns {@exp;cell@call;getElements}
 */
function getEditInputs(cell, prefix) {
    prefix = typeof prefix !== 'undefined' ? prefix : "editrow";
    return jQuery(cell).find('input[id^="' + prefix + '."]');
}

/**
 * Enable or disable the checkboxes hierarchically below the specified root
 * element.
 *
 * @param {HTMLElement} root_element
 * @param {Boolean} enabled
 * @returns {undefined}
 */
function enableCheckboxes(root_element, enabled) {
    var enabled = typeof enabled !== 'undefined' ? enabled : true;

    var checkboxes = jQuery(root_element).find('input[type=checkbox]');
    jQuery.each(checkboxes, function(i, cb) {
        jQuery(cb).prop('disabled', !enabled);
    });
}

function enableEditButtons(root_element, enabled) {
    var edit_buttons = jQuery(root_element).find('a[id^=edit_]');
    var image;

    jQuery.each (edit_buttons, function (i, btn) {
        image = jQuery(btn).find('img').get(0);
        if (enabled) {
            jQuery(btn).on('click', editBegin);
            image.src="assets/images/edit.svg";
        } else {
            jQuery(btn).off('click', editBegin);
            image.src="assets/images/edit_disabled.svg";
        }
    });
}

function enableSortableFieldHeaders(enabled) {
    var order_by;
    var direction;

    if (enabled) {
        var query = parseQueryString(location.href);
        order_by = (typeof query.order_by !== 'undefined') ? query.order_by : 'role_id';
        direction = (typeof query.direction !== 'undefined') ? query.direction : 'asc';
    }

    // Iterate through the columns.
    var column_id = 1;
    var field;
    jQuery('#role_list_table').find('th').each(function (i,th) {
        field = cell_fields[column_id];
        if (enabled) {
            if (field.name === order_by) {
                field_order = ((direction === 'asc') ? 'desc' : 'asc');
            } else {
                field_order = 'asc';
            }
            jQuery(th).get(0).innerHTML = '<a href="javascript:changeView(\'' + field.name
                + '\', \'' + field_order + '\');">' + field.title + '</a>';
        } else {
            jQuery(th).get(0).innerHTML = cell_fields[column_id].title;
        }

        column_id++;
    });
}

function enableTableBottomControls(enabled) {
    jQuery('tr[id=base-tr]').css('display', (enabled ? '' : 'none'));
}

/*
 * Replace the HTML in a table cell with one or more HTML inputs as specified
 * in <fieldspecs>
 *
 * @param {HTMLTableCell}     cell        Cell to replace with input controls.
 * @param {Array.<FieldSpec>} fieldspecs  Definitions of the fields being used
 * @param {int}               field_index Index of the cell within the array.
 * @param {String}            extra_html  Additional HTML to inject
 *
 * @returns {int} Updated field index
 */
function injectEditInput(cell, fieldspecs, field_index, extra_html) {
    var html = typeof extra_html !== 'undefined' ? extra_html : "";
    html += (new TextField(fieldspecs[field_index], jQuery(cell).html())).toHTML();
    return html;
}

function removeEditInputs(cell, fieldspecs, preserve_changes) {
    // Get the inputs in this cell
    var edit_inputs = getEditInputs(cell);
    var fieldspec = null;

    // Default to not preserving the input.
    preserve_changes = typeof preserve_changes !== 'undefined' ? preserve_changes : false;

    jQuery.each(edit_inputs, function(index, input) {
        // Strip the "editrow." prefix from the input's id
        input_id = jQuery(input).attr('id').split(".").slice(1).join(".");

        jQuery.each(fieldspecs, function(i, cf) {
            if (cf.name === input_id) {
                fieldspec = cf;
                return true;
            }
        });

        /* Ignore this input if there's no matching field spec OR if there's no
         * css class specified for the field spec (which is intended to suggest
         * that it's not meant to be visible under non-editing circumstances.
         * Yes it's a hack, but...
         */
        if ((fieldspec === null) || (fieldspec.css_class === "")) {
            return true;
        }

        // If we get to here then the current field is the one we want.
        jQuery(cell).get(0).innerHTML = ((preserve_changes) ? input.value : input.getAttribute("previous_value"));
    });
}

/**
 * Cancel editing a selected role.
 *
 * @param {int} role_id The ID number of the role being edited.
 * @returns {undefined}
 */
function editCancel(role_id) {
    var table = jQuery('#role_list_form').find('table');
    var edit_row = jQuery('tr[id=role_row_' + role_id + ']').eq(0);

    removeEditControls(table);

    enableCheckboxes(table, true);
    enableTableBottomControls(true);
    enableEditButtons(table, true);
    enableSortableFieldHeaders(true);

    // Remove the red border around the row being edited
    if (role_id > 0) {
        // Then it's an existing row, we need to restore it.

        // Get all of the input cells except the first and last...
        var input_cells = edit_row.find('td');
        input_cells = input_cells.not(':first').not(':last');

        // Iterate through the rest...
        jQuery.each(input_cells, function(i, cell) {
            removeEditInputs(cell, cell_fields, false);
        });

        edit_row.get(0).className = "datarow";
    } else {
        edit_row.remove();
    }
}

/**
 * Save changed role info.
 *
 * @param {int} role_id The ID number of the role we're updating...
 * @returns {undefined}
 */
function editCommit(role_id) {
    var table = jQuery('#role_list_form').find('table');
    var edit_row = jQuery('tr[id=role_row_' + role_id + ']').eq(0);
    var form_data = {};

    var adding_record = (role_id === 0);

    // State that this is an update rather than an add.
    form_data['action'] = ((role_id === 0) ? 'add' : 'update');

    // Get all of the input cells except the first and last...
    var input_cells = edit_row.find('td');
    input_cells = input_cells.not(':first').not(':last');

    // Iterate through all relevant inputs in all relevant cells, get form data
    jQuery.each(input_cells, function (i, cell) {
        jQuery.each (getEditInputs(cell), function (i, input) {
            // Adds an entry to form_data in the form input.name = input.value
            form_data[jQuery(input).attr('name').split(".").slice(1).join(".")] = jQuery(input).val();
        });
    });

    console.log(JSON.stringify(form_data))
    jQuery.ajax({
        url: 'roles/ajx_roles.php',
        type: "post",
        data: {
            json: JSON.stringify(form_data)
        }
     })
     .done(function(response){
        if (!response.result) {
            alert("Failed to add/update role: " + response.message);
            return true;
        }

        // Do stuff here to revert the controls.
        jQuery.each(input_cells, function(i, cell) {
            removeEditInputs(cell, cell_fields, true);
        });

        // Revert the table to how it looked before.
        enableCheckboxes(table, true);
        enableEditButtons(table, true);
        enableSortableFieldHeaders(true);
        removeEditControls(table);

        edit_row.get(0).className = "datarow";

        if (adding_record) {
            /* Specific changes that need to be made if a new record is
            /* being inserted.
             */
            enableTableBottomControls(true);
            edit_row.attr('id', 'role_row_' + response.record_id);
            jQuery('#edit_0').attr('id', 'edit_' + response.record_id);

            var checkbox = jQuery('#role_id[0]');
            checkbox.val(response.record_id);
            checkbox.attr('id', 'role_id[' + response.record_id + ']');
        }
     })
     .fail(function(jqXHR) {
        var message = "Failed to add/update role.\n";
        switch (jqXHR.status) {
            case 404:
            case 500:
                message += "An internal error occurred";
                break;
            case 400:
                message += "Update request was invalid.";
                break;
            case 403:
                message += "Sorry, You may not be logged in or your session may have expired";
                break;
            default:
                message += "An unspecified error has occurred. Contact your system administrator.";
                message += "\n(HTTP Code: " + jqXHR.status + ")";
        }

        alert(message);
     });
}

function handleClickGo() {
    var task_select = jQuery('#todo');
    var action = jQuery(task_select).get(0).options[task_select.selectedIndex].value;
    var role_id;

    switch (action) {
        case 'delete':
            var to_delete = [];

            /* Iterate through all of the checked roles and add their IDs to a
             * list.
             */
            var checked_boxes = jQuery('input[id^=role_id]:checked');
            jQuery.each(checked_boxes, function (i, checkbox) {
                role_id = parseInt(checkbox.id.match(/[0-9]+/)[0]);
                to_delete.push(role_id);
            });

            /* Make sure the user wants to delete records. If not, don't go
             * any further.
             */
            if (to_delete.length > 0) {
                var dialog_text = "** Are you sure you want to DELETE the "
                    + to_delete.length + " selected Examiner(s) permanently "
                    + "from the system? **";

                // Bail out if the user doesn't want to continue.
                if (!confirm(dialog_text)) {
                    return true;
                }
            }

            // Populate the form.
            var form_data = {};
            form_data['action'] = action;
            form_data['role_id'] = to_delete;

            // Okay, here we go. Push the request.
            jQuery.ajax({
                url: 'roles/ajx_roles.php',
                type: "post",
                data: {
                    json: JSON.stringify(form_data)
                }
             })
             .done(function(response){
                if (!response.result) {
                    alert("Failed to delete role(s): " + response.message);
                    return true;
                } else {
                    // TODO: Remove all table rows that were just deleted.
                    jQuery.each(response.record_id, function (i, role_id) {
                        var row = jQuery('#role_row_' + role_id);
                        row.remove();
                    });
                }
             })
             .fail(function(jqXHR) {
                var message = "Failed to add/update role.\n";
                switch (jqXHR.status) {
                    case 404:
                    case 500:
                        message += "An internal error occurred";
                        break;
                    case 400:
                        message += "Update request was invalid.";
                        break;
                    case 403:
                        message += "Sorry, You may not be logged in or your session may have expired";
                        break;
                    default:
                        message += "An unspecified error has occurred. Contact your system administrator.";
                        message += "\n(HTTP Code: " + jqXHR.status + ")";
                }

                alert(message);
             });

            break
        default:
            alert("Unrecognized action: " + action);
            return true;
    }
}

/**
 * "Add" click handler.
 *
 * @returns {undefined}
 */
function handleClickAdd() {
    var table = jQuery('#role_list_table');
    // This index should be right as we'll have the table header row as well.
    var cell_index = 1;
    var input_cell;
    var edit_cell;
    var html;
    // 
    var new_row = jQuery('<tr>');
    new_row.attr('id', 'role_row_0');    // Deciding that zero means new role.
    
    // Add the cells...
    var checkbox_cell = jQuery('<td>').get(0);
    checkbox_cell.className = "checkb";
    checkbox_cell.innerHTML = '<input type="checkbox" id="role_id[0]" name="role_id[]" value="0"/>';
    
    new_row.prepend(checkbox_cell);
    
    jQuery.each(cell_fields, function (i, cell) {
        input_cell = jQuery('<td>').get(0);
        input_cell.className = "w0";
        if(cell_fields.length - 1 > i){
            new_row.find('td').eq(i).after(jQuery(input_cell))
        }
        cell_index = i;
    });
    
    edit_cell = jQuery('<td>');
    html = '<a id="edit_0" href="javascript:void(0)">';
    html += '<img class="editb" title="Edit" alt="Edit" src="assets/images/edit.svg"/>';
    html += '</a>';
    edit_cell.html(html);
    new_row.find('td').eq(cell_index).after(jQuery(edit_cell))
    
    // Get all cells except the first (checkbox) and last (edit button).
    var cells = jQuery(new_row).find('td').not(':first').not(':last');
    cell_index = 1;
    jQuery.each(cells, function (i, cell) {
        cell.innerHTML = injectEditInput(jQuery(cell).get(0), cell_fields, cell_index);
        cell_index++;
    });
    
    // Add the red border around the row being edited
    new_row.attr('class', 'editrw');
    table.append(new_row)
    
    enableCheckboxes(table, false);
    enableTableBottomControls(false);
    enableEditButtons(table, false);
    enableSortableFieldHeaders(false);
    
    injectEditControls(jQuery(table), jQuery(new_row));

    jQuery('#role_id[0]').on('click', handleEnableTaskSelect);
}

jQuery(document).ready(function() {
    // Attach an event to the "check all" checkbox.
    jQuery('#check-all').on('click', handleClickSelectAll);
    jQuery('#add').on('click', handleClickAdd);

    // Add event handlers to the checkboxes that select each role/row.
    var checkboxes = jQuery('#role_list_form').find('input[type=checkbox]');
    jQuery.each(checkboxes, function (i, cb) {
        jQuery(cb).on('click', handleEnableTaskSelect);
    });

    // Make the row headers sortable...
    enableSortableFieldHeaders(true);

    // Attach event handlers to the edit "pencils"...
    enableEditButtons(jQuery('#role_list_form'), true);

    // Attach an event handler to the "go" button...
    jQuery('#role_task').on('click', handleClickGo);
});