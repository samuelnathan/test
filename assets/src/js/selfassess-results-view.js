 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2018, Qpercom Limited
  */
 
 jQuery(document).ready(function() {

    jQuery('.results-assessments').click(function () {

        if (jQuery('#selfassess-table').length === 0) {

          jQuery('#results-assessments').text("loading self assessments...");  

          jQuery.ajax({
             url: "analysis/self_assessments.php",
             type: "post",
             dataType: "json",
             cache: false,
             data: {
               exam: jQuery('#exam-id').val()
             }
          })
          .done(function(data) {

             // No self assessments
             if (Object.keys(data).length == 0) {

                 jQuery('#results-assessments')
                 .empty().text("No self assessments found");
                 return;

             }

             // Create self assessment table
             var saTitleRow = jQuery("<tr>", {'class': 'top-titlerow'})
                     .append(jQuery("<th>", {'class': 'titletop3 wrap', 'text': 'Applicant'}));                 
             var saTable = jQuery("<table>", {'id': 'selfassess-table'})
                     .append(saTitleRow);

             jQuery('#results-assessments').html(saTable);

             // Add self assessment questions
             var selfAssessCount = 0;
             jQuery.each(data.questions, function(questionID, question) {

                selfAssessCount++;

                saTitleRow.append(
                    jQuery("<th>", {'class': 'titletop3 wrap', 'text': selfAssessCount + ") " + question.text})
                );

             });

             // Add self assessment responses
             jQuery.each(data.responses, function(applicantID, responseData) {

                var applicantLink = jQuery("<a>", {
                    'href': 'javascript:void(0)',
                    'class': 'examineel self-section',
                    'title': "Click to view results for applicant: " + applicantID,
                    'text': applicantID
                });

                var saDataRow = jQuery("<tr>", {'class': 'self-assessment-rw'})
                    .append(jQuery("<td>", {'class': 'bglg2 wrap'})
                    .append(applicantLink));      

                jQuery.each(data.questions, function(questionID) {

                      if (questionID in responseData) {

                          var responseDiv = jQuery("<div>", {
                            'class': 'response-quote', 
                            'text': "\"" + responseData[questionID].text + "\""
                          });

                          var scoreDiv = jQuery("<div>", {
                              'class': 'response-score', 
                              'text': "Score: " + responseData[questionID].score
                          });                      

                          saDataRow.append(
                            (jQuery("<td>", {'class': 'bglg2 wrap'}).append(responseDiv, scoreDiv))
                          );
                      
                      } else {

                          saDataRow.append(
                            (jQuery("<td>", {
                              'class': 'response-missing', 
                              'text': 'Not Imported, please review import file'
                            }))
                          );               

                      }
      
                });

               saTable.append(saDataRow);

             });
             
             // Add applicants with no self assessments
             jQuery.each(data.studentsWithout, function(index, id) {

              var applicantLink = jQuery("<a>", {
                'href': 'javascript:void(0)',
                'class': 'examineel self-section',
                'title': "Click to view results for applicant: " + id,
                'text': id
              });         

              var withoutRow = jQuery("<tr>", {'class': 'self-assessment-rw'})
              .append(
                  jQuery("<td>", {'class': 'bglg2 wrap'})
                  .append(applicantLink)
              )
              .append(
                  jQuery("<td>", {
                      'class': 'red',
                      'colspan': Object.keys(data.questions).length,
                      'text': "no self assessment responses/scores found for this applicant"
                  })
              )

              saTable.append(withoutRow);

           });

           // Add warning about missing self assessments
           if (data.studentsWithout.length > 0) {

               saTable.before(jQuery("<div>",
                   {html: data.studentsWithout.length + " applicants are missing their self assessments. Please review and " +
                       "<a href='manage.php?page=importingStep1&dataType=6'>import complete set again</a>",
                    class: "selfassess-warn"
                   })
               );

           }

          })
          .fail(function(jqXHR) {

            console.log(
              httpStatusCodeHandler(
                 jqXHR.status,
                 "Failed to retrieve self assessments.\n"
            ));

          });             

    }

  });

  // Trigger onload
  if (jQuery('.results-assessments').attr('id') === "tab-selected") {

    jQuery('.results-assessments').trigger('click');

  }

 });

 