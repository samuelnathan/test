/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2019, Qpercom Limited
 * Student Summary Page & Student History Page
 */
 
 jQuery(document).ready(function() {
       
    // Print Student Feedback Section
    jQuery('#print-sres').click(function() {
        window.print();
    });
 
    // Back Button
    jQuery('#back-button').click(function() {
        history.go(-1);
        return false;
    });
 
    // Comparison (Partial Mootools Implementation)
    jQuery('#compare-type').change(function() {
        var theURI = parseQueryString(window.location.search)
        theURI['cnm'] = jQuery(this).val();
        window.location.search = '?' + jQuery.param( theURI )
    });
 
    // Which title to use
    if (jQuery('#examinee-summary-history').length) {
        document.title = jQuery('#surname').val() + ' | Results History';
    } else {
        document.title = jQuery('#surname').val() + ' | ' + jQuery('#exam-name').val();
    }
 
    // Advanced options show link
    jQuery('#advanced-options-show').click(function() {

        if (jQuery(this).val() == "show") {
          
             jQuery('#advanced-options-div').show();
             jQuery(this).val("hide");

        } else {
           
            jQuery('#advanced-options-div').hide();
            jQuery(this).val("show");

        }

    });
 
    // Cancel advanced settings
    jQuery('#cancel-action').click(function() {

        jQuery('#advanced-options-div').hide();
        jQuery('#advanced-options-show').val("show");

    });        

    // Grade/Appointability events
    jQuery('#edit-grade').click(function() {

        jQuery(this).hide();
        jQuery('#grade-readonly').hide();
        jQuery('#change-grade').show();

    });

    jQuery('#change-grade').change(function() {

        // Dialog message and title
        var changeFromClass = jQuery('#grade-readonly-value').text().replace(/\s+/g, "-").toLowerCase();
        var changeToClass = jQuery('#change-grade').val().replace(/\s+/g, "-").toLowerCase();

        jQuery('#dialog-grade-message').html(
           [jQuery("<div>", {
               'html': "Are you sure you want to change the appointability for this "
               + jQuery('#student-translate').val() + " <span class='red'>"
               + jQuery('#surname').val() + " - " + jQuery('#current-student-id').val() + "</span>?"
           }),
           jQuery("<div>", {
               'id': 'grade-confirm-criteria',
               'html': "<span class='" + changeFromClass + "'>" + jQuery('#grade-readonly-value').text()
               + "</span> => <span class='" + changeToClass + "'>" + jQuery('#change-grade').val() + "</span>"
            })]
        );

        // Confirm dialog
        jQuery('#dialog-grade-confirm').dialog({
            title: "Change Appointability",
            height: 180,
            width: 400,
            modal: true,
            buttons: {
                "Cancel": function() {

                    jQuery('#dialog-grade-message').html("");
                    jQuery(this).dialog('close');
                    jQuery('#edit-grade').show();
                    jQuery('#grade-readonly').show();
                    jQuery('#change-grade').hide();

                },
                "Change": function() {

                    jQuery(this).dialog('close');

                    jQuery.ajax({
                        url: "analysis/asyn_analysis.php",
                        type: "post",
                        dataType: "json",
                        cache: false,
                        data: {
                            isfor: 'change_grade',
                            student: jQuery('#student-id').val(),
                            exam: jQuery('#exam-id').val(),
                            grade: jQuery('#change-grade').val(),
                            last_updated_timestamp: jQuery('#last-updated-timestamp').val(),
                            last_updated_grade: jQuery('#last-updated-grade').val(),
                        }
                    }).done(function() {

                        window.opener.submitUpdateForm(null);
                        refreshPage(false);

                    }).fail(function(jqXHR) {

                        // expired record, appointability/grade has since updated
                        if (jqXHR.status == 409) {

                            var confirmMessage = "The record you are trying to update has since " +
                            "been updated, would you like to reload the page to view changes?";
                            if (confirm(confirmMessage)) {

                                refreshPage(false);

                            }

                        } else {

                            alert(
                                httpStatusCodeHandler(
                                    jqXHR.status,
                                    "Failed to update grade, please try again\n"
                                ));
                        }

                    });

                }
            }
        });

    });

     /**
     * Advanced options form on the student exam results page
     */
    jQuery('#submit-action').click(function() {  

       var passwordValue = jQuery('#confirm-password').val();
       var dialogMessage, dialogTitle = "";
         
       if (jQuery('#mark-all-absent').is(':checked')) {
         
          var actionToTake = 0;
          
       } else if (jQuery('#delete-all-results').is(':checked')) {

          var actionToTake = 1; 
           
       }
 
       // Action to take not selected
       if (typeof actionToTake === "undefined") {

         alert("Please choose an action to take");
         return false;
 
       // Password field incomplete
       } else if (passwordValue.length == 0) {

         alert("Please complete password field");
         return false;
 
        // Mark all as absent
        } else if (actionToTake == 0) {

        dialogMessage = "All " + jQuery('#exam-translate').val() + " results for this " + jQuery('#student-translate').val() + " will be marked as ABSENT." +
                        "<br/><br/>Are you sure you want to continue?";
        dialogTitle = "Mark All " + jQuery('#student-translate-uc').val() + " Results Absent";
 
        // Delete all results
        } else if(actionToTake == 1) {

        dialogMessage = "All " + jQuery('#exam-translate').val() + " results for this " + jQuery('#student-translate').val() + " will be permanently DELETED and cannot be recovered." +
                        "<br/><br/>Are you sure you want to continue?";
        dialogTitle = "Delete " + jQuery('#exam-translate-uc').val() + " Results for this " + jQuery('#student-translate-uc').val();
        
        }
        
        jQuery('#advanced-options-div').hide();
        jQuery('#advanced-options-show').val("show");    
 
        // Dialog message and title
        jQuery('#dialog-message').html(dialogMessage);
 
        // Confirm dialog
        jQuery('#dialog-action-confirm').dialog({
          title: dialogTitle,
          height: 180,
          width: 400,
          modal: true,
          buttons: {
          "Cancel": function() {

             jQuery('#dialog-message').html("");
             jQuery(this).dialog('close');

          },
          "Continue": function() {

              jQuery(this).dialog('close');
              jQuery('#advanced-results-options').submit();

          }
         }
       });           
 
       return false;
    });
 
    /**
     * Student notes/issues field events
     */
    jQuery('button#edit-student-notes').click(function() {
         
        var action = jQuery(this).val(); 

        if (action == "edit") {

            jQuery('#student-notes')
               .addClass('notes-hl')
               .prop('readonly', false);

            jQuery(this).val('save').text('save');
            jQuery('#cancel-student-notes').show();
         
        } else if (action == "save") {

            jQuery('#notes-button-box').addClass('waitv6');

            jQuery.ajax({
                url: "analysis/asyn_analysis.php",
                type: "post",
                dataType: "json",
                cache: false,
                data: {
                   isfor: 'update_student_notes',
                   student: jQuery('#notes-student-id').val(),
                   exam: jQuery('#notes-exam-id').val(),
                   notes: jQuery('#student-notes').val()
                }
            })
            .done(function(response) {

                jQuery('#notes-button-box').removeClass('waitv6');
                
                if (response.error) {

                   alert("Failed to update notes field, please try again");
                   return false;

                } else {

                   jQuery('#notes-updated').text(response.notes_updated);
                   jQuery('#notes-author').text(response.notes_author); 

                }
                
                jQuery('#student-notes')
                  .prop('readonly', true)
                  .removeClass('notes-hl');

                var buttonLabel = jQuery('#student-notes').val().length > 0 ? 'edit' : 'add';
 
                jQuery('#edit-student-notes').val('edit').text(buttonLabel);
                jQuery('#cancel-student-notes').hide();
                jQuery('#notes-original').val(
                    jQuery('#student-notes').val()
                );

            })
            .fail(function(jqXHR) {

                jQuery('#notes-button-box').removeClass('waitv6');
                alert(
                    httpStatusCodeHandler(
                        jqXHR.status,
                        "Failed to update notes field, please try again\n"
                ));
             
            });

        }

    });

    /**
     * Student notes/issues update cancel event
     */
    jQuery('button#cancel-student-notes').click(function() {

        var orgNotes = jQuery('#notes-original').val();
        jQuery('#student-notes')
           .removeClass('notes-hl')
           .val(orgNotes)
           .prop('readonly', true);

        var cancelMode = orgNotes.length > 0 ? 'edit' : 'add';

        jQuery('#edit-student-notes')
          .val('edit')
          .text(cancelMode);

        jQuery(this).hide();

    });    
 
  // Update parent page
  if (jQuery('#action-success-yes').length) {
    window.opener.submitUpdateForm(null);
  }
  
  // Grab out of place flash messages and place them into the html rendered from the templates
  var breakFlash = jQuery("#break-flash").detach();
  var flashMessages = jQuery("#flash-messages").detach();
  jQuery("#error_status_messages").append(breakFlash, flashMessages);
  jQuery('#content-div').show();
  
 });
