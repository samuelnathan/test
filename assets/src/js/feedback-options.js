function FeedbackOptions(className, optionNameAttribute, examSelectSelector) {
    /**
     * Options that can be hidden
     * @type {HTMLCollectionOf<Element>}
     */
    var autoHideOptions = document.getElementsByClassName(className);

    /**
     * Association of option names and a list of the elements that are shown if the option is enabled
     * @type {{}}
     */
    var options = {};

    var refreshOptionsForExam = function(examId) {
        // Hide all the auto-hide options
        Array.prototype.forEach.call(autoHideOptions, function(optionElement) { jQuery(optionElement).hide() });

        // Get the enabled options for the selected exam
        jQuery.ajax('tools/ajx_feedback_tool_options.php', {
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                options: Object.keys(options),
                examId: examId
            }),
            success: function(enabledOptions) {
                // Show the enabled options
                enabledOptions.forEach(function (enabledOption) {
                    options[enabledOption].forEach(function(element) {
                        jQuery(element).show();
                    });
                });
            }
        });
    };

    this.start = function() {
        options = Array.prototype.reduce.call(autoHideOptions, function (options, optionElement) {
            var optionName = optionElement.dataset[optionNameAttribute];
            if (options[optionName])
                options[optionName].push(optionElement);
            else
                options[optionName] = [optionElement];

            return options;
        }, {});

        jQuery(examSelectSelector).change(function() {
            refreshOptionsForExam(this.value);
        });
    };
}

jQuery(function () {
    var optionsAutoHide = new FeedbackOptions('auto-hide-option', 'optionName', '#exam');
    optionsAutoHide.start();
});