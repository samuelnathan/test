/**
 * Vue.js component that displays the rotation monitor of a session
 */
Vue.component('session-monitor', {
    template: '#session-monitor-template',

    props: [
        'rotationData',
        'sessionId',
        'sessionDescription',
        'sessionDate'
    ],

    data: function() {
        // Initialize the WebSocket server
        var sessionMonitor = SessionMonitorPool.getMonitorForSession(this.sessionId, this);
        // Instantiate the business logic object
        var rotationsLogic = SessionRotationsPool.sessionRotations(this.rotationData, this.sessionId);

        // If the session has an ongoing rotation, notify it as an event
        if (this.rotationData.ongoing) {
            rotationsLogic.updateOngoing();
            this.$emit('changed-ongoing');
        }

        return {
            /**
             * Presentation options
             */
            options: {
                selectedGroups: this.rotationData.groups.map(rotationsLogic.selectGroupId)
            },

            sessionMonitor: sessionMonitor,

            rotationsLogic: rotationsLogic
        };
    },

    computed: {
        formattedDate: function() {
            return moment(this.sessionDate).format('MMMM Do YYYY');
        }
    },

    methods: {
        /**
         * Check if a given group is selected to be displayed
         * @param group
         * @returns {boolean}
         */
        displayGroup: function(group) {
            return this.options.selectedGroups.some(function(groupId) {
                return group.group.group_id === groupId;
            });
        },

        /**
         * Get the classes that a rotation cell in the table may have depending on whether the rotation is
         * the ongoing one
         * @param {Object} item Object that may be ongoing
         * @param {ongoingCallback} isOngoing Function that, when evaluated with the item, returns true if
         *        it's ongoing and false otherwise
         * @returns {Object}
         */
        ongoingCell: function(item, isOngoing) {
            return {
                'info': isOngoing(item),
                'rotation': true
            }
        },

        /**
         * Show the details for a given student-station
         * @param {Object} studentStation
         */
        detailStudentStation: function(event) {
            this.$emit('detailed-student', event);
        },

        /**
         * Update the position of the details dialog
         * @param event {MouseEvent}
         */
        updateDetailPosition: function(event) {
            this.$emit('update-detail-position', event);
        },

        hideStudentStation: function() {
            this.$emit('hide-student');
        },

        /**
         * Toggle all the selected groups checkboxes
         * @param {MouseEvent} event
         */
        toggleCheckboxes: function(event) {
            this.options.selectedGroups = event.target.checked ?
                this.rotationData.groups.map(this.rotationsLogic.selectGroupId) :
                [];
        }
    }
});