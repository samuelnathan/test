/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */ 
 
// add button event 
jQuery(document).ready(function() {
    if (jQuery('#check-all').length) {
        addCheckboxEvents('assist');
    }

    if (jQuery('#add').length) {
        add_Button_Event();
    }

    if (jQuery('#assist_list_form').length) {
        edit_Events();
    }
    showMenu();
    performScroll();
});

function add_Button_Event() {
    jQuery('#add').on('click', function(e) {
        e.preventDefault();
        jQuery('#assist_table').find("input, select, img, a:not([class~=orderlink])").css('display', 'none');

        if (jQuery('#first_assist_row').length) {
            var fsr = jQuery("#first_assist_row").remove();
            var lastrow = jQuery("#title-row");
        } else {
            if (jQuery('#count').length) {
                var lastrow = jQuery('#assist_row_' + jQuery('#count').val());
            }else{
                var lastrow = jQuery('[id^="assist_row_"]:last');
            }
        }

        var new_addrow = jQuery('<tr>', {'id': 'add-row'});
        lastrow.after(new_addrow);

        setWaitStatusRow(new_addrow, 5);

        var url = "assistants/ajx_assist.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random, 
                isfor: "add"
            }
         })
         .done(function(html){
            new_addrow.empty().attr('class', 'addrow').html(html);
            addButtonsRow();
            add_submit_Event();
            add_Cancel_Event(new_addrow, fsr);
            jQuery('#add_id').focus();
            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            new_addrow.remove();
            jQuery('#assist_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

//submit
function add_submit_Event() {
    jQuery('#add-save').on('click', function(e) {
        e.preventDefault();
        if (validate_Admin_Row('add')) {
            
            // Validate record ID field
            if (!isRecordIDValid(jQuery('#add_id').val(), false)) {
                return false;
            }          
            
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            var url = "assistants/ajx_assist.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "submit",
                    add_id: jQuery('#add_id').val(),
                    add_fn: jQuery('#add_fn').val(),
                    add_sn: jQuery('#add_sn').val(),
                    add_email: jQuery('#add_email').val(),
                    add_mobile: jQuery('#add_mobile').val()
                }
             })
             .done(function(html){
                if (html === "USER_ID_EXISTS") {
                    failAdd();
                    alert('User ID exists in the System already, please specify a different User ID');
                }
                else {
                    prepareScroll('scrolltobase');
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });
        }
    });
}

//validate station row
function validate_Admin_Row(rt) {
    if (jQuery('#' + rt + '_id').val().length === 0) {
        alert("please specify an ID for the Assistant");
        return false;
    } else if (jQuery('#' + rt + '_fn').val().length === 0) {
        alert("please specify a Forename for the Assistant");
        return false;
    } else if (jQuery('#' + rt + '_sn').val().length === 0) {
        alert("please specify a Surname for the Assistant");
        return false;
    } else if (jQuery('#' + rt + '_email').val().length > 0 && !isEmailValid(jQuery('#' + rt + '_email').val())) {
        alert("Email Address specified is invalid, please review");
        return false;
    } else if (jQuery('#' + rt + '_mobile').val().length > 0 && isNaN(jQuery('#' + rt + '_mobile').val())) {
        alert("The Mobile Number must be numeric");
        return false;
    }
    return true;
}

// add cancel event
function add_Cancel_Event(row, fsr) {
    jQuery('#add-cancel').on('click', function(e) {
        e.preventDefault();

        if (!jQuery('#count').length) {
            row.replaceWith(fsr);
        } else {
            row.remove();
        }
        jQuery('#assist_table').find("select, img, input, a").css('display', '');
        removeNewRecordControls();
    });
}

//Edit Events
function edit_Events() {
    if (jQuery('#count').length || jQuery('#assist_list_form').find('a[id^=edit_]').length) {
        var buttons = jQuery('#assist_list_form').find('a[id^=edit_]');

        jQuery.each(buttons, function(i, b) {
            edit_Event(b);
        });
    }
}
 
function edit_Event(b) {
    jQuery(b).off('click').on('click', function (e) {
        e.preventDefault();
        var row = jQuery(this).parents('tr:first');
        var user_id = row.find('input[id^=assist-check]').val();
        var fsr = row.clone(true, true);
    
        jQuery('#assist_table').find("input").css('display', 'none');
        jQuery('#assist_table').find("select, img, a:not([class~=orderlink])").css('display', 'none');
        setWaitStatusRow(row, 5);
    
        var url = "assistants/ajx_assist.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: "edit",
                org_user_id: user_id
            }
         })
         .done(function(html){
            if (html === "USER_NON_EXIST") {
                alert('The Assistant you were trying to update was not found in the System or has recently been deleted, the Session Administrators list will now be refreshed');
                refreshPage(false);
            }
            else {
                row.empty().attr('class', "editrw").attr('id', "edit_row").html(html);
                editButtonsRow(jQuery(row), 5);
                cancel_Update_Event(jQuery('#edit-cancel'), fsr, row);
                update_Event();
            }
         })
         .fail(function(jqXHR) {
            row.replaceWith(fsr);
            jQuery('#assist_table').find("input, select, a, img").css('display', '');
            edit_Event(fsr.find('a[id^=edit_]'));
            addCheckboxEvent(fsr.find('input[id^=assist-check]'), "assist");
            alert('The request failed, Please try again');
         });
    });
}

//update event
function update_Event() {
    jQuery('#edit-update').on('click', function (e) {
        e.preventDefault();
        if (validate_Admin_Row('edit')) {

            // Validate record ID field
            if (!isRecordIDValid(jQuery('#edit_id').val(), false)) {
                return false;
            }

            jQuery('#edit-update').css('display', 'none');
            jQuery('#edit-cancel').css('display', 'none');
            jQuery('#b_edit_td').attr('class', 'waittd2');

            var url = "assistants/ajx_assist.php";
            var random = Math.random();
            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: "update",
                    org_user_id: jQuery('#org_user_id').val(),
                    edit_id: jQuery('#edit_id').val(),
                    edit_fn: jQuery('#edit_fn').val(),
                    edit_sn: jQuery('#edit_sn').val(),
                    edit_email: jQuery('#edit_email').val(),
                    edit_mobile: jQuery('#edit_mobile').val()
                }
             })
             .done(function(html){
                if (html === "USER_NON_EXIST") {
                    jQuery('#b_edit_td').attr('class', 'editbts');
                    alert('The Assistant you were trying to update was not found in the System or has recently been deleted, the Assistants list will now be refreshed');
                    refreshPage(false);
                } else if (html === "USERID_DOES_EXIST") {
                    failEdit();
                    alert('The Assistant ID you specified is already in use in the System, please specify a different ID');
                } else {
                    refreshPage(false);
                }
             })
             .fail(function(jqXHR) {
                failEdit();
                alert('The request failed, Please try again');
             });
        }
    });
}

//cancel update event
function cancel_Update_Event(button, org, oldrow, br) {
    jQuery(button).on('click', function(e) {
        e.preventDefault();
        jQuery('#b_edit').remove();
        oldrow.replaceWith(org);
        jQuery('#assist_table').find("input, a, img, select").css('display', '');
        edit_Event(org.find('a[id^=edit_]'));
        addCheckboxEvent(org.find('input[id^=assist-check]'), "assist");
    });
}

function ValidateForm(form) {
    if (jQuery(form).attr('name') === 'assist_list_form') {
        var thecbs = jQuery('#assist_list_form').find('input[id^=assist-check]');

        var isChecked = false;
        jQuery.each(thecbs, function(i, cb) {
            isChecked |= jQuery(cb).prop('checked');
        });

        if (isChecked) {
            return confirm("Are you sure you want to delete the selected assistant(s)?");
        } else {
            // If there are no items checked, cancel the form submission process.
            return false;
        }
    }
}