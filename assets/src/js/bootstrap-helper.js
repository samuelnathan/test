// Determine screen type
document.documentElement.className += (("ontouchstart" in document.documentElement) ? ' touch-screen' : ' no-touch-screen');


document.addEventListener("DOMContentLoaded", function() {
    var bootstrapButton = $.fn.button.noConflict();
    $.fn.bootstrapBtn = bootstrapButton;

    $(document).on('change', '#todo',function () {
        setDeleteClass($(this));
    });

    $('#todo').each(function () {
        setDeleteClass($(this));
    });

    function setDeleteClass($el) {
        var value = $el.val();
        if(value == 'delete' || value == 'remove') {
            $el.addClass('is-delete');
        }else{
            $el.removeClass('is-delete');
        }
    }
});

function getColorTheme(primary_color) {
    if(!primary_color){
        return;
    }
    var hsl = tinycolor(primary_color).toHsl();

    setVariable('--primary-main', primary_color);
    setVariable('--primary-dark-main', dark());
    setVariable('--primary-border-main', border())
    setVariable('--primary-control-main', control())
    setVariable('--primary-active-main', active())
    setVariable('--primary-group-main', group())
    setVariable('--primary-group-dark-main', groupDark())
    setVariable('--primary-group-2-main', group2())
    setVariable('--primary-success-main', success())
    setVariable('--primary-opacity-main', opacity1())
    setVariable('--primary-opacity-2-main', opacity2())
    setVariable('--primary-opacity-3-main', opacity3())
    setVariable('--primary-opacity-4-main', opacity4())
    // test color
    // var mainhsl = tinycolor('rgba(47,128,64,.08)').toHsl();

    // console.log(getC());

    // function getC (){
    //     var h = hsl.h;
    //     var s = hsl.s;
    //     var l = hsl.l;
    //     var hmainhsl = mainhsl.h;
    //     var smainhsl = mainhsl.s;
    //     var lmainhsl = mainhsl.l;
    //     console.log(hmainhsl/h);
    //     console.log(smainhsl/s);
    //     console.log(lmainhsl/l);
    // }

    function opacity4() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0034005343696868;
        result.s = hsl.s * 0.662551440329218;
        result.l = hsl.l * 1.2857142857142856;
        result.a = .08;

        return tinycolor(result).toRgbString();
    }
    function opacity3() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0034005343696868;
        result.s = hsl.s * 0.662551440329218;
        result.l = hsl.l * 1.2857142857142856;
        result.a = .5;

        return tinycolor(result).toRgbString();
    }
    function opacity2() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0034005343696868;
        result.s = hsl.s * 0.662551440329218;
        result.l = hsl.l * 1.2857142857142856;
        result.a = .25;

        return tinycolor(result).toRgbString();
    }
    function opacity1() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0034005343696868;
        result.s = hsl.s * 0.662551440329218;
        result.l = hsl.l * 1.2857142857142856;
        result.a = .5;

        return tinycolor(result).toRgbString();
    }
    function success() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0034005343696868;
        result.s = hsl.s * 1.014109347442681;
        result.l = hsl.l * 0.5599999999999999;

        return '#' + tinycolor(result).toHex().toString();
    }
    function group2() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.018156424581006;
        result.s = hsl.s * 0.5041152263374484;
        result.l = hsl.l * 2.228571428571428;

        return '#' + tinycolor(result).toHex().toString();
    }
    function groupDark() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 0.999740158503313;
        result.s = hsl.s * 1.0208926875593545;
        result.l = hsl.l * 0.52;

        return '#' + tinycolor(result).toHex().toString();
    }

    function group() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 1.0078720162519044;
        result.s = hsl.s * 0.5056474914630941;
        result.l = hsl.l * 2.3771428571428572;

        return '#' +tinycolor(result).toHex().toString();
    }

    function active() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 0.9993016759776537;
        result.s = hsl.s * 0.9971509971509975;
        result.l = hsl.l * 2.0228571428571427;

        return '#' + tinycolor(result).toHex().toString();
    }
    
    function control() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * 0.9993016759776537;
        result.s = hsl.s * 0.9971509971509971;
        result.l = hsl.l * 1.7257142857142858;

        return '#' + tinycolor(result).toHex().toString();
    }

    function border() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * .9986515122327106;
        result.s = hsl.s * 1.0105535643170052;
        result.l = hsl.l * 0.7085714285714286;
        return '#' + tinycolor(result).toHex().toString();
    }

    function dark() {
        var result = tinycolor(primary_color).clone().toHsl()
        result.h = hsl.h * .9984038308060655;
        result.s = hsl.s * .9935117599351178;
        result.l = hsl.l * .7828571428571428;
        return '#' + tinycolor(result).toHex().toString();
    }

    function setVariable(name, value) {
        document.documentElement.style.setProperty(name, value);
    }
}