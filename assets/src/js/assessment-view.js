  /**
   * @author Enda Phelan <enda.phelan@qpercom.ie>
   * For Qpercom Ltd
   * @copyright Copyright (c) 2017, Qpercom Limited
   */

  var session = null;
  var sessionKey = 'self-assessment-panel-options';
  var contentWidth = null;

  $(document).ready(function () {
      session = getSession();

      setContentWidth();
      showAssessmentButtonIfExists();

      if (!session) {
          // this is a brand new session, so let's create a session object and save it.
          session = {
              open: false,
              position: 'right',
              width: 300,
              fullScreen: false
          };

          saveSession(session);

      } else {
          session = getSession();
      }

      if (session.open) {
          // create the side panel then open it automatically
          // as the user had it when they left this page
          createSidePanel(session.width, session.fullScreen, '-150%', session.position);
          openSidePanel();
      } else {
          // just create the side panel and sit it off to one side
          createSidePanel(session.width, session.fullScreen, '-150%', session.position);
      }
  });

  /**
   * Any time the window has been resized (including an orientation change on mobile)
   * Reset the panel's height and width, if necessary, to ensure it all sides remain in
   * the viewport.
   */
  $(window).resize(function () {
      var panel = $('#self-assessment-panel');

      // the window has been re-sized so ensure that the panel is full height.
      panel.css('height', '100vh');

      // if the panel's old width is too wide, shrink
      // it so that all edges are within the new window width.
      if ($(this).width() < session.width) {
          session.width = $(this).width() - 50;
          setWidth(panel, session.width + 'px');
          saveSession(session);
      }

      $('#maincontdiv').css('width', '100%');
      setContentWidth();

      var panelWidth = session.open ? panel.width() : 0;
      resizeContentDiv(panelWidth);
  });

  function setContentWidth() {
      var content = $('#maincontdiv');

      contentWidth = content.width();
  }

  /**
   * Check if a self assessment exists for the current student and exam. Only make the
   * button to open the scoresheet visible it available if it exists
   */
  function showAssessmentButtonIfExists() {
      // Commented after deletion of assess/ajx_self_assessment_exists.php
      // $.ajax({
      //     url: 'assess/ajx_self_assessment_exists.php',
      //     type: 'post',
      //     dataType: 'json',
      //     data: {
      //         mode: 'list'
      //     },
      //     success: function () {
      //         showSelfAssessmentButton();
      //     }
      // });
  }

  /**
   * Set up an event that will trigger when the user re-sizes the score-sheet panel.
   */
  function setUpResizeEvent() {
      interact('#self-assessment-panel').resizable({
          edges: {
              right: session.position === 'left',
              left: session.position === 'right'
          },
          restrictEdges: {
              outer: 'parent',
              endOnly: true
          },
          restrictSize: {
              min: {
                  width: 300
              },
              max: {
                  width: $(window).width() - 50
              }
          }
      }).on('resizemove', function (event) {
          var target = event.target;

          target.style.width = event.rect.width + 'px';
          target.style.height = event.rect.height + 'px';

          session.width = event.rect.width;

          resizeContentDiv(session.width);

          saveSession(session);
      });
  }

  /**
   * Save the session with an updated configuration.
   *
   * @param {Object} _session
   */
  function saveSession(_session) {
      var jsonOptions = JSON.stringify(_session);
      sessionStorage.setItem(sessionKey, jsonOptions);
  }

  /**
   * Retrieve the session that contains the state of
   * the self assessment panel.
   *
   * @returns {Object|null}
   */
  function getSession() {
      var session = sessionStorage.getItem(sessionKey);

      return session ? JSON.parse(session) : null;
  }

  /**
   * Get the self assessment data from the database, update the self
   * assessment panel with the score sheet data and then open the side panel
   */
  function openSidePanel() {
      // Commented after deletion of assess/ajx_assessment_view.php
      // $.ajax({
      //     url: 'assess/ajx_assessment_view.php',
      //     type: 'post',
      //     data: {
      //         mode: 'list'
      //     },
      //     dataType: 'json',
      //     success: function (data) {
      //         updateTitle(data.title);
      //         updateSelfAssessmentList(data.items);
      //         openSelfAssessmentPanel();
      //         showSelfAssessmentButton();
      //     }
      // });
  }

  /**
   * Show the self assessment button on the exam_assessment page
   */
  function showSelfAssessmentButton() {
      var button = $('#self-assessment');

      button.css('visibility', 'visible');
  }

  /**
   * Create the self assessment side panel element.
   *
   * @param {Number} width - The width to set the panel to.
   * @param {Boolean} fullScreen - Should the panel be full screen?
   * @param {String} offset - This is really the position off the page. Example, 'right': '-150%';
   * @param {String} position - The position that the panel is anchored from
   * (left, right, top, bottom)
   */
  function createSidePanel(width, fullScreen, offset, position) {
      var sidePanel = $('#self-assessment-panel');

      if (!sidePanel.length) {
          sidePanel = getSelfAssessmentPanel(position);

          var assessmentBody = $('#assessment-body');

          assessmentBody.prepend(sidePanel);
      }

      var _width = width;
      if (fullScreen) {
         _width = '100%';
      } else {
          _width += 'px';
      }

      sidePanel.css('width', _width);
      sidePanel.css(position, offset);

      setUpResizeEvent();
  }

  /**
   * Toggle the full screen state of the side panel.
   */
  function toggleFullScreen() {
      var panel = $('#self-assessment-panel');
      if (session.fullScreen) {
          setWidth(panel, session.width + 'px');

          session.fullScreen = false;
      } else {
          setWidth(panel, '100%');
          session.fullScreen = true;
      }

      saveSession(session);
  }

  /**
   * Set the width of an element.
   *
   * @param el
   * @param width
   */
  function setWidth(el, width) {
      el.css('width', width);
  }

  /**
   * Create the question row for the list item of the assessment panel.
   *
   * @param question
   * @returns {jQuery|HTMLElement}
   */
  function createQuestionRow(question) {
      var questionDiv = $('<div class="self-assess-item-heading"></div>');
      questionDiv.append('<div>' + question + '</div>');
      return questionDiv;
  }

  /**
   * Update the main title of the assessment panel.
   *
   * @param title
   */
  function updateTitle(title) {
      var titleEl = $('.self-assess-title');
      titleEl.text(title);
  }

  /**
   * Create a two-columned row on the score sheet. Typically this will have a label and its value.
   *
   * @param {String} label
   * @param {String} value
   * @param {Boolean} isTotalScore - If true, this is the last row of the score
   * sheet that has the total score. This needs to be treated differently so we will apply a different class.
   *
   * @returns {jQuery|HTMLElement}
   */
  function createRow(label, value, isTotalScore) {
      var element = $('<div class="self-assess-item-row"></div>');

      var labelClass = "self-assess-item-label";
      var valueClass = "self-assess-item-value";
      if (isTotalScore) {
          labelClass += " self-assess-total-score-label";
          valueClass += " self-assess-total-score-value";
      }

      element.append('<div class="' + labelClass + '">' + label + '</div>');
      element.append('<div class="' + valueClass + '">' + value + '</div>');

      return element;
  }

  /**
   * Create and return the self assessment panel.
   *
   * @param position
   * @returns {jQuery|HTMLElement}
   */
  function getSelfAssessmentPanel(position) {
      var selfAssessPanel = $('<div id="self-assessment-panel" class="self-assess-panel self-assess-panel-' + position + '"></div>');

      var selfAssessContent = $('<div class="self-assess-content"></div>');

      var actionsDiv = $('<div class="self-assess-actions"></div>');

      actionsDiv.append('<div class="self-assess-actions-left"><i class="fa fa-arrows-alt" aria-hidden="true" onclick="toggleFullScreen()"></i></a></div>');
      actionsDiv.append('<div class="self-assess-actions-right"><i class="fa fa-close" aria-hidden="true" onclick="closeSelfAssessmentPanel()"></i></a></div>');

      selfAssessContent.append(actionsDiv);

      selfAssessPanel.append(selfAssessContent);

      var title = $('<div><h1 class="self-assess-title"></h1>');

      selfAssessContent.append(title);

      selfAssessContent.append('<ul id="self-assess-list" data-scroll-scope></ul>');

      return selfAssessPanel;
  }

  /**
   * Update the score sheet items.
   *
   * @param items - The score sheet's items.
   */
  function updateSelfAssessmentList(items) {
      var unorderedList = $('#self-assess-list');

      unorderedList.empty();

      for (var i = 0; i < items.length; i++) {
          var listItem = createSelfAssessmentListItem(items[i]);

          unorderedList.append(listItem);
      }

      var totalScore = getTotalScore(items);

      unorderedList.append(createTotalScoreListItem(totalScore));

      $('.self-assess-content').append(unorderedList);
  }

  /**
   * Calculate the total score in the score sheet.
   *
   * @param items
   * @returns {number}
   */
  function getTotalScore(items) {
      if (!items || !items.length) {
          return 0;
      }

      var total = 0;
      items.forEach(function (item) {
          total += parseFloat(item.score);
      });

      return total;
  }

  /**
   * Create a list item for the total score.
   *
   * @param totalScore
   * @returns {jQuery|HTMLElement}
   */
  function createTotalScoreListItem(totalScore) {
      var listItem = $('<li></li>');

      var selfAssessItemDiv = $('<div class="self-assess-item-group"></div>');

      listItem.append(selfAssessItemDiv);

      selfAssessItemDiv.append(createRow('Total Portfolio Content Score', totalScore, true));

      return listItem;
  }

  /**
   * Create a self assessment list item and add the label and score.
   *
   * @param selfAssessmentItem
   * @returns {jQuery|HTMLElement}
   */
  function createSelfAssessmentListItem(selfAssessmentItem) {
      var listItem = $('<li></li>');

      var selfAssessItemDiv = $('<div class="self-assess-item-group"></div>');

      listItem.append(selfAssessItemDiv);

      selfAssessItemDiv.append(createQuestionRow(selfAssessmentItem.question));

      selfAssessItemDiv.append(createRow('Self Assessment Descriptor', selfAssessmentItem.response, false));
      selfAssessItemDiv.append(createRow('Self Assessment Score', selfAssessmentItem.score, false));

      return listItem;
  }

  function resizeContentDiv(panelWidth) {
      var content = $('#maincontdiv');

      var containerWidth = contentWidth - panelWidth;

      setWidth(content, containerWidth + 'px');
  }

  /**
   * Toggle open the self assessment panel.
   */
  function openSelfAssessmentPanel() {
      var panel = $('#self-assessment-panel');

      resizeContentDiv(session.width);

      panel.css(session.position, '0');

      session.open = true;
      saveSession(session);
  }

  /**
   * Toggle closed the self assessment panel.
   */
  function closeSelfAssessmentPanel() {
      var panel = $('#self-assessment-panel');

      resizeContentDiv(0);

      panel.css(session.position, '-150%');

      session.open = false;
      saveSession(session);
  }
