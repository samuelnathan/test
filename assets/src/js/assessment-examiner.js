/* Original Author: David Cunningham
   For Qpercom Ltd
   Date: 25/03/2016
   Change Assessment Examiner Section
   © 2016 Qpercom Limited.  All rights reserved.
*/

$(document).ready(function(){

   includeManageEvent();
   includeSupportOption();

   // jQuery ui look and feel for examiner dropdown and password entry
   $('#new-examiner').selectmenu();
   $('#newexmnr-pass').button()
   .css({
      'font' : 'inherit',
      'color' : 'inherit',
      'text-align' : 'left',
      'outline' : 'none',
      'cursor' : 'text'
    });
     
   // Examiner Form
   $('#examiner-form').submit(function() {

        if ($('#new-examiner').length > 0 && $('#new-examiner').val() != null) {

          $('.titleerror').html("");

              if ($('#new-examiner').val().length == 0) {
                      $('#new-examiner-info').html("&#xab; Select");
                      return false;
              }
          else if ($('#newexmnr-pass').val().length == 0) {
                      $('#newexmnr-pass-info').html("&#xab; Fill in");
                      return false;
              }

        }

        return true;
   });
	
  
});