/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2018 Qpercom Limited.  All rights reserved
 */

var canSave = false;
var returnPrevPage = false;
var toSaveGroups = false;
var toRevertGroups = false;
var examNumberMode = false;
var candidateNumberMode = false;
var sortGroups;
var sortStudents;
  
jQuery(document).ready(function() {  

    // Group printing (PDF)
    if (jQuery("#groups-print").length || jQuery("#pdframe").length) {

        document.title = jQuery("#exam-name").val() + " | " 
                       + jQuery("#session-name").val() + " | " 
                       + jQuery("#session-date").val();

    // Other group section functionality
    } else {
      
     // Groups unordered list 'ul' exists
     if (jQuery("#grpsort").length) {
      
       // Group sort lists
       sortGroups = jQuery("#grpsort").sortable({
          handle: ".draghandle",
          update: function() {
            enableOptionPanel();
            shuffleInfoMessages();
          }
       });
             
       // Student sort lists
      sortStudents = jQuery(".sortable").sortable({ 
         connectWith: ".sortable",
          scroll: false,
          update: function(event, ui) {
             studentSortAction(this, ui);
          },
          receive: function(e, ui) {

              if (jQuery(this).hasClass("modslist") && ui.item.find('img.has-results').length > 0) {

                  ui.sender.sortable("cancel");
                  alert("This person has results data (or absent data) attached. You cannot remove this person " +
                        "from the original list without first confirming the deletion of their results.\n\n" +
                        "Please click on the red 'x' icon beside their name");

              }
          },
          items: ".sorting-initialize"
       });

       /*
        * Only all sort element to sort event when mouse enters sort element 
        * (bug in jQuery sorting lib., cannot handle 1000+ elements)
        */
       sortStudents.on("mouseenter", "li.eachsnew", function() {

          jQuery(this).addClass("sorting-initialize");
          sortStudents.sortable('refresh');

      });

      /* 
       * If the default 'li' element in the module list
       * = 'No students' then remove the sorting event 
       */
      jQuery("#modslist").remove("#no-students");
      
      sortStudents.sortable("refresh");

        deleteGroupEvents();
        editGroupsEvents();
        deleteStudentEvents();

    }

    // Add students event
    jQuery("#add-students").click(function() {
        jQuery("#moddiv").css({display: "block", position: "fixed"});
        jQuery("#moddiv").draggable({
            containment: "#wrapper",
            zIndex: 100,
            handle: jQuery("#drag-module-div")
        });
        
        // Get next increment number
        incrementNextExamNumber(true);
        
    });
    
    // Close event for add students window
    jQuery("#close-modules-students").click(function() {
       jQuery("#moddiv").css({display: "none", position: "static", left: "280px", top: "220px"});
    });
    
    // Exam number switch and student ID switch
    if (jQuery("#exam-number-switch").length) {
        jQuery("#exam-number-switch, #student-id-switch").button();
        jQuery("#exam-number-switch, #student-id-switch").click(function() {
            var studentSortables = jQuery(".eachs");
            var switchID = jQuery(this).attr("id");
                jQuery(this).addClass("switch-on");

            if (switchID == "exam-number-switch") {
                jQuery("#student-id-switch").removeClass("switch-on");
                jQuery.each(studentSortables, function() {
                   var studentNumber = jQuery(this).find(".student-exam-number").val();
                   jQuery(this).find(".student-identifier-display")
                        .text(studentNumber).removeClass("boldgreen").addClass("bold-dark-amber");
                });

            } else if (switchID == "student-id-switch") {
                jQuery("#exam-number-switch").removeClass("switch-on");
                jQuery.each(studentSortables, function() {
                   var studentIdentifier = jQuery(this).find(".student-identifier").val();
                   jQuery(this).find(".student-identifier-display")
                       .text(studentIdentifier).removeClass("bold-dark-amber").addClass("boldgreen");
                });            

            }

        });
    }
    
    // Candidate number switch
    if (jQuery("#candidate-number-switch").length) {
        jQuery("#candidate-number-switch, #student-id-switch").button();
        jQuery("#candidate-number-switch, #student-id-switch").click(function() {
            var studentSortables = jQuery(".eachs");
            var switchID = jQuery(this).attr("id");
                jQuery(this).addClass("switch-on");

            if (switchID == "candidate-number-switch") {
                jQuery("#student-id-switch").removeClass("switch-on");
                jQuery.each(studentSortables, function() {
                   var candidateNumber = jQuery(this).find(".student-candidate-number").val();
                   jQuery(this).find(".student-identifier-display")
                        .text(candidateNumber).removeClass("boldgreen").addClass("bold-dark-red");
                });

            } else if (switchID == "student-id-switch") {
                jQuery("#candidate-number-switch").removeClass("switch-on");
                jQuery.each(studentSortables, function() {
                   var studentIdentifier = jQuery(this).find(".student-identifier").val();
                   jQuery(this).find(".student-identifier-display")
                       .text(studentIdentifier).removeClass("bold-dark-red").addClass("boldgreen");
                });            

            }

        });
    }    
    
    
    jQuery("#search-student-list").addClass("ui-widget ui-widget-content ui-corner-all");
   
    // Module select event, displays list of students in that module
    if (jQuery("#module-select").length && jQuery("#group-filter").length) {
        
        /* Filter lists by search term */
        jQuery("#search-student-list").keyup(function() {

            var searchValue = jQuery(this).val().toLowerCase();
            var regularEx = "(?:.*?)" + (searchValue+"").replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1") + "(?:.*?)";
            
            jQuery("#modslist>li").each(function() {
                var text = jQuery(this).text().toLowerCase();
                (text.match(regularEx, "i")) ? jQuery(this).show() : jQuery(this).hide();
            });
        }).focus();
        
        jQuery("#module-select, #group-filter").selectmenu({
            width: null,
            change: function() {

            // Dropdown ID
            var dropDownID = jQuery(this).attr("id");

            // If different Group/Module selected, default index 0
            if (jQuery(this).attr("id") === "module-select") {
                jQuery("#group-filter").selectedIndex = 0;
            }

            groupFilter = jQuery("#group-filter").val();

            // Clean up
            jQuery("#modslist").find("li").remove();
            jQuery("#modslist").html("<li>Loading list. Please wait....</li>");

            jQuery.ajax({
               url: "osce/ajx_groups.php",
               type: "post",
               data: {
                  isfor: "add",
                  module: jQuery("#module-select").val(),
                  group_filter: groupFilter,
                  session_id: jQuery("#session_id").val() 
               },               
               dataType: "json"
             })
             .done(function(studentData) {
        
                jQuery("#by-sn").find("option").remove();
                jQuery("#by-fn").find("option").remove();
                jQuery("#modslist").html("");
                var students = studentData.data;
                
                if (students.length > 0) {
                       
                  // Student list
                  jQuery.each(students, function() {
                    var student = jQuery(this)[0];
                    var studentID = student.student_id;

                    if (!jQuery("#es-" + studentID).length) {

                        var studentLi = jQuery("#eachsnew-template").clone();
                            studentLi.removeAttr("id").addClass("eachsnew").attr("id", "es-" + studentID); 
                            studentLi.attr("title", studentID);
                            
                        var idSeparated = student.surname.length > 0 || student.forename.length > 0  ? " - " : "";
                        var nameSeparated = student.surname.length > 0 && student.forename.length > 0  ? ", " : "";
                        var name = student.surname + nameSeparated + student.forename + idSeparated;
           
                        var studentIDSpan = studentLi.find("#student-id-placeholder");
                        var studentLabelSpan = studentLi.find("#student-label");
                            studentLabelSpan.append(name);
                            studentIDSpan.append(studentID);
                            studentIDSpan.removeAttr("id");

                        var candidateNumber = student.candidate_number;
                        studentLi.find("input.student-candidate-number").val(candidateNumber);
                        
                        jQuery("#modslist").append(studentLi);
                        deleteStudentEvent(studentLi.find("a"));
                        
                    }
                    
                  });
                 
                  // Filter List Surnames
                  jQuery.each(studentData.filter.sn, function() {
                     var name = jQuery(this)[0];
                     var option = jQuery("<option>", {"value": "sn::" + name}).text(name);
                     jQuery("#by-sn").append(option);
                     if (groupFilter === "sn::" + name) {
                         option.selected = true;
                     }
                  });

                  // Filter List Forenames
                  jQuery.each(studentData.filter.fn, function() {
                     var name = jQuery(this)[0];
                     var option = jQuery("<option>", {"value": "fn::" + name}).text(name);
                     jQuery("#by-fn").append(option);
                     if (groupFilter === "fn::" + name) {
                        option.selected = true;
                     }
                  });

                  //sortStudents.sortable("refresh");
                  
                } else {

                    var noStudentsLi = jQuery("<li>", {"id": "no-students"});
                        noStudentsLi.html("No records found from selection above");
                        jQuery("#modslist").append(noStudentsLi);

                }
                
                // Refresh group filter
                if (dropDownID != "group-filter") {
                    jQuery("#group-filter").selectmenu("refresh", true);
                }
             })
             .fail(function(jqXHR) {
               
               var message = httpStatusCodeHandler(
                     jqXHR.status,
                     "Failed to retrieve list in selected group(s).\n"
               );
 
               alert(message);
                
             });
           }  
        });
        
        // Style exam number prefix if not set to "auto"
        if (jQuery("#exam-number-prefix").val() != "auto") {
          jQuery("#exam-number-prefix").selectmenu({
             width: null,
             change: function() {
                // true param = reset postfix, begin at 1 
                incrementNextExamNumber(true);
             }
          });
          
          // Exam number postfix event
          jQuery("#exam-number-postfix").addClass("ui-widget ui-widget-content ui-corner-all");
          jQuery("#exam-number-postfix").focusout(function() {

              var prefix = jQuery("#exam-number-prefix").val();
              var postfix = jQuery(this).val();
              var examNumber = prefix + postfix;
              var regEx = /^([0-9]+)$/i;
              
              if (!regEx.test(postfix) || isExamNumberTaken(examNumber)) {
                incrementNextExamNumber(true);
              }
          });
          
          examNumberMode = true;
        }
        
        if (jQuery("#candidate-number-mode").length) {
          candidateNumberMode = true;  
        }
        
    }

    // Cancel changes event
    jQuery("#cancel_changes").click(function() {
        var confirmText = "Are you sure you want to reset all changes? \n\n'OK' = Yes \n'Cancel' = No";
        if (confirm(confirmText) === true) {
            toRevertGroups = true;
            refreshPage(false);
        }
    });

    // Return to session list event
    jQuery(".return").click(function() {
        var confirmText = "Are you sure you want to return to previous "
                        + "page without saving changes? \n\n'OK' = Yes \n'Cancel' = No";
        if (!canSave || confirm(confirmText) === true) {
            returnPrevPage = true;
            returnURL = "manage.php?";
            var urlParams = {"page" : "sessions_osce", "exam_id" : jQuery("#exam_id").val()};
                                                  
            if (jQuery("#show_session").val().length > 0) {
                jQuery.extend(urlParams, {"show_session": jQuery("#show_session").val()});
                returnURL += jQuery.param(urlParams);
            } else {
                returnURL += jQuery.param(urlParams) + "#X" + jQuery("#session_id").val();
            }
            parent.location = returnURL;
            
        }
    });

    // Previous and next records navigation
    jQuery('.previous, .next').click(function(event) {

        var confirmText = "Are you sure you want to move away from this "
                        + "page without saving changes? \n\n'OK' = Yes \n'Cancel' = No";
        if (!canSave || confirm(confirmText) === true) {
            returnPrevPage = true;    
            parent.location = jQuery("#" + jQuery(this).attr('id') + '-url').val();
        }
        
    });
 
    // Print groups list event
    jQuery("#print_lists").click(function() {
        var session = jQuery("#session_id").val();
        var winUrl = "manage.php?page=preview_groups" + "&session=" + session;
        var winName = "print_osce_test_day_groups" +  session;
        var winPop = window.open(winUrl, winName);
        winPop.focus();
    });

    /**
     * Add new group event (jQuery)
     */ 
    jQuery("#add-group-button").click(function() {
 
        // No name entered then inform user
        if (jQuery("#group-name").val().length === 0) {
            alert("Please specify a group name");

        // We've got a name so continue to create new group element
        } else {

         // Look for a new ID that's not already used
         var newIDNum = 1;
         while (true) {
           // already exists
           if (jQuery("#new" + newIDNum).length) {
             newIDNum++;

           // Got a new ID
           } else {
             break;
           }
         }

        var newID = "new" + newIDNum;
        var newLi = jQuery("#eachglist-template").clone();
        newLi.removeAttr("id").addClass("eachglist");       
        newLi.find(".group-id").attr("id", newID).val(newID);
        newLi.find(".group-title-input").val(jQuery("#group-name").val())
                                        .attr("name", "name_"+newID);
        newLi.find("#inner-ul-template").addClass("eachslist sortable")
                                        .removeAttr("id");
       
        jQuery("#group-name").val("");
        jQuery("#addgroupli").before(newLi);
        
        // Add new group and list to sorting
        sortGroups.sortable("refresh");         
        jQuery(".sortable").sortable({
           connectWith: ".sortable",
           update: function(event, ui) { 
               studentSortAction(this, ui); 
           }
        });
        
        jQuery(".sortable").sortable("refresh");
        deleteGroupEvent(newLi.find(".deletegroup"));
        editGroupEvent(newLi.find(".editgroup"));
        reCountLists();
        enableOptionPanel();
        shuffleInfoMessages();
        
    }
  });
  
   jQuery('#module-select-menu, #group-filter-menu, #exam-number-prefix-menu')
           .attr('data-scroll-scope', 'force');
  
   reCountLists();
   window.onbeforeunload = leavePageCheck;
   
 }

});

jQuery(document).ready(function() {

    showMenu();

});

/**
 * Do a recount of the number of students in each list
 */ 
function reCountLists() {
    
    // Abort if there are no lists
    if(!jQuery("#grpsort").length){
        return;
    }
    
    var max = 0;
    var studentLists = jQuery(".eachslist");
    /* Recount lists */
    jQuery.each(studentLists, function() {
        var listObject = jQuery(this);
        var liElements = listObject.find("li");
        var groupCount = listObject.parent("li").find("span.groupcount");
        groupCount.text(liElements.length);
        if (liElements.length > max) {
            max = liElements.length;
        }
    });

    /* Resize Boxes 20px height */
    var maxheight = max * 23 + 40;
    jQuery.each(studentLists, function() {
        jQuery(this).css("height", maxheight);
    });

    jQuery(".eachglist2").css("height", maxheight + 76);

}

// Add Edit Events
function editGroupsEvents() {
    var buttons = jQuery("#grpsort").find(".editgroup");

    jQuery.each(buttons, function() {
        editGroupEvent(jQuery(this));
    });
}

// Add Edit Event
function editGroupEvent(button) {

    button.click(function() {   
        sortGroups.sortable("disable");

        var parentLi = jQuery(this).closest("li.eachglist");       
            parentLi.find("div.group-options-div").css("visibility", "hidden");
        
        var dragHandle = parentLi.children("div[class^=draghandle]");
            dragHandle.attr("class", "nondrag").attr("title", "");
        
        var groupEditDiv = parentLi.find("div.group-edit-div");
        
        var input = groupEditDiv.children("input.group-title-input");
            input.prop("readonly", false).attr("class", "group-title-input2");
        var originalTitle = input.val();

        var cancel = jQuery("<a>", {"id": "edit-cancel", "title": "cancel", "class": "fl", "href": "javascript:void(0)"});
        var save = jQuery("<a>", {"id": "edit_save", "title": "save", "class": "fl", "href": "javascript:void(0)"});
            cancel.append(jQuery("<img>", {"src": "assets/images/cancel.png", "alt": "cancel", "class": "cancel-group"}));
            save.append(jQuery("<img>", {"src": "assets/images/save.png", "alt": "save", "class": "update-group"}));

        input.after(cancel);
        cancel.after(save);

        cancel.click(function() {
            input.val(originalTitle);
            parentLi.find("div.group-options-div").css("visibility", "visible");
            parentLi.find("div[class=nondrag]")
                    .attr("class", "draghandle")
                    .attr("title", "Drag to order group");
            input.prop("readonly", true).attr("class", "group-title-input");

            save.remove();
            jQuery(this).remove();
            sortGroups.sortable("enable");
        });

        save.click(function() {
            if (input.val().length > 0) {
                parentLi.find("div.group-options-div").css("visibility", "visible");
                parentLi.find("div[class=nondrag]")
                        .attr("class", "draghandle")
                        .attr("title", "Drag to order Group");
                input.prop("readonly", true).attr("class", "group-title-input");

                cancel.remove();
                jQuery(this).remove();
                sortGroups.sortable("enable");
                enableOptionPanel();
                shuffleInfoMessages();
            } else {
                alert("Please complete group title field");
            }
        });
    });
}

// Add delete group events
function deleteGroupEvents() {
    var buttons = jQuery("#grpsort").find(".deletegroup");
    jQuery.each(buttons, function() {
        deleteGroupEvent(jQuery(this));
    });
}

function deleteGroupEvent(button) {
    
    button.click(function() {
        var parentLi = jQuery(this).closest("li");
        var message = "Are you sure you want to remove this Group?\n"
                    + "The Group will not be deleted from database "
                    + "until you click the 'Save' button at the top of the page."
        var yesno = confirm(message);
        if (yesno) {
            parentLi.remove();
            enableOptionPanel();
            shuffleInfoMessages();
        }
    });
    
}

// Delete student events
function deleteStudentEvents() {
    var buttons = jQuery("#contentdiv").find("a.removestudent");
    jQuery.each(buttons, function() {
        deleteStudentEvent(jQuery(this));
    });
}

// Delete student event
function deleteStudentEvent(button) {
    
    button.click(function() {
        
      // Don't delete from ungrouped student list  
      if (jQuery(this).parent('li').hasClass('eachsnew')) {
        
        return false;
        
      }
      
      var parentUL = jQuery(this).closest('ul');
      var parentLI = jQuery(this).closest('li');
      var results = parentLI.find('img.has-results');
      var studentID = parentLI.find('input.student-identifier').val();
      if (parentUL == null || parentUL.hasClass('blank-list')) {
         return;
      }
     
     if (results.length > 0) {
       jQuery('#dialog-action-confirm').dialog({
          autoOpen: true,
          height: 400,
          width: 750,
          modal: true,
          "open": function() {
            
            jQuery(this).off('submit').on('submit', function () {
                return authenticateAndDelete(button, studentID);
            });
             
            var nameSeperated = parentLI.find('.student-label').text().length > 0 ? " - " : "";  
            jQuery('#student-name').text(
               parentLI.find('.student-label').text() + nameSeperated +
               parentLI.find('.student-identifier-display').text()
            );
            jQuery('#groups-password-field').val("");
            jQuery('#password-message').text("");
            loadExamResults(studentID);

            jQuery('#groups-password-field').keyup(function(event) {
                if (event.which == 13) return; // Except enter key
                jQuery('#password-message').text("");
            });
             
          },
          buttons: {
            "Cancel": function() {
              jQuery(this).dialog('close');
            },
             "Delete": function() {      
                return authenticateAndDelete(button, studentID);                             
            },           
          }
       });
    } else {
       deleteStudentAction(button);
    }
       
    });
    
}

// Authenticate and Delete
function authenticateAndDelete(button, studentID) {
    
        // Check for complete password
        if (jQuery('#groups-password-field').val().length == 0) {
            jQuery('#password-message').text('Please complete password field');
            jQuery('#groups-password-field').focus();
            return false;
        }

        // Authenticate
        jQuery.ajax({
            url: "osce/ajx_groups.php",
            type: "post",
            data: {
               isfor: "authenticate",
               password: jQuery('#groups-password-field').val() 
            }
         })
         .done(function() {

            jQuery('#dialog-action-confirm').dialog('close');
            jQuery('#results-to-delete').append(
              jQuery('<input>', { 
                  'type': 'hidden', 
                  'name': 'remove_results[]', 
                  'value': studentID
              })
            );
            deleteStudentAction(button);

          })
         .fail(function(jqXHR) {

             if (jqXHR.status == 401) {
                jQuery('#password-message').html(
                    "Password incorrect, please try again"
                );
             } else {

               jQuery('#password-message').html(
                httpStatusCodeHandler(
                    jqXHR.status,
                    "Failed to authenticate with your password.\n"
                 )
               );

             }
        });
        
    return false;
}

// Load exam results
function loadExamResults(studentID) {
     jQuery.ajax({
        url: "osce/ajx_groups.php",
        type: "post",
        data: {
           isfor: "exam_results",
           student_id: studentID,
           session_id: jQuery("#session_id").val() 
        },
        dataType: "json"
     })
     .done(function(results) {
         
         jQuery('.group-results-row').remove();
         
         if (results.length == 0) {
             jQuery('#results-status-row').find('td').html(
                 'Current status: no results found for this person'
             );
             return false; 
         } else {
           jQuery('#results-status-row').hide();  
         }
         
         jQuery.each(results, function() {
             var result = jQuery(this)[0];
             var row = jQuery('<tr>', { 'class': 'group-results-row'}).append(
                jQuery("<td>", {'class': 'tac', 'text': result.number}),
                jQuery("<td>", {'text': result.name}),
                jQuery("<td>", {'text': result.examiner_id}),
                jQuery("<td>", {'text': result.examiner_name})
             );
     
            // We've got an absent
            if (result.absent == 1) {
                row.append( 
                  jQuery("<td>", {'text': 'ABSENT', 'colspan': 2, 'class': 'red c'})
                );
            } else {
                row.append(
                   jQuery("<td>", {'text': (result.complete ? 'yes' : 'no'), 'class': 'tac'}),
                   jQuery("<td>", {'text': result.score})
                );
            }
           
           // Append row to table
           jQuery('#loading-results').append(row);
         });
         
      })
     .fail(function(jqXHR) {
 
         var message = httpStatusCodeHandler(
           jqXHR.status,
           "Failed to retrieve results.\n"
         );
 
         jQuery('.group-results-row').remove();
         jQuery('#results-status-row').find('td').html(message);

      });
}

// Delete action for student
function deleteStudentAction(button) {
       
    button.parent('li').remove();
    sortStudents.sortable('refresh');
    reCountLists();
    enableOptionPanel();
    shuffleInfoMessages();
    
}

// Validate form
function ValidateForm(form) {
    if (jQuery(form).attr('name') === "manage_grps") {
        var text = "Would you like to continue with saving the groups " 
                 + "overwriting any previous groups specified";
        var response = confirm(text);
        
        if (response) {
            toSaveGroups = true;
        }
        
        return response;
    }
}

// Enable options panel
function enableOptionPanel() {
    jQuery("#cancel_changes").prop("disabled", false);
    jQuery("#save").prop("disabled", false);
    jQuery("#print_lists").prop("disabled", true);
    canSave = true;
}

// Shuffle Info Messages
function shuffleInfoMessages() {
    if (jQuery("#saveupwarn") && !jQuery("#saveupwarn").hasClass("show")) {
        jQuery("#saveupwarn").addClass("show");
    }
    if (jQuery("#saved") && jQuery("#saved").hasClass("show")) {
        jQuery("#saved").removeClass("show");
    }
}

/*
 * Student item sort action
 */
function studentSortAction(list, ui) {
   
  // Take action when a sort affects this list
  if (list === ui.item.parent()[0]) {

    var item = ui.item;
    var parent = item.parent("ul");

    // Parent 'UL' exists
    if (parent.length) {

      // Get parent 'li' list element
      var parentli = parent.parent("li");
      
      /*
       * If an item is MOVED BACK to the Module List then
       * remove it from the submission data
       */
      if (parent.hasClass("modslist")) {
         
        // Grap student ID value set it as ID to display
        var studentIDValue = item.find("input.student-identifier").val();
        item.find("span.student-identifier-display")
                .text(studentIDValue)
                .removeClass("bold-dark-amber bold-dark-red");
        
        // Strip grouping hidden variables
        item.removeClass("eachs").addClass("eachsnew");
        item.find("input.student-identifier").remove();
        item.find("input.student-exam-number").remove();

        // Switch icon that represents remove rather than add
        item.find("img.remove-student").attr("src", "assets/images/person-icon-add.png");
        
      /*
       * New student ADDED to Sort List from Module
       * List or blank records list
       */
      } else if (parent.hasClass("eachslist") && item.hasClass("eachsnew")) {
        
        var groupID = parentli.children(".group-id")[0].value;
        var fieldID = item.attr("id");
        var isBlank = false;

       /* 
        * When the BLANK record is added to the student list, 
        * add a new one to the blank record list
        */
       if (fieldID.indexOf("es-:bs:") >= 0) {
         var splitID = fieldID.split(":");
         var copy = item.clone(true, true);
         isBlank = true;
        
         // New ID
         var newID = "es-:bs:" + ((parseInt(splitID[2], 10)) + 1);
         copy.attr("id", newID);

         // Add new blank record to blank record list
         jQuery("#blanklist").children("li").remove();
         jQuery("#blanklist").append(copy);

         // Enable deletion for the blank record added to list
         item.find("a").addClass("removestudent");
         var itemID = item.attr("id");
         var deleteButton = jQuery("#" + itemID).find("a.removestudent");
         deleteStudentEvent(deleteButton);      

       }
       
       // Switch icon to remove rather than add
       item.find("img.remove-student").attr("src", "assets/images/person-icon-remove.png");
       item.css("class", "eachs");
       
       var studentID = fieldID.substring(3, fieldID.length);
       var hiddenField = jQuery("<input>", {
              "type": "hidden",
              "class": "student-identifier",
              "name": "groups_students[" + groupID + "][]",
              "value": studentID
       });
       
       // Exam number mode
       if (isBlank) {
         incrementNextExamNumber();  
       } else if (examNumberMode) {
         jQuery("#exam-number-postfix").blur();
         var currentPrefix = jQuery("#exam-number-prefix").val();
         var currentPostfix = jQuery("#exam-number-postfix").val();
         var newExamNumber = currentPrefix + currentPostfix;
         incrementNextExamNumber();
      
         // Exam number hidden field
         var hiddenExamNumField = jQuery("<input>", {
              "type": "hidden",
              "class": "student-exam-number",
              "name": "student_exam_number[" + studentID + "]",
              "value": newExamNumber
         });
       
         item.find(".student-identifier-display")
                 .text(newExamNumber)
                 .addClass("bold-dark-amber");
         item.append(hiddenExamNumField);
       
       // Candidate number mode
       } else if(candidateNumberMode) {
         var candidateNumber = item.find("input.student-candidate-number").val();
         item.find(".student-identifier-display")
                 .text(candidateNumber)
                 .addClass("bold-dark-red");            
       }
                
       item.append(hiddenField);
       item.removeClass("eachsnew").addClass("eachs");

     /*
      * Item (student) moved from 1 group to another,
      * then change items group ID
      */
     } else if (parent.hasClass("eachslist") && item.hasClass("eachs")) {
        var groupID = parentli.children(".group-id")[0].value;
        var hiddenField = item.find("input[name^=groups_students]");
        hiddenField.attr("name", "groups_students[" + groupID + "][]");
     }
    
    enableOptionPanel();
    shuffleInfoMessages();
    reCountLists();
    
  }  
 }
}

/**
 * Increment next available exam number per prefix
 * @param bool  reset postfix
 */ 
function incrementNextExamNumber(reset) {
    
    var postfix = (reset) ? 0 : jQuery("#exam-number-postfix").val();
    postfix++
    var prefix = jQuery("#exam-number-prefix").val();
    var newExamNumber = prefix + postfix;
    var sessionNumbersTaken = jQuery(".student-exam-number").map(function() {
      return jQuery(this).val();
    }).get();
   
    var numbersTaken = examNumbersTaken.concat(sessionNumbersTaken);

    // Find next available exam number
    while (true) {
      
      // This exam number has been taken
      if (jQuery.inArray(newExamNumber, numbersTaken) > -1) {
        postfix++;
        newExamNumber = prefix + postfix;
      
      // This exam number is available then lets be on our way
      } else {
        break;
      }
     
    }
    
    // Set next postfix
    jQuery("#exam-number-postfix").val(postfix);
}

/**
 * Is the exam number taken
 * @param string examNumber  exam number to check
 * @returns bool true|false
 */
function isExamNumberTaken(examNumber) {
    var sessionNumbersTaken = jQuery(".student-exam-number").map(function() {
      return jQuery(this).val();
    }).get();
    var numbersTaken = examNumbersTaken.concat(sessionNumbersTaken);
    return (jQuery.inArray(examNumber, numbersTaken) > -1);   
}

// Leave page check
function leavePageCheck() {
    if (!returnPrevPage && !toSaveGroups && !toRevertGroups && canSave) {
        return "There are changes on this page that may need to be saved";
    }
}
