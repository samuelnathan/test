/* Original Author: David Cunningham
 * For Qpercom Ltd
 * © 2016 Qpercom Limited.  All rights reserved.
 */
// Add support events
function includeSupportOption() {
    if ($('#support')) {
       $('#support').click(function(event) {
          event.preventDefault();
          $('.failure, .success').remove();
          $(this).hide();
          $('#supportfs').css('display', 'inline');
       });
       
       // Add cancel event to close assistant call box
       cancelSupportRequest();
       
       // Add submit event
       submitSupportRequest();
    }
}

// Cancel support event
function cancelSupportRequest() {
   
   if ($('#cancel-support-button')) {
     $('#cancel-support-button').click(function(event) {
        event.preventDefault();
        $('#support').css('display', 'inline');
        $('#supportfs').hide();
     });
   }
   
}

// Submit support event
function submitSupportRequest() {
  
 // Submit button exists, then add 'click' event
 if ($('#submit-support-button')) {
    $('#submit-support-button').click(function(event) {
      event.preventDefault();
      
      // Abort if there isn't assistants selected
      var checkCount = $('#support-form').find("input[id^=contacts]:checked");
      if (checkCount.length === 0) {
        alert("Please select at least 1 person from the list");
        return false;
      }
      
      var statusDiv = $('<div>', {'class': 'success'});
      
      $.ajax({
         url: 'assess/requestAssistance.php',
         type: 'post',
         data: $('form#support-form').serialize(),
         dataType: 'json',
         beforeSend: function() {
           $('#support').css('display', 'inline');
           $('#supportfs').hide();
         }
       })
       .done(function() {
          statusDiv.html('Request for assistance message sent');  
       })
       .fail(function(jqXHR) {
          
          statusDiv.attr('class', 'failure');
          var error = "";
          switch (jqXHR.status) {
             case 404:
             case 500:
                error = "An internal server error has occurred";
                break;
             case 501:
                error = "Service unavailable, please try again later";
                break;
             case 503:
                error = "There are no credits available to use this service";
                break;
             case 400:
                error = "1 or more of the required parameters are missing "
                      + "or are invalid (e.g. mobile number)";
                break;
             case 403:
                error = "Sorry, your session may have expired "
                      + "or you do not have access to this feature";
                break;
             default:
                error = "An unspecified error has occurred. "
                      + "Please contact your system administrator";
                error += "\n [HTTP Code: " + jqXHR.status + "]";
          }
          statusDiv.html(error);
            
       })
       .always(function() {
          $('#maincontdiv').before(statusDiv);
       });
    });
  }

}

// Validation before request
function showRequest(formData, jqForm, options) {
    var checkcnt = jqForm.find("input[id^=contacts]:checked").length;
    if (checkcnt > 0) {
        $('#support').css('display', 'inline');
        $('#supportfs').hide();
        return true;
    } else {
        alert("Please select at least 1 person from the list");
        return false;
    }
}

// Include Manage Event
function includeManageEvent() {
    if ($('.management_sel').length > 0) {
        $('.management_sel').click(function() {
        var message = "Are you sure you would like to move away from "
                    + "assessment tool to the management system?";
           
        if (confirm(message)) {
                window.location.href = './manage.php?page=main';
        }
        });
    }
}

// Is offline alert message
function isOfflineAlert() {
    if (Offline.state == "down") {
       alert("Internet/Network connection is down, please try again.\n\n" +
             "If unsuccessful, please move to paper backup forms and if possible inform support.");
       return true;
    } else {
       return false;
    }
}

// Percent calculation
function getPercent(val1, val2) {
    if (val1 === 0 || val2 === 0) {
        return "0%";
    }
    return (Math.round((val1 / val2) * (100)) + "%");
}

