/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2018, Qpercom Limited
 */

 var clearFormatting = false;
 var competencyDialog;
 jQuery(document).ready(function() {
    /* Load TinyMCE */
    if (typeof (tinymce) !== "undefined") {


      tinymce.init({
         setup: function() {
           // Remove wait indicator if visible
           if (jQuery('#load-editor').length) {
               jQuery('#load-editor').remove();
           }
         },
         theme: "modern",
         width: 1050,
         height: 180,
         indentation : '8pt',
         entity_encoding: "raw",
         contextmenu_never_use_native: true,
         elementpath: false,
         image_advtab: true,
         paste_as_text: true,
         button_tile_map: true,
         menubar: false,
         default_link_target: "_blank",
         auto_focus: true,
         content_css : "assets/css/tinymce-content-style.css",
         moxiemanager_image_settings: {
     	  moxiemanager_title: 'Image Folder'
         },
         external_plugins: {
          'moxiemanager': '../../../vendor/moxiemanager/moxiemanager/plugin.min.js'
         },
         plugins: [
            "advlist autolink lists link image charmap preview hr anchor pagebreak",
            "searchreplace visualblocks visualchars fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "paste textcolor colorpicker textpattern imagetools"
         ],
         toolbar1: "undo redo | styleselect | bold italic | " +
                   "alignleft aligncenter alignright alignjustify | " +
                   "removeformat | bullist numlist outdent indent | " +
                   "link image | forecolor backcolor | table | preview",
         contextmenu: "copy paste removeformat | link image inserttable | cell row column deletetable",
         style_formats_merge: true,
         style_formats: [
           {
             'title': 'Dash (Bullet) List',
             'selector': "ul",
             'classes': "bulletlist-hyphen"
           }
         ]

     });

    }

   /**
    * Clone Dialog
    */
   var cloneDialog = jQuery('#clone-selector').dialog({
     autoOpen: false,
     height: 280,
     width: 500,
     modal: true,
     buttons: {
       'Cancel': function() {

           cloneDialog.dialog('close');

       },
       'Clone': function() {

         if (jQuery('#new-name').val().length === 0) {

             jQuery('#new-name').addClass('field-required')
             .keydown(function() {

                 jQuery(this).removeClass('field-required')

             });

             return;

         }

         jQuery('#clone-form').submit();

       }
     }
   });

   jQuery('#clone-options').on('click', function() {
       cloneDialog.dialog('open');
   });

   // Cloned scoresheet notification
   var tooltipSuccess = jQuery('#form-name-field').tooltip({
        placement: "left",
       position: { my: "center", at: "center top-15" },
       tooltipClass: (jQuery('#form-name-field').data('cloned') == "yes" ?
       "success" : "failed") + "-tooltip"
   });

   setTimeout(function () {
     tooltipSuccess.mouseover();
   }, 300);

   setTimeout(function () {
     tooltipSuccess.mouseout();
   }, 7000);

   jQuery('form#addoptions_formtop').parents('.col-lg-5.col-sm-6.col-12').removeClass('col-lg-5,col-sm-6,col-12').addClass('col-auto');

   // Tag management, events auto-suggest
   jQuery('form#addoptions_formtop').on('keydown blur', 'input#new-tag-input', function(e) {

       var stringValue = jQuery.trim(jQuery(this).val());
       allowedEvent = (e.type == "focusout" || e.which == 13);
       if (stringValue.length > 0 && allowedEvent) {

         jQuery('#tagging-div').append(
           jQuery('<span>', {
             'class': 'form-tag-linked',
             'text': stringValue
                    .trim()
                    .toLowerCase()
                    .replace(/[_\s]+/g, '-')
                    .replace(/[^-a-z0-9\s]/g, '')
           })
        );

        jQuery(this).val('');

      }

   });

   // Remove tag event
   jQuery('form#addoptions_formtop').on('click', 'span.form-tag-linked', function() {
       jQuery(this).remove();
   });

     /* Action Type Drop-Down Event */
     if (jQuery('#action-type').length) {
         jQuery('#action-type').on('change', function() {
             if (jQuery(this).val() === '1') {
                 jQuery('#add_previous_form').attr('class', 'optionshide');
                 jQuery('#comp_add_form').attr('class', 'optionsdisplay');
             } else {
                 jQuery('#add_previous_form').attr('class', 'optionsdisplay');
                 jQuery('#comp_add_form').attr('class', 'optionshide');
             }
         });
     }

     /* Competence Type Drop-Down jQuery Event */
     if (jQuery("#competence-type").length) {
       jQuery("#competence-type").change(function() {
         var selectedOption = jQuery(':selected', this);
         var selectedOptGroup = selectedOption.parent('optgroup').attr('label');
             selectedOptGroup = selectedOptGroup.toLowerCase();

         // Iterate through feedback required options
         jQuery('#feedback-required option').each(function() {

          var option = jQuery(this);

          // If scale type
          if (selectedOptGroup === "scale") {

            if (option.hasClass('selected')) {
                option.prop('selected', true);
            }

            if (option.hasClass('other-type')) {
                option.addClass('hide');
            } else {
                option.removeClass('hide');
            }


          // If standard type
          } else {

            if (option.hasClass('grs-type')) {
                option.addClass('hide');
            } else if (option.hasClass('other-type')) {
                option.removeClass('hide');
            }
            option.prop('selected', false);

          }

        });

       });
     }

     //add previous form's section & items
     if (jQuery('#add_previous_form').length) {
         var url = "forms/ajx_forms.php?";
         //Term Selection
         jQuery('#strm').on('change', function() {
             var random = Math.random();
             jQuery('#add-previous-items').prop('disabled', true);
             jQuery('#previous-form-id').html('').css('visibility', 'hidden');
             jQuery('#form-section-list').html('Please choose ' + jQuery('#translation-form').val());
             jQuery('#fstationtd').attr('class', 'waittd');

             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random, isfor: 'dstations', formid: jQuery('#current-form-id').val(), sterm: jQuery(this).val(), fdept: jQuery('#dept_idtop').val()
                }
             })
             .done(function(html){
                jQuery('#fstationtd').removeClass('waittd');
                     jQuery('#previous-form-id').html(html).css('visibility', 'visible');
                     jQuery("#previous-form-id").prop('selectedIndex', 0);
             })
             .fail(function(jqXHR) {
                alert("The request failed, please make selection again");
             });
         });

         //form Selection
         jQuery('#previous-form-id').on('change', function() {
             if (jQuery('#previous-form-id').val().length === 0 || jQuery('#previous-form-id').val() === '--') {
                 jQuery('#form-section-list').html('Please choose ' + jQuery('#translation-form').val());
                 jQuery('#add-previous-items').prop('disabled', true);
                 return;
             }

             jQuery('#form-section-list').empty().addClass('waitv5');
             var random = Math.random();
             jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: 'comps',
                    previous_form_id: jQuery('#previous-form-id').val(),
                    current_form_id: jQuery('#current-form-id').val()
                }
             })
             .done(function(html){
                jQuery('#form-section-list').removeClass('waitv5');
                jQuery('#form-section-list').html(html);

                if (html.indexOf('No Competencies') > -1) {
                    jQuery('#add-previous-items').prop('disabled', true);
                } else {
                    jQuery('#add-previous-items').prop('disabled', false);
                    jQuery('#form-section-list').find('input[name^=prevsection]').on('click', function() {
                        var cbchecked = false;
                        var celes = jQuery('#form-section-list').find('input[name^=prevsection]');
                        jQuery.each(celes, function(i, cb) {
                            if (jQuery(cb).prop('checked')) {
                                cbchecked = true;
                            }
                        });
                        if (cbchecked === false) {
                            jQuery('#add-previous-items').prop('disabled', true);
                        } else {
                            jQuery('#add-previous-items').prop('disabled', false);
                        }
                    });
                }
             })
             .fail(function(jqXHR) {
                alert("The request failed, please make selection again");
             });
         });
     }

     // Assessment Form Exists
     if (jQuery('#assessform_sections_form').length) {

       // Standard Checkboxes
       var bs = jQuery('#assessform_sections_form').find('a[class=standard]');

       if (bs.length > 0) {
           bs.on('click', function(){
            addEditEvent(this);
           })
        }

       // Scale Checkboxes
       var bss = jQuery('#assessform_sections_form').find('a[class=scale]');

       if (bss.length > 0) {
           bss.on('click', function(){
            currentScaleEditEvent(jQuery(this));
           })
       }

       // Section Edit Buttons
       var sectionEditButtons = jQuery('#assessform_sections_form').find('input[id^=editsection]');
       if (sectionEditButtons.length > 0) {
           sectionEditButtons.on('click', function(){
            addSectionEditEvent(jQuery(this));
           })
       }

       // Section Delete Buttons
       var sectionDeleteButtons = jQuery('#assessform_sections_form').find('input[id^=deletesection]');
       if (sectionDeleteButtons.length > 0) {
           sectionDeleteButtons.on('click', function(){
            addSectionDeleteEvent(jQuery(this));
           })
        }

       // Form Preview Buttons
       jQuery('.prevb').click(function() {
             var location = jQuery(this).attr('id').split('-')[1];
             var url = "manage.php?page=previewform_osce";

             if (jQuery('#sessionid_' + location).length && jQuery('#stationid_' + location).length) {
                 url += "&frm=" + jQuery('#sessionid_' + location).val()
                     + "&station_id=" + jQuery('#stationid_' + location).val();
             }
             url += "&form_id=" + jQuery('#formid_' + location).val();
             var winpop = window.open(url, '_form'+jQuery('#formid_' + location).val());
             winpop.focus();
     });


       // Add / Event for Adding Scale Item to Competence
       newScaleAddEvent();
     }

     if (jQuery('#cformat').length) {
         jQuery('#cformat').on('click', function() {
             clearFormatting = true;
         });
     }

     /**
      * Row hightlighting for assessment form builder
      */
     jQuery('#section-items').on('change', '.item-cb', function() {
         if (jQuery(this).prop('checked')) {
            jQuery(this).closest('tr').addClass('row-highlight');
         } else {
            jQuery(this).closest('tr').removeClass('row-highlight');
         }
     });

     /**
      * Feedback fields hide/display 'Add Scoresheet' section
      */
     jQuery('input#section-feedback').click(function() {

       jQuery('#feedback-required, #internal-feedback-only')
       .prop('disabled', !jQuery(this).prop('checked'));

     });

    /* Edit/Update Form details Form name, notes etc */
     if (jQuery('#editform_top').length) {

         jQuery('#editform_top').on('click', function() {

             if (jQuery(this).val() === 'edit') {

                 /* Station Name Field */
                 jQuery('#formname').removeAttr('readonly');
                 jQuery('#formname').removeClass('readonly');

                 /* Station Description Field */
                 if (jQuery('#formdesc').length) {

                     jQuery('#formdesc').removeAttr('readonly');
                     jQuery('#formdesc').removeClass('readonly');
                     jQuery('#formdesc').removeClass('empty');
                     jQuery('#formdesc').attr('rows', 10);

                 }

                 /* Station Complete Field */
                 jQuery('#input[name=form_finished]').removeAttr('disabled');

                 /* Change Name to Update */
                 jQuery(this).val('update');

                 /* Hide Other Buttons */
                 jQuery('#setback-top, #preview-top, #cformat, #clone-options').hide().prop('disabled', true);

                 jQuery('span.form-tag').attr('class', 'form-tag-linked');

                 jQuery('#new-tag').show().combobox({
                    select: function (event, ui) {

                      setTimeout(function() {
                        jQuery('input#new-tag-input').trigger('blur');
                      }, 100);

                 }});

                 /* Border Style of FieldSets */
                 jQuery('.inner-fs').css('border', '1px solid #ff0000');

                 /* Cancel Button */
                 var cancelsbu = jQuery('<input>', {'type': 'button', 'id': 'cancelsbu', 'value': 'cancel', class: 'btn-sm btn-outline-danger btn'});
                 jQuery(this).before(cancelsbu);

                 cancelsbu.on('click', function() {

                    window.location.hash = '';
                    window.location.reload();

                 });

             } else if (jQuery(this).val() === 'update') {

                 if (jQuery('#formname').length && jQuery('#formname').val().length === 0) {

                     alert("Please specify a name");
                     return;

                 }

                 var tags = jQuery('span.form-tag-linked').map(function() {

                    return jQuery(this).text();

                 }).get();

                 var url = 'forms/ajx_update_form_info.php?';
                 var random = Math.random();

                 jQuery.ajax({
                    url: url,
                    type: "post",
                    data: {
                        sid: random,
                        isfor: 'update_form_details',
                        formid: jQuery('#formid_top').val(),
                        complete: jQuery('#addoptions_formtop').find('input[name=form_finished]:checked').val(),
                        formname: jQuery('#formname').val(),
                        formdesc: ((jQuery('#formdesc').length) ? jQuery('#formdesc').val() : ''),
                        tags: tags.length > 0 ? tags.join(',') : ''
                    }
                 })
                 .done(function(html){

                     /* Deal with any error messages returned from script and output appropriate message to user. */

                      // User does not exist
                      if (html.indexOf('USER_NON_EXIST') > -1) {

                        alert('User no longer exists in system, reloading web page');
                        window.location.hash = '';
                        window.location.reload();

                      }

                      // Form does not exist
                      else if (html.indexOf('FORM_NON_EXIST') > -1) {

                        alert('Object no longer exists in system, reloading web page');
                        window.location.hash = '';
                        window.location.reload();

                      } else {

                        window.location.hash = '';
                        window.location.reload();

                      }
                 })
                 .fail(function(jqXHR) {
                    window.location.hash = '';
                    window.location.reload();
                    alert('The request failed, please try again');
                 });

             }

         });

     }

     showMenu();
     performScroll();

 });
