/**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2017, Qpercom Limited
  */

 $(document).ready(function() {
    
   // Text scaling cookie management (requires plugin: jquery-cookie)
   if (!!$.cookie('assess-scale-accessibility')) {

       $('#assessment-body').parents('html').css(
           'font-size',
           $.cookie('assess-scale-accessibility') + 'px'
       );

   } else {
       
       var defaultScale = $('#scale-accessibility').length ?
           $('#scale-accessibility').val() : 12;
       
       $.cookie('assess-scale-accessibility', defaultScale);
       $('#assessment-body').parents('html').css('font-size', defaultScale + 'px');

   }
   
   // If we arrive on the config screen, select scale from dropdown
   if ($('#scale-accessibility').length) {
       
       $('#scale-accessibility').val(
          $.cookie('assess-scale-accessibility')
       );
       
   }
   
 });
