/**
 * @author David Cunningham <david.cunningham@qpercom.ie>
 * For Qpercom Ltd
 * @copyright Copyright (c) 2017, Qpercom Limited
 */

// Domready event
jQuery(document).ready(function() {

    if (jQuery('#admin_list_form').length) {
      if (jQuery('#add').length) {
         newAdminAddEvent();
      }

      if (jQuery('#count').length && jQuery('#count').val() > 0) {
        if (jQuery('#check-all').length) {
          addCheckboxEvents("admin");
        }
        adminsEditEvents();
      }
    }

    // Dialog checkbox events
    jQuery('#dialog-school-admins').on('click', 'input#all-schools, input.school-cb', function() {

        if (jQuery(this).attr('id') == "all-schools") {
            jQuery('input.school-cb').prop(
               'checked',
               jQuery('input#all-schools').prop('checked')
            );
        } else {
            jQuery('input#all-schools').prop(
               'checked',
               (jQuery('input.school-cb:checked').length == jQuery('input.school-cb').length)
            );
        }

    });

    // Link to schools dialog
    jQuery('#admin_table').on('click', 'img.admin-schools', function() {

      var parentTR = jQuery(this).parents('td:first').parents('tr:first');
      var userID = parentTR.find("input[type=checkbox]").val();

      jQuery('#dialog-school-admins').dialog({
          autoOpen: true,
          height: 500,
          width: 560,
          modal: true,
          "open": function() {

            jQuery('#link-error, #linked-schools').hide();
            jQuery('#link-loading').show();
            jQuery('#admin-surname').text(jQuery('#'+ parentTR.attr('id') + ' td').eq(3).text());
            jQuery('#admin-forenames').text(jQuery('#'+ parentTR.attr('id') + ' td').eq(2).text());

            jQuery(this).off('submit').on('submit', function () {
              return saveLinkedSchools(userID);
            });

            loadLinkedSchools(userID);

         },
         "close": function() {
            jQuery('#linked-schools li.school-li').remove();
            jQuery('#admin-surname, #admin-forenames').empty();
         },
         create:function () {
              $(this).closest(".ui-dialog")
                  .find(".ui-button:contains('Cancel')") // the first button
                  .addClass("btn btn-sm btn-danger")
                  .removeClass("ui-button");

              $(this).closest(".ui-dialog")
                  .find(".ui-button:contains('Update')") // the first button
                  .addClass("btn btn-sm btn-success")
                  .removeClass("ui-button");
          },
         buttons: {
          "Cancel": function() {
            jQuery(this).dialog('close');
          },
          "Update": function() {
            return saveLinkedSchools(userID);
          },
        }
      });

    });

    // Prepare password validation (jquery-password-fields.js)
    preparePasswordValidation();

    // Show menu and perform scroll
    showMenu();
    performScroll();


});

// Saved linked schools
function saveLinkedSchools(user) {

    jQuery('#link-error').hide();

    var schools = jQuery('input.school-cb:checked').map(function() {
        return jQuery(this).val();
    }).get();

    if (schools.length == 0) {
      jQuery('#link-error')
      .text('Please select at least 1 from list')
      .show();
      return false;
    }

    jQuery.ajax({
        url: "admins/json_admin.php",
        type: "post",
        data: {
           isfor: "save_admin_schools",
           user: user,
           schools: schools.join(',')
        },
        dataType: "json"
    })
    .done(function() {
        jQuery('#dialog-school-admins').dialog('close');
    })
    .fail(function(jqXHR) {

         var message = httpStatusCodeHandler(
           jqXHR.status,
           "Failed to save records.\n"
         );

         jQuery('#link-error')
         .html(message)
         .show();

    });
}
// Load linked schools
function loadLinkedSchools(user) {
     jQuery.ajax({
        url: "admins/json_admin.php",
        type: "post",
        data: {
           isfor: "get_admin_schools",
           user: user
        },
        dataType: "json"
     })
     .done(function(schools) {

         jQuery('#linked-schools li.school-li').remove();
         jQuery('#link-loading').hide();

         if (schools.length == 0) {
             jQuery('#linked-schools').append(
                jQuery('<li>', { 'text': 'No records found to link to', 'class': 'school-cb' })
             );
             jQuery('#linked-schools').show();
             return false;
         } else {

           var allChecked = true;
           jQuery.each(schools, function() {
             var school = jQuery(this)[0];

             if (!school.linked) {
                allChecked = false;
             }

             var li = jQuery('<li>', { 'class': 'school-li'}).append(
                jQuery('<input>', {
                       'type': 'checkbox', 'checked': school.linked,
                       'class': 'school-cb', 'id': ('link' + school.id),
                       'value': school.id
                    }
                 ),
                jQuery("<label>", {'text': school.name, 'for': ('link' + school.id) })
             );

             // Append to list
             jQuery('#linked-schools').append(li);
          });

          // Check all
          jQuery('#all-schools').prop('checked', allChecked);
          jQuery('#linked-schools').show();
         }

      })
     .fail(function(jqXHR) {

         jQuery('#linked-schools li.school-li').remove();
         jQuery('#link-loading').hide();

         var message = httpStatusCodeHandler(
           jqXHR.status,
           "Failed to retrieve records.\n"
         );

         jQuery('#link-error')
         .html(message)
         .show();

     });
}


/*
 * Edit administrators events
 */
function adminsEditEvents() {
   if (jQuery('#admin_table').length && jQuery('#count').length) {
      var buttons = jQuery('#admin_table').find('a[class=editba]');

      jQuery.each(buttons, function(i, button) {
          adminEditEvent(button);
      });
    }
}

/*
 * Edit administrator event
 * button object - edit button
 */
function adminEditEvent(button) {
  jQuery(button).off('click').on('click', function (e) {
    e.preventDefault();
    var parentRow = jQuery(this).parents('tr:first');
    var originalRowClone = parentRow.clone(true, true);
    var adminID = parentRow.find('input[id^=admin-check]').val();
    var url = "admins/ajx_admin.php";
    var random = Math.random();

    jQuery('#admin_table').find("input, select, a:not([class~=orderlink]), img").css('display', 'none');
     setWaitStatusRow(parentRow, 9);

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: 'edit_admin',
            id: adminID
        }
     })
     .done(function(html){
        if (!html.indexOf('USER_NON_EXIST') > -1) {
            parentRow.empty().html(html);
            parentRow.attr('class', "editrw");
            parentRow.attr('id', "edit_row");
            editButtonsRow(jQuery(parentRow), 9);
            editAdminCancelEvent(jQuery('#edit-cancel'), originalRowClone, parentRow);
            editAdminUpdateEvent(jQuery('#edit-update'));
            passwordFieldsEvents(jQuery('.password-fields-td'), false);
         } else {
            var message = "This Administrator does not exist in the System anymore, " +
                          "click 'OK' button to proceed with refreshing list";
            alert(message);
            refreshPage(false);
         }
     })
     .fail(function(jqXHR) {
        parentRow.replaceWith(originalRowClone);
        jQuery('#admin_table').find("input, select, a, img").css('display', '');
        adminEditEvent(originalRowClone.find('a[class=editba]'));
        addCheckboxEvent(originalRowClone.find('input[id^=admin-check]'), 'admin');
        alert('The request failed, Please try again');
     });
    });
}


/*
 * Edit admin update event
 * @param object button - edit button
 * @returns void
 */
function editAdminUpdateEvent(button) {
   jQuery(button).on('click', function(e) {
     e.preventDefault();

     // All fields complete ?
     if (!areFieldsComplete(jQuery('#edit_row'))) {
         alert("Please complete all fields");
         return false;
     }

    // Validate record ID field
    if (!isRecordIDValid(jQuery('#edit_id').val(), false)) {
        return false;
    }

     /* Change password action required? (jQuery) */
     var changePasswordChecked = jQuery('.change-password').prop('checked');
     if (changePasswordChecked) {

       // If password fields are not valid then abort
       if (!passwordFieldsValid('edit')) {
           return false;
       }

     }

     var url = "admins/ajx_admin.php";
     var random = Math.random();

     jQuery('#edit-update').css('display', 'none');
     jQuery('#edit-cancel').css('display', 'none');
     jQuery('#b_edit_td').attr('class', 'waittd2');

     jQuery.ajax({
        url: url,
        type: "post",
        data: {
            sid: random,
            isfor: 'update_admin',
            orgid: jQuery('#org_id').val(),
            id: jQuery('#edit_id').val(),
            fn: jQuery('#edit_fn').val(),
            sn: jQuery('#edit_sn').val(),
            em: jQuery('#edit_em').val(),
            pass: jQuery('.first-password-field').val(),
            passc: (changePasswordChecked ? 1 : 0),
            lv: jQuery('#edit_lvl').val(),
            ac: jQuery('#edit_ac').val()
        }
     })
     .done(function(html){
        if (html.indexOf('USER_NON_EXIST') > -1) {
            failEdit();
            var message = "The Administrator you are trying to update does not exist " +
                          "in the system anymore, refreshing page";
            alert(message);
            refreshPage(false);
          } else if (html.indexOf('USER_ALREADY_USED') > -1) {
            failEdit();
            var message = "The Administrator ID you specified is already in use," +
                          "please try a different ID";
            alert(message);
          } else {
            refreshPage(false);
          }
     })
     .fail(function(jqXHR) {
        failEdit();
        alert('The request failed, Please try again');
     });
    });

}

/*
 * Cancel event when editing administrator record
 * @param object button - cancel button
 * @param object oldRowClone - old row cloned
 * @param object editRow - Current edit row
 * @returns void
 */
function editAdminCancelEvent(button, oldRowClone, editRow) {

    jQuery(button).off('click').on('click', function (e) {

        e.preventDefault();
        jQuery('#b_edit').remove();
        editRow.replaceWith(oldRowClone);
        jQuery('#admin_table').find("input, a, img, select").css('display', '');
        var orginalEditButton = oldRowClone.find('a[class=editba]');
        var originalCheckbox = oldRowClone.find('input[id^=admin-check]');
            originalCheckbox.css('display', '');
    
        adminEditEvent(orginalEditButton);
        addCheckboxEvent(originalCheckbox, "admin");
    
      });
}

/*
 * New administrator add event
 */
function newAdminAddEvent() {
    if (!jQuery('#add').length) {
        return;
    }

    jQuery('#add').on('click', function(e) {
        e.preventDefault();
        jQuery('#admin_table').find("input, select, img, a:not([class~=orderlink])").css('display', 'none');

        if (jQuery('#first_admin_row').length) {
            var originalRowClone = jQuery('#first_admin_row').remove();
            var lastRow = jQuery('#title-row');
        }
        else {
          if (jQuery('#count').length) {
             var lastRow = jQuery('#admin_row_' + jQuery('#count').val());
          }
        }
        var newRow = jQuery('<tr>', {'id': 'add-row'});
        lastRow.after(newRow);

        setWaitStatusRow(newRow, 9);

        var url = "admins/ajx_admin.php";
        var random = Math.random();
        jQuery.ajax({
            url: url,
            type: "post",
            data: {
                sid: random,
                isfor: 'add_admin'
            }
         })
         .done(function(html){
            newRow.empty().attr('class', 'addrow').html(html);
            addButtonsRow();
            addAdminSubmitEvent();
            addAdminCancelEvent(newRow, originalRowClone);
            passwordFieldsEvents(jQuery('.password-fields-td'), true);
            jQuery('#add_id').focus();
            jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
         })
         .fail(function(jqXHR) {
            newRow.remove();
            jQuery('#admin_table').find("input, select, a, img").css('display', '');
            alert('The request failed, Please try again');
         });
    });
}

/*
 * Submit event for adding a  new administrator
 */
function addAdminSubmitEvent() {
    jQuery('#add-save').on('click', function (e) {
        e.preventDefault();

        // All fields are complete ?
        if (!areFieldsComplete(jQuery('#add-row'))) {
            alert("Please complete all fields");
            return false;
        }

        // Email address is valid ?
        else if (!isEmailValid(jQuery('#add_em').val())) {
            alert("Email Address specified is not valid");
            return false;
        }

        // Validate record ID field
        if (!isRecordIDValid(jQuery('#add_id').val(), false)) {
            return false;
        }

        /* Password fields check (jQuery) */
        else if (!passwordFieldsValid('add')) {
            return false;

            // If we've passed all checks then send request
        } else {
            jQuery(this).css('display', 'none');
            jQuery('#add-cancel').css('display', 'none');
            jQuery('#base-button-td').attr('class', 'waittd3');

            var url = "admins/ajx_admin.php";
            var random = Math.random();

            jQuery.ajax({
                url: url,
                type: "post",
                data: {
                    sid: random,
                    isfor: 'submit_admin',
                    id: jQuery('#add_id').val(),
                    fn: jQuery('#add_fn').val(),
                    sn: jQuery('#add_sn').val(),
                    em: jQuery('#add_em').val(),
                    pass: jQuery('.first-password-field').val(),
                    lv: jQuery('#add_lvl').val(),
                    ac: jQuery('#add_ac').val()
                }
             })
             .done(function(html){
                if (html.indexOf('USER_EXISTS') > -1) {
                    failAdd();
                    var message = "Administrator ID specified already exists as a User " +
                            "ID in the System, please specify a different ID";
                    alert(message);
                } else {
                    prepareScroll('scrolltobase');
                    parent.location = 'manage.php?page=man_admin';
                }
             })
             .fail(function(jqXHR) {
                failAdd();
                alert('The request failed, Please try again');
             });
        }
    });
}

/*
 * Cancel even for adding a new admin record
 *
 * @param  object addRow -  current new record row
 * @param {type} originalRowClone - original row cloned
 * @returns void
 */
function addAdminCancelEvent(addRow, originalRowClone) {

   jQuery('#add-cancel')
        .off('click')
        .on('click', function (e) {

            e.preventDefault();
            if (!jQuery('#count').length) {
               addRow.replaceWith(originalRowClone);
            } else {
               addRow.remove();
            }
            jQuery('#admin_table').find("select, img, input, a").css('display', '');
            removeNewRecordControls();
         });

}

/*
 * Validate form
 * @param object form - form to validate
 * @returns Boolean
 */
function ValidateForm(form) {

  // Administrator list
  if (jQuery(form).attr('name') === 'admin_list_form') {
    var isChecked = false;
    var checkboxes = jQuery('#admin_list_form').find('input[id^=admin-check]');

    jQuery.each(checkboxes, function(i, cb) {
     /* This uses a bitwise OR to update isChecked. If any checkbox is
      * checked, isChecked will be true by the end of the loop.
      */
      isChecked |= jQuery(cb).prop('checked');
    });

    if (isChecked) {
     var message = "** WARNING PLEASE READ **\n\n\n" +
                   "Are you sure you want to delete the selected Administrators?";
     return confirm(message);
    }
  }
  return false;
}
