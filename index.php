<?php

define('_OMIS', 1);
$loader = require 'vendor/autoload.php';
session_start();

$config = new OMIS\Config;
$pageWithMenus = false;
$page = "login";
$_SESSION['base'] = basename(__DIR__);

include 'extra/header_html.php'

?>
<div id="wrapper"><?php

  $template = new OMIS\Template('header_login.html.twig');
  $template->render([
    "skin" => $config->localization['skin'],
    "name" => [
      "short" => $config->getName(),
      "long" => $config->getName(\TRUE)
    ]
  ]);

  // Capture error
  $error = iS($_SESSION, "loginerror");

  // Unset error in session
  unset($_SESSION['loginerror']);

  // Get app version and release date
  $release = \OMIS\Release::getInstance();
  $releaseData = [
    "version" => $release::$version,
    "date" => $release::$date
  ];

  // Loggedin Status
  $loggedIn = !(!isset($_SESSION['instance_identifier'])
    || $_SESSION['instance_identifier'] != $config->installation_id);

  // If no session then render the login template
  if (!$loggedIn) {
      
    $isExamToday = $db->exams->examOnDate(date("Y-m-d"));
    $examLabel = trim(ucwords(gettext('exam')));
    $examsLabel = trim(ucwords(gettext('exams')));

    // Boolean flag to set if the Manage exams option is set by default
    $manageDefault = 
      (isset($_SESSION['todo']) && $_SESSION['todo'] == 1) ||
      (!isset($_SESSION['todo']) && !$isExamToday);

    // Get the label to show in the form before the release
    $localization = $config->localization['skin'];
    $loginSettings = $localization['login'];
    $fieldsTitle = (isset($loginSettings['fields_title']) &&
      strlen($loginSettings['fields_title'] > 0)) ?
      $localization['login']['fields_title'] :
      $config->getName();

    // Links to show in the bottom row
    $bottomRowOptions = [
      'forgot password' => 'manage.php?page=request'
    ];

    // Additional link login screen (sample)
    // $bottomRowOptions['visitors'] = 'manage.php?page=main';

    // Check if the user just logged out from SSO
    $ssoLogout = filter_has_var(INPUT_GET, 'logout') && filter_input(INPUT_GET, 'logout', FILTER_SANITIZE_STRING) == 'sso';

    // Render de template passing the data
    $loginTemplate = new OMIS\Template('login.html.twig');
    $loginTemplate->render([
      "fieldsTitle" => $fieldsTitle,
      "release" => $releaseData,
      "error" => $error,
      "sessionUserId" => iS($_SESSION, 'userinputid'),
      "examLabel" => $examLabel,
      "examsLabel" => $examsLabel,
      "manageDefault" => $manageDefault,
      "bottomRowOptions" => $bottomRowOptions,
      "ssoEnabled" => $config->isSSOEnabled(),
      "ssoLogout" => $ssoLogout,
      "clientLogoExists" => file_exists("storage/custom/images/client-logo-login.png")
    ]);

  } else {

    // If the user is logged in

    $termLabel = trim(ucwords(gettext('academic term')));

    $cterm = isset($_SESSION['cterm']) ?
      $_SESSION['cterm'] :
      'Not Known';

    $landingTemplate = new OMIS\Template('landing.html.twig');
    $landingTemplate->render([
      "userID" => $_SESSION['user_identifier'],
      "lastLogin" => $_SESSION['lastlogin'],
      "termLabel" => $termLabel,
      "academicYear" => $cterm,
      "release" => $releaseData
    ]);

  }

  ?></div>
  <?php

  (new OMIS\Template('footer.html.twig'))->render([
    'loginScreen' => true,
    'skin' => $config->localization['skin'],
  ]);
  ?>
  </body>
  </html>
