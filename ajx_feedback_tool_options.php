<?php

define('_OMIS', 1);

require_once __DIR__ . '/../extra/essentials.php';

// Page Access Check / Can User Access this Section?
//if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
//    return false;
//}

$jsonInput = file_get_contents('php://input');
$input = json_decode($jsonInput, true);

$optionsToFilter = $input['options'];
$examId = $input['examId'];

$enabledOptions = (new \OMIS\Feedback\Options\FeedbackOptionFilter($config))->filterEnabledOptions($optionsToFilter, $examId);

header('Content-Type: application/json');
echo json_encode($enabledOptions);