<?php
 /**
  * @author David Cunningham <david.cunningham@qpercom.ie>
  * For Qpercom Ltd
  * @copyright Copyright (c) 2019, Qpercom Limited
  * @Email Feedback to Students
  */

 define('_OMIS', 1);

 use \OMIS\Utilities\JSON as JSON;
 use \OMIS\Utilities\HttpStatusCode as HttpStatusCode;

 include_once __DIR__ . '/../vendor/autoload.php';

 $examID = filter_input(INPUT_POST, 'exam', FILTER_SANITIZE_NUMBER_INT);

 // Exam ID is not empty then include necessary classes
 if (!empty($examID)) {
  
    require_once __DIR__ . '/../extra/essentials.php';
    
    if (!\OMIS\Auth\Role::loadID($_SESSION['user_role'])->canAccess()) {
      
       http_response_code(HttpStatusCode::HTTP_FORBIDDEN);
       exit();
       
    }
 } else {
  
       http_response_code(HttpStatusCode::HTTP_BAD_REQUEST);
       die ("Not authorized to view this section");
       
 }

 //Unlimited Script Time
 set_time_limit(0);

 $db->set_Convert_Html_Entities(false);
 $db->feedback->setFeedbackReady($examID);

 // Load Feedback Templates
 $templates = $db->feedback->getEmailTemplates(NULL, true, 'template_id');

 // Output array
 $output = [];

 // Return data
 $returnData = [];

 // Return Status 0 = Success
 $return;

 // User session identifier
 $userIdentifier = $_SESSION['user_identifier'];

 /**
  * From address settings
  */
 if (filter_var($config->email['services']['from'], FILTER_VALIDATE_EMAIL) !== false) {

    $fromAddress = $config->email['services']['from'];

 } else {

    $fromAddress = $_SESSION['user_email'];

 }

 /**
  * From name settings
  */
 if (isset($config->email['services']['from_name']) && !empty($config->email['services']['from_name'])) {

    $fromName = $config->email['services']['from_name'];

 } else {

    $fromName = $_SESSION['user_forename'] . ' ' . $_SESSION['user_surname'];

 }

 /**
  * Reply-to address settings
  * Note: Reply-to settings 'StudentFeedbackJob.ReplyToEmailAddress' and 'StudentFeedbackJob.ReplyToFriendlyName'
  * MUST exist (not necessarily complete) in the .NET harness config file so that these settings (from OMIS config file)
  * can override them
  */
 if (filter_var($config->email['services']['reply'], FILTER_VALIDATE_EMAIL) !== false) {

    $replyAddress = $config->email['services']['reply'];

 } else {

    $replyAddress = $_SESSION['user_email'];

 }

/**
 * Reply name settings
 */
 if (isset($config->email['services']['reply_name']) && !empty($config->email['services']['reply_name'])) {

    $replyName = $config->email['services']['reply_name'];

 } else {

    $replyName = $_SESSION['user_forename'] . ' ' . $_SESSION['user_surname'];

 }

 // Email Service Parameters (Include current user email)
 $emailServiceParameters = ' -j1,'
      . 'FromEmailAddress:"' . $fromAddress . '",'
      . 'FromFriendlyName:"' . $fromName . '",'
      . 'ReplyToEmailAddress:"' . $replyAddress . '",'
      . 'ReplyToFriendlyName:"' . $replyName . '"';

 $platform = trim(php_uname('s'));
 switch ($platform) {
    case 'Windows NT':
        $emailService = $config->email['services']['windows'];
        break;
    case 'Linux':
        $emailService = $config->email['services']['linux'];
        break;
    default:
        $emailService = null;
 }

 /* If we don't know what mailer to use, then log that fact and give up. As the
  * AJAX script that calls this script watches the HTTP status code, let it know
  * that something went wrong so that it knows that the e-mails weren't sent.
  */
 if (is_null($emailService)) {
    error_log(__FILE__ . ": No mailer configured for platform '$platform'");
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
 }

 // Close Session to allow other PHP scripts to run
 session_write_close();

 // Send the e-mail, if we can
 exec($emailService . $emailServiceParameters, $output, $return);

 $examDB = $db->exams->getExam($examID);
 \Analog::info($userIdentifier . " emailed feedback for " . $examDB['exam_name']);

 //Renew connection if required
 $db->renewConnection($config->db);

 // Get Feedback Records
 $feedbackStudents = $db->feedback->getStudentFeedbackRecords($examID);

 while ($each = $db->fetch_row($feedbackStudents)) {
    
    if ($each['time_created'] != null) {
           
        $returnData[] = [
            'id' => $each['student_id'],
            'time_sent' => $each['time_sent'],
            'is_sent' => $each['is_sent']
        ];
    }
    
 }

 try {
    $json = JSON::encode($returnData);
 } catch (Exception $ex) {
    // Something went wrong encoding the data to JSON.
    error_log(__FILE__ . ": JSON Encode failed. Data follows:");
    error_log(print_r($returnData, true));
    /* The only correct thing to do here is return an internal server error
     * message.
     */
    http_response_code(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR);
    exit();
 }

 echo $json;
